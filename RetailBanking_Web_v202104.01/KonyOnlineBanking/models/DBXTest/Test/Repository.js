define([], function() {
    var BaseRepository = kony.mvc.Data.BaseRepository;

    //Create the Repository Class
    function TestRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
        BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
    };

    //Setting BaseRepository as Parent to this Repository
    TestRepository.prototype = Object.create(BaseRepository.prototype);
    TestRepository.prototype.constructor = TestRepository;

    //For Operation 'createTransferMFAFlowTest' with service id 'createTransfer4437'
    TestRepository.prototype.createTransferMFAFlowTest = function(params, onCompletion) {
        return TestRepository.prototype.customVerb('createTransferMFAFlowTest', params, onCompletion);
    };


    return TestRepository;
})