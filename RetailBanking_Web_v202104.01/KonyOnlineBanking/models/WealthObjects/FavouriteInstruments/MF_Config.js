/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"id": "id",
		"userId": "userId",
		"customerId": "customerId",
		"favInstrumentCodes": "favInstrumentCodes",
		"RICCode": "RICCode",
		"operation": "operation",
	};

	Object.freeze(mappings);

	var typings = {
		"id": "string",
		"userId": "string",
		"customerId": "string",
		"favInstrumentCodes": "string",
		"RICCode": "string",
		"operation": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"id",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "WealthObjects",
		tableName: "FavouriteInstruments"
	};

	Object.freeze(config);

	return config;
})