#!/bin/bash

if ["$1" != ""];
then
echo $1
else
    error_exit $? "Kony Visualizer Web Archive File Path must be present"
fi

#tempdir = $(mktemp - d)

# - d is for directory
unzip $1 -d tempdir
rm -rf $1
cd tempdir

cat meta.json | sed -n 's|.*"cacheId":"\([^"]*\)".*|\1|p' > cacheIdFile.txt

value=$(<cacheIdFile.txt)

rm -rf cacheIdFile.txt

cd $value
cd desktopweb
mkdir obf

cd appjs
mv app.js ../obf/app.js
mv BusinessBanking/*Controller.js ../obf
mv *Module/*Controller.js ../obf


cd ../jslib
mv konyframework.js ../obf/konyframework.js



#Obfuscate file remove original and make obfuscated in common folder
cd ..

javascript-obfuscator obf --compact true --control-flow-flattening false --rename-globals false --unicode-escape-sequence false --self-defending true --log true

cd obf
rm -rf app.js konyframework.js kvmodules.js *Controller.js
mv konyframework-obfuscated.js ../jslib/konyframework.js
mv app-obfuscated.js ../appjs/app.js

#Accounts Module
mv frmAccountsDetailsController-obfuscated.js ../appjs/AccountsModule/frmAccountsDetailsController.js
mv frmAccountsLandingController-obfuscated.js ../appjs/AccountsModule/frmAccountsLandingController.js
mv frmPrintTransactionController-obfuscated.js ../appjs/AccountsModule/frmPrintTransactionController.js
mv frmPrintTransferController-obfuscated.js ../appjs/AccountsModule/frmPrintTransferController.js
mv frmScheduledTransactionsController-obfuscated.js ../appjs/AccountsModule/frmScheduledTransactionsController.js

#AlertsMsgsModule
mv frmNotificationsAndMessagesController-obfuscated.js ../appjs/AlertsMsgsModule/frmNotificationsAndMessagesController.js

#AuthModule
mv frmEnrollNowController-obfuscated.js ../appjs/AuthModule/frmEnrollNowController.js
mv frmLoginController-obfuscated.js ../appjs/AuthModule/frmLoginController.js
mv frmLoginLanguageController-obfuscated.js ../appjs/AuthModule/frmLoginLanguageController.js
mv frmMFAPreLoginController-obfuscated.js ../appjs/AuthModule/frmMFAPreLoginController.js

#BillPayModule
mv frmAcknowledgementController-obfuscated.js ../appjs/BillPayModule/frmAcknowledgementController.js
mv frmAddPayeeController-obfuscated.js ../appjs/BillPayModule/frmAddPayeeController.js
mv frmBillPayController-obfuscated.js ../appjs/BillPayModule/frmBillPayController.js
mv frmConfirmController-obfuscated.js ../appjs/BillPayModule/frmConfirmController.js

#BusinessBanking
mv frmBBUsersDashboardController-obfuscated.js ../appjs/BusinessBanking/frmBBUsersDashboardController.js
mv frmUserManagementController-obfuscated.js ../appjs/BusinessBanking/frmUserManagementController.js

#CardManagementModule
mv frmCardManagementController-obfuscated.js ../appjs/AlertsMsgsModule/frmCardManagementController.js

#FeedbackModule
mv frmCustomerFeedbackController-obfuscated.js ../appjs/FeedbackModule/frmCustomerFeedbackController.js

#InformationContentModule
mv frmContactUsPrivacyTandCController-obfuscated.js ../appjs/InformationContentModule/frmContactUsPrivacyTandCController.js
mv frmOnlineHelpController-obfuscated.js ../appjs/InformationContentModule/frmOnlineHelpController.js

#LoanPayModule
mv frmPayDueAmountController-obfuscated.js ../appjs/LoanPayModule/frmPayDueAmountController.js

#LocateUsModule
mv frmLocateUsController-obfuscated.js ../appjs/LocateUsModule/frmLocateUsController.js

#MultiFactorAuthenticationModule
mv frmMFATransactionsController-obfuscated.js ../appjs/MultiFactorAuthenticationModule/frmMFATransactionsController.js
mv frmMultiFactorAuthenticationController-obfuscated.js ../appjs/MultiFactorAuthenticationModule/frmMultiFactorAuthenticationController.js

#NAOModule
mv frmNAOController-obfuscated.js ../appjs/NAOModule/frmNAOController.js

#NUOModule
mv frmNewUserOnboardingController-obfuscated.js ../appjs/NUOModule/frmNewUserOnboardingController.js

#PayAPersonModule
mv frmPayAPersonController-obfuscated.js ../appjs/PayAPersonModule/frmPayAPersonController.js

#PersonalFinanceManagementModule
mv frmPersonalFinanceManagementController-obfuscated.js ../appjs/PersonalFinanceManagementModule/frmPersonalFinanceManagementController.js

#ProfileModule
mv frmProfileManagementController-obfuscated.js ../appjs/ProfileModule/frmProfileManagementController.js

#StopPaymentsModule
mv frmStopPaymentsController-obfuscated.js ../appjs/StopPaymentsModule/frmStopPaymentsController.js

#SurveyModule
mv frmCustomerFeedbackSurveyController-obfuscated.js ../appjs/SurveyModule/frmCustomerFeedbackSurveyController.js

#TransferEurModule
mv frmAcknowledgementEurController-obfuscated.js ../appjs/TransferEurModule/frmAcknowledgementEurController.js
mv frmAddExternalAccountEurController-obfuscated.js ../appjs/TransferEurModule/frmAddExternalAccountEurController.js
mv frmAddInternalAccountEurController-obfuscated.js ../appjs/TransferEurModule/frmAddInternalAccountEurController.js
mv frmConfirmAccountEurController-obfuscated.js ../appjs/TransferEurModule/frmConfirmAccountEurController.js
mv frmConfirmEurController-obfuscated.js ../appjs/TransferEurModule/frmConfirmEurController.js
mv frmTransfersEurController-obfuscated.js ../appjs/TransferEurModule/frmTransfersEurController.js
mv frmVerifyAccountEurController-obfuscated.js ../appjs/TransferEurModule/frmVerifyAccountEurController.js

#TransferModule
mv frmAddExternalAccountController-obfuscated.js ../appjs/TransferModule/frmAddExternalAccountController.js
mv frmAddInternalAccountController-obfuscated.js ../appjs/TransferModule/frmAddInternalAccountController.js
mv frmConfirmAccountController-obfuscated.js ../appjs/TransferModule/frmConfirmAccountController.js
mv frmTransfersController-obfuscated.js ../appjs/TransferModule/frmTransfersController.js
mv frmVerifyAccountController-obfuscated.js ../appjs/TransferModule/frmVerifyAccountController.js

#WireTransferModule
mv frmWireTransferController-obfuscated.js ../appjs/TransferModule/frmWireTransferController.js

#FastTransfreModule
mv frmConfirmTransferController-obfuscated.js ../appjs/TransferFastModule/frmConfirmTransferController.js
mv frmFastActiveAckController-obfuscated.js ../appjs/TransferFastModule/frmFastActiveAckController.js
mv frmFastActiveRecipientController-obfuscated.js ../appjs/TransferFastModule/frmFastActiveRecipientController.js
mv frmFastActiveServicesController-obfuscated.js ../appjs/TransferFastModule/frmFastActiveServicesController.js
mv frmFastAddDBXAccountAcknowledgementController-obfuscated.js ../appjs/TransferFastModule/frmFastAddDBXAccountAcknowledgementController.js
mv frmFastAddDBXAccountConfirmController-obfuscated.js ../appjs/TransferFastModule/frmFastAddDBXAccountConfirmController.js
mv frmFastAddDBXAccountController-obfuscated.js ../appjs/TransferFastModule/frmFastAddDBXAccountController.js
mv frmFastAddExternalAccountAcknowledgementController-obfuscated.js ../appjs/TransferFastModule/frmFastAddExternalAccountAcknowledgementController.js
mv frmFastAddExternalAccountConfirmController-obfuscated.js ../appjs/TransferFastModule/frmFastAddExternalAccountConfirmController.js
mv frmFastAddExternalAccountController-obfuscated.js ../appjs/TransferFastModule/frmFastAddExternalAccountController.js
mv frmFastAddInternationalAccountAcknowledgementController-obfuscated.js ../appjs/TransferFastModule/frmFastAddInternationalAccountAcknowledgementController.js
mv frmFastAddInternationalAccountConfirmController-obfuscated.js ../appjs/TransferFastModule/frmFastAddInternationalAccountConfirmController.js
mv frmFastAddInternationalAccountController-obfuscated.js ../appjs/TransferFastModule/frmFastAddInternationalAccountController.js
mv frmFastAddRecipientAcknowledgementController-obfuscated.js ../appjs/TransferFastModule/frmFastAddRecipientAcknowledgementController.js
mv frmFastAddRecipientConfirmController-obfuscated.js ../appjs/TransferFastModule/frmFastAddRecipientConfirmController.js
mv frmFastAddRecipientController-obfuscated.js ../appjs/TransferFastModule/frmFastAddRecipientController.js
mv frmFastDeActiveRecipientController-obfuscated.js ../appjs/TransferFastModule/frmFastDeActiveRecipientController.js
mv frmFastDeActiveRecipienttAcknowledgementController-obfuscated.js ../appjs/TransferFastModule/frmFastDeActiveRecipienttAcknowledgementController.js
mv frmFastManagePayeeController-obfuscated.js ../appjs/TransferFastModule/frmFastManagePayeeController.js
mv frmFastRecipientGateWayController-obfuscated.js ../appjs/TransferFastModule/frmFastRecipientGateWayController.js
mv frmFastTransfersActivitesController-obfuscated.js ../appjs/TransferFastModule/frmFastTransfersActivitesController.js
mv frmFastTransfersController-obfuscated.js ../appjs/TransferFastModule/frmFastTransfersController.js
mv frmFastVerifyAccountController-obfuscated.js ../appjs/TransferFastModule/frmFastVerifyAccountController.js
mv frmFastViewActivityController-obfuscated.js ../appjs/TransferFastModule/frmFastViewActivityController.js
mv frmP2PSettingsController-obfuscated.js ../appjs/TransferFastModule/frmP2PSettingsController.js
mv frmReviewController-obfuscated.js ../appjs/TransferFastModule/frmReviewController.js

#TermsandConditionsModule

mv frmPreTermsandConditionController-obfuscated.js ../appjs/TermsAndConditionsModule/frmPreTermsandConditionController.js

#BulkWireTransfer

mv frmBulkWireActivityAllController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireActivityAllController.js
mv frmBulkWireActivityPendingController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireActivityPendingController.js
mv frmBulkWireActivitySuccessController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireActivitySuccessController.js
mv frmBulkWireActivityFailedController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireActivityFailedController.js
mv frmBulkTransferFilesController-obfuscated.js ../appjs/WireTransferNew/frmBulkTransferFilesController.js
mv frmAddBulkTransferFileController-obfuscated.js ../appjs/WireTransferNew/frmAddBulkTransferFileController.js
mv frmMakeBulkTransferController-obfuscated.js ../appjs/WireTransferNew/frmMakeBulkTransferController.js
mv frmBulkWireConfirmTransferController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireConfirmTransferController.js
mv frmBulkWireEditRecipientController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireEditRecipientController.js
mv frmBulkWireFileTransactionsActivityController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireFileTransactionsActivityController.js
mv frmBulkWireTransferRemovedRecipientsController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireTransferRemovedRecipientsController.js
mv frmBulkWireViewFileDetailsController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireViewFileDetailsController.js
mv frmCreateTempSelectRecipientsController-obfuscated.js ../appjs/WireTransferNew/frmCreateTempSelectRecipientsController.js
mv frmAddBulkTemplateFileController-obfuscated.js ../appjs/WireTransferNew/frmAddBulkTemplateFileController.js
mv frmBWTCreateRecipientFileController-obfuscated.js ../appjs/WireTransferNew/frmBWTCreateRecipientFileController.js
mv frmCreateTemplatePrimaryDetailsController-obfuscated.js ../appjs/WireTransferNew/frmCreateTemplatePrimaryDetailsController.js
mv frmBulkTemplateAddRecipientsController-obfuscated.js ../appjs/WireTransferNew/frmBulkTemplateAddRecipientsController.js
mv frmBulkwireCreateTemplateConfirmController-obfuscated.js ../appjs/WireTransferNew/frmBulkwireCreateTemplateConfirmController.js
mv frmBulkWireCreateTemplateAckController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireCreateTemplateAckController.js
mv frmMakeBulkTransferTemplateController-obfuscated.js ../appjs/WireTransferNew/frmMakeBulkTransferTemplateController.js
mv frmEditTemplatePrimaryDetailsController-obfuscated.js ../appjs/WireTransferNew/frmEditTemplatePrimaryDetailsController.js
mv frmEditBulkTemplateFileController-obfuscated.js ../appjs/WireTransferNew/frmEditBulkTemplateFileController.js
mv frmBWTEditRecipientFileController-obfuscated.js ../appjs/WireTransferNew/frmBWTEditRecipientFileController.js
mv frmBulkTemplateEditRecipientsController-obfuscated.js ../appjs/WireTransferNew/frmBulkTemplateEditRecipientsController.js
mv frmBulkWireViewTemplateController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireViewTemplateController.js
mv frmEditTempSelectRecipientsController-obfuscated.js ../appjs/WireTransferNew/frmEditTempSelectRecipientsController.js
mv frmBulkWireEditTemplateRecipientController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireEditTemplateRecipientController.js
mv frmBulkWireTemplateRemovedRecipientsController-obfuscated.js ../appjs/WireTransferNew/frmBulkWireTemplateRemovedRecipientsController.js


#SavingsPot

mv frmCreateGoalAckController-obfuscated.js ../appjs/SavingsPotModule/frmCreateGoalAckController.js
mv frmCreateGoalConfirmController-obfuscated.js ../appjs/SavingsPotModule/frmCreateGoalConfirmController.js
mv frmCreateBudgetConfirmController-obfuscated.js ../appjs/SavingsPotModule/frmCreateBudgetConfirmController.js
mv frmFundBudgetConfirmController-obfuscated.js ../appjs/SavingsPotModule/frmFundBudgetConfirmController.js
mv frmFundGoalConfirmController-obfuscated.js ../appjs/SavingsPotModule/frmFundGoalConfirmController.js
mv frmWithdrawBudgetConfirmController-obfuscated.js ../appjs/SavingsPotModule/frmWithdrawBudgetConfirmController.js
mv frmWithdrawGoalConfirmController-obfuscated.js ../appjs/SavingsPotModule/frmWithdrawGoalConfirmController.js
mv frmFundGoalController-obfuscated.js ../appjs/SavingsPotModule/frmFundGoalController.js
mv frmCreateBudgetController-obfuscated.js ../appjs/SavingsPotModule/frmCreateBudgetController.js
mv frmFundBudgetController-obfuscated.js ../appjs/SavingsPotModule/frmFundBudgetController.js
mv frmWithdrawGoalController-obfuscated.js ../appjs/SavingsPotModule/frmWithdrawGoalController.js
mv frmWithdrawBudgetController-obfuscated.js ../appjs/SavingsPotModule/frmWithdrawBudgetController.js
mv frmCreateSavingsPotController-obfuscated.js ../appjs/SavingsPotModule/frmCreateSavingsPotController.js
mv frmCreateBudgetAckController-obfuscated.js ../appjs/SavingsPotModule/frmCreateBudgetAckController.js
mv frmFundBudgetAckController-obfuscated.js ../appjs/SavingsPotModule/frmFundBudgetAckController.js
mv frmFundGoalAckController-obfuscated.js ../appjs/SavingsPotModule/frmFundGoalAckController.js
mv frmWithdrawBudgetAckController-obfuscated.js ../appjs/SavingsPotModule/frmWithdrawBudgetAckController.js
mv frmWithdrawGoalAckController-obfuscated.js ../appjs/SavingsPotModule/frmWithdrawGoalAckController.js
mv frmCreateSavingsGoalController-obfuscated.js ../appjs/SavingsPotModule/frmCreateSavingsGoalController.js
mv frmEditBudgetController-obfuscated.js ../appjs/SavingsPotModule/frmEditBudgetController.js
mv frmSavingsPotLandingController-obfuscated.js ../appjs/SavingsPotModule/frmSavingsPotLandingController.js

cd ..
rm -rf obf
cd ../..

newvalue=$(( $value + 1 ))
mv $value $newvalue

sed -i -e "s+$value+$newvalue+g" meta.json

cd nocache/desktopweb
sed -i -e "s+$value+$newvalue+g" kony.manifest

cd ../..
if [ "$(uname -s)" == "Darwin" ]; then
   echo "*************************this one $1***********"
   cd ../tempdir
   zip -r "$1" . 
else
   "C:\Program Files\7-Zip\7z.exe" a -tzip "../$1"
fi

rm -rf tempdir