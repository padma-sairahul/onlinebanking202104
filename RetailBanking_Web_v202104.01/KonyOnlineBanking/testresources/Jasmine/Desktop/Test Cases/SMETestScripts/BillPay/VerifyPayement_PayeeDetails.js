it("VerifyPayement_PayeeDetails", async function() {

  await navigateToManagePayee();
  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
  await clickOnBillPayBtn_ManagePayees();
  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
  await selectfrequency_SheduledBillPay("Yearly");
  await SelectDateRange_SheduledBillpay();
  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Yearly");
  await confirmSheduledBillpay();
  await verifySheduledBillpaySuccessMsg();
  await verifyAccountsLandingScreen();
  
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");


//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Yearly");
//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");

//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");


},120000);