it("VerifyAddPayee_BillPayFlow", async function() {
  
  // Add payee and Then Delete same payee
  var unique_RecipitentName=BillPay.addPayee.unique_RecipitentName+new Date().getTime();
  var unique_Accnumber="0"+new Date().getTime();

  await navigateToBillPay();
  await clickOnAddPayeeLink();
  await enterPayeeDetails_UsingPayeeinfo(unique_RecipitentName,BillPay.addPayee.address1,BillPay.addPayee.address2,BillPay.addPayee.city,BillPay.addPayee.zipcode,unique_Accnumber,"PayBills_AddPayee");
  await clickOnNextButton_payeeDetails();
  await clickOnConfirmButton_verifyPayee();
  await verifyAddPayeeSuccessMsg();
  await verifyAccountsLandingScreen();
  
  //Activate Payee to initilize payments
  await navigateToManagePayee();
  await selectPayee_ManagePayeeList(unique_RecipitentName);
  await activateNewlyAddedpayee();
 

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);

//   await kony.automation.playback.waitFor(["frmBulkPayees","flxMakeOneTimePayment"]);
//   kony.automation.flexcontainer.click(["frmBulkPayees","flxMakeOneTimePayment"]);

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],"A");
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"]);
//   kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],"500055");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],"1234567890");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],"1234567890");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],"1234567890");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"]);
//   kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],"2.1");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],"test OneTime payment");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"]);
//   kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);

//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
//   kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);

//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);

//   await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
//   kony.automation.button.click(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
  
//   await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
//   kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
  
//   await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
//   kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
  
//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAddPayee"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAddPayee"],"text")).toEqual("Add Payee");
  
//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
  
  
//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","btnViewAllPayees"]);
//   kony.automation.button.click(["frmPayeeAcknowledgement","btnViewAllPayees"]);

//   await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");


},120000);