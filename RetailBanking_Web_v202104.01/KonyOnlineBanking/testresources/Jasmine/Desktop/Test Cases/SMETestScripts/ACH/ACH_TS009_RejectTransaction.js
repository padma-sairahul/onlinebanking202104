it("ACHTransact_Reject", async function() {
  
   await kony.automation.playback.wait(3000);
  kony.automation.button.click(["frmACHDashboard","dbRightContainerNew","btnAction2"]);
  await kony.automation.playback.wait(3000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbTemplateType"], "1");
  await kony.automation.playback.wait(5000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbRequestType"], "3");
  await kony.automation.playback.wait(4000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbAccount"], "200922062603619");
  await kony.automation.playback.wait(5000);
  kony.automation.textbox.enterText(["frmACHDashboard","tbxMaxAmt"],"1");
  await kony.automation.playback.waitFor(["frmACHDashboard","calEffectiveDate"]);
  kony.automation.calendar.selectDate(["frmACHDashboard","calEffectiveDate"], [9,10,2021]);
  await kony.automation.playback.waitFor(["frmACHDashboard","createFlowFormActionsNew","btnNext"]);
  kony.automation.button.click(["frmACHDashboard","createFlowFormActionsNew","btnNext"]);
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrName"],"Reject");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrAccountNumber"],"1234");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.listbox.selectItem(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","lstbxCrAccountType"], "1");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrTRCNumber"],"54666");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrDetailID"],"9897");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrAmount"],"1");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxAdditionalInfo"],"NA");
  //await kony.automation.playback.waitFor(["frmACHDashboard","CommonFormActionsNew","btnOption"]);
  await kony.automation.playback.wait(6000);
  kony.automation.button.click(["frmACHDashboard","CommonFormActionsNew","btnOption"]);
  await kony.automation.playback.wait(7000);
  kony.automation.button.click(["frmACHDashboard","CommonFormActionsNew","btnNext"]);
  
//Reject The TRansaction 
  await kony.automation.playback.wait(7000);
  kony.automation.flexcontainer.click(["frmACHDashboard","TabPaneNew","TabBodyNew","segTemplates[0,0]","flxDropDown"]);
  await kony.automation.playback.wait(3000);
  kony.automation.button.click(["frmACHDashboard","TabPaneNew","TabBodyNew","segTemplates[0,0]","btnViewDetailsTr"]);
  await kony.automation.playback.wait(3000);
  kony.automation.textarea.enterText(["frmACHDashboard","flxPopup","trComments"],"NA");
  await kony.automation.playback.wait(3000);
  kony.automation.button.click(["frmACHDashboard","flxPopup","formActionsNew","btnNext"]);
  await kony.automation.playback.wait(3000);
	
});
