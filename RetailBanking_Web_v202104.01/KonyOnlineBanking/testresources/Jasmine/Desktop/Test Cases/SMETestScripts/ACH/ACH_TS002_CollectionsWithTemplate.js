it("ACH_TS002_TransactionWithPayments", async function() {
  
  await kony.automation.playback.wait(3000);
  kony.automation.button.click(["frmACHDashboard","dbRightContainerNew","btnAction1"]);

//Click On Create an ACH Template with Collections

  await kony.automation.playback.wait(3000);
  await kony.automation.playback.waitFor(["frmACHDashboard","tbxTemplateName"]);
  kony.automation.textbox.enterText(["frmACHDashboard","tbxTemplateName"],"Test");
  await kony.automation.playback.waitFor(["frmACHDashboard","tbxTemplateDescription"]);
  kony.automation.textbox.enterText(["frmACHDashboard","tbxTemplateDescription"],"ABC");
  await kony.automation.playback.waitFor(["frmACHDashboard","lstbTemplateType"]);
  await kony.automation.playback.wait(3000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbTemplateType"], "1");
  await kony.automation.playback.wait(4000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbRequestType"], "1");
  await kony.automation.playback.wait(4000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbAccount"], "200922062603619");
  await kony.automation.playback.wait(4000);
  kony.automation.textbox.enterText(["frmACHDashboard","tbxMaxAmt"],"1");
  await kony.automation.playback.wait(1000);
  kony.automation.button.click(["frmACHDashboard","createFlowFormActionsNew","btnNext"]);
  await kony.automation.playback.wait(3000);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrName"],"Sample");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrAccountNumber"],"1234");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.listbox.selectItem(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","lstbxCrAccountType"], "1");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrTRCNumber"],"65677");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrDetailID"],"1234");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrAmount"],"1");
  await kony.automation.playback.wait(4000);
  kony.automation.button.click(["frmACHDashboard","CommonFormActionsNew","btnOption"]);
  await kony.automation.playback.wait(4000);
  kony.automation.button.click(["frmACHDashboard","CommonFormActionsNew","btnNext"]);
  await kony.automation.playback.wait(3000);
  
});
