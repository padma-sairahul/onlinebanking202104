it("ACHTransaction_TS006_WithOutTetmplatePayments", async function() {

  //expect(kony.automation.widget.getWidgetProperty(["frmACHDashboard","dbRightContainerNew","btnAction1"], "text")).toEqual("Create Transaction without a Template");
  // Click On The link Create Transaction without a Template  
  await kony.automation.playback.wait(3000);
  kony.automation.button.click(["frmACHDashboard","dbRightContainerNew","btnAction2"]);
  await kony.automation.playback.wait(3000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbTemplateType"], "2");
  await kony.automation.playback.wait(5000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbRequestType"], "2");
  await kony.automation.playback.wait(4000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbAccount"], "200922062603619");
  await kony.automation.playback.wait(3000);
  kony.automation.textbox.enterText(["frmACHDashboard","tbxMaxAmt"],"1");
  await kony.automation.playback.waitFor(["frmACHDashboard","calEffectiveDate"]);
  kony.automation.calendar.selectDate(["frmACHDashboard","calEffectiveDate"], [9,10,2021]);
  await kony.automation.playback.waitFor(["frmACHDashboard","createFlowFormActionsNew","btnNext"]);
  kony.automation.button.click(["frmACHDashboard","createFlowFormActionsNew","btnNext"]);
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrName"],"WithoutTemplateSample");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrAccountNumber"],"1234");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.listbox.selectItem(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","lstbxCrAccountType"], "1");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrTRCNumber"],"54666");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrDetailID"],"9897");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrAmount"],"1");
  //await kony.automation.playback.waitFor(["frmACHDashboard","CommonFormActionsNew","btnOption"]);
  await kony.automation.playback.wait(5000);
  kony.automation.button.click(["frmACHDashboard","CommonFormActionsNew","btnOption"]);
  await kony.automation.playback.wait(7000);
  kony.automation.button.click(["frmACHDashboard","CommonFormActionsNew","btnNext"]);
   await kony.automation.playback.wait(3000);

});