it("ACH_Ts0001_VerifyCreateTemplateFeature", async function() {



  // Create An ACH Template 

  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(2000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","ACHflxAccountsMenu"]);
  await kony.automation.playback.wait(2000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","ACH1flxMyAccounts"]);
  await kony.automation.playback.wait(3000);
  expect(kony.automation.widget.getWidgetProperty(["frmACHDashboard","dbRightContainerNew","btnAction1"], "text")).toEqual("Create an ACH Template");
  await kony.automation.playback.wait(3000);

});