it("ACH_Ts0003_TransactionWithCollection", async function() {


 // Click on the Create Template with Payments:
  await kony.automation.playback.wait(6000);
  kony.automation.button.click(["frmACHDashboard","dbRightContainerNew","btnAction1"]);
  await kony.automation.playback.waitFor(["frmACHDashboard","tbxTemplateName"]);
  kony.automation.textbox.enterText(["frmACHDashboard","tbxTemplateName"],"Collection");
  //await kony.automation.playback.waitFor(["frmACHDashboard","tbxTemplateDescription"]);
  await kony.automation.playback.wait(3000);
  kony.automation.textbox.enterText(["frmACHDashboard","tbxTemplateDescription"],"ABC");
  await kony.automation.playback.wait(3000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbTemplateType"], "2");
  await kony.automation.playback.wait(7000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbRequestType"], "2");
  await kony.automation.playback.wait(6000);
  kony.automation.listbox.selectItem(["frmACHDashboard","lstbAccount"], "200922062603619");
  await kony.automation.playback.waitFor(["frmACHDashboard","tbxMaxAmt"]);
  kony.automation.textbox.enterText(["frmACHDashboard","tbxMaxAmt"],"1");
  await kony.automation.playback.wait(1000);
  kony.automation.button.click(["frmACHDashboard","createFlowFormActionsNew","btnNext"]);
  await kony.automation.playback.wait(3000);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrName"],"CollectionSample");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrAccountNumber"],"1234");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.listbox.selectItem(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","lstbxCrAccountType"], "1");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrTRCNumber"],"65677");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrDetailID"],"1234");
  await kony.automation.playback.waitFor(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates"]);
  kony.automation.textbox.enterText(["frmACHDashboard","TemplateRecordsNew","TabBodyNew","segTemplates[0,0]","tbxCrAmount"],"1");
  await kony.automation.playback.waitFor(["frmACHDashboard","CommonFormActionsNew","btnOption"]);
  kony.automation.button.click(["frmACHDashboard","CommonFormActionsNew","btnOption"]);
  await kony.automation.playback.waitFor(["frmACHDashboard","CommonFormActionsNew","btnNext"]);
  kony.automation.button.click(["frmACHDashboard","CommonFormActionsNew","btnNext"]);
  await kony.automation.playback.wait(3000);

});