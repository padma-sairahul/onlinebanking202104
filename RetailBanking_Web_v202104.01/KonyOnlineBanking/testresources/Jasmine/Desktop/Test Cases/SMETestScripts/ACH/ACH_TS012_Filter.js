it("ACH_TS012_Filter", async function() {
  
    await kony.automation.playback.wait(3000);
    kony.automation.listbox.selectItem(["frmACHDashboard","TabPaneNew","TabSearchBarNew","listBoxViewType"], "Payment,transactionTypeName");
    await kony.automation.playback.wait(5000);
	kony.automation.listbox.selectItem(["frmACHDashboard","TabPaneNew","TabSearchBarNew","listBoxViewType"], "Collection,Pending,transactionTypeName,achtransaction.status");
	await kony.automation.playback.wait(5000);
	await kony.automation.playback.waitFor(["frmACHDashboard","customheader","topmenu","flxMenu"]);
	kony.automation.flexcontainer.click(["frmACHDashboard","customheader","topmenu","flxMenu"]);
	await kony.automation.playback.wait(2000);
	kony.automation.flexcontainer.click(["frmACHDashboard","customheader","customhamburger","ACCOUNTSflxAccountsMenu"]);
	await kony.automation.playback.wait(2000);
	await kony.automation.playback.waitFor(["frmACHDashboard","customheader","customhamburger","ACCOUNTS0flxMyAccounts"]);
	kony.automation.flexcontainer.click(["frmACHDashboard","customheader","customhamburger","ACCOUNTS0flxMyAccounts"]);
	await kony.automation.playback.wait(2000);
});