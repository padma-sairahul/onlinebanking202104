it("CustomRole_TS008_ManualCreationCustomRole", async function() {
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(2000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.wait(1000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.wait(2000);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagement3flxMyAccounts"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagement3flxMyAccounts"]);
  await kony.automation.playback.wait(1000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement3flxMyAccounts"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","flxManual"]);
  await kony.automation.playback.wait(10000);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","flxManual"]);


  var today = new Date();
  var date = today.getFullYear()+""+(today.getMonth()+1)+""+today.getDate();
  var time = today.getHours() + "" + today.getMinutes() + "" + today.getSeconds();


  kony.automation.textbox.enterText(["frmPermissionsTemplate","tbxRoleName"],"CROLEMANUAL"+date+time);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmPermissionsTemplate","btnCheckAvailability"]);
  await kony.automation.playback.wait(2000);
  kony.automation.widget.touch(["frmPermissionsTemplate"], null,null,[129,569]);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","segRoleNames[0]","flxSelectRole"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnProceedRoles"]);
  await kony.automation.scrollToWidget(["frmPermissionsTemplate","btnProceedRoles"]);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmPermissionsTemplate","btnProceedRoles"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate"]);
  kony.automation.widget.touch(["frmPermissionsTemplate"], null,null,[119,332]);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","segAccounts[1]","flxAccountRow","lblCheckAccount"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnProceedAccess"]);
  await kony.automation.scrollToWidget(["frmPermissionsTemplate","btnProceedAccess"]);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmPermissionsTemplate","btnProceedAccess"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnSaveAndProceed"]);
  await kony.automation.scrollToWidget(["frmPermissionsTemplate","btnSaveAndProceed"]);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmPermissionsTemplate","btnSaveAndProceed"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(4000);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(1000);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","customheader","customhamburger","ACCOUNTSflxAccountsMenu"]);
  await kony.automation.playback.wait(1000);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","customheader","customhamburger","ACCOUNTS0flxMyAccounts"]);
  await kony.automation.playback.wait(3000);
},60000);