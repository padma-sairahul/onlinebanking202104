it("CustomRole_TS001_DeleteCustomRole", async function() {

  //Scenario 4 : Custom Roles -Verify delete custom role functionality

  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  await kony.automation.playback.wait(2000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  //await kony.automation.playback.waitFor(["frmBBUsersDashboard","TabPane","TabBodyNew","segTemplates"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmBBUsersDashboard","TabPane","TabBodyNew","segTemplates"]);
  await kony.automation.playback.wait(5000);
  kony.automation.button.click(["frmBBUsersDashboard","TabPane","TabBodyNew","segTemplates[0,0]","btnViewPermissions"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnVerifyUserActionsCancel"]);
  await kony.automation.scrollToWidget(["frmPermissionsTemplate","btnVerifyUserActionsCancel"]);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmPermissionsTemplate","btnVerifyUserActionsCancel"]);
  await kony.automation.playback.wait(1000);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","PopupHeaderUM","btnYes"]);
  kony.automation.button.click(["frmPermissionsTemplate","PopupHeaderUM","btnYes"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
},45000);