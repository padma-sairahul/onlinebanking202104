it("CustomRole_TS006_SortCustomRole", async function() {


  //Scenario 6 :  Custome Roles - Verify sort functionality in custom roles list

  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.wait(2000);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  await kony.automation.playback.wait(1000);
  await kony.automation.playback.waitFor(["frmBBUsersDashboard","TabPane","TabBodyNew","flxRoleName"]);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","TabPane","TabBodyNew","flxRoleName"]);
  await kony.automation.playback.wait(3000);
  await kony.automation.playback.waitFor(["frmBBUsersDashboard","TabPane","TabBodyNew","flxRoleName"]);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","TabPane","TabBodyNew","flxRoleName"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.wait(5000);
},45000);