it("VerifyUpdatingNewPhoneno", async function() {
  
  var updatedPhonenum=Settings.phone.updatedPhonenum;
  
  await NavigateToProfileSettings();
  await selectProfileSettings_PhoneNumber();
  await ProfileSettings_UpdatePhoneNumber(updatedPhonenum);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);

//   await kony.automation.playback.wait(5000);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","lblPersonalDetailsHeading"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPersonalDetailsHeading"], "text")).toEqual("Personal Details");

//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxPhone"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxPhone"]);

//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewNumber"]);
//   kony.automation.button.click(["frmProfileManagement","settings","btnAddNewNumber"]);

//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddPhoneNumberHeading"], "text")).toEqual("Add Phone Number");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxAddPhoneNumberType"]);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"]);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddCheckBox3"]);

//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Work");
//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Home");
//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Other");


//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"]);
//   kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumber"],"888456789");

//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
//   kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
//   await kony.automation.playback.wait(5000);

//   // Update Number
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"]);
//   var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");

//   var segLength1=accounts_Size1.length;
//   for(var x = 0; x <segLength1; x++) {
//     var seg="segPhoneNumbers["+x+"]";
//     await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"flxProfileManagementPhoneNumbers","lblPhoneNumber"]);
//     var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"flxProfileManagementPhoneNumbers","lblPhoneNumber"], "text");
//     kony.print("Text is :: "+address1);
//     if(address1==="888456789"){
//       kony.automation.button.click(["frmProfileManagement","settings",seg,"btnViewDetail"]);
//       await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEdit"]);
//       kony.automation.button.click(["frmProfileManagement","settings","btnEdit"]);
//       await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxPhoneNumber"]);
//       kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxPhoneNumber"],"888456780");
//       await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
//       kony.automation.button.click(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
//       await kony.automation.playback.wait(5000);
//       break;
//     }
//   }

//   // Delete Number
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"]);
//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");

//   var segLength=accounts_Size.length;
//   for(var y = 0; y <segLength; y++) {
//     var seg1="segPhoneNumbers["+y+"]";
//     await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"flxProfileManagementPhoneNumbers","lblPhoneNumber"]);
//     var address=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"flxProfileManagementPhoneNumbers","lblPhoneNumber"], "text");
//     kony.print("Text is :: "+address);
//     if(address==="888456780"){
//       kony.automation.button.click(["frmProfileManagement","settings",seg1,"btnViewDetail"]);
//       await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnDelete"]);
//       kony.automation.button.click(["frmProfileManagement","settings","btnDelete"]);
//       await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"]);
//       kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
//       await kony.automation.playback.wait(5000);
//       break;
//     }
//   }

//   // Move back to base state
//   await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);