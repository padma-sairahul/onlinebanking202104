it("VerifyMakePrimaryPhoneno", async function() {
  
  var phoneNumber=Settings.phone.primaryPhoneNumber;
  var isPrimary='YES';
  
  await NavigateToProfileSettings();
  await selectProfileSettings_PhoneNumber();
  await ProfileSettings_VerifyaddNewPhoneNumberFunctionality(phoneNumber,isPrimary);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();
  
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);

//   await kony.automation.playback.wait(5000);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","lblPersonalDetailsHeading"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPersonalDetailsHeading"], "text")).toEqual("Personal Details");

//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxPhone"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxPhone"]);

//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewNumber"]);
//   kony.automation.button.click(["frmProfileManagement","settings","btnAddNewNumber"]);

//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddPhoneNumberHeading"], "text")).toEqual("Add Phone Number");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxAddPhoneNumberType"]);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"]);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddCheckBox3"]);

//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Work");
//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Home");
//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Other");


//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"]);
//   kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumber"],"1234567890");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddCheckBox3"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddCheckBox3"]);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
//   kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
//   await kony.automation.playback.wait(5000);

//   // Move back to base state
//   await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
},120000);