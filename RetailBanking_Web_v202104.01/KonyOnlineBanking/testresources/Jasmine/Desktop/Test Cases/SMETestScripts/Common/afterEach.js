afterEach(async function() {

  //await kony.automation.playback.wait(10000);
  appLog('Inside after Each function');

  if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
    appLog('Already in dashboard');
  }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
    appLog('Inside Login Screen');
  }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
    appLog('Moving back from frmAccountsDetails');
    kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmFastTransfers');
    kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmReview","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmReview');
    kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmConfirmTransfer');
    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmPastPaymentsNew","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmPastPaymentsNew');
    kony.automation.flexcontainer.click(["frmPastPaymentsNew","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsNew","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmScheduledPaymentsNew');
    kony.automation.flexcontainer.click(["frmScheduledPaymentsNew","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmDirectDebits","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmDirectDebits');
    kony.automation.flexcontainer.click(["frmDirectDebits","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmFastViewActivity","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmFastViewActivity');
    kony.automation.flexcontainer.click(["frmFastViewActivity","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmFastManagePayee');
    kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmFastP2P');
    kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmBulkPayees');
    kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmMakeOneTimePayee');
    kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmMakeOneTimePayment');
    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmOneTimePaymentConfirm');
    kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmOneTimePaymentAcknowledgement');
    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmManagePayees');
    kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmPayABill');
    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmPayBillConfirm');
    kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"])
  }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmPayBillAcknowledgement');
    kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmBillPayScheduled');
    kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmAddPayee1');
    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmAddPayeeInformation');
    kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmPayeeDetails');
    kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmVerifyPayee');
    kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmPayeeAcknowledgement');
    kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmBillPayHistory');
    kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmBillPayActivation');
    kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
    appLog('Moving back from frmBillPayActivationAcknowledgement');
    kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],5000)){
    appLog('Moving back from frmNotificationsAndMessages');
    kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],5000)){
    appLog('Moving back from frmOnlineHelp');
    kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],5000)){
    appLog('Moving back from frmContactUsPrivacyTandC');
    kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
    appLog('Moving back from frmProfileManagement');
    kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmMFATransactions","customheader","topmenu","flxaccounts"],5000)){
    appLog('***Moving back from frmMFATransactions****');
    kony.automation.flexcontainer.click(["frmMFATransactions","customheader","topmenu","flxaccounts"]);
  }else{
    appLog("Form name is not available");
  }



},240000);

//Before each with flags

// beforeEach(function() {

//   var flgDashboard =  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000);
//   appLog("Dashboard : "+flgDashboard);
//   if(flgDashboard){
//     appLog("Nothing to Do");
//   }
//   //Accounts Related
//   var flgAccountsDetails =  await kony.automation.playback.waitFor(["frmAccountsDetails"],5000);
//   appLog("flgAccountsDetails : "+flgAccountsDetails);
//   if(flgAccountsDetails){
//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
//   }

//   //Transfers Related
//   var flgFastTransfers =  await kony.automation.playback.waitFor(["frmFastTransfers"],5000);
//   appLog("FastTransfers : "+flgFastTransfers);
//   if(flgFastTransfers){
//     kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
//   }

//   var flgReview =  await kony.automation.playback.waitFor(["frmReview"],5000);
//   appLog("frmReview : "+flgReview);
//   if(flgReview){
//     kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
//   }
//   var flgConfirmTransfer =  await kony.automation.playback.waitFor(["frmConfirmTransfer"],5000);
//   appLog("frmConfirmTransfer : "+flgConfirmTransfer);
//   if(frmConfirmTransfer){
//     kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
//   }

//   var flgFastTransfersActivites =  await kony.automation.playback.waitFor(["frmFastTransfersActivites"],5000);
//   appLog("flgFastTransfersActivites : "+flgFastTransfersActivites);
//   if(flgFastTransfersActivites){
//     kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
//   }

//   //Manage payee Related
//   var flgFastManagePayee =  await kony.automation.playback.waitFor(["frmFastManagePayee"],5000);
//   appLog("flgFastManagePayee : "+flgFastManagePayee);
//   if(flgFastManagePayee){
//     kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   }

//   var flgFastP2P =  await kony.automation.playback.waitFor(["frmFastP2P"],5000);
//   appLog("flgFastP2P : "+flgFastP2P);
//   if(flgFastP2P){
//     kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
//   }

//   //BillPay Related

//   var flgBulkPayees =  await kony.automation.playback.waitFor(["frmBulkPayees"],5000);
//   appLog("flgBulkPayees : "+flgBulkPayees);
//   if(flgBulkPayees){
//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
//   } 

//   var flgMakeOneTimePayee =  await kony.automation.playback.waitFor(["frmMakeOneTimePayee"],5000);
//   appLog("flgMakeOneTimePayee : "+flgMakeOneTimePayee);
//   if(flgMakeOneTimePayee){
//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
//   }

//   var flgMakeOneTimePayment =  await kony.automation.playback.waitFor(["frmMakeOneTimePayment"],5000);
//   appLog("flgMakeOneTimePayment : "+flgMakeOneTimePayment);
//   if(flgMakeOneTimePayment){
//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
//   }

//   var flgOneTimePaymentConfirm =  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm"],5000);
//   appLog("flgOneTimePaymentConfirm : "+flgOneTimePaymentConfirm);
//   if(flgOneTimePaymentConfirm){
//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
//   }

//   var flgOneTimePaymentAcknowledgement =  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],5000);
//   appLog("flgOneTimePaymentAcknowledgement : "+flgOneTimePaymentAcknowledgement);
//   if(flgOneTimePaymentAcknowledgement){
//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
//   }

//   var flgManagePayees =  await kony.automation.playback.waitFor(["frmManagePayees"],5000);
//   appLog("flgManagePayees : "+flgManagePayees);
//   if(flgManagePayees){
//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
//   }

//   var flgPayABill =  await kony.automation.playback.waitFor(["frmPayABill"],5000);
//   appLog("flgPayABill : "+flgPayABill);
//   if(flgPayABill){
//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
//   }

//   var flgPayBillConfirm =  await kony.automation.playback.waitFor(["frmPayBillConfirm"],5000);
//   appLog("flgPayBillConfirm : "+flgPayBillConfirm);
//   if(flgPayBillConfirm){
//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
//   }

//   var flgPayBillAcknowledgement =  await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],5000);
//   appLog("flgPayBillAcknowledgement : "+flgPayBillAcknowledgement);
//   if(flgPayBillAcknowledgement){
//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//   }

//   var flgBillPayScheduled =  await kony.automation.playback.waitFor(["frmBillPayScheduled"],5000);
//   appLog("flgBillPayScheduled : "+flgBillPayScheduled);
//   if(flgBillPayScheduled){
//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
//   }

//   var flgAddPayee1 =  await kony.automation.playback.waitFor(["frmAddPayee1"],5000);
//   appLog("flgAddPayee1 : "+flgAddPayee1);
//   if(flgAddPayee1){
//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
//   }

//   var flgAddPayeeInformation =  await kony.automation.playback.waitFor(["frmAddPayeeInformation"],5000);
//   appLog("flgAddPayeeInformation : "+flgAddPayeeInformation);
//   if(flgAddPayeeInformation){
//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
//   }

//   var flgPayeeDetails =  await kony.automation.playback.waitFor(["frmPayeeDetails"],5000);
//   appLog("flgPayeeDetails : "+flgPayeeDetails);
//   if(flgPayeeDetails){
//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
//   }

//   var flgVerifyPayee =  await kony.automation.playback.waitFor(["frmVerifyPayee"],5000);
//   appLog("flgVerifyPayee : "+flgVerifyPayee);
//   if(flgVerifyPayee){
//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
//   }

//   var flgPayeeAcknowledgement =  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement"],5000);
//   appLog("flgPayeeAcknowledgement : "+flgPayeeAcknowledgement);
//   if(flgPayeeAcknowledgement){
//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
//   }

//   var flgBillPayHistory =  await kony.automation.playback.waitFor(["frmBillPayHistory"],5000);
//   appLog("flgBillPayHistory : "+flgBillPayHistory);
//   if(flgBillPayHistory){
//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
//   }

//   var flgBillPayActivation =  await kony.automation.playback.waitFor(["frmBillPayActivation"],5000);
//   appLog("flgBillPayActivation : "+flgBillPayActivation);
//   if(flgBillPayActivation){
//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
//   }


//   //Messages Related


//   var flgNotificationsAndMessages =  await kony.automation.playback.waitFor(["frmNotificationsAndMessages"],5000);
//   appLog("flgNotificationsAndMessages : "+flgNotificationsAndMessages);
//   if(flgNotificationsAndMessages){
//     kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
//   }

//   //Post Login Related

//   var flgOnlineHelp =  await kony.automation.playback.waitFor(["frmOnlineHelp"],5000);
//   appLog("flgOnlineHelp : "+flgOnlineHelp);
//   if(flgOnlineHelp){
//     kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
//   }


//   var flgContactUsPrivacyTandC =  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC"],5000);
//   appLog("flgContactUsPrivacyTandC : "+flgContactUsPrivacyTandC);
//   if(flgOnlineHelp){
//     kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
//   }

//   //Settings Related

//   var flgProfileManagement =  await kony.automation.playback.waitFor(["frmProfileManagement"],5000);
//   appLog("flgProfileManagement : "+flgProfileManagement);
//   if(flgProfileManagement){
//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
//   }



// });