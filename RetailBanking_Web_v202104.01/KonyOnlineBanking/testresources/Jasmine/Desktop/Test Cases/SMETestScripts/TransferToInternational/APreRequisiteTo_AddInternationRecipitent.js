it("PreRequisite_AddInternationRecipitent", async function() {
  
  var swiftCode=ManageRecipients.internationalAccount.swiftCode;
  var Accno="0"+new Date().getTime();
  var unique_RecipitentName=ManageRecipients.internationalAccount.unique_RecipitentName+getRandomString(5);
  
  await NavigateToManageRecipitents();
  await clickonAddInternationalAccounttab();
  await enterInternationalBankAccountDetails(swiftCode,Accno,unique_RecipitentName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();

},120000);