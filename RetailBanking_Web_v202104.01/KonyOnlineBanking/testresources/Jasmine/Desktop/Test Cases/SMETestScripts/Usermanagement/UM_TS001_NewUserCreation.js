it("UM_TS001_NewUserCreation", async function() {


  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(1000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","SettingsflxAccountsCollapse"]);
  await kony.automation.playback.wait(2000);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","SettingsflxAccountsCollapse"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","SettingsflxAccountsCollapse"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","Settings0flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","Settings0flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxUsernameAndPassword"]);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxUsernameAndPassword"]);
  await kony.automation.playback.wait(5000);
  var userName = kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblUsernameValue"], "text");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);




  //Scenario 1: UserManagement - Verify the Create user functionality from SME Banking application

  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement2flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  kony.automation.textbox.enterText(["frmUserManagement","tbxName"],"New");
  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","tbLastName"],"User");

  var today = new Date();
  var date = today.getFullYear()+""+(today.getMonth()+1)+""+today.getDate();
  var time = today.getHours() + "" + today.getMinutes() + "" + today.getSeconds();


  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","CustomDate","tbxDateInputKA"],"01/01/1980");
  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","tbxEmail"],"temenos@temenos.com");
  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","tbxPhoneNum"],"1234567890");
  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","tbxSSN"],date+time);
  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","tbxDriversLicense"],time);
  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","tbxUsername"],"USER@"+date+time);
  await kony.automation.playback.wait(2000);
  await kony.automation.scrollToWidget(["frmUserManagement","btnCheckAvailability"]);
  kony.automation.button.click(["frmUserManagement","btnCheckAvailability"]);
  await kony.automation.playback.wait(5000);
  kony.automation.button.click(["frmUserManagement","btnProceedCreate"]);
  await kony.automation.playback.wait(10000);
  kony.automation.textbox.enterText(["frmUserManagement","searchRoleTemplate","tbxSearch"],userName);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmUserManagement","segUserNames[0]","flxSelectRole"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmUserManagement","btnProceedRoles"]);
  await kony.automation.scrollToWidget(["frmUserManagement","btnProceedRoles"]);
  await kony.automation.playback.wait(5000);
  kony.automation.button.click(["frmUserManagement","btnProceedRoles"]);
  await kony.automation.playback.waitFor(["frmUserManagement","btnSaveAndProceed"]);
  await kony.automation.scrollToWidget(["frmUserManagement","btnSaveAndProceed"]);
  await kony.automation.playback.wait(8000);
  kony.automation.button.click(["frmUserManagement","btnSaveAndProceed"]);
  await kony.automation.playback.waitFor(["frmUserManagement","customheader","topmenu","flxMenu"]);
  kony.automation.flexcontainer.click(["frmUserManagement","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmUserManagement","customheader","customhamburger","UserManagement0flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmUserManagement","customheader","customhamburger","UserManagement0flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","customheader","topmenu","flxaccounts"]);
},250000);