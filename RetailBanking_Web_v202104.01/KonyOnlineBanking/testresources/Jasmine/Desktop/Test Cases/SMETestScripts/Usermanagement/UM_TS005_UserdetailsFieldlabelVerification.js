it("UM_TS005_UserdetailsFieldlabelVerification", async function() {
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement2flxMyAccounts"]);

  await kony.automation.playback.waitFor(["frmUserManagement","lbFirstName"]);
  await kony.automation.playback.wait(5000);
  expect(kony.automation.widget.getWidgetProperty(["frmUserManagement","lbFirstName"], "text")).toEqual("First Name");
  expect(kony.automation.widget.getWidgetProperty(["frmUserManagement","lblMiddleName"], "text")).toEqual("Middle Name");
  expect(kony.automation.widget.getWidgetProperty(["frmUserManagement","lblLastName"], "text")).toEqual("Last Name");
  expect(kony.automation.widget.getWidgetProperty(["frmUserManagement","lblDOB"], "text")).toEqual("Date of Birth");
  expect(kony.automation.widget.getWidgetProperty(["frmUserManagement","lblEmail"], "text")).toEqual("Email");
  expect(kony.automation.widget.getWidgetProperty(["frmUserManagement","lblPhoneNum"], "text")).toEqual("Registered Phone Number");
  expect(kony.automation.widget.getWidgetProperty(["frmUserManagement","lblSSN"], "text")).toEqual("Social Security Number");
  expect(kony.automation.widget.getWidgetProperty(["frmUserManagement","lblUsername"], "text")).toEqual("Username");
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmUserManagement","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(2000);
  kony.automation.flexcontainer.click(["frmUserManagement","customheader","customhamburger","ACCOUNTSflxAccountsMenu"]);
  await kony.automation.playback.wait(2000);
  kony.automation.flexcontainer.click(["frmUserManagement","customheader","customhamburger","ACCOUNTS0flxMyAccounts"]);
  await kony.automation.playback.wait(2000);
},25000);