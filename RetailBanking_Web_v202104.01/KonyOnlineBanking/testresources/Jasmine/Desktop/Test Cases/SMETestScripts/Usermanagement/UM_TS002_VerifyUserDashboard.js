it("UM_TS002_VerifyUserDashboard", async function() {

  //Scenario :2  - UserManagement - Verify the View all user dashboard

  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(2000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement0flxMyAccounts"]);
  await kony.automation.playback.wait(10000);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","customheader","topmenu","flxaccounts"]); 
},25000);
