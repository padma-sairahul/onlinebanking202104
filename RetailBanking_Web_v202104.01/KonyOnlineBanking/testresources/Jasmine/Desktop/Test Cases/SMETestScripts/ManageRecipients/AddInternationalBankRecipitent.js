it("AddInternationalBankRecipitent", async function() {
  
  // Add a recipitent and Then delete the same recipitent
  
  var swiftCode=ManageRecipients.internationalAccount.swiftCode;
  var Accno="0"+new Date().getTime();
  var unique_RecipitentName=ManageRecipients.internationalAccount.unique_RecipitentName+getRandomString(5);
  
  await NavigateToManageRecipitents();
  await clickonAddInternationalAccounttab();
  await enterInternationalBankAccountDetails(swiftCode,Accno,unique_RecipitentName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
  //Delete Added Recipitent
  
//   await NavigateToManageRecipitents();
//   await clickOnExternalRecipitentsTab();
//   await SearchforPayee_External(unique_RecipitentName);
//   await DeleteReciptent();
//   await MoveBacktoDashboard_ManageRecipitent();
  
//   var unique_RecipitentName="TestInternational_"+new Date().getTime();
  
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransferMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransferMoney"]);

//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmFastTransfers","flxAddInternationalAccount"]);
//   kony.automation.flexcontainer.click(["frmFastTransfers","flxAddInternationalAccount"]);

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxSWIFTCodeKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxSWIFTCodeKA"],"BOFAUS3N");

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxAccountNumberKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxAccountNumberKA"],"1234567890");

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxAccountNumberAgainKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxAccountNumberAgainKA"],"1234567890");

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxBeneficiaryNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxBeneficiaryNameKA"],unique_RecipitentName);

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxAccountNickNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxAccountNickNameKA"],unique_RecipitentName);

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","btnAddAccountKA"]);
//   kony.automation.button.click(["frmFastAddInternationalAccount","btnAddAccountKA"]);

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccountConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmFastAddInternationalAccountConfirm","btnConfirm"]);

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccountAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastAddInternationalAccountAcknowledgement","customheadernew","flxAccounts"]);

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
//    //Delete International Bank Recipitent
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
//   await kony.automation.playback.wait(5000);
  
//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
  
//   if(error){
//     fail("There was a technical delay. Please try again.");
//   }
//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
},120000);