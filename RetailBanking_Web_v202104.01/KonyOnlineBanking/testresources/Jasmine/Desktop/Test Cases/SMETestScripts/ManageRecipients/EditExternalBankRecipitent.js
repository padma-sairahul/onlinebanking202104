it("EditExternalBankRecipitent", async function() {

  // Add a recipitent and Then Edit the same recipitent
  
   // Add a recipitent and Then delete the same recipitent
  
  var Routingno=ManageRecipients.externalAccount.Routingno;
  var Accno="0"+new Date().getTime();
  var unique_RecipitentName=ManageRecipients.externalAccount.unique_RecipitentName+getRandomString(5);
  
  //var AccType="External";
  var unique_EditRecipitentName=ManageRecipients.externalAccount.unique_EditRecipitentName+getRandomString(5);
  var unique_EditNickName=ManageRecipients.externalAccount.unique_EditNickName+getRandomString(5);
  
  await NavigateToManageRecipitents();
  await clickonAddExternalAccounttab();
  await enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
  //Edit Added Recipitent
  
  await NavigateToManageRecipitents();
  await clickOnExternalRecipitentsTab();
  await SearchforPayee_External(unique_RecipitentName);
  await EditReciptent(unique_EditRecipitentName,unique_EditNickName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
//   var unique_RecipitentName="TestExtAccEdit_"+new Date().getTime();
  
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddKonyAccount"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddKonyAccount"]);
//   await kony.automation.playback.wait(5000);
  
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxRoutingNumberKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxRoutingNumberKA"],"1234567890");
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNumberKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNumberKA"],"1234567890");
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNumberAgainKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNumberAgainKA"],"1234567890");
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxBeneficiaryNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxBeneficiaryNameKA"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNickNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNickNameKA"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","btnAddAccountKA"]);
//   kony.automation.button.click(["frmFastAddExternalAccount","btnAddAccountKA"]);
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmFastAddExternalAccountConfirm","btnConfirm"]);

//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountAcknowledgement","lblSuccessMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddExternalAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");

//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastAddExternalAccountAcknowledgement","customheadernew","flxAccounts"]);

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
//   // Edit the added Recipitent
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnEdit"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","lblTransfers"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddExternalAccount","lblTransfers"],"text")).toContain("Edit");
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNickName"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNickName"],"Auto Updated");
  
//   //Having intermittent issue in Save button
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","btnSave"]);
//   kony.automation.button.click(["frmFastAddExternalAccount","btnSave"]);
  
//   var successMsg=await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],10000);
//   if(!successMsg){
//     // Move back to base state
//     await kony.automation.playback.waitFor(["frmFastAddExternalAccount","customheadernew","flxAccounts"]);
// 	kony.automation.flexcontainer.click(["frmFastAddExternalAccount","customheadernew","flxAccounts"]);
//   }else{
//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been successfully edited");
//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
//   }

//  // Delete the recipitent to clean list

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
//   await kony.automation.playback.wait(5000);
  
//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
  
//   if(error){
//     fail("There was a technical delay. Please try again.");
//   }

//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
},120000);