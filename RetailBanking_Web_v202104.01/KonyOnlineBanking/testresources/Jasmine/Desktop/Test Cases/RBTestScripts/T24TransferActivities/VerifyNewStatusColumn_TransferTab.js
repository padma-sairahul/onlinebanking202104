it("VerifyNewStatusColumn_TransferTab", async function() {
  
  await navigateToManageTranscations();
  await VerifyNewStatusColumn_AllTransfers();
  await MoveBackFrom_PastTransferActivities();
  
},TimeOuts.TrasferActivities.ActivitiesList);