it("CancelFutureDatedPayment", async function() {
  
  await navigateToManageTranscations();
  await selectSheduledTransfers();
  await clickOnCancelTransferButton(SearchTransferActivities.Name);
  
},TimeOuts.TrasferActivities.PaymentActivity);