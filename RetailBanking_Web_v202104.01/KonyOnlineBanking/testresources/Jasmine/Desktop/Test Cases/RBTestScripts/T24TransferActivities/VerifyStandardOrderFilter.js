it("VerifyStandardOrderFilter", async function() {
  
  await navigateToManageTranscations();
  await ClickonRecurringTab();
  await VerifyStandardOrdersList();
  await MoveBackFrom_SheduledTransferActivities();
  
},TimeOuts.TrasferActivities.ActivitiesList);