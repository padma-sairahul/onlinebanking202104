it("AddRecipient_DomesticAccount_TS03-TC01", async function() {


  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.scrollToWidget(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"]);

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","lblAddAccountHeading"], "text")).toEqual("Wire Transfer");
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","lblStep1"], "text")).toEqual("Step 1: Recipient Details");

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"],"WireTranx_Domestic_Acc");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","flxNameAndType"]);
  kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","flxNameAndType"]);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"],"Auto_Domestic_Acc");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxAddressLine1"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxAddressLine1"],"AutoAddress1");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxAddressLine2"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxAddressLine2"],"AutoAdress2");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxCity"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxCity"],"Hyd");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxZipcode"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxZipcode"],"500055");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","btnProceed"]);
  kony.automation.button.click(["frmWireTransferAddKonyAccountStep1","btnProceed"]);

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep2","lblAddAccountHeading"], "text")).toEqual("Add Recipient");
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep2","lblStep2"], "text")).toEqual("Step 2 - Recipient Account Details");

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxSwiftCode"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxSwiftCode"],"1234567890");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxAccountNumber"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxAccountNumber"],"1234567890");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxReAccountNumber"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxReAccountNumber"],"1234567890");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxNickName"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxNickName"],"Auto_Domestic");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankName"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankName"],"BOA");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine1"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine1"],"AutoAddress1");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine2"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine2"],"AutoAddress2");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankCity"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankCity"],"Hyd");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankZipcode"]);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankZipcode"],"500000");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"]);
  kony.automation.button.click(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"]);


  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountConfirm","lblRecipientDetails"], "text")).toEqual("Recipient Details");
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountConfirm","lblBankDetails"], "text")).toEqual("Bank Details");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","btnConfirm"]);
  kony.automation.button.click(["frmWireTransferAddKonyAccountConfirm","btnConfirm"]);

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountAck","lblAddAccountHeading"], "text")).toEqual("Add Recipient - Acknowledgment");
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountAck","lblSuccessAcknowledgement"], "text")).toContain("added succesfully!");

  // Move back to base state
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},90000);