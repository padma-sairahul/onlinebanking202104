it("WireTransfer_DomesticAccount_TS05-TC01", async function() {

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"]);
  await kony.automation.playback.wait(10000);

  await kony.automation.playback.waitFor(["frmWireTransfersWindow","Search","txtSearch"]);
  kony.automation.textbox.enterText(["frmWireTransfersWindow","Search","txtSearch"],"Domestic");
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmWireTransfersWindow","Search","lblSearch"]);
  kony.automation.flexcontainer.click(["frmWireTransfersWindow","Search","lblSearch"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmWireTransfersWindow","segWireTransfers"]);
  kony.automation.flexcontainer.click(["frmWireTransfersWindow","segWireTransfers[0]","flxDropdown"]);
  await kony.automation.playback.waitFor(["frmWireTransfersWindow","segWireTransfers"]);
  kony.automation.button.click(["frmWireTransfersWindow","segWireTransfers[0]","btnAction"]);
  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","tbxAmount"]);
  kony.automation.textbox.enterText(["frmWireTransferMakeTransfer","tbxAmount"],"1");
  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","flxAmount"]);
  kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","flxAmount"]);
  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","tbxNotes"]);
  kony.automation.textbox.enterText(["frmWireTransferMakeTransfer","tbxNotes"],"Test Domestic Wire Tranx");
  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","btnStepContinue"]);
  kony.automation.button.click(["frmWireTransferMakeTransfer","btnStepContinue"]);


  await kony.automation.playback.waitFor(["frmConfirmDetails","lblAddAccountHeading"]);
  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblAddAccountHeading"], "text")).toEqual("Confirm Money Transfer");
  await kony.automation.playback.waitFor(["frmConfirmDetails","lblConfirmHeaderMain"]);
  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblConfirmHeaderMain"], "text")).toEqual("Confirm Details");
  await kony.automation.playback.waitFor(["frmConfirmDetails","lblRecipientDetails"]);
  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblRecipientDetails"], "text")).toEqual("Recipient Details");

  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"]);
  await kony.automation.scrollToWidget(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"]);
  kony.automation.flexcontainer.click(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"]);

  await kony.automation.playback.waitFor(["frmConfirmDetails","btnConfirm"]);
  kony.automation.button.click(["frmConfirmDetails","btnConfirm"]);

  await kony.automation.playback.waitFor(["frmConfirmDetails","lblSuccessAcknowledgement"]);
  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblSuccessAcknowledgement"], "text")).toEqual("Success! Your transaction has been completed");


  // Move back to base state
  await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
},90000);