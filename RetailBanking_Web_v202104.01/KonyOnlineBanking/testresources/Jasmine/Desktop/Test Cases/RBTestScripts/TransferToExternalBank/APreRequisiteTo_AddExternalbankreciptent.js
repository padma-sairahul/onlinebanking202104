it("AddExternalbankreciptent", async function() {
  
  var Routingno=ManageRecipients.externalAccount.Routingno;
  var Accno="0"+new Date().getTime();
  var unique_RecipitentName=ManageRecipients.externalAccount.unique_RecipitentName+getRandomString(5);
  
  await NavigateToManageRecipitents();
  await clickonAddExternalAccounttab();
  await enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
  
  
},120000);