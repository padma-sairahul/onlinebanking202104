it("EditDuplicateViewName", async function() {
  
  var viewName="Edit"+CustomViewDetails.ViewName+getRandomString(5);
  var AccountName=CustomViewDetails.AccountName;
  
  // Create a View
  await ClickonCustomviewDropdown();
  await ClickonEditButton();
  await EnterCustomViewName(viewName);
  //await SearchAccounts_forView(AccountName);
  //await SelectAccounts_forView();
  await ClickonCreateButton();
  await VerifySelectedViewName_onDashBoard(viewName);
  
  // Create same view -Duplicate
  await ClickonCustomviewDropdown();
  await ClickonAddNewFlex();
  await EnterCustomViewName(viewName);
  await VerifyDuplicateViewNameError();
  
},TimeOuts.CustomView.Timeout);