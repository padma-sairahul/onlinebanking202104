it("VerifyCheckUncheckAccounts", async function() {
  
  var viewName=CustomViewDetails.ViewName+getRandomString(5);
  var AccountName=CustomViewDetails.AccountName;
      
  await ClickonCustomviewDropdown();
  await ClickonAddNewFlex();
  await EnterCustomViewName(viewName);
  await SearchAccounts_forView(AccountName);
  await SelectAccounts_forView();
  await MoveBackfrom_CustomView();
  
},TimeOuts.CustomView.Timeout);