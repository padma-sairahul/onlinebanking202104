it("VerifySearchFunctionality", async function() {
  
  var viewName=CustomViewDetails.ViewName+getRandomString(5);
  var AccountName=CustomViewDetails.AccountName;
      
  await ClickonCustomviewDropdown();
  await ClickonAddNewFlex();
  await EnterCustomViewName(viewName);
  await SearchAccounts_forView(AccountName);
  await MoveBackfrom_CustomView();
  
},TimeOuts.CustomView.Timeout);