it("EditVerifySearchFunctionality", async function() {
  
  var viewName="Edit"+CustomViewDetails.ViewName+getRandomString(5);
  var AccountName=CustomViewDetails.AccountName;
      
  await ClickonCustomviewDropdown();
  await ClickonEditButton();
  await EnterCustomViewName(viewName);
  await SearchAccounts_forView(AccountName);
  await MoveBackfrom_CustomView();
  
},TimeOuts.CustomView.Timeout);