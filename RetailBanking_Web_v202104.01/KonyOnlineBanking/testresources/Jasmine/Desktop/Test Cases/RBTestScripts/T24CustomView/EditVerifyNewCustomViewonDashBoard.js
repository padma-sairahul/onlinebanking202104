it("EditVerifyNewCustomViewonDashBoard", async function() {
  
  var viewName=CustomViewDetails.ViewName+getRandomString(5);
  var AccountName=CustomViewDetails.AccountName;
      
  await ClickonCustomviewDropdown();
  await ClickonEditButton();
  await EnterCustomViewName(viewName);
  await SearchAccounts_forView(AccountName);
  await SelectAccounts_forView();
  await ClickonCreateButton();
  
  await VerifySelectedViewName_onDashBoard(viewName);
  
},TimeOuts.CustomView.Timeout);