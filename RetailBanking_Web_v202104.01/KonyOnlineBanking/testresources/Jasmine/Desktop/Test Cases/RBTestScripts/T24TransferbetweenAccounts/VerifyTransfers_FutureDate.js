it("VerifyTransfers_FutureDate", async function() {
  
  await navigateToTransfers();
  await SelectFromAccount(Payments.OwnAcc.FromAcc);
  await SelectOwnTransferToAccount(Payments.OwnAcc.ToAcc);
  await EnterAmount(Payments.OwnAcc.Amount);
  await SelectSendOnDate();
  await EnterNoteValue("VerifyTransfers_FutureDate");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.OwnPayments.Payment);