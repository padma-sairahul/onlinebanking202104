it("CancelRecurringTransfer", async function() {
  
  await navigateToManageTranscations();
  await ClickonRecurringTab();
  await selectActiveOrders();
  await clickOnCancelSeriesButton(AllAccounts.Saving.accType);
  
},TimeOuts.TrasferActivities.PaymentActivity);