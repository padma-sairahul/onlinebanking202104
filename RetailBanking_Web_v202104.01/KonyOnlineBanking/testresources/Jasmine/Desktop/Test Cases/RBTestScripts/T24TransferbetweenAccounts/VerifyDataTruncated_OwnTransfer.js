it("VerifyDataTruncated_OwnTransfer", async function() {
  
  await navigateToTransfers();
  await SelectFromAccount(Payments.OwnAcc.FromAcc);
  await SelectOwnTransferToAccount(Payments.OwnAcc.ToAcc);
  await EnterAmount(Payments.OwnAcc.Amount);
  await EnterNoteValue("VerifyDataTruncated_OwnTransfer");
  await ConfirmTransfer();
  await verifyDataCutOff_Ackform();
  
},TimeOuts.OwnPayments.Payment);