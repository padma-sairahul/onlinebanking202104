it("VerifyContextualMenuTransfer", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
  await selectContextMenuOption(AllAccounts.Current.MenuOptions[0].option);
  await VerifyTransfersScreen();
  
  //Do Transfer
  //await ReSelectFromAccount(Payments.OwnAcc.FromAcc);
  
  await SelectOwnTransferToAccount(Payments.OwnAcc.ToAcc);
  await EnterAmount(Payments.OwnAcc.Amount);
  await EnterNoteValue("VerifyContextualMenuTransfer");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.OwnPayments.Payment);