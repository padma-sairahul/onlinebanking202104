it("HalfYearlySheduledTransfer-Occurences", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("Domestic");
  await SelectUTFFromAccount(UTFPayments.Domestic.FromAcc);
  await SelectUTFToAccount(UTFPayments.Domestic.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.Domestic.Amount);
  await SelectUTFFrequency("HalfYearly");
  await SelectUTFSendOnDate();
  await SelectUTFOccurences();
  await EnterUTFNoteValue("Domestic-HalfYearlySheduledTransfer-Occurences");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
 },TimeOuts.UnifiedTransfers.Transfers);