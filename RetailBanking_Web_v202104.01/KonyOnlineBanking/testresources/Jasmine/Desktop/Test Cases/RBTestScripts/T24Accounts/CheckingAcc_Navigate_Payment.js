it("CheckingAcc_Navigate_Payment", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
  await selectContextMenuOption(AllAccounts.Current.MenuOptions[1].option);
  await VerifyPaymentsScreen();
  await MoveBackToLandingScreen_Transfers();
  
},TimeOuts.Accounts.Timeout);