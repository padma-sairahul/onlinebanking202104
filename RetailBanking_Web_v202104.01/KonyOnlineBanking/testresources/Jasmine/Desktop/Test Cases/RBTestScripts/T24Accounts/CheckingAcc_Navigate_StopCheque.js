it("CheckingAcc_Navigate_StopCheque", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
  await selectContextMenuOption(AllAccounts.Current.MenuOptions[6].option);
  await VerifyStopChequePaymentScreen();
  await MoveBack_Accounts_StopChequeBook();
  
},TimeOuts.Accounts.Timeout);