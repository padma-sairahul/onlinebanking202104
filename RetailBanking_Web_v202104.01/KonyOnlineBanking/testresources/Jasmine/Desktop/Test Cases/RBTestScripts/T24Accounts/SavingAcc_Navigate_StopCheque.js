it("SavingAcc_Navigate_StopCheque", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[4].option);
  await VerifyStopChequePaymentScreen();
  await MoveBack_Accounts_StopChequeBook();
  
},TimeOuts.Accounts.Timeout);