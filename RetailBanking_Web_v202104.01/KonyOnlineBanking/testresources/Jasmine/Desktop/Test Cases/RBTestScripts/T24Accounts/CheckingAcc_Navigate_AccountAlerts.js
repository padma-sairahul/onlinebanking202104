it("CheckingAcc_Navigate_AccountAlerts", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
  await selectContextMenuOption(AllAccounts.Current.MenuOptions[9].option);
  await VerifyAccountAlertScreen();
  await MoveBack_Accounts_ProfileManagement();
  
},TimeOuts.Accounts.Timeout);