it("SavingAcc_Navigate_Payment", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[1].option);
  await VerifyPaymentsScreen();
  await MoveBackToLandingScreen_Transfers();
  
},TimeOuts.Accounts.Timeout);