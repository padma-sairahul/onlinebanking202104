it("CheckingAcc_Navigate_Transfer", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
  await selectContextMenuOption(AllAccounts.Current.MenuOptions[0].option);
  await VerifyTransfersScreen();
  await MoveBackToLandingScreen_Transfers();
  
},TimeOuts.Accounts.Timeout);