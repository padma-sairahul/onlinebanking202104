it("PayBills_DeletePayee", async function() {
  
  // Add payee and Then Delete same payee
  var delete_RecipitentName=BillPay.deletePayee.delete_RecipitentName+getRandomString(5);
  var unique_Accnumber="0"+new Date().getTime();

  await navigateToBillPay();
  await clickOnAddPayeeLink();
  await enterPayeeDetails_UsingPayeeinfo(delete_RecipitentName,BillPay.deletePayee.address1,BillPay.deletePayee.address2,BillPay.deletePayee.city,BillPay.deletePayee.zipcode,unique_Accnumber,"PayBills_DeletePayee");
  await clickOnNextButton_payeeDetails();
  await clickOnConfirmButton_verifyPayee();
  await verifyAddPayeeSuccessMsg();
  await verifyAccountsLandingScreen();
  
   //Delete same payee
  await navigateToManagePayee();
  await selectPayee_ManagePayeeList(delete_RecipitentName);
  await deletePayee_ManagePayee();

//    var unique_RecipitentName="Test Automation_"+new Date().getTime();
//   // Add payee and Then Delete same payee
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   await kony.automation.playback.waitFor(["frmBulkPayees","flxdeletePayee"]);
//   kony.automation.flexcontainer.click(["frmBulkPayees","flxdeletePayee"]);
//   await kony.automation.playback.waitFor(["frmdeletePayee1","btnEnterPayeeInfo"]);
//   kony.automation.button.click(["frmdeletePayee1","btnEnterPayeeInfo"]);
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterName"]);
//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterName"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterAddress"]);
//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterAddress"],"LR PALLI");
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterAddressLine2"]);
//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterAddressLine2"],"ATMAKUR");
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxCity"]);
//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxCity"],"ATMAKUR");
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterZipCode"]);
//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterZipCode"],"500055");
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterAccountNmber"]);
//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterAccountNmber"],"1234567890");
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxConfirmAccNumber"]);
//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxConfirmAccNumber"],"1234567890");
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","flxClick"]);
//   kony.automation.flexcontainer.click(["frmdeletePayeeInformation","flxClick"]);
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxAdditionalNote"]);
//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxAdditionalNote"],"ADD PAYYE");
//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","btnReset"]);
//   kony.automation.button.click(["frmdeletePayeeInformation","btnReset"]);
//   await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
//   kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
//   await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
//   kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");

//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   //Delete same payee
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"Test");
//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDropdown"]);

//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnDeleteBiller"]);
//   await kony.automation.playback.waitFor(["frmManagePayees","DeletePopup","btnYes"]);
//   kony.automation.button.click(["frmManagePayees","DeletePopup","btnYes"]);
//   await kony.automation.playback.wait(5000);


//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);