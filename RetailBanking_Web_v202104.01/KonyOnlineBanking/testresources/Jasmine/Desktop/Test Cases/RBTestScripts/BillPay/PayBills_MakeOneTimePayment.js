it("PayBills_MakeOneTimePayment", async function() {

   await navigateToOneTimePayment();
   await enterOneTimePayeeInformation(BillPay.oneTimePay.payeeName,BillPay.oneTimePay.zipcode,BillPay.oneTimePay.accno,BillPay.oneTimePay.accnoAgain,BillPay.oneTimePay.mobileno);
   await enterOneTimePaymentdetails(BillPay.oneTimePay.amountValue,"PayBills_MakeOneTimePayment");
   await confirmOneTimePaymnet();
   await verifyOneTimePaymentSuccessMsg();
   await verifyAccountsLandingScreen();
  
  
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);

//   await kony.automation.playback.waitFor(["frmBulkPayees","flxMakeOneTimePayment"]);
//   kony.automation.flexcontainer.click(["frmBulkPayees","flxMakeOneTimePayment"]);

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],"A");
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"]);
//   kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],"500055");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],"1234567890");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],"1234567890");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],"1234567890");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"]);
//   kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],"2.1");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"]);
//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],"test OneTime payment");

//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"]);
//   kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);

//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
//   kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);

//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);
  
//   //expect(kony.automation.widget.getWidgetProperty(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],"text")).toEqual("Success! Your transaction has been completed");
  
//   await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
  
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);