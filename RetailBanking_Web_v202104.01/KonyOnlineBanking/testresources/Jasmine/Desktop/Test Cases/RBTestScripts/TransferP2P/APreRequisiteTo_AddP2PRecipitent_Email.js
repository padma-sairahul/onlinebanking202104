it("APreRequisiteTo_AddP2PRecipitent_Email", async function() {
  
 
  var unique_RecipitentName=ManageRecipients.p2pAccount.unique_RecipitentName+getRandomString(5);
  var email=ManageRecipients.p2pAccount.email;
  
  await NavigateToManageRecipitents();
  await clickonAddP2PAccounttab();
  await enterP2PAccountDetails_Email(unique_RecipitentName,email);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
  // Add a recipitent and Then delete the same recipitent
  
//   var unique_RecipitentName="TestP2PRecipitentEmail_"+new Date().getTime();
  
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddReciepient"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddReciepient"]);

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxRecipientNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxRecipientNameKA"],unique_RecipitentName);

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxAccountNickNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxAccountNickNameKA"],unique_RecipitentName);

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","flxNUORadioBtn2"]);
//   kony.automation.flexcontainer.click(["frmFastAddRecipient","flxNUORadioBtn2"]);

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxEmailAddressKA"]);
//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxEmailAddressKA"],"testauto@gmail.com");

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","btnAddAccountKA"]);
//   kony.automation.button.click(["frmFastAddRecipient","btnAddAccountKA"]);

//   await kony.automation.playback.waitFor(["frmFastAddRecipientConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmFastAddRecipientConfirm","btnConfirm"]);
//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");

//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
//   //Delete Same Bank Recipitent
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","Search1","txtSearch"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search1","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup1","lblPopupMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup1","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this recipient?");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup1","btnYes"]);
//   kony.automation.button.click(["frmFastManagePayee","CustomPopup1","btnYes"]);
//   await kony.automation.playback.wait(5000);
  
//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
  
//   if(error){
//     fail("There was a technical delay. Please try again.");
//     //expect(true).toBe(false, 'There was a technical delay. Please try again.');
//   }

//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);