it("SearchTransaction_ByTranxtype", async function() {
  
  await clickOnFirstAvailableAccount();
  await VerifyAdvancedSearch_byTranxType(SearchTranscation.TransactionType);
  await MoveBackToLandingScreen_AccDetails();
  
},120000);