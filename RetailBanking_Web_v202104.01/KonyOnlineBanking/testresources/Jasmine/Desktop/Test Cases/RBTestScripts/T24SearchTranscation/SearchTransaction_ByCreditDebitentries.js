it("SearchTransaction_ByCreditDebitentries", async function() {
  
  await clickOnFirstAvailableAccount();
  await VerifyAdvancedSearch_byTranxType(SearchTranscation.debitEntry);
  await MoveBackToLandingScreen_AccDetails();
  
},120000);