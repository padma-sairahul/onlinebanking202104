it("DbxLogin",async function() {

  //var userName='testuat52';
  var userName=LoginDetails.username;
  //var userName='adithyasai.kovuru';
  var passWord=LoginDetails.password;

   // Check for MayBeLater
  await verifyMayBeLater();
  
  await verifyLoginFunctionality(userName,passWord);
  
  //Check for terms and conditions
  await verifyTermsandConditions();
  
  await verifyAccountsLandingScreen();
  
},120000);
