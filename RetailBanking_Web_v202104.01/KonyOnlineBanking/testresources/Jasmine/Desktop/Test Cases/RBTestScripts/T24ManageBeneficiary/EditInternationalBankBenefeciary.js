it("EditInternationalBankBenefeciary", async function() {
  
  var BenefeciarySearch=ManageBeneficiary.International.SWIFT;
  var updateCity=ManageBeneficiary.International.updateCity;
    
  await NavigateToManageBeneficiary();
  await EditBeneficiaryFromList(BenefeciarySearch,updateCity);
  
},TimeOuts.ManageBenefeciary.EditBenefeciary);