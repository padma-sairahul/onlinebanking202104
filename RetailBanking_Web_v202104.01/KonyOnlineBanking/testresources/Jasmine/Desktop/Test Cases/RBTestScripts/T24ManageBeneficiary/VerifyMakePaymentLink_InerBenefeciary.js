it("VerifyMakePaymentLink_InerBenefeciary", async function() {

  //var AccountNumber=ManageBeneficiary.International.AccountNumber;
  var AccountNumber=getRandomNumber(8);
  var SwiftCode=ManageBeneficiary.International.SWIFT;
  var BeneficiaryName=ManageBeneficiary.International.BeneficiaryName;
  var Nickname=ManageBeneficiary.International.Nickname;
  var Address1=ManageBeneficiary.International.Address1;
  var Address2=ManageBeneficiary.International.Address2;
  var city=ManageBeneficiary.International.city;
  var zipcode=ManageBeneficiary.International.zipcode;

  await NavigateToManageBeneficiary();
  await clickonAddNewBeneficiaryBtn();
  await enterInternationalBeneficiaryDetails(AccountNumber,SwiftCode,BeneficiaryName,Nickname,Address1,Address2,city,zipcode);
  await SubmitBankBeneficiaryDetails();

  await clickOnMakePaymentLink_Ackform();
  //do Payment from ack screen
  await SelectFromAccount(Payments.International.FromAcc);
  await EnterAmount(Payments.International.Amount);
  await EnterNoteValue("VerifyMakePaymentLink_InerBenefeciary");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();



},TimeOuts.ManageBenefeciary.PaymentBenefeciary);