it("EditDomesticBenefeciary", async function() {
  
  var BenefeciarySearch=ManageBeneficiary.Domestic.IBANList[0].IBAN;
  var updateCity=ManageBeneficiary.Domestic.updateCity;
    
  await NavigateToManageBeneficiary();
  await EditBeneficiaryFromList(BenefeciarySearch,updateCity);
  
},TimeOuts.ManageBenefeciary.EditBenefeciary);