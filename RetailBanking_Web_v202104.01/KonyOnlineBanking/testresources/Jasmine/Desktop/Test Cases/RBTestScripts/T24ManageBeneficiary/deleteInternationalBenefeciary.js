it("deleteInternationalBenefeciary", async function() {
  
  var BenefeciarySearch=ManageBeneficiary.International.SWIFT;
  
  await NavigateToManageBeneficiary();
  await DeleteBeneficiaryFromList(BenefeciarySearch);
  
},TimeOuts.ManageBenefeciary.DeleteBenefeciary);