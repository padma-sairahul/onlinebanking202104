it("deleteSameBankBenefeciary", async function() {
  
  var BenefeciarySearch=ManageBeneficiary.SameBank.AccountNumberList[0].accno;
  
  await NavigateToManageBeneficiary();
  await DeleteBeneficiaryFromList(BenefeciarySearch);
  
},TimeOuts.ManageBenefeciary.DeleteBenefeciary);