it("deleteDomesticBenefeciary", async function() {
  
  var BenefeciarySearch=ManageBeneficiary.Domestic.IBANList[0].IBAN;
  
  await NavigateToManageBeneficiary();
  await DeleteBeneficiaryFromList(BenefeciarySearch);
  
},TimeOuts.ManageBenefeciary.DeleteBenefeciary);