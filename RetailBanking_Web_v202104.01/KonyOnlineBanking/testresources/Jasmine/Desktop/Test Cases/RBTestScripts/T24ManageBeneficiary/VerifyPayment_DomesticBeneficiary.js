it("VerifyPayment_DomesticBeneficiary", async function() {
  
  await NavigateToManageBeneficiary();
  await ClickonMakePaymentLink_BeneficiariesList(Payments.Domestic.ToAcc);
  await SelectFromAccount(Payments.Domestic.FromAcc);
  await EnterAmount(Payments.Domestic.Amount);
  await EnterNoteValue("VerifyPayment_DomesticBeneficiary");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.ManageBenefeciary.PaymentBenefeciary);