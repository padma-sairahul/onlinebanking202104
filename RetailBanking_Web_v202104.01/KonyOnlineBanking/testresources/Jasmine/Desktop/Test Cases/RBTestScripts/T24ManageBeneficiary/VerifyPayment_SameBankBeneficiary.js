it("VerifyPayment_SameBankBeneficiary", async function() {
  
  await NavigateToManageBeneficiary();
  await ClickonMakePaymentLink_BeneficiariesList(Payments.SameBank.ToAcc);
  await SelectFromAccount(Payments.SameBank.FromAcc);
  await EnterAmount(Payments.SameBank.Amount);
  await EnterNoteValue("VerifyPayment_SameBankBeneficiary");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.ManageBenefeciary.PaymentBenefeciary);