it("VerifyBankName_AddInternationalReview", async function() {

  //var AccountNumber=ManageBeneficiary.International.AccountNumber;
  var AccountNumber=getRandomNumber(8);
  var SwiftCode=ManageBeneficiary.International.SWIFT;
  var BeneficiaryName=ManageBeneficiary.International.BeneficiaryName;
  var Nickname=ManageBeneficiary.International.Nickname;
  var Address1=ManageBeneficiary.International.Address1;
  var Address2=ManageBeneficiary.International.Address2;
  var city=ManageBeneficiary.International.city;
  var zipcode=ManageBeneficiary.International.zipcode;

  await NavigateToManageBeneficiary();
  if(await isBenefeciaryAlreadyAdded(SwiftCode)){
    await MoveBackFrom_ManageBeneficiaries();
  }else{
    await clickonAddNewBeneficiaryBtn();
    await enterInternationalBeneficiaryDetails(AccountNumber,SwiftCode,BeneficiaryName,Nickname,Address1,Address2,city,zipcode);
    await VerifyNewelyAddedBenefeciaryDetails();
  }

},TimeOuts.ManageBenefeciary.AddBenefeciary);