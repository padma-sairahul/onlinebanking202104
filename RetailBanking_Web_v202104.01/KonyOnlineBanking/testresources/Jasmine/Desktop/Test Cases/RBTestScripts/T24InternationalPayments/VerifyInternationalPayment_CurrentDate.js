it("VerifyInternationalPayment_CurrentDate", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.International.FromAcc);
  await SelectToAccount(Payments.International.ToAcc);
  await EnterAmount(Payments.International.Amount);
  await selectFeePaidRadio();
  await EnterNoteValue("VerifyInternationalPayment_CurrentDate");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.InternationalPayments.Payment);