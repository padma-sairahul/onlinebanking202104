it("VerifyInternationalOneTimePayment_Currentdate", async function() {
  
  var AccountNumber=getRandomNumber(8);
  
  await navigateToMakePayments();
  await SelectFromAccount(OneTimePayment.International.FromAcc);
  await EnterNewToAccountName(OneTimePayment.International.ToAcc);
  await clickOnNewButton_OneTimePay();
  await selectOtherBankRadioBtn();
  await enterOneTimePaymentDetails_International(AccountNumber,OneTimePayment.International.SWIFT,OneTimePayment.International.Amount);
  await selectFeePaidRadio();
  await EnterNoteValue("VerifyInternationalOneTimePayment_Currentdate");
  await ConfirmTransfer();
  await VerifyOneTimePaymentSuccessMessage();
  
},TimeOuts.InternationalPayments.OneTimepay);