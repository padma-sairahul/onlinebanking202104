it("VerifyInternationalPayment_Occurences", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.International.FromAcc);
  await SelectToAccount(Payments.International.ToAcc);
  await EnterAmount(Payments.International.Amount);
  await SelectFrequency("Monthly");
  await SelectDateRange();
  await selectFeePaidRadio();
  await EnterNoteValue("VerifyInternationalPayment_Occurences");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.InternationalPayments.Payment);