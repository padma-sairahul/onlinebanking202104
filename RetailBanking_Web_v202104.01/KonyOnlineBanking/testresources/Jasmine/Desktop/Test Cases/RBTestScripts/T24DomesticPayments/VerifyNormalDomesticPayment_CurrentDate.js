it("VerifyNormalDomesticPayment_CurrentDate", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.Domestic.FromAcc);
  await SelectToAccount(Payments.Domestic.ToAcc);
  await EnterAmount(Payments.Domestic.Amount);
  await selectNormalPaymentRadio();
  await EnterNoteValue("VerifyNormalDomesticPayment_CurrentDate");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.DomesticPayments.Payment);