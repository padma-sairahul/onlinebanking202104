it("CancelDomesticRecurringPayment", async function() {
  
  await navigateToManageTranscations();
  await ClickonRecurringTab();
  await selectActiveOrders();
  await clickOnCancelSeriesButton(ManageBeneficiary.Domestic.BeneficiaryName);
  
},TimeOuts.TrasferActivities.PaymentActivity);