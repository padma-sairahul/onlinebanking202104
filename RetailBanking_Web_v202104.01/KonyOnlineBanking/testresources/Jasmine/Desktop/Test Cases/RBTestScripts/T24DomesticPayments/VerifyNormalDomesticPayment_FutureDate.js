it("VerifyNormalDomesticPayment_FutureDate", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.Domestic.FromAcc);
  await SelectToAccount(Payments.Domestic.ToAcc);
  await EnterAmount(Payments.Domestic.Amount);
  await SelectSendOnDate();
  await selectNormalPaymentRadio();
  await EnterNoteValue("VerifyNormalDomesticPayment_FutureDate");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.DomesticPayments.Payment);