it("VerifyDomesticInstaOneTimePayment_Currentdate", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(OneTimePayment.Domestic.FromAcc);
  await EnterNewToAccountName(OneTimePayment.Domestic.ToAcc);
  await clickOnNewButton_OneTimePay();
  await selectOtherBankRadioBtn();
  await enterOneTimePaymentDetails_SameBank(OneTimePayment.Domestic.IBANList[2].IBAN,OneTimePayment.Domestic.Amount);
  await EnterNoteValue("VerifyDomesticInstaOneTimePayment_Currentdate");
  await ConfirmTransfer();
  await VerifyOneTimePaymentSuccessMessage();
  
},TimeOuts.DomesticPayments.OneTimepay);