it("VerifyInstantDomesticPayment_CurrentDate", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.Domestic.FromAcc);
  await SelectToAccount(Payments.Domestic.ToAcc);
  await EnterAmount(Payments.Domestic.Amount);
  await EnterNoteValue("VerifyInstantDomesticPayment_CurrentDate");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.DomesticPayments.Payment);