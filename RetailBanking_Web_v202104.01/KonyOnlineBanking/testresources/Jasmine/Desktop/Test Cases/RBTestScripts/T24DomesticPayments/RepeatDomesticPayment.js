it("RepeatDomesticPayment", async function() {
  
  await navigateToManageTranscations();
  await selectCompletedTransfers();
  await clickOnRepeatButton(ManageBeneficiary.Domestic.BeneficiaryName);
  
},TimeOuts.TrasferActivities.PaymentActivity);