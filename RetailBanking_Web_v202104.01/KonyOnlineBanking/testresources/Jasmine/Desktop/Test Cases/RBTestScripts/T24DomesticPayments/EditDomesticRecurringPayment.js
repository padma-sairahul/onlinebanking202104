it("EditDomesticRecurringPayment", async function() {
  
  await navigateToManageTranscations();
  await ClickonRecurringTab();
  await selectActiveOrders();
  await clickOnEditButton(ManageBeneficiary.Domestic.BeneficiaryName);
  
},TimeOuts.TrasferActivities.PaymentActivity);