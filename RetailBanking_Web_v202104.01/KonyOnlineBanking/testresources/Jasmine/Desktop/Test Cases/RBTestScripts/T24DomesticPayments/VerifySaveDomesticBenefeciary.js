it("VerifySaveDomesticBenefeciary", async function() {
  
  var IBAN=OneTimePayment.Domestic.IBANList[3].IBAN;
  
  await NavigateToManageBeneficiary();
  if(await isBenefeciaryAlreadyAdded(IBAN)){
    await MoveBackFrom_ManageBeneficiaries();
  }else{
  await MoveBackFrom_ManageBeneficiaries();
  await navigateToMakePayments();
  await SelectFromAccount(OneTimePayment.Domestic.FromAcc);
  await EnterNewToAccountName(OneTimePayment.Domestic.ToAcc);
  await clickOnNewButton_OneTimePay();
  await selectOtherBankRadioBtn();
  await enterOneTimePaymentDetails_SameBank(IBAN,OneTimePayment.Domestic.Amount);
  await EnterNoteValue("VerifySaveDomesticBenefeciary");
  await ConfirmTransfer();
  await SaveOneTimePaymentBenefeciary();
  }
  
},TimeOuts.DomesticPayments.OneTimepay);