it("ModifyShedulePayment", async function() {
  
  
  await navigateToSheduledBillPay();
  await EditSheduledBillPay("Updated Sheduled BillPay");

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmBulkPayees","btnScheduled"]);
//   kony.automation.button.click(["frmBulkPayees","btnScheduled"]);
//   await kony.automation.playback.wait(5000);
  
 
//   var nopayments=await kony.automation.playback.waitFor(["frmBillPayScheduled","rtxNoPaymentMessage"],10000);
//   if(nopayments){
//     kony.print("There are no sheduled payments");
//     //Move back to accounts
//     await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"]);
//     kony.automation.button.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
//   }else{

//   kony.print("There are few sheduled payments");
//   await kony.automation.playback.waitFor(["frmBillPayScheduled","segmentBillpay"]);
//   kony.automation.button.click(["frmBillPayScheduled","segmentBillpay[0]","btnEdit"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmPayABill","lblPayABill"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmPayABill","lblPayABill"],"text")).toEqual("Pay a Bill");


//   //Update frequency and Notes as well
//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);

//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Updated Sheduled BillPay");

//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxCheckBoxTnC"]);
//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxCheckBoxTnC"]);
//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);

//   var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],10000);
//   if(warning){
//     await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"]);
//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
//     //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
    
//   }else{
//     await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
//     await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
//   }  
// }
  
},TimeOuts.BillPay.Payment);