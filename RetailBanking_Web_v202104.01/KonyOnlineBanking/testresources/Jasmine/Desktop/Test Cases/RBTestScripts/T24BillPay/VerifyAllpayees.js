it("VerifyAllpayees", async function() {
  
  await navigateToBillPay();
  await clickOnAllpayeesTab();
  await verifyAllPayeesList();
  await MoveBackToDashBoard_AllPayees();
  await verifyAccountsLandingScreen();

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmBulkPayees","btnAllPayees"]);
//   kony.automation.button.click(["frmBulkPayees","btnAllPayees"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmBulkPayees","segmentBillpay"]);
//   kony.automation.flexcontainer.click(["frmBulkPayees","segmentBillpay[0]","flxDropdown"]);

//   await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");


},TimeOuts.BillPay.Payment);