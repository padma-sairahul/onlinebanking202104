it("RepeatBillPayment", async function() {
  
  await navigateToPastBillPay();
  await repeatPastBillPayment("Repeat a BillPay");

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmBulkPayees","btnHistory"]);
//   kony.automation.button.click(["frmBulkPayees","btnHistory"]);
//   await kony.automation.playback.wait(5000);
  
//   var nopayments=await kony.automation.playback.waitFor(["frmBillPayHistory","rtxNoPaymentMessage"],10000);
 
//   if(nopayments){
//     kony.print("There are no History payments");
//     //Move back to accounts
//     await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"]);
//     kony.automation.button.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
//   }else{
//     await kony.automation.playback.waitFor(["frmBillPayHistory","segmentBillpay"]);
//   kony.automation.button.click(["frmBillPayHistory","segmentBillpay[0]","btnRepeat"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Repeat a BillPay");
//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);

//   var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],10000);
//   if(warning){
//     await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"]);
//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
//     //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");

//   }else{
//     await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
//   }
    
//   }

},TimeOuts.BillPay.Payment);