it("VerifyActivateDeActiveBillPay", async function() {

  var unique_RecipitentName="Test Automation_"+new Date().getTime();
  // Add payee and Then Delete same payee once activate/deactivate
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
  await kony.automation.playback.waitFor(["frmBulkPayees","flxAddPayee"]);
  kony.automation.flexcontainer.click(["frmBulkPayees","flxAddPayee"]);
  await kony.automation.playback.waitFor(["frmAddPayee1","btnEnterPayeeInfo"]);
  kony.automation.button.click(["frmAddPayee1","btnEnterPayeeInfo"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterName"]);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterName"],unique_RecipitentName);
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddress"]);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddress"],"LR PALLI");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddressLine2"]);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddressLine2"],"ATMAKUR");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxCity"]);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxCity"],"ATMAKUR");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterZipCode"]);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterZipCode"],"500055");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAccountNmber"]);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAccountNmber"],"1234567890");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxConfirmAccNumber"]);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxConfirmAccNumber"],"1234567890");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","flxClick"]);
  kony.automation.flexcontainer.click(["frmAddPayeeInformation","flxClick"]);
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxAdditionalNote"]);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxAdditionalNote"],"ADD PAYYE");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","btnReset"]);
  kony.automation.button.click(["frmAddPayeeInformation","btnReset"]);
  await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
  kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
  await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
  kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");

  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

  //Activate added payee

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
  kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
  kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"Test");
  await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
  kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","btnPayBill"]);
  kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","btnPayBill"]);

  await kony.automation.playback.waitFor(["frmManagePayees","lblWarningOne"]);
  expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","lblWarningOne"], "text")).toEqual("You are activating the e-bill feature for this biller.");
  expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","lblWarningTwo"], "text")).toEqual("Going forward, your bill amount will be appearing in the list automatically.");
  expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","lblWarningThree"], "text")).toEqual("Do you want to continue?");

  await kony.automation.playback.waitFor(["frmManagePayees","btnProceed"]);
  kony.automation.flexcontainer.click(["frmManagePayees","btnProceed"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
  kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"Test");
  await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
  kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","flxDropdown"]);
  kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","flxDropdown"]);
  await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","btnViewEbill"]);
  kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","btnViewEbill"]);
  await kony.automation.playback.wait(5000);

  // Delete Biller
  await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
  kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"Test");
  await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
  kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
  kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDropdown"]);

  await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
  kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnDeleteBiller"]);
  await kony.automation.playback.waitFor(["frmManagePayees","DeletePopup","btnYes"]);
  kony.automation.button.click(["frmManagePayees","DeletePopup","btnYes"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},TimeOuts.BillPay.Payment);