it("PayBills_AddPayee", async function() {

 // Add payee and Then Delete same payee
  var unique_RecipitentName=BillPay.addPayee.unique_RecipitentName+getRandomString(5);
  var unique_Accnumber="0"+new Date().getTime();

  await navigateToBillPay();
  await clickOnAddPayeeLink();
  await enterPayeeDetails_UsingPayeeinfo(unique_RecipitentName,BillPay.addPayee.address1,BillPay.addPayee.address2,BillPay.addPayee.city,BillPay.addPayee.zipcode,unique_Accnumber,"PayBills_AddPayee");
  await clickOnNextButton_payeeDetails();
  await clickOnConfirmButton_verifyPayee();
  await verifyAddPayeeSuccessMsg();
  await verifyAccountsLandingScreen();
  
  //Activate Payee to initilize payments
  await navigateToManagePayee();
  await selectPayee_ManagePayeeList(unique_RecipitentName);
  await activateNewlyAddedpayee();
  
  
   //Delete same payee
  //await navigateToManagePayee();
  //await selectPayee_ManagePayeeList("Test");
  //await deletePayee_ManagePayee();

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   await kony.automation.playback.waitFor(["frmBulkPayees","flxAddPayee"]);
//   kony.automation.flexcontainer.click(["frmBulkPayees","flxAddPayee"]);
//   await kony.automation.playback.waitFor(["frmAddPayee1","btnEnterPayeeInfo"]);
//   kony.automation.button.click(["frmAddPayee1","btnEnterPayeeInfo"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterName"]);
//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterName"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddress"]);
//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddress"],"LR PALLI");
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddressLine2"]);
//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddressLine2"],"ATMAKUR");
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxCity"]);
//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxCity"],"ATMAKUR");
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterZipCode"]);
//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterZipCode"],"500055");
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAccountNmber"]);
//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAccountNmber"],"1234567890");
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxConfirmAccNumber"]);
//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxConfirmAccNumber"],"1234567890");
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","flxClick"]);
//   kony.automation.flexcontainer.click(["frmAddPayeeInformation","flxClick"]);
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxAdditionalNote"]);
//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxAdditionalNote"],"ADD PAYYE");
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","btnReset"]);
//   kony.automation.button.click(["frmAddPayeeInformation","btnReset"]);
//   await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
//   kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
//   await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
//   kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");

//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   //Delete same payee
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"Test");
//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDropdown"]);

//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnDeleteBiller"]);
//   await kony.automation.playback.waitFor(["frmManagePayees","DeletePopup","btnYes"]);
//   kony.automation.button.click(["frmManagePayees","DeletePopup","btnYes"]);
//   await kony.automation.playback.wait(5000);


//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},TimeOuts.BillPay.Payment);