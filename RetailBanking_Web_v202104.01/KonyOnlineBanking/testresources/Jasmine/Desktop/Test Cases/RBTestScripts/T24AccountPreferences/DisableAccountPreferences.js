it("DisableAccountPreferences", async function() {
  
  await navigateToAccountPreferences();
  await clickonEditButton();
  await DisableEStatement();
  await clickOnSaveButton();
  await MoveBackToDashBoard_ProfileManagement();
  
},120000);