it("VerifyCurrencyField", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("International");
  await SelectUTFFromAccount(UTFPayments.International.FromAcc);
  await SelectUTFToAccount(UTFPayments.International.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.International.Amount);
  await EnterUTFNoteValue("International-VerifyCurrency");
  await VerifyCurrencyField_International();
  
},TimeOuts.UnifiedTransfers.Transfers);