it("CancelTransfer", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("International");
  await SelectUTFFromAccount(UTFPayments.International.FromAcc);
  await SelectUTFToAccount(UTFPayments.International.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.International.Amount);
  await EnterUTFNoteValue("International-Cancel OneTimeTransfers");
  await clickonCancelbutton();
  
},TimeOuts.UnifiedTransfers.Transfers);