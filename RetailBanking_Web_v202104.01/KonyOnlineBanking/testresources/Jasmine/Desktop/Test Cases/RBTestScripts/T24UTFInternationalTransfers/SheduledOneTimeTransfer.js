it("SheduledOneTimeTransfer", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("International");
  await SelectUTFFromAccount(UTFPayments.International.FromAcc);
  await SelectUTFToAccount(UTFPayments.International.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.International.Amount);
  await SelectUTFSendOnDate();
  await EnterUTFNoteValue("International-OneTimeTransfers");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
},TimeOuts.UnifiedTransfers.Transfers);