it("DailySheduledTransfer-DateRange", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("International");
  await SelectUTFFromAccount(UTFPayments.International.FromAcc);
  await SelectUTFToAccount(UTFPayments.International.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.International.Amount);
  await SelectUTFFrequency("Daily");
  await SelectUTFDateRange();
  await EnterUTFNoteValue("International-DailySheduledTransfer-DateRange");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
},TimeOuts.UnifiedTransfers.Transfers);