it("VerifyCompleteTransfersList", async function() {
  
  await navigateToManageTranscations();
  await selectCompletedTransfers();
  await MoveBackFrom_PastTransferActivities();
  
},TimeOuts.UnifiedTransfers.Transfers);