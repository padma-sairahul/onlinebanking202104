it("APrerequesite_AddSameBankBeneficiary", async function() {

  var AccountNumber=ManageBeneficiary.SameBank.AccountNumberList[0].accno;
  var Nickname=ManageBeneficiary.SameBank.Nickname;
  var Address1=ManageBeneficiary.SameBank.Address1;
  var Address2=ManageBeneficiary.SameBank.Address2;
  var city=ManageBeneficiary.SameBank.city;
  var zipcode=ManageBeneficiary.SameBank.zipcode;

  await NavigateToManageBeneficiary();
  if(await isBenefeciaryAlreadyAdded(AccountNumber)){
    await MoveBackFrom_ManageBeneficiaries();
  }else{
    await clickonAddNewBeneficiaryBtn();
    await enterSameBankBeneficiaryDetails(AccountNumber,Nickname,Address1,Address2,city,zipcode);
    await SubmitBankBeneficiaryDetails();
    await VerifyAddBeneficiarySuccessMsg();
  }

},TimeOuts.ManageBenefeciary.AddBenefeciary);