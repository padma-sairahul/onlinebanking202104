it("VerifySaveSameBankBeneficiary", async function() {

  var AccountNumber=OneTimePayment.SameBank.AccountNumberList[3].accno;

  await NavigateToManageBeneficiary();
  if(await isBenefeciaryAlreadyAdded(AccountNumber)){
    await MoveBackFrom_ManageBeneficiaries();
  }else{
    await MoveBackFrom_ManageBeneficiaries();
    await navigateToMakePayments();
    await SelectFromAccount(OneTimePayment.SameBank.FromAcc);
    await EnterNewToAccountName(OneTimePayment.SameBank.ToAcc);
    await clickOnNewButton_OneTimePay();
    await selectSameBankRadioBtn();
    await enterOneTimePaymentDetails_SameBank(AccountNumber,OneTimePayment.SameBank.Amount);
    await EnterNoteValue("VerifySaveSameBankBeneficiary");
    await ConfirmTransfer();
    await SaveOneTimePaymentBenefeciary();
  }


},TimeOuts.SameBankPayments.OneTimepay);