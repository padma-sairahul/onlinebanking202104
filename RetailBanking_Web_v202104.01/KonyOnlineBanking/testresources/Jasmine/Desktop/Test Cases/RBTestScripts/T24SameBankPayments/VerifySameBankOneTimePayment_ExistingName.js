it("VerifySameBankOneTimePayment_ExistingName", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(OneTimePayment.SameBank.FromAcc);
  await EnterNewToAccountName(OneTimePayment.SameBank.ToAcc);
  await clickOnNewButton_OneTimePay();
  await selectSameBankRadioBtn();
  await enterOneTimePaymentDetails_SameBank(OneTimePayment.SameBank.AccountNumberList[2].accno,OneTimePayment.SameBank.Amount);
  await EnterNoteValue("VerifySameBankOneTimePayment_ExistingName");
  await ConfirmTransfer();
  await VerifyOneTimePaymentSuccessMessage();
  
},TimeOuts.SameBankPayments.OneTimepay);