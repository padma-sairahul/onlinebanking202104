it("VerifySameBankPayment_CurrentDate", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.SameBank.FromAcc);
  await SelectToAccount(Payments.SameBank.ToAcc);
  await EnterAmount(Payments.SameBank.Amount);
  await EnterNoteValue("VerifySameBankPayment_CurrentDate");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.SameBankPayments.Payment);