it("VerifyContextualMenuPayment", async function() {

  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
  await selectContextMenuOption(AllAccounts.Current.MenuOptions[1].option);
  await VerifyPaymentsScreen();

  //Do Payment
  await SelectToAccount(Payments.SameBank.ToAcc);
  await EnterAmount(Payments.SameBank.Amount);
  await EnterNoteValue("VerifyContextualMenuPayment");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();

},TimeOuts.SameBankPayments.Payment);
