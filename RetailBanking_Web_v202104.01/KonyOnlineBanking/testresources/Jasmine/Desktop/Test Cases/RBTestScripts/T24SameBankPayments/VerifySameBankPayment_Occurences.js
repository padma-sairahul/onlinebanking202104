it("VerifySameBankPayment_Occurences", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.SameBank.FromAcc);
  await SelectToAccount(Payments.SameBank.ToAcc);
  await EnterAmount(Payments.SameBank.Amount);
  await SelectFrequency("Monthly");
  await SelectDateRange();
  await EnterNoteValue("VerifySameBankPayment_Occurences");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.SameBankPayments.Payment);