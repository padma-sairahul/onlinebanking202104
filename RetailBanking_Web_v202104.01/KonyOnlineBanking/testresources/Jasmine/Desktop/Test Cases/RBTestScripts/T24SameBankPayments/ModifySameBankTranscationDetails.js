it("ModifySameBankTranscationDetails", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.SameBank.FromAcc);
  await SelectToAccount(Payments.SameBank.ToAcc);
  await EnterAmount(Payments.SameBank.Amount);
  await EnterNoteValue("ModifySameBankTranscationDetails");
  await ClickOnModifyButton();
  await EnterNoteValue("Update-ModifySameBankTranscationDetails");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.ManageBenefeciary.AddBenefeciary);