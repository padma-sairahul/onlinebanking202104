it("VerifySwiftFiled_SameBankBeneficiary", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.SameBank.FromAcc);
  await SelectToAccount(Payments.SameBank.ToAcc);
  await ValidateSwiftCodeDetails_SameBankBenefeciary();
  await MoveBackToLandingScreen_Transfers();
  
},120000);