it("EditSameankRecurringPayment", async function() {
  
  await navigateToManageTranscations();
  await ClickonRecurringTab();
  await selectActiveOrders();
  await clickOnEditButton(ManageBeneficiary.SameBank.AccountNumberList[0].accno);
  
},TimeOuts.TrasferActivities.PaymentActivity);