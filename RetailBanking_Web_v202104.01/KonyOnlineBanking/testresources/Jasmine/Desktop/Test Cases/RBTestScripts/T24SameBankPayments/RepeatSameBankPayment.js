it("RepeatSameBankPayment", async function() {
  
  await navigateToManageTranscations();
  await selectCompletedTransfers();
  await clickOnRepeatButton(ManageBeneficiary.SameBank.AccountNumberList[0].accno);
  
},TimeOuts.TrasferActivities.PaymentActivity);