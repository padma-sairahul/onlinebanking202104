it("CancelSameBankRecurringPayment", async function() {
  
  await navigateToManageTranscations();
  await ClickonRecurringTab();
  await selectActiveOrders();
  await clickOnCancelSeriesButton(ManageBeneficiary.SameBank.AccountNumberList[0].accno);
  
},TimeOuts.TrasferActivities.PaymentActivity);