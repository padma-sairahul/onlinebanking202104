it("CombinedStatement_PDF", async function() {
  
  await navigateToCombinedStatements();
  await clickOnGenarateNewStatement();
  await selectFromDate();
  await selectAccountforStatement();
  await clickOnCombinedStatementContinueButton();
  await selectFormatAsPDF();
  await clickOnCreateStatementButton();
  await verifyStatementCreationPopupMsg();
  await MoveBackToLandingScreen_AccDetails();
  
},120000);