it("CombinedStatement_Excel", async function() {
  
  await navigateToCombinedStatements();
  await clickOnGenarateNewStatement();
  await selectFromDate();
  await selectAccountforStatement();
  await clickOnCombinedStatementContinueButton();
  await selectFormatAsExcel();
  await clickOnCreateStatementButton();
  await verifyStatementCreationPopupMsg();
  await MoveBackToLandingScreen_AccDetails();
  
},120000);