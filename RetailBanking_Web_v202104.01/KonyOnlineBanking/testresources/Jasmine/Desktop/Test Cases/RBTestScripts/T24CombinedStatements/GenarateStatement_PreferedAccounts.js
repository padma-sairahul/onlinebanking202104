it("GenarateStatement_AllAccounts", async function() {
  
  await navigateToCombinedStatements();
  await clickOnGenarateNewStatement();
  await selectPreferedAccountsFilter();
  await selectFromDate();
  await selectAccountforStatement();
  await clickOnCombinedStatementContinueButton();
  await clickOnCreateStatementButton();
  await verifyStatementCreationPopupMsg();
  await MoveBackToLandingScreen_AccDetails();

},120000);