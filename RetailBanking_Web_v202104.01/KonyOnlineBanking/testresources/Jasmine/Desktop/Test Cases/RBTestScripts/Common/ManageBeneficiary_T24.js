async function NavigateToManageBeneficiary(){

  appLog("Intiated method to navigate to ManageBeneficiary");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
  appLog("Clicked on ManageBeneficiary button");
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmManageBeneficiaries","lblPayABill"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmManageBeneficiaries","lblPayABill"], "text")).not.toBe("");
  appLog("Successfully verified ManageBeneficiary Header");

}

async function clickonAddNewBeneficiaryBtn(){

  appLog("Intiated method to click on AddNewBeneficiaryButton");

  await kony.automation.playback.waitFor(["frmManageBeneficiaries","flxAddNewBeneficiary"],15000);
  kony.automation.flexcontainer.click(["frmManageBeneficiaries","flxAddNewBeneficiary"]);
  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","lblAddBeneficiary"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmAddBeneficiaryEuro","lblAddBeneficiary"], "text")).not.toBe("");
  appLog("Successfully verified AddNewBeneficiary Header");

}

async function isBenefeciaryAlreadyAdded(accNumber){

  appLog("Intiated method to verify isBenefeciaryAlreadyAdded : <b>"+accNumber+"</b>");
  var isBenAlreadyAdded=false;
  await SearchforBeneficiaryFromList(accNumber);
  var NoBenefeciary=await kony.automation.playback.waitFor(["frmManageBeneficiaries","rtxNoPaymentMessage"],5000);
  appLog("NoBenefeciary :: <b>"+NoBenefeciary+"</b>");
  
  if(!NoBenefeciary){
    isBenAlreadyAdded=true;
    appLog("***Custom Message *** : Benefeciary is Already Added : <b>"+accNumber+"</b>");
  }
  return isBenAlreadyAdded;
}

async function enterSameBankBeneficiaryDetails(AccountNumber,Nickname,Address1,Address2,city,zipcode){

  appLog("Intiated method to enter SameBankBeneficiaryDetails");

  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAccountNumber"],AccountNumber);
  appLog("Successfully Entered Acc Number : <b>"+AccountNumber+"</b>");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],15000);
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],Nickname);
  appLog("Successfully Entered Nickname: <b>"+Nickname+"</b>");
  
  await enterBenefeciaryAddressDetails(Address1,Address2,city,zipcode);

  await clickOnContinueButton();
}

async function enterDomesticBeneficiaryDetails(IBAN,BeneficiaryName,Nickname,Address1,Address2,city,zipcode){

  appLog("Intiated method to enter SameBankBeneficiaryDetails");

  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","flxRadioBtn2"],15000);
  kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","flxRadioBtn2"]);
  await kony.automation.playback.wait(5000);


  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAccountNumber"],IBAN);
  appLog("Successfully Entered IBAN : <b>"+IBAN+"</b>");

  //For page refresh
  kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","flxMainContainer"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryName"],15000);
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryName"],BeneficiaryName);
  appLog("Successfully Entered BeneficiaryName : <b>"+BeneficiaryName+"</b>");
  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],15000);
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],Nickname);
  appLog("Successfully Entered Nickname: <b>"+Nickname+"</b>");
  
  await enterBenefeciaryAddressDetails(Address1,Address2,city,zipcode);

  await clickOnContinueButton();
}

async function enterInternationalBeneficiaryDetails(AccountNumber,SwiftCode,BeneficiaryName,Nickname,Address1,Address2,city,zipcode){

  appLog("Intiated method to enter SameBankBeneficiaryDetails");

  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","flxRadioBtn2"],15000);
  kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","flxRadioBtn2"]);
  await kony.automation.playback.wait(5000);


  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAccountNumber"],AccountNumber);
  appLog("Successfully Entered AccountNumber : <b>"+AccountNumber+"</b>");

  //For page refresh
  kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","flxMainContainer"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxSWIFTBIC"],15000);
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxSWIFTBIC"],SwiftCode);

  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryName"],15000);
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryName"],BeneficiaryName);
  appLog("Successfully Entered BeneficiaryName : <b>"+BeneficiaryName+"</b>");
  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],15000);
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],Nickname);
  appLog("Successfully Entered Nickname: <b>"+Nickname+"</b>");
  
  await enterBenefeciaryAddressDetails(Address1,Address2,city,zipcode);
  
  await clickOnContinueButton();

}

async function enterBenefeciaryAddressDetails(Address1,Address2,city,zipcode){
  
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAddressLine01"],Address1);
  appLog("Successfully Entered Address1: <b>"+Address1+"</b>");
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAddressLine02"],Address2);
  appLog("Successfully Entered Address2: <b>"+Address2+"</b>");
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxCity"],city);
  appLog("Successfully Entered City: <b>"+city+"</b>");
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxPostCode"],zipcode);
  appLog("Successfully Entered Zipcode : <b>"+zipcode+"</b>");
  
}

async function clickOnContinueButton(){

  appLog("Intiated method to click on Continue button");
  kony.automation.button.click(["frmAddBeneficiaryEuro","btnContinue"]);
  await kony.automation.playback.wait(5000);
  var confirmScreen=await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","btnConfirm"],15000);
  expect(confirmScreen).toBe(true,"Custom Message :: Failed to Navigate to BeneficiaryConfirm");
  appLog("Successfully Clicked on Continue Button");
}

async function VerifyNewelyAddedBenefeciaryDetails(){

  await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","lblBankValue"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmAddBeneficiaryConfirmEuro","lblBankValue"], "text")).not.toBe("");
  await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);

}

async function ModifyNewelyAddedBenefeciaryDetails(UpdateCity){

  await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","btnModify"],15000);
  kony.automation.button.click(["frmAddBeneficiaryConfirmEuro","btnModify"]);

  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxCity"],15000);
  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxCity"],UpdateCity);
  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","btnContinue"],15000);
  kony.automation.button.click(["frmAddBeneficiaryEuro","btnContinue"]);
  await kony.automation.playback.wait(5000);
  appLog("Submitted Updated Beneficiary details: <b>"+UpdateCity+"</b>");

}
async function SubmitBankBeneficiaryDetails(){

  await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","btnConfirm"],15000);
  kony.automation.button.click(["frmAddBeneficiaryConfirmEuro","btnConfirm"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on Confirm Button");
}

async function VerifyAddBeneficiarySuccessMsg(){

  appLog("Intiated method to verify Newly Added Beneficiary");

  var SuccessMsg=await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","lblSuccessMessage"],15000);
  if(SuccessMsg){
    expect(kony.automation.widget.getWidgetProperty(["frmAddBeneficiaryAcknowledgementEuro","lblSuccessMessage"], "text")).not.toBe("");
    kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
    appLog("Successfully verified Newly Added Beneficiary");

  }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","rtxDowntimeWarning"],15000)){
    //Move Back to DashBoard
    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
    appLog("Successfully Moved back to Accounts dashboard");

    appLog("Custom Message :: Failed to add Beneficiary");
    fail("Custom Message :: Failed to add Beneficiary");

  }else{
    appLog("Custom Message :: Unable to addBenficiary Successfully");
  }

}

async function clickOnMakePaymentLink_Ackform(){

  var SuccessMsg=await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","lblSuccessMessage"],15000);
  if(SuccessMsg){
    appLog("Successfully verified Newly Added Beneficiary");
    await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","btnMakePayment"],15000);
    kony.automation.button.click(["frmAddBeneficiaryAcknowledgementEuro","btnMakePayment"]);
    //Page will get refresh with required details
    await kony.automation.playback.wait(10000);

    await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],30000);
    expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text")).toEqual("Payments");

    appLog("Successfully Navigated to MakePayment Screen Screen");


  }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","rtxDowntimeWarning"],10000)){
    //Move Back to DashBoard
    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
    appLog("Successfully Moved back to Accounts dashboard");

    appLog("Custom Message :: Failed to add Beneficiary");
    fail("Custom Message :: Failed to add Beneficiary");

  }else{
    appLog("Custom Message :: Unable to addBenficiary Successfully");
  }

}

async function VerifyAddedBeneficiaryFromList(BenficiarySearch){

  await NavigateToManageBeneficiary();
  await SearchforBeneficiaryFromList(BenficiarySearch);
  //var noBeneficiary=await kony.automation.playback.waitFor(["frmManageBeneficiaries","rtxNoPaymentMessage"],15000);

  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
  if(BeneficiaryList){
    kony.automation.flexcontainer.click(["frmManageBeneficiaries","segmentBillpay[0]","flxDropdown"]);
    await MoveBackFrom_ManageBeneficiaries();
  }else{
    //MoveBack to DashBoard
    await MoveBackFrom_ManageBeneficiaries();
    appLog("Custom Message :: Failed to Search Beneficiary from List");
    fail("Custom Message :: Failed to Search Beneficiary from List");
  }

}

async function MoveBackFrom_ManageBeneficiaries(){

  appLog("Intiated method to move back from ManageBenefeciaries");
  await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
  appLog("Successfully Moved back to Accounts dashboard");
}
async function SearchforBeneficiaryFromList(BenficiarySearch){

  appLog("Intiated method to search for Beneficiary : <b>"+BenficiarySearch+"</b>");
  await kony.automation.playback.waitFor(["frmManageBeneficiaries","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmManageBeneficiaries","txtSearch"],BenficiarySearch);
  kony.automation.flexcontainer.click(["frmManageBeneficiaries","btnConfirm"]);
  await kony.automation.playback.wait(5000);
}

async function VerifyBenefeciaryListItem(){

  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
  if(BeneficiaryList){
    appLog("Message :: Benefeciary List is available");
  }else{
    //MoveBack to DashBoard
    await MoveBackFrom_ManageBeneficiaries();
    appLog("Custom Message :: Failed to Search Beneficiary from List");
    fail("Custom Message :: Failed to Search Beneficiary from List");
  }
}

async function DeleteBeneficiaryFromList(BenficiarySearch){

  SearchforBeneficiaryFromList(BenficiarySearch);

  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
  if(BeneficiaryList){
    appLog("Found Beneficiary from List : <b>"+BenficiarySearch+"</b>");
    kony.automation.flexcontainer.click(["frmManageBeneficiaries","segmentBillpay[0]","flxDropdown"]);
    kony.automation.button.click(["frmManageBeneficiaries","segmentBillpay[0]","btnRemoveRecipient"]);
    await kony.automation.playback.waitFor(["frmManageBeneficiaries","DeletePopup","btnYes"],15000);
    kony.automation.button.click(["frmManageBeneficiaries","DeletePopup","btnYes"]);
    await kony.automation.playback.wait(5000);
    appLog("Successfully Removed Beneficiary from List: <b>"+BenficiarySearch+"</b>");
    await MoveBackFrom_ManageBeneficiaries();
  }else{
    //MoveBack to DashBoard
    await MoveBackFrom_ManageBeneficiaries();
    appLog("Custom Message :: Failed to Search Beneficiary from List");
    //fail("Custom Message :: Failed to Search Beneficiary from List");
  }

}

async function EditBeneficiaryFromList(BenficiarySearch,UpdateCity){

  await SearchforBeneficiaryFromList(BenficiarySearch);

  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
  if(BeneficiaryList){

    appLog("Found Beneficiary from List : <b>"+BenficiarySearch+"</b>");
    kony.automation.flexcontainer.click(["frmManageBeneficiaries","segmentBillpay[0]","flxDropdown"]);
    kony.automation.button.click(["frmManageBeneficiaries","segmentBillpay[0]","btnEdit"]);
    await kony.automation.playback.wait(5000);
    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","lblAddBeneficiary"],15000);
    expect(kony.automation.widget.getWidgetProperty(["frmAddBeneficiaryEuro","lblAddBeneficiary"], "text")).not.toBe("");
    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxCity"],15000);
    kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxCity"],UpdateCity);
    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","btnContinue"],15000);
    kony.automation.button.click(["frmAddBeneficiaryEuro","btnContinue"]);
    await kony.automation.playback.wait(5000);
    appLog("Submitted Edited Beneficiary details: <b>"+BenficiarySearch+"</b>");

    await VerifyAddBeneficiarySuccessMsg();

  }else{

    appLog("Custom Message :: Failed to Search Beneficiary from List");
    //fail("Custom Message :: Failed to Search Beneficiary from List");

    await MoveBackFrom_ManageBeneficiaries();
  }

}

async function ClickonMakePaymentLink_BeneficiariesList(BenficiarySearch){

  await SearchforBeneficiaryFromList(BenficiarySearch);

  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
  if(BeneficiaryList){
    appLog("Found Beneficiary from List : <b>"+BenficiarySearch+"</b>");
    kony.automation.button.click(["frmManageBeneficiaries","segmentBillpay[0]","btnAction"]);
    await kony.automation.playback.wait(10000);
  }else{
    appLog("Custom Message :: Failed to Search Beneficiary from List");
    fail("Custom Message :: Failed to Search Beneficiary from List");
    await MoveBackFrom_ManageBeneficiaries();
  }

}

async function SearchForBenefeciary(SearchText){

  await kony.automation.playback.waitFor(["frmManageBeneficiaries","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmManageBeneficiaries","txtSearch"],SearchText);
  kony.automation.flexcontainer.click(["frmManageBeneficiaries","btnConfirm"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],10000);
  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],10000);
  if(BeneficiaryList){
    appLog("Found Beneficiary from List : <b>"+SearchText+"</b>");
  }else{
    appLog("Custom Message :: Failed to Search Beneficiary from List");
    //fail("Custom Message :: Failed to Search Beneficiary from List");
    await MoveBackFrom_ManageBeneficiaries();
  }

  //kony.automation.flexcontainer.click(["frmManageBeneficiaries","flxClearBtn"]);
}

async function VerifyBenefeciariesPageCount(){

  await kony.automation.playback.waitFor(["frmManageBeneficiaries","lblPagination"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmManageBeneficiaries","lblPagination"], "text")).not.toBe("");
  await MoveBackFrom_ManageBeneficiaries();
}

async function VerifyBenefeciariesPagination(){

  var pageNext=await kony.automation.playback.waitFor(["frmManageBeneficiaries","flxPaginationNext"],15000);
  if(pageNext){
    kony.automation.flexcontainer.click(["frmManageBeneficiaries","flxPaginationNext"]); 
  }
  var pagePrevious=await kony.automation.playback.waitFor(["frmManageBeneficiaries","flxPaginationPrevious"],15000);
  if(pagePrevious){
    kony.automation.flexcontainer.click(["frmManageBeneficiaries","flxPaginationPrevious"]);
  }

  await MoveBackFrom_ManageBeneficiaries();
}