
async function verifyAccountsLandingScreen(){

  //await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
}

async function SelectAccountsOnDashBoard(AccountType){

  appLog("Intiated method to analyze accounts data Dashboard");

  var Status=false;
  
  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
  var segLength=accounts_Size.length;

  var finished = false;
  for(var x = 0; x <segLength && !finished; x++) {

    var segHeaders="segAccounts["+x+",-1]";

    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
    //appLog('Sub accounts size is '+subaccounts_Length);

    for(var y = 0; y <subaccounts_Length; y++){

      var seg="segAccounts["+x+","+y+"]";

      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
      if(typeOfAccount.includes(AccountType)){
        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
        await kony.automation.playback.wait(5000);
        finished = true;
        Status=true;
        break;
      }
    }
  }
  
  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
}

async function clickOnFirstCheckingAccount(){

  appLog("Intiated method to click on First Checking account");
  SelectAccountsOnDashBoard("Checking");
  //appLog("Successfully Clicked on First Checking account");
  //await kony.automation.playback.wait(5000);
}

async function clickOnFirstSavingsAccount(){

  appLog("Intiated method to click on First Savings account");
  SelectAccountsOnDashBoard("Saving");
  //appLog("Successfully Clicked on First Savings account");
  //await kony.automation.playback.wait(5000);
}


async function clickOnFirstCreditCardAccount(){

  appLog("Intiated method to click on First CreditCard account");
  SelectAccountsOnDashBoard("Credit");
  //appLog("Successfully Clicked on First CreditCard account");
  //await kony.automation.playback.wait(5000);
}

async function clickOnFirstDepositAccount(){

  appLog("Intiated method to click on First Deposit account");
  SelectAccountsOnDashBoard("Deposit");
  //appLog("Successfully Clicked on First Deposit account");
  //await kony.automation.playback.wait(5000);
}

async function clickOnFirstLoanAccount(){

  appLog("Intiated method to click on First Loan account");
  SelectAccountsOnDashBoard("Loan");
  //appLog("Successfully Clicked on First Loan account");
  //await kony.automation.playback.wait(5000);
}

async function clickOnSearch_AccountDetails(){

  appLog("Intiated method to click on Search Icon");
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
  appLog("Successfully Clicked on Search Icon");
}

async function selectTranscationtype(TransactionType){

  appLog("Intiated method to select Transcation type");
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],TransactionType);
  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
}

async function selectAmountRange(From,To){

  appLog("Intiated method to select Amount Range");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],From);

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],To);

  appLog("Successfully selected amount Range : ["+From+","+To+"]");
}

async function selectCustomdate(){

  appLog("Intiated method to select Custom Date Range");
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
  appLog("Successfully selected Date Range");
}

async function clickOnbtnSearch(){

  appLog("Intiated method to click on Search Button with given search criteria");
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
  appLog("Successfully Clicked on Search Button");
  await kony.automation.playback.wait(5000);
}

async function validateSearchResult() {

  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
  if(noResult){
    appLog("No Results found with given criteria..");
    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
  }else{
    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
    appLog("Successfully clicked on Transcation with given search criteria");
  }
}

async function MoveBackToLandingScreen_AccDetails(){

  appLog("Move back to Account Dashboard from AccountsDetails");
  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);

  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  appLog("Successfully Moved back to Account Dashboard");
}

async function SelectContextualOnDashBoard(AccountType){

  appLog("Intiated method to analyze accounts data Dashboard");
  
  var Status=false;

  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
  var segLength=accounts_Size.length;

  var finished = false;
  for(var x = 0; x <segLength && !finished; x++) {

    var segHeaders="segAccounts["+x+",-1]";

    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
    //appLog('Sub accounts size is '+subaccounts_Length);

    for(var y = 0; y <subaccounts_Length; y++){

      var seg="segAccounts["+x+","+y+"]";

      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
      if(typeOfAccount.includes(AccountType)){
        await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
        finished = true;
        Status=true;
        break;
      }
    }
  }
  
  expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
}

async function clickOnCheckingAccountContextMenu(){

  appLog("Intiated method to click on Checking account context Menu");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,0]"]);
  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
  //   appLog("Successfully clicked on Checking account context Menu");

  SelectContextualOnDashBoard("Checking");
}

async function clickOnSavingsAccountContextMenu(){

  appLog("Intiated method to click on Saving account context Menu");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,1]"]);
  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,1]","flxMenu"]);
  //   appLog("Successfully clicked on Saving account context Menu");
  SelectContextualOnDashBoard("Saving");
}

async function clickOnCreditCardAccountContextMenu(){

  appLog("Intiated method to click on CreditCard account context Menu");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,2]"]);
  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,2]","flxMenu"]);
  //   appLog("Successfully clicked on CreditCard account context Menu");

  SelectContextualOnDashBoard("Credit");
}

async function clickOnDepositAccountContextMenu(){

  appLog("Intiated method to click on Deposit account context Menu");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,3]"]);
  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,3]","flxMenu"]);
  //   appLog("Successfully clicked on Deposit account context Menu");

  SelectContextualOnDashBoard("Deposit");
}

async function clickOnLoanAccountContextMenu(){

  appLog("Intiated method to click on Loan account context Menu");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[1,0]"]);
  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxMenu"]);
  //   appLog("Successfully clicked on Loan account context Menu");

  SelectContextualOnDashBoard("Loan");

}

async function verifyContextMenuOptions(myList_Expected){

  //var myList_Expected = new Array();
  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
  myList_Expected.push(myList_Expected);

  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
  var segLength=accounts_Size.length;
  //appLog("Length is :: "+segLength);
  var myList = new Array();

  for(var x = 0; x <segLength-1; x++) {

    var seg="segAccountListActions["+x+"]";
    //appLog("Segment is :: "+seg);
    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
    //appLog("Text is :: "+options);
    myList.push(options);
  }

  appLog("My Actual List is :: "+myList);
  appLog("My Expected List is:: "+myList_Expected);

  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
  //appLog("isFounded"+isFounded);
  expect(isFounded).toBe(true);
}
async function MoveBackToLandingScreen_Accounts(){

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

}

async function scrolltoTranscations_accountDetails(){

  appLog("Intiated method to scroll to Transcations under account details");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");

  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);

}

async function verifyAccountSummary_CheckingAccounts(){

  appLog("Intiated method to verify account summary for Checking Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
}

async function verifyAccountSummary_DepositAccounts(){

  appLog("Intiated method to verify account summary for Deposit Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
}

async function verifyAccountSummary_CreditCardAccounts(){

  appLog("Intiated method to verify account summary for CreditCard Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);

}

async function verifyAccountSummary_LoanAccounts(){

  appLog("Intiated method to verify account summary for Loan Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
}

async function verifyAccountSummary_SavingsAccounts(){

  appLog("Intiated method to verify account summary for Savings Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
}

async function verifyAccountsOrder_DashBoard(){

  appLog("Intiated method to verify accounts order in Dashboard");

  //Accounts Order can't be garunteed across different users. Hence checking all Types of accounts.
  var myList = new Array();
  var myList_Expected = new Array();
  myList_Expected.push("Checking","Saving","Credit","Deposit","Loan");

  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
  var segLength=accounts_Size.length;

  for(var x = 0; x <segLength; x++) {

    var segHeaders="segAccounts["+x+",-1]";

    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
    //appLog('Sub accounts size is '+subaccounts_Length);

    for(var y = 0; y <subaccounts_Length; y++){
      var seg="segAccounts["+x+","+y+"]";
      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
      myList.push(accountName);
    }
  }

  appLog("My Actual List is :: "+myList);
  appLog("My Expected List is:: "+myList_Expected);
}


async function VerifyAccountOnDashBoard(AccountType){

  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
  var myList = new Array();

  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
  var segLength=accounts_Size.length;

  var finished = false;
  for(var x = 0; x <segLength && !finished; x++) {

    var segHeaders="segAccounts["+x+",-1]";

    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
    //appLog('Sub accounts size is '+subaccounts_Length);

    for(var y = 0; y <subaccounts_Length; y++){

      var seg="segAccounts["+x+","+y+"]";

      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
      if(typeOfAccount.includes(AccountType)){
        appLog("Successfully verified : <b>"+accountName+"</b>");
        myList.push("TRUE");
        finished = true;
        break;
      }else{
        myList.push("FALSE");
      }
    }
  }

  appLog("My Actual List is :: "+myList);
  var Status=JSON.stringify(myList).includes("TRUE");
  appLog("Over all Result is  :: <b>"+Status+"</b>");
  expect(Status).toBe(true);
}

async function VerifyCheckingAccountonDashBoard(){

  appLog("Intiated method to verify Checking account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
  VerifyAccountOnDashBoard("Checking");
}

async function VerifySavingsAccountonDashBoard(){

  appLog("Intiated method to verify Savings account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
  VerifyAccountOnDashBoard("Saving");
}
async function VerifyCreditCardAccountonDashBoard(){

  appLog("Intiated method to verify CreditCard account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
  VerifyAccountOnDashBoard("Credit");
}

async function VerifyDepositAccountonDashBoard(){

  appLog("Intiated method to verify Deposit account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
  VerifyAccountOnDashBoard("Deposit");
}

async function VerifyLoanAccountonDashBoard(){

  appLog("Intiated method to verify Loan account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
  VerifyAccountOnDashBoard("Loan");
}

async function verifyViewAllTranscation(){

  appLog("Intiated method to view all Tranx in AccountDetails");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
}

async function verifyAdvancedSearch_AccountDetails(AmountRange1,AmountRange2){

  appLog("Intiated method to verify Advanced Search in AccountDetails");

  //   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnAll"],15000);
  //   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnAll"]);
  //   appLog("Successfully clicked on All button under AccountDetails");
  //   await kony.automation.playback.wait(5000);

  appLog("Intiated method to click on Seach Icon");
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
  appLog("Successfully Clicked on Seach Icon");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], "Transfers");
  appLog("Successfully selected Transcation Type");

  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
  appLog("Successfully selected amount range From");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
  appLog("Successfully selected amount range To");

  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
  appLog("Successfully selected date range");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
  appLog("Successfully clicked on Search button");
  await kony.automation.playback.wait(5000);

  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
  if(noResult){
    appLog("No Results found with given criteria..");
    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
  }else{
    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
    appLog("Successfully clicked on Transcation with given search criteria");
  }

}


async function selectContextMenuOption(Option){

  appLog("Intiated method to select context menu option :: "+Option);

  var myList = new Array();

  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");

  var segLength=accounts_Size.length;
  //appLog("Length is :: "+segLength);
  for(var x = 0; x <segLength; x++) {

    var seg="segAccountListActions["+x+"]";
    //appLog("Segment will be :: "+seg);
    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
    //appLog("Text is :: "+TransfersText);

    if(TransfersText===Option){
      appLog("Option to be selected is :"+TransfersText);
      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
      await kony.automation.playback.wait(5000);
      myList.push("TRUE");
      break;
    }else{
      myList.push("FALSE");
    }
  }

  appLog("My Actual List is :: "+myList);
  var Status=JSON.stringify(myList).includes("TRUE");
  appLog("Over all Result is  :: <b>"+Status+"</b>");
  expect(Status).toBe(true);
}


async function verifyVivewStatementsHeader(){

  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");

}
