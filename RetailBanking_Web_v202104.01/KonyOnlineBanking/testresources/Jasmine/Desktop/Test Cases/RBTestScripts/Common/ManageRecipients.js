async function NavigateToManageRecipitents(){

  appLog("Intiated method to navigate to ManageRecipitents");
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
  await kony.automation.playback.wait(10000);
  appLog("Clicked on ManageRecipients button");

  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","lblTitle"],20000);
  expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","BeneficiaryList","lblTitle"],"text")).toEqual("Manage Recipients");
  appLog("Successfully verified ManageRecipients Header");
}

async function clickOnExternalRecipitentsTab(){

  //External Acc list
  appLog("Intiated method to navigate to External Reciptents");
  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnTab1"],15000);
  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","btnTab1"]);
  appLog("Clicked on External Recipients tab");
  await kony.automation.playback.wait(5000);
  //   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"],15000);
  //   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
  //   appLog("Successfully Clicked on External Recipient from list");
}
async function clickOnP2PRecipitentsTab(){

  //P2P Acc list
  appLog("Intiated method to navigate to P2P Reciptents");
  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnTab2"],15000);
  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","btnTab2"]);
  appLog("Clicked on P2P Recipients tab");
  await kony.automation.playback.wait(5000);
  //   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"],15000);
  //   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
  //   appLog("Successfully Clicked on P2P Recipient from list");
}

async function MoveBacktoDashboard_ManageRecipitent(){

  appLog("Intiated method to Move back Accounts dashboard");
  await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
  appLog("Successfully Moved back to Accounts dashboard");
}

async function clickonAddExternalAccounttab(){

  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow2"],15000);
  expect(status).toBe(true);
  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow2"]);
  appLog("Successfully Clicked on Add External Account Flex");
  await kony.automation.playback.wait(5000);
}

async function clickonAddinfinityBankAccounttab(){

  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow1"],15000);
  expect(status).toBe(true);
  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow1"]);
  appLog("Successfully Clicked on Add Infinity Account Flex");
  await kony.automation.playback.wait(5000);
}

async function clickonAddInternationalAccounttab(){

  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow3"],15000);
  expect(status).toBe(true);
  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow3"]);
  appLog("Successfully Clicked on Add International Account Flex");
  await kony.automation.playback.wait(5000);
}

async function clickonAddP2PAccounttab(){

  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow4"],15000);
  expect(status).toBe(true);
  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow4"]);
  appLog("Successfully Clicked on Add P2P Account Flex");
  await kony.automation.playback.wait(5000);
}

async function enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName){

  appLog("Intiated method to add enterExternalBankAccountDetails");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],Routingno);
  appLog("Successfully Entered Routing Number : <b>"+Routingno+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],Accno);
  appLog("Successfully Entered Acc Number : <b>"+Accno+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],Accno);
  appLog("Successfully Re-Entered Acc Number : <b>"+Accno+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],unique_RecipitentName);
  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
  appLog("Successfully Clicked on Continue Button");

  await linkReciptent();
}

async function enterInternationalBankAccountDetails(swiftCode,Accno,unique_RecipitentName){

  appLog("Intiated method to add enterInternationalBankAccountDetails");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],swiftCode);
  appLog("Successfully Entered SwiftCode : <b>"+swiftCode+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],Accno);
  appLog("Successfully Entered Acc Number : <b>"+Accno+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],Accno);
  appLog("Successfully Re-Entered Acc Number : <b>"+Accno+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],unique_RecipitentName);
  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
  appLog("Successfully Clicked on Continue Button");

  await linkReciptent();
}

async function enterSameBankAccountDetails(Accno,unique_RecipitentName){

  appLog("Intiated method to add enterSameBankAccountDetails");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],Accno);
  appLog("Successfully Entered Acc Number : <b>"+Accno+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],Accno);
  appLog("Successfully Re-Entered Acc Number : <b>"+Accno+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],unique_RecipitentName);
  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
  appLog("Successfully Clicked on Continue Button");

  await linkReciptent();

}

async function enterP2PAccountDetails_Email(unique_RecipitentName,email){

  appLog("Intiated method to add enterSameBankAccountDetails");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],unique_RecipitentName);
  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],unique_RecipitentName);
  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","img2"],15000);
  kony.automation.widget.touch(["frmFastP2P","addBenificiary","img2"], null,null,[10,19]);
  appLog("Successfully Selected Email Radio button ");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],email);
  appLog("Successfully Entered Email name : <b>"+email+"</b>");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
  appLog("Successfully Clicked on Continue Button");

  await linkReciptent();

}

async function enterP2PAccountDetails_MobileNumber(unique_RecipitentName,phno){

  appLog("Intiated method to add enterSameBankAccountDetails");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],unique_RecipitentName);
  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],unique_RecipitentName);
  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","img1"],15000);
  kony.automation.widget.touch(["frmFastP2P","addBenificiary","img1"], null,null,[10,19]);
  appLog("Successfully Selected Email Radio button ");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],phno);
  appLog("Successfully Entered Email name : <b>"+phno+"</b>");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
  appLog("Successfully Clicked on Continue Button");

  await linkReciptent();

}


async function linkReciptent(){

  var linkreciptent=await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","lblHeader"],15000);

  if(linkreciptent){
    kony.automation.widget.touch(["frmFastP2P","addBenificiary","contractList","lblCheckBoxSelectAll"], [8,7],null,null);
    appLog("Successfully selected Select All CheckBox");
    await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","flxCol4"],15000);
    kony.automation.flexcontainer.click(["frmFastP2P","addBenificiary","contractList","flxCol4"]);
    await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","btnAction6"],15000);
    kony.automation.button.click(["frmFastP2P","addBenificiary","contractList","btnAction6"]);
    appLog("Successfully Clicked on Link Reciptent SaveReciptent Button");
  }

  appLog("Intiated Method to Click on AddAccount Button");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction6"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction6"]);
  appLog("Successfully Clicked on AddAccount Button");
}

async function verifyAddingNewReciptientSuccessMsg(){

  var success=await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblSection1Message"],30000);
  if(success){
    expect(kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","lblSection1Message"],"text")).not.toBe('');
    appLog("Successfully verified Newly Added Reciptent");
  }else if(await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","rtxDowntimeWarning"],5000)){
    //appLog("Logged in User is not authorized to perform this action");
    //fail('Logged in User is not authorized to perform this action');
    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","rtxDowntimeWarning"],"text"));
    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","rtxDowntimeWarning"],"text"));
  }
  await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
  appLog("Successfully Moved back to Accounts dashboard");
}


async function SearchforPayee_External(payeeName){

  appLog("Intiated method to Search for a Payee :: <b>"+payeeName+"</b>");

  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmFastManagePayee","BeneficiaryList","txtSearch"],payeeName);
  appLog("Successfully Entered payee name : <b>"+payeeName+"</b>");
  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnConfirm"],15000);
  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","btnConfirm"]);
  appLog("Successfully Clicked on Search button");
  await kony.automation.playback.wait(5000);

}

async function DeleteReciptent(){

  appLog("Intiated method to Delete a Reciptent");

  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"],15000);
  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn3"]);
  appLog("Successfully Clicked on RemoveRecipient button");
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmFastManagePayee","lblDescriptionIC"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblDescriptionIC"],"text")).not.toBe('');
  appLog("Successfully Verified RemoveRecipient PopUp Msg");

  await kony.automation.playback.waitFor(["frmFastManagePayee","btnYesIC"],15000);
  kony.automation.button.click(["frmFastManagePayee","btnYesIC"]);
  appLog("Successfully Clicked on RemoveRecipient YES button");
  //await kony.automation.playback.wait(5000);

}

async function EditReciptent(UniqueUpdatedName,UniqueUpdatedNickName){

  appLog("Intiated method to Edit Reciptent");

  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"],15000);
  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn2"]);
  appLog("Successfully Clicked on EditRecipient button");
  //await kony.automation.playback.wait(5000);

  // Line Items 3,4, and 5 will be different for External,International and SameBank acc.
  //More over Searc button is not working hence Iterating over acc and Editing accordingly.

  for(var i=3;i<=5;i++){

    var key="lblDetailKey"+i;
    var value="lblDetailValue"+i;

    await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary",key],15000);
    var keyLabel =kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary",key], "text");

    if(keyLabel==='Recipient Name'){

      kony.automation.textbox.enterText(["frmFastP2P","addBenificiary",value],UniqueUpdatedName);
      appLog("Successfully Updated <b>"+keyLabel+"</b>");

    }else if(keyLabel==='Account Nickname'){

      kony.automation.textbox.enterText(["frmFastP2P","addBenificiary",value],UniqueUpdatedNickName);
      appLog("Successfully Updated <b>"+keyLabel+"</b>");

    }else{
      appLog("Select Name or Nick name Text filed to Update");
    }

  }

  //   if(AccType.toUpperCase() === SAMEBANK){

  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],UniqueUpdatedName);

  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],UniqueUpdatedNickName);

  //   }else{

  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],UniqueUpdatedName);

  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],UniqueUpdatedNickName);
  //   }


  //Having intermittent issue in Save button
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
  appLog("Successfully Clicked on SAVE Button");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","btnAction6"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","contractList","btnAction6"]);
  appLog("Successfully Clicked on Link Reciptent SaveReciptent Button");

}

async function EditP2PReciptent(UniqueUpdatedName,UniqueUpdatedNickName){

  appLog("Intiated method to Edit P2P Reciptent");

  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"],15000);
  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn1"]);
  appLog("Successfully Clicked on EditRecipient button");
  //await kony.automation.playback.wait(5000);

  appLog("Intiated method to Update Reciptent value");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],UniqueUpdatedName);
  appLog("Successfully Updated Reciptent name value");

  appLog("Intiated method to Update Reciptent value");
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],UniqueUpdatedNickName);
  appLog("Successfully Updated Reciptent nick name value");

  //Having intermittent issue in Save button
  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
  appLog("Successfully Clicked on SAVE Button");

  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","btnAction6"],15000);
  kony.automation.button.click(["frmFastP2P","addBenificiary","contractList","btnAction6"]);
  appLog("Successfully Clicked on Link Reciptent SaveReciptent Button");

}