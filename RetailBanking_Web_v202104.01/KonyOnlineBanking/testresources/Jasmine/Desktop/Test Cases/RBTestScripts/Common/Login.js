async function verifyMayBeLater(){

  // Verifying may be later screen
  appLog('Verifying MayBeLater Popup');
  var mayBeLater=await kony.automation.playback.waitFor(["frmLogout","CustomFeedbackPopup","btnNo"],5000);
  if(mayBeLater){
    kony.automation.button.click(["frmLogout","CustomFeedbackPopup","btnNo"]);
    await kony.automation.playback.waitFor(["frmLogout","logOutMsg","AlterneteActionsLoginNow"],10000);
    kony.automation.flexcontainer.click(["frmLogout","logOutMsg","AlterneteActionsLoginNow"]);
  }

}

async function verifyTermsandConditions(){

  // Verifying Terms and conditions screen 
  var termsconditions=await kony.automation.playback.waitFor(["frmPreTermsandCondition","flxAgree"],15000);

  appLog('Verifying Terms and conditions');
  appLog("Is terms and conditions : <b>"+termsconditions+"</b>");

  if(termsconditions){

    kony.automation.widget.touch(["frmPreTermsandCondition","lblFavoriteEmailCheckBox"], null,null,[15,9]);
    kony.automation.flexcontainer.click(["frmPreTermsandCondition","flxAgree"]);

    await kony.automation.playback.waitFor(["frmPreTermsandCondition","btnProceed"],10000);
    kony.automation.button.click(["frmPreTermsandCondition","btnProceed"]);
  }

  await kony.automation.playback.waitFor(["frmDashboard"],15000);
}

async function verifyLoginFunctionality(userName,passWord){

  // Login to the application
  appLog('Initiating app login functionality');
  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],10000);
  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxUserName"],userName);
  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxPassword"],10000);
  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxPassword"],passWord);
  await kony.automation.playback.waitFor(["frmLogin","loginComponent","btnLogin"],10000);
  kony.automation.button.click(["frmLogin","loginComponent","btnLogin"]);
  appLog('Successfully clicked on Sign in Button');
  
}

