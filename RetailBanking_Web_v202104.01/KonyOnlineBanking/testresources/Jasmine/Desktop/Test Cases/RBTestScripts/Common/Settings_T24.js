async function NavigateToProfileSettings(){

  appLog("Intiated method to Navigate to Personal Details");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);
  await kony.automation.playback.wait(15000);

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPersonalDetailsHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPersonalDetailsHeading"], "text")).toEqual("Personal Details");

  appLog("Successfully Navigated to Personal Details");
}

async function NavigateToAlertSettings(){

  appLog("Intiated method to Navigate to Alert Settings");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings4flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings4flxMyAccounts"]);
  await kony.automation.playback.wait(10000);

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAlertsHeading"],30000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAlertsHeading"], "text")).toEqual("Security");
  
  appLog("Successfully Navigated to Alert Settings");
}

async function selectProfileSettings_PhoneNumber(){

  appLog("Intiated method to Navigate to PhoneNumber Flex");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxPhone"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxPhone"]);
  //await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPhoneNumbersHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPhoneNumbersHeading"], "text")).toEqual("Phone Number");

  appLog("Successfully Navigated to PhoneNumber Flex");
}

async function selectProfileSettings_EmailAddress(){

  appLog("Intiated method to Navigate to EmailID Flex");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEmail"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEmail"]);
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEmailHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEmailHeading"], "text")).toEqual("Email");

  appLog("Successfully Navigated to EmailID Flex");
}

async function selectProfileSettings_Address(){

  appLog("Intiated method to Navigate to Address Flex");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddress"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddress"]);
  //await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddressHeading"], "text")).toEqual("Address");

  appLog("Successfully Navigated to Address Flex");
}

async function ProfileSettings_addNewPhoneNumberDetails(phoneNumber,isPrimary){

  appLog("Intiated method to addNewPhoneNumberDetails");

  var Status="FALSE";

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddPhoneNumberHeading"], "text")).toEqual("Add Phone Number");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxAddPhoneNumberType"],15000);
  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Other");
  appLog("Successfully Selected PhoneNumber Type");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],"91");
  appLog("Successfully Entered CountryCode");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumber"],phoneNumber);
  appLog("Successfully Entered Phone Number as : <b>"+phoneNumber+"</b>");

  if(isPrimary==='YES'){

    await selectMakePrimayPhoneNumbercheckBox();
  }
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberSave"],15000);
  kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
  appLog("Successfully clicked on SAVE button");
  await kony.automation.playback.wait(5000);

  try{
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
    var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
    var segLength=seg_Size.length;
    //appLog("Length is :: "+segLength);
    for(var x = 0; x <segLength; x++){
      var seg="segPhoneNumbers["+x+"]";
      //appLog("Segment is :: "+seg);
      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
      var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
      appLog("Phone Number Text is :: "+phonenum);
      if(phonenum===phoneNumber){
        appLog("Successfully Added Mobile Number "+phoneNumber);
        Status="TRUE";
        break;
      }
    }
  }catch(Exception){

  }finally{
    expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
  }

}

async function ProfileSettings_addInvalidPhoneNumberDetails(phoneNumber,isPrimary){

  appLog("Intiated method to addNewPhoneNumberDetails");

  var Status="FALSE";

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddPhoneNumberHeading"], "text")).toEqual("Add Phone Number");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxAddPhoneNumberType"],15000);
  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Other");
  appLog("Successfully Selected PhoneNumber Type");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],"91");
  appLog("Successfully Entered CountryCode");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumber"],phoneNumber);
  appLog("Successfully Entered Phone Number as : <b>"+phoneNumber+"</b>");

  if(isPrimary==='YES'){

    await selectMakePrimayPhoneNumbercheckBox();
  }
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberSave"],15000);
  kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
  appLog("Successfully clicked on SAVE button");
  await kony.automation.playback.wait(5000);

  var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);

  if(isAddHeader){

    appLog("Custom Message :: Invalid characters entered");

  }else{
    appLog("Accepted Invalid Characters");
    fail("Custom Message :: Accepted Invalid Characters");
  }


}

async function ProfileSettings_VerifyaddInvalidPhoneNumberFunctionality(phoneNumber,isPrimary){

  appLog("Intiated method to VerifyaddNewPhoneNumberFunctionality");

  var isAddNewNumber=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewNumber"],15000);
  //appLog("Button status is :"+isAddNewNumber);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
  var segLength=seg_Size.length;
  //appLog("PhoneNumbers size is :: "+segLength);

  if(segLength<3&&isAddNewNumber){
    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewNumber"]);
    appLog("Successfully Clicked on Add New Phone Number Button");
    await ProfileSettings_addInvalidPhoneNumberDetails(phoneNumber,isPrimary);
  }else{
    appLog("Maximum phone numbers already added");
  }
}

async function selectMakePrimayPhoneNumbercheckBox(){

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddCheckBox3"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddCheckBox3"]);
  appLog("Successfully Selected Entered Phone Number as Primary");
}

async function ProfileSettings_UpdatePhoneNumber(updatedPhonenum){

  // Update Number
  appLog("Intiated method to Update PhoneNumberDetails");

  var Status="FALSE";

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");

  var segLength1=accounts_Size1.length;
  kony.print("Segment length is :"+segLength1);

  var isEditble=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers[0]","btnViewDetail"],15000);
  if(isEditble){

    kony.automation.button.click(["frmProfileManagement","settings","segPhoneNumbers[0]","btnViewDetail"]);
    appLog("Successfully clicked on ViewDetails button");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxPhoneNumber"]);
    kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxPhoneNumber"],updatedPhonenum);
    appLog("Successfully Updated Phone number as : <b>"+updatedPhonenum+"</b>");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
    kony.automation.button.click(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
    appLog("Successfully clicked on SAVE button");
    await kony.automation.playback.wait(5000);

    try{
      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
      var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
      var segLength=seg_Size.length;
      //appLog("Length is :: "+segLength);
      for(var x = 0; x <segLength; x++){
        var seg="segPhoneNumbers["+x+"]";
        //appLog("Segment is :: "+seg);
        await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
        var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
        appLog("Phone Number Text is :: "+phonenum);
        if(phonenum===updatedPhonenum){
          appLog("Successfully Updated Mobile Number to : "+updatedPhonenum);
          Status="TRUE";
          break;
        }
      }
    }catch(exception){
      Status="FALSE";
    }finally{
      expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
    }

  }else{
    appLog("Unable to Update PhoneNumberDetails");
  }
}

async function ProfileSettings_DeletePhoneNumber(phoneNumber){

  //Delete already added Mobile Number
  appLog("Intiated method to Delete PhoneNumberDetails");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
  var segLength=seg_Size.length;
  //appLog("Length is :: "+segLength);
  for(var x = 0; x <segLength; x++){
    var seg="segPhoneNumbers["+x+"]";
    //appLog("Segment is :: "+seg);
    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
    var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
    appLog("Phone Number Text is :: "+phonenum);
    if(phonenum===phoneNumber){
      appLog("Phone Number to be Deleted is : "+phoneNumber);
      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
      appLog("Successfully Clicked on Delete Button");

      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
      appLog("Successfully Clicked on YES Button");
      await kony.automation.playback.wait(5000);
      break;
    }
  } 
}

async function ProfileSettings_VerifyaddNewPhoneNumberFunctionality(phoneNumber,isPrimary){

  appLog("Intiated method to VerifyaddNewPhoneNumberFunctionality");

  var isAddNewNumber=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewNumber"],15000);
  //appLog("Button status is :"+isAddNewNumber);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
  var segLength=seg_Size.length;
  //appLog("PhoneNumbers size is :: "+segLength);

  if(segLength<3&&isAddNewNumber){
    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewNumber"]);
    appLog("Successfully Clicked on Add New Phone Number Button");
    await ProfileSettings_addNewPhoneNumberDetails(phoneNumber,isPrimary);

    // No Need to delete after adding
    
//     var Header=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPhoneNumbersHeading"],15000);
//     if(Header&&isPrimary==='NO'){
//       await ProfileSettings_DeletePhoneNumber(phoneNumber);
//     }else{
//       appLog("Custom Message :: Update Customer Details Failed");
//       fail("Custom Message :: Custom Message :: Update Customer Details Failed");
//     }
     // No Need to delete after adding

  }else{
    appLog("Maximum phone numbers already added");
  }
}

async function ProfileSettings_addNewAddressDetails(addressLine1,addressLine2,zipcode,isPrimary){

  appLog("Intiated method to addNewAddressDetails");

  var Status="FALSE";

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine1"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine1"],addressLine1);
  appLog("Successfully Entered AddressLine1 : <b>"+addressLine1+"</b>");
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine2"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine2"],addressLine2);
  appLog("Successfully Entered AddressLine2 : <b>"+addressLine2+"</b>");
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCountry"],15000);
  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCountry"], "IN");
  appLog("Successfully Selected Country Code");
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxState"],15000);
  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxState"], "IN-TG");
  appLog("Successfully Selected State Code");
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","txtCity"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","txtCity"],"HYD");
  appLog("Successfully Selected City Code");
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxZipcode"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxZipcode"],zipcode);
  appLog("Successfully Entered Zipcode : <b>"+zipcode+"</b>");

  if(isPrimary==='YES'){

    await selectMakeDefaultAddresscheckBox();
  }
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddressAdd"],15000);
  kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddressAdd"]);
  appLog("Successfully Clicked on Add Address Button");
  await kony.automation.playback.wait(5000);

  //   var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewAddressHeader"],15000);

  //   if(isAddHeader){
  //     appLog("Custom Message :: Update Customer Details Failed");
  //     fail("Custom Message :: Update Customer Details Failed");

  //   }else{
  //     appLog("Successfully Added new Address details");
  //   }
  try{
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
    var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");

    var segLength=accounts_Size.length;
    for(var x = 0; x <segLength; x++) {
      var seg="segAddresses["+x+"]";
      //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblAddressLine1"],15000);
      var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblAddressLine1"], "text");
      appLog("Address Text is :: "+address1);
      if(address1===addressLine1){
        appLog("Successfully added Address "+addressLine1);
        Status="TRUE";
        break;
      }
    }
  }catch(Exception){
    Status="FALSE";
  }finally{
    expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
  }
}


async function selectMakeDefaultAddresscheckBox(){

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"]);
  appLog("Successfully Selected Entered Address as Primary");
}

async function ProfileSettings_UpdateAddress(UpdatedZip){

  // Update Address
  appLog("Intiated method to update Address Details");

  var Status="FALSE";

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"]);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");

  var segLength=accounts_Size.length;
  for(var x = 0; x <segLength; x++) {
    var seg="segAddresses["+x+"]";
    var ismailing=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblCommunicationAddress"], "text");
    appLog("is Mailing Address : "+ismailing);
    if(ismailing===""){

      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnEdit"]);
      appLog("Successfully clicked on Edit button");
      await kony.automation.playback.wait(5000);
      await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditZipcode"],15000);
      kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditZipcode"],UpdatedZip);
      appLog("Successfully Entered Updated Zipcode as : <b>"+UpdatedZip+"</b>");
      await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAddressSave"],15000);
      kony.automation.button.click(["frmProfileManagement","settings","btnEditAddressSave"]);
      appLog("Successfully clicked on SAVE button");
      await kony.automation.playback.wait(5000);

      try{
        await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
        var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");

        var segLength1=accounts_Size1.length;
        for(var x1 = 0; x1 <segLength1; x1++) {
          var seg1="segAddresses["+x1+"]";
          //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
          await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"lblAddressLine1"],15000);
          var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"lblAddressLine1"], "text");
          appLog("Address Text is :: "+address1);
          if(address1===addressLine1){
            appLog("Successfully Updated Address to : "+addressLine1);
            Status="TRUE";
            break;
          }
        }

      }catch(Exception){
        Status="FALSE";
      }finally{
        expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
      }

    }
    break;
  }

}

async function ProfileSettings_DeleteAddress(addressLine1){

  // Delete Address
  appLog("Intiated method to Delete Address Details");

  var Status="FALSE";

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");

  var segLength=accounts_Size.length;
  for(var x = 0; x <segLength; x++) {
    var seg="segAddresses["+x+"]";
    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblAddressLine1"],15000);
    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblAddressLine1"], "text");
    appLog("Address Text is :: "+address1);
    if(address1===addressLine1){
      appLog("Address to be Deleted is : "+addressLine1);
      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
      appLog("Successfully clicked on DELETE button");
      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
      appLog("Successfully clicked on YES button");
      await kony.automation.playback.wait(5000);
      break;
    }
  }

  try{
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
    var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");

    var segLength1=accounts_Size1.length;
    for(var x1 = 0; x1 <segLength1; x1++) {
      var seg1="segAddresses["+x1+"]";
      //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"lblAddressLine1"],15000);
      var address2=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"lblAddressLine1"], "text");
      appLog("Address Text is :: "+address2);
      if(address2===addressLine1){
        appLog("Address is not deleted");
        Status="TRUE";
        break;
      }
    }

  }catch(Exception){
    Status="FALSE";
  }finally{
    expect(Status).toContain("FALSE", "Custom Message :: Unable to delete address");
  }

}

async function ProfileSettings_VerifyaddNewAddressFunctionality(addressLine1,addressLine2,zipcode,isPrimary){


  appLog("Intiated method to VerifyaddNewAddressFunctionality");

  var isAddNewAddress=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddress"],15000);
  //appLog("Button status is :"+isAddNewAddress);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
  var segLength=seg_Size.length;
  //appLog("Address size is :: "+segLength);

  if(segLength<3&&isAddNewAddress){
    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddress"]);
    appLog("Successfully clicked on AddNewAddress button");
    await ProfileSettings_addNewAddressDetails(addressLine1,addressLine2,zipcode,isPrimary);

    // No Need to delete address after adding
//     var Header=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"],15000);
//     if(Header&&isPrimary==='NO'){
//       await ProfileSettings_DeleteAddress(addressLine1);
//     }else{
//       appLog("Custom Message :: Update Customer Details Failed");
//       fail("Custom Message :: Update Customer Details Failed");
//     }
    // No Need to delete address after adding

  }else{
    appLog("Maximum Address already added");
  }

}

async function ProfileSettings_addEmailAddressDetails(emailAddress,isPrimary){

  // Add new email ID
  appLog("Intiated method to add new Emai Address Details");

  var Status="FALSE";

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewEmailHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddNewEmailHeading"], "text")).toEqual("Add New Email");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEmailId"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEmailId"],emailAddress);
  appLog("Successfully Entered new Email Address : <b>"+emailAddress+"</b>");

  if(isPrimary==='YES'){

    await selectMakePrimayEmailIDcheckBox();
  }

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddEmailIdAdd"],15000);
  kony.automation.button.click(["frmProfileManagement","settings","btnAddEmailIdAdd"]);
  appLog("Successfully clicked on Add Button");
  await kony.automation.playback.wait(5000);

  //   var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewEmailHeading"],15000);

  //   if(isAddHeader){
  //     appLog("Custom Message :: Update Customer Details Failed");
  //     fail("Custom Message :: Update Customer Details Failed");
  //   }else{
  //     appLog("Successfully added new Email Address");
  //   }

  try{
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");

  var segLength=accounts_Size.length;
  for(var x = 0; x <segLength; x++) {
    var seg="segEmailIds["+x+"]";
    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblEmail"],15000);
    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblEmail"], "text");
    appLog("Email Text is :: "+address1);
    if(address1===emailAddress){
      appLog("Email Number to be Deleted is : "+emailAddress);
      Status="TRUE";
      break;
    }
  }

  }catch(Exception){
    Status="FALSE";
  }finally{
    expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
  }
}

async function selectMakePrimayEmailIDcheckBox(){

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxMarkAsPrimaryEmailCheckBox"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxMarkAsPrimaryEmailCheckBox"]);
  appLog("Successfully Selected Entered EmailID as Primary");
}

async function ProfileSettings_UpdateEmailAddress(updatedemailid){

  // Update email ID
  appLog("Intiated method to Update Email address");

  var Status="FALSE";

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");

  var segLength1=accounts_Size1.length;
  appLog("Length is :"+segLength1);

  var isEditble=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds[0]","btnEdit"],15000);
  if(isEditble){
    kony.automation.button.click(["frmProfileManagement","settings","segEmailIds[0]","btnEdit"]);
    appLog("Successfully clicked on Edit Button");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditEmailId"],15000);
    kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditEmailId"],updatedemailid);
    appLog("Successfully Entered Updated Email ID : <b>"+updatedemailid+"</b>");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditEmailIdSave"],15000);
    kony.automation.button.click(["frmProfileManagement","settings","btnEditEmailIdSave"]);
    appLog("Successfully Clicked on SAVE button");
    await kony.automation.playback.wait(5000);

    //     var isEditHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditEmailHeading"],15000);

    //     if(isEditHeader){
    //       appLog("Custom Message :: Update Customer Details Failed");
    //       fail("Custom Message :: Update Customer Details Failed");
    //     }else{
    //       appLog("Successfully Updated email Address");
    //     }

    try{
      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
    var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");

    var segLength=accounts_Size.length;
    for(var x = 0; x <segLength; x++) {
      var seg="segEmailIds["+x+"]";
      //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblEmail"],15000);
      var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblEmail"], "text");
      appLog("Email Text is :: "+address1);
      if(address1===updatedemailid){
        appLog("Successfully updated EmailAddress : "+updatedemailid);
        Status="TRUE";
        break;
      }
    }

    }catch(Exception){
       Status="FALSE";
    }finally{
       expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
    }
  }

}

async function ProfileSettings_deleteEmailAddressDetails(emailAddress){

  // Delete Address
  appLog("Intiated method to Delete Email Details");

  var Status="FALSE";

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");

  var segLength=accounts_Size.length;
  for(var x = 0; x <segLength; x++) {
    var seg="segEmailIds["+x+"]";
    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblEmail"],15000);
    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblEmail"], "text");
    appLog("Email Text is :: "+address1);
    if(address1===emailAddress){
      appLog("Email Number to be Deleted is : "+emailAddress);
      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
      appLog("Successfully Clicked on Delete Button");
      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
      appLog("Successfully Clicked on YES Button");
      await kony.automation.playback.wait(5000);
      break;
    }
  }

  try{
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");

  var segLength1=accounts_Size1.length;
  for(var x1 = 0; x1 <segLength1; x1++) {
    var seg1="segEmailIds["+x1+"]";
    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"lblEmail"],15000);
    var address2=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"lblEmail"], "text");
    appLog("Email Text is :: "+address2);
    if(address2===emailAddress){
      appLog("Failed to delete EmailAddress : "+emailAddress);
      Status="TRUE";
      break;
    }
  }

  }catch(Exception){
    Status="FALSE";
  }finally{
    expect(Status).toContain("FALSE", "Custom Message :: Unable to delete Email");
  }
}

async function ProfileSettings_VerifyaddEmailAddressFunctionality(emailAddress,isPrimary){

  appLog("Intiated method to VerifyaddEmailAddressFunctionality");

  var isAddEmailAddress=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewEmail"],15000);
  //appLog("Button status is :"+isAddEmailAddress);

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
  var segLength=seg_Size.length;
  //appLog("email size is :: "+segLength);

  if(segLength<3&&isAddEmailAddress){

    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewEmail"]);
    appLog("Successfully clicked on NewEmail Button");
    await ProfileSettings_addEmailAddressDetails(emailAddress,isPrimary);

    // No need to delete after adding
//     var Header= await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEmailHeading"],15000);
//     if(Header&&isPrimary==='NO'){
//       await ProfileSettings_deleteEmailAddressDetails(emailAddress);
//     }else{

//       appLog("Custom Message :: Update Customer Details Failed");
//       fail("Custom Message :: Custom Message :: Update Customer Details Failed");
//       //await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberCancel"]);
//       //kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberCancel"]);
//     }
       // No need to delete after adding

  }else{
    appLog("Maximum Email Address already added");
  }

}


async function MoveBackToDashBoard_ProfileManagement(){

  // Move back to base state
  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
  appLog("Successfully Moved back to Accounts dashboard");
}

async function NavigateToAccountSettings(){

  appLog("Intiated method to navigate to Account Settings");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"]);
  await kony.automation.playback.wait(10000);

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAccountsHeader"],30000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAccountsHeader"], "text")).toEqual("Accounts");

  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
  expect(Status).toBe(true,"FAILED to load Accounts segment");
  appLog("Successfully Navigated to AccountSettings");
}


async function clickonDefaultAccountstab(){

  appLog("Intiated method to click on DefaultAccountstab");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxSetDefaultAccount"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxSetDefaultAccount"]);

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblDefaultTransactionAccounttHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblDefaultTransactionAccounttHeading"], "text")).toEqual("Default Transaction Accounts");

  appLog("Successfully clicked on DefaultAccountstab");

  await kony.automation.playback.wait(5000);
}
async function clickonAccountPreferencetab(){

  appLog("Intiated method to click on AccountPreferencetab");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAccountPreferences"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAccountPreferences"]);

  appLog("Successfully clicked on AccountPreferencetab");

  await kony.automation.playback.wait(5000);
}


async function EditFavAccountPreferences(){

  appLog("Intiated method to Edit FavAccountPreferences");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
  kony.automation.button.click(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"]);
  await kony.automation.playback.wait(10000);
  appLog("Successfully Clicked on Edit button");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAccountsHeader"],30000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEditAccountsHeader"], "text")).toEqual("Edit Account");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAccountNickNameValue"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAccountNickNameValue"],'My Checking');
  appLog("Successfully Updated NickName value");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAccountsSave"],15000);
  kony.automation.button.click(["frmProfileManagement","settings","btnEditAccountsSave"]);
  appLog("Successfully Clicked on SAVE button");
}

async function CancelFavAccountPreferences(){

  appLog("Intiated method to cancel FavAccountPreferences");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
  kony.automation.button.click(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"]);
  await kony.automation.playback.wait(10000);
  appLog("Successfully Clicked on Edit button");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAccountsHeader"],30000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEditAccountsHeader"], "text")).toEqual("Edit Account");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAccountNickNameValue"],15000);
  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAccountNickNameValue"],'My Checking');
  appLog("Successfully Updated NickName value");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAccountsCancel"],15000);
  kony.automation.button.click(["frmProfileManagement","settings","btnEditAccountsCancel"]);
  appLog("Successfully Clicked on CANCEL button");
}

async function SetDefaultAccountPreferences(){

  appLog("Intiated method to Set DefaultAccountPreferences Tab");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"],15000);
  kony.automation.button.click(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on Edit Button");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblSelectedDefaultAccounts"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblSelectedDefaultAccounts"], "text")).not.toBe('');

  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxBillPay"],15000);
  //   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxBillPay"], "190128223241502");
  //   appLog("Successfully Selected Default BillPay acc");

  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCheckDeposit"],15000);
  //   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCheckDeposit"], "190128223242830");
  //   appLog("Successfully Selected Default Deposit acc");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"]);
  appLog("Successfully Clicked on SAVE Button");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblSelectedDefaultAccounts"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblSelectedDefaultAccounts"], "text")).not.toBe('');
  appLog("Successfully Verified Default accounts");
}

async function EnableDisableSecurityAlerts(){
  
	await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAlerts"],15000);
	kony.automation.button.click(["frmProfileManagement","settings","btnEditAlerts"]);
    appLog("Successfully clicked on Edit button");
	await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblStatus1"],15000);
	kony.automation.widget.touch(["frmProfileManagement","settings","lblStatus1"], null,null,[29,15]);
	await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEnableSwitch"],15000);
	kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEnableSwitch"]);
    appLog("Successfully clicked on Radio button");
	await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblStatus1"],15000);
	kony.automation.widget.touch(["frmProfileManagement","settings","lblStatus1"], null,null,[14,17]);
	await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEnableSwitch"],15000);
	kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEnableSwitch"]);
    appLog("Successfully clicked on Radio button");
	await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAlertSave"],15000);
	kony.automation.button.click(["frmProfileManagement","settings","btnAlertSave"]);
	await kony.automation.playback.wait(10000);
    appLog("Successfully clicked on SAVE button");
	await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
}

