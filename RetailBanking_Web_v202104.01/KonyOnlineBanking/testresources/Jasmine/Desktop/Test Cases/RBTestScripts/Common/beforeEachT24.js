beforeEach(async function() {

  //await kony.automation.playback.wait(10000);
  strLogger=[];
  var formName='';
  formName=kony.automation.getCurrentForm();
  appLog('Inside before Each function');
  appLog("Current form name is  :: "+formName);
  if(formName==='frmDashboard'){
    appLog('Already in dashboard');
    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
    appLog('Moving back from '+formName);
    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
    appLog('Moving back from '+formName);
    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  }else{
    appLog("Form name is not available");
  }

//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
//     appLog('Already in dashboard');
//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
//     appLog('Inside Login Screen');
//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
//     appLog('Moving back from frmAccountsDetails');
//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
//     appLog('Moving back from frmProfileManagement');
//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
//     appLog('Moving back from frmManageBeneficiaries');
//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmAddBeneficiaryEuro');
//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmMakePayment');
//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmConfirmEuro');
//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmAcknowledgementEuro');
//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmPastPaymentsEurNew');
//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmScheduledPaymentsEurNew');
//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
//     appLog('Moving back from frmConsolidatedStatements');
//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
//     appLog('Moving back from frmCardManagement');
//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmBulkPayees');
//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmMakeOneTimePayee');
//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmMakeOneTimePayment');
//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmOneTimePaymentConfirm');
//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmManagePayees');
//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmPayABill');
//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmPayBillConfirm');
//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmPayBillAcknowledgement');
//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmBillPayScheduled');
//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmAddPayee1');
//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmAddPayeeInformation');
//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmPayeeDetails');
//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmVerifyPayee');
//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmPayeeAcknowledgement');
//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmBillPayHistory');
//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmBillPayActivation');
//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmBillPayActivationAcknowledgement');
//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransfersWindow');
//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferMakeTransfer');
//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmConfirmDetails');
//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmBulkTemplateAddRecipients');
//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateTempSelectRecipients');
//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateTempAddDomestic');
//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransfersRecent');
//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmWireTransfersManageRecipients');
//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmActivateWireTransfer');
//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
//     appLog('Moving back from frmPersonalFinanceManagement');
//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
//     appLog('Moving back from frmCustomViews');
//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmStopPayments');
//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
//     appLog('Moving back from frmNAO');
//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmSavingsPotLanding');
//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateSavingsPot');
//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateSavingsGoal');
//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateGoalConfirm');
//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateGoalAck');
//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateBudget');
//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmEditGoal');
//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateBudgetConfirm');
//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmCreateBudgetAck');
//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
//     appLog('Moving back from frmEditBudget');
//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
//   }else{
//     appLog("Form name is not available");
//   }
  
},480000);