async function ClickonCustomviewDropdown(){

  appLog("Intiated method to click on custom View dropdown");
  await kony.automation.playback.waitFor(["frmDashboard","flxDropDown"],30000);
    kony.automation.widget.touch(["frmDashboard","flxDropDown"], [6,15],null,null);
  kony.automation.flexcontainer.click(["frmDashboard","flxDropDown"]);
  appLog("successfully click on custom View dropdown");
  await kony.automation.playback.wait(5000);
  //appLog("Intiated method to Verify Add New+ Flex");
  //var Status=await kony.automation.playback.waitFor(["frmDashboard","accountsFilter","flxAddNew"],30000);
  //expect(Status).toBe(true,'Failed to Verify Add New+ Flex');
}

async function ClickonAddNewFlex(){

  appLog("Intiated method to click on Add New+ Flex");
  await kony.automation.playback.waitFor(["frmDashboard","accountsFilter","flxAddNew"],15000);
    kony.automation.flexcontainer.click(["frmDashboard","accountsFilter","flxAddNew"]);
  kony.automation.widget.touch(["frmDashboard","accountsFilter","flxAddNew"], [6,15],null,null);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on Add New+ Flex");
  var Status=await kony.automation.playback.waitFor(["frmCustomViews","lblCustomView"],15000);
  expect(Status).toBe(true,"Failed to Navigate to frmCustomViews");

}

async function MoveBackfrom_CustomView(){

  appLog('Intiated method to moveback from CustomViews screen');
  await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
  await verifyAccountsLandingScreen();
  appLog('Successfully Moved back from  Custom view screen');
}

async function EnterCustomViewName(customViewName){

  appLog("Intiated method to Enter View name :: <b>"+customViewName+"</b>");
  await kony.automation.playback.waitFor(["frmCustomViews","txtCustomViewName"],15000);
  kony.automation.textbox.enterText(["frmCustomViews","txtCustomViewName"],customViewName);
  appLog("Successfully Entered Custom View Name");
}

async function VerifyDuplicateViewNameError(){

  await kony.automation.playback.waitFor(["frmCustomViews","lbltxtCustomViewNameValid"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCustomViews","lbltxtCustomViewNameValid"], "text")).not.toBe("");

  await MoveBackfrom_CustomView();
}

async function SearchAccounts_forView(AccountName){

  appLog("Intiated method to Search for accounts for view :: <b>"+AccountName+"</b>");
  await kony.automation.playback.waitFor(["frmCustomViews","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmCustomViews","txtSearch"],AccountName);
  appLog("Successfully entered account to search");
  await kony.automation.playback.waitFor(["frmCustomViews","btnConfirm"],15000);
  kony.automation.flexcontainer.click(["frmCustomViews","btnConfirm"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on Search button");
}

async function SelectAccounts_forView(){

  appLog("Intiated method to select CheckBox for accounts");
  await kony.automation.playback.waitFor(["frmCustomViews","segCustomViews"],15000);
  kony.automation.flexcontainer.click(["frmCustomViews","segCustomViews[0,-1]","flxChecked"]);
  appLog("Successfully selected CheckBox for accounts");
}

async function ClickonCreateButton(){

  appLog("Intiated method to click on create button");
  await kony.automation.playback.waitFor(["frmCustomViews","btnCreate"],15000);
  kony.automation.button.click(["frmCustomViews","btnCreate"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on create button");
}

async function VerifySelectedViewName_onDashBoard(ViewName){

  appLog("Intiated method to verify current view Name");
  await kony.automation.playback.waitFor(["frmDashboard","lblSelectedFilter"],30000);
  var selectedViewName=kony.automation.widget.getWidgetProperty(["frmDashboard","lblSelectedFilter"], "text");
  if(selectedViewName===ViewName){
    appLog("View Name created successfully:: <b>"+ViewName+"</b>");
  }else{
    appLog("View Name :: <b>"+ViewName+"</b>"+"Doesn't match with selected one :: "+selectedViewName+"</b>");
    fail("View Name :: <b>"+ViewName+"</b>"+"Doesn't match with selected one :: "+selectedViewName+"</b>");
  }
}

async function ClickonEditButton(){

  appLog("Intiated method to click on EDIT Option");
  await kony.automation.playback.waitFor(["frmDashboard","accountsFilter","segCustomFiltersHeader"],30000);
  kony.automation.widget.touch(["frmDashboard","accountsFilter","segCustomFiltersHeader[0]","flxEdit"], null,null,[1,24]);
  kony.automation.flexcontainer.click(["frmDashboard","accountsFilter","segCustomFiltersHeader[0]","flxEdit"]);
  appLog("Successfully clicked on EDIT Option");
  var Status=await kony.automation.playback.waitFor(["frmCustomViews","lblCustomView"],15000);
  expect(Status).toBe(true,"Failed to Navigate to frmCustomViews");
}

async function DeleteCustomView(){

  appLog("Intiated method to click on DELETE Option");
  await kony.automation.playback.waitFor(["frmCustomViews","btnDelete"],15000);
  kony.automation.button.click(["frmCustomViews","btnDelete"]);
  appLog("Successfully clicked on DELETE Option");
  await kony.automation.playback.waitFor(["frmCustomViews","deletePopup","btnYes"],15000);
  kony.automation.button.click(["frmCustomViews","deletePopup","btnYes"]);
  appLog("Successfully clicked on YES button");
  await kony.automation.playback.wait(5000);
  appLog("Intiated method to verify DashBoard screen after View deletion");
  var Status=await kony.automation.playback.waitFor(["frmDashboard","lblSelectedFilter"],30000);
  expect(Status).toBe(true,'Failed to Move to DashBoard after view deletion');
}

