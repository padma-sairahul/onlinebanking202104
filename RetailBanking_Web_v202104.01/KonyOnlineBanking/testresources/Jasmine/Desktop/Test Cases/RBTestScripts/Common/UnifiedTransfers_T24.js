async function navigateToUnifiedTransfers(){

  appLog("Intiated method to Navigate UTF Screen");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","UNIFIEDTRANSFERflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","UNIFIEDTRANSFERflxAccountsMenu"]);
  //await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  await VerifyUTFScreen();

}

async function VerifyUTFScreen(){

  var Status=await kony.automation.playback.waitFor(["frmLanding","lblSelectTransfer"],30000);
  expect(Status).toBe(true,"Failed to Navigate to UTF Screen");
  //var Status=kony.automation.widget.getWidgetProperty(["frmLanding","lblSelectTransfer"], "text");
  //expect(Status).toEqual("Select Transfer Type","Failed to Navigate to UTF Screen");
  //appLog("Successfully Navigated to UTF Screen");

}

async function clickOnAddNewAccountBtn_SameBank(){

  appLog("Intiated method to Click on AddNewAccount button");
  await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection1","btnAction2"],15000);
  kony.automation.button.click(["frmLanding","transferTypeSelection1","btnAction2"]);
  //await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  appLog("Successfully Clicked on AddNewAccount button");
  await VerifyAddNewaccountHeader();
}

async function clickOnAddNewAccountBtn_Domestic(){

  appLog("Intiated method to Click on AddNewAccount button");
  await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection2","btnAction2"],15000);
  kony.automation.button.click(["frmLanding","transferTypeSelection2","btnAction2"]);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  appLog("Successfully Clicked on AddNewAccount button");
  await VerifyAddNewaccountHeader();
}

async function clickOnAddNewAccountBtn_International(){

  appLog("Intiated method to Click on AddNewAccount button");
  await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection3","btnAction2"],15000);
  kony.automation.button.click(["frmLanding","transferTypeSelection3","btnAction2"]);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  appLog("Successfully Clicked on AddNewAccount button");
  await VerifyAddNewaccountHeader();
}

async function clickOnAddNewAccountBtn_PayAPerson(){

  appLog("Intiated method to Click on AddNewAccount button");
  await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection4","btnAction2"],15000);
  kony.automation.button.click(["frmLanding","transferTypeSelection4","btnAction2"]);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  appLog("Successfully Clicked on AddNewAccount button");
  await VerifyAddNewaccountHeader();
}

async function VerifyAddNewaccountHeader(){

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"lblAddNewAccount"],15000);
  var Status=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"lblAddNewAccount"],"text");
  expect(Status).toBe("Add New Account","Failed to Navigate to New acount Screen");
}

async function EnterInternationalAccDetails(BeneficiaryName,AccountNumber,SwiftCode,Nickname){

  appLog("Intiated method to enter International acc details");

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],15000);
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],BeneficiaryName);
  appLog("Successfully entered benefeciary name as :: <b>"+BeneficiaryName+"</b>");
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNewAccountNumber"],AccountNumber);
  appLog("Successfully entered acc number as :: <b>"+AccountNumber+"</b>");
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxReenterAccountNumber"],AccountNumber);
  appLog("Successfully Re-entered acc number as :: <b>"+AccountNumber+"</b>");
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeDetailField1"],SwiftCode);
  appLog("Successfully entered SWIFT number as :: <b>"+SwiftCode+"</b>");
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNickName"],Nickname);
  appLog("Successfully entered Nick name as :: <b>"+Nickname+"</b>");
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"],15000);
  await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
  kony.automation.button.click([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
  appLog("Successfully clicked on Continue button");
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);

  var Status=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000);
  //var Status=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"lblHeader"], "text");
  expect(Status).toBe(true,"Failed to Navigate to confirm Screen");
}

async function EnterDomesticAccDetails(BeneficiaryName,IBAN,Nickname){

  appLog("Intiated method to enter Domestic acc details");

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],15000);
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],BeneficiaryName);
  appLog("Successfully entered benefeciary name as :: <b>"+BeneficiaryName+"</b>");
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNewAccountNumber"],IBAN);
  appLog("Successfully entered acc number as :: <b>"+IBAN+"</b>");
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxReenterAccountNumber"],IBAN);
  appLog("Successfully Re-entered acc number as :: <b>"+IBAN+"</b>");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNickName"],Nickname);
  appLog("Successfully entered Nick name as :: <b>"+Nickname+"</b>");
  //await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"],15000);
  await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
  kony.automation.button.click([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
  appLog("Successfully clicked on Continue button");
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);

  var Status=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000);
  //var Status=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"lblHeader"], "text");
  expect(Status).toBe(true,"Failed to Navigate to confirm Screen");
}

async function EnterSameBankAccDetails(BeneficiaryName,AccountNumber,Nickname){

  appLog("Intiated method to enter Domestic acc details");

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],15000);
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],BeneficiaryName);
  appLog("Successfully entered benefeciary name as :: <b>"+BeneficiaryName+"</b>");
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNewAccountNumber"],AccountNumber);
  appLog("Successfully entered acc number as :: <b>"+AccountNumber+"</b>");
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxReenterAccountNumber"],AccountNumber);
  appLog("Successfully Re-entered acc number as :: <b>"+AccountNumber+"</b>");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNickName"],Nickname);
  appLog("Successfully entered Nick name as :: <b>"+Nickname+"</b>");
  //await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"],15000);
  await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
  kony.automation.button.click([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
  appLog("Successfully clicked on Continue button");
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);

  var Status=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000);
  //var Status=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"lblHeader"], "text");
  expect(Status).toBe(true,"Failed to Navigate to confirm Screen");
}


async function ClickonConfirmButton(){

  appLog("Intiated method to click on Confirm button");
  kony.automation.button.click([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"]);
  appLog("Successfully clicked on Confirm button");
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);

}

async function VerifyAddNewAccSuccessMsg(){

  appLog("Intiated method to Verify AddNewAcc SuccessMsg");
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"TransferAcknowledgement","lblSection1Message"],30000);
  expect(kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"TransferAcknowledgement","lblSection1Message"], "text")).not.toBe("");
  appLog("Successfully Verified AddNewAcc SuccessMsg");
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"customheadernew","flxAccounts"]);
  appLog("Successfully Moved back to account dashboard");
}

async function ClickonMakeTransfrBtn(TransferType){

  appLog("Intiated method to click on MakeTransfer button");

  switch (TransferType) {
    case "SameBank":
      await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection1","btnAction1"],15000);
      kony.automation.button.click(["frmLanding","transferTypeSelection1","btnAction1"]);
      appLog("Successfully Clicked on MakeTransfer: <b>"+TransferType+"</b>");
      break;
    case "Domestic":
      await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection2","btnAction1"],15000);
      kony.automation.button.click(["frmLanding","transferTypeSelection2","btnAction1"]);
      appLog("Successfully Clicked on MakeTransfer: <b>"+TransferType+"</b>");
      break;
    case "International":
      await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection3","btnAction1"],15000);
      kony.automation.button.click(["frmLanding","transferTypeSelection3","btnAction1"]);
      appLog("Successfully Clicked on MakeTransfer: <b>"+TransferType+"</b>");
      break;
    case "PayAPersoon":
      await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection4","btnAction1"],15000);
      kony.automation.button.click(["frmLanding","transferTypeSelection4","btnAction1"]);
      appLog("Successfully Clicked on MakeTransfer: <b>"+TransferType+"</b>");
      break;
  }

  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  appLog("Successfully clicked on MakeTransfer button");
}

async function SelectUTFFromAccount(fromAcc){

  appLog("Intiated method to Select From Account :: <b>"+fromAcc+"</b>");
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","flxTextBox"],30000);
  kony.automation.widget.touch([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","flxTextBox"], [241,25],null,null);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","flxTextBox","txtBox"],30000);
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","flxTextBox","txtBox"],fromAcc);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","segRecords"],30000);
  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","segRecords[0,0]","flxDropdownRecordField"]);
  appLog("Successfully Selected From Account from List");
}

async function SelectUTFToAccount(ToAccReciptent){

  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","flxTextBox"],30000);
  kony.automation.widget.touch([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","flxTextBox"], [241,25],null,null);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","flxTextBox","txtBox"],30000);
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","flxTextBox","txtBox"],ToAccReciptent);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","segRecords"],30000);
  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","segRecords[0,0]","flxDropdownRecordField"]);
  appLog("Successfully Selected To Account from List");
  // To laod dynamic data after selecting To account
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);

}

async function Enter_CCY_AmountValue(amountValue){

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxTransferCCYDropdown"],15000);
  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxTransferCCYDropdown"]);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","segTransferCCYList"],15000);
  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segTransferCCYList[1]","flxDropdownRecord"]);
  appLog("Successfully Selected Currency Type");
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","txtBoxTransferAmount"],15000);
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","txtBoxTransferAmount"],amountValue);
  appLog("Successfully Selected Amount Value");

}

async function SelectUTFFrequency(freqValue) {

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxFrequencyDropdown"],15000);
  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxFrequencyDropdown"]);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList"],15000);
  switch (freqValue) {
    case "Once":
      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[0]","flxDropdownRecord"]);
      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
      break;
    case "Daily":
      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[1]","flxDropdownRecord"]);
      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
      break;
    case "Weekly":
      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[2]","flxDropdownRecord"]);
      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
      break;
    case "Bi-weekly":
      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[3]","flxDropdownRecord"]);
      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
      break;
    case "Monthly":
      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[4]","flxDropdownRecord"]);
      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
      break;
    case "Qtrly":
      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[5]","flxDropdownRecord"]);
      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
      break;
    case "HalfYearly":
      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[6]","flxDropdownRecord"]);
      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
      break;
    case "Yearly":
      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[7]","flxDropdownRecord"]);
      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
      break;
  }
}

async function SelectUTFDateRange() {

  //var today = new Date();
  //kony.automation.calendar.selectDate(["frmConsolidatedStatements","calFromDate"], [(today.getDate()-2),(today.getMonth()+1),today.getFullYear()]);
  kony.automation.calendar.selectDate([kony.automation.getCurrentForm(),"unifiedTransfers","calStartDate"], [4,25,2021]);
  kony.automation.calendar.selectDate([kony.automation.getCurrentForm(),"unifiedTransfers","calEndDate"], [9,25,2021]);
  appLog("Successfully Selected DateRange");
}

async function SelectUTFSendOnDate() {

  kony.automation.calendar.selectDate([kony.automation.getCurrentForm(),"unifiedTransfers","calStartDate"], [4,25,2021]);
  appLog("Successfully Selected SendOn Date");
}

async function SelectUTFOccurences(){

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxTransferDurationTypeDropdown"],15000);
  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxTransferDurationTypeDropdown"]);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","segTransferDurationList"],15000);
  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segTransferDurationList[1]","flxDropdownRecord"]);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","txtBoxRecurrences"],15000);
  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","txtBoxRecurrences"],"2");
  appLog("Successfully Selected Occurences");
}

async function EnterUTFNoteValue(notes){

  kony.automation.textarea.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","txtAreaNotes"],notes);
  appLog("Successfully entered note value : "+notes);

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","btnContinue"],15000);
  var isEnable=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"unifiedTransfers","btnContinue"],"enable");
  
  if(isEnable){
    await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"unifiedTransfers","btnContinue"]);
    kony.automation.button.click([kony.automation.getCurrentForm(),"unifiedTransfers","btnContinue"]);
    appLog("Successfully Clicked on CONTINUE button");
    await kony.automation.playback.wait(5000);
    await kony.automation.playback.waitForLoadingScreenToDismiss(30000);
    var Status=false;
    if(await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000)){
      Status=true;
      appLog('Custom Message : Successfully moved to frmConfirmEuro Screen');
    }else if(await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","txtErrormessage"],5000)){
      Status=false;
      appLog('Custom Message : Failed with rtxMakeTransferError');
      fail('Custom Message :'+kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"unifiedTransfers","txtErrormessage"],"text"));
    }else{
      appLog('Custom Message : Failed to Navigate to frmConfirmEuro Screen');
    }
    expect(Status).toBe(true,"Failed to Navigate to Confirmation Sccreen");
  }else{
    appLog("CONTINUE button is not enabled");
  }
}     

async function clickonUTFConfirmBtn(){

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000);
  await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"]);
  kony.automation.button.click([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"]);
  appLog("Successfully Clicked on CONFIRM button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(30000);
}

async function clickonCancelbutton(){

  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction1"],15000);
  kony.automation.button.click([kony.automation.getCurrentForm(),"confirmTransfer","btnAction1"]);
  appLog("Successfully Clicked on CANCEL button");
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"btnYes"],15000);
  kony.automation.button.click([kony.automation.getCurrentForm(),"btnYes"]);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  appLog("Successfully Clicked on YES button");
}

async function VerifyCurrencyField_Domestic(){

  appLog("Intiated method to verify currency field- Domestic");
  var currency=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","lblValue8"],15000);
  expect(currency).toBe(true,"Failed to verify currency field");
}
async function VerifyCurrencyField_International(){

  appLog("Intiated method to verify currency field - International");
  var currency=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","lblValue6"],15000);
  expect(currency).toBe(true,"Failed to verify currency field");
}

async function VerifyCurrencyField_SameBank(){

  appLog("Intiated method to verify currency field - Samebank");
  var currency=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","lblValue4"],15000);
  expect(currency).toBe(true,"Failed to verify currency field");
}

async function VerifyCurrencyField_PayAPerson(){

  appLog("Intiated method to verify currency field - PayAPerson");
  var currency=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","lblValue6"],15000);
  expect(currency).toBe(true,"Failed to verify currency field");
}

async function VerifyUTFTransferSuccessMsg(){

  var SuccessfrmName=kony.automation.getCurrentForm();
  appLog("Intiated method to Verify Transfer SuccessMessage "+SuccessfrmName);
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"TransferAcknowledgement","lblSection1Message"],15000);
  expect(kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"TransferAcknowledgement","lblSection1Message"], "text")).not.toBe("");
  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.waitForLoadingScreenToDismiss(30000);
  appLog("Successfully moved back to Accounts DashBoard");
}
