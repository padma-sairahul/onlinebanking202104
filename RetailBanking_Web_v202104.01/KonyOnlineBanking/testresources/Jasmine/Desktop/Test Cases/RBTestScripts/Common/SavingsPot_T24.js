async function navigateToSavingPot(){

  appLog("Intiated method to Navigate SavingPot");
  await kony.automation.playback.waitFor(["frmAccountsDetails","quicklinks","flxRow1"],15000);
  kony.automation.flexcontainer.click(["frmAccountsDetails","quicklinks","flxRow1"]);
  await kony.automation.playback.wait(10000);
  var Status=await kony.automation.playback.waitFor(["frmSavingsPotLanding","lblMySavingsPot"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmSavingsPotLanding","lblMySavingsPot"], "text")).toEqual("My Savings Pot");
  expect(Status).toBe(true,"Failed to navigate to My Savings Pot");

}

async function clickOnCreateNewSavingPot(){

  appLog("Intiated method to click on new Savings Pot link");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","flxAction"],15000);
  kony.automation.flexcontainer.click(["frmSavingsPotLanding","flxAction"]);
  await kony.automation.playback.wait(5000);
  var Status=await kony.automation.playback.waitFor(["frmCreateSavingsPot","lblTransfers"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmCreateSavingsPot","lblTransfers"], "text")).toEqual("New Savings Pot");
  expect(Status).toBe(true,"Failed to click on New Savings Pot Link");

}

async function CreateNewGoal(){

  appLog("Intiated method to create new Goal");
  await kony.automation.playback.waitFor(["frmCreateSavingsPot","btnCreateGoal"],15000);
  kony.automation.button.click(["frmCreateSavingsPot","btnCreateGoal"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on new Goal button");
  await kony.automation.playback.waitFor(["frmCreateSavingsGoal","tbxGoalName"],15000);
  kony.automation.textbox.enterText(["frmCreateSavingsGoal","tbxGoalName"],"My Goal"+getRandomString(5));
  appLog("Successfully Entered new Goal name");
  await kony.automation.playback.waitFor(["frmCreateSavingsGoal","tbxGoalAmount"],15000);
  kony.automation.textbox.enterText(["frmCreateSavingsGoal","tbxGoalAmount"],"500");
  appLog("Successfully Entered new Goal amount");
  kony.automation.slider.slide(["frmCreateSavingsGoal","slider1","sldSlider"], 50);
  await kony.automation.playback.waitFor(["frmCreateSavingsGoal","btnContinue"],15000);
  kony.automation.button.click(["frmCreateSavingsGoal","btnContinue"]);
  appLog("Successfully clicked on continue button");
  await kony.automation.playback.waitFor(["frmCreateGoalConfirm","lblTransfers"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCreateGoalConfirm","lblTransfers"], "text")).toContain("Confirmation");
  await kony.automation.playback.waitFor(["frmCreateGoalConfirm","confirmation","btnConfirm"],15000);
  await kony.automation.scrollToWidget(["frmCreateGoalConfirm","confirmation","btnConfirm"]);
  kony.automation.button.click(["frmCreateGoalConfirm","confirmation","btnConfirm"]);
  appLog("Successfully clicked on Confirm button");
  await kony.automation.playback.waitFor(["frmCreateGoalAck","acknowledgment","lblSuccessMessage"],30000);
  //expect(kony.automation.widget.getWidgetProperty(["frmCreateGoalAck","acknowledgment","lblSuccessMessage"], "text")).toEqual("Success");
  await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
  await kony.automation.playback.wait(5000);

}

async function EditMyGoal(){

  appLog("Intiated method to Edit Goal");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction2"],15000);
  kony.automation.button.click(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction2"]);
  await kony.automation.playback.waitFor(["frmEditGoal","tbxGoalName"],15000);
  kony.automation.textbox.enterText(["frmEditGoal","tbxGoalName"],"Update Goal"+getRandomString(5));
  appLog("Succesfully Updated Goal name");
  await kony.automation.playback.waitFor(["frmEditGoal","btnContinue"],15000);
  kony.automation.button.click(["frmEditGoal","btnContinue"]);
  appLog("Successfully clicked on continue button");
  await kony.automation.playback.waitFor(["frmCreateGoalConfirm","lblTransfers"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCreateGoalConfirm","lblTransfers"], "text")).toContain("Edit");
  await kony.automation.playback.waitFor(["frmCreateGoalConfirm","confirmation","btnConfirm"],15000);
  await kony.automation.scrollToWidget(["frmCreateGoalConfirm","confirmation","btnConfirm"]);
  kony.automation.button.click(["frmCreateGoalConfirm","confirmation","btnConfirm"]);
  appLog("Successfully clicked on Confirm button");
  await kony.automation.playback.waitFor(["frmCreateGoalAck","acknowledgment","lblSuccessMessage"],30000);
  //expect(kony.automation.widget.getWidgetProperty(["frmCreateGoalAck","acknowledgment","lblSuccessMessage"], "text")).toEqual("Success");
  await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
  await kony.automation.playback.wait(5000);
}

async function closeMyGoal(){

  appLog("Intiated method to Close Goal");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction4"],15000);
  kony.automation.button.click(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction4"]);
  //await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on CLOSE button");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","deletePopup","btnYes"],15000);
  kony.automation.button.click(["frmSavingsPotLanding","deletePopup","btnYes"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on YES button");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","lblGoalClosedSuccess"],30000);
  expect(kony.automation.widget.getWidgetProperty(["frmSavingsPotLanding","lblGoalClosedSuccess"], "text")).toContain("closed");
  appLog("Successfully Closed My Goal");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
  await kony.automation.playback.wait(5000);
}

async function CreateNewBudget(){

  appLog("Intiated method to create new Budget");
  await kony.automation.playback.waitFor(["frmCreateSavingsPot","btnCreateBudget"]);
  kony.automation.button.click(["frmCreateSavingsPot","btnCreateBudget"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on new Budget button");
  //await kony.automation.playback.waitFor(["frmCreateBudget","lblContentHeader"]);
  //expect(kony.automation.widget.getWidgetProperty(["frmCreateBudget","lblContentHeader"], "text")).toContain("Create");
  await kony.automation.playback.waitFor(["frmCreateBudget","tbxbudgetName"]);
  kony.automation.textbox.enterText(["frmCreateBudget","tbxbudgetName"],"My Budget"+getRandomString(5));
  appLog("Successfully Entered Budget name");
  await kony.automation.playback.waitFor(["frmCreateBudget","tbxAmount"]);
  kony.automation.textbox.enterText(["frmCreateBudget","tbxAmount"],"500");
  appLog("Successfully Entered Budget Amount");
  await kony.automation.playback.waitFor(["frmCreateBudget","btnContinue"]);
  kony.automation.button.click(["frmCreateBudget","btnContinue"]);
  appLog("Successfully Clicked on Continue Button");
  await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","lblTransfers"]);
  expect(kony.automation.widget.getWidgetProperty(["frmCreateBudgetConfirm","lblTransfers"], "text")).toContain("Confirmation");
  await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","confirmation","btnConfirm"]);
  kony.automation.button.click(["frmCreateBudgetConfirm","confirmation","btnConfirm"]);
  appLog("Successfully Clicked on Confirm Button");
  await kony.automation.playback.waitFor(["frmCreateBudgetAck","acknowledgment","lblSuccessMessage"]);
  //expect(kony.automation.widget.getWidgetProperty(["frmCreateBudgetAck","acknowledgment","lblSuccessMessage"], "text")).toContain("Success");
  await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
  await kony.automation.playback.wait(5000);
}

async function EditMyBudget(){

  appLog("Intiated method to Edit Budget");

  await kony.automation.playback.waitFor(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction2"]);
  kony.automation.button.click(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction2"]);
  appLog("Successfully clicked on edit button");
  await kony.automation.playback.waitFor(["frmEditBudget","tbxbudgetName"]);
  kony.automation.textbox.enterText(["frmEditBudget","tbxbudgetName"],"Updated My Budget"+getRandomString(5));
  appLog("Successfully Updated budget name");
  await kony.automation.playback.waitFor(["frmEditBudget","btnContinue"]);
  kony.automation.button.click(["frmEditBudget","btnContinue"]);
  appLog("Successfully clicked on continue button");
  await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","lblTransfers"]);
  expect(kony.automation.widget.getWidgetProperty(["frmCreateBudgetConfirm","lblTransfers"], "text")).toContain("Edit");
  await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","confirmation","btnConfirm"]);
  kony.automation.button.click(["frmCreateBudgetConfirm","confirmation","btnConfirm"]);
  appLog("Successfully clicked on Confirm button");
  await kony.automation.playback.waitFor(["frmCreateBudgetAck","acknowledgment","lblSuccessMessage"]);
  //expect(kony.automation.widget.getWidgetProperty(["frmCreateBudgetAck","acknowledgment","lblSuccessMessage"], "text")).toContain("Success");
  await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);

}

async function closeMyBudget(){

  appLog("Intiated method to Close Budget");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction4"]);
  kony.automation.button.click(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction4"]);
  //await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on CLOSE button");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","deletePopup","btnYes"]);
  kony.automation.button.click(["frmSavingsPotLanding","deletePopup","btnYes"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on YES button");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","lblGoalClosedSuccess"]);
  expect(kony.automation.widget.getWidgetProperty(["frmSavingsPotLanding","lblGoalClosedSuccess"], "text")).toContain("closed");
  appLog("Successfully Closed My Budget");
  await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
  await kony.automation.playback.wait(5000);
}