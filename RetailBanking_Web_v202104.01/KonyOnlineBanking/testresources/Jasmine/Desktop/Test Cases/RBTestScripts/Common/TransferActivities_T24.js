async function navigateToManageTranscations(){

  appLog("Intiated method to Navigate ManageTranscation Screen");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
  kony.automation.widget.touch(["frmDashboard","customheader","topmenu","flxTransfersAndPay"], [118,39],null,null);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxWireMoney"]);
  //Need loader to be disbale to load segment data
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  //await kony.automation.playback.wait(15000);

  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","lblManagePayments"],30000);
  expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","lblManagePayments"], "text")).toEqual("Transfer Activities");
  appLog("Successfully Navigated to Transfer Activities Screen");

}

async function ClickonTransfersTab(){

  appLog("Intiated method to click on Transfers tab");

  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","tabs","btnTab1"],15000);
  kony.automation.button.click(["frmPastPaymentsEurNew","tabs","btnTab1"]);
  //Need loader to be disbale to load segment data
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  //await kony.automation.playback.wait(15000);

  appLog("Successfully clicked on Transfers Tab");
  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","lblSelectedFilterValue"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","SearchAndFilter","lblSelectedFilterValue"], "text")).not.toBe("");
}

async function ClickonRecurringTab(){

  appLog("Intiated method to click on Recurrences tab");

  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","tabs","btnTab2"],15000);
  kony.automation.button.click(["frmPastPaymentsEurNew","tabs","btnTab2"]);
  //Need loader to be disbale to load segment data
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  //await kony.automation.playback.wait(15000);

  appLog("Successfully clicked on Recurrences Tab");
  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","SearchAndFilter","lblSelectedFilterValue"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmScheduledPaymentsEurNew","SearchAndFilter","lblSelectedFilterValue"], "text")).not.toBe("");
}

async function VerifyAllTransfersList(){

  var AllTransfersStatus=false;

  var AllTransfersList=await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","segmentTransfers"],15000);
  if(AllTransfersList){
    appLog("Successfully found Transcation List");
    AllTransfersStatus=true;
  }else{
    //MoveBack to DashBoard
    await MoveBackFrom_PastTransferActivities();
    appLog("Custom Message :: No records found.");
    fail("Custom Message :: No records found.");
  }

  return AllTransfersStatus;
}

async function VerifyStandardOrdersList(){

  var StandardStatus=false;

  var StandardOrderList=await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","segmentTransfers"],15000);
  if(StandardOrderList){
    appLog("Successfully found Transcation List");
    StandardStatus=true;
  }else{
    //MoveBack to DashBoard
    await MoveBackFrom_SheduledTransferActivities();
    appLog("Custom Message :: No records found.");
    fail("Custom Message :: No records found.");
  }

  return StandardStatus;

}

async function VerifyNewStatusColumn_AllTransfers(){

  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","lblColumn4"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","List","lblColumn4"], "text")).not.toBe("");
}

async function VerifyNewStatusColumn_StandardOrders(){

  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","lblColumn4"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmScheduledPaymentsEurNew","List","lblColumn4"], "text")).not.toBe("");
}

async function clickOnAllTransfersFilter(){

  var AllList=await VerifyAllTransfersList();

  if(AllList){
    appLog("Intiated method to click on Transers Filter");
    await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","flxDropdown"],15000);
    kony.automation.widget.touch(["frmPastPaymentsEurNew","SearchAndFilter","flxDropdown"], [43,22],null,null);
    kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","SearchAndFilter","flxDropdown"]);
    appLog("Successfully click on Transers Filter");
  }else{
    appLog("Custom Message : No Activities List is available");
  }

}

async function clickOnStandardOrdersFilter(){

  var StandardList=await VerifyStandardOrdersList();
  if(StandardList){
    appLog("Intiated method to click on Standard Order Filter");
    await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","SearchAndFilter","flxDropdown"],15000);
    kony.automation.widget.touch(["frmScheduledPaymentsEurNew","SearchAndFilter","flxDropdown"], [43,22],null,null);
    kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","SearchAndFilter","flxDropdown"]);
    appLog("Successfully clicked on Standard Order Filter");
  }else{
    appLog("Custom Message : No Activities List is available");
  }
}

async function selectCompletedTransfers(){

  await clickOnAllTransfersFilter();

  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","segFilter"],15000);
  kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","SearchAndFilter","segFilter[5]","flxRadioBtn"]);
  await kony.automation.playback.wait(10000);
  appLog("Successfully Selected Completed Radio");

  //await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","segmentTransfers"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","List","segmentTransfers[0]","lblColumn4"], "text")).toEqual("Completed");

}

async function selectSheduledTransfers(){

  await clickOnAllTransfersFilter();

  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","segFilter"],15000);
  kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","SearchAndFilter","segFilter[4]","flxRadioBtn"]);
  await kony.automation.playback.wait(10000);
  appLog("Successfully Selected Sheduled Radio");

  //await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","segmentTransfers"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","List","segmentTransfers[0]","lblColumn4"], "text")).toEqual("Scheduled");

}

async function selectActiveOrders(){

  await clickOnStandardOrdersFilter();

  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","SearchAndFilter","segFilter"],15000);
  kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","SearchAndFilter","segFilter[1]","flxRadioBtn"]);
  await kony.automation.playback.wait(10000);
  appLog("Successfully Selected Active Radio");

  //await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","segmentTransfers"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmScheduledPaymentsEurNew","List","segmentTransfers[0]","lblColumn4"], "text")).toEqual("Active");

}

async function SearchforPastTransferActivities(keyword){

  appLog("Intiated method to search for keyword : <b>"+keyword+"</b>");

  var PastTransfers=false;

  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmPastPaymentsEurNew","SearchAndFilter","txtSearch"],keyword);
  kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","SearchAndFilter","flxSearchBtn"]);
  //await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
  appLog("Successfully clicked on Search button");

  var ActivityList=await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","segmentTransfers"],30000);
  if(ActivityList){
    appLog("Successfully found Transcation with keyword : <b>"+keyword+"</b>");
    PastTransfers=true;
    //kony.automation.widget.touch(["frmPastPaymentsEurNew","List","segmentTransfers[0]","imgDropdown"], null,null,[9,8]);
    kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","List","segmentTransfers[0]","flxDropdown"]);
  }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","rtxNoPaymentMessage"],5000)){
    appLog("Custom Message :: Failed with rtxNoPaymentMessage");
  }else{
    appLog("Custom Message :: Failed to Search Transcation from List");
    //fail("Custom Message :: Failed to Search Transcation from List");
  }

  return PastTransfers;
}

async function SearchforSheduledTransferActivities(keyword){

  appLog("Intiated method to search for keyword : <b>"+keyword+"</b>");

  var SheduledTransfers=false;

  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","SearchAndFilter","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmScheduledPaymentsEurNew","SearchAndFilter","txtSearch"],keyword);
  kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","SearchAndFilter","flxSearchBtn"]);
  //await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
  appLog("Successfully clicked on Search button");

  var ActivityList=await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","segmentTransfers"],30000);
  if(ActivityList){
    appLog("Successfully found Transcation with keyword : <b>"+keyword+"</b>");
    SheduledTransfers=true;
    kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","List","segmentTransfers[0]","flxDropdown"]);
  }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","rtxNoPaymentMessage"],5000)){
    appLog("Custom Message :: Failed with rtxNoPaymentMessage");
  }else{
    appLog("Custom Message :: Failed to Search Transcation from List");
    //fail("Custom Message :: Failed to Search Transcation from List");
  }

  return SheduledTransfers;
}

async function clickOnRepeatButton(keyword){

  var Repeat=await SearchforPastTransferActivities(keyword);
  if(Repeat){
    appLog("Intiated method to click on Repeat button");
    kony.automation.button.click(["frmPastPaymentsEurNew","List","segmentTransfers[0]","btnAction"]);
    await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
    //await kony.automation.playback.wait(10000);
    appLog("Successfully clicked on Repeat button");

    // Add required Details - It was supposed to Populate all details. Some times not displaying details.

    //     await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],15000);
    //     var HaderText=kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text");
    //     appLog("Header text of Payment/Transfer screen is : "+HaderText);
    //     if(HaderText==='Payments'){
    //       appLog("Repeating Payment");
    //       await SelectToAccount(keyword);
    //     }else if(HaderText==='Transfers'){
    //       appLog("Repeating Transfers");
    //       await SelectOwnTransferToAccount(keyword);
    //     }else{
    //       appLog("Failed to verify Payment/Transfer screen text : "+HaderText);
    //     }

    //await EnterNoteValue("Verify Repeat Functionality");
    //await ConfirmTransfer();
    //await VerifyTransferSuccessMessage();
    await MoveBackToLandingScreen_Transfers();
  }else{
    appLog("Custom Message :: No Series available to Repeat");
    await MoveBackFrom_PastTransferActivities();
  }

}

async function clickOnEditButton(keyword){

  var Edit=await SearchforSheduledTransferActivities(keyword);
  if(Edit){
    appLog("Intiated method to Edit Transfer");
    kony.automation.button.click(["frmScheduledPaymentsEurNew","List","segmentTransfers[0]","btnAction"]);
    //await kony.automation.playback.wait(10000);
    await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
    appLog("Successfully clicked on Edit button");

    // Add required Details
    //await EnterNoteValue("EditRecurringFunctionality");
    //await ConfirmTransfer();
    //await VerifyTransferSuccessMessage();
    await MoveBackToLandingScreen_Transfers();
  }else{
    appLog("Custom Message :: No Series available to Edit");
    await MoveBackFrom_SheduledTransferActivities();
  }
}

async function clickOnCancelSeriesButton(keyword){

  var Series=await SearchforSheduledTransferActivities(keyword);
  if(Series){
    appLog("Intiated method to cancel Series Recurring Transfer");
    kony.automation.button.click(["frmScheduledPaymentsEurNew","List","segmentTransfers[0]","btn1"]);
    kony.automation.button.click(["frmScheduledPaymentsEurNew","btnYesIC"]);
    appLog("Successfully clicked on CANCEL button");
    //await kony.automation.playback.wait(10000);
    await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
    var Success=await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","lblSuccessAcknowledgement"],45000);
    if(Success){
      expect(kony.automation.widget.getWidgetProperty(["frmScheduledPaymentsEurNew","lblSuccessAcknowledgement"], "text")).not.toBe("");
      await MoveBackFrom_SheduledTransferActivities();
    }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","rtxDowntimeWarning"],5000)){
      appLog("Custom Message :: Failed with rtxDowntimeWarning");
      fail("Faild :: Failed with rtxDowntimeWarning");
      await MoveBackFrom_SheduledTransferActivities();
    }else{
      appLog("Custom Message :: Failed CANCEL Series");
      fail("Custom Message :: Failed CANCEL Series");
      await MoveBackFrom_SheduledTransferActivities();
    }

  }else{
    appLog("Custom Message :: No Series available to cancel");
    await MoveBackFrom_SheduledTransferActivities();
  }

}

async function clickOnCancelTransferButton(keyword){

  var Series=await SearchforPastTransferActivities(keyword);
  if(Series){
    appLog("Intiated method to cancel Sheduled Transfer");
    kony.automation.button.click(["frmPastPaymentsEurNew","List","segmentTransfers[0]","btn1"]);
    kony.automation.button.click(["frmPastPaymentsEurNew","btnYesIC"]);
    appLog("Successfully clicked on CANCEL button");
    //await kony.automation.playback.wait(10000);
    await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
    var Success=await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","lblSuccessAcknowledgement"],30000);
    if(Success){
      expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","lblSuccessAcknowledgement"], "text")).not.toBe("");
      await MoveBackFrom_PastTransferActivities();
    }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","rtxDowntimeWarning"],5000)){
      appLog("Custom Message :: Failed with rtxDowntimeWarning");
      fail("Faild :: Failed with rtxDowntimeWarning");
      await MoveBackFrom_PastTransferActivities();
    }else{
      appLog("Custom Message :: Failed CANCEL Transfer");
      fail("Custom Message :: Failed CANCEL Transfer");
      await MoveBackFrom_PastTransferActivities();
    }
  }else{
    appLog("Custom Message :: No Series available to cancel");
    await MoveBackFrom_PastTransferActivities();
  }

}

async function MoveBackFrom_PastTransferActivities(){

  appLog("Intiated method to moveback from PastTransferActivities");

  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
}

async function MoveBackFrom_SheduledTransferActivities(){

  appLog("Intiated method to moveback from SheduledTransferActivities");
  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
}