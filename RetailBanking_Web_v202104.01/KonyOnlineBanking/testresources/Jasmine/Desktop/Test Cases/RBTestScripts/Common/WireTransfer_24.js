async function NavigateToWireTransfer_AddRecipitent(){

  appLog("Intiated method to Navigate AddRecipitent Screen");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"]);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","lblAddAccountHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","lblAddAccountHeading"], "text")).not.toBe("");

  appLog("successfully Navigated to AddRecipitent Screen");

}

async function EnterDomestic_RecipitentDetails_Step1(RecipientName,AddressLine1,AddressLine2,City,ZipCode){

  appLog("Intiated method to Enter Domestic Recipitent details - Step1");

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"],RecipientName);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxAddressLine1"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxAddressLine1"],AddressLine1);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxAddressLine2"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxAddressLine2"],AddressLine2);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxCity"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxCity"],City);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","lbxState"],15000);
  kony.automation.listbox.selectItem(["frmWireTransferAddKonyAccountStep1","lbxState"], "Dubai");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxZipcode"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxZipcode"],ZipCode);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","btnProceed"],15000);
  kony.automation.button.click(["frmWireTransferAddKonyAccountStep1","btnProceed"]);

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","lblStep2"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep2","lblStep2"], "text")).toContain("Step 2");

  appLog("Successfully Entered Domestic Recipitent details - Step1");
}

async function EnterDomestic_BankDetails_Step2(RoutingNumber,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){

  appLog("Intiated method to Enter Domestic Bank details - Step2");

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxSwiftCode"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxSwiftCode"],RoutingNumber);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxAccountNumber"],AccNumber);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxReAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxReAccountNumber"],AccNumber);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxNickName"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxNickName"],NickName);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankName"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankName"],BankName);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine1"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine1"],BankAddressLine1);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine2"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine2"],BankAddressLine2);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankCity"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankCity"],BankCity);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","lbxBankState"],15000);
  kony.automation.listbox.selectItem(["frmWireTransferAddKonyAccountStep2","lbxBankState"], "Dubai");
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankZipcode"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankZipcode"],BankZipcode);
  await kony.automation.scrollToWidget(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"]);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"],15000);
  kony.automation.button.click(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"]);

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","lblAddAccountHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountConfirm","lblAddAccountHeading"], "text")).not.toBe("");

  appLog("Successfully Entered Domestic Bank details - Step2");

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","btnConfirm"],15000);
  kony.automation.button.click(["frmWireTransferAddKonyAccountConfirm","btnConfirm"]);

  appLog("Successfully Clicked on Confirm Button");

}

async function VerifyAddDomesticRecipitentSuccessMsg(){

  appLog("Intiated method to verify AddDomesticRecipitent SuccessMsg");

  var Success=await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","lblSuccessAcknowledgement"],15000);
  if(Success){
    await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","rtxDowntimeWarning"],15000)){
    //expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","rtxDowntimeWarning"], "text")).toEqual("SOME");
    await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);

    appLog("Failed with : rtxDowntimeWarning");
    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","rtxDowntimeWarning"], "text"));
  }else{
    // This is the condition for use cases where it won't throw error on UI but struck at same screen
    appLog("Unable to add Recipitent");
    fail("Unable to add Recipitent");
  }
}

async function EnterInternational_RecipitentDetails_Step1(RecipientName,AddressLine1,AddressLine2,City,ZipCode){

  appLog("Intiated method to Enter International Recipitent details - Step1");

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","btnInternationalAccount"],15000);
  kony.automation.button.click(["frmWireTransferAddKonyAccountStep1","btnInternationalAccount"]);
  await kony.automation.playback.wait(10000);
  var Status=await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","lbxCountry"],15000);
  expect(Status).toBe(true,"Failed to Navigate to International recipitent")
  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep1","lbxCountry"], "AU");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxRecipientName"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxRecipientName"],RecipientName);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine1"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine1"],AddressLine1);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine2"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine2"],AddressLine2);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxCity"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxCity"],City);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","lbxState"],15000);
  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep1","lbxState"], "Independencia");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxZipcode"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxZipcode"],ZipCode);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","btnProceed"],15000);
  kony.automation.button.click(["frmWireTransferAddInternationalAccountStep1","btnProceed"]);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","lblStep2"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep2","lblStep2"], "text")).toContain("Step 2");

  appLog("Successfully Entered International Recipitent details - Step1");
}

async function EnterInternational_BankDetails_Step2(SwiftCode,IBAN,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){

  appLog("Intiated method to Enter International Bank details - Step2");

  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxSwiftCode"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxSwiftCode"],SwiftCode);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxIBANOrIRC"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxIBANOrIRC"],IBAN);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxAccountNumber"],AccNumber);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxReAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxReAccountNumber"],AccNumber);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxNickName"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxNickName"],NickName);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankName"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankName"],BankName);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine1"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine1"],BankAddressLine1);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine2"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine2"],BankAddressLine2);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankCity"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankCity"],BankCity);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","lbxBankState"],15000);
  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep2","lbxBankState"], "Independencia");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankZipcode"],15000);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankZipcode"],BankZipcode);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","btnAddRecipent"],15000);
  kony.automation.button.click(["frmWireTransferAddInternationalAccountStep2","btnAddRecipent"]);

  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","lblAddAccountHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountConfirm","lblAddAccountHeading"], "text")).not.toBe("");

  appLog("Successfully Entered International Bank details - Step1");

  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","btnConfirm"],15000);
  kony.automation.button.click(["frmWireTransferAddInternationalAccountConfirm","btnConfirm"]);

  appLog("Successfully Clicked on Confirm Button");

}


async function VerifyAddInternationalRecipitentSuccessMsg(){

  appLog("Intiated method to verify AddInternationalRecipitentSuccessMsg SuccessMsg");

  var Success=await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","lblSuccessAcknowledgement"],15000);
  if(Success){
    await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
  }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","rtxDowntimeWarning"],15000)){
    //expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep1","rtxDowntimeWarning"], "text")).toEqual("SOME");
    await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);

    appLog("Failed with : rtxDowntimeWarning");
    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep1","rtxDowntimeWarning"], "text"));
  }else{
    // This is the condition for use cases where it won't throw error on UI but struck at same screen
    appLog("Unable to add Recipitent");
    fail("Unable to add Recipitent");
  }
}


async function NavigateToMakeTransfer(){

  appLog("Intiated method to Navigate to Make Transfers");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  //await kony.automation.playback.waitFor(["frmWireTransfersWindow","lblAddAccountHeading"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmWireTransfersWindow","lblAddAccountHeading"], "text")).not.toBe("");
  appLog("Succesfully Navigated to Make Transfers screen");
}

async function ClickOnMakeTransferLink(){

  appLog("Intiated method to click on Make Transfer Link ");
  await kony.automation.playback.waitFor(["frmWireTransfersWindow","segWireTransfers"],15000);
  kony.automation.button.click(["frmWireTransfersWindow","segWireTransfers[0]","btnAction"]);
  await kony.automation.playback.wait(10000);
  //await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","lblAddAccountHeading"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmWireTransferMakeTransfer","lblAddAccountHeading"], "text")).not.toBe("");
  appLog("Successfully clicked on Make Transfer Link ");

}

async function MakeWireTransfer(){

  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","txtTransferFrom"],15000);
  kony.automation.widget.touch(["frmWireTransferMakeTransfer","txtTransferFrom"], [110,12],null,null);
  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","segTransferFrom"],15000);
  kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","segTransferFrom[0,0]","flxAmount"]);
  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","tbxAmount"],15000);
  kony.automation.textbox.enterText(["frmWireTransferMakeTransfer","tbxAmount"],"1.5");
  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","tbxNotes"],15000);
  kony.automation.textbox.enterText(["frmWireTransferMakeTransfer","tbxNotes"],"TEST");
  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","btnStepContinue"],15000);
  kony.automation.button.click(["frmWireTransferMakeTransfer","btnStepContinue"]);
  await kony.automation.playback.wait(5000);
  //await kony.automation.playback.waitFor(["frmConfirmDetails","lblAddAccountHeading"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblAddAccountHeading"], "text")).not.toBe("");

  await kony.automation.playback.waitFor(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"],15000);
  await kony.automation.scrollToWidget(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"]);
  kony.automation.widget.touch(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"], null,null,[16,11]);
  await kony.automation.playback.waitFor(["frmConfirmDetails","flxAgree"],15000);
  kony.automation.flexcontainer.click(["frmConfirmDetails","flxAgree"]);

  await kony.automation.playback.waitFor(["frmConfirmDetails","btnConfirm"],15000);
  kony.automation.button.click(["frmConfirmDetails","btnConfirm"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmConfirmDetails","lblSuccessAcknowledgement"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblSuccessAcknowledgement"], "text")).not.toBe("");
  await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);

}

async function NavigateToWireOneTimePayment(){

  appLog("Intiated method to navigate to OneTime WireTransfer");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer4flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer4flxMyAccounts"]);
  await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","lblAddAccountHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferOneTimePaymentStep1","lblAddAccountHeading"], "text")).not.toBe("");

  appLog("Successfully navigated to OneTime WireTransfer");

}
async function EnterOneTimeDomesticRecipitentDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode){

  appLog("Intiated method to Enter OneTime-DomesticRecipitentDetails- Step1");

  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxRecipientName"],15000);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxRecipientName"],RecipientName);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxAddressLine1"],15000);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxAddressLine1"],AddressLine1);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxAddressLine2"],15000);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxAddressLine2"],AddressLine2);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxCity"],15000);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxCity"],City);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","lbxState"],15000);
  kony.automation.listbox.selectItem(["frmWireTransferOneTimePaymentStep1","lbxState"], "Independencia");
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxZipcode"],15000);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxZipcode"],ZipCode);

  appLog("Successfully Entered OneTime-DomesticRecipitentDetails- Step1");

  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","btnProceed"],15000);
  await kony.automation.scrollToWidget(["frmWireTransferOneTimePaymentStep1","btnProceed"]);
  kony.automation.button.click(["frmWireTransferOneTimePaymentStep1","btnProceed"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on Proceed Button");
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","lblStep2"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferOneTimePaymentStep2","lblStep2"], "text")).not.toBe("");
  appLog("Successfully Verified Step2 screen");
}

async function EnterOneTimeDomesticBankDetails(SwiftCode,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){

  appLog("Intiated method to enter OneTime-DomesticBankDetails-Step2");

  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxSwiftCode"]);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxSwiftCode"],SwiftCode);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxAccountNumber"]);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxAccountNumber"],AccNumber);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxReAccountNumber"]);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxReAccountNumber"],AccNumber);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxNickName"]);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxNickName"],NickName);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankName"]);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankName"],BankName);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine1"]);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine1"],BankAddressLine1);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine2"]);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine2"],BankAddressLine2);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankCity"]);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankCity"],BankCity);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","lbxBankState"]);
  kony.automation.listbox.selectItem(["frmWireTransferOneTimePaymentStep2","lbxBankState"], "Indiana");
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankZipcode"]);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankZipcode"],BankZipcode);

  appLog("Successfully Entered OneTime-DomesticRecipitentDetails- Step1");

  await kony.automation.scrollToWidget(["frmWireTransferOneTimePaymentStep2","btnStep2Proceed"]);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","btnStep2Proceed"]);
  kony.automation.button.click(["frmWireTransferOneTimePaymentStep2","btnStep2Proceed"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on Proceed Button");
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","lblAddAccountHeading"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferOneTimePaymentStep3","lblStep3"], "text")).not.toBe("");
  appLog("Successfully Verified Step3 screen");
}

async function MakeOneTimeWireTransfer(){

  appLog("Intiated method to Make-OneTimeTransfer");

  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","txtTransferFrom"],15000);
  kony.automation.widget.touch(["frmWireTransferOneTimePaymentStep3","txtTransferFrom"], [140,21],null,null);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","segTransferFrom"],15000);
  kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","segTransferFrom[0,0]","flxAmount"]);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","tbxAmount"],15000);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep3","tbxAmount"],"1.5");
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","tbxNote"],15000);
  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep3","tbxNote"],"OneTimePayment");
  await kony.automation.scrollToWidget(["frmWireTransferOneTimePaymentStep3","btnStep3MakeTransfer"]);
  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","btnStep3MakeTransfer"],15000);
  kony.automation.button.click(["frmWireTransferOneTimePaymentStep3","btnStep3MakeTransfer"]);
  appLog("Successfully clicked on Make-OneTimeTransfer button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmConfirmDetails","lblAddAccountHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblAddAccountHeading"], "text")).not.toBe("");
  appLog("Successfully clicked on Make-OneTimeTransfer button");

  await ConfirmDomesticOneTimeWireTransfer();

}

async function ConfirmDomesticOneTimeWireTransfer(){

  await kony.automation.playback.waitFor(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"],15000);
  kony.automation.widget.touch(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"], null,null,[14,16]);
  await kony.automation.playback.waitFor(["frmConfirmDetails","flxAgree"],15000);
  kony.automation.flexcontainer.click(["frmConfirmDetails","flxAgree"]);
  appLog("Successfully Accepted Terms and conditions");

  await kony.automation.playback.waitFor(["frmConfirmDetails","btnConfirm"],15000);
  kony.automation.button.click(["frmConfirmDetails","btnConfirm"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on confirm button");

}

async function VerifyOneTimeDomesticWireTransferSuccessMsg(){

  var success=await kony.automation.playback.waitFor(["frmConfirmDetails","lblSuccessAcknowledgement"],15000);
  if(success){
    //expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblSuccessAcknowledgement"], "text")).not.toBe("");
    appLog("Successfully Verified Onetime Wire Transfer Success Message");
    await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
    appLog("Successfully Moved back to Accounts Dashboard");
  }else{
    appLog("Unable to Perform OneTimeDomestic WireTransfers");
    fail("Unable to Perform OneTimeDomestic WireTransfers");

  }

}

async function NavigatetoCreateNewTemplate(){

  appLog("Intiated method to navigate to create Template");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer5flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer5flxMyAccounts"]);
  await kony.automation.playback.wait(10000);
  //await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","lblAddAccountHeading"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmCreateTemplatePrimaryDetails","lblAddAccountHeading"], "text")).not.toBe("");

  appLog("Successfully navigated to NewTemplate creation");
}

async function EnterTemplateDetails(TemplateName){

  await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","tbxRecipientName"],15000);
  kony.automation.textbox.enterText(["frmCreateTemplatePrimaryDetails","tbxRecipientName"],TemplateName);
  appLog("Successfully entered template name");
  await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","btnContinue"],15000);
  await kony.automation.scrollToWidget(["frmCreateTemplatePrimaryDetails","btnContinue"]);
  kony.automation.button.click(["frmCreateTemplatePrimaryDetails","btnContinue"]);
  appLog("Successfully cliced on Continue button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","lblHeading"]);
  expect(kony.automation.widget.getWidgetProperty(["frmBulkTemplateAddRecipients","lblHeading"], "text")).not.toBe("");
}

async function selectExistingRecipitentOption(){

  await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","lblExistingRecipients"],15000);
  kony.automation.widget.touch(["frmBulkTemplateAddRecipients","lblExistingRecipients"], null,null,[16,14]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully selected existing recipitent option");
  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","lblAddAccountHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCreateTempSelectRecipients","lblAddAccountHeading"], "text")).not.toBe("");
}

async function CreateNewTemplate_Existingrecipitent(){

  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","lblStatus"],15000);
  kony.automation.widget.touch(["frmCreateTempSelectRecipients","lblStatus"], null,null,[16,14]);
  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","flxSelectAll"],15000);
  kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","flxSelectAll"]);
  appLog("Successfully selected recipitent Select All Button");

  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","btnContinue"],15000);
  kony.automation.button.click(["frmCreateTempSelectRecipients","btnContinue"]);
  appLog("Successfully selected Continue button");

  await VerifyNewTemplateSuccessMsg();

}

async function selectNewManualRecipitentOption(){

  await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","lblManualRecipients"],15000);
  kony.automation.widget.touch(["frmBulkTemplateAddRecipients","lblManualRecipients"], null,null,[16,14]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully selected New Manual Recipitent option");
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","lblAddAccountHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCreateTempAddDomestic","lblAddAccountHeading"], "text")).not.toBe("");
}

async function EnterManualRecipitentTemplateDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode,RoutingNumber,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){

  appLog("Intiated method to enter ManualRecipitentTemplateDetails");

  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRecipientName"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRecipientName"],RecipientName);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxAddressLine1"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxAddressLine1"],AddressLine1);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxAddressLine2"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxAddressLine2"],AddressLine2);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxCity"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxCity"],City);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","lbxState"]);
  kony.automation.listbox.selectItem(["frmCreateTempAddDomestic","lbxState"], "Independencia");
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxZipcode"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxZipcode"],ZipCode);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRoutingNumber"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRoutingNumber"],RoutingNumber);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRecipientAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRecipientAccountNumber"],AccNumber);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxReEnterAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxReEnterAccountNumber"],AccNumber);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxAccountNickName"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxAccountNickName"],NickName);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRecipientBankName"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRecipientBankName"],BankName);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxBankAddressLine1"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxBankAddressLine1"],BankAddressLine1);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxBankAddressLIne2"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxBankAddressLIne2"],BankAddressLine2);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxCompanyCity"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxCompanyCity"],BankCity);
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","lbxCompanyState"]);
  kony.automation.listbox.selectItem(["frmCreateTempAddDomestic","lbxCompanyState"], "Independencia");
  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxCompanyZipCode"],15000);
  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxCompanyZipCode"],BankZipcode);

  appLog("Successfully entered ManualRecipitentTemplateDetails");

  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","btnProceed"],15000);
  kony.automation.button.click(["frmCreateTempAddDomestic","btnProceed"]);
  appLog("Successfully Clicked on Proceed button");
}


async function VerifyNewTemplateSuccessMsg(){

  var Success=await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","rtxMakeTransferError"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmBulkTemplateAddRecipients","rtxMakeTransferError"], "text")).not.toBe("");
  if(Success){
    appLog("Successfully created new Template");
    await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
    appLog("Successfully Moved back to Accounts Dashboard");
  }else{
    appLog("Failed to create Template -Existing Recipitent");
    fail("Failed to create Template -Existing Recipitent");
  }

}

async function NavigateToWireHistoryTab(){

  appLog("Intiated method to navigate to History Tab");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer1flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer1flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmWireTransfersRecent","lblAddAccountHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransfersRecent","lblAddAccountHeading"], "text")).not.toBe("");

  appLog("Successfully Navigated to History Tab");
}

async function VerifyWireTransferHistory(){

  appLog("Intiated method to check Transfer History");

  var History=await kony.automation.playback.waitFor(["frmWireTransfersRecent","segWireTransfers"],15000);
  if(History){
    kony.automation.flexcontainer.click(["frmWireTransfersRecent","segWireTransfers[0]","flxDropdown"]);
    appLog("Successfully verified History");
    await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
    appLog("Successfully ovedBack to DashBoard");
  }else{
    appLog("Custom Message : No History availble at this moment");
    await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
  }
}

async function NavigateToWireRecipitentsTab(){

  appLog("Intiated method to WireRecipitents Tab");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer2flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer2flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","lblAddAccountHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransfersManageRecipients","lblAddAccountHeading"], "text")).not.toBe("");

  appLog("Successfully Navigated to WireRecipitents Tab");
}

async function VerifyWireRecipitents(){

  appLog("Intiated method to check wire Recipitents");

  var List=await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","segWireTransfers"],15000);
  if(List){
    kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","segWireTransfers[0]","flxDropdown"]);
    appLog("Successfully verified Wire recipitents");
    await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
    appLog("Successfully ovedBack to DashBoard");
  }else{
    appLog("Custom Message : No Recipitents availble at this moment");
    await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
  }
}


async function ActivateWireTransfersTermsConditions(){

  // First click on Any one of Wire Transfer
  appLog("Intiated method to Navigate to Activate WireTransfer");
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"]);
  await kony.automation.playback.wait(10000);
  appLog("Successfully Navigated to wire transfer screen");

  var Terms=await kony.automation.playback.waitFor(["frmActivateWireTransfer","lblAddAccountHeading"],15000);
  if(Terms){
    appLog("Intiated method to accept terms and conditions");
    await kony.automation.playback.waitFor(["frmActivateWireTransfer","lbxDefaultAccountForSending"],15000);
    await kony.automation.scrollToWidget(["frmActivateWireTransfer","lbxDefaultAccountForSending"]);
    kony.automation.listbox.selectItem(["frmActivateWireTransfer","lbxDefaultAccountForSending"],AllAccounts.Current.accno);
    appLog("Successfully Selected default account number");
    await kony.automation.playback.waitFor(["frmActivateWireTransfer","lblFavoriteEmailCheckBoxMain"],15000);
    kony.automation.widget.touch(["frmActivateWireTransfer","lblFavoriteEmailCheckBoxMain"], null,null,[10,22]);
    await kony.automation.playback.waitFor(["frmActivateWireTransfer","flxCheckbox"],15000);
    kony.automation.flexcontainer.click(["frmActivateWireTransfer","flxCheckbox"]);
    appLog("Successfully accepted Terms and conditions");
    await kony.automation.playback.waitFor(["frmActivateWireTransfer","btnProceed"],15000);
    kony.automation.button.click(["frmActivateWireTransfer","btnProceed"]);
    appLog("Successfully clicked on Proceed button");
    await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
    appLog("Successfully Navigated to Accounts DashBoard");

  }else{
    appLog("WireTransfers is activated already");
    await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
    appLog("Successfully Navigated to Accounts DashBoard");
  }
}

