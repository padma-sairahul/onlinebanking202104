async function navigateToAlerts_Notifications(){

  appLog("Intiated method to navigate to alerts and notifications");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGESflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGESflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGES0flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGES0flxMyAccounts"]);
  //await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  appLog("Successfully navigated to alerts and notifications");
}

async function SearchAlertMessage(){

  appLog("Intiated method to view alerts and notifications");
  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","txtSearch"],"Sign");
  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","lblSearch"],15000);
  kony.automation.widget.touch(["frmNotificationsAndMessages","NotficationsAndMessages","lblSearch"], null,null,[3,15]);
  //await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification"],15000);
  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification[0]","flxNotificationsAndMessages"]);
  appLog("Successfully Verified alerts and notifications");
  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
}

async function DeleteAlertMessage(){

  appLog("Intiated method to DISMISS alerts and notifications");
  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification"],15000);
  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification[0]","flxNotificationsAndMessages"]);
  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnDismiss"],15000);
  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnDismiss"]);
  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","CustomPopup1","btnYes"],15000);
  kony.automation.button.click(["frmNotificationsAndMessages","CustomPopup1","btnYes"]);
  //await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
  appLog("Successfully DISMISS alerts and notifications");
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
}

async function VerifyAlertsCount(){
  
	await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnNotifications"]);
	var AlertsText=kony.automation.widget.getWidgetProperty(["frmNotificationsAndMessages","NotficationsAndMessages","btnNotifications"], "text");
    //parseInt(subaccounts_Size.substring(1, 2));
    return AlertsText;
}

async function VerifyAlertCountIncrementDecrement(){
  
  var Alert_before=await VerifyAlertsCount();
  appLog("Alerts count before is : "+Alert_before);
  await DeleteAlertMessage();
  await navigateToAlerts_Notifications();
  var Alert_After=await VerifyAlertsCount();
  appLog("Alerts count ater is : "+Alert_After);
  expect(Alert_before).not.toBe(Alert_After);
  
}

async function VerifyNotificationDetail(){
  
	await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","lblHeadingNotification"]);
	var details=kony.automation.widget.getWidgetProperty(["frmNotificationsAndMessages","NotficationsAndMessages","lblHeadingNotification"], "text");
    return details;
}