async function verifyAccountsLandingScreen(){

  //await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
}

async function SelectAccountsOnDashBoard(AccountType){

  appLog("Intiated method to analyze accounts data Dashboard");

  var Status=false;

  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
  var segLength=accounts_Size.length;

  var finished = false;
  for(var x = 0; x <segLength && !finished; x++) {

    var segHeaders="segAccounts["+x+",-1]";

    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
    //appLog('Sub accounts size is '+subaccounts_Length);

    for(var y = 0; y <subaccounts_Length; y++){

      var seg="segAccounts["+x+","+y+"]";

      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
      if(typeOfAccount.includes(AccountType)){
        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
        await kony.automation.playback.wait(10000);
        
        finished = true;
        Status=true;
        break;
      }
    }
  }

  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
}

async function clickOnFirstCheckingAccount(){

  appLog("Intiated method to click on First Checking account");
  SelectAccountsOnDashBoard("Checking");
  //appLog("Successfully Clicked on First Checking account");
  //await kony.automation.playback.wait(5000);
}

async function clickOnFirstSavingsAccount(){

  appLog("Intiated method to click on First Savings account");
  SelectAccountsOnDashBoard("Saving");
  //appLog("Successfully Clicked on First Savings account");
  //await kony.automation.playback.wait(5000);
}


async function clickOnFirstCreditCardAccount(){

  appLog("Intiated method to click on First CreditCard account");
  SelectAccountsOnDashBoard("Credit");
  //appLog("Successfully Clicked on First CreditCard account");
  //await kony.automation.playback.wait(5000);
}

async function clickOnFirstDepositAccount(){

  appLog("Intiated method to click on First Deposit account");
  SelectAccountsOnDashBoard("Deposit");
  //appLog("Successfully Clicked on First Deposit account");
  //await kony.automation.playback.wait(5000);
}

async function clickOnFirstLoanAccount(){

  appLog("Intiated method to click on First Loan account");
  SelectAccountsOnDashBoard("Loan");
  //appLog("Successfully Clicked on First Loan account");
  //await kony.automation.playback.wait(5000);
}

async function clickOnFirstAvailableAccount(){

  appLog("Intiated method to click on First available account on DashBoard");

  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
  kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
  appLog("Successfully Clicked on First available account on DashBoard");
  await kony.automation.playback.wait(5000);
}

async function clickOnSearch_AccountDetails(){

  appLog("Intiated method to click on Search Glass Icon");
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
  appLog("Successfully Clicked on Seach Glass Icon");
}

async function selectTranscationtype(TransactionType){

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], TransactionType);
  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
}

async function enterKeywordtoSearch(keyword){

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtKeyword"],15000);
  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtKeyword"],keyword);
  appLog("Successfully entered keyword for search : <b>"+keyword+"</b>");
}

async function selectAmountRange(AmountRange1,AmountRange2){

  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
  appLog("Successfully selected amount range From");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
  appLog("Successfully selected amount range To");

  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");
}

async function selectCustomdate(){

  appLog("Intiated method to select Custom Date Range");
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
  appLog("Successfully selected Date Range");
}

async function selectTimePeriod(){

  appLog("Intiated method to select custom timeperiod");
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
  appLog("Successfully selected Time period");
}

async function clickOnAdvancedSearchBtn(){

  appLog("Intiated method to click on Search Button with given search criteria");
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
  appLog("Successfully clicked on Search button");
  await kony.automation.playback.wait(5000);
}

async function validateSearchResult() {

  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
  if(noResult){
    appLog("No Results found with given criteria..");
    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
  }else{
    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
    appLog("Successfully clicked on Transcation with given search criteria");
  }
}

async function scrolltoTranscations_accountDetails(){

  appLog("Intiated method to scroll to Transcations under account details");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");

  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);

}

async function VerifyAdvancedSearch_byAmount(AmountRange1,AmountRange2){

  await clickOnSearch_AccountDetails();
  await selectAmountRange(AmountRange1,AmountRange2);
  await clickOnAdvancedSearchBtn();
  await validateSearchResult();

}

async function VerifyAdvancedSearch_byTranxType(TransactionType){

  await clickOnSearch_AccountDetails();
  await selectTranscationtype(TransactionType);
  await clickOnAdvancedSearchBtn();
  await validateSearchResult();

}

async function VerifyAdvancedSearch_byDate(){

  await clickOnSearch_AccountDetails();
  await selectTimePeriod();
  await clickOnAdvancedSearchBtn();
  await validateSearchResult();
}

async function VerifyAdvancedSearch_byKeyword(keyword){

  await clickOnSearch_AccountDetails();
  await enterKeywordtoSearch(keyword);
  await clickOnAdvancedSearchBtn();
  await validateSearchResult();
}

async function MoveBackToLandingScreen_AccDetails(){

  appLog("Intiated method to Move back to Account Dashboard from AccountsDetails");
  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);

  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  appLog("Successfully Moved back to Account Dashboard");
}

async function VerifyAccountOnDashBoard(AccountType){

  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
  var myList = new Array();

  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
  var segLength=accounts_Size.length;

  var finished = false;
  for(var x = 0; x <segLength && !finished; x++) {

    var segHeaders="segAccounts["+x+",-1]";

    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
    //appLog('Sub accounts size is '+subaccounts_Length);

    for(var y = 0; y <subaccounts_Length; y++){

      var seg="segAccounts["+x+","+y+"]";

      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
      if(typeOfAccount.includes(AccountType)){
        appLog("Successfully verified : <b>"+accountName+"</b>");
        myList.push("TRUE");
        finished = true;
        break;
      }else{
        myList.push("FALSE");
      }
    }
  }

  appLog("My Actual List is :: "+myList);
  var Status=JSON.stringify(myList).includes("TRUE");
  appLog("Over all Result is  :: <b>"+Status+"</b>");
  expect(Status).toBe(true);
}


async function VerifyCheckingAccountonDashBoard(){

  appLog("Intiated method to verify Checking account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
  VerifyAccountOnDashBoard("Checking");
}

async function VerifySavingsAccountonDashBoard(){

  appLog("Intiated method to verify Savings account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
  VerifyAccountOnDashBoard("Saving");
}
async function VerifyCreditCardAccountonDashBoard(){

  appLog("Intiated method to verify CreditCard account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
  VerifyAccountOnDashBoard("Credit");
}

async function VerifyDepositAccountonDashBoard(){

  appLog("Intiated method to verify Deposit account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
  VerifyAccountOnDashBoard("Deposit");
}

async function VerifyLoanAccountonDashBoard(){

  appLog("Intiated method to verify Loan account in Dashboard");

  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
  VerifyAccountOnDashBoard("Loan");
}

async function verifyViewAllTranscation(){

  appLog("Intiated method to view all Tranx in AccountDetails");

  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
}


async function verifyContextMenuOptions(myList_Expected){

  //var myList_Expected = new Array();
  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
  myList_Expected.push(myList_Expected);

  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
  var segLength=accounts_Size.length;
  //appLog("Length is :: "+segLength);
  var myList = new Array();

  for(var x = 0; x <segLength-1; x++) {

    var seg="segAccountListActions["+x+"]";
    //appLog("Segment is :: "+seg);
    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
    //appLog("Text is :: "+options);
    myList.push(options);
  }

  appLog("My Actual List is :: "+myList);
  appLog("My Expected List is:: "+myList_Expected);

  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
  //appLog("isFounded"+isFounded);
  expect(isFounded).toBe(true);
}

async function MoveBackToLandingScreen_Accounts(){

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

}


async function verifyAccountSummary_CheckingAccounts(){

  appLog("Intiated method to verify account summary for Checking Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
}

async function verifyAccountSummary_DepositAccounts(){

  appLog("Intiated method to verify account summary for Deposit Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
}

async function verifyAccountSummary_CreditCardAccounts(){

  appLog("Intiated method to verify account summary for CreditCard Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);

}

async function verifyAccountSummary_LoanAccounts(){

  appLog("Intiated method to verify account summary for Loan Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
}

async function verifyAccountSummary_SavingsAccounts(){

  appLog("Intiated method to verify account summary for Savings Account");

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
}

async function VerifyPendingWithdrawls_accountSummary(){

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab1"],15000);
  kony.automation.button.click(["frmAccountsDetails","summary","btnTab1"]);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"]);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"], "text")).not.toBe("");

}

async function VerifyInterestdetails_accountSummary(){

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab2"],15000);
  kony.automation.button.click(["frmAccountsDetails","summary","btnTab2"]);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab2"],10000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab2"], "text")).not.toBe("");
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl7Tab2"],10000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl7Tab2"], "text")).not.toBe("");

}

async function VerifySwiftCode_accountSummary(){

  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab3"],15000);
  kony.automation.button.click(["frmAccountsDetails","summary","btnTab3"]);
  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab3"],10000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab3"], "text")).not.toBe("");
}

async function selectContextMenuOption(Option){

  appLog("Intiated method to select context menu option :: "+Option);

  var myList = new Array();

  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],30000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");

  var segLength=accounts_Size.length;
  appLog("Length is :: "+segLength);
  for(var x = 0; x <segLength; x++) {

    var seg="segAccountListActions["+x+"]";
    //appLog("Segment will be :: "+seg);
    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg],15000);
    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
    appLog("Menu Item Text is :: "+TransfersText);
    if(TransfersText===Option){
      appLog("Option to be selected is :"+TransfersText);
       await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes"],15000);
      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
      await kony.automation.playback.wait(10000);
      myList.push("TRUE");
      break;
    }else{
      myList.push("FALSE");
    }
  }

  appLog("My Actual List is :: "+myList);
  var Status=JSON.stringify(myList).includes("TRUE");
  appLog("Over all Result is  :: <b>"+Status+"</b>");

  expect(Status).toBe(true,"Failed to click on option <b>"+Option+"</b>");
}

// async function SelectContextualOnDashBoard(AccountType){

//   appLog("Intiated method to analyze accounts data Dashboard");

//   var Status=false;

//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
//   var segLength=accounts_Size.length;

//   var finished = false;
//   for(var x = 0; x <segLength && !finished; x++) {

//     var segHeaders="segAccounts["+x+",-1]";

//     var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
//     var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
//     //appLog('Sub accounts size is '+subaccounts_Length);

//     for(var y = 0; y <subaccounts_Length; y++){

//       var seg="segAccounts["+x+","+y+"]";

//       var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
//       var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
//       if(typeOfAccount.includes(AccountType)){
//         await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
//         kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
//         appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
//         await kony.automation.playback.wait(5000);
//         finished = true;
//         Status=true;
//         break;
//       }
//     }
//   }

//   expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
// }

async function SelectContextualOnDashBoard(AccountNumber){

  appLog("Intiated method to select Menu of account : "+AccountNumber);
  await kony.automation.playback.wait(5000);
  var Status=false;

  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],30000);
  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
  var segLength=accounts_Size.length;

  var finished = false;
  for(var x = 0; x <segLength && !finished; x++) {

    var segHeaders="segAccounts["+x+",-1]";

    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
    appLog('Sub accounts size is '+subaccounts_Length);

    for(var y = 0; y <subaccounts_Length; y++){

      var seg="segAccounts["+x+","+y+"]";

      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
      appLog("Account Name is : "+accountName);
      if(accountName.includes(AccountNumber)){
        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
        await kony.automation.playback.wait(5000);
        // Validate really list is displayed or not
        var menuList=await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
        expect(menuList).toBe(true,"Failed to display contextual menu list items");
        finished = true;
        Status=true;
        break;
      }
    }
  }

  expect(Status).toBe(true,"Failed to click on Menu of AccNumber: <b>"+AccountNumber+"</b>");
}

async function verifyVivewStatementsHeader(){

  appLog('Intiated method to verify account statement header');
  var Status=await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
  //kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");
  expect(Status).toBe(true,"Failed to Navigate to Account Statements");
  //appLog('Successfully Verified Account Statement Header');
}

async function VerifyPostedPendingTranscation(){

  appLog('Intiated method to verify Pending and Posted Transcations');
  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
  var TransType=kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","segTransactions[0,-1]","lblTransactionHeader"], "text");
  if(TransType.includes("Posted")||TransType.includes("Pending")){
    //kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
    //await kony.automation.playback.wait(5000);
    appLog('Transfer Type is : '+TransType);
  }else{
    appLog('No Pending or Posted Transcation available');
  }

}


async function VerifyStopChequePaymentScreen(){
  
  appLog("Intiated method to verify StopChequePayment Screen");
  var Status=await kony.automation.playback.waitFor(["frmStopPayments","lblChequeBookRequests"],30000);
  expect(Status).toBe(true,"Failed to Verify StopChequePayment screen");
  appLog("Successfully Verified StopChequePayment");
  
}

async function VerifyAccountAlertScreen(){
  
  appLog("Intiated method to verify AccountAlert Screen");
  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAlertsHeading"],30000);
  expect(Status).toBe(true,"Failed to Verify AccountAlert screen");
  appLog("Successfully Verified AccountAlert");
}

async function MoveBack_Accounts_StopChequeBook(){
  
  appLog("Intiated method to Move back to Account Dashboard from StopPayments");
  await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);

  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  appLog("Successfully Moved back to Account Dashboard");

}

async function MoveBack_Accounts_ProfileManagement(){

  // Move back to base state
  appLog("Intiated method to Move back to Account Dashboard from ProfileManagement");
  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
  
  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  appLog("Successfully Moved back to Account Dashboard");
}
