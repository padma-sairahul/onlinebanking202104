async function NavigatetoExchangeRates(){

  appLog("Intiated method to Navigate to Exchange Rate");

  // Navigate to Exchange Rate
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ExchangeRatesflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ExchangeRatesflxAccountsMenu"]);
  await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitFor(["frmForexDashboard","lblForexHeader"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmForexDashboard","lblForexHeader"], "text")).toEqual("Exchange Rate");

  appLog("Successfully Navigated to Exchange Rate");
}

async function MoveBackFrom_ExchangeRates(){

  await kony.automation.playback.waitFor(["frmForexDashboard","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmForexDashboard","customheadernew","flxAccounts"]);
}

async function setToCurrencytoEuro(){

  appLog("Intiated method to Select To Currency dropdown");
  //Set To Currency to EURO
  await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","flxDropDownIcon"],15000);
  kony.automation.flexcontainer.click(["frmForexDashboard","foreignExchange","flxDropDownIcon"]);
  appLog("Successfully selected To Currency dropdown");
  appLog("Intiated method to set To currency");
  await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","tbxSearch"],15000);
  kony.automation.textbox.enterText(["frmForexDashboard","foreignExchange","tbxSearch"],"EUR");
  await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","segToCuurencyCode"],15000);
  kony.automation.flexcontainer.click(["frmForexDashboard","foreignExchange","segToCuurencyCode[0]","flxRowExchangeRate"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully selected EUR currency");
  //Verify Warnings
  var Warning1=await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","lblDowntimeWarning"],10000);
  expect(Warning1).toBe(false,"Failed to fetch base currency");
  var Warning2=await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","lblSelectedDowntimeWarning"],10000);
  expect(Warning2).toBe(false,"Unable to fetch the data.");
  var Warning3=await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","lblRecentDowntimeWarning"],10000);
  expect(Warning3).toBe(false,"Unable to fetch the data.");
}

async function CalucateExchangeRate(){

  appLog("Intiated method to Enter ");
  // Calucate the exchange Rate
  await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","tbxBaseCurrencyCodeValue"],15000);
  kony.automation.textbox.enterText(["frmForexDashboard","foreignExchange","tbxBaseCurrencyCodeValue"],"2");

}