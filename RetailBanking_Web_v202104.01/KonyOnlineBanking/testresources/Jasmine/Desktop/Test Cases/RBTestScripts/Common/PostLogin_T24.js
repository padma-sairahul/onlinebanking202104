async function PostLogin_NavigateToAboutUs_FAQ(){

  appLog("Intiated  method to Navigate to About US");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmOnlineHelp","help","lblHeading"],15000);

  appLog("Successfully Navigated to About US");
}

async function PostLogin_MoveBacktoDashboard_AboutUs_FAQ(){

  await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
  //await kony.automation.playback.wait(5000);
  appLog("Successfully Moved back to Accounts dashboard");
}

async function PostLogin_NavigateToPrivacyPolicy(){

  appLog("Intiated method to Navigate to Privacypolicy");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs1flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs1flxMyAccounts"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"],15000);

  appLog("Successfully Navigated to Privacypolicy");

}

async function PostLogin_MoveBacktoDashboard_PrivacyPolicy(){

  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
  //await kony.automation.playback.wait(5000);
  appLog("Successfully Moved back to Accounts dashboard");
}

async function PostLogin_NavigateToTermsConditions(){

  appLog("Intiated method to Navigate to TC's");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs0flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs0flxMyAccounts"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"],15000);

  appLog("Successfully Navigated to TC's");
}

async function PostLogin_MoveBacktoDashboard_TermsConditions(){

  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
  //await kony.automation.playback.wait(5000);
  appLog("Successfully Moved back to Accounts dashboard");
}

async function PostLogin_NavigateToContactUs(){

  appLog("Intiated method to Navigate to Contact US");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs2flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs2flxMyAccounts"]);
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"],15000);

  appLog("Successfully Navigated to Contact US");
}

async function PostLogin_MoveBacktoDashboard_ContactUs(){

  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
  //await kony.automation.playback.wait(5000);
  appLog("Successfully Moved back to Accounts dashboard");
}

async function PostLogin_NavigateToAboutUs_Help(){

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"]);
  await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
}


async function PostLogin_VerifyFeedBackFunctionality(){

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs5flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs5flxMyAccounts"]);
  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","flxRating5"],15000);
  kony.automation.flexcontainer.click(["frmCustomerFeedback","Feedback","flxRating5"]);
  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","txtareaUserComments"],15000);
  kony.automation.textarea.enterText(["frmCustomerFeedback","Feedback","txtareaUserComments"],"test");
  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","confirmButtons","btnConfirm"],15000);
  kony.automation.button.click(["frmCustomerFeedback","Feedback","confirmButtons","btnConfirm"]);
  await kony.automation.playback.waitFor(["frmCustomerFeedback","acknowledgment","lblTransactionMessage"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCustomerFeedback","acknowledgment","lblTransactionMessage"], "text")).toContain("Thank you");
  await kony.automation.playback.waitFor(["frmCustomerFeedback","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmCustomerFeedback","customheader","topmenu","flxaccounts"]);
  
}