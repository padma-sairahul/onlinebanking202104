it("VerifyDeleteEmailID", async function() {
  
  var isPrimary='NO';
  var emailid=Settings.email.emailAddress+getRandomString(3)+"@infinity.com";
  
  await NavigateToProfileSettings();
  await selectProfileSettings_EmailAddress();
  await ProfileSettings_VerifyaddEmailAddressFunctionality(emailid,isPrimary);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();
  
},120000);