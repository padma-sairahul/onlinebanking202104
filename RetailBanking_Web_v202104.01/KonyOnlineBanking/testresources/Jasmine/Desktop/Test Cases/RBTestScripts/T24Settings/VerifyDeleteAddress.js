it("VerifyDeleteAddress", async function() {
  
  var isPrimary='NO';
  
  var addressLine1=Settings.address.addressLine1+getRandomString(3);
  var addressLine2=Settings.address.addressLine2+getRandomString(3);
  var zipcode=Settings.address.zipcode+getRandomNumber(3);
  
  await NavigateToProfileSettings();
  await selectProfileSettings_Address();
  await ProfileSettings_VerifyaddNewAddressFunctionality(addressLine1,addressLine2,zipcode,isPrimary);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();
  
},120000);