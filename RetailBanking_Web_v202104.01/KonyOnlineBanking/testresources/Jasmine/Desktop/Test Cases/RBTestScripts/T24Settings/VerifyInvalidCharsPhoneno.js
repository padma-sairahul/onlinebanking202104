it("VerifyInvalidCharsPhoneno", async function() {
  
  var phoneNumber="abcd"+getRandomString(5);
  var isPrimary='NO';
  
  await NavigateToProfileSettings();
  await selectProfileSettings_PhoneNumber();
  await ProfileSettings_VerifyaddInvalidPhoneNumberFunctionality(phoneNumber,isPrimary);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();
  
},120000);