it("VerifyDeletePhoneno", async function() {
  
  var phoneNumber=Settings.phone.phoneNumber+getRandomNumber(3);
  var isPrimary='NO';
  
  await NavigateToProfileSettings();
  await selectProfileSettings_PhoneNumber();
  await ProfileSettings_VerifyaddNewPhoneNumberFunctionality(phoneNumber,isPrimary);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();
  
},120000);