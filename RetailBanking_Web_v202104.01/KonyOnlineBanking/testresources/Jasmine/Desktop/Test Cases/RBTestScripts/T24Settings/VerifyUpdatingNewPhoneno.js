it("VerifyUpdatingNewPhoneno", async function() {
  
  var updatedPhonenum=Settings.phone.updatedPhonenum+getRandomNumber(3);
  
  await NavigateToProfileSettings();
  await selectProfileSettings_PhoneNumber();
  await ProfileSettings_UpdatePhoneNumber(updatedPhonenum);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();
  
},120000);