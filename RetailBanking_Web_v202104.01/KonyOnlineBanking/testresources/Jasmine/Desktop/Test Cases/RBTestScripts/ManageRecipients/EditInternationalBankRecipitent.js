it("EditInternationalBankRecipitent", async function() {

  // Add a recipitent and Then delete the same recipitent
  
  var swiftCode=ManageRecipients.internationalAccount.swiftCode;
  var Accno="0"+new Date().getTime();
  var unique_RecipitentName=ManageRecipients.internationalAccount.unique_RecipitentName+getRandomString(5);
  
  //var AccType="International";
  var unique_EditRecipitentName=ManageRecipients.internationalAccount.unique_EditRecipitentName+getRandomString(5);
  var unique_EditNickName=ManageRecipients.internationalAccount.unique_EditNickName+getRandomString(5);
  
  await NavigateToManageRecipitents();
  await clickonAddInternationalAccounttab();
  await enterInternationalBankAccountDetails(swiftCode,Accno,unique_RecipitentName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
  //Edit Added Recipitent
  await NavigateToManageRecipitents();
  await clickOnExternalRecipitentsTab();
  await SearchforPayee_External(unique_RecipitentName);
  await EditReciptent(unique_EditRecipitentName,unique_EditNickName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
  
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransferMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransferMoney"]);

//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmFastTransfers","flxAddInternationalAccount"]);
//   kony.automation.flexcontainer.click(["frmFastTransfers","flxAddInternationalAccount"]);

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxSWIFTCodeKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxSWIFTCodeKA"],"BOFAUS3N");

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxAccountNumberKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxAccountNumberKA"],"1234567890");

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxAccountNumberAgainKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxAccountNumberAgainKA"],"1234567890");

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxBeneficiaryNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxBeneficiaryNameKA"],"TestInternational");

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxAccountNickNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxAccountNickNameKA"],"TestInternational");

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","btnAddAccountKA"]);
//   kony.automation.button.click(["frmFastAddInternationalAccount","btnAddAccountKA"]);

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccountConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmFastAddInternationalAccountConfirm","btnConfirm"]);

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccountAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastAddInternationalAccountAcknowledgement","customheadernew","flxAccounts"]);

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   // Edit the added Recipitent
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],"TestInternational");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnEdit"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","lblTransfers"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddInternationalAccount","lblTransfers"],"text")).toContain("Edit");
//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","tbxAccountNickName"]);
//   kony.automation.textbox.enterText(["frmFastAddInternationalAccount","tbxAccountNickName"],"Auto Updated");

//   //Having intermittent issue in Save button
//   await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","btnSave"]);
//   kony.automation.button.click(["frmFastAddInternationalAccount","btnSave"]);

//   var successMsg=await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],10000);

//   if(!successMsg){
//     // Move back to base state
//     await kony.automation.playback.waitFor(["frmFastAddInternationalAccount","customheadernew","flxAccounts"]);
//     kony.automation.flexcontainer.click(["frmFastAddInternationalAccount","customheadernew","flxAccounts"]);
//   }else{
//     await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been successfully edited");
//     await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
//     kony.automation.flexcontainer.click(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   }
  
//   // Delete the recipitent to clean list

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
//   await kony.automation.playback.wait(5000);
  
//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
  
//   if(error){
//     fail("There was a technical delay. Please try again.");
//   }

//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);