it("AddExternalBankRecipitent", async function() {
  
  // Add a recipitent and Then delete the same recipitent
  
  var Routingno=ManageRecipients.externalAccount.Routingno;
  var Accno="0"+new Date().getTime();
  var unique_RecipitentName=ManageRecipients.externalAccount.unique_RecipitentName+getRandomString(5);
  
  await NavigateToManageRecipitents();
  await clickonAddExternalAccounttab();
  await enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
  //Delete Added Recipitent
  
//   await NavigateToManageRecipitents();
//   await clickOnExternalRecipitentsTab();
//   await SearchforPayee_External(unique_RecipitentName);
//   await DeleteReciptent();
//   await MoveBacktoDashboard_ManageRecipitent();

  
  
  //var unique_RecipitentName="ExtAcc"+new Date().getTime();
  
//   var unique_RecipitentName="ExtAccJasmine";
  
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(10000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","lblTitle"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","BeneficiaryList","lblTitle"],"text")).toEqual("Manage Recipients");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow2"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow2"]);
//   await kony.automation.playback.wait(5000);
  
//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"]);
//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],"1234567890");
//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"]);
//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],"1234567890");
//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"]);
//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],"1234567890");
//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"]);
//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"]);
//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],unique_RecipitentName);
  
//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"]);
//   kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
  
//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction6"]);
//   kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction6"]);

//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblSection1Message"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","lblSection1Message"],"text")).toContain("has been added.");

//   await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
//    //Delete Same Bank Recipitent
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","lblTitle"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","BeneficiaryList","lblTitle"],"text")).toEqual("Manage Recipients");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnTab1"]);
//   kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","btnTab1"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","BeneficiaryList","txtSearch"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn3"]);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnYesIC"]);
//   kony.automation.button.click(["frmFastManagePayee","btnYesIC"]);
//   await kony.automation.playback.wait(5000);
  
//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
  
//   if(error){
//     fail("There was a technical delay. Please try again.");
     
//   }

//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);