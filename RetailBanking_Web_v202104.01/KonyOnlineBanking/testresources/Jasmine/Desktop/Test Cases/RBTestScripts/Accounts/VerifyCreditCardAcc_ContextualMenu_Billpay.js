it("VerifyCreditCardAcc_ContextualMenu_Billpay", async function() {

  await clickOnCreditCardAccountContextMenu();
  await selectContextMenuOption("Pay Bill");
  
  //Bill pay
  await enterAmount_SheduleBillPay(Accounts.creditcardAcc.amountValue);
  await selectfrequency_SheduledBillPay("Daily");
  await SelectDateRange_SheduledBillpay();
  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Daily");
  await confirmSheduledBillpay();
  await verifySheduledBillpaySuccessMsg();
  await verifyAccountsLandingScreen();
  
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[2,0]","flxMenu"]);
//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);

//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");

//   var segLength=accounts_Size.length;
//   kony.print("Length is :: "+segLength);
//   for(var x = 0; x <segLength; x++) {

//     var seg="segAccountListActions["+x+"]";
//     kony.print("Segment is :: "+seg);
//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"]);
//     var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"], "text");
//     kony.print("Text is :: "+TransfersText);
//     if(TransfersText==="Pay Bill"){
//       kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
//       break;
//     }
//   }

//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"VerifyCreditCardAcc_ContextualMenu_Billpay");
//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
  
//   //Checking for exception message
//   var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],10000);
//   if(warning){
//     //Move back to dashboard again there is an exception message
//     await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"]);
// 	kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
//     //fail("Amount Greater than Allowed Maximum Deposit");
//   }else{
//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
//   }

 

},90000);