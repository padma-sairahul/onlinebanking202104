it("AddSameBankRecipitent", async function() {
  
  var Accno="0"+new Date().getTime();
  var unique_RecipitentName=ManageRecipients.sameBankAccount.unique_RecipitentName+getRandomString(5);
  
  await NavigateToManageRecipitents();
  await clickonAddinfinityBankAccounttab();
  await enterSameBankAccountDetails(Accno,unique_RecipitentName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen(); 
  
},120000);