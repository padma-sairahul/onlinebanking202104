it("VerifyRepeatTransfer", async function() {
  
  await navigateToManageTranscations();
  await selectCompletedTransfers();
  await clickOnRepeatButton(AllAccounts.Saving.accType);
  
},TimeOuts.UnifiedTransfers.Transfers);