it("CancelTransfer", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("SameBank");
  await SelectUTFFromAccount(UTFPayments.SameBank.FromAcc);
  await SelectUTFToAccount(UTFPayments.SameBank.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.SameBank.Amount);
  await EnterUTFNoteValue("SameBank-Cancel OneTimeTransfers");
  await clickonCancelbutton();
  
},TimeOuts.UnifiedTransfers.Transfers);