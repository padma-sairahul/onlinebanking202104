it("OneTimeTransfer", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("SameBank");
  await SelectUTFFromAccount(UTFPayments.SameBank.FromAcc);
  await SelectUTFToAccount(UTFPayments.SameBank.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.SameBank.Amount);
  await EnterUTFNoteValue("SameBank-OneTimeTransfers");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
},TimeOuts.UnifiedTransfers.Transfers);