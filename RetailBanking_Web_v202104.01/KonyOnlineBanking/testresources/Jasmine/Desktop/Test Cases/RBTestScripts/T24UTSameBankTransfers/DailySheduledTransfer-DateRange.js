it("DailySheduledTransfer-DateRange", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("SameBank");
  await SelectUTFFromAccount(UTFPayments.SameBank.FromAcc);
  await SelectUTFToAccount(UTFPayments.SameBank.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.SameBank.Amount);
  await SelectUTFFrequency("Daily");
  await SelectUTFDateRange();
  await EnterUTFNoteValue("SameBank-DailySheduledTransfer-DateRange");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
},TimeOuts.UnifiedTransfers.Transfers);