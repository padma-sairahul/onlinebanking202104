it("AddNewSameBankAcc", async function() {
  
  var BeneficiaryName=UTFManageBeneficiary.SameBank.BeneficiaryName;
  var AccountNumber=UTFManageBeneficiary.SameBank.AccountNumberList[0].accno;
  var Nickname=UTFManageBeneficiary.SameBank.Nickname;
  var Address1=UTFManageBeneficiary.SameBank.Address1;
  var Address2=UTFManageBeneficiary.SameBank.Address2;
  var city=UTFManageBeneficiary.SameBank.city;
  var zipcode=UTFManageBeneficiary.SameBank.zipcode;
  
  await NavigateToManageBeneficiary();
  if(await isBenefeciaryAlreadyAdded(AccountNumber)){
    await MoveBackFrom_ManageBeneficiaries();
  }else{
  await MoveBackFrom_ManageBeneficiaries();
  await navigateToUnifiedTransfers();
  await clickOnAddNewAccountBtn_SameBank();
  await EnterSameBankAccDetails(BeneficiaryName,AccountNumber,Nickname);
  await ClickonConfirmButton();
  await VerifyAddNewAccSuccessMsg();
  }
  
},TimeOuts.UnifiedTransfers.AddNewAccount);