it("YearlySheduledTransfer-Occurences", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("SameBank");
  await SelectUTFFromAccount(UTFPayments.SameBank.FromAcc);
  await SelectUTFToAccount(UTFPayments.SameBank.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.SameBank.Amount);
  await SelectUTFFrequency("Yearly");
  await SelectUTFSendOnDate();
  await SelectUTFOccurences();
  await EnterUTFNoteValue("SameBank-YearlySheduledTransfer-Occurences");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
 },TimeOuts.UnifiedTransfers.Transfers);