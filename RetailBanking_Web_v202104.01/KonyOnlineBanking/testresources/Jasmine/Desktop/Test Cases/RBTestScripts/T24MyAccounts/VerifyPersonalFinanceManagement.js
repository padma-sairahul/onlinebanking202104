it("VerifyPersonalFinanceManagement", async function() {
  
	await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS6flxMyAccounts"],15000);
	kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS6flxMyAccounts"]);
	await kony.automation.playback.wait(10000);
	await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","CommonHeader","lblHeading"],15000);
	expect(kony.automation.widget.getWidgetProperty(["frmPersonalFinanceManagement","CommonHeader","lblHeading"], "text")).not.toBe("");
    await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],15000);
	kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
  
},90000);