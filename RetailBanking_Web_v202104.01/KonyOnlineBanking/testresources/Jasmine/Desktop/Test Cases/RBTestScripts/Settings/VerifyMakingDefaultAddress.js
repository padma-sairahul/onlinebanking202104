it("VerifyMakingDefaultAddress", async function() {
  
  var isPrimary='YES';
  
  var addressLine1=Settings.address.primaryAddressLine1;
  var addressLine2=Settings.address.primaryAddressLine2;
  var zipcode=Settings.address.primaryZipcode;
  
  await NavigateToProfileSettings();
  await selectProfileSettings_Address();
  await ProfileSettings_VerifyaddNewAddressFunctionality(addressLine1,addressLine2,zipcode,isPrimary);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);

//   await kony.automation.playback.wait(5000);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","lblPersonalDetailsHeading"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPersonalDetailsHeading"], "text")).toEqual("Personal Details");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddress"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddress"]);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddressHeading"], "text")).toEqual("Address");

//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddress"]);
//   kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddress"]);
//   await kony.automation.playback.wait(5000);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine1"]);
//   kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine1"],"Test Automation1");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine2"]);
//   kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine2"],"Test Automation2");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCountry"]);
//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCountry"], "IN");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxState"]);
//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxState"], "IN-TG");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxCity"]);
//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","tbxCity"], "C1");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxZipcode"]);
//   kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxZipcode"],"500049");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"]);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddressAdd"]);
//   kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddressAdd"]);
//   await kony.automation.playback.wait(5000);
  
//   await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);