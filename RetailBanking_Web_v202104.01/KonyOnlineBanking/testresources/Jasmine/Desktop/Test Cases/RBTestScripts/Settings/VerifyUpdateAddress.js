it("VerifyUpdateAddress", async function() {

  var updatedZip=Settings.address.updatedZip;
  
  await NavigateToProfileSettings();
  await selectProfileSettings_Address();
  ProfileSettings_UpdateAddress(updatedZip);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();


  //   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  //   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  //   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
  //   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
  //   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);
  //   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);

  //   await kony.automation.playback.wait(5000);
  //   kony.automation.flexcontainer.click(["frmProfileManagement","settings","lblPersonalDetailsHeading"]);
  //   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPersonalDetailsHeading"], "text")).toEqual("Personal Details");
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddress"]);
  //   kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddress"]);
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"]);
  //   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddressHeading"], "text")).toEqual("Address");

  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddress"]);
  //   kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddress"]);
  //   await kony.automation.playback.wait(5000);
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine1"]);
  //   kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine1"],"Hyderabad");
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine2"]);
  //   kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine2"],"Madhapur");
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCountry"]);
  //   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCountry"], "IN");
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxState"]);
  //   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxState"], "IN-TG");
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxCity"]);
  //   kony.automation.listbox.selectItem(["frmProfileManagement","settings","tbxCity"], "C1");
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxZipcode"]);
  //   kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxZipcode"],"500049");
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddressAdd"]);
  //   kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddressAdd"]);
  //   await kony.automation.playback.wait(5000);

  //   // Update Address
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"]);
  //   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");

  //   var segLength=accounts_Size.length;
  //   for(var y = 0; y <segLength; y++) {
  //     var seg1="segAddresses["+y+"]";
  //     await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"flxRow","lblAddressLine1"]);
  //     var addressline1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"flxRow","lblAddressLine1"], "text");
  //     kony.print("Text is :: "+addressline1);
  //     if(addressline1==="Test Automation1"){
  //       kony.automation.button.click(["frmProfileManagement","settings",seg1,"btnEdit"]);
  //       await kony.automation.playback.wait(5000);
  //       await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxRight"]);
  //       kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxRight"]);
  //       await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditZipcode"]);
  //       kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditZipcode"],"12345");
  //       await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAddressSave"]);
  //       kony.automation.button.click(["frmProfileManagement","settings","btnEditAddressSave"]);
  //       await kony.automation.playback.wait(5000);
  //       break;
  //     }
  //   }

  //   //Delete Address
  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"]);
  //   var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");

  //   var segLength1=accounts_Size1.length;
  //   for(var x = 0; x <segLength1; x++) {
  //     var seg="segAddresses["+x+"]";
  //     await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"flxRow","lblAddressLine1"]);
  //     var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"flxRow","lblAddressLine1"], "text");
  //     kony.print("Text is :: "+address1);
  //     if(address1==="Test Automation1"){
  //       kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
  //       await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"]);
  //       kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
  //       await kony.automation.playback.wait(5000);
  //       break;
  //     }
  //   }

  //   await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
  //   kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
  //   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);