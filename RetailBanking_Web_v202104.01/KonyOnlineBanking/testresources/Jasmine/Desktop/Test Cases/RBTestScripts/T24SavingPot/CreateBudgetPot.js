it("CreateBudgetPot", async function() {
  
  await clickOnFirstAvailableAccount();
  await navigateToSavingPot();
  await clickOnCreateNewSavingPot();
  await CreateNewBudget();
  
},120000);
