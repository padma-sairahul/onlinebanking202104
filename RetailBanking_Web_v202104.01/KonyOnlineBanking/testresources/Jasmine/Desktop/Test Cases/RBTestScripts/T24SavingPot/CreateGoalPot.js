it("CreateGoalPot", async function() {
  
  await clickOnFirstAvailableAccount();
  await navigateToSavingPot();
  await clickOnCreateNewSavingPot();
  await CreateNewGoal();
  
},120000);
