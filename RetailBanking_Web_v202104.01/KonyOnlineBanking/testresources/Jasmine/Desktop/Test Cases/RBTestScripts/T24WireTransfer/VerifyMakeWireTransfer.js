it("VerifyMakeWireTransfer", async function() {
  
  await NavigateToMakeTransfer();
  await ClickOnMakeTransferLink();
  await MakeWireTransfer();
  
},TimeOuts.WireTransfers.Payment);