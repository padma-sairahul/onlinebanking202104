it("CreateTemplate_ManualRecipitent", async function() {
  
  var RecipientName=WireTransers.DomesticRecipitentDetails.BeneficiaryName;
  var AddressLine1=WireTransers.DomesticRecipitentDetails.Address1;
  var AddressLine2=WireTransers.DomesticRecipitentDetails.Address2;
  var City=WireTransers.DomesticRecipitentDetails.city;
  var ZipCode=WireTransers.DomesticRecipitentDetails.zipcode;
  
  var RoutingNumber=WireTransers.DomesticBankdetails.RoutingNumber+getRandomNumber(7);
  var AccNumber=WireTransers.DomesticBankdetails.AccNumber+getRandomNumber(7);
  var NickName=WireTransers.DomesticBankdetails.Nickname;
  var BankName=WireTransers.DomesticBankdetails.BankName;
  var BankAddressLine1=WireTransers.DomesticBankdetails.BankAddressLine1;
  var BankAddressLine2=WireTransers.DomesticBankdetails.BankAddressLine2;
  var BankCity=WireTransers.DomesticBankdetails.BankCity;
  var BankZipcode=WireTransers.DomesticBankdetails.BankZipcode;
  
  await NavigatetoCreateNewTemplate();
  await EnterTemplateDetails("Temp_ManualRecipitent"+getRandomString(5));
  await selectNewManualRecipitentOption();
  await EnterManualRecipitentTemplateDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode,RoutingNumber,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode);
  await VerifyNewTemplateSuccessMsg();
  
},TimeOuts.WireTransfers.Payment);