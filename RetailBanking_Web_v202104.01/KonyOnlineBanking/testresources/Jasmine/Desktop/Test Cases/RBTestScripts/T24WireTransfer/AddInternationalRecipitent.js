it("AddInternationalRecipitent", async function() {
  
  
  var RecipientName=WireTransers.InternationalRecipitentDetails.BeneficiaryName;
  var AddressLine1=WireTransers.InternationalRecipitentDetails.Address1;
  var AddressLine2=WireTransers.InternationalRecipitentDetails.Address2;
  var City=WireTransers.InternationalRecipitentDetails.city;
  var ZipCode=WireTransers.InternationalRecipitentDetails.zipcode;
  
  var SwiftCode=WireTransers.InternationalBankdetails.SWIFT;
  var IBAN=WireTransers.InternationalBankdetails.IBAN+getRandomNumber(7);
  var AccNumber=WireTransers.InternationalBankdetails.AccNumber+getRandomNumber(7);
  var NickName=WireTransers.InternationalBankdetails.Nickname;
  var BankName=WireTransers.InternationalBankdetails.BankName;
  var BankAddressLine1=WireTransers.InternationalBankdetails.BankAddressLine1;
  var BankAddressLine2=WireTransers.InternationalBankdetails.BankAddressLine2;
  var BankCity=WireTransers.InternationalBankdetails.BankCity;
  var BankZipcode=WireTransers.InternationalBankdetails.BankZipcode;
  
  
  await NavigateToWireTransfer_AddRecipitent();
  await EnterInternational_RecipitentDetails_Step1(RecipientName,AddressLine1,AddressLine2,City,ZipCode);
  await EnterInternational_BankDetails_Step2(SwiftCode,IBAN,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode);
  await VerifyAddInternationalRecipitentSuccessMsg();
  
},TimeOuts.WireTransfers.Payment);