describe("temp1", function() {
	beforeEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  strLogger=[];
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside before Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	},480000);
	
	afterEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside after Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	  //strLogger=null;
	
	},480000);
	
	async function verifyAccountsLandingScreen(){
	
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
	}
	
	async function SelectAccountsOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
	        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(10000);
	        
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnFirstCheckingAccount(){
	
	  appLog("Intiated method to click on First Checking account");
	  SelectAccountsOnDashBoard("Checking");
	  //appLog("Successfully Clicked on First Checking account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstSavingsAccount(){
	
	  appLog("Intiated method to click on First Savings account");
	  SelectAccountsOnDashBoard("Saving");
	  //appLog("Successfully Clicked on First Savings account");
	  //await kony.automation.playback.wait(5000);
	}
	
	
	async function clickOnFirstCreditCardAccount(){
	
	  appLog("Intiated method to click on First CreditCard account");
	  SelectAccountsOnDashBoard("Credit");
	  //appLog("Successfully Clicked on First CreditCard account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstDepositAccount(){
	
	  appLog("Intiated method to click on First Deposit account");
	  SelectAccountsOnDashBoard("Deposit");
	  //appLog("Successfully Clicked on First Deposit account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstLoanAccount(){
	
	  appLog("Intiated method to click on First Loan account");
	  SelectAccountsOnDashBoard("Loan");
	  //appLog("Successfully Clicked on First Loan account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstAvailableAccount(){
	
	  appLog("Intiated method to click on First available account on DashBoard");
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	  kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	  appLog("Successfully Clicked on First available account on DashBoard");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickOnSearch_AccountDetails(){
	
	  appLog("Intiated method to click on Search Glass Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Seach Glass Icon");
	}
	
	async function selectTranscationtype(TransactionType){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], TransactionType);
	  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
	}
	
	async function enterKeywordtoSearch(keyword){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtKeyword"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtKeyword"],keyword);
	  appLog("Successfully entered keyword for search : <b>"+keyword+"</b>");
	}
	
	async function selectAmountRange(AmountRange1,AmountRange2){
	
	  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
	  appLog("Successfully selected amount range From");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
	  appLog("Successfully selected amount range To");
	
	  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");
	}
	
	async function selectCustomdate(){
	
	  appLog("Intiated method to select Custom Date Range");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
	  appLog("Successfully selected Date Range");
	}
	
	async function selectTimePeriod(){
	
	  appLog("Intiated method to select custom timeperiod");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	  appLog("Successfully selected Time period");
	}
	
	async function clickOnAdvancedSearchBtn(){
	
	  appLog("Intiated method to click on Search Button with given search criteria");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function validateSearchResult() {
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	}
	
	async function scrolltoTranscations_accountDetails(){
	
	  appLog("Intiated method to scroll to Transcations under account details");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	}
	
	async function VerifyAdvancedSearch_byAmount(AmountRange1,AmountRange2){
	
	  await clickOnSearch_AccountDetails();
	  await selectAmountRange(AmountRange1,AmountRange2);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byTranxType(TransactionType){
	
	  await clickOnSearch_AccountDetails();
	  await selectTranscationtype(TransactionType);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byDate(){
	
	  await clickOnSearch_AccountDetails();
	  await selectTimePeriod();
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function VerifyAdvancedSearch_byKeyword(keyword){
	
	  await clickOnSearch_AccountDetails();
	  await enterKeywordtoSearch(keyword);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function MoveBackToLandingScreen_AccDetails(){
	
	  appLog("Intiated method to Move back to Account Dashboard from AccountsDetails");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	async function VerifyAccountOnDashBoard(AccountType){
	
	  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        appLog("Successfully verified : <b>"+accountName+"</b>");
	        myList.push("TRUE");
	        finished = true;
	        break;
	      }else{
	        myList.push("FALSE");
	      }
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	
	async function VerifyCheckingAccountonDashBoard(){
	
	  appLog("Intiated method to verify Checking account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
	  VerifyAccountOnDashBoard("Checking");
	}
	
	async function VerifySavingsAccountonDashBoard(){
	
	  appLog("Intiated method to verify Savings account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
	  VerifyAccountOnDashBoard("Saving");
	}
	async function VerifyCreditCardAccountonDashBoard(){
	
	  appLog("Intiated method to verify CreditCard account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
	  VerifyAccountOnDashBoard("Credit");
	}
	
	async function VerifyDepositAccountonDashBoard(){
	
	  appLog("Intiated method to verify Deposit account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
	  VerifyAccountOnDashBoard("Deposit");
	}
	
	async function VerifyLoanAccountonDashBoard(){
	
	  appLog("Intiated method to verify Loan account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
	  VerifyAccountOnDashBoard("Loan");
	}
	
	async function verifyViewAllTranscation(){
	
	  appLog("Intiated method to view all Tranx in AccountDetails");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	}
	
	
	async function verifyContextMenuOptions(myList_Expected){
	
	  //var myList_Expected = new Array();
	  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	  myList_Expected.push(myList_Expected);
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  var myList = new Array();
	
	  for(var x = 0; x <segLength-1; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+options);
	    myList.push(options);
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	
	  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	  //appLog("isFounded"+isFounded);
	  expect(isFounded).toBe(true);
	}
	
	async function MoveBackToLandingScreen_Accounts(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	}
	
	
	async function verifyAccountSummary_CheckingAccounts(){
	
	  appLog("Intiated method to verify account summary for Checking Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_DepositAccounts(){
	
	  appLog("Intiated method to verify account summary for Deposit Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_CreditCardAccounts(){
	
	  appLog("Intiated method to verify account summary for CreditCard Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	
	}
	
	async function verifyAccountSummary_LoanAccounts(){
	
	  appLog("Intiated method to verify account summary for Loan Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_SavingsAccounts(){
	
	  appLog("Intiated method to verify account summary for Savings Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function VerifyPendingWithdrawls_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab1"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab1"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"], "text")).not.toBe("");
	
	}
	
	async function VerifyInterestdetails_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab2"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab2"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab2"], "text")).not.toBe("");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl7Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl7Tab2"], "text")).not.toBe("");
	
	}
	
	async function VerifySwiftCode_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab3"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab3"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab3"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab3"], "text")).not.toBe("");
	}
	
	async function selectContextMenuOption(Option){
	
	  appLog("Intiated method to select context menu option :: "+Option);
	
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	  var segLength=accounts_Size.length;
	  appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment will be :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg],15000);
	    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    appLog("Menu Item Text is :: "+TransfersText);
	    if(TransfersText===Option){
	      appLog("Option to be selected is :"+TransfersText);
	       await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes"],15000);
	      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
	      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
	      await kony.automation.playback.wait(10000);
	      myList.push("TRUE");
	      break;
	    }else{
	      myList.push("FALSE");
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	
	  expect(Status).toBe(true,"Failed to click on option <b>"+Option+"</b>");
	}
	
	// async function SelectContextualOnDashBoard(AccountType){
	
	//   appLog("Intiated method to analyze accounts data Dashboard");
	
	//   var Status=false;
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	//   var segLength=accounts_Size.length;
	
	//   var finished = false;
	//   for(var x = 0; x <segLength && !finished; x++) {
	
	//     var segHeaders="segAccounts["+x+",-1]";
	
	//     var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	//     var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	//     //appLog('Sub accounts size is '+subaccounts_Length);
	
	//     for(var y = 0; y <subaccounts_Length; y++){
	
	//       var seg="segAccounts["+x+","+y+"]";
	
	//       var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	//       var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	//       if(typeOfAccount.includes(AccountType)){
	//         await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
	//         kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	//         appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	//         await kony.automation.playback.wait(5000);
	//         finished = true;
	//         Status=true;
	//         break;
	//       }
	//     }
	//   }
	
	//   expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
	// }
	
	async function SelectContextualOnDashBoard(AccountNumber){
	
	  appLog("Intiated method to select Menu of account : "+AccountNumber);
	  await kony.automation.playback.wait(5000);
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      appLog("Account Name is : "+accountName);
	      if(accountName.includes(AccountNumber)){
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(5000);
	        // Validate really list is displayed or not
	        var menuList=await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	        expect(menuList).toBe(true,"Failed to display contextual menu list items");
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on Menu of AccNumber: <b>"+AccountNumber+"</b>");
	}
	
	async function verifyVivewStatementsHeader(){
	
	  appLog('Intiated method to verify account statement header');
	  var Status=await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
	  //kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");
	  expect(Status).toBe(true,"Failed to Navigate to Account Statements");
	  //appLog('Successfully Verified Account Statement Header');
	}
	
	async function VerifyPostedPendingTranscation(){
	
	  appLog('Intiated method to verify Pending and Posted Transcations');
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  var TransType=kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","segTransactions[0,-1]","lblTransactionHeader"], "text");
	  if(TransType.includes("Posted")||TransType.includes("Pending")){
	    //kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    //await kony.automation.playback.wait(5000);
	    appLog('Transfer Type is : '+TransType);
	  }else{
	    appLog('No Pending or Posted Transcation available');
	  }
	
	}
	
	
	async function VerifyStopChequePaymentScreen(){
	  
	  appLog("Intiated method to verify StopChequePayment Screen");
	  var Status=await kony.automation.playback.waitFor(["frmStopPayments","lblChequeBookRequests"],30000);
	  expect(Status).toBe(true,"Failed to Verify StopChequePayment screen");
	  appLog("Successfully Verified StopChequePayment");
	  
	}
	
	async function VerifyAccountAlertScreen(){
	  
	  appLog("Intiated method to verify AccountAlert Screen");
	  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAlertsHeading"],30000);
	  expect(Status).toBe(true,"Failed to Verify AccountAlert screen");
	  appLog("Successfully Verified AccountAlert");
	}
	
	async function MoveBack_Accounts_StopChequeBook(){
	  
	  appLog("Intiated method to Move back to Account Dashboard from StopPayments");
	  await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	
	}
	
	async function MoveBack_Accounts_ProfileManagement(){
	
	  // Move back to base state
	  appLog("Intiated method to Move back to Account Dashboard from ProfileManagement");
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	
	async function navigateToAccountPreferences(){
	
	  appLog("Intiated method to Navigate AccountPreferences Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAccountsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAccountsHeader"], "text")).toContain("Accounts");
	  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
	  expect(Status).toBe(true,"FAILED to load Accounts segment");
	  appLog("Successfully Navigated to AccountPreferences Screen");
	}
	
	async function clickonEditButton(){
	
	  appLog("Intiated method to click on Edit button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
	  // Even segAccounts is visible unable to click EDIT as there is a AppLoader
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"]);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAccountsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEditAccountsHeader"], "text")).toContain("Edit Account");
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Edit button");
	}
	
	async function EnableEStatement(){
	
	  appLog("Intiated method to Enable e-Statements");
	  
	  var Status=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblFavoriteEmailCheckBox"],"text");
	
	  if(Status==='D'){
	    appLog("Intiated method to click on enable e-Statements");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","flximgEnableEStatementsCheckBox"],15000);
	    kony.automation.flexcontainer.click(["frmProfileManagement","settings","flximgEnableEStatementsCheckBox"]);
	    appLog("Successfully Clicked on ENABLE e-Statement CheckBox");
	    // Accept Terms and conditions after enabling e-Statement
		//await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblIAccept"],15000);
		//expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblIAccept"], "text")).not.toBe("");
		var isTerms=await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxTCContentsCheckbox"],15000);
	    if(isTerms){
	      kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxTCContentsCheckbox"]);
	      appLog("Successfully accepted ENABLE e-Statement terms and conditions");
	    }
		
	  }else{
	    appLog("e-Statement is already enabled");
	  }
	}
	
	async function DisableEStatement(){
	
	  appLog("Intiated method to Disable e-Statements");
	  
	  var Status=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblFavoriteEmailCheckBox"],"text");
	
	  if(Status==='C'){
	    appLog("Intiated method to click on disable e-Statements");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","flximgEnableEStatementsCheckBox"],10000);
	    kony.automation.flexcontainer.click(["frmProfileManagement","settings","flximgEnableEStatementsCheckBox"]);
	    appLog("Successfully Clicked on DISABLE e-Statement CheckBox");
	    // Accept Terms and conditions after enabling e-Statement
		//await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblIAccept"],15000);
		//expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblIAccept"], "text")).not.toBe("");
		var isTerms=await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxTCContentsCheckbox"],15000);
	    if(isTerms){
	      kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxTCContentsCheckbox"]);
	      appLog("Successfully accepted ENABLE e-Statement terms and conditions");
	    }
		
	  }else{
	    appLog("e-Statement is already disabled");
	  }
	
	}
	
	async function clickOnSaveButton(){
	
	  appLog("Intiated method to click on SAVE button");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAccountsSave"],30000);
	  await kony.automation.scrollToWidget(["frmProfileManagement","settings","btnEditAccountsSave"]);
	  kony.automation.button.click(["frmProfileManagement","settings","btnEditAccountsSave"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on SAVE button");
	  //await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAccountsHeader"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAccountsHeader"], "text")).toContain("Accounts");
	  
	}
	
	async function MoveBackToDashBoard_ProfileManagement(){
	
	  // Move back to base state
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function navigateToBillPay(){
	
	  appLog("Intiated method to navigate to BillPay");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPay0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPay0flxMyAccounts"]);
	  appLog("Successfully clicked on BillPay option");
	  await kony.automation.playback.wait(5000);
	  var isPayeeScreen=await kony.automation.playback.waitFor(["frmBulkPayees","lblTransactions"],15000);
	  if(isPayeeScreen){
	    expect(kony.automation.widget.getWidgetProperty(["frmBulkPayees","lblTransactions"], "text")).not.toBe("");
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","lblWarning"],5000)){
	    appLog("Custom Message : Activate Billpay feature to proceed further");
	  }else{
	    appLog("Custom Message : Failed to Navigate to BillPay screen");
	  }
	}
	
	async function navigateToOneTimePayment(){
	
	  appLog("Intiated method to navigate to OneTimePayment");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  appLog("Successfully clicked on Menu on Dashboard");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"]);
	  appLog("Successfully clicked on Billpay option");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPay4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPay4flxMyAccounts"]);
	  appLog("Successfully clicked on OneTimePayment option");
	  await kony.automation.playback.wait(10000);
	
	}
	
	async function enterOneTimePayeeInformation(payeeName,zipcode,accno,accnoAgain,mobileno){
	
	  appLog("Intiated method to enter OneTime Payee Information");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],payeeName);
	  appLog("Successfully entered payee name to auto select : <b>"+payeeName+"</b>");
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"],15000);
	  kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);
	  appLog("Successfully selected payee name from list");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],zipcode);
	  appLog("Successfully entered zipcode : <b>"+zipcode+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],accno);
	  appLog("Successfully entered acc number : <b>"+accno+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],accnoAgain);
	  appLog("Successfully Re-entered account number : <b>"+accnoAgain+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],mobileno);
	  appLog("Successfully entered mobile number : <b>"+mobileno+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"],15000);
	  kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Next button");
	}
	
	async function enterOneTimePaymentdetails(amount,note){
	
	  appLog("Intiated method to enter details for OneTime payment");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],amount);
	  appLog("Successfully entered amount : <b>"+amount+"</b>");
	
	  appLog("Intiated method to Select Payee From Acc for OneTime payment");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmMakeOneTimePayment","txtTransferFrom"], [264,20],null,null);
	  kony.automation.flexcontainer.click(["frmMakeOneTimePayment","segTransferFrom[0,0]","flxAccountListItem"]);
	  appLog("Successfully selected Bill PayFrom");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],note);
	  appLog("Successfully entered note value : <b>"+note+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"],15000);
	  kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Next button");
	}
	
	async function confirmOneTimePaymnet(){
	
	  appLog("Intiated method to confirm OneTimePayment");
	
	  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	  appLog("Successfully accepted Checkbox");
	
	  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	async function verifyOneTimePaymentSuccessMsg(){
	
	  appLog("Intiated method to verify OneTimePayment SuccessMsg");
	
	  await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],30000);
	
	  if(success){
	    //await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],15000);
	    //expect(kony.automation.widget.getWidgetProperty(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","rtxDowntimeWarning"],5000)){
	    //appLog("Logged in User is not authorized to perform this action");
	    //fail('Logged in User is not authorized to perform this action');
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmMakeOneTimePayment","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmMakeOneTimePayment","rtxDowntimeWarning"],"text"));
	
	
	    await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else{
	    appLog("Unable to perform OneTimePayment");
	  }
	
	}
	
	
	async function navigateToManagePayee(){
	
	  await navigateToBillPay();
	  appLog("Intiated method to navigate to Manage Payee list");
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	  appLog("Successfully clicked on Manage payee Button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function selectPayee_ManagePayeeList(payeename){
	
	  appLog("Intiated method to select Payee from Manage Payee list : <b>"+payeename+"</b>");
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmManagePayees","manageBiller","txtSearch"],payeename);
	  appLog("Successfully entered Payee "+payeename);
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","btnConfirm"],15000);
	  kony.automation.flexcontainer.click(["frmManagePayees","manageBiller","btnConfirm"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	
	  //   await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","txtSearch"],15000);
	  //   kony.automation.textbox.enterText(["frmManagePayees","manageBiller","txtSearch"], [ { modifierCapsLock:true, key : 'A' },
	  //                                                             { modifierCapsLock:true, key : 'B' },
	  // 															{ modifierCapsLock:true, key : 'C' },
	  //                                                             { modifierCapsLock:false, keyCode : 13 }
	  // 														]);
	
	  appLog("Intiated Method to verify Payee <b>"+payeename+"</b>");
	
	  var PayeeList=await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  if(PayeeList){
	    //expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","lblColumn1"],"text")).toEqual(payeename);
	    expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","lblColumn1"],"text")).not.toBe('');
	  }else if(await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],5000)){
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],"text"));
	
	  }else{
	    appLog("Unable to find Payee in ManagePayees List");
	  }
	
	}
	
	async function clickOnBillPayBtn_ManagePayees(){
	
	
	  // BillPay and Active ebill has same locator hence verifying text and doing operation accordingly, Instead of directly failing.
	
	  appLog("Intiated method to click on Billpay button from Manage Payee list");
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	
	  var ButtonName=kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"], "text");
	
	  //appLog('Button Name is : '+ButtonName);
	
	  if(ButtonName==='Activate ebill'){
	
	    appLog("Info : <b>"+ButtonName+"</b>"+" is Available instead of BillPay button");
	    //Activate e Bill to convert button to PayaBill. instead of failing we can proceed execution
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    await kony.automation.playback.waitFor(["frmManagePayees","btnProceedIC"],15000);
	    kony.automation.button.click(["frmManagePayees","btnProceedIC"]);
	    appLog('Successfully clicked on YES button');
	    await kony.automation.playback.wait(10000);
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    appLog("Successfully clicked on BillPay button");
	
	  }else{
	
	    // We can directly click on BillPay button
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    appLog("Successfully clicked on BillPay button");
	    await kony.automation.playback.wait(5000);
	  }
	}
	
	
	async function enterAmount_SheduleBillPay(amount){
	
	  appLog("Intiated method to enter amount : <b>"+amount+"</b>");
	  await kony.automation.playback.waitFor(["frmPayABill","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmPayABill","txtSearch"],amount);
	  appLog("Successfully entered amount : <b>"+amount+"</b>");
	
	  await SelectPayFromAcc_SheduleBillPay();
	}
	
	async function SelectPayFromAcc_SheduleBillPay(){
	
	  appLog("Intiated method to Select Payee From");
	
	  await kony.automation.playback.waitFor(["frmPayABill","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmPayABill","txtTransferFrom"], [600,17],null,null);
	  kony.automation.flexcontainer.click(["frmPayABill","segTransferFrom[0,0]","flxAccountListItem"]);
	
	  appLog("Successfully selected Payee from the list");
	}
	
	async function selectfrequency_SheduledBillPay(freq){
	
	  appLog("Intiated method to select freq : <b>"+freq+"</b>");
	  await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"],15000);
	  kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"],freq);
	  appLog("Successfully selected freq : "+freq);
	}
	
	async function SelectDateRange_SheduledBillpay() {
	
	  //new chnage in 202010
	  //await kony.automation.playback.wait(5000);
	  appLog("Intiated method to select DateRange");
	  await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"],15000);
	  kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "ON_SPECIFIC_DATE");
	
	  await kony.automation.playback.waitFor(["frmPayABill","calSendOn"],15000);
	  kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	  appLog("Successfully selected sendOn Date");
	  await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"],15000);
	  kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,21,2021]);
	  appLog("Successfully selected EndOn Date");
	}
	
	async function SelectSendOnDate_SheduledBillpay() {
	
	  await kony.automation.playback.waitFor(["frmPayABill","calSendOn"],15000);
	  kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	  appLog("Successfully selected sendOn Date");
	}
	
	async function SelectOccurences_SheduledBillPay(occurences) {
	  //new chnage in 202010
	  appLog("Intiated method to select N.of Occurences");
	  await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"],15000);
	  kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "NO_OF_RECURRENCES");
	  await kony.automation.playback.waitFor(["frmPayABill","txtEndingOn"],15000);
	  kony.automation.textbox.enterText(["frmPayABill","txtEndingOn"],occurences);
	  appLog("Successfully selected Occurences : <b>"+occurences+"</b>");
	}
	
	async function EnterNoteValue_SheduledBillPay(notes) {
	
	  appLog("Intiated method to enter note value");
	  await kony.automation.playback.waitFor(["frmPayABill","txtNotes"],15000);
	  kony.automation.textbox.enterText(["frmPayABill","txtNotes"],notes);
	  appLog("Successfully entered Note value : <b>"+notes+"</b>");
	
	  appLog("Intiated method to click on Confirm button");
	  await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"],15000);
	  kony.automation.button.click(["frmPayABill","btnConfirm"]);
	  appLog("Successfully clicked on Confirm button");
	}
	
	async function confirmSheduledBillpay(){
	
	  appLog("Intiated method to Confirm Sheduled BillPayment");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	  appLog("Successfully accepted terms check box");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	  appLog("Successfully clicked on Confirm button");
	}
	
	async function cancelSheduledBillPay(){
	
	  appLog("Intiated method to CANCEL Sheduled BillPayment");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","btnCancel"],15000);
	  kony.automation.button.click(["frmPayBillConfirm","btnCancel"]);
	  appLog("Successfully clicked on Cancel button");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to cancel this transaction?");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","btnYes"],15000);
	  kony.automation.button.click(["frmPayBillConfirm","CancelPopup","btnYes"]);
	  appLog("Successfully clicked on YES button");
	
	  await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	  appLog("Successfully MovedBack to Account DashBoard");
	}
	
	async function verifySheduledBillpaySuccessMsg(){
	
	  appLog("Intiated method to verify Sheduled BillPay SuccessMsg");
	
	  await kony.automation.playback.wait(5000);
	  var Success= await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],30000);
	
	  if(Success){
	    //expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	    appLog("Successfully MovedBack to Account DashBoard");
	  }else if(await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000)){
	    //Checking for exception message
	    //Move back to dashboard again there is an exception message
	    appLog("Exception while performing a Sheduled BillPay");
	    await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	    await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	    expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	    //appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	    //fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	    appLog("Failed : Unable to Perform Successfull Transcation. Failed with rtxDowntimeWarning");
	    fail("Failed : Unable to Perform Successfull Transcation. Failed with rtxDowntimeWarning");
	  }else{
	    appLog("Unable to verify Success Message");
	  }
	
	}
	
	async function navigateToSheduledBillPay(){
	
	  await navigateToBillPay();
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnScheduled"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnScheduled"]);
	  appLog("Successfully clicked on Sheduled tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickOnEditButton_SheduledBillPayment(){
	
	  appLog("Intiated method to click on Edit button");
	  await kony.automation.playback.waitFor(["frmBillPayScheduled","segmentBillpay"],15000);
	  kony.automation.button.click(["frmBillPayScheduled","segmentBillpay[0]","btnEdit"])
	  appLog("Successfully clicked on Edit button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmPayABill","lblPayABill"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayABill","lblPayABill"],"text")).toEqual("Pay a Bill");
	
	}
	
	async function UpdatedSheduledBillPayment(notes){
	
	  await SelectPayFromAcc_SheduleBillPay();
	  await selectfrequency_SheduledBillPay("Once");
	  await EnterNoteValue_SheduledBillPay(notes);
	  await confirmSheduledBillpay();
	
	}
	async function EditSheduledBillPay(notes){
	
	  var nopayments=await kony.automation.playback.waitFor(["frmBillPayScheduled","rtxNoPaymentMessage"],15000);
	  if(nopayments){
	    appLog("There are no sheduled payments");
	    //Move back to accounts
	    await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],15000);
	    kony.automation.button.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	    appLog("Successfully MovedBack to Account DashBoard");
	  }else{
	
	    appLog("There are few sheduled payments");
	    await clickOnEditButton_SheduledBillPayment();
	    await UpdatedSheduledBillPayment(notes);
	    var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000);
	    if(warning){
	      await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	      await verifyAccountsLandingScreen();
	      appLog("Successfully MovedBack to Account DashBoard");
	      //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	      appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	      fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	
	    }else{
	      await verifySheduledBillpaySuccessMsg();
	      await verifyAccountsLandingScreen();
	    }  
	
	  }
	}
	
	async function clickOnAddPayeeLink(){
	
	  appLog("Intiated method to click on Add payee link");
	  await kony.automation.playback.waitFor(["frmBulkPayees","flxAddPayee"],15000);
	  kony.automation.flexcontainer.click(["frmBulkPayees","flxAddPayee"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on addPayee link");
	}
	
	async function enterPayeeDetails_UsingPayeeinfo(payeeName,address1,address2,city,zipcode,accno,note){
	
	  appLog("Intiated method to Add Payee Details");
	
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","btnEnterPayeeInfo"],15000);
	  kony.automation.button.click(["frmAddPayee1","addPayee","btnEnterPayeeInfo"]);
	  appLog("Successfully Clicked on EnterPayeeInfo Tab");
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","tbx1Tab2"],15000);
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx1Tab2"],payeeName);
	  appLog("Successfully Entered Payee name as : <b>"+payeeName+"</b>");
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","tbx2Tab2"],15000);
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx2Tab2"],address1);
	  appLog("Successfully Entered Address Line1 as : <b>"+address1+"</b>");
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx3Tab2"],address2);
	  appLog("Successfully Entered Address Line1 as : <b>"+address2+"</b>");
	  kony.automation.listbox.selectItem(["frmAddPayee1","addPayee","lbxCountry"], "IN");
	  kony.automation.listbox.selectItem(["frmAddPayee1","addPayee","lbxStateValue"], "IN-KA");
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx5Tab2"],city);
	  appLog("Successfully Entered CityName as : <b>"+city+"</b>");
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbxZipCodeTab2"],zipcode);
	  appLog("Successfully Entered Zipcode as : <b>"+zipcode+"</b>");
	  kony.automation.flexcontainer.click(["frmAddPayee1","addPayee","flxContainer"]);
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx6Tab2"],accno);
	  appLog("Successfully Entered account number as : <b>"+accno+"</b>");
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx7Tab2"],accno);
	  appLog("Successfully Re-Entered account number as : <b>"+accno+"</b>");
	  kony.automation.button.click(["frmAddPayee1","addPayee","btnRight1"]);
	
	}
	
	async function clickOnNextButton_payeeDetails(){
	
	  appLog("Intiated method verify Payee Details");
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","editDetailsBtnRight1"],15000);
	  kony.automation.button.click(["frmAddPayee1","addPayee","editDetailsBtnRight1"]);
	  appLog("Successfully Clicked on Next button ");
	
	  //await linkPayee();
	}
	
	async function SelectPayeeBankingType_payeeDetails(BankingType){
	
	  appLog("Intiated method to click on AddRecepientContinue");
	  var btnAddRecepient=await kony.automation.playback.waitFor(["frmPayeeDetails","btnAddRecepientContinue"],15000);
	  if(btnAddRecepient){
	    kony.automation.button.click(["frmPayeeDetails","btnAddRecepientContinue"]);
	    appLog("Successfully Clicked on AddRecepientContinue button ");
	    await kony.automation.playback.wait(5000);
	  }else{
	    appLog("Selecting Banking type screen is not available");
	  }
	
	}
	
	async function linkPayee(){
	
	  var linkreciptent=await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","contractList","lblHeader"],15000);
	
	  if(linkreciptent){
	    await kony.automation.playback.wait(5000);
	    kony.automation.widget.touch(["frmAddPayee1","addPayee","contractList","lblCheckBoxSelectAll"], [9,10],null,null);
	    kony.automation.flexcontainer.click(["frmAddPayee1","addPayee","contractList","flxCol4"]);
	    appLog("Successfully selected select All CheckBox");
	    kony.automation.button.click(["frmAddPayee1","addPayee","contractList","btnAction6"]);
	    appLog("Successfully Clicked on Link Reciptent Continue Button");
	  }
	}
	
	async function clickOnConfirmButton_verifyPayee(){
	
	  appLog("Intiated method to confirm Payee Details");
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","btnAction6"],15000);
	  kony.automation.button.click(["frmAddPayee1","addPayee","btnAction6"]);
	  appLog("Successfully Clicked on Confirm button ");
	}
	
	async function verifyAddPayeeSuccessMsg(){
	
	  appLog("Intiated method to verify Add payee SuccessMsg");
	  //await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","lblSection1Message"],30000);
	
	  if(success){
	    expect(kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","lblSection1Message"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","rtxDowntimeWarning"],5000)){
	
	    appLog("Intiated method to verify DowntimeWarning");
	    await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","rtxDowntimeWarning"],15000);
	    kony.automation.flexcontainer.click(["frmAddPayee1","addPayee","rtxDowntimeWarning"]);
	
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","rtxDowntimeWarning"],"text"));
	
	  }else{
	    appLog("Unable to add Payee");
	  }
	
	}
	
	
	async function expandPayee_ManagePayee(){
	
	  appLog("Intiated method to Expand payee from Manage payee");
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  kony.automation.flexcontainer.click(["frmManagePayees","manageBiller","segmentBillPay[0]","flxDropdown"]);
	  appLog("Successfully clicked on Manage Payees dropdown arrow");
	}
	
	async function MoveBackToDashBoard_ManagePayees(){
	
	  await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	async function clickOnEditBtn_ManagePayees(){
	
	  await expandPayee_ManagePayee();
	  appLog("Intiated method to Edit Biller");
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btn3"]);
	  appLog("Successfully clicked on Editbutton under manage payee");
	}
	
	async function deletePayee_ManagePayee(){
	
	  appLog("Intiated method to Delete Payee");
	  await expandPayee_ManagePayee();
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btn4"]);
	  appLog("Successfully clicked on Delete button under manage payee");
	  await kony.automation.playback.waitFor(["frmManagePayees","btnYesIC"],15000);
	  kony.automation.button.click(["frmManagePayees","btnYesIC"]);
	  appLog("Successfully clicked on YES button on delete biller");
	  await kony.automation.playback.wait(5000);
	  await MoveBackToDashBoard_ManagePayees();
	}
	
	async function EditPayee_ManagePayee(){
	
	  appLog("Intiated method to Edit Payee");
	
	  await clickOnEditBtn_ManagePayees();
	
	//   appLog("Intiated method to updated biller");
	//   await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","tbxName"],15000);
	//   kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbxName"],"EditBiller");
	//   appLog("Successfully entered Updated biller value");
	
	  appLog("Intiated method to click on Continue button");
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","btnSave"],15000);
	  kony.automation.button.click(["frmAddPayee1","addPayee","btnSave"]);
	  appLog("Successfully Clicked on Save button")
	
	  // Currently not present on UI
	  //   appLog("Intiated method to click on Savelink Continue button");
	  //   await kony.automation.playback.waitFor(["frmManagePayees","contractList","btnAction6"],15000);
	  //   kony.automation.button.click(["frmManagePayees","contractList","btnAction6"]);
	  //   await kony.automation.playback.wait(5000);
	  //   appLog("Successfully Clicked on Savelink Continue button");
	
	  await verifyUpdatePayeeSuccessMsg();
	
	}
	
	
	async function verifyUpdatePayeeSuccessMsg(){
	
	  appLog("Intiated method to verify Update payee SuccessMsg");
	  await kony.automation.playback.wait(5000);
	  var successMsg=await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","lblSection1Message"],30000);
	  if(successMsg){
	    expect(kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","lblSection1Message"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","rtxDowntimeWarning"],5000)){
	    appLog("Intiated method to verify DowntimeWarning");
	    await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","rtxDowntimeWarning"],15000);
	    kony.automation.flexcontainer.click(["frmAddPayee1","addPayee","rtxDowntimeWarning"]);
	
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","rtxDowntimeWarning"],"text"));
	
	  }else{
	    appLog("Unable to add Payee");
	  }
	
	}
	
	async function navigateToPastBillPay(){
	
	  appLog("Intiated method to navigate to Billpay History");
	  await navigateToBillPay();
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnHistory"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnHistory"]);
	  appLog("Successfully clicked on History tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickonRepeatButton_PastBillpay(){
	
	  appLog("Intiated method to click on Repeat button");
	  await kony.automation.playback.waitFor(["frmBillPayHistory","segmentBillpay"],15000);
	  kony.automation.button.click(["frmBillPayHistory","segmentBillpay[0]","btnRepeat"]);
	  appLog("Successfully clicked on Repeat tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function repeatPastBillPayment(note){
	
	  appLog("Intiated method to Repeat a BillPay");
	
	  var nopayments=await kony.automation.playback.waitFor(["frmBillPayHistory","rtxNoPaymentMessage"],15000);
	
	  if(nopayments){
	    appLog("There are no History payments");
	    //Move back to accounts
	    await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],15000);
	    kony.automation.button.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else{
	
	    await clickonRepeatButton_PastBillpay();
	    await SelectPayFromAcc_SheduleBillPay();
	    await EnterNoteValue_SheduledBillPay(note);
	    await confirmSheduledBillpay();
	
	    var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000);
	    if(warning){
	      await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	      await verifyAccountsLandingScreen();
	      appLog("Successfully Moved back to Accounts dashboard");
	      //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	      appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	      fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	
	
	    }else{
	      await verifySheduledBillpaySuccessMsg();
	      await verifyAccountsLandingScreen();
	      appLog("Successfully Moved back to Accounts dashboard");
	    }
	
	  }
	}
	
	async function clickOnAllpayeesTab(){
	
	  appLog("Intiated method to click on Allpayees tab");
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnAllPayees"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnAllPayees"]);
	  appLog("Successfully clicked on Allpayees tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function verifyAllPayeesList(){
	
	  appLog("Intiated method to verify Allpayees List");
	
	  var PayeeList=await kony.automation.playback.waitFor(["frmBulkPayees","segmentBillpay"],15000);
	
	  if(PayeeList){
	    kony.automation.flexcontainer.click(["frmBulkPayees","segmentBillpay[0]","flxDropdown"]);
	    appLog("Successfully verified on Allpayees List");
	  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","rtxNoPaymentMessage"],5000)){
	
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmBulkPayees","rtxNoPaymentMessage"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmBulkPayees","rtxNoPaymentMessage"],"text"));
	
	  }else {
	    appLog("Unable to verify Allpayees List");
	  }
	
	}
	
	async function MoveBackToDashBoard_AllPayees(){
	
	  await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	}
	
	async function clickOnSavePayeeButton_OneTimePay(){
	
	  appLog("Intiated method to Save Payee from OneTime Payment");
	
	  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","btnSavePayee"],15000);
	  kony.automation.button.click(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
	  appLog("Successfully Clicked on Save button");
	
	  //Continue Button
	  await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"],15000);
	  kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	  appLog("Successfully Clicked on Continue button");
	
	  //Confirm Button
	  await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"],15000);
	  kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm button");
	
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAddPayee"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAddPayee"],"text")).toEqual("Add Payee");
	
	
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
	  appLog("Successfully verified Added payee");
	
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","btnViewAllPayees"],15000);
	  kony.automation.button.click(["frmPayeeAcknowledgement","btnViewAllPayees"]);
	  appLog("Successfully clicked on ViewAll payees button");
	
	}
	
	async function activateBillPayTermsconditions(){
	
	  appLog("Intiated method to Activate Billpay TC's");
	
	  var warning=await kony.automation.playback.waitFor(["frmBillPayActivation","lblWarning"],15000);
	  if(warning){
	    //expect(kony.automation.widget.getWidgetProperty(["frmBillPayActivation","lblWarning"], "text")).toEqual("Please activate My Bills.");
	    appLog("Intiated method to select Default account");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","txtTransferFrom"],15000);
	    kony.automation.widget.touch(["frmBillPayActivation","txtTransferFrom"], [97,15],null,null);
	    kony.automation.flexcontainer.click(["frmBillPayActivation","segTransferFrom[0,0]","flxAmount"]);
	    appLog("Successfully Selected Default BillPay Acc");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","lblFavoriteEmailCheckBox"],15000);
	    kony.automation.widget.touch(["frmBillPayActivation","lblFavoriteEmailCheckBox"], null,null,[14,13]);
	    appLog("Successfully accepted checkbox");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","flxAgree"],15000);
	    kony.automation.flexcontainer.click(["frmBillPayActivation","flxAgree"]);
	    appLog("Successfully clicked on AgreeFlex");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","btnProceed"],15000);
	    kony.automation.button.click(["frmBillPayActivation","btnProceed"]);
	    appLog("Successfully clicked on Proceed button");
	    var Success=await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],15000)
	    if(Success){
	      kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	      appLog('Successfully Acivated BillPay feature');
	    }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","rtxErrorMessage"],5000)){
	      fail('Error while activating BillPay feature');
	      await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	    }else {
	      fail('Failed to activate Billpay Feature');
	    }
	  }else{
	    appLog("Already accepted billpay activation..Moveback to dashboard");
	    await MoveBackToDashBoard_AllPayees();
	  }
	}
	
	async function activateNewlyAddedpayee(){
	
	  appLog('Intiated method to activate Newly Added Payee');
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	
	  var ButtonName=kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"], "text");
	
	  //appLog('Button Name is : '+ButtonName);
	
	  if(ButtonName==='Activate ebill'){
	
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    appLog('Successfully clicked on activate button');
	
	    var activate=await kony.automation.playback.waitFor(["frmManagePayees","lblWarningOneIC"],15000);
	    if(activate){
	      await kony.automation.playback.waitFor(["frmManagePayees","btnProceedIC"],15000);
	      kony.automation.button.click(["frmManagePayees","btnProceedIC"]);
	      appLog('Successfully clicked on YES button');
	      await kony.automation.playback.wait(10000);
	      await MoveBackToDashBoard_ManagePayees();
	    }else {
	      appLog('Failed : Unable to Activate Added Payee');
	      fail('Failed : Unable to Activate Added Payee');
	    }
	  }else {
	    appLog('Payee Already activated');
	  }
	
	}
	
	async function navigateToCardManegement(){
	
	  appLog("Intiated method to Navigate Card Management Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS4flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","lblMyCardsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","lblMyCardsHeader"], "text")).toEqual("My Cards");
	
	  appLog("Successfully Navigated to Card Management Screen");
	}
	async function VerifyNoCardsError(){
	
	  noCardsError=false;
	  appLog("Intiated method to verify Cards available or Not");
	  var noCardsError=await kony.automation.playback.waitFor(["frmCardManagement","myCards","lblNoCardsError"],30000);
	  //var noCardsError=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","lblNoCardsError"], "text");
	  appLog("no Cards Error :: "+noCardsError);
	  return noCardsError;
	}
	
	async function applyforNewCard(){
	
	  appLog("Intiated method to apply for new Card");
	
	  if(await VerifyNoCardsError()){
	    appLog("No cards avalable - applyNew");
	    await kony.automation.playback.waitFor(["frmCardManagement","myCards","btnApplyForCard"],15000);
	    kony.automation.button.click(["frmCardManagement","myCards","btnApplyForCard"]);
	    appLog("Successfully clicked on Apply Card button");
	  }else{
	    appLog("Cards are available already- Request new Card");
	    await kony.automation.playback.waitFor(["frmCardManagement","flxRequestANewCard"],15000);
	    kony.automation.flexcontainer.click(["frmCardManagement","flxRequestANewCard"]);
	    appLog("Successfully clicked on Request new Card button");
	  }
	
	}
	
	async function addNewCardDetails(){
	
	  appLog("Intiated method to add new card details");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","segAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","segAccounts[0]","flxAccountNameWrapper"]);
	  appLog("Successfully clicked on Flex accounts");
	  await kony.automation.playback.waitFor(["frmCardManagement","segCardProducts"],15000);
	  kony.automation.button.click(["frmCardManagement","segCardProducts[0]","btnSelect"]);
	  appLog("Successfully clicked on btnSelect");
	  await kony.automation.playback.waitFor(["frmCardManagement","tbxNameOnCard"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","tbxNameOnCard"],"JasmineCard"+getRandomString(5));
	  appLog("Successfully entered card name ");
	  await kony.automation.playback.waitFor(["frmCardManagement","tbxEnterCardPIN"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","tbxEnterCardPIN"],"5050");
	  appLog("Successfully entered card PIN ");
	  await kony.automation.playback.waitFor(["frmCardManagement","tbxConfirmCardPIN"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","tbxConfirmCardPIN"],"5050");
	  appLog("Successfully Re-entered card PIN ");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnNewCardContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnNewCardContinue"]);
	  appLog("Successfully clicked on CONTINUE button");
	
	  var Success=await kony.automation.playback.waitFor(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"],30000);
	  expect(Success).toEqual(true,"Failed to add new card details");
	
	  await MoveBackfrom_MyCards();
	}
	async function activateNewCard(){
	
	  appLog("Intiated method to activate new card");
	  var MyValues=await ClickOnCardOptions("Activate Card");
	  if(MyValues[0]===true){
	    await kony.automation.playback.waitFor(["frmCardManagement","tbxCVVNumber"],15000);
	    //appLog("Before converting is : "+MyValues[1]);
	    var myCVV=""+MyValues[1].toString();
	    appLog("Intiated method to enter CVV as : "+myCVV);
	    kony.automation.textbox.enterText(["frmCardManagement","tbxCVVNumber"],myCVV);
	    appLog("Successfully entered CVV as : "+myCVV);
	    await kony.automation.playback.waitFor(["frmCardManagement","btnContinue2"],15000);
	    kony.automation.button.click(["frmCardManagement","btnContinue2"]);
	    var Success=await kony.automation.playback.waitFor(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"],30000);
	    //expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"], "text")).toEqual("activated");
	    expect(Success).toEqual(true,"Failed to Activate new card");
	
	    await MoveBackfrom_MyCards();
	  }else{
	    appLog("all cards are activated alreay");
	    await MoveBackfrom_MyCards();
	  }
	}
	async function VerifyMaskedAccountNumber(){
	
	  appLog("Intiated method to verify Masked account number");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards[0]","rtxValue1"], "text")).toContain("XXXX");
	  appLog("Successfully Validated Masked account number");
	}
	
	async function MoveBackfrom_MyCards(){
	
	  appLog("Intiated method to moveback from CardManagement");
	  await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	  appLog("Successfully Moved to DashBoard Screen");
	
	}
	
	async function ClickOnCardOptions(CardOption){
	
	  // Possible Options were : Lock Card,Replace Card,Report Lost,Set Limits,Change PIN,Activate Card
	
	  appLog("Intiated method to click on Option is : <b>"+CardOption+"</b>");
	  var Status=false;
	  var maskedAccNumber="";
	  var cvvnumber="";
	  var Values = new Array();
	
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  var segData=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards"],"data");
	  var segLength=segData.length;
	  //appLog("Length is : "+segLength);
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	    for(var y = 1; y <=5; y++){
	      //appLog("x value is : "+x);
	      //appLog("y value is : "+y);
	      var Option=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards["+x+"]","btnAction"+y], "text");
	      if(CardOption===Option){
	        appLog("Successfully found option : <b>"+CardOption+"</b>");
	        // get account number before clicking on it
	        var accnumber=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards["+x+"]","rtxValue1"], "text");
	        kony.automation.button.click(["frmCardManagement","myCards","segMyCards["+x+"]","btnAction"+y]);
	        await kony.automation.playback.wait(5000);
	        appLog("Successfully clicked on Option : <b>"+CardOption+"</b>");
	        finished = true;
	        Status=true;
	        maskedAccNumber=accnumber;
	        // get these value before Break statement
	        cvvnumber=await getCVVNumber(maskedAccNumber);
	        Values[0]=Status;
	        Values[1]=cvvnumber;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to get Option from List : <b>"+CardOption+"</b>");
	  return Values;
	}
	
	async function getCVVNumber(maskedAccNumber){
	
	  //appLog("Intiated method to get required values from account number");
	  var CVV=maskedAccNumber.substring(16,19);
	  appLog("CVV is : "+CVV);
	  return CVV;
	}
	
	async function VerifylockCard(){
	
	  await ClickOnCardOptions("Lock Card");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"], "text")).not.toBe("");
	  appLog("Successfully verified LockCard Screen");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","CardActivation","flxCheckbox"],15000);
	  kony.automation.widget.touch(["frmCardManagement","CardLockVerificationStep","CardActivation","flxCheckbox"], null,null,[45,33]);
	  appLog("Successfully Clicked on CheckBox");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Confirm Button");
	
	  await VerifyCardTransactionMessage();
	}
	
	async function VerifyUnlockCard(){
	
	  await ClickOnCardOptions("Unlock Card");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","CardActivation","lblHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","CardActivation","lblHeader"], "text")).not.toBe("");
	  appLog("Successfully Verified Unlock Card form");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardActivation","CardActivation","flxCheckbox"],15000);
	  kony.automation.widget.touch(["frmCardManagement","CardActivation","CardActivation","flxCheckbox"], null,null,[45,33]);
	  appLog("Successfully Clicked on CheckBox");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardActivation","btnProceed"],15000);
	  kony.automation.button.click(["frmCardManagement","CardActivation","btnProceed"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Continue Button");
	
	  await VerifyCardTransactionMessage();
	}
	
	async function VerifyChangePIN(){
	
	  await ClickOnCardOptions("Change PIN");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"], "text")).not.toBe("");
	  appLog("Successfully Verified Change PIN Card form");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","tbxCurrentPIN"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxCurrentPIN"],"5050");
	  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxNewPIN"],"5050");
	  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxConfirmPIN"],"5050");
	  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxNote"],"Change PIN");
	  appLog("Successfully Entered Change PIN details");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await VerifyCardTransactionMessage();
	}
	
	async function setDailyWithDrawLimit(){
	
	  await ClickOnCardOptions("Set Limits");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","lblSetCardLimitsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblSetCardLimitsHeader"], "text")).toEqual("Set Card Limits");
	  appLog("Successfully Verified SetLimits form");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnEditLimitWithdrawal"],15000);
	  kony.automation.button.click(["frmCardManagement","btnEditLimitWithdrawal"]);
	  appLog("Successfully Clicked on EditLimitWithdrawal Button");
	  await kony.automation.playback.waitFor(["frmCardManagement","limitWithdrawalSlider"],15000);
	  kony.automation.slider.slide(["frmCardManagement","limitWithdrawalSlider"], 12450);
	  await kony.automation.playback.waitFor(["frmCardManagement","confirmButtons","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","confirmButtons","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	  await VerifyCardTransactionMessage();
	
	
	}
	
	async function setDailyPurchaseLimit(){
	
	  await ClickOnCardOptions("Set Limits");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","lblSetCardLimitsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblSetCardLimitsHeader"], "text")).toEqual("Set Card Limits");
	  appLog("Successfully Verified SetLimits form");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnEditLimitPurchase"],15000);
	  kony.automation.button.click(["frmCardManagement","btnEditLimitPurchase"]);
	  appLog("Successfully Clicked on EditLimitPurchase Button");
	  kony.automation.slider.slide(["frmCardManagement","limitPurchaseSlider"], 7250);
	  kony.automation.button.click(["frmCardManagement","confirmButtons","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	  await VerifyCardTransactionMessage();
	
	}
	
	async function VerifyCardTransactionMessage(){
	
	  var Success=await kony.automation.playback.waitFor(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"],30000);
	  if(Success){
	    appLog("Intiated method to Verify Transfer SuccessMessage");
	    expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"], "text")).not.toBe("");
	    await MoveBackfrom_MyCards();
	  }else if(await kony.automation.playback.waitFor(["frmCardManagement","rtxDowntimeWarning"],5000)){
	    appLog("Failed with : rtxDowntimeWarning");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmCardManagement","rtxDowntimeWarning"], "text"));
	    await MoveBackfrom_MyCards();
	  }else {
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	async function NavigateToManageTravelPlan(){
	
	  appLog("Intiated method to navigate to ManageTravelPlan form");
	  await kony.automation.playback.waitFor(["frmCardManagement","flxMangeTravelPlans"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","flxMangeTravelPlans"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","lblMyCardsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","lblMyCardsHeader"], "text")).not.toBe("");
	  appLog("Successfully Verified TravelPlan form");
	}
	
	async function CreateNewTravelPlan(Destination,PhoneNumber,NoteValue){
	
	  await NavigateToManageTravelPlan();
	
	  appLog("Intiated method to create new Travel Plan");
	  await kony.automation.playback.waitFor(["frmCardManagement","flxMangeTravelPlans"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","flxMangeTravelPlans"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","lblMyCardsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblMyCardsHeader"], "text")).toContain("Travel Plan");
	  appLog("Successfully Verified New TravelPlan form");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","calFrom"],15000);
	  kony.automation.calendar.selectDate(["frmCardManagement","calFrom"], [5,25,2021]);
	  await kony.automation.playback.waitFor(["frmCardManagement","calTo"],15000);
	  kony.automation.calendar.selectDate(["frmCardManagement","calTo"], [5,30,2021]);
	  appLog("Successfully Entered Travel Plan Calender");
	  await kony.automation.playback.waitFor(["frmCardManagement","txtDestination"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","txtDestination"],Destination);
	  await kony.automation.playback.waitFor(["frmCardManagement","segDestinationList"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","segDestinationList[1]","flxCustomListBox"]);
	  kony.automation.flexcontainer.click(["frmCardManagement","flxAddFeatureRequestandimg"]);
	  appLog("Successfully Added Destination");
	  await kony.automation.playback.waitFor(["frmCardManagement","txtPhoneNumber"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","txtPhoneNumber"],PhoneNumber);
	  appLog("Successfully entered Mobile number");
	  await kony.automation.playback.waitFor(["frmCardManagement","txtareaUserComments"],15000);
	  kony.automation.textarea.enterText(["frmCardManagement","txtareaUserComments"],NoteValue);
	  appLog("Successfully entered Travel Note values");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnContinue"]);
	  appLog("Successfully Clicked on continue button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  kony.automation.widget.touch(["frmCardManagement","myCards","segMyCards[0]","flxCheckBox"], null,null,[26,27]);
	  appLog("Successfully Clicked on CheckBox");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnCardsContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnCardsContinue"]);
	  appLog("Successfully Clicked on Continue Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","lblConfirmTravelPlan"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblConfirmTravelPlan"], "text")).toEqual("Confirm Travel Plan");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	  await kony.automation.playback.wait(5000);
	
	  await VerifyCardTransactionMessage();
	}
	
	
	async function EditTravelPlan(UpdatedNumber){
	
	  await NavigateToManageTravelPlan();
	
	  appLog("Intiated method to Edit Travel Plan");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  kony.automation.button.click(["frmCardManagement","myCards","segMyCards[0]","btnAction1"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","txtPhoneNumber"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","txtPhoneNumber"],UpdatedNumber);
	  appLog("Successfully Updated Phone Number");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnContinue"]);
	  appLog("Successfully Clicked on Continue Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  kony.automation.widget.touch(["frmCardManagement","myCards","segMyCards[0]","flxCheckBox"], null,null,[26,28]);
	  appLog("Successfully Clicked on CheckBox");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnCardsContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnCardsContinue"]);
	  appLog("Successfully Clicked on Continue Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	  await VerifyCardTransactionMessage();
	}
	
	async function DeleteTravelPlan(){
	
	  await NavigateToManageTravelPlan();
	
	  appLog("Intiated method to Delete Travel Plan");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  kony.automation.button.click(["frmCardManagement","myCards","segMyCards[0]","btnAction2"]);
	  appLog("Successfully Clicked on Delete Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","CustomAlertPopup","btnYes"],15000);
	  kony.automation.button.click(["frmCardManagement","CustomAlertPopup","btnYes"]);
	  appLog("Successfully Clicked on YES Button");
	  await kony.automation.playback.wait(5000);
	
	  await MoveBackfrom_MyCards();
	
	}
	
	async function searchforCards(SaerchCard){
	
	  appLog("Intiated method to Search for Cards");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","txtSearch"],15000);
	  kony.automation.widget.touch(["frmCardManagement","myCards","txtSearch"], [113,16],null,null);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","myCards","txtSearch"],SaerchCard);
	  appLog("Successfully entered card : <b>"+SaerchCard+"</b>");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","btnConfirm"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","myCards","btnConfirm"]);
	  appLog("Successfully clicked on Confirm/Search button");
	
	  await MoveBackfrom_MyCards();
	}
	
	
	
	async function navigateToCombinedStatements(){
	
	  await navigateToAccontStatements();
	  
	  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","btnCombinedStatements"],15000);
	  kony.automation.button.click(["frmAccountsDetails","viewStatementsnew","btnCombinedStatements"]);
	  await kony.automation.playback.wait(5000);
	
	  appLog("Successfully Navigated to CombinedStatement Screen");
	}
	
	async function navigateToAccontStatements(){
	
	  appLog("Intiated method to Navigate CombinedStatement Screen");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS7flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS7flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).not.toBe(""); 
	}
	
	async function verifyDownLoadOption(){
	
	  appLog("Intiated method to verify Download option");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblDownload"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblDownload"], "text")).not.toBe("");
	
	}
	
	async function clickOnGenarateNewStatement(){
	
	  appLog("Intiated method to click on Genarate new Statement");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","btnCombinedStatements"],15000);
	  kony.automation.button.click(["frmAccountsDetails","viewStatementsnew","btnConfirm"]);
	  appLog("Successfully clicked on to Genarate new Statement button");
	
	}
	
	async function ValidateDefaultFilterValue(){
	
	  appLog("Intiated method to validate default filter value");
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","lblSelectedFilter"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmConsolidatedStatements","lblSelectedFilter"], "text")).toContain("All Accounts");
	}
	
	async function selectPreferedAccountsFilter(){
	
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","flxFilterListBox"],15000);
	  kony.automation.flexcontainer.click(["frmConsolidatedStatements","flxFilterListBox"]);
	}
	
	
	async function selectFromDate(){
	
	  appLog("Intiated method to Select Statement From Date");
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","calFromDate"],15000);
	  kony.automation.calendar.selectDate(["frmConsolidatedStatements","calFromDate"], [4,17,2021]);
	  //var today = new Date();
	  //kony.automation.calendar.selectDate(["frmConsolidatedStatements","calFromDate"], [(today.getDate()-2),(today.getMonth()+1),today.getFullYear()]);
	  appLog("Successfully selected From date for statement");
	}
	
	async function selectAccountforStatement(){
	
	  appLog("Intiated method to Select From acc for Statement");
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","segAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmConsolidatedStatements","segAccounts[0,-1]","flxCheckBox"]);
	}
	
	async function clickOnCombinedStatementContinueButton(){
	
	  appLog("Intiated method to Click on ContinueButton");
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","btnDownloadStatements"],15000);
	  await kony.automation.scrollToWidget(["frmConsolidatedStatements","btnDownloadStatements"]);
	  kony.automation.button.click(["frmConsolidatedStatements","btnDownloadStatements"]);
	  appLog("Successfully Clicked on ContinueButton");
	}
	
	async function selectFormatAsPDF(){
	
	  appLog("Intiated method to select PDF format");
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","flxFilterListBoxPopup"],15000);
	  kony.automation.flexcontainer.click(["frmConsolidatedStatements","flxFilterListBoxPopup"]);
	  kony.automation.flexcontainer.click(["frmConsolidatedStatements","segFileFilters[0]","flxFileFilter"]);
	
	}
	
	async function selectFormatAsExcel(){
	
	  appLog("Intiated method to select Excel format");
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","flxFilterListBoxPopup"],15000);
	  kony.automation.flexcontainer.click(["frmConsolidatedStatements","flxFilterListBoxPopup"]);
	  kony.automation.flexcontainer.click(["frmConsolidatedStatements","segFileFilters[1]","flxFileFilter"]);
	}
	
	async function selectFormatAsCSV(){
	
	  appLog("Intiated method to select CSV format");
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","flxFilterListBoxPopup"],15000);
	  kony.automation.flexcontainer.click(["frmConsolidatedStatements","flxFilterListBoxPopup"]);
	  kony.automation.flexcontainer.click(["frmConsolidatedStatements","segFileFilters[2]","flxFileFilter"]);
	}
	
	async function clickOnCreateStatementButton(){
	
	  appLog("Intiated method to Click on Create StatementButton");
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","btnDownload"],15000);
	  kony.automation.button.click(["frmConsolidatedStatements","btnDownload"]);
	  await kony.automation.playback.wait(5000);
	}
	
	async function verifyStatementCreationPopupMsg(){
	
	  appLog("Intiated method to verify Successfull Statement Creation");
	  var Status=await kony.automation.playback.waitFor(["frmConsolidatedStatements","lblPreparingStatementmessage"],15000);
	  if(Status){
	    kony.automation.button.click(["frmConsolidatedStatements","btnOkay"]);
	    appLog("Successfully clicked on Statement success message");
	  }else{
	    appLog("Custom Message : Failed to genarate Combined Statement");
	    fail("Custom Message : Failed to genarate Combined Statement");
	  }
	
	}
	
	async function MoveBackFrom_CombinedStatements(){
	
	  appLog("Intiated method to move back from ConsolidatedStatements");
	  await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	  appLog("Successfully moved back to account dashboard");
	}
	
	
	
	async function navigateToConsentmanagement(){
	
	  appLog("Intiated method to Navigate Consentmanagement Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings5flxMyAccounts"]);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings5flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblflxConsentManagementHeader"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblflxConsentManagementHeader"], "text")).toEqual("Your Consent");
	
	  appLog("Successfully Navigated to Consentmanagement Screen");
	}
	
	async function VerifyDisableCheckbox_ViewMode(){
	
	  appLog("Intiated method to verify edit view mode");
	  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditConsent"],15000);
	  expect(Status).toBe(true,"Failed to dispaly View Mode");
	}
	
	async function EditConsent(){
	
	  appLog("Intiated method to Edit consent");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditConsent"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnEditConsent"]);
	  appLog("Successfully clicked on Edit button");
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxCloneRow0lblCheckBox"],15000);
	  kony.automation.widget.touch(["frmProfileManagement","settings","flxCloneRow0lblCheckBox"], [9,11],null,null);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxCloneRow0flxProfileManagementConsentOptions"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxCloneRow0flxProfileManagementConsentOptions"]);
	  appLog("Successfully clicked on Check Box");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnConsentManagementSave"],15000);
	  await kony.automation.scrollToWidget(["frmProfileManagement","settings","btnConsentManagementSave"]);
	  kony.automation.button.click(["frmProfileManagement","settings","btnConsentManagementSave"]);
	  appLog("Successfully clicked on Save button");
	  //await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  
	}
	
	
	async function NavigatetoExchangeRates(){
	
	  appLog("Intiated method to Navigate to Exchange Rate");
	
	  // Navigate to Exchange Rate
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ExchangeRatesflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ExchangeRatesflxAccountsMenu"]);
	  await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitFor(["frmForexDashboard","lblForexHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmForexDashboard","lblForexHeader"], "text")).toEqual("Exchange Rate");
	
	  appLog("Successfully Navigated to Exchange Rate");
	}
	
	async function MoveBackFrom_ExchangeRates(){
	
	  await kony.automation.playback.waitFor(["frmForexDashboard","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmForexDashboard","customheadernew","flxAccounts"]);
	}
	
	async function setToCurrencytoEuro(){
	
	  appLog("Intiated method to Select To Currency dropdown");
	  //Set To Currency to EURO
	  await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","flxDropDownIcon"],15000);
	  kony.automation.flexcontainer.click(["frmForexDashboard","foreignExchange","flxDropDownIcon"]);
	  appLog("Successfully selected To Currency dropdown");
	  appLog("Intiated method to set To currency");
	  await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","tbxSearch"],15000);
	  kony.automation.textbox.enterText(["frmForexDashboard","foreignExchange","tbxSearch"],"EUR");
	  await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","segToCuurencyCode"],15000);
	  kony.automation.flexcontainer.click(["frmForexDashboard","foreignExchange","segToCuurencyCode[0]","flxRowExchangeRate"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully selected EUR currency");
	  //Verify Warnings
	  var Warning1=await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","lblDowntimeWarning"],10000);
	  expect(Warning1).toBe(false,"Failed to fetch base currency");
	  var Warning2=await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","lblSelectedDowntimeWarning"],10000);
	  expect(Warning2).toBe(false,"Unable to fetch the data.");
	  var Warning3=await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","lblRecentDowntimeWarning"],10000);
	  expect(Warning3).toBe(false,"Unable to fetch the data.");
	}
	
	async function CalucateExchangeRate(){
	
	  appLog("Intiated method to Enter ");
	  // Calucate the exchange Rate
	  await kony.automation.playback.waitFor(["frmForexDashboard","foreignExchange","tbxBaseCurrencyCodeValue"],15000);
	  kony.automation.textbox.enterText(["frmForexDashboard","foreignExchange","tbxBaseCurrencyCodeValue"],"2");
	
	}
	
	async function verifyMayBeLater(){
	
	  // Verifying may be later screen
	  appLog('Verifying MayBeLater Popup');
	  var mayBeLater=await kony.automation.playback.waitFor(["frmLogout","CustomFeedbackPopup","btnNo"],5000);
	  if(mayBeLater){
	    kony.automation.button.click(["frmLogout","CustomFeedbackPopup","btnNo"]);
	    await kony.automation.playback.waitFor(["frmLogout","logOutMsg","AlterneteActionsLoginNow"],10000);
	    kony.automation.flexcontainer.click(["frmLogout","logOutMsg","AlterneteActionsLoginNow"]);
	  }
	
	}
	
	async function verifyTermsandConditions(){
	
	  // Verifying Terms and conditions screen 
	  var termsconditions=await kony.automation.playback.waitFor(["frmPreTermsandCondition","flxAgree"],15000);
	
	  appLog('Verifying Terms and conditions');
	  appLog("Is terms and conditions : <b>"+termsconditions+"</b>");
	
	  if(termsconditions){
	
	    kony.automation.widget.touch(["frmPreTermsandCondition","lblFavoriteEmailCheckBox"], null,null,[15,9]);
	    kony.automation.flexcontainer.click(["frmPreTermsandCondition","flxAgree"]);
	
	    await kony.automation.playback.waitFor(["frmPreTermsandCondition","btnProceed"],10000);
	    kony.automation.button.click(["frmPreTermsandCondition","btnProceed"]);
	  }
	
	  await kony.automation.playback.waitFor(["frmDashboard"],15000);
	}
	
	async function verifyLoginFunctionality(userName,passWord){
	
	  // Login to the application
	  appLog('Initiating app login functionality');
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],10000);
	  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxUserName"],userName);
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxPassword"],10000);
	  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxPassword"],passWord);
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","btnLogin"],10000);
	  kony.automation.button.click(["frmLogin","loginComponent","btnLogin"]);
	  appLog('Successfully clicked on Sign in Button');
	  
	}
	
	
	
	async function NavigateToManageBeneficiary(){
	
	  appLog("Intiated method to navigate to ManageBeneficiary");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	  appLog("Clicked on ManageBeneficiary button");
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmManageBeneficiaries","lblPayABill"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmManageBeneficiaries","lblPayABill"], "text")).not.toBe("");
	  appLog("Successfully verified ManageBeneficiary Header");
	
	}
	
	async function clickonAddNewBeneficiaryBtn(){
	
	  appLog("Intiated method to click on AddNewBeneficiaryButton");
	
	  await kony.automation.playback.waitFor(["frmManageBeneficiaries","flxAddNewBeneficiary"],15000);
	  kony.automation.flexcontainer.click(["frmManageBeneficiaries","flxAddNewBeneficiary"]);
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","lblAddBeneficiary"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAddBeneficiaryEuro","lblAddBeneficiary"], "text")).not.toBe("");
	  appLog("Successfully verified AddNewBeneficiary Header");
	
	}
	
	async function isBenefeciaryAlreadyAdded(accNumber){
	
	  appLog("Intiated method to verify isBenefeciaryAlreadyAdded : <b>"+accNumber+"</b>");
	  var isBenAlreadyAdded=false;
	  await SearchforBeneficiaryFromList(accNumber);
	  var NoBenefeciary=await kony.automation.playback.waitFor(["frmManageBeneficiaries","rtxNoPaymentMessage"],5000);
	  appLog("NoBenefeciary :: <b>"+NoBenefeciary+"</b>");
	  
	  if(!NoBenefeciary){
	    isBenAlreadyAdded=true;
	    appLog("***Custom Message *** : Benefeciary is Already Added : <b>"+accNumber+"</b>");
	  }
	  return isBenAlreadyAdded;
	}
	
	async function enterSameBankBeneficiaryDetails(AccountNumber,Nickname,Address1,Address2,city,zipcode){
	
	  appLog("Intiated method to enter SameBankBeneficiaryDetails");
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAccountNumber"],AccountNumber);
	  appLog("Successfully Entered Acc Number : <b>"+AccountNumber+"</b>");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],15000);
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],Nickname);
	  appLog("Successfully Entered Nickname: <b>"+Nickname+"</b>");
	  
	  await enterBenefeciaryAddressDetails(Address1,Address2,city,zipcode);
	
	  await clickOnContinueButton();
	}
	
	async function enterDomesticBeneficiaryDetails(IBAN,BeneficiaryName,Nickname,Address1,Address2,city,zipcode){
	
	  appLog("Intiated method to enter SameBankBeneficiaryDetails");
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","flxRadioBtn2"],15000);
	  kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","flxRadioBtn2"]);
	  await kony.automation.playback.wait(5000);
	
	
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAccountNumber"],IBAN);
	  appLog("Successfully Entered IBAN : <b>"+IBAN+"</b>");
	
	  //For page refresh
	  kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","flxMainContainer"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryName"],15000);
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryName"],BeneficiaryName);
	  appLog("Successfully Entered BeneficiaryName : <b>"+BeneficiaryName+"</b>");
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],15000);
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],Nickname);
	  appLog("Successfully Entered Nickname: <b>"+Nickname+"</b>");
	  
	  await enterBenefeciaryAddressDetails(Address1,Address2,city,zipcode);
	
	  await clickOnContinueButton();
	}
	
	async function enterInternationalBeneficiaryDetails(AccountNumber,SwiftCode,BeneficiaryName,Nickname,Address1,Address2,city,zipcode){
	
	  appLog("Intiated method to enter SameBankBeneficiaryDetails");
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","flxRadioBtn2"],15000);
	  kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","flxRadioBtn2"]);
	  await kony.automation.playback.wait(5000);
	
	
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAccountNumber"],AccountNumber);
	  appLog("Successfully Entered AccountNumber : <b>"+AccountNumber+"</b>");
	
	  //For page refresh
	  kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","flxMainContainer"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxSWIFTBIC"],15000);
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxSWIFTBIC"],SwiftCode);
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryName"],15000);
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryName"],BeneficiaryName);
	  appLog("Successfully Entered BeneficiaryName : <b>"+BeneficiaryName+"</b>");
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],15000);
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxBeneficiaryNickname"],Nickname);
	  appLog("Successfully Entered Nickname: <b>"+Nickname+"</b>");
	  
	  await enterBenefeciaryAddressDetails(Address1,Address2,city,zipcode);
	  
	  await clickOnContinueButton();
	
	}
	
	async function enterBenefeciaryAddressDetails(Address1,Address2,city,zipcode){
	  
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAddressLine01"],Address1);
	  appLog("Successfully Entered Address1: <b>"+Address1+"</b>");
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxAddressLine02"],Address2);
	  appLog("Successfully Entered Address2: <b>"+Address2+"</b>");
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxCity"],city);
	  appLog("Successfully Entered City: <b>"+city+"</b>");
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxPostCode"],zipcode);
	  appLog("Successfully Entered Zipcode : <b>"+zipcode+"</b>");
	  
	}
	
	async function clickOnContinueButton(){
	
	  appLog("Intiated method to click on Continue button");
	  kony.automation.button.click(["frmAddBeneficiaryEuro","btnContinue"]);
	  await kony.automation.playback.wait(5000);
	  var confirmScreen=await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","btnConfirm"],15000);
	  expect(confirmScreen).toBe(true,"Custom Message :: Failed to Navigate to BeneficiaryConfirm");
	  appLog("Successfully Clicked on Continue Button");
	}
	
	async function VerifyNewelyAddedBenefeciaryDetails(){
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","lblBankValue"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAddBeneficiaryConfirmEuro","lblBankValue"], "text")).not.toBe("");
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	
	}
	
	async function ModifyNewelyAddedBenefeciaryDetails(UpdateCity){
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","btnModify"],15000);
	  kony.automation.button.click(["frmAddBeneficiaryConfirmEuro","btnModify"]);
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxCity"],UpdateCity);
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","btnContinue"],15000);
	  kony.automation.button.click(["frmAddBeneficiaryEuro","btnContinue"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Submitted Updated Beneficiary details: <b>"+UpdateCity+"</b>");
	
	}
	async function SubmitBankBeneficiaryDetails(){
	
	  await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","btnConfirm"],15000);
	  kony.automation.button.click(["frmAddBeneficiaryConfirmEuro","btnConfirm"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Confirm Button");
	}
	
	async function VerifyAddBeneficiarySuccessMsg(){
	
	  appLog("Intiated method to verify Newly Added Beneficiary");
	
	  var SuccessMsg=await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","lblSuccessMessage"],15000);
	  if(SuccessMsg){
	    expect(kony.automation.widget.getWidgetProperty(["frmAddBeneficiaryAcknowledgementEuro","lblSuccessMessage"], "text")).not.toBe("");
	    kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully verified Newly Added Beneficiary");
	
	  }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","rtxDowntimeWarning"],15000)){
	    //Move Back to DashBoard
	    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	
	    appLog("Custom Message :: Failed to add Beneficiary");
	    fail("Custom Message :: Failed to add Beneficiary");
	
	  }else{
	    appLog("Custom Message :: Unable to addBenficiary Successfully");
	  }
	
	}
	
	async function clickOnMakePaymentLink_Ackform(){
	
	  var SuccessMsg=await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","lblSuccessMessage"],15000);
	  if(SuccessMsg){
	    appLog("Successfully verified Newly Added Beneficiary");
	    await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","btnMakePayment"],15000);
	    kony.automation.button.click(["frmAddBeneficiaryAcknowledgementEuro","btnMakePayment"]);
	    //Page will get refresh with required details
	    await kony.automation.playback.wait(10000);
	
	    await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],30000);
	    expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text")).toEqual("Payments");
	
	    appLog("Successfully Navigated to MakePayment Screen Screen");
	
	
	  }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","rtxDowntimeWarning"],10000)){
	    //Move Back to DashBoard
	    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	
	    appLog("Custom Message :: Failed to add Beneficiary");
	    fail("Custom Message :: Failed to add Beneficiary");
	
	  }else{
	    appLog("Custom Message :: Unable to addBenficiary Successfully");
	  }
	
	}
	
	async function VerifyAddedBeneficiaryFromList(BenficiarySearch){
	
	  await NavigateToManageBeneficiary();
	  await SearchforBeneficiaryFromList(BenficiarySearch);
	  //var noBeneficiary=await kony.automation.playback.waitFor(["frmManageBeneficiaries","rtxNoPaymentMessage"],15000);
	
	  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
	  if(BeneficiaryList){
	    kony.automation.flexcontainer.click(["frmManageBeneficiaries","segmentBillpay[0]","flxDropdown"]);
	    await MoveBackFrom_ManageBeneficiaries();
	  }else{
	    //MoveBack to DashBoard
	    await MoveBackFrom_ManageBeneficiaries();
	    appLog("Custom Message :: Failed to Search Beneficiary from List");
	    fail("Custom Message :: Failed to Search Beneficiary from List");
	  }
	
	}
	
	async function MoveBackFrom_ManageBeneficiaries(){
	
	  appLog("Intiated method to move back from ManageBenefeciaries");
	  await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	async function SearchforBeneficiaryFromList(BenficiarySearch){
	
	  appLog("Intiated method to search for Beneficiary : <b>"+BenficiarySearch+"</b>");
	  await kony.automation.playback.waitFor(["frmManageBeneficiaries","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmManageBeneficiaries","txtSearch"],BenficiarySearch);
	  kony.automation.flexcontainer.click(["frmManageBeneficiaries","btnConfirm"]);
	  await kony.automation.playback.wait(5000);
	}
	
	async function VerifyBenefeciaryListItem(){
	
	  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
	  if(BeneficiaryList){
	    appLog("Message :: Benefeciary List is available");
	  }else{
	    //MoveBack to DashBoard
	    await MoveBackFrom_ManageBeneficiaries();
	    appLog("Custom Message :: Failed to Search Beneficiary from List");
	    fail("Custom Message :: Failed to Search Beneficiary from List");
	  }
	}
	
	async function DeleteBeneficiaryFromList(BenficiarySearch){
	
	  SearchforBeneficiaryFromList(BenficiarySearch);
	
	  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
	  if(BeneficiaryList){
	    appLog("Found Beneficiary from List : <b>"+BenficiarySearch+"</b>");
	    kony.automation.flexcontainer.click(["frmManageBeneficiaries","segmentBillpay[0]","flxDropdown"]);
	    kony.automation.button.click(["frmManageBeneficiaries","segmentBillpay[0]","btnRemoveRecipient"]);
	    await kony.automation.playback.waitFor(["frmManageBeneficiaries","DeletePopup","btnYes"],15000);
	    kony.automation.button.click(["frmManageBeneficiaries","DeletePopup","btnYes"]);
	    await kony.automation.playback.wait(5000);
	    appLog("Successfully Removed Beneficiary from List: <b>"+BenficiarySearch+"</b>");
	    await MoveBackFrom_ManageBeneficiaries();
	  }else{
	    //MoveBack to DashBoard
	    await MoveBackFrom_ManageBeneficiaries();
	    appLog("Custom Message :: Failed to Search Beneficiary from List");
	    //fail("Custom Message :: Failed to Search Beneficiary from List");
	  }
	
	}
	
	async function EditBeneficiaryFromList(BenficiarySearch,UpdateCity){
	
	  await SearchforBeneficiaryFromList(BenficiarySearch);
	
	  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
	  if(BeneficiaryList){
	
	    appLog("Found Beneficiary from List : <b>"+BenficiarySearch+"</b>");
	    kony.automation.flexcontainer.click(["frmManageBeneficiaries","segmentBillpay[0]","flxDropdown"]);
	    kony.automation.button.click(["frmManageBeneficiaries","segmentBillpay[0]","btnEdit"]);
	    await kony.automation.playback.wait(5000);
	    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","lblAddBeneficiary"],15000);
	    expect(kony.automation.widget.getWidgetProperty(["frmAddBeneficiaryEuro","lblAddBeneficiary"], "text")).not.toBe("");
	    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","tbxCity"],15000);
	    kony.automation.textbox.enterText(["frmAddBeneficiaryEuro","tbxCity"],UpdateCity);
	    await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","btnContinue"],15000);
	    kony.automation.button.click(["frmAddBeneficiaryEuro","btnContinue"]);
	    await kony.automation.playback.wait(5000);
	    appLog("Submitted Edited Beneficiary details: <b>"+BenficiarySearch+"</b>");
	
	    await VerifyAddBeneficiarySuccessMsg();
	
	  }else{
	
	    appLog("Custom Message :: Failed to Search Beneficiary from List");
	    //fail("Custom Message :: Failed to Search Beneficiary from List");
	
	    await MoveBackFrom_ManageBeneficiaries();
	  }
	
	}
	
	async function ClickonMakePaymentLink_BeneficiariesList(BenficiarySearch){
	
	  await SearchforBeneficiaryFromList(BenficiarySearch);
	
	  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],15000);
	  if(BeneficiaryList){
	    appLog("Found Beneficiary from List : <b>"+BenficiarySearch+"</b>");
	    kony.automation.button.click(["frmManageBeneficiaries","segmentBillpay[0]","btnAction"]);
	    await kony.automation.playback.wait(10000);
	  }else{
	    appLog("Custom Message :: Failed to Search Beneficiary from List");
	    fail("Custom Message :: Failed to Search Beneficiary from List");
	    await MoveBackFrom_ManageBeneficiaries();
	  }
	
	}
	
	async function SearchForBenefeciary(SearchText){
	
	  await kony.automation.playback.waitFor(["frmManageBeneficiaries","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmManageBeneficiaries","txtSearch"],SearchText);
	  kony.automation.flexcontainer.click(["frmManageBeneficiaries","btnConfirm"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],10000);
	  var BeneficiaryList=await kony.automation.playback.waitFor(["frmManageBeneficiaries","segmentBillpay"],10000);
	  if(BeneficiaryList){
	    appLog("Found Beneficiary from List : <b>"+SearchText+"</b>");
	  }else{
	    appLog("Custom Message :: Failed to Search Beneficiary from List");
	    //fail("Custom Message :: Failed to Search Beneficiary from List");
	    await MoveBackFrom_ManageBeneficiaries();
	  }
	
	  //kony.automation.flexcontainer.click(["frmManageBeneficiaries","flxClearBtn"]);
	}
	
	async function VerifyBenefeciariesPageCount(){
	
	  await kony.automation.playback.waitFor(["frmManageBeneficiaries","lblPagination"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmManageBeneficiaries","lblPagination"], "text")).not.toBe("");
	  await MoveBackFrom_ManageBeneficiaries();
	}
	
	async function VerifyBenefeciariesPagination(){
	
	  var pageNext=await kony.automation.playback.waitFor(["frmManageBeneficiaries","flxPaginationNext"],15000);
	  if(pageNext){
	    kony.automation.flexcontainer.click(["frmManageBeneficiaries","flxPaginationNext"]); 
	  }
	  var pagePrevious=await kony.automation.playback.waitFor(["frmManageBeneficiaries","flxPaginationPrevious"],15000);
	  if(pagePrevious){
	    kony.automation.flexcontainer.click(["frmManageBeneficiaries","flxPaginationPrevious"]);
	  }
	
	  await MoveBackFrom_ManageBeneficiaries();
	}
	
	async function NavigateToMessages(){
	
	  appLog("Intiated method to Navigate to NotficationsAndMessages");
	  
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGESflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGESflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGES1flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGES1flxMyAccounts"]);
	  appLog("Successfully Navigated to NotficationsAndMessages");
	  await kony.automation.playback.wait(5000);
	
	}
	
	async function ComposeNewMessage(){
	
	  appLog("Intiated method to Compose a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessage"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessage"]);
	  appLog("Successfully Clicked on NewMessage Button");
	  await kony.automation.playback.wait(5000);
	  
	  await EnterMessageDetails();
	  
	}
	
	async function EnterMessageDetails(){
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","listbxCategory"],15000);
	  kony.automation.listbox.selectItem(["frmNotificationsAndMessages","NotficationsAndMessages","listbxCategory"], "RCID_ONLINEBANKING");
	  appLog("Successfully Selected Category");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","tbxSubject"],15000);
	  kony.automation.textbox.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","tbxSubject"],"First Test Message");
	  appLog("Successfully Entered Message subject");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","textareaDescription"],15000);
	  kony.automation.textarea.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","textareaDescription"],"Test Message");
	  appLog("Successfully Entered Message content");
	  
	  await ClickonSendButton();
	  
	}
	
	async function ClickonSendButton(){
	  
	  //await kony.automation.scrollToWidget(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessageSend"]);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessageSend"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessageSend"]);
	  appLog("Successfully Clicked on SEND button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function deleteNewMessage(){
	
	  appLog("Intiated method to Delete a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","flxDelete"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","NotficationsAndMessages","flxDelete"]);
	  appLog("Successfully Clicked on Delete button");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","CustomPopup1","btnYes"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","CustomPopup1","btnYes"]);
	  appLog("Successfully Clicked on YES button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function replyNewMessage(){
	
	  appLog("Intiated method to Reply a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnSendReply"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnSendReply"]);
	  appLog("Successfully Clicked on REPLY button");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","txtAreaReply"],15000);
	  kony.automation.textarea.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","txtAreaReply"],"Reply to Message");
	  appLog("Successfully Entered Message content");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnSendReply"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnSendReply"]);
	  appLog("Successfully Clicked on Send REPLY button");
	  await kony.automation.playback.wait(15000);
	}
	
	async function restoreNewMessage(){
	
	  appLog("Intiated method to Restore a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnDeletedMessages"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnDeletedMessages"]);
	  appLog("Successfully Clicked on DELETE button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnRestore"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnRestore"]);
	  appLog("Successfully Clicked on RESTORE button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function searchNewMessage(){
	
	  appLog("Intiated method to Search a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","txtSearch"],"Test");
	  appLog("Successfully Entered Text to Search");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnSearch"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnSearch"]);
	  appLog("Successfully Clicked on SEARCH button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function verifyRequestID(){
	
	  appLog("Intiated method to Verify Request ID");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification[0]","flxNotificationsAndMessages","lblRequestIdValue"],"text")).not.toBe('');
	}
	
	async function MoveBackToDashBoard_Messages(){
	
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Accounts dashboard");
	
	}
	
	async function PostLogin_NavigateToAboutUs_FAQ(){
	
	  appLog("Intiated  method to Navigate to About US");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmOnlineHelp","help","lblHeading"],15000);
	
	  appLog("Successfully Navigated to About US");
	}
	
	async function PostLogin_MoveBacktoDashboard_AboutUs_FAQ(){
	
	  await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function PostLogin_NavigateToPrivacyPolicy(){
	
	  appLog("Intiated method to Navigate to Privacypolicy");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs1flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs1flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"],15000);
	
	  appLog("Successfully Navigated to Privacypolicy");
	
	}
	
	async function PostLogin_MoveBacktoDashboard_PrivacyPolicy(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function PostLogin_NavigateToTermsConditions(){
	
	  appLog("Intiated method to Navigate to TC's");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs0flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"],15000);
	
	  appLog("Successfully Navigated to TC's");
	}
	
	async function PostLogin_MoveBacktoDashboard_TermsConditions(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function PostLogin_NavigateToContactUs(){
	
	  appLog("Intiated method to Navigate to Contact US");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs2flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs2flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"],15000);
	
	  appLog("Successfully Navigated to Contact US");
	}
	
	async function PostLogin_MoveBacktoDashboard_ContactUs(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function PostLogin_NavigateToAboutUs_Help(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"]);
	  await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	}
	
	
	async function PostLogin_VerifyFeedBackFunctionality(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs5flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs5flxMyAccounts"]);
	  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","flxRating5"],15000);
	  kony.automation.flexcontainer.click(["frmCustomerFeedback","Feedback","flxRating5"]);
	  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","txtareaUserComments"],15000);
	  kony.automation.textarea.enterText(["frmCustomerFeedback","Feedback","txtareaUserComments"],"test");
	  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","confirmButtons","btnConfirm"],15000);
	  kony.automation.button.click(["frmCustomerFeedback","Feedback","confirmButtons","btnConfirm"]);
	  await kony.automation.playback.waitFor(["frmCustomerFeedback","acknowledgment","lblTransactionMessage"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCustomerFeedback","acknowledgment","lblTransactionMessage"], "text")).toContain("Thank you");
	  await kony.automation.playback.waitFor(["frmCustomerFeedback","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmCustomerFeedback","customheader","topmenu","flxaccounts"]);
	  
	}
	
	async function NavigateToProfileSettings(){
	
	  appLog("Intiated method to Navigate to Personal Details");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);
	  await kony.automation.playback.wait(15000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPersonalDetailsHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPersonalDetailsHeading"], "text")).toEqual("Personal Details");
	
	  appLog("Successfully Navigated to Personal Details");
	}
	
	async function NavigateToAlertSettings(){
	
	  appLog("Intiated method to Navigate to Alert Settings");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings4flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAlertsHeading"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAlertsHeading"], "text")).toEqual("Security");
	  
	  appLog("Successfully Navigated to Alert Settings");
	}
	
	async function selectProfileSettings_PhoneNumber(){
	
	  appLog("Intiated method to Navigate to PhoneNumber Flex");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxPhone"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxPhone"]);
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPhoneNumbersHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPhoneNumbersHeading"], "text")).toEqual("Phone Number");
	
	  appLog("Successfully Navigated to PhoneNumber Flex");
	}
	
	async function selectProfileSettings_EmailAddress(){
	
	  appLog("Intiated method to Navigate to EmailID Flex");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEmail"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEmail"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEmailHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEmailHeading"], "text")).toEqual("Email");
	
	  appLog("Successfully Navigated to EmailID Flex");
	}
	
	async function selectProfileSettings_Address(){
	
	  appLog("Intiated method to Navigate to Address Flex");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddress"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddress"]);
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddressHeading"], "text")).toEqual("Address");
	
	  appLog("Successfully Navigated to Address Flex");
	}
	
	async function ProfileSettings_addNewPhoneNumberDetails(phoneNumber,isPrimary){
	
	  appLog("Intiated method to addNewPhoneNumberDetails");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddPhoneNumberHeading"], "text")).toEqual("Add Phone Number");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxAddPhoneNumberType"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Other");
	  appLog("Successfully Selected PhoneNumber Type");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],"91");
	  appLog("Successfully Entered CountryCode");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumber"],phoneNumber);
	  appLog("Successfully Entered Phone Number as : <b>"+phoneNumber+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakePrimayPhoneNumbercheckBox();
	  }
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberSave"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
	  appLog("Successfully clicked on SAVE button");
	  await kony.automation.playback.wait(5000);
	
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	    var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	    var segLength=seg_Size.length;
	    //appLog("Length is :: "+segLength);
	    for(var x = 0; x <segLength; x++){
	      var seg="segPhoneNumbers["+x+"]";
	      //appLog("Segment is :: "+seg);
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
	      var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
	      appLog("Phone Number Text is :: "+phonenum);
	      if(phonenum===phoneNumber){
	        appLog("Successfully Added Mobile Number "+phoneNumber);
	        Status="TRUE";
	        break;
	      }
	    }
	  }catch(Exception){
	
	  }finally{
	    expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	  }
	
	}
	
	async function ProfileSettings_addInvalidPhoneNumberDetails(phoneNumber,isPrimary){
	
	  appLog("Intiated method to addNewPhoneNumberDetails");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddPhoneNumberHeading"], "text")).toEqual("Add Phone Number");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxAddPhoneNumberType"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Other");
	  appLog("Successfully Selected PhoneNumber Type");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],"91");
	  appLog("Successfully Entered CountryCode");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumber"],phoneNumber);
	  appLog("Successfully Entered Phone Number as : <b>"+phoneNumber+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakePrimayPhoneNumbercheckBox();
	  }
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberSave"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
	  appLog("Successfully clicked on SAVE button");
	  await kony.automation.playback.wait(5000);
	
	  var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
	
	  if(isAddHeader){
	
	    appLog("Custom Message :: Invalid characters entered");
	
	  }else{
	    appLog("Accepted Invalid Characters");
	    fail("Custom Message :: Accepted Invalid Characters");
	  }
	
	
	}
	
	async function ProfileSettings_VerifyaddInvalidPhoneNumberFunctionality(phoneNumber,isPrimary){
	
	  appLog("Intiated method to VerifyaddNewPhoneNumberFunctionality");
	
	  var isAddNewNumber=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewNumber"],15000);
	  //appLog("Button status is :"+isAddNewNumber);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	  var segLength=seg_Size.length;
	  //appLog("PhoneNumbers size is :: "+segLength);
	
	  if(segLength<3&&isAddNewNumber){
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewNumber"]);
	    appLog("Successfully Clicked on Add New Phone Number Button");
	    await ProfileSettings_addInvalidPhoneNumberDetails(phoneNumber,isPrimary);
	  }else{
	    appLog("Maximum phone numbers already added");
	  }
	}
	
	async function selectMakePrimayPhoneNumbercheckBox(){
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddCheckBox3"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddCheckBox3"]);
	  appLog("Successfully Selected Entered Phone Number as Primary");
	}
	
	async function ProfileSettings_UpdatePhoneNumber(updatedPhonenum){
	
	  // Update Number
	  appLog("Intiated method to Update PhoneNumberDetails");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	
	  var segLength1=accounts_Size1.length;
	  kony.print("Segment length is :"+segLength1);
	
	  var isEditble=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers[0]","btnViewDetail"],15000);
	  if(isEditble){
	
	    kony.automation.button.click(["frmProfileManagement","settings","segPhoneNumbers[0]","btnViewDetail"]);
	    appLog("Successfully clicked on ViewDetails button");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxPhoneNumber"]);
	    kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxPhoneNumber"],updatedPhonenum);
	    appLog("Successfully Updated Phone number as : <b>"+updatedPhonenum+"</b>");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
	    kony.automation.button.click(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
	    appLog("Successfully clicked on SAVE button");
	    await kony.automation.playback.wait(5000);
	
	    try{
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	      var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	      var segLength=seg_Size.length;
	      //appLog("Length is :: "+segLength);
	      for(var x = 0; x <segLength; x++){
	        var seg="segPhoneNumbers["+x+"]";
	        //appLog("Segment is :: "+seg);
	        await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
	        var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
	        appLog("Phone Number Text is :: "+phonenum);
	        if(phonenum===updatedPhonenum){
	          appLog("Successfully Updated Mobile Number to : "+updatedPhonenum);
	          Status="TRUE";
	          break;
	        }
	      }
	    }catch(exception){
	      Status="FALSE";
	    }finally{
	      expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	    }
	
	  }else{
	    appLog("Unable to Update PhoneNumberDetails");
	  }
	}
	
	async function ProfileSettings_DeletePhoneNumber(phoneNumber){
	
	  //Delete already added Mobile Number
	  appLog("Intiated method to Delete PhoneNumberDetails");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	  var segLength=seg_Size.length;
	  //appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++){
	    var seg="segPhoneNumbers["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
	    var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
	    appLog("Phone Number Text is :: "+phonenum);
	    if(phonenum===phoneNumber){
	      appLog("Phone Number to be Deleted is : "+phoneNumber);
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
	      appLog("Successfully Clicked on Delete Button");
	
	      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
	      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
	      appLog("Successfully Clicked on YES Button");
	      await kony.automation.playback.wait(5000);
	      break;
	    }
	  } 
	}
	
	async function ProfileSettings_VerifyaddNewPhoneNumberFunctionality(phoneNumber,isPrimary){
	
	  appLog("Intiated method to VerifyaddNewPhoneNumberFunctionality");
	
	  var isAddNewNumber=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewNumber"],15000);
	  //appLog("Button status is :"+isAddNewNumber);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	  var segLength=seg_Size.length;
	  //appLog("PhoneNumbers size is :: "+segLength);
	
	  if(segLength<3&&isAddNewNumber){
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewNumber"]);
	    appLog("Successfully Clicked on Add New Phone Number Button");
	    await ProfileSettings_addNewPhoneNumberDetails(phoneNumber,isPrimary);
	
	    // No Need to delete after adding
	    
	//     var Header=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPhoneNumbersHeading"],15000);
	//     if(Header&&isPrimary==='NO'){
	//       await ProfileSettings_DeletePhoneNumber(phoneNumber);
	//     }else{
	//       appLog("Custom Message :: Update Customer Details Failed");
	//       fail("Custom Message :: Custom Message :: Update Customer Details Failed");
	//     }
	     // No Need to delete after adding
	
	  }else{
	    appLog("Maximum phone numbers already added");
	  }
	}
	
	async function ProfileSettings_addNewAddressDetails(addressLine1,addressLine2,zipcode,isPrimary){
	
	  appLog("Intiated method to addNewAddressDetails");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine1"],addressLine1);
	  appLog("Successfully Entered AddressLine1 : <b>"+addressLine1+"</b>");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine2"],addressLine2);
	  appLog("Successfully Entered AddressLine2 : <b>"+addressLine2+"</b>");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCountry"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCountry"], "IN");
	  appLog("Successfully Selected Country Code");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxState"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxState"], "IN-TG");
	  appLog("Successfully Selected State Code");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","txtCity"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","txtCity"],"HYD");
	  appLog("Successfully Selected City Code");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxZipcode"],zipcode);
	  appLog("Successfully Entered Zipcode : <b>"+zipcode+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakeDefaultAddresscheckBox();
	  }
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddressAdd"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddressAdd"]);
	  appLog("Successfully Clicked on Add Address Button");
	  await kony.automation.playback.wait(5000);
	
	  //   var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewAddressHeader"],15000);
	
	  //   if(isAddHeader){
	  //     appLog("Custom Message :: Update Customer Details Failed");
	  //     fail("Custom Message :: Update Customer Details Failed");
	
	  //   }else{
	  //     appLog("Successfully Added new Address details");
	  //   }
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	    var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	    var segLength=accounts_Size.length;
	    for(var x = 0; x <segLength; x++) {
	      var seg="segAddresses["+x+"]";
	      //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblAddressLine1"],15000);
	      var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblAddressLine1"], "text");
	      appLog("Address Text is :: "+address1);
	      if(address1===addressLine1){
	        appLog("Successfully added Address "+addressLine1);
	        Status="TRUE";
	        break;
	      }
	    }
	  }catch(Exception){
	    Status="FALSE";
	  }finally{
	    expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	  }
	}
	
	
	async function selectMakeDefaultAddresscheckBox(){
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"]);
	  appLog("Successfully Selected Entered Address as Primary");
	}
	
	async function ProfileSettings_UpdateAddress(UpdatedZip){
	
	  // Update Address
	  appLog("Intiated method to update Address Details");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"]);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segAddresses["+x+"]";
	    var ismailing=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblCommunicationAddress"], "text");
	    appLog("is Mailing Address : "+ismailing);
	    if(ismailing===""){
	
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnEdit"]);
	      appLog("Successfully clicked on Edit button");
	      await kony.automation.playback.wait(5000);
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditZipcode"],15000);
	      kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditZipcode"],UpdatedZip);
	      appLog("Successfully Entered Updated Zipcode as : <b>"+UpdatedZip+"</b>");
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAddressSave"],15000);
	      kony.automation.button.click(["frmProfileManagement","settings","btnEditAddressSave"]);
	      appLog("Successfully clicked on SAVE button");
	      await kony.automation.playback.wait(5000);
	
	      try{
	        await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	        var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	        var segLength1=accounts_Size1.length;
	        for(var x1 = 0; x1 <segLength1; x1++) {
	          var seg1="segAddresses["+x1+"]";
	          //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
	          await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"lblAddressLine1"],15000);
	          var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"lblAddressLine1"], "text");
	          appLog("Address Text is :: "+address1);
	          if(address1===addressLine1){
	            appLog("Successfully Updated Address to : "+addressLine1);
	            Status="TRUE";
	            break;
	          }
	        }
	
	      }catch(Exception){
	        Status="FALSE";
	      }finally{
	        expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	      }
	
	    }
	    break;
	  }
	
	}
	
	async function ProfileSettings_DeleteAddress(addressLine1){
	
	  // Delete Address
	  appLog("Intiated method to Delete Address Details");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segAddresses["+x+"]";
	    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblAddressLine1"],15000);
	    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblAddressLine1"], "text");
	    appLog("Address Text is :: "+address1);
	    if(address1===addressLine1){
	      appLog("Address to be Deleted is : "+addressLine1);
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
	      appLog("Successfully clicked on DELETE button");
	      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
	      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
	      appLog("Successfully clicked on YES button");
	      await kony.automation.playback.wait(5000);
	      break;
	    }
	  }
	
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	    var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	    var segLength1=accounts_Size1.length;
	    for(var x1 = 0; x1 <segLength1; x1++) {
	      var seg1="segAddresses["+x1+"]";
	      //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"lblAddressLine1"],15000);
	      var address2=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"lblAddressLine1"], "text");
	      appLog("Address Text is :: "+address2);
	      if(address2===addressLine1){
	        appLog("Address is not deleted");
	        Status="TRUE";
	        break;
	      }
	    }
	
	  }catch(Exception){
	    Status="FALSE";
	  }finally{
	    expect(Status).toContain("FALSE", "Custom Message :: Unable to delete address");
	  }
	
	}
	
	async function ProfileSettings_VerifyaddNewAddressFunctionality(addressLine1,addressLine2,zipcode,isPrimary){
	
	
	  appLog("Intiated method to VerifyaddNewAddressFunctionality");
	
	  var isAddNewAddress=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddress"],15000);
	  //appLog("Button status is :"+isAddNewAddress);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	  var segLength=seg_Size.length;
	  //appLog("Address size is :: "+segLength);
	
	  if(segLength<3&&isAddNewAddress){
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddress"]);
	    appLog("Successfully clicked on AddNewAddress button");
	    await ProfileSettings_addNewAddressDetails(addressLine1,addressLine2,zipcode,isPrimary);
	
	    // No Need to delete address after adding
	//     var Header=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"],15000);
	//     if(Header&&isPrimary==='NO'){
	//       await ProfileSettings_DeleteAddress(addressLine1);
	//     }else{
	//       appLog("Custom Message :: Update Customer Details Failed");
	//       fail("Custom Message :: Update Customer Details Failed");
	//     }
	    // No Need to delete address after adding
	
	  }else{
	    appLog("Maximum Address already added");
	  }
	
	}
	
	async function ProfileSettings_addEmailAddressDetails(emailAddress,isPrimary){
	
	  // Add new email ID
	  appLog("Intiated method to add new Emai Address Details");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewEmailHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddNewEmailHeading"], "text")).toEqual("Add New Email");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEmailId"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEmailId"],emailAddress);
	  appLog("Successfully Entered new Email Address : <b>"+emailAddress+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakePrimayEmailIDcheckBox();
	  }
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddEmailIdAdd"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddEmailIdAdd"]);
	  appLog("Successfully clicked on Add Button");
	  await kony.automation.playback.wait(5000);
	
	  //   var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewEmailHeading"],15000);
	
	  //   if(isAddHeader){
	  //     appLog("Custom Message :: Update Customer Details Failed");
	  //     fail("Custom Message :: Update Customer Details Failed");
	  //   }else{
	  //     appLog("Successfully added new Email Address");
	  //   }
	
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segEmailIds["+x+"]";
	    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblEmail"],15000);
	    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblEmail"], "text");
	    appLog("Email Text is :: "+address1);
	    if(address1===emailAddress){
	      appLog("Email Number to be Deleted is : "+emailAddress);
	      Status="TRUE";
	      break;
	    }
	  }
	
	  }catch(Exception){
	    Status="FALSE";
	  }finally{
	    expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	  }
	}
	
	async function selectMakePrimayEmailIDcheckBox(){
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxMarkAsPrimaryEmailCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxMarkAsPrimaryEmailCheckBox"]);
	  appLog("Successfully Selected Entered EmailID as Primary");
	}
	
	async function ProfileSettings_UpdateEmailAddress(updatedemailid){
	
	  // Update email ID
	  appLog("Intiated method to Update Email address");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength1=accounts_Size1.length;
	  appLog("Length is :"+segLength1);
	
	  var isEditble=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds[0]","btnEdit"],15000);
	  if(isEditble){
	    kony.automation.button.click(["frmProfileManagement","settings","segEmailIds[0]","btnEdit"]);
	    appLog("Successfully clicked on Edit Button");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditEmailId"],15000);
	    kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditEmailId"],updatedemailid);
	    appLog("Successfully Entered Updated Email ID : <b>"+updatedemailid+"</b>");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditEmailIdSave"],15000);
	    kony.automation.button.click(["frmProfileManagement","settings","btnEditEmailIdSave"]);
	    appLog("Successfully Clicked on SAVE button");
	    await kony.automation.playback.wait(5000);
	
	    //     var isEditHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditEmailHeading"],15000);
	
	    //     if(isEditHeader){
	    //       appLog("Custom Message :: Update Customer Details Failed");
	    //       fail("Custom Message :: Update Customer Details Failed");
	    //     }else{
	    //       appLog("Successfully Updated email Address");
	    //     }
	
	    try{
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	    var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	    var segLength=accounts_Size.length;
	    for(var x = 0; x <segLength; x++) {
	      var seg="segEmailIds["+x+"]";
	      //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblEmail"],15000);
	      var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblEmail"], "text");
	      appLog("Email Text is :: "+address1);
	      if(address1===updatedemailid){
	        appLog("Successfully updated EmailAddress : "+updatedemailid);
	        Status="TRUE";
	        break;
	      }
	    }
	
	    }catch(Exception){
	       Status="FALSE";
	    }finally{
	       expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	    }
	  }
	
	}
	
	async function ProfileSettings_deleteEmailAddressDetails(emailAddress){
	
	  // Delete Address
	  appLog("Intiated method to Delete Email Details");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segEmailIds["+x+"]";
	    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblEmail"],15000);
	    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblEmail"], "text");
	    appLog("Email Text is :: "+address1);
	    if(address1===emailAddress){
	      appLog("Email Number to be Deleted is : "+emailAddress);
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
	      appLog("Successfully Clicked on Delete Button");
	      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
	      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
	      appLog("Successfully Clicked on YES Button");
	      await kony.automation.playback.wait(5000);
	      break;
	    }
	  }
	
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength1=accounts_Size1.length;
	  for(var x1 = 0; x1 <segLength1; x1++) {
	    var seg1="segEmailIds["+x1+"]";
	    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"lblEmail"],15000);
	    var address2=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"lblEmail"], "text");
	    appLog("Email Text is :: "+address2);
	    if(address2===emailAddress){
	      appLog("Failed to delete EmailAddress : "+emailAddress);
	      Status="TRUE";
	      break;
	    }
	  }
	
	  }catch(Exception){
	    Status="FALSE";
	  }finally{
	    expect(Status).toContain("FALSE", "Custom Message :: Unable to delete Email");
	  }
	}
	
	async function ProfileSettings_VerifyaddEmailAddressFunctionality(emailAddress,isPrimary){
	
	  appLog("Intiated method to VerifyaddEmailAddressFunctionality");
	
	  var isAddEmailAddress=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewEmail"],15000);
	  //appLog("Button status is :"+isAddEmailAddress);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	  var segLength=seg_Size.length;
	  //appLog("email size is :: "+segLength);
	
	  if(segLength<3&&isAddEmailAddress){
	
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewEmail"]);
	    appLog("Successfully clicked on NewEmail Button");
	    await ProfileSettings_addEmailAddressDetails(emailAddress,isPrimary);
	
	    // No need to delete after adding
	//     var Header= await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEmailHeading"],15000);
	//     if(Header&&isPrimary==='NO'){
	//       await ProfileSettings_deleteEmailAddressDetails(emailAddress);
	//     }else{
	
	//       appLog("Custom Message :: Update Customer Details Failed");
	//       fail("Custom Message :: Custom Message :: Update Customer Details Failed");
	//       //await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberCancel"]);
	//       //kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberCancel"]);
	//     }
	       // No need to delete after adding
	
	  }else{
	    appLog("Maximum Email Address already added");
	  }
	
	}
	
	
	async function MoveBackToDashBoard_ProfileManagement(){
	
	  // Move back to base state
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function NavigateToAccountSettings(){
	
	  appLog("Intiated method to navigate to Account Settings");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAccountsHeader"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAccountsHeader"], "text")).toEqual("Accounts");
	
	  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
	  expect(Status).toBe(true,"FAILED to load Accounts segment");
	  appLog("Successfully Navigated to AccountSettings");
	}
	
	
	async function clickonDefaultAccountstab(){
	
	  appLog("Intiated method to click on DefaultAccountstab");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxSetDefaultAccount"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxSetDefaultAccount"]);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblDefaultTransactionAccounttHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblDefaultTransactionAccounttHeading"], "text")).toEqual("Default Transaction Accounts");
	
	  appLog("Successfully clicked on DefaultAccountstab");
	
	  await kony.automation.playback.wait(5000);
	}
	async function clickonAccountPreferencetab(){
	
	  appLog("Intiated method to click on AccountPreferencetab");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAccountPreferences"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAccountPreferences"]);
	
	  appLog("Successfully clicked on AccountPreferencetab");
	
	  await kony.automation.playback.wait(5000);
	}
	
	
	async function EditFavAccountPreferences(){
	
	  appLog("Intiated method to Edit FavAccountPreferences");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
	  kony.automation.button.click(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Successfully Clicked on Edit button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAccountsHeader"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEditAccountsHeader"], "text")).toEqual("Edit Account");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAccountNickNameValue"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAccountNickNameValue"],'My Checking');
	  appLog("Successfully Updated NickName value");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAccountsSave"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnEditAccountsSave"]);
	  appLog("Successfully Clicked on SAVE button");
	}
	
	async function CancelFavAccountPreferences(){
	
	  appLog("Intiated method to cancel FavAccountPreferences");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
	  kony.automation.button.click(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Successfully Clicked on Edit button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAccountsHeader"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEditAccountsHeader"], "text")).toEqual("Edit Account");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAccountNickNameValue"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAccountNickNameValue"],'My Checking');
	  appLog("Successfully Updated NickName value");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAccountsCancel"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnEditAccountsCancel"]);
	  appLog("Successfully Clicked on CANCEL button");
	}
	
	async function SetDefaultAccountPreferences(){
	
	  appLog("Intiated method to Set DefaultAccountPreferences Tab");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Edit Button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblSelectedDefaultAccounts"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblSelectedDefaultAccounts"], "text")).not.toBe('');
	
	  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxBillPay"],15000);
	  //   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxBillPay"], "190128223241502");
	  //   appLog("Successfully Selected Default BillPay acc");
	
	  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCheckDeposit"],15000);
	  //   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCheckDeposit"], "190128223242830");
	  //   appLog("Successfully Selected Default Deposit acc");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"]);
	  appLog("Successfully Clicked on SAVE Button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblSelectedDefaultAccounts"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblSelectedDefaultAccounts"], "text")).not.toBe('');
	  appLog("Successfully Verified Default accounts");
	}
	
	async function EnableDisableSecurityAlerts(){
	  
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAlerts"],15000);
		kony.automation.button.click(["frmProfileManagement","settings","btnEditAlerts"]);
	    appLog("Successfully clicked on Edit button");
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblStatus1"],15000);
		kony.automation.widget.touch(["frmProfileManagement","settings","lblStatus1"], null,null,[29,15]);
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEnableSwitch"],15000);
		kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEnableSwitch"]);
	    appLog("Successfully clicked on Radio button");
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblStatus1"],15000);
		kony.automation.widget.touch(["frmProfileManagement","settings","lblStatus1"], null,null,[14,17]);
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEnableSwitch"],15000);
		kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEnableSwitch"]);
	    appLog("Successfully clicked on Radio button");
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAlertSave"],15000);
		kony.automation.button.click(["frmProfileManagement","settings","btnAlertSave"]);
		await kony.automation.playback.wait(10000);
	    appLog("Successfully clicked on SAVE button");
		await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
		kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	}
	
	
	
	async function navigateToManageTranscations(){
	
	  appLog("Intiated method to Navigate ManageTranscation Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.widget.touch(["frmDashboard","customheader","topmenu","flxTransfersAndPay"], [118,39],null,null);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxWireMoney"]);
	  //Need loader to be disbale to load segment data
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  //await kony.automation.playback.wait(15000);
	
	  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","lblManagePayments"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","lblManagePayments"], "text")).toEqual("Transfer Activities");
	  appLog("Successfully Navigated to Transfer Activities Screen");
	
	}
	
	async function ClickonTransfersTab(){
	
	  appLog("Intiated method to click on Transfers tab");
	
	  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","tabs","btnTab1"],15000);
	  kony.automation.button.click(["frmPastPaymentsEurNew","tabs","btnTab1"]);
	  //Need loader to be disbale to load segment data
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  //await kony.automation.playback.wait(15000);
	
	  appLog("Successfully clicked on Transfers Tab");
	  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","lblSelectedFilterValue"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","SearchAndFilter","lblSelectedFilterValue"], "text")).not.toBe("");
	}
	
	async function ClickonRecurringTab(){
	
	  appLog("Intiated method to click on Recurrences tab");
	
	  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","tabs","btnTab2"],15000);
	  kony.automation.button.click(["frmPastPaymentsEurNew","tabs","btnTab2"]);
	  //Need loader to be disbale to load segment data
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  //await kony.automation.playback.wait(15000);
	
	  appLog("Successfully clicked on Recurrences Tab");
	  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","SearchAndFilter","lblSelectedFilterValue"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmScheduledPaymentsEurNew","SearchAndFilter","lblSelectedFilterValue"], "text")).not.toBe("");
	}
	
	async function VerifyAllTransfersList(){
	
	  var AllTransfersStatus=false;
	
	  var AllTransfersList=await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","segmentTransfers"],15000);
	  if(AllTransfersList){
	    appLog("Successfully found Transcation List");
	    AllTransfersStatus=true;
	  }else{
	    //MoveBack to DashBoard
	    await MoveBackFrom_PastTransferActivities();
	    appLog("Custom Message :: No records found.");
	    fail("Custom Message :: No records found.");
	  }
	
	  return AllTransfersStatus;
	}
	
	async function VerifyStandardOrdersList(){
	
	  var StandardStatus=false;
	
	  var StandardOrderList=await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","segmentTransfers"],15000);
	  if(StandardOrderList){
	    appLog("Successfully found Transcation List");
	    StandardStatus=true;
	  }else{
	    //MoveBack to DashBoard
	    await MoveBackFrom_SheduledTransferActivities();
	    appLog("Custom Message :: No records found.");
	    fail("Custom Message :: No records found.");
	  }
	
	  return StandardStatus;
	
	}
	
	async function VerifyNewStatusColumn_AllTransfers(){
	
	  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","lblColumn4"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","List","lblColumn4"], "text")).not.toBe("");
	}
	
	async function VerifyNewStatusColumn_StandardOrders(){
	
	  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","lblColumn4"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmScheduledPaymentsEurNew","List","lblColumn4"], "text")).not.toBe("");
	}
	
	async function clickOnAllTransfersFilter(){
	
	  var AllList=await VerifyAllTransfersList();
	
	  if(AllList){
	    appLog("Intiated method to click on Transers Filter");
	    await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","flxDropdown"],15000);
	    kony.automation.widget.touch(["frmPastPaymentsEurNew","SearchAndFilter","flxDropdown"], [43,22],null,null);
	    kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","SearchAndFilter","flxDropdown"]);
	    appLog("Successfully click on Transers Filter");
	  }else{
	    appLog("Custom Message : No Activities List is available");
	  }
	
	}
	
	async function clickOnStandardOrdersFilter(){
	
	  var StandardList=await VerifyStandardOrdersList();
	  if(StandardList){
	    appLog("Intiated method to click on Standard Order Filter");
	    await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","SearchAndFilter","flxDropdown"],15000);
	    kony.automation.widget.touch(["frmScheduledPaymentsEurNew","SearchAndFilter","flxDropdown"], [43,22],null,null);
	    kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","SearchAndFilter","flxDropdown"]);
	    appLog("Successfully clicked on Standard Order Filter");
	  }else{
	    appLog("Custom Message : No Activities List is available");
	  }
	}
	
	async function selectCompletedTransfers(){
	
	  await clickOnAllTransfersFilter();
	
	  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","segFilter"],15000);
	  kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","SearchAndFilter","segFilter[5]","flxRadioBtn"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Successfully Selected Completed Radio");
	
	  //await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","segmentTransfers"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","List","segmentTransfers[0]","lblColumn4"], "text")).toEqual("Completed");
	
	}
	
	async function selectSheduledTransfers(){
	
	  await clickOnAllTransfersFilter();
	
	  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","segFilter"],15000);
	  kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","SearchAndFilter","segFilter[4]","flxRadioBtn"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Successfully Selected Sheduled Radio");
	
	  //await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","segmentTransfers"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","List","segmentTransfers[0]","lblColumn4"], "text")).toEqual("Scheduled");
	
	}
	
	async function selectActiveOrders(){
	
	  await clickOnStandardOrdersFilter();
	
	  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","SearchAndFilter","segFilter"],15000);
	  kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","SearchAndFilter","segFilter[1]","flxRadioBtn"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Successfully Selected Active Radio");
	
	  //await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","segmentTransfers"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmScheduledPaymentsEurNew","List","segmentTransfers[0]","lblColumn4"], "text")).toEqual("Active");
	
	}
	
	async function SearchforPastTransferActivities(keyword){
	
	  appLog("Intiated method to search for keyword : <b>"+keyword+"</b>");
	
	  var PastTransfers=false;
	
	  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","SearchAndFilter","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmPastPaymentsEurNew","SearchAndFilter","txtSearch"],keyword);
	  kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","SearchAndFilter","flxSearchBtn"]);
	  //await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
	  appLog("Successfully clicked on Search button");
	
	  var ActivityList=await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","segmentTransfers"],30000);
	  if(ActivityList){
	    appLog("Successfully found Transcation with keyword : <b>"+keyword+"</b>");
	    PastTransfers=true;
	    //kony.automation.widget.touch(["frmPastPaymentsEurNew","List","segmentTransfers[0]","imgDropdown"], null,null,[9,8]);
	    kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","List","segmentTransfers[0]","flxDropdown"]);
	  }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","List","rtxNoPaymentMessage"],5000)){
	    appLog("Custom Message :: Failed with rtxNoPaymentMessage");
	  }else{
	    appLog("Custom Message :: Failed to Search Transcation from List");
	    //fail("Custom Message :: Failed to Search Transcation from List");
	  }
	
	  return PastTransfers;
	}
	
	async function SearchforSheduledTransferActivities(keyword){
	
	  appLog("Intiated method to search for keyword : <b>"+keyword+"</b>");
	
	  var SheduledTransfers=false;
	
	  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","SearchAndFilter","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmScheduledPaymentsEurNew","SearchAndFilter","txtSearch"],keyword);
	  kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","SearchAndFilter","flxSearchBtn"]);
	  //await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
	  appLog("Successfully clicked on Search button");
	
	  var ActivityList=await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","segmentTransfers"],30000);
	  if(ActivityList){
	    appLog("Successfully found Transcation with keyword : <b>"+keyword+"</b>");
	    SheduledTransfers=true;
	    kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","List","segmentTransfers[0]","flxDropdown"]);
	  }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","List","rtxNoPaymentMessage"],5000)){
	    appLog("Custom Message :: Failed with rtxNoPaymentMessage");
	  }else{
	    appLog("Custom Message :: Failed to Search Transcation from List");
	    //fail("Custom Message :: Failed to Search Transcation from List");
	  }
	
	  return SheduledTransfers;
	}
	
	async function clickOnRepeatButton(keyword){
	
	  var Repeat=await SearchforPastTransferActivities(keyword);
	  if(Repeat){
	    appLog("Intiated method to click on Repeat button");
	    kony.automation.button.click(["frmPastPaymentsEurNew","List","segmentTransfers[0]","btnAction"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
	    //await kony.automation.playback.wait(10000);
	    appLog("Successfully clicked on Repeat button");
	
	    // Add required Details - It was supposed to Populate all details. Some times not displaying details.
	
	    //     await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],15000);
	    //     var HaderText=kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text");
	    //     appLog("Header text of Payment/Transfer screen is : "+HaderText);
	    //     if(HaderText==='Payments'){
	    //       appLog("Repeating Payment");
	    //       await SelectToAccount(keyword);
	    //     }else if(HaderText==='Transfers'){
	    //       appLog("Repeating Transfers");
	    //       await SelectOwnTransferToAccount(keyword);
	    //     }else{
	    //       appLog("Failed to verify Payment/Transfer screen text : "+HaderText);
	    //     }
	
	    //await EnterNoteValue("Verify Repeat Functionality");
	    //await ConfirmTransfer();
	    //await VerifyTransferSuccessMessage();
	    await MoveBackToLandingScreen_Transfers();
	  }else{
	    appLog("Custom Message :: No Series available to Repeat");
	    await MoveBackFrom_PastTransferActivities();
	  }
	
	}
	
	async function clickOnEditButton(keyword){
	
	  var Edit=await SearchforSheduledTransferActivities(keyword);
	  if(Edit){
	    appLog("Intiated method to Edit Transfer");
	    kony.automation.button.click(["frmScheduledPaymentsEurNew","List","segmentTransfers[0]","btnAction"]);
	    //await kony.automation.playback.wait(10000);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
	    appLog("Successfully clicked on Edit button");
	
	    // Add required Details
	    //await EnterNoteValue("EditRecurringFunctionality");
	    //await ConfirmTransfer();
	    //await VerifyTransferSuccessMessage();
	    await MoveBackToLandingScreen_Transfers();
	  }else{
	    appLog("Custom Message :: No Series available to Edit");
	    await MoveBackFrom_SheduledTransferActivities();
	  }
	}
	
	async function clickOnCancelSeriesButton(keyword){
	
	  var Series=await SearchforSheduledTransferActivities(keyword);
	  if(Series){
	    appLog("Intiated method to cancel Series Recurring Transfer");
	    kony.automation.button.click(["frmScheduledPaymentsEurNew","List","segmentTransfers[0]","btn1"]);
	    kony.automation.button.click(["frmScheduledPaymentsEurNew","btnYesIC"]);
	    appLog("Successfully clicked on CANCEL button");
	    //await kony.automation.playback.wait(10000);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
	    var Success=await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","lblSuccessAcknowledgement"],45000);
	    if(Success){
	      expect(kony.automation.widget.getWidgetProperty(["frmScheduledPaymentsEurNew","lblSuccessAcknowledgement"], "text")).not.toBe("");
	      await MoveBackFrom_SheduledTransferActivities();
	    }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","rtxDowntimeWarning"],5000)){
	      appLog("Custom Message :: Failed with rtxDowntimeWarning");
	      fail("Faild :: Failed with rtxDowntimeWarning");
	      await MoveBackFrom_SheduledTransferActivities();
	    }else{
	      appLog("Custom Message :: Failed CANCEL Series");
	      fail("Custom Message :: Failed CANCEL Series");
	      await MoveBackFrom_SheduledTransferActivities();
	    }
	
	  }else{
	    appLog("Custom Message :: No Series available to cancel");
	    await MoveBackFrom_SheduledTransferActivities();
	  }
	
	}
	
	async function clickOnCancelTransferButton(keyword){
	
	  var Series=await SearchforPastTransferActivities(keyword);
	  if(Series){
	    appLog("Intiated method to cancel Sheduled Transfer");
	    kony.automation.button.click(["frmPastPaymentsEurNew","List","segmentTransfers[0]","btn1"]);
	    kony.automation.button.click(["frmPastPaymentsEurNew","btnYesIC"]);
	    appLog("Successfully clicked on CANCEL button");
	    //await kony.automation.playback.wait(10000);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(15000);
	    var Success=await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","lblSuccessAcknowledgement"],30000);
	    if(Success){
	      expect(kony.automation.widget.getWidgetProperty(["frmPastPaymentsEurNew","lblSuccessAcknowledgement"], "text")).not.toBe("");
	      await MoveBackFrom_PastTransferActivities();
	    }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","rtxDowntimeWarning"],5000)){
	      appLog("Custom Message :: Failed with rtxDowntimeWarning");
	      fail("Faild :: Failed with rtxDowntimeWarning");
	      await MoveBackFrom_PastTransferActivities();
	    }else{
	      appLog("Custom Message :: Failed CANCEL Transfer");
	      fail("Custom Message :: Failed CANCEL Transfer");
	      await MoveBackFrom_PastTransferActivities();
	    }
	  }else{
	    appLog("Custom Message :: No Series available to cancel");
	    await MoveBackFrom_PastTransferActivities();
	  }
	
	}
	
	async function MoveBackFrom_PastTransferActivities(){
	
	  appLog("Intiated method to moveback from PastTransferActivities");
	
	  await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	}
	
	async function MoveBackFrom_SheduledTransferActivities(){
	
	  appLog("Intiated method to moveback from SheduledTransferActivities");
	  await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	}
	
	async function navigateToMakePayments(){
	
	  appLog("Intiated method to Navigate Payments Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransferMoney"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransferMoney"]);
	  await kony.automation.playback.wait(15000);
	
	  await VerifyPaymentsScreen();
	
	}
	
	async function navigateToTransfers(){
	
	  appLog("Intiated method to Navigate Transfers Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  //kony.automation.widget.touch(["frmDashboard","customheader","topmenu","flxTransfersAndPay"], [105,12],null,null);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxPayBills"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxPayBills"]);
	  await kony.automation.playback.wait(10000);
	
	  await VerifyTransfersScreen();
	
	}
	
	async function VerifyTransfersScreen(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],30000);
	  var Status=kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text");
	  expect(Status).toEqual("Transfers","Failed to Navigate to Transfers Screen");
	  appLog("Successfully Navigated to Transfers Screen");
	}
	
	async function VerifyPaymentsScreen(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],30000);
	  var Status=kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text");
	  expect(Status).toEqual("Payments","Failed to Navigate to Payments Screen");
	  appLog("Successfully Navigated to MakePayment Screen");
	}
	
	async function SelectFromAccount(fromAcc){
	
	  appLog("Intiated method to Select From Account :: <b>"+fromAcc+"</b>");
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferFrom"],30000);
	  kony.automation.widget.touch(["frmMakePayment","txtTransferFrom"], [241,25],null,null);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferFrom"],fromAcc);
	  await kony.automation.playback.wait(3000);
	  await kony.automation.playback.waitFor(["frmMakePayment","segTransferFrom"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","segTransferFrom[0,0]","flxAmount"]);
	  appLog("Successfully Selected From Account from List");
	}
	
	async function ReSelectFromAccount(fromAcc){
	
	  appLog("Intiated method to Re-Select From Account :: <b>"+fromAcc+"</b>");
	  await kony.automation.playback.waitFor(["frmMakePayment","flxDropdown"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxDropdown"]);
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferFrom"],30000);
	  kony.automation.widget.touch(["frmMakePayment","txtTransferFrom"], [189,19],null,null);
	  await kony.automation.playback.waitFor(["frmMakePayment","flxMainWrapper"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxMainWrapper"]);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferFrom"],fromAcc);
	  kony.automation.flexcontainer.click(["frmMakePayment","segTransferFrom[0,0]","flxAmount"]);
	
	  appLog("Successfully Selected From Account from List");
	}
	
	async function SelectToAccount(ToAccReciptent){
	
	  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferTo"],15000);
	  //kony.automation.widget.touch(["frmMakePayment","txtTransferTo"], [150,23],null,null);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferTo"],ToAccReciptent);
	  await kony.automation.playback.wait(3000);
	  await kony.automation.playback.waitFor(["frmMakePayment","segTransferTo"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","segTransferTo[0]","flxAccountListItemWrapper"]);
	  appLog("Successfully Selected To Account from List");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	
	}
	
	async function SelectOwnTransferToAccount(ToAccReciptent){
	
	  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferTo"],15000);
	  //kony.automation.widget.touch(["frmMakePayment","txtTransferTo"], [150,23],null,null);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferTo"],ToAccReciptent);
	  await kony.automation.playback.waitFor(["frmMakePayment","segTransferTo"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","segTransferTo[0,0]","flxAccountListItemWrapper"]);
	
	  appLog("Successfully Selected To Account from List");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	
	}
	
	async function verifyExistingSameBanBeneficiaryDetails(){
	
	
	}
	
	async function verifyExistingDomesticBeneficiaryDetails(){
	
	
	}
	
	async function verifyExistingInternationalBeneficiaryDetails(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtSwift"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtSwift"], "text")).not.toBe("");
	  //   await kony.automation.playback.waitFor(["frmMakePayment","txtAddressLine01"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtAddressLine01"], "text")).not.toBe("");
	  //   await kony.automation.playback.waitFor(["frmMakePayment","txtCity"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtCity"], "text")).not.toBe("");
	  //   await kony.automation.playback.waitFor(["frmMakePayment","txtPostCode"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtPostCode"], "text")).not.toBe("");
	
	}
	
	
	async function EnterAmount(amountValue) {
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amountValue);
	  appLog("Successfully Entered Amount as : <b>"+amountValue+"</b>");
	  await kony.automation.scrollToWidget(["frmMakePayment","customfooternew","btnFaqs"]);
	}
	
	async function SelectFrequency(freqValue) {
	
	  //kony.automation.flexcontainer.click(["frmFastTransfers","flxContainer4"]);
	  await kony.automation.playback.waitFor(["frmMakePayment","lbxFrequency"],15000);
	  kony.automation.listbox.selectItem(["frmMakePayment","lbxFrequency"], freqValue);
	  appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	}
	
	async function SelectDateRange() {
	
	  //var today = new Date();
	  //kony.automation.calendar.selectDate(["frmConsolidatedStatements","calFromDate"], [(today.getDate()-2),(today.getMonth()+1),today.getFullYear()]);
	  await kony.automation.playback.waitFor(["frmMakePayment","calSendOnNew"],15000);
	  kony.automation.calendar.selectDate(["frmMakePayment","calSendOnNew"], [4,25,2021]);
	  await kony.automation.playback.waitFor(["frmMakePayment","calEndingOnNew"],15000);
	  kony.automation.calendar.selectDate(["frmMakePayment","calEndingOnNew"], [4,25,2022]);
	  appLog("Successfully Selected DateRange");
	}
	
	async function SelectSendOnDate() {
	
	  await kony.automation.playback.waitFor(["frmMakePayment","calSendOnNew"],15000);
	  kony.automation.calendar.selectDate(["frmMakePayment","calSendOnNew"], [4,25,2021]);
	  appLog("Successfully Selected SendOn Date");
	}
	
	async function selectNormalPaymentRadio(){
	
	  appLog("Intiated method to select Payment mode as Normal");
	  await kony.automation.playback.waitFor(["frmMakePayment","flxRadioBtn5"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxRadioBtn5"]);
	  appLog("Successfully Selected Payment mode as Normal");
	}
	
	async function selectFeePaidRadio(){
	
	  // sync issue where it's not selecting properly some times
	  appLog("Intiated method to Selected Fee Pay Radio");
	//   await kony.automation.playback.waitFor(["frmMakePayment","flxtooltipFeesImg"],30000);
	//   kony.automation.flexcontainer.click(["frmMakePayment","flxtooltipFeesImg"]);
	  // - flxShared is t0 just come out of cursor
	  await kony.automation.playback.waitFor(["frmMakePayment","flxShared"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxShared"]);
	
	  //await kony.automation.scrollToWidget(["frmMakePayment","lblRadioBtn3"]);
	  //kony.automation.widget.touch(["frmMakePayment","lblRadioBtn3"], null,null,[8,11]);
	  var isRadio=await kony.automation.playback.waitFor(["frmMakePayment","flxRadioBtn3"],45000);
	  expect(isRadio).toBe(true,"Failed to find Fee-Paid Radio");
	  //await kony.automation.scrollToWidget(["frmMakePayment","flxRadioBtn3"]);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxRadioBtn3"]);
	  appLog("Successfully Selected Fee Pay Radio");
	}
	
	async function EnterNoteValue(notes) {
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtPaymentReference"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtPaymentReference"],notes);
	  appLog("Successfully entered Note value as : <b>"+notes+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakePayment","btnConfirm"],15000);
	  var isEnable=kony.automation.widget.getWidgetProperty(["frmMakePayment","btnConfirm"], "enable");
	  if(isEnable){
	    appLog('Intiated method to click on Continue button');
	    await kony.automation.scrollToWidget(["frmMakePayment","btnConfirm"]);
	    kony.automation.button.click(["frmMakePayment","btnConfirm"]);
	    await kony.automation.playback.wait(5000);
	    appLog('Successfully Clicked on Continue button');
	
	    var Confirm=await kony.automation.playback.waitFor(["frmConfirmEuro","lblHeading"],45000);
	    if(Confirm){
	      appLog('Custom Message : Successfully moved to frmConfirmEuro Screen');
	    }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	      appLog('Custom Message : Failed with rtxMakeTransferError');
	      fail('Custom Message :'+kony.automation.widget.getWidgetProperty(["frmMakePayment","rtxMakeTransferError"], "text"));
	      await MoveBackToLandingScreen_Transfers();
	    }else{
	      appLog('Custom Message : Failed to Navigate to frmConfirmEuro Screen');
	      fail('Custom Message : Failed to Navigate to frmConfirmEuro Screen');
	      await MoveBackToLandingScreen_Transfers();
	    }
	  }else{
	    appLog('Custom Message : CONTINUE button is not enabled');
	  }
	
	}
	
	async function ValidatePaymentField_OwnTransfers(){
	
	  var Status=	await kony.automation.playback.waitFor(["frmConfirmEuro","lblPaymentMethodKey"],15000);
	  expect(Status).toBe(false,"Payment Method is not expected for Transfers");
	}
	
	async function ValidateSwiftCodeDetails_SameBankBenefeciary(){
	
	  var Status=	await kony.automation.playback.waitFor(["frmConfirmEuro","lblSWIFTBICKey"],15000);
	  expect(Status).toBe(false,"SWIFT Details are not expected for SameBank Benefeciary");
	
	}
	
	
	async function ConfirmTransfer() {
	
	  appLog("Intiated method to Confirm Transfer Details");
	
	  await kony.automation.playback.waitFor(["frmConfirmEuro","btnContinue"],30000);
	  kony.automation.button.click(["frmConfirmEuro","btnContinue"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	async function ClickOnModifyButton(){
	
	  await kony.automation.playback.waitFor(["frmConfirmEuro","btnModify"],15000);
	  kony.automation.button.click(["frmConfirmEuro","btnModify"]);
	  appLog("Successfully Clicked on btnModify Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text")).not.toBe("");
	}
	
	async function VerifyTransferSuccessMessage() {
	
	  //await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblSuccessMessage"],90000);
	
	  if(success){
	    appLog("Intiated method to Verify Transfer SuccessMessage");
	    //await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblSuccessMessage"],15000);
	    //expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblSuccessMessage"], "text")).not.toBe("");
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text"));
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	async function verifyDataCutOff_Ackform(){
	
	  //await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro"],60000);
	
	  if(success){
	    appLog("Intiated method to Verify Transfer SuccessMessage");
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblFrequencyValue"],15000);
	    expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblFrequencyValue"], "text")).not.toBe("");
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblPaymentReferenceValue"],15000);
	    expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblPaymentReferenceValue"], "text")).not.toBe("");
	
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	
	  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text"));
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	}
	
	async function MoveBackToLandingScreen_Transfers(){
	
	  //Move back to landing Screen
	  appLog("Intiated method to move from frmMakePayment screen");
	  await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	  var DashBoard=await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(DashBoard).toBe(true,"Failed to navigate to DashBoard");
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function MoveBackToLandingScreen_TransferConfirm(){
	
	  //Move back to landing Screen
	  appLog("Intiated method to move from frmFastTransfers screen");
	  await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	  var DashBoard=await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(DashBoard).toBe(true,"Failed to navigate to DashBoard");
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	
	async function EnterNewToAccountName(ToAccReciptent){
	
	  appLog("Intiated method to enter new To Account :: <b>"+ToAccReciptent+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferTo"],15000);
	  kony.automation.widget.touch(["frmMakePayment","txtTransferTo"], [150,23],null,null);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferTo"],ToAccReciptent);
	  appLog("Successfully entered New To acc name  as : <b>"+ToAccReciptent+"</b>");
	  // To laod dynamic data after selecting To account
	  //await kony.automation.playback.wait(5000);
	
	}
	
	async function clickOnNewButton_OneTimePay(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","lblNew"],15000);
	  kony.automation.widget.touch(["frmMakePayment","lblNew"], null,null,[12,11]);
	  await kony.automation.playback.waitFor(["frmMakePayment","flxCancelFilterTo"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxCancelFilterTo"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on new button for OneTimePay");
	
	}
	
	async function selectSameBankRadioBtn(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","flxBankOption1"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxBankOption1"]);
	  appLog("Successfully clicked on Radio button - This account is with us");
	}
	
	async function selectOtherBankRadioBtn(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","flxBankOption2"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxBankOption2"]);
	  appLog("Successfully clicked on Radio button - This account is Other bank");
	}
	
	async function enterOneTimePaymentDetails_SameBank(Accno,amount){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAccountNumber"],Accno);
	  appLog("Successfully entered acc no as : <b>"+Accno+"</b>");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amount);
	  appLog("Successfully entered amount as : <b>"+amount+"</b>");
	}
	
	async function enterOneTimePaymentDetails_Domestic(IBAN,amount){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAccountNumber"],IBAN);
	  appLog("Successfully entered acc no as : <b>"+IBAN+"</b>");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amount);
	  appLog("Successfully entered amount as : <b>"+amount+"</b>");
	}
	async function enterOneTimePaymentDetails_International(Accno,Swift,amount){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAccountNumber"],Accno);
	  appLog("Successfully entered acc no as : <b>"+Accno+"</b>");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmMakePayment","txtSwift"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtSwift"],Swift);
	  appLog("Successfully entered SWIFT as : <b>"+Swift+"</b>");
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amount);
	  appLog("Successfully entered amount as : <b>"+amount+"</b>");
	}
	
	async function VerifyOneTimePaymentSuccessMessage(){
	
	  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro"],60000);
	
	  if(success){
	    appLog("Intiated method to Verify OneTimePaymentSuccessMessage");
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmMakePayment","rtxMakeTransferError"], "text"));
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	async function SaveOneTimePaymentBenefeciary(){
	
	  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro","btnSaveBeneficiary"],60000);
	
	  if(success){
	    kony.automation.button.click(["frmAcknowledgementEuro","btnSaveBeneficiary"]);
	    appLog("Successfully clicked on btnSaveBeneficiary");
	    if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblSuccessmesg"],10000)){
	      //expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblSuccessmesg"], "text")).not.toBe("");
	      appLog("Info : Benefeciary is Saved Successfully");
	    }else{
	      await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblFailureMsg"],5000)
	      //expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblFailureMsg"], "text")).toEqual("jndj");
	      appLog("Warning : Same Benefeciary is Already Saved previousely");
	    }
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	
	  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : rtxMakeTransferError");
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	
	
	async function NavigateToWireTransfer_AddRecipitent(){
	
	  appLog("Intiated method to Navigate AddRecipitent Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"]);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("successfully Navigated to AddRecipitent Screen");
	
	}
	
	async function EnterDomestic_RecipitentDetails_Step1(RecipientName,AddressLine1,AddressLine2,City,ZipCode){
	
	  appLog("Intiated method to Enter Domestic Recipitent details - Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"],RecipientName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxAddressLine1"],AddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxAddressLine2"],AddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxCity"],City);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","lbxState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferAddKonyAccountStep1","lbxState"], "Dubai");
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxZipcode"],ZipCode);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","btnProceed"],15000);
	  kony.automation.button.click(["frmWireTransferAddKonyAccountStep1","btnProceed"]);
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","lblStep2"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep2","lblStep2"], "text")).toContain("Step 2");
	
	  appLog("Successfully Entered Domestic Recipitent details - Step1");
	}
	
	async function EnterDomestic_BankDetails_Step2(RoutingNumber,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){
	
	  appLog("Intiated method to Enter Domestic Bank details - Step2");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxSwiftCode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxSwiftCode"],RoutingNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxReAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxReAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxNickName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxNickName"],NickName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankName"],BankName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine1"],BankAddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine2"],BankAddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankCity"],BankCity);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","lbxBankState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferAddKonyAccountStep2","lbxBankState"], "Dubai");
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankZipcode"],BankZipcode);
	  await kony.automation.scrollToWidget(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"]);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"],15000);
	  kony.automation.button.click(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"]);
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountConfirm","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully Entered Domestic Bank details - Step2");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmWireTransferAddKonyAccountConfirm","btnConfirm"]);
	
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	async function VerifyAddDomesticRecipitentSuccessMsg(){
	
	  appLog("Intiated method to verify AddDomesticRecipitent SuccessMsg");
	
	  var Success=await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","lblSuccessAcknowledgement"],15000);
	  if(Success){
	    await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","rtxDowntimeWarning"],15000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","rtxDowntimeWarning"], "text")).toEqual("SOME");
	    await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	
	    appLog("Failed with : rtxDowntimeWarning");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","rtxDowntimeWarning"], "text"));
	  }else{
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to add Recipitent");
	    fail("Unable to add Recipitent");
	  }
	}
	
	async function EnterInternational_RecipitentDetails_Step1(RecipientName,AddressLine1,AddressLine2,City,ZipCode){
	
	  appLog("Intiated method to Enter International Recipitent details - Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","btnInternationalAccount"],15000);
	  kony.automation.button.click(["frmWireTransferAddKonyAccountStep1","btnInternationalAccount"]);
	  await kony.automation.playback.wait(10000);
	  var Status=await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","lbxCountry"],15000);
	  expect(Status).toBe(true,"Failed to Navigate to International recipitent")
	  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep1","lbxCountry"], "AU");
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxRecipientName"],RecipientName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine1"],AddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine2"],AddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxCity"],City);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","lbxState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep1","lbxState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxZipcode"],ZipCode);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","btnProceed"],15000);
	  kony.automation.button.click(["frmWireTransferAddInternationalAccountStep1","btnProceed"]);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","lblStep2"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep2","lblStep2"], "text")).toContain("Step 2");
	
	  appLog("Successfully Entered International Recipitent details - Step1");
	}
	
	async function EnterInternational_BankDetails_Step2(SwiftCode,IBAN,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){
	
	  appLog("Intiated method to Enter International Bank details - Step2");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxSwiftCode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxSwiftCode"],SwiftCode);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxIBANOrIRC"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxIBANOrIRC"],IBAN);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxReAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxReAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxNickName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxNickName"],NickName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankName"],BankName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine1"],BankAddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine2"],BankAddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankCity"],BankCity);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","lbxBankState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep2","lbxBankState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankZipcode"],BankZipcode);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","btnAddRecipent"],15000);
	  kony.automation.button.click(["frmWireTransferAddInternationalAccountStep2","btnAddRecipent"]);
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountConfirm","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully Entered International Bank details - Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmWireTransferAddInternationalAccountConfirm","btnConfirm"]);
	
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	
	async function VerifyAddInternationalRecipitentSuccessMsg(){
	
	  appLog("Intiated method to verify AddInternationalRecipitentSuccessMsg SuccessMsg");
	
	  var Success=await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","lblSuccessAcknowledgement"],15000);
	  if(Success){
	    await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","rtxDowntimeWarning"],15000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep1","rtxDowntimeWarning"], "text")).toEqual("SOME");
	    await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	
	    appLog("Failed with : rtxDowntimeWarning");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep1","rtxDowntimeWarning"], "text"));
	  }else{
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to add Recipitent");
	    fail("Unable to add Recipitent");
	  }
	}
	
	
	async function NavigateToMakeTransfer(){
	
	  appLog("Intiated method to Navigate to Make Transfers");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  //await kony.automation.playback.waitFor(["frmWireTransfersWindow","lblAddAccountHeading"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmWireTransfersWindow","lblAddAccountHeading"], "text")).not.toBe("");
	  appLog("Succesfully Navigated to Make Transfers screen");
	}
	
	async function ClickOnMakeTransferLink(){
	
	  appLog("Intiated method to click on Make Transfer Link ");
	  await kony.automation.playback.waitFor(["frmWireTransfersWindow","segWireTransfers"],15000);
	  kony.automation.button.click(["frmWireTransfersWindow","segWireTransfers[0]","btnAction"]);
	  await kony.automation.playback.wait(10000);
	  //await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","lblAddAccountHeading"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmWireTransferMakeTransfer","lblAddAccountHeading"], "text")).not.toBe("");
	  appLog("Successfully clicked on Make Transfer Link ");
	
	}
	
	async function MakeWireTransfer(){
	
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmWireTransferMakeTransfer","txtTransferFrom"], [110,12],null,null);
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","segTransferFrom"],15000);
	  kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","segTransferFrom[0,0]","flxAmount"]);
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","tbxAmount"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferMakeTransfer","tbxAmount"],"1.5");
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","tbxNotes"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferMakeTransfer","tbxNotes"],"TEST");
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","btnStepContinue"],15000);
	  kony.automation.button.click(["frmWireTransferMakeTransfer","btnStepContinue"]);
	  await kony.automation.playback.wait(5000);
	  //await kony.automation.playback.waitFor(["frmConfirmDetails","lblAddAccountHeading"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblAddAccountHeading"], "text")).not.toBe("");
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"],15000);
	  await kony.automation.scrollToWidget(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"]);
	  kony.automation.widget.touch(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"], null,null,[16,11]);
	  await kony.automation.playback.waitFor(["frmConfirmDetails","flxAgree"],15000);
	  kony.automation.flexcontainer.click(["frmConfirmDetails","flxAgree"]);
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","btnConfirm"],15000);
	  kony.automation.button.click(["frmConfirmDetails","btnConfirm"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","lblSuccessAcknowledgement"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblSuccessAcknowledgement"], "text")).not.toBe("");
	  await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	
	}
	
	async function NavigateToWireOneTimePayment(){
	
	  appLog("Intiated method to navigate to OneTime WireTransfer");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer4flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferOneTimePaymentStep1","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully navigated to OneTime WireTransfer");
	
	}
	async function EnterOneTimeDomesticRecipitentDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode){
	
	  appLog("Intiated method to Enter OneTime-DomesticRecipitentDetails- Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxRecipientName"],RecipientName);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxAddressLine1"],AddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxAddressLine2"],AddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxCity"],City);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","lbxState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferOneTimePaymentStep1","lbxState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxZipcode"],ZipCode);
	
	  appLog("Successfully Entered OneTime-DomesticRecipitentDetails- Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","btnProceed"],15000);
	  await kony.automation.scrollToWidget(["frmWireTransferOneTimePaymentStep1","btnProceed"]);
	  kony.automation.button.click(["frmWireTransferOneTimePaymentStep1","btnProceed"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Proceed Button");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","lblStep2"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferOneTimePaymentStep2","lblStep2"], "text")).not.toBe("");
	  appLog("Successfully Verified Step2 screen");
	}
	
	async function EnterOneTimeDomesticBankDetails(SwiftCode,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){
	
	  appLog("Intiated method to enter OneTime-DomesticBankDetails-Step2");
	
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxSwiftCode"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxSwiftCode"],SwiftCode);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxAccountNumber"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxReAccountNumber"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxReAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxNickName"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxNickName"],NickName);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankName"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankName"],BankName);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine1"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine1"],BankAddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine2"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine2"],BankAddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankCity"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankCity"],BankCity);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","lbxBankState"]);
	  kony.automation.listbox.selectItem(["frmWireTransferOneTimePaymentStep2","lbxBankState"], "Indiana");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankZipcode"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankZipcode"],BankZipcode);
	
	  appLog("Successfully Entered OneTime-DomesticRecipitentDetails- Step1");
	
	  await kony.automation.scrollToWidget(["frmWireTransferOneTimePaymentStep2","btnStep2Proceed"]);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","btnStep2Proceed"]);
	  kony.automation.button.click(["frmWireTransferOneTimePaymentStep2","btnStep2Proceed"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Proceed Button");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","lblAddAccountHeading"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferOneTimePaymentStep3","lblStep3"], "text")).not.toBe("");
	  appLog("Successfully Verified Step3 screen");
	}
	
	async function MakeOneTimeWireTransfer(){
	
	  appLog("Intiated method to Make-OneTimeTransfer");
	
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmWireTransferOneTimePaymentStep3","txtTransferFrom"], [140,21],null,null);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","segTransferFrom"],15000);
	  kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","segTransferFrom[0,0]","flxAmount"]);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","tbxAmount"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep3","tbxAmount"],"1.5");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","tbxNote"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep3","tbxNote"],"OneTimePayment");
	  await kony.automation.scrollToWidget(["frmWireTransferOneTimePaymentStep3","btnStep3MakeTransfer"]);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","btnStep3MakeTransfer"],15000);
	  kony.automation.button.click(["frmWireTransferOneTimePaymentStep3","btnStep3MakeTransfer"]);
	  appLog("Successfully clicked on Make-OneTimeTransfer button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmConfirmDetails","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblAddAccountHeading"], "text")).not.toBe("");
	  appLog("Successfully clicked on Make-OneTimeTransfer button");
	
	  await ConfirmDomesticOneTimeWireTransfer();
	
	}
	
	async function ConfirmDomesticOneTimeWireTransfer(){
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"],15000);
	  kony.automation.widget.touch(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"], null,null,[14,16]);
	  await kony.automation.playback.waitFor(["frmConfirmDetails","flxAgree"],15000);
	  kony.automation.flexcontainer.click(["frmConfirmDetails","flxAgree"]);
	  appLog("Successfully Accepted Terms and conditions");
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","btnConfirm"],15000);
	  kony.automation.button.click(["frmConfirmDetails","btnConfirm"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on confirm button");
	
	}
	
	async function VerifyOneTimeDomesticWireTransferSuccessMsg(){
	
	  var success=await kony.automation.playback.waitFor(["frmConfirmDetails","lblSuccessAcknowledgement"],15000);
	  if(success){
	    //expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblSuccessAcknowledgement"], "text")).not.toBe("");
	    appLog("Successfully Verified Onetime Wire Transfer Success Message");
	    await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else{
	    appLog("Unable to Perform OneTimeDomestic WireTransfers");
	    fail("Unable to Perform OneTimeDomestic WireTransfers");
	
	  }
	
	}
	
	async function NavigatetoCreateNewTemplate(){
	
	  appLog("Intiated method to navigate to create Template");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer5flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer5flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	  //await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","lblAddAccountHeading"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmCreateTemplatePrimaryDetails","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully navigated to NewTemplate creation");
	}
	
	async function EnterTemplateDetails(TemplateName){
	
	  await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmCreateTemplatePrimaryDetails","tbxRecipientName"],TemplateName);
	  appLog("Successfully entered template name");
	  await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","btnContinue"],15000);
	  await kony.automation.scrollToWidget(["frmCreateTemplatePrimaryDetails","btnContinue"]);
	  kony.automation.button.click(["frmCreateTemplatePrimaryDetails","btnContinue"]);
	  appLog("Successfully cliced on Continue button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","lblHeading"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmBulkTemplateAddRecipients","lblHeading"], "text")).not.toBe("");
	}
	
	async function selectExistingRecipitentOption(){
	
	  await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","lblExistingRecipients"],15000);
	  kony.automation.widget.touch(["frmBulkTemplateAddRecipients","lblExistingRecipients"], null,null,[16,14]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully selected existing recipitent option");
	  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCreateTempSelectRecipients","lblAddAccountHeading"], "text")).not.toBe("");
	}
	
	async function CreateNewTemplate_Existingrecipitent(){
	
	  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","lblStatus"],15000);
	  kony.automation.widget.touch(["frmCreateTempSelectRecipients","lblStatus"], null,null,[16,14]);
	  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","flxSelectAll"],15000);
	  kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","flxSelectAll"]);
	  appLog("Successfully selected recipitent Select All Button");
	
	  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","btnContinue"],15000);
	  kony.automation.button.click(["frmCreateTempSelectRecipients","btnContinue"]);
	  appLog("Successfully selected Continue button");
	
	  await VerifyNewTemplateSuccessMsg();
	
	}
	
	async function selectNewManualRecipitentOption(){
	
	  await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","lblManualRecipients"],15000);
	  kony.automation.widget.touch(["frmBulkTemplateAddRecipients","lblManualRecipients"], null,null,[16,14]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully selected New Manual Recipitent option");
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCreateTempAddDomestic","lblAddAccountHeading"], "text")).not.toBe("");
	}
	
	async function EnterManualRecipitentTemplateDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode,RoutingNumber,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){
	
	  appLog("Intiated method to enter ManualRecipitentTemplateDetails");
	
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRecipientName"],RecipientName);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxAddressLine1"],AddressLine1);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxAddressLine2"],AddressLine2);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxCity"],City);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","lbxState"]);
	  kony.automation.listbox.selectItem(["frmCreateTempAddDomestic","lbxState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxZipcode"],ZipCode);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRoutingNumber"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRoutingNumber"],RoutingNumber);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRecipientAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRecipientAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxReEnterAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxReEnterAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxAccountNickName"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxAccountNickName"],NickName);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRecipientBankName"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRecipientBankName"],BankName);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxBankAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxBankAddressLine1"],BankAddressLine1);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxBankAddressLIne2"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxBankAddressLIne2"],BankAddressLine2);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxCompanyCity"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxCompanyCity"],BankCity);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","lbxCompanyState"]);
	  kony.automation.listbox.selectItem(["frmCreateTempAddDomestic","lbxCompanyState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxCompanyZipCode"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxCompanyZipCode"],BankZipcode);
	
	  appLog("Successfully entered ManualRecipitentTemplateDetails");
	
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","btnProceed"],15000);
	  kony.automation.button.click(["frmCreateTempAddDomestic","btnProceed"]);
	  appLog("Successfully Clicked on Proceed button");
	}
	
	
	async function VerifyNewTemplateSuccessMsg(){
	
	  var Success=await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","rtxMakeTransferError"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmBulkTemplateAddRecipients","rtxMakeTransferError"], "text")).not.toBe("");
	  if(Success){
	    appLog("Successfully created new Template");
	    await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else{
	    appLog("Failed to create Template -Existing Recipitent");
	    fail("Failed to create Template -Existing Recipitent");
	  }
	
	}
	
	async function NavigateToWireHistoryTab(){
	
	  appLog("Intiated method to navigate to History Tab");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer1flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer1flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmWireTransfersRecent","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransfersRecent","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully Navigated to History Tab");
	}
	
	async function VerifyWireTransferHistory(){
	
	  appLog("Intiated method to check Transfer History");
	
	  var History=await kony.automation.playback.waitFor(["frmWireTransfersRecent","segWireTransfers"],15000);
	  if(History){
	    kony.automation.flexcontainer.click(["frmWireTransfersRecent","segWireTransfers[0]","flxDropdown"]);
	    appLog("Successfully verified History");
	    await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	    appLog("Successfully ovedBack to DashBoard");
	  }else{
	    appLog("Custom Message : No History availble at this moment");
	    await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	  }
	}
	
	async function NavigateToWireRecipitentsTab(){
	
	  appLog("Intiated method to WireRecipitents Tab");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer2flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer2flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransfersManageRecipients","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully Navigated to WireRecipitents Tab");
	}
	
	async function VerifyWireRecipitents(){
	
	  appLog("Intiated method to check wire Recipitents");
	
	  var List=await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","segWireTransfers"],15000);
	  if(List){
	    kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","segWireTransfers[0]","flxDropdown"]);
	    appLog("Successfully verified Wire recipitents");
	    await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	    appLog("Successfully ovedBack to DashBoard");
	  }else{
	    appLog("Custom Message : No Recipitents availble at this moment");
	    await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	  }
	}
	
	
	async function ActivateWireTransfersTermsConditions(){
	
	  // First click on Any one of Wire Transfer
	  appLog("Intiated method to Navigate to Activate WireTransfer");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Successfully Navigated to wire transfer screen");
	
	  var Terms=await kony.automation.playback.waitFor(["frmActivateWireTransfer","lblAddAccountHeading"],15000);
	  if(Terms){
	    appLog("Intiated method to accept terms and conditions");
	    await kony.automation.playback.waitFor(["frmActivateWireTransfer","lbxDefaultAccountForSending"],15000);
	    await kony.automation.scrollToWidget(["frmActivateWireTransfer","lbxDefaultAccountForSending"]);
	    kony.automation.listbox.selectItem(["frmActivateWireTransfer","lbxDefaultAccountForSending"],AllAccounts.Current.accno);
	    appLog("Successfully Selected default account number");
	    await kony.automation.playback.waitFor(["frmActivateWireTransfer","lblFavoriteEmailCheckBoxMain"],15000);
	    kony.automation.widget.touch(["frmActivateWireTransfer","lblFavoriteEmailCheckBoxMain"], null,null,[10,22]);
	    await kony.automation.playback.waitFor(["frmActivateWireTransfer","flxCheckbox"],15000);
	    kony.automation.flexcontainer.click(["frmActivateWireTransfer","flxCheckbox"]);
	    appLog("Successfully accepted Terms and conditions");
	    await kony.automation.playback.waitFor(["frmActivateWireTransfer","btnProceed"],15000);
	    kony.automation.button.click(["frmActivateWireTransfer","btnProceed"]);
	    appLog("Successfully clicked on Proceed button");
	    await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	    appLog("Successfully Navigated to Accounts DashBoard");
	
	  }else{
	    appLog("WireTransfers is activated already");
	    await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	    appLog("Successfully Navigated to Accounts DashBoard");
	  }
	}
	
	
	
	async function ClickonCustomviewDropdown(){
	
	  appLog("Intiated method to click on custom View dropdown");
	  await kony.automation.playback.waitFor(["frmDashboard","flxDropDown"],30000);
	    kony.automation.widget.touch(["frmDashboard","flxDropDown"], [6,15],null,null);
	  kony.automation.flexcontainer.click(["frmDashboard","flxDropDown"]);
	  appLog("successfully click on custom View dropdown");
	  await kony.automation.playback.wait(5000);
	  //appLog("Intiated method to Verify Add New+ Flex");
	  //var Status=await kony.automation.playback.waitFor(["frmDashboard","accountsFilter","flxAddNew"],30000);
	  //expect(Status).toBe(true,'Failed to Verify Add New+ Flex');
	}
	
	async function ClickonAddNewFlex(){
	
	  appLog("Intiated method to click on Add New+ Flex");
	  await kony.automation.playback.waitFor(["frmDashboard","accountsFilter","flxAddNew"],15000);
	    kony.automation.flexcontainer.click(["frmDashboard","accountsFilter","flxAddNew"]);
	  kony.automation.widget.touch(["frmDashboard","accountsFilter","flxAddNew"], [6,15],null,null);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Add New+ Flex");
	  var Status=await kony.automation.playback.waitFor(["frmCustomViews","lblCustomView"],15000);
	  expect(Status).toBe(true,"Failed to Navigate to frmCustomViews");
	
	}
	
	async function MoveBackfrom_CustomView(){
	
	  appLog('Intiated method to moveback from CustomViews screen');
	  await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	  await verifyAccountsLandingScreen();
	  appLog('Successfully Moved back from  Custom view screen');
	}
	
	async function EnterCustomViewName(customViewName){
	
	  appLog("Intiated method to Enter View name :: <b>"+customViewName+"</b>");
	  await kony.automation.playback.waitFor(["frmCustomViews","txtCustomViewName"],15000);
	  kony.automation.textbox.enterText(["frmCustomViews","txtCustomViewName"],customViewName);
	  appLog("Successfully Entered Custom View Name");
	}
	
	async function VerifyDuplicateViewNameError(){
	
	  await kony.automation.playback.waitFor(["frmCustomViews","lbltxtCustomViewNameValid"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCustomViews","lbltxtCustomViewNameValid"], "text")).not.toBe("");
	
	  await MoveBackfrom_CustomView();
	}
	
	async function SearchAccounts_forView(AccountName){
	
	  appLog("Intiated method to Search for accounts for view :: <b>"+AccountName+"</b>");
	  await kony.automation.playback.waitFor(["frmCustomViews","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmCustomViews","txtSearch"],AccountName);
	  appLog("Successfully entered account to search");
	  await kony.automation.playback.waitFor(["frmCustomViews","btnConfirm"],15000);
	  kony.automation.flexcontainer.click(["frmCustomViews","btnConfirm"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Search button");
	}
	
	async function SelectAccounts_forView(){
	
	  appLog("Intiated method to select CheckBox for accounts");
	  await kony.automation.playback.waitFor(["frmCustomViews","segCustomViews"],15000);
	  kony.automation.flexcontainer.click(["frmCustomViews","segCustomViews[0,-1]","flxChecked"]);
	  appLog("Successfully selected CheckBox for accounts");
	}
	
	async function ClickonCreateButton(){
	
	  appLog("Intiated method to click on create button");
	  await kony.automation.playback.waitFor(["frmCustomViews","btnCreate"],15000);
	  kony.automation.button.click(["frmCustomViews","btnCreate"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on create button");
	}
	
	async function VerifySelectedViewName_onDashBoard(ViewName){
	
	  appLog("Intiated method to verify current view Name");
	  await kony.automation.playback.waitFor(["frmDashboard","lblSelectedFilter"],30000);
	  var selectedViewName=kony.automation.widget.getWidgetProperty(["frmDashboard","lblSelectedFilter"], "text");
	  if(selectedViewName===ViewName){
	    appLog("View Name created successfully:: <b>"+ViewName+"</b>");
	  }else{
	    appLog("View Name :: <b>"+ViewName+"</b>"+"Doesn't match with selected one :: "+selectedViewName+"</b>");
	    fail("View Name :: <b>"+ViewName+"</b>"+"Doesn't match with selected one :: "+selectedViewName+"</b>");
	  }
	}
	
	async function ClickonEditButton(){
	
	  appLog("Intiated method to click on EDIT Option");
	  await kony.automation.playback.waitFor(["frmDashboard","accountsFilter","segCustomFiltersHeader"],30000);
	  kony.automation.widget.touch(["frmDashboard","accountsFilter","segCustomFiltersHeader[0]","flxEdit"], null,null,[1,24]);
	  kony.automation.flexcontainer.click(["frmDashboard","accountsFilter","segCustomFiltersHeader[0]","flxEdit"]);
	  appLog("Successfully clicked on EDIT Option");
	  var Status=await kony.automation.playback.waitFor(["frmCustomViews","lblCustomView"],15000);
	  expect(Status).toBe(true,"Failed to Navigate to frmCustomViews");
	}
	
	async function DeleteCustomView(){
	
	  appLog("Intiated method to click on DELETE Option");
	  await kony.automation.playback.waitFor(["frmCustomViews","btnDelete"],15000);
	  kony.automation.button.click(["frmCustomViews","btnDelete"]);
	  appLog("Successfully clicked on DELETE Option");
	  await kony.automation.playback.waitFor(["frmCustomViews","deletePopup","btnYes"],15000);
	  kony.automation.button.click(["frmCustomViews","deletePopup","btnYes"]);
	  appLog("Successfully clicked on YES button");
	  await kony.automation.playback.wait(5000);
	  appLog("Intiated method to verify DashBoard screen after View deletion");
	  var Status=await kony.automation.playback.waitFor(["frmDashboard","lblSelectedFilter"],30000);
	  expect(Status).toBe(true,'Failed to Move to DashBoard after view deletion');
	}
	
	
	
	async function navigateToUnifiedTransfers(){
	
	  appLog("Intiated method to Navigate UTF Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","UNIFIEDTRANSFERflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","UNIFIEDTRANSFERflxAccountsMenu"]);
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  await VerifyUTFScreen();
	
	}
	
	async function VerifyUTFScreen(){
	
	  var Status=await kony.automation.playback.waitFor(["frmLanding","lblSelectTransfer"],30000);
	  expect(Status).toBe(true,"Failed to Navigate to UTF Screen");
	  //var Status=kony.automation.widget.getWidgetProperty(["frmLanding","lblSelectTransfer"], "text");
	  //expect(Status).toEqual("Select Transfer Type","Failed to Navigate to UTF Screen");
	  //appLog("Successfully Navigated to UTF Screen");
	
	}
	
	async function clickOnAddNewAccountBtn_SameBank(){
	
	  appLog("Intiated method to Click on AddNewAccount button");
	  await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection1","btnAction2"],15000);
	  kony.automation.button.click(["frmLanding","transferTypeSelection1","btnAction2"]);
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  appLog("Successfully Clicked on AddNewAccount button");
	  await VerifyAddNewaccountHeader();
	}
	
	async function clickOnAddNewAccountBtn_Domestic(){
	
	  appLog("Intiated method to Click on AddNewAccount button");
	  await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection2","btnAction2"],15000);
	  kony.automation.button.click(["frmLanding","transferTypeSelection2","btnAction2"]);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  appLog("Successfully Clicked on AddNewAccount button");
	  await VerifyAddNewaccountHeader();
	}
	
	async function clickOnAddNewAccountBtn_International(){
	
	  appLog("Intiated method to Click on AddNewAccount button");
	  await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection3","btnAction2"],15000);
	  kony.automation.button.click(["frmLanding","transferTypeSelection3","btnAction2"]);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  appLog("Successfully Clicked on AddNewAccount button");
	  await VerifyAddNewaccountHeader();
	}
	
	async function clickOnAddNewAccountBtn_PayAPerson(){
	
	  appLog("Intiated method to Click on AddNewAccount button");
	  await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection4","btnAction2"],15000);
	  kony.automation.button.click(["frmLanding","transferTypeSelection4","btnAction2"]);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  appLog("Successfully Clicked on AddNewAccount button");
	  await VerifyAddNewaccountHeader();
	}
	
	async function VerifyAddNewaccountHeader(){
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"lblAddNewAccount"],15000);
	  var Status=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"lblAddNewAccount"],"text");
	  expect(Status).toBe("Add New Account","Failed to Navigate to New acount Screen");
	}
	
	async function EnterInternationalAccDetails(BeneficiaryName,AccountNumber,SwiftCode,Nickname){
	
	  appLog("Intiated method to enter International acc details");
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],15000);
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],BeneficiaryName);
	  appLog("Successfully entered benefeciary name as :: <b>"+BeneficiaryName+"</b>");
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNewAccountNumber"],AccountNumber);
	  appLog("Successfully entered acc number as :: <b>"+AccountNumber+"</b>");
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxReenterAccountNumber"],AccountNumber);
	  appLog("Successfully Re-entered acc number as :: <b>"+AccountNumber+"</b>");
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeDetailField1"],SwiftCode);
	  appLog("Successfully entered SWIFT number as :: <b>"+SwiftCode+"</b>");
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNickName"],Nickname);
	  appLog("Successfully entered Nick name as :: <b>"+Nickname+"</b>");
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"],15000);
	  await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
	  kony.automation.button.click([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
	  appLog("Successfully clicked on Continue button");
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	
	  var Status=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000);
	  //var Status=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"lblHeader"], "text");
	  expect(Status).toBe(true,"Failed to Navigate to confirm Screen");
	}
	
	async function EnterDomesticAccDetails(BeneficiaryName,IBAN,Nickname){
	
	  appLog("Intiated method to enter Domestic acc details");
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],15000);
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],BeneficiaryName);
	  appLog("Successfully entered benefeciary name as :: <b>"+BeneficiaryName+"</b>");
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNewAccountNumber"],IBAN);
	  appLog("Successfully entered acc number as :: <b>"+IBAN+"</b>");
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxReenterAccountNumber"],IBAN);
	  appLog("Successfully Re-entered acc number as :: <b>"+IBAN+"</b>");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNickName"],Nickname);
	  appLog("Successfully entered Nick name as :: <b>"+Nickname+"</b>");
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"],15000);
	  await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
	  kony.automation.button.click([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
	  appLog("Successfully clicked on Continue button");
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	
	  var Status=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000);
	  //var Status=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"lblHeader"], "text");
	  expect(Status).toBe(true,"Failed to Navigate to confirm Screen");
	}
	
	async function EnterSameBankAccDetails(BeneficiaryName,AccountNumber,Nickname){
	
	  appLog("Intiated method to enter Domestic acc details");
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],15000);
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxPayeeName"],BeneficiaryName);
	  appLog("Successfully entered benefeciary name as :: <b>"+BeneficiaryName+"</b>");
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNewAccountNumber"],AccountNumber);
	  appLog("Successfully entered acc number as :: <b>"+AccountNumber+"</b>");
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxReenterAccountNumber"],AccountNumber);
	  appLog("Successfully Re-entered acc number as :: <b>"+AccountNumber+"</b>");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","tbxNickName"],Nickname);
	  appLog("Successfully entered Nick name as :: <b>"+Nickname+"</b>");
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"],15000);
	  await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
	  kony.automation.button.click([kony.automation.getCurrentForm(),"unifiedAddBeneficiary","btnContinue"]);
	  appLog("Successfully clicked on Continue button");
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	
	  var Status=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000);
	  //var Status=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"lblHeader"], "text");
	  expect(Status).toBe(true,"Failed to Navigate to confirm Screen");
	}
	
	
	async function ClickonConfirmButton(){
	
	  appLog("Intiated method to click on Confirm button");
	  kony.automation.button.click([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"]);
	  appLog("Successfully clicked on Confirm button");
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	
	}
	
	async function VerifyAddNewAccSuccessMsg(){
	
	  appLog("Intiated method to Verify AddNewAcc SuccessMsg");
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"TransferAcknowledgement","lblSection1Message"],30000);
	  expect(kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"TransferAcknowledgement","lblSection1Message"], "text")).not.toBe("");
	  appLog("Successfully Verified AddNewAcc SuccessMsg");
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to account dashboard");
	}
	
	async function ClickonMakeTransfrBtn(TransferType){
	
	  appLog("Intiated method to click on MakeTransfer button");
	
	  switch (TransferType) {
	    case "SameBank":
	      await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection1","btnAction1"],15000);
	      kony.automation.button.click(["frmLanding","transferTypeSelection1","btnAction1"]);
	      appLog("Successfully Clicked on MakeTransfer: <b>"+TransferType+"</b>");
	      break;
	    case "Domestic":
	      await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection2","btnAction1"],15000);
	      kony.automation.button.click(["frmLanding","transferTypeSelection2","btnAction1"]);
	      appLog("Successfully Clicked on MakeTransfer: <b>"+TransferType+"</b>");
	      break;
	    case "International":
	      await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection3","btnAction1"],15000);
	      kony.automation.button.click(["frmLanding","transferTypeSelection3","btnAction1"]);
	      appLog("Successfully Clicked on MakeTransfer: <b>"+TransferType+"</b>");
	      break;
	    case "PayAPersoon":
	      await kony.automation.playback.waitFor(["frmLanding","transferTypeSelection4","btnAction1"],15000);
	      kony.automation.button.click(["frmLanding","transferTypeSelection4","btnAction1"]);
	      appLog("Successfully Clicked on MakeTransfer: <b>"+TransferType+"</b>");
	      break;
	  }
	
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  appLog("Successfully clicked on MakeTransfer button");
	}
	
	async function SelectUTFFromAccount(fromAcc){
	
	  appLog("Intiated method to Select From Account :: <b>"+fromAcc+"</b>");
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","flxTextBox"],30000);
	  kony.automation.widget.touch([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","flxTextBox"], [241,25],null,null);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","flxTextBox","txtBox"],30000);
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","flxTextBox","txtBox"],fromAcc);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","segRecords"],30000);
	  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxFromAccount","fromAccount","segRecords[0,0]","flxDropdownRecordField"]);
	  appLog("Successfully Selected From Account from List");
	}
	
	async function SelectUTFToAccount(ToAccReciptent){
	
	  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","flxTextBox"],30000);
	  kony.automation.widget.touch([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","flxTextBox"], [241,25],null,null);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","flxTextBox","txtBox"],30000);
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","flxTextBox","txtBox"],ToAccReciptent);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","segRecords"],30000);
	  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxToAccount","toAccount","segRecords[0,0]","flxDropdownRecordField"]);
	  appLog("Successfully Selected To Account from List");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	
	}
	
	async function Enter_CCY_AmountValue(amountValue){
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxTransferCCYDropdown"],15000);
	  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxTransferCCYDropdown"]);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","segTransferCCYList"],15000);
	  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segTransferCCYList[1]","flxDropdownRecord"]);
	  appLog("Successfully Selected Currency Type");
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","txtBoxTransferAmount"],15000);
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","txtBoxTransferAmount"],amountValue);
	  appLog("Successfully Selected Amount Value");
	
	}
	
	async function SelectUTFFrequency(freqValue) {
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxFrequencyDropdown"],15000);
	  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxFrequencyDropdown"]);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList"],15000);
	  switch (freqValue) {
	    case "Once":
	      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[0]","flxDropdownRecord"]);
	      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	      break;
	    case "Daily":
	      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[1]","flxDropdownRecord"]);
	      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	      break;
	    case "Weekly":
	      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[2]","flxDropdownRecord"]);
	      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	      break;
	    case "Bi-weekly":
	      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[3]","flxDropdownRecord"]);
	      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	      break;
	    case "Monthly":
	      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[4]","flxDropdownRecord"]);
	      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	      break;
	    case "Qtrly":
	      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[5]","flxDropdownRecord"]);
	      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	      break;
	    case "HalfYearly":
	      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[6]","flxDropdownRecord"]);
	      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	      break;
	    case "Yearly":
	      kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segFrequencyList[7]","flxDropdownRecord"]);
	      appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	      break;
	  }
	}
	
	async function SelectUTFDateRange() {
	
	  //var today = new Date();
	  //kony.automation.calendar.selectDate(["frmConsolidatedStatements","calFromDate"], [(today.getDate()-2),(today.getMonth()+1),today.getFullYear()]);
	  kony.automation.calendar.selectDate([kony.automation.getCurrentForm(),"unifiedTransfers","calStartDate"], [4,25,2021]);
	  kony.automation.calendar.selectDate([kony.automation.getCurrentForm(),"unifiedTransfers","calEndDate"], [9,25,2021]);
	  appLog("Successfully Selected DateRange");
	}
	
	async function SelectUTFSendOnDate() {
	
	  kony.automation.calendar.selectDate([kony.automation.getCurrentForm(),"unifiedTransfers","calStartDate"], [4,25,2021]);
	  appLog("Successfully Selected SendOn Date");
	}
	
	async function SelectUTFOccurences(){
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","flxTransferDurationTypeDropdown"],15000);
	  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","flxTransferDurationTypeDropdown"]);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","segTransferDurationList"],15000);
	  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"unifiedTransfers","segTransferDurationList[1]","flxDropdownRecord"]);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","txtBoxRecurrences"],15000);
	  kony.automation.textbox.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","txtBoxRecurrences"],"2");
	  appLog("Successfully Selected Occurences");
	}
	
	async function EnterUTFNoteValue(notes){
	
	  kony.automation.textarea.enterText([kony.automation.getCurrentForm(),"unifiedTransfers","txtAreaNotes"],notes);
	  appLog("Successfully entered note value : "+notes);
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","btnContinue"],15000);
	  var isEnable=kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"unifiedTransfers","btnContinue"],"enable");
	  
	  if(isEnable){
	    await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"unifiedTransfers","btnContinue"]);
	    kony.automation.button.click([kony.automation.getCurrentForm(),"unifiedTransfers","btnContinue"]);
	    appLog("Successfully Clicked on CONTINUE button");
	    await kony.automation.playback.wait(5000);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(30000);
	    var Status=false;
	    if(await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000)){
	      Status=true;
	      appLog('Custom Message : Successfully moved to frmConfirmEuro Screen');
	    }else if(await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"unifiedTransfers","txtErrormessage"],5000)){
	      Status=false;
	      appLog('Custom Message : Failed with rtxMakeTransferError');
	      fail('Custom Message :'+kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"unifiedTransfers","txtErrormessage"],"text"));
	    }else{
	      appLog('Custom Message : Failed to Navigate to frmConfirmEuro Screen');
	    }
	    expect(Status).toBe(true,"Failed to Navigate to Confirmation Sccreen");
	  }else{
	    appLog("CONTINUE button is not enabled");
	  }
	}     
	
	async function clickonUTFConfirmBtn(){
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"],15000);
	  await kony.automation.scrollToWidget([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"]);
	  kony.automation.button.click([kony.automation.getCurrentForm(),"confirmTransfer","btnAction3"]);
	  appLog("Successfully Clicked on CONFIRM button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(30000);
	}
	
	async function clickonCancelbutton(){
	
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","btnAction1"],15000);
	  kony.automation.button.click([kony.automation.getCurrentForm(),"confirmTransfer","btnAction1"]);
	  appLog("Successfully Clicked on CANCEL button");
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"btnYes"],15000);
	  kony.automation.button.click([kony.automation.getCurrentForm(),"btnYes"]);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  appLog("Successfully Clicked on YES button");
	}
	
	async function VerifyCurrencyField_Domestic(){
	
	  appLog("Intiated method to verify currency field- Domestic");
	  var currency=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","lblValue8"],15000);
	  expect(currency).toBe(true,"Failed to verify currency field");
	}
	async function VerifyCurrencyField_International(){
	
	  appLog("Intiated method to verify currency field - International");
	  var currency=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","lblValue6"],15000);
	  expect(currency).toBe(true,"Failed to verify currency field");
	}
	
	async function VerifyCurrencyField_SameBank(){
	
	  appLog("Intiated method to verify currency field - Samebank");
	  var currency=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","lblValue4"],15000);
	  expect(currency).toBe(true,"Failed to verify currency field");
	}
	
	async function VerifyCurrencyField_PayAPerson(){
	
	  appLog("Intiated method to verify currency field - PayAPerson");
	  var currency=await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"confirmTransfer","lblValue6"],15000);
	  expect(currency).toBe(true,"Failed to verify currency field");
	}
	
	async function VerifyUTFTransferSuccessMsg(){
	
	  var SuccessfrmName=kony.automation.getCurrentForm();
	  appLog("Intiated method to Verify Transfer SuccessMessage "+SuccessfrmName);
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"TransferAcknowledgement","lblSection1Message"],15000);
	  expect(kony.automation.widget.getWidgetProperty([kony.automation.getCurrentForm(),"TransferAcknowledgement","lblSection1Message"], "text")).not.toBe("");
	  await kony.automation.playback.waitFor([kony.automation.getCurrentForm(),"customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click([kony.automation.getCurrentForm(),"customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(30000);
	  appLog("Successfully moved back to Accounts DashBoard");
	}
	
	
	async function PreLogin_NavigateToFAQ(){
	
	  appLog("Intiated method to Navigate to About US");
	
	  await kony.automation.playback.waitFor(["frmLogin","btnFaqs"],10000);
	  kony.automation.button.click(["frmLogin","btnFaqs"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to About US");
	}
	async function MoveBacktoLogin_FAQ(){
	
	  await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmOnlineHelp","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	
	async function PreLogin_NavigateToPrivacyPolicy(){
	
	  appLog("Intiated method to Navigate to PrivacyPolicy");
	
	  await kony.automation.playback.waitFor(["frmLogin","btnPrivacy"],10000);
	  kony.automation.button.click(["frmLogin","btnPrivacy"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to PrivacyPolicy");
	}
	async function MoveBacktoLogin_PrivacyPolicyScreen(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	async function PreLogin_NavigateToTermsConditions(){
	
	  appLog("Intiated method to Navigate to TC's");
	
	  await kony.automation.playback.waitFor(["frmLogin","btnTermsAndConditions"],10000);
	  kony.automation.button.click(["frmLogin","btnTermsAndConditions"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to TC's");
	}
	async function MoveBacktoLogin_TermsConditions(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	async function PreLogin_NavigateToContactUs(){
	
	  appLog("Intiated method to Navigate to ContactUs");
	
	  await kony.automation.playback.waitFor(["frmLogin","btnContactUs"],10000);
	  kony.automation.button.click(["frmLogin","btnContactUs"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to Contact US");
	}
	async function MoveBacktoLogin_ContactUsScreen(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	
	async function ClickonLanguageDropdown(){
	
	  appLog("Intiated method to click on Language change dropwdown");
	  await kony.automation.playback.waitFor(["frmLogin","flxDropdown"],10000);
	  kony.automation.flexcontainer.click(["frmLogin","flxDropdown"]);
	  appLog("Successfully clicked on Dropdown");
	
	}
	
	async function selectLanguage(language){
	
	  appLog("Intiated method to select Language from dropwdown");
	  await kony.automation.playback.waitFor(["frmLogin","segLanguagesList"],30000);
	  switch (language) {
	    case "US":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[0]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	    case "UK":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[1]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	    case "Spanish":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[2]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	    case "German":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[3]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	    case "French":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[4]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	  }
	  
	}
	
	async function ClickonYesbutton(){
	
	  appLog("Intiated method to click on YES button");
	  await kony.automation.playback.waitFor(["frmLogin","CustomChangeLanguagePopup","btnYes"]);
	  kony.automation.button.click(["frmLogin","CustomChangeLanguagePopup","btnYes"]);
	  appLog("Successfully clicked on YES button");
	  await kony.automation.playback.wait(5000);
	  // intermetient fix where after YES button not agaon selecting dropdown- Changing Focus
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"]);
	  kony.automation.widget.touch(["frmLogin","loginComponent","tbxUserName"], [123,16],null,null);
	  //kony.automation.flexcontainer.click(["frmLogin","loginComponent","segUsers[0]","flxUserNames"]);
	  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxUserName"],"ChangeLanguage");
	}
	
	
	
	async function navigateToSavingPot(){
	
	  appLog("Intiated method to Navigate SavingPot");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","quicklinks","flxRow1"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","quicklinks","flxRow1"]);
	  await kony.automation.playback.wait(10000);
	  var Status=await kony.automation.playback.waitFor(["frmSavingsPotLanding","lblMySavingsPot"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmSavingsPotLanding","lblMySavingsPot"], "text")).toEqual("My Savings Pot");
	  expect(Status).toBe(true,"Failed to navigate to My Savings Pot");
	
	}
	
	async function clickOnCreateNewSavingPot(){
	
	  appLog("Intiated method to click on new Savings Pot link");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","flxAction"],15000);
	  kony.automation.flexcontainer.click(["frmSavingsPotLanding","flxAction"]);
	  await kony.automation.playback.wait(5000);
	  var Status=await kony.automation.playback.waitFor(["frmCreateSavingsPot","lblTransfers"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmCreateSavingsPot","lblTransfers"], "text")).toEqual("New Savings Pot");
	  expect(Status).toBe(true,"Failed to click on New Savings Pot Link");
	
	}
	
	async function CreateNewGoal(){
	
	  appLog("Intiated method to create new Goal");
	  await kony.automation.playback.waitFor(["frmCreateSavingsPot","btnCreateGoal"],15000);
	  kony.automation.button.click(["frmCreateSavingsPot","btnCreateGoal"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on new Goal button");
	  await kony.automation.playback.waitFor(["frmCreateSavingsGoal","tbxGoalName"],15000);
	  kony.automation.textbox.enterText(["frmCreateSavingsGoal","tbxGoalName"],"My Goal"+getRandomString(5));
	  appLog("Successfully Entered new Goal name");
	  await kony.automation.playback.waitFor(["frmCreateSavingsGoal","tbxGoalAmount"],15000);
	  kony.automation.textbox.enterText(["frmCreateSavingsGoal","tbxGoalAmount"],"500");
	  appLog("Successfully Entered new Goal amount");
	  kony.automation.slider.slide(["frmCreateSavingsGoal","slider1","sldSlider"], 50);
	  await kony.automation.playback.waitFor(["frmCreateSavingsGoal","btnContinue"],15000);
	  kony.automation.button.click(["frmCreateSavingsGoal","btnContinue"]);
	  appLog("Successfully clicked on continue button");
	  await kony.automation.playback.waitFor(["frmCreateGoalConfirm","lblTransfers"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCreateGoalConfirm","lblTransfers"], "text")).toContain("Confirmation");
	  await kony.automation.playback.waitFor(["frmCreateGoalConfirm","confirmation","btnConfirm"],15000);
	  await kony.automation.scrollToWidget(["frmCreateGoalConfirm","confirmation","btnConfirm"]);
	  kony.automation.button.click(["frmCreateGoalConfirm","confirmation","btnConfirm"]);
	  appLog("Successfully clicked on Confirm button");
	  await kony.automation.playback.waitFor(["frmCreateGoalAck","acknowledgment","lblSuccessMessage"],30000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmCreateGoalAck","acknowledgment","lblSuccessMessage"], "text")).toEqual("Success");
	  await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	  kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	}
	
	async function EditMyGoal(){
	
	  appLog("Intiated method to Edit Goal");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction2"],15000);
	  kony.automation.button.click(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction2"]);
	  await kony.automation.playback.waitFor(["frmEditGoal","tbxGoalName"],15000);
	  kony.automation.textbox.enterText(["frmEditGoal","tbxGoalName"],"Update Goal"+getRandomString(5));
	  appLog("Succesfully Updated Goal name");
	  await kony.automation.playback.waitFor(["frmEditGoal","btnContinue"],15000);
	  kony.automation.button.click(["frmEditGoal","btnContinue"]);
	  appLog("Successfully clicked on continue button");
	  await kony.automation.playback.waitFor(["frmCreateGoalConfirm","lblTransfers"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCreateGoalConfirm","lblTransfers"], "text")).toContain("Edit");
	  await kony.automation.playback.waitFor(["frmCreateGoalConfirm","confirmation","btnConfirm"],15000);
	  await kony.automation.scrollToWidget(["frmCreateGoalConfirm","confirmation","btnConfirm"]);
	  kony.automation.button.click(["frmCreateGoalConfirm","confirmation","btnConfirm"]);
	  appLog("Successfully clicked on Confirm button");
	  await kony.automation.playback.waitFor(["frmCreateGoalAck","acknowledgment","lblSuccessMessage"],30000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmCreateGoalAck","acknowledgment","lblSuccessMessage"], "text")).toEqual("Success");
	  await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	  kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	  await kony.automation.playback.wait(5000);
	}
	
	async function closeMyGoal(){
	
	  appLog("Intiated method to Close Goal");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction4"],15000);
	  kony.automation.button.click(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction4"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on CLOSE button");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","deletePopup","btnYes"],15000);
	  kony.automation.button.click(["frmSavingsPotLanding","deletePopup","btnYes"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on YES button");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","lblGoalClosedSuccess"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmSavingsPotLanding","lblGoalClosedSuccess"], "text")).toContain("closed");
	  appLog("Successfully Closed My Goal");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	  kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	  await kony.automation.playback.wait(5000);
	}
	
	async function CreateNewBudget(){
	
	  appLog("Intiated method to create new Budget");
	  await kony.automation.playback.waitFor(["frmCreateSavingsPot","btnCreateBudget"]);
	  kony.automation.button.click(["frmCreateSavingsPot","btnCreateBudget"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on new Budget button");
	  //await kony.automation.playback.waitFor(["frmCreateBudget","lblContentHeader"]);
	  //expect(kony.automation.widget.getWidgetProperty(["frmCreateBudget","lblContentHeader"], "text")).toContain("Create");
	  await kony.automation.playback.waitFor(["frmCreateBudget","tbxbudgetName"]);
	  kony.automation.textbox.enterText(["frmCreateBudget","tbxbudgetName"],"My Budget"+getRandomString(5));
	  appLog("Successfully Entered Budget name");
	  await kony.automation.playback.waitFor(["frmCreateBudget","tbxAmount"]);
	  kony.automation.textbox.enterText(["frmCreateBudget","tbxAmount"],"500");
	  appLog("Successfully Entered Budget Amount");
	  await kony.automation.playback.waitFor(["frmCreateBudget","btnContinue"]);
	  kony.automation.button.click(["frmCreateBudget","btnContinue"]);
	  appLog("Successfully Clicked on Continue Button");
	  await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","lblTransfers"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmCreateBudgetConfirm","lblTransfers"], "text")).toContain("Confirmation");
	  await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","confirmation","btnConfirm"]);
	  kony.automation.button.click(["frmCreateBudgetConfirm","confirmation","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	  await kony.automation.playback.waitFor(["frmCreateBudgetAck","acknowledgment","lblSuccessMessage"]);
	  //expect(kony.automation.widget.getWidgetProperty(["frmCreateBudgetAck","acknowledgment","lblSuccessMessage"], "text")).toContain("Success");
	  await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	  kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	  await kony.automation.playback.wait(5000);
	}
	
	async function EditMyBudget(){
	
	  appLog("Intiated method to Edit Budget");
	
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction2"]);
	  kony.automation.button.click(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction2"]);
	  appLog("Successfully clicked on edit button");
	  await kony.automation.playback.waitFor(["frmEditBudget","tbxbudgetName"]);
	  kony.automation.textbox.enterText(["frmEditBudget","tbxbudgetName"],"Updated My Budget"+getRandomString(5));
	  appLog("Successfully Updated budget name");
	  await kony.automation.playback.waitFor(["frmEditBudget","btnContinue"]);
	  kony.automation.button.click(["frmEditBudget","btnContinue"]);
	  appLog("Successfully clicked on continue button");
	  await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","lblTransfers"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmCreateBudgetConfirm","lblTransfers"], "text")).toContain("Edit");
	  await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","confirmation","btnConfirm"]);
	  kony.automation.button.click(["frmCreateBudgetConfirm","confirmation","btnConfirm"]);
	  appLog("Successfully clicked on Confirm button");
	  await kony.automation.playback.waitFor(["frmCreateBudgetAck","acknowledgment","lblSuccessMessage"]);
	  //expect(kony.automation.widget.getWidgetProperty(["frmCreateBudgetAck","acknowledgment","lblSuccessMessage"], "text")).toContain("Success");
	  await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	  kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	
	}
	
	async function closeMyBudget(){
	
	  appLog("Intiated method to Close Budget");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction4"]);
	  kony.automation.button.click(["frmSavingsPotLanding","goalsAndBudgets","row01btnAction4"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on CLOSE button");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","deletePopup","btnYes"]);
	  kony.automation.button.click(["frmSavingsPotLanding","deletePopup","btnYes"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on YES button");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","lblGoalClosedSuccess"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmSavingsPotLanding","lblGoalClosedSuccess"], "text")).toContain("closed");
	  appLog("Successfully Closed My Budget");
	  await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	  kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	  await kony.automation.playback.wait(5000);
	}
	
	async function navigateToDisputeTranscation(){
	
	  appLog("Intiated method to navigate to DisputeTranscation");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS5flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS5flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitFor(["frmDisputedTransactionsList","lblDisputeTransactions"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDisputedTransactionsList","lblDisputeTransactions"], "text")).toContain("Disputed Transactions");
	  appLog("Successfully navigate to DisputeTranscation");
	}
	
	async function DisputeTranscation_SendMsg(){
	
	  var isDispute=await kony.automation.playback.waitFor(["frmDisputedTransactionsList","segDisputeTransactions"],15000);
	  if(isDispute){
	    kony.automation.flexcontainer.click(["frmDisputedTransactionsList","segDisputeTransactions[0]","flxDropdown"]);
	    await kony.automation.playback.waitFor(["frmDisputedTransactionsList","segDisputeTransactions"]);
	    kony.automation.button.click(["frmDisputedTransactionsList","segDisputeTransactions[0]","btnViewMessage"]);
	    await kony.automation.playback.wait(10000);
	    await EnterMessageDetails();
	    await MoveBackToDashBoard_Messages();
	  }else{
	    appLog("No Dispute Transcations List");
	    await MoveBackfrom_DisputeTranscation();
	  }
	
	}
	
	async function MoveBackfrom_DisputeTranscation(){
	
	  await kony.automation.playback.waitFor(["frmDisputedTransactionsList","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDisputedTransactionsList","customheadernew","flxAccounts"]);
	}
	
	
	async function navigateToAlerts_Notifications(){
	
	  appLog("Intiated method to navigate to alerts and notifications");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGESflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGESflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGES0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGES0flxMyAccounts"]);
	  //await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  appLog("Successfully navigated to alerts and notifications");
	}
	
	async function SearchAlertMessage(){
	
	  appLog("Intiated method to view alerts and notifications");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","txtSearch"],"Sign");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","lblSearch"],15000);
	  kony.automation.widget.touch(["frmNotificationsAndMessages","NotficationsAndMessages","lblSearch"], null,null,[3,15]);
	  //await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification[0]","flxNotificationsAndMessages"]);
	  appLog("Successfully Verified alerts and notifications");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	}
	
	async function DeleteAlertMessage(){
	
	  appLog("Intiated method to DISMISS alerts and notifications");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification[0]","flxNotificationsAndMessages"]);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnDismiss"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnDismiss"]);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","CustomPopup1","btnYes"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","CustomPopup1","btnYes"]);
	  //await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  appLog("Successfully DISMISS alerts and notifications");
	  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	}
	
	async function VerifyAlertsCount(){
	  
		await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnNotifications"]);
		var AlertsText=kony.automation.widget.getWidgetProperty(["frmNotificationsAndMessages","NotficationsAndMessages","btnNotifications"], "text");
	    //parseInt(subaccounts_Size.substring(1, 2));
	    return AlertsText;
	}
	
	async function VerifyAlertCountIncrementDecrement(){
	  
	  var Alert_before=await VerifyAlertsCount();
	  appLog("Alerts count before is : "+Alert_before);
	  await DeleteAlertMessage();
	  await navigateToAlerts_Notifications();
	  var Alert_After=await VerifyAlertsCount();
	  appLog("Alerts count ater is : "+Alert_After);
	  expect(Alert_before).not.toBe(Alert_After);
	  
	}
	
	async function VerifyNotificationDetail(){
	  
		await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","lblHeadingNotification"]);
		var details=kony.automation.widget.getWidgetProperty(["frmNotificationsAndMessages","NotficationsAndMessages","lblHeadingNotification"], "text");
	    return details;
	}
	
	it("DbxLogin",async function() {
	
	  //var userName='testuat52';
	  var userName=LoginDetails.username;
	  //var userName='adithyasai.kovuru';
	  var passWord=LoginDetails.password;
	
	   // Check for MayBeLater
	  await verifyMayBeLater();
	  
	  await verifyLoginFunctionality(userName,passWord);
	  
	  //Check for terms and conditions
	  await verifyTermsandConditions();
	  
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	
	it("CheckingAcc_Navigate_Transfer", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Current.MenuOptions[0].option);
	  await VerifyTransfersScreen();
	  await MoveBackToLandingScreen_Transfers();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("CheckingAcc_Navigate_StopCheque", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Current.MenuOptions[6].option);
	  await VerifyStopChequePaymentScreen();
	  await MoveBack_Accounts_StopChequeBook();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("CheckingAcc_Navigate_Statement", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Current.MenuOptions[2].option);
	  await verifyVivewStatementsHeader();
	  await MoveBackToLandingScreen_AccDetails();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("CheckingAcc_Navigate_Payment", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Current.MenuOptions[1].option);
	  await VerifyPaymentsScreen();
	  await MoveBackToLandingScreen_Transfers();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("CheckingAcc_Navigate_AccountAlerts", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Current.MenuOptions[9].option);
	  await VerifyAccountAlertScreen();
	  await MoveBack_Accounts_ProfileManagement();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("SavingAcc_Navigate_Transfer", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[0].option);
	  await VerifyTransfersScreen();
	  await MoveBackToLandingScreen_Transfers();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("SavingAcc_Navigate_StopCheque", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[4].option);
	  await VerifyStopChequePaymentScreen();
	  await MoveBack_Accounts_StopChequeBook();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("SavingAcc_Navigate_Statement", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[2].option);
	  await verifyVivewStatementsHeader();
	  await MoveBackToLandingScreen_AccDetails();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("SavingAcc_Navigate_Payment", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[1].option);
	  await VerifyPaymentsScreen();
	  await MoveBackToLandingScreen_Transfers();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("SavingAcc_Navigate_AccountAlerts", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[7].option);
	  await VerifyAccountAlertScreen();
	  await MoveBack_Accounts_ProfileManagement();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("CreateNewTravelPlan", async function() {
	  
	  var Destination="GOA";
	  var PhoneNumber="1234567890";
	  var NoteValue="My Summer Travel Plan";
	  
	  await navigateToCardManegement();
	  await CreateNewTravelPlan(Destination,PhoneNumber,NoteValue);
	  
	},120000);
	
	it("EditTravelPlan", async function() {
	
	  var UpdatedPhoneNumber="0987654321";
	  
	  await navigateToCardManegement();
	  await EditTravelPlan(UpdatedPhoneNumber);
	  
	},120000);
	
	it("DeleteTravelPlan", async function() {
	  
	  await navigateToCardManegement();
	  await DeleteTravelPlan();
	  
	},120000);
});