describe("T24_CardManagement", function() {
	beforeEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  strLogger=[];
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside before Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	},480000);
	
	afterEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside after Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	  //strLogger=null;
	
	},480000);
	
	async function verifyAccountsLandingScreen(){
	
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
	}
	
	async function SelectAccountsOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
	        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(10000);
	        
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnFirstCheckingAccount(){
	
	  appLog("Intiated method to click on First Checking account");
	  SelectAccountsOnDashBoard("Checking");
	  //appLog("Successfully Clicked on First Checking account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstSavingsAccount(){
	
	  appLog("Intiated method to click on First Savings account");
	  SelectAccountsOnDashBoard("Saving");
	  //appLog("Successfully Clicked on First Savings account");
	  //await kony.automation.playback.wait(5000);
	}
	
	
	async function clickOnFirstCreditCardAccount(){
	
	  appLog("Intiated method to click on First CreditCard account");
	  SelectAccountsOnDashBoard("Credit");
	  //appLog("Successfully Clicked on First CreditCard account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstDepositAccount(){
	
	  appLog("Intiated method to click on First Deposit account");
	  SelectAccountsOnDashBoard("Deposit");
	  //appLog("Successfully Clicked on First Deposit account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstLoanAccount(){
	
	  appLog("Intiated method to click on First Loan account");
	  SelectAccountsOnDashBoard("Loan");
	  //appLog("Successfully Clicked on First Loan account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstAvailableAccount(){
	
	  appLog("Intiated method to click on First available account on DashBoard");
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	  kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	  appLog("Successfully Clicked on First available account on DashBoard");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickOnSearch_AccountDetails(){
	
	  appLog("Intiated method to click on Search Glass Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Seach Glass Icon");
	}
	
	async function selectTranscationtype(TransactionType){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], TransactionType);
	  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
	}
	
	async function enterKeywordtoSearch(keyword){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtKeyword"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtKeyword"],keyword);
	  appLog("Successfully entered keyword for search : <b>"+keyword+"</b>");
	}
	
	async function selectAmountRange(AmountRange1,AmountRange2){
	
	  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
	  appLog("Successfully selected amount range From");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
	  appLog("Successfully selected amount range To");
	
	  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");
	}
	
	async function selectCustomdate(){
	
	  appLog("Intiated method to select Custom Date Range");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
	  appLog("Successfully selected Date Range");
	}
	
	async function selectTimePeriod(){
	
	  appLog("Intiated method to select custom timeperiod");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	  appLog("Successfully selected Time period");
	}
	
	async function clickOnAdvancedSearchBtn(){
	
	  appLog("Intiated method to click on Search Button with given search criteria");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function validateSearchResult() {
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	}
	
	async function scrolltoTranscations_accountDetails(){
	
	  appLog("Intiated method to scroll to Transcations under account details");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	}
	
	async function VerifyAdvancedSearch_byAmount(AmountRange1,AmountRange2){
	
	  await clickOnSearch_AccountDetails();
	  await selectAmountRange(AmountRange1,AmountRange2);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byTranxType(TransactionType){
	
	  await clickOnSearch_AccountDetails();
	  await selectTranscationtype(TransactionType);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byDate(){
	
	  await clickOnSearch_AccountDetails();
	  await selectTimePeriod();
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function VerifyAdvancedSearch_byKeyword(keyword){
	
	  await clickOnSearch_AccountDetails();
	  await enterKeywordtoSearch(keyword);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function MoveBackToLandingScreen_AccDetails(){
	
	  appLog("Intiated method to Move back to Account Dashboard from AccountsDetails");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	async function VerifyAccountOnDashBoard(AccountType){
	
	  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        appLog("Successfully verified : <b>"+accountName+"</b>");
	        myList.push("TRUE");
	        finished = true;
	        break;
	      }else{
	        myList.push("FALSE");
	      }
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	
	async function VerifyCheckingAccountonDashBoard(){
	
	  appLog("Intiated method to verify Checking account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
	  VerifyAccountOnDashBoard("Checking");
	}
	
	async function VerifySavingsAccountonDashBoard(){
	
	  appLog("Intiated method to verify Savings account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
	  VerifyAccountOnDashBoard("Saving");
	}
	async function VerifyCreditCardAccountonDashBoard(){
	
	  appLog("Intiated method to verify CreditCard account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
	  VerifyAccountOnDashBoard("Credit");
	}
	
	async function VerifyDepositAccountonDashBoard(){
	
	  appLog("Intiated method to verify Deposit account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
	  VerifyAccountOnDashBoard("Deposit");
	}
	
	async function VerifyLoanAccountonDashBoard(){
	
	  appLog("Intiated method to verify Loan account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
	  VerifyAccountOnDashBoard("Loan");
	}
	
	async function verifyViewAllTranscation(){
	
	  appLog("Intiated method to view all Tranx in AccountDetails");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	}
	
	
	async function verifyContextMenuOptions(myList_Expected){
	
	  //var myList_Expected = new Array();
	  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	  myList_Expected.push(myList_Expected);
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  var myList = new Array();
	
	  for(var x = 0; x <segLength-1; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+options);
	    myList.push(options);
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	
	  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	  //appLog("isFounded"+isFounded);
	  expect(isFounded).toBe(true);
	}
	
	async function MoveBackToLandingScreen_Accounts(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	}
	
	
	async function verifyAccountSummary_CheckingAccounts(){
	
	  appLog("Intiated method to verify account summary for Checking Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_DepositAccounts(){
	
	  appLog("Intiated method to verify account summary for Deposit Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_CreditCardAccounts(){
	
	  appLog("Intiated method to verify account summary for CreditCard Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	
	}
	
	async function verifyAccountSummary_LoanAccounts(){
	
	  appLog("Intiated method to verify account summary for Loan Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_SavingsAccounts(){
	
	  appLog("Intiated method to verify account summary for Savings Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function VerifyPendingWithdrawls_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab1"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab1"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"], "text")).not.toBe("");
	
	}
	
	async function VerifyInterestdetails_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab2"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab2"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab2"], "text")).not.toBe("");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl7Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl7Tab2"], "text")).not.toBe("");
	
	}
	
	async function VerifySwiftCode_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab3"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab3"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab3"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab3"], "text")).not.toBe("");
	}
	
	async function selectContextMenuOption(Option){
	
	  appLog("Intiated method to select context menu option :: "+Option);
	
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	  var segLength=accounts_Size.length;
	  appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment will be :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg],15000);
	    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    appLog("Menu Item Text is :: "+TransfersText);
	    if(TransfersText===Option){
	      appLog("Option to be selected is :"+TransfersText);
	       await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes"],15000);
	      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
	      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
	      await kony.automation.playback.wait(10000);
	      myList.push("TRUE");
	      break;
	    }else{
	      myList.push("FALSE");
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	
	  expect(Status).toBe(true,"Failed to click on option <b>"+Option+"</b>");
	}
	
	// async function SelectContextualOnDashBoard(AccountType){
	
	//   appLog("Intiated method to analyze accounts data Dashboard");
	
	//   var Status=false;
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	//   var segLength=accounts_Size.length;
	
	//   var finished = false;
	//   for(var x = 0; x <segLength && !finished; x++) {
	
	//     var segHeaders="segAccounts["+x+",-1]";
	
	//     var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	//     var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	//     //appLog('Sub accounts size is '+subaccounts_Length);
	
	//     for(var y = 0; y <subaccounts_Length; y++){
	
	//       var seg="segAccounts["+x+","+y+"]";
	
	//       var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	//       var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	//       if(typeOfAccount.includes(AccountType)){
	//         await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
	//         kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	//         appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	//         await kony.automation.playback.wait(5000);
	//         finished = true;
	//         Status=true;
	//         break;
	//       }
	//     }
	//   }
	
	//   expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
	// }
	
	async function SelectContextualOnDashBoard(AccountNumber){
	
	  appLog("Intiated method to select Menu of account : "+AccountNumber);
	  await kony.automation.playback.wait(5000);
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      appLog("Account Name is : "+accountName);
	      if(accountName.includes(AccountNumber)){
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(5000);
	        // Validate really list is displayed or not
	        var menuList=await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	        expect(menuList).toBe(true,"Failed to display contextual menu list items");
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on Menu of AccNumber: <b>"+AccountNumber+"</b>");
	}
	
	async function verifyVivewStatementsHeader(){
	
	  appLog('Intiated method to verify account statement header');
	  var Status=await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
	  //kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");
	  expect(Status).toBe(true,"Failed to Navigate to Account Statements");
	  //appLog('Successfully Verified Account Statement Header');
	}
	
	async function VerifyPostedPendingTranscation(){
	
	  appLog('Intiated method to verify Pending and Posted Transcations');
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  var TransType=kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","segTransactions[0,-1]","lblTransactionHeader"], "text");
	  if(TransType.includes("Posted")||TransType.includes("Pending")){
	    //kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    //await kony.automation.playback.wait(5000);
	    appLog('Transfer Type is : '+TransType);
	  }else{
	    appLog('No Pending or Posted Transcation available');
	  }
	
	}
	
	
	async function VerifyStopChequePaymentScreen(){
	  
	  appLog("Intiated method to verify StopChequePayment Screen");
	  var Status=await kony.automation.playback.waitFor(["frmStopPayments","lblChequeBookRequests"],30000);
	  expect(Status).toBe(true,"Failed to Verify StopChequePayment screen");
	  appLog("Successfully Verified StopChequePayment");
	  
	}
	
	async function VerifyAccountAlertScreen(){
	  
	  appLog("Intiated method to verify AccountAlert Screen");
	  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAlertsHeading"],30000);
	  expect(Status).toBe(true,"Failed to Verify AccountAlert screen");
	  appLog("Successfully Verified AccountAlert");
	}
	
	async function MoveBack_Accounts_StopChequeBook(){
	  
	  appLog("Intiated method to Move back to Account Dashboard from StopPayments");
	  await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	
	}
	
	async function MoveBack_Accounts_ProfileManagement(){
	
	  // Move back to base state
	  appLog("Intiated method to Move back to Account Dashboard from ProfileManagement");
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	
	async function navigateToCardManegement(){
	
	  appLog("Intiated method to Navigate Card Management Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS4flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","lblMyCardsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","lblMyCardsHeader"], "text")).toEqual("My Cards");
	
	  appLog("Successfully Navigated to Card Management Screen");
	}
	async function VerifyNoCardsError(){
	
	  noCardsError=false;
	  appLog("Intiated method to verify Cards available or Not");
	  var noCardsError=await kony.automation.playback.waitFor(["frmCardManagement","myCards","lblNoCardsError"],30000);
	  //var noCardsError=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","lblNoCardsError"], "text");
	  appLog("no Cards Error :: "+noCardsError);
	  return noCardsError;
	}
	
	async function applyforNewCard(){
	
	  appLog("Intiated method to apply for new Card");
	
	  if(await VerifyNoCardsError()){
	    appLog("No cards avalable - applyNew");
	    await kony.automation.playback.waitFor(["frmCardManagement","myCards","btnApplyForCard"],15000);
	    kony.automation.button.click(["frmCardManagement","myCards","btnApplyForCard"]);
	    appLog("Successfully clicked on Apply Card button");
	  }else{
	    appLog("Cards are available already- Request new Card");
	    await kony.automation.playback.waitFor(["frmCardManagement","flxRequestANewCard"],15000);
	    kony.automation.flexcontainer.click(["frmCardManagement","flxRequestANewCard"]);
	    appLog("Successfully clicked on Request new Card button");
	  }
	
	}
	
	async function addNewCardDetails(){
	
	  appLog("Intiated method to add new card details");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","segAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","segAccounts[0]","flxAccountNameWrapper"]);
	  appLog("Successfully clicked on Flex accounts");
	  await kony.automation.playback.waitFor(["frmCardManagement","segCardProducts"],15000);
	  kony.automation.button.click(["frmCardManagement","segCardProducts[0]","btnSelect"]);
	  appLog("Successfully clicked on btnSelect");
	  await kony.automation.playback.waitFor(["frmCardManagement","tbxNameOnCard"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","tbxNameOnCard"],"JasmineCard"+getRandomString(5));
	  appLog("Successfully entered card name ");
	  await kony.automation.playback.waitFor(["frmCardManagement","tbxEnterCardPIN"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","tbxEnterCardPIN"],"5050");
	  appLog("Successfully entered card PIN ");
	  await kony.automation.playback.waitFor(["frmCardManagement","tbxConfirmCardPIN"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","tbxConfirmCardPIN"],"5050");
	  appLog("Successfully Re-entered card PIN ");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnNewCardContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnNewCardContinue"]);
	  appLog("Successfully clicked on CONTINUE button");
	
	  var Success=await kony.automation.playback.waitFor(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"],30000);
	  expect(Success).toEqual(true,"Failed to add new card details");
	
	  await MoveBackfrom_MyCards();
	}
	async function activateNewCard(){
	
	  appLog("Intiated method to activate new card");
	  var MyValues=await ClickOnCardOptions("Activate Card");
	  if(MyValues[0]===true){
	    await kony.automation.playback.waitFor(["frmCardManagement","tbxCVVNumber"],15000);
	    //appLog("Before converting is : "+MyValues[1]);
	    var myCVV=""+MyValues[1].toString();
	    appLog("Intiated method to enter CVV as : "+myCVV);
	    kony.automation.textbox.enterText(["frmCardManagement","tbxCVVNumber"],myCVV);
	    appLog("Successfully entered CVV as : "+myCVV);
	    await kony.automation.playback.waitFor(["frmCardManagement","btnContinue2"],15000);
	    kony.automation.button.click(["frmCardManagement","btnContinue2"]);
	    var Success=await kony.automation.playback.waitFor(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"],30000);
	    //expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"], "text")).toEqual("activated");
	    expect(Success).toEqual(true,"Failed to Activate new card");
	
	    await MoveBackfrom_MyCards();
	  }else{
	    appLog("all cards are activated alreay");
	    await MoveBackfrom_MyCards();
	  }
	}
	async function VerifyMaskedAccountNumber(){
	
	  appLog("Intiated method to verify Masked account number");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards[0]","rtxValue1"], "text")).toContain("XXXX");
	  appLog("Successfully Validated Masked account number");
	}
	
	async function MoveBackfrom_MyCards(){
	
	  appLog("Intiated method to moveback from CardManagement");
	  await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	  appLog("Successfully Moved to DashBoard Screen");
	
	}
	
	async function ClickOnCardOptions(CardOption){
	
	  // Possible Options were : Lock Card,Replace Card,Report Lost,Set Limits,Change PIN,Activate Card
	
	  appLog("Intiated method to click on Option is : <b>"+CardOption+"</b>");
	  var Status=false;
	  var maskedAccNumber="";
	  var cvvnumber="";
	  var Values = new Array();
	
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  var segData=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards"],"data");
	  var segLength=segData.length;
	  //appLog("Length is : "+segLength);
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	    for(var y = 1; y <=5; y++){
	      //appLog("x value is : "+x);
	      //appLog("y value is : "+y);
	      var Option=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards["+x+"]","btnAction"+y], "text");
	      if(CardOption===Option){
	        appLog("Successfully found option : <b>"+CardOption+"</b>");
	        // get account number before clicking on it
	        var accnumber=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards["+x+"]","rtxValue1"], "text");
	        kony.automation.button.click(["frmCardManagement","myCards","segMyCards["+x+"]","btnAction"+y]);
	        await kony.automation.playback.wait(5000);
	        appLog("Successfully clicked on Option : <b>"+CardOption+"</b>");
	        finished = true;
	        Status=true;
	        maskedAccNumber=accnumber;
	        // get these value before Break statement
	        cvvnumber=await getCVVNumber(maskedAccNumber);
	        Values[0]=Status;
	        Values[1]=cvvnumber;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to get Option from List : <b>"+CardOption+"</b>");
	  return Values;
	}
	
	async function getCVVNumber(maskedAccNumber){
	
	  //appLog("Intiated method to get required values from account number");
	  var CVV=maskedAccNumber.substring(16,19);
	  appLog("CVV is : "+CVV);
	  return CVV;
	}
	
	async function VerifylockCard(){
	
	  await ClickOnCardOptions("Lock Card");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"], "text")).not.toBe("");
	  appLog("Successfully verified LockCard Screen");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","CardActivation","flxCheckbox"],15000);
	  kony.automation.widget.touch(["frmCardManagement","CardLockVerificationStep","CardActivation","flxCheckbox"], null,null,[45,33]);
	  appLog("Successfully Clicked on CheckBox");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Confirm Button");
	
	  await VerifyCardTransactionMessage();
	}
	
	async function VerifyUnlockCard(){
	
	  await ClickOnCardOptions("Unlock Card");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","CardActivation","lblHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","CardActivation","lblHeader"], "text")).not.toBe("");
	  appLog("Successfully Verified Unlock Card form");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardActivation","CardActivation","flxCheckbox"],15000);
	  kony.automation.widget.touch(["frmCardManagement","CardActivation","CardActivation","flxCheckbox"], null,null,[45,33]);
	  appLog("Successfully Clicked on CheckBox");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardActivation","btnProceed"],15000);
	  kony.automation.button.click(["frmCardManagement","CardActivation","btnProceed"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Continue Button");
	
	  await VerifyCardTransactionMessage();
	}
	
	async function VerifyChangePIN(){
	
	  await ClickOnCardOptions("Change PIN");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"], "text")).not.toBe("");
	  appLog("Successfully Verified Change PIN Card form");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","tbxCurrentPIN"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxCurrentPIN"],"5050");
	  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxNewPIN"],"5050");
	  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxConfirmPIN"],"5050");
	  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxNote"],"Change PIN");
	  appLog("Successfully Entered Change PIN details");
	  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await VerifyCardTransactionMessage();
	}
	
	async function setDailyWithDrawLimit(){
	
	  await ClickOnCardOptions("Set Limits");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","lblSetCardLimitsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblSetCardLimitsHeader"], "text")).toEqual("Set Card Limits");
	  appLog("Successfully Verified SetLimits form");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnEditLimitWithdrawal"],15000);
	  kony.automation.button.click(["frmCardManagement","btnEditLimitWithdrawal"]);
	  appLog("Successfully Clicked on EditLimitWithdrawal Button");
	  await kony.automation.playback.waitFor(["frmCardManagement","limitWithdrawalSlider"],15000);
	  kony.automation.slider.slide(["frmCardManagement","limitWithdrawalSlider"], 12450);
	  await kony.automation.playback.waitFor(["frmCardManagement","confirmButtons","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","confirmButtons","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	  await VerifyCardTransactionMessage();
	
	
	}
	
	async function setDailyPurchaseLimit(){
	
	  await ClickOnCardOptions("Set Limits");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","lblSetCardLimitsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblSetCardLimitsHeader"], "text")).toEqual("Set Card Limits");
	  appLog("Successfully Verified SetLimits form");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnEditLimitPurchase"],15000);
	  kony.automation.button.click(["frmCardManagement","btnEditLimitPurchase"]);
	  appLog("Successfully Clicked on EditLimitPurchase Button");
	  kony.automation.slider.slide(["frmCardManagement","limitPurchaseSlider"], 7250);
	  kony.automation.button.click(["frmCardManagement","confirmButtons","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	  await VerifyCardTransactionMessage();
	
	}
	
	async function VerifyCardTransactionMessage(){
	
	  var Success=await kony.automation.playback.waitFor(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"],30000);
	  if(Success){
	    appLog("Intiated method to Verify Transfer SuccessMessage");
	    expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"], "text")).not.toBe("");
	    await MoveBackfrom_MyCards();
	  }else if(await kony.automation.playback.waitFor(["frmCardManagement","rtxDowntimeWarning"],5000)){
	    appLog("Failed with : rtxDowntimeWarning");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmCardManagement","rtxDowntimeWarning"], "text"));
	    await MoveBackfrom_MyCards();
	  }else {
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	async function NavigateToManageTravelPlan(){
	
	  appLog("Intiated method to navigate to ManageTravelPlan form");
	  await kony.automation.playback.waitFor(["frmCardManagement","flxMangeTravelPlans"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","flxMangeTravelPlans"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","lblMyCardsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","lblMyCardsHeader"], "text")).not.toBe("");
	  appLog("Successfully Verified TravelPlan form");
	}
	
	async function CreateNewTravelPlan(Destination,PhoneNumber,NoteValue){
	
	  await NavigateToManageTravelPlan();
	
	  appLog("Intiated method to create new Travel Plan");
	  await kony.automation.playback.waitFor(["frmCardManagement","flxMangeTravelPlans"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","flxMangeTravelPlans"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","lblMyCardsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblMyCardsHeader"], "text")).toContain("Travel Plan");
	  appLog("Successfully Verified New TravelPlan form");
	
	  await kony.automation.playback.waitFor(["frmCardManagement","calFrom"],15000);
	  kony.automation.calendar.selectDate(["frmCardManagement","calFrom"], [5,25,2021]);
	  await kony.automation.playback.waitFor(["frmCardManagement","calTo"],15000);
	  kony.automation.calendar.selectDate(["frmCardManagement","calTo"], [5,30,2021]);
	  appLog("Successfully Entered Travel Plan Calender");
	  await kony.automation.playback.waitFor(["frmCardManagement","txtDestination"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","txtDestination"],Destination);
	  await kony.automation.playback.waitFor(["frmCardManagement","segDestinationList"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","segDestinationList[1]","flxCustomListBox"]);
	  kony.automation.flexcontainer.click(["frmCardManagement","flxAddFeatureRequestandimg"]);
	  appLog("Successfully Added Destination");
	  await kony.automation.playback.waitFor(["frmCardManagement","txtPhoneNumber"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","txtPhoneNumber"],PhoneNumber);
	  appLog("Successfully entered Mobile number");
	  await kony.automation.playback.waitFor(["frmCardManagement","txtareaUserComments"],15000);
	  kony.automation.textarea.enterText(["frmCardManagement","txtareaUserComments"],NoteValue);
	  appLog("Successfully entered Travel Note values");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnContinue"]);
	  appLog("Successfully Clicked on continue button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  kony.automation.widget.touch(["frmCardManagement","myCards","segMyCards[0]","flxCheckBox"], null,null,[26,27]);
	  appLog("Successfully Clicked on CheckBox");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnCardsContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnCardsContinue"]);
	  appLog("Successfully Clicked on Continue Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","lblConfirmTravelPlan"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblConfirmTravelPlan"], "text")).toEqual("Confirm Travel Plan");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	  await kony.automation.playback.wait(5000);
	
	  await VerifyCardTransactionMessage();
	}
	
	
	async function EditTravelPlan(UpdatedNumber){
	
	  await NavigateToManageTravelPlan();
	
	  appLog("Intiated method to Edit Travel Plan");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  kony.automation.button.click(["frmCardManagement","myCards","segMyCards[0]","btnAction1"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","txtPhoneNumber"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","txtPhoneNumber"],UpdatedNumber);
	  appLog("Successfully Updated Phone Number");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnContinue"]);
	  appLog("Successfully Clicked on Continue Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  kony.automation.widget.touch(["frmCardManagement","myCards","segMyCards[0]","flxCheckBox"], null,null,[26,28]);
	  appLog("Successfully Clicked on CheckBox");
	  await kony.automation.playback.waitFor(["frmCardManagement","btnCardsContinue"],15000);
	  kony.automation.button.click(["frmCardManagement","btnCardsContinue"]);
	  appLog("Successfully Clicked on Continue Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","btnConfirm"],15000);
	  kony.automation.button.click(["frmCardManagement","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	  await VerifyCardTransactionMessage();
	}
	
	async function DeleteTravelPlan(){
	
	  await NavigateToManageTravelPlan();
	
	  appLog("Intiated method to Delete Travel Plan");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
	  kony.automation.button.click(["frmCardManagement","myCards","segMyCards[0]","btnAction2"]);
	  appLog("Successfully Clicked on Delete Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmCardManagement","CustomAlertPopup","btnYes"],15000);
	  kony.automation.button.click(["frmCardManagement","CustomAlertPopup","btnYes"]);
	  appLog("Successfully Clicked on YES Button");
	  await kony.automation.playback.wait(5000);
	
	  await MoveBackfrom_MyCards();
	
	}
	
	async function searchforCards(SaerchCard){
	
	  appLog("Intiated method to Search for Cards");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","txtSearch"],15000);
	  kony.automation.widget.touch(["frmCardManagement","myCards","txtSearch"], [113,16],null,null);
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmCardManagement","myCards","txtSearch"],SaerchCard);
	  appLog("Successfully entered card : <b>"+SaerchCard+"</b>");
	  await kony.automation.playback.waitFor(["frmCardManagement","myCards","btnConfirm"],15000);
	  kony.automation.flexcontainer.click(["frmCardManagement","myCards","btnConfirm"]);
	  appLog("Successfully clicked on Confirm/Search button");
	
	  await MoveBackfrom_MyCards();
	}
	
	
	
	it("RequestNewCard", async function() {
	  
	  await navigateToCardManegement();
	  await applyforNewCard();
	  await addNewCardDetails();
	  
	},120000);
	
	it("ActivateNewCard", async function() {
	  
	  await navigateToCardManegement();
	  await activateNewCard();
	  
	},120000);
	
	it("VerifyChangePIN", async function() {
	  
	  await navigateToCardManegement();
	  await VerifyChangePIN();
	  
	},120000);
	
	it("VerifySearchCards", async function() {
	  
	  await navigateToCardManegement();
	  await searchforCards("Saving");
	  
	},120000);
	
	it("VerifyMaskingCardNumber", async function() {
	  
	  await navigateToCardManegement();
	  await VerifyMaskedAccountNumber();
	  await MoveBackfrom_MyCards();
	  
	},120000);
	
	it("VerifyDailyPurchaseLimit", async function() {
	  
	    await navigateToCardManegement();
	    await setDailyWithDrawLimit();
	  
	},120000);
	
	it("VerifyDailyWithDrawLimit", async function() {
	  
	  await navigateToCardManegement();
	  await setDailyWithDrawLimit();
	  
	},120000);
	
	it("CreateNewTravelPlan", async function() {
	  
	  var Destination="GOA";
	  var PhoneNumber="1234567890";
	  var NoteValue="My Summer Travel Plan";
	  
	  await navigateToCardManegement();
	  await CreateNewTravelPlan(Destination,PhoneNumber,NoteValue);
	  
	},120000);
	
	it("EditTravelPlan", async function() {
	
	  var UpdatedPhoneNumber="0987654321";
	  
	  await navigateToCardManegement();
	  await EditTravelPlan(UpdatedPhoneNumber);
	  
	},120000);
	
	it("DeleteTravelPlan", async function() {
	  
	  await navigateToCardManegement();
	  await DeleteTravelPlan();
	  
	},120000);
	
	it("VerifyLockCard", async function() {
	  
	  await navigateToCardManegement();
	  await VerifylockCard();
	  
	},120000);
	
	it("VerifyUnLockCard", async function() {
	  
	  await navigateToCardManegement();
	  await VerifyUnlockCard();
	  
	},120000);
});