describe("MiniAccountsSuite", function() {
	afterEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  appLog('Inside after Each function');
	
	  if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	    appLog('Already in dashboard');
	  }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	    appLog('Inside Login Screen');
	  }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmAccountsDetails');
	    kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastTransfers');
	    kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmReview","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmReview');
	    kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmConfirmTransfer');
	    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPastPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPastPaymentsNew');
	    kony.automation.flexcontainer.click(["frmPastPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmScheduledPaymentsNew');
	    kony.automation.flexcontainer.click(["frmScheduledPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmDirectDebits","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmDirectDebits');
	    kony.automation.flexcontainer.click(["frmDirectDebits","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastViewActivity","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastViewActivity');
	    kony.automation.flexcontainer.click(["frmFastViewActivity","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastManagePayee');
	    kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastP2P');
	    kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBulkPayees');
	    kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayee');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayment');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentConfirm');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentAcknowledgement');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmManagePayees');
	    kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayABill');
	    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillConfirm');
	    kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"])
	  }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayScheduled');
	    kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayee1');
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayeeInformation');
	    kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeDetails');
	    kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmVerifyPayee');
	    kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayHistory');
	    kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivation');
	    kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivationAcknowledgement');
	    kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmNotificationsAndMessages');
	    kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmOnlineHelp');
	    kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmContactUsPrivacyTandC');
	    kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmProfileManagement');
	    kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMFATransactions","customheader","topmenu","flxaccounts"],5000)){
	    appLog('***Moving back from frmMFATransactions****');
	    kony.automation.flexcontainer.click(["frmMFATransactions","customheader","topmenu","flxaccounts"]);
	  }else{
	    appLog("Form name is not available");
	  }
	
	
	
	},240000);
	
	//Before each with flags
	
	// beforeEach(function() {
	
	//   var flgDashboard =  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000);
	//   appLog("Dashboard : "+flgDashboard);
	//   if(flgDashboard){
	//     appLog("Nothing to Do");
	//   }
	//   //Accounts Related
	//   var flgAccountsDetails =  await kony.automation.playback.waitFor(["frmAccountsDetails"],5000);
	//   appLog("flgAccountsDetails : "+flgAccountsDetails);
	//   if(flgAccountsDetails){
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Transfers Related
	//   var flgFastTransfers =  await kony.automation.playback.waitFor(["frmFastTransfers"],5000);
	//   appLog("FastTransfers : "+flgFastTransfers);
	//   if(flgFastTransfers){
	//     kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgReview =  await kony.automation.playback.waitFor(["frmReview"],5000);
	//   appLog("frmReview : "+flgReview);
	//   if(flgReview){
	//     kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	//   }
	//   var flgConfirmTransfer =  await kony.automation.playback.waitFor(["frmConfirmTransfer"],5000);
	//   appLog("frmConfirmTransfer : "+flgConfirmTransfer);
	//   if(frmConfirmTransfer){
	//     kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastTransfersActivites =  await kony.automation.playback.waitFor(["frmFastTransfersActivites"],5000);
	//   appLog("flgFastTransfersActivites : "+flgFastTransfersActivites);
	//   if(flgFastTransfersActivites){
	//     kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	//   }
	
	//   //Manage payee Related
	//   var flgFastManagePayee =  await kony.automation.playback.waitFor(["frmFastManagePayee"],5000);
	//   appLog("flgFastManagePayee : "+flgFastManagePayee);
	//   if(flgFastManagePayee){
	//     kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastP2P =  await kony.automation.playback.waitFor(["frmFastP2P"],5000);
	//   appLog("flgFastP2P : "+flgFastP2P);
	//   if(flgFastP2P){
	//     kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	//   }
	
	//   //BillPay Related
	
	//   var flgBulkPayees =  await kony.automation.playback.waitFor(["frmBulkPayees"],5000);
	//   appLog("flgBulkPayees : "+flgBulkPayees);
	//   if(flgBulkPayees){
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   } 
	
	//   var flgMakeOneTimePayee =  await kony.automation.playback.waitFor(["frmMakeOneTimePayee"],5000);
	//   appLog("flgMakeOneTimePayee : "+flgMakeOneTimePayee);
	//   if(flgMakeOneTimePayee){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgMakeOneTimePayment =  await kony.automation.playback.waitFor(["frmMakeOneTimePayment"],5000);
	//   appLog("flgMakeOneTimePayment : "+flgMakeOneTimePayment);
	//   if(flgMakeOneTimePayment){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentConfirm =  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm"],5000);
	//   appLog("flgOneTimePaymentConfirm : "+flgOneTimePaymentConfirm);
	//   if(flgOneTimePaymentConfirm){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentAcknowledgement =  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],5000);
	//   appLog("flgOneTimePaymentAcknowledgement : "+flgOneTimePaymentAcknowledgement);
	//   if(flgOneTimePaymentAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgManagePayees =  await kony.automation.playback.waitFor(["frmManagePayees"],5000);
	//   appLog("flgManagePayees : "+flgManagePayees);
	//   if(flgManagePayees){
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayABill =  await kony.automation.playback.waitFor(["frmPayABill"],5000);
	//   appLog("flgPayABill : "+flgPayABill);
	//   if(flgPayABill){
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillConfirm =  await kony.automation.playback.waitFor(["frmPayBillConfirm"],5000);
	//   appLog("flgPayBillConfirm : "+flgPayBillConfirm);
	//   if(flgPayBillConfirm){
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillAcknowledgement =  await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],5000);
	//   appLog("flgPayBillAcknowledgement : "+flgPayBillAcknowledgement);
	//   if(flgPayBillAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayScheduled =  await kony.automation.playback.waitFor(["frmBillPayScheduled"],5000);
	//   appLog("flgBillPayScheduled : "+flgBillPayScheduled);
	//   if(flgBillPayScheduled){
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayee1 =  await kony.automation.playback.waitFor(["frmAddPayee1"],5000);
	//   appLog("flgAddPayee1 : "+flgAddPayee1);
	//   if(flgAddPayee1){
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayeeInformation =  await kony.automation.playback.waitFor(["frmAddPayeeInformation"],5000);
	//   appLog("flgAddPayeeInformation : "+flgAddPayeeInformation);
	//   if(flgAddPayeeInformation){
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeDetails =  await kony.automation.playback.waitFor(["frmPayeeDetails"],5000);
	//   appLog("flgPayeeDetails : "+flgPayeeDetails);
	//   if(flgPayeeDetails){
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgVerifyPayee =  await kony.automation.playback.waitFor(["frmVerifyPayee"],5000);
	//   appLog("flgVerifyPayee : "+flgVerifyPayee);
	//   if(flgVerifyPayee){
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeAcknowledgement =  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement"],5000);
	//   appLog("flgPayeeAcknowledgement : "+flgPayeeAcknowledgement);
	//   if(flgPayeeAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayHistory =  await kony.automation.playback.waitFor(["frmBillPayHistory"],5000);
	//   appLog("flgBillPayHistory : "+flgBillPayHistory);
	//   if(flgBillPayHistory){
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayActivation =  await kony.automation.playback.waitFor(["frmBillPayActivation"],5000);
	//   appLog("flgBillPayActivation : "+flgBillPayActivation);
	//   if(flgBillPayActivation){
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }
	
	
	//   //Messages Related
	
	
	//   var flgNotificationsAndMessages =  await kony.automation.playback.waitFor(["frmNotificationsAndMessages"],5000);
	//   appLog("flgNotificationsAndMessages : "+flgNotificationsAndMessages);
	//   if(flgNotificationsAndMessages){
	//     kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Post Login Related
	
	//   var flgOnlineHelp =  await kony.automation.playback.waitFor(["frmOnlineHelp"],5000);
	//   appLog("flgOnlineHelp : "+flgOnlineHelp);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	//   var flgContactUsPrivacyTandC =  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC"],5000);
	//   appLog("flgContactUsPrivacyTandC : "+flgContactUsPrivacyTandC);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Settings Related
	
	//   var flgProfileManagement =  await kony.automation.playback.waitFor(["frmProfileManagement"],5000);
	//   appLog("flgProfileManagement : "+flgProfileManagement);
	//   if(flgProfileManagement){
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	
	// });
	
	beforeEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  strLogger=[];
	  appLog('Inside before Each function');
	
	 if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	    appLog('Already in dashboard');
	  }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	    appLog('Inside Login Screen');
	  }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmAccountsDetails');
	    kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastTransfers');
	    kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmReview","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmReview');
	    kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmConfirmTransfer');
	    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPastPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPastPaymentsNew');
	    kony.automation.flexcontainer.click(["frmPastPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmScheduledPaymentsNew');
	    kony.automation.flexcontainer.click(["frmScheduledPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmDirectDebits","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmDirectDebits');
	    kony.automation.flexcontainer.click(["frmDirectDebits","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastViewActivity","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastViewActivity');
	    kony.automation.flexcontainer.click(["frmFastViewActivity","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastManagePayee');
	    kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastP2P');
	    kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBulkPayees');
	    kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayee');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayment');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentConfirm');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentAcknowledgement');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmManagePayees');
	    kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayABill');
	    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillConfirm');
	    kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"])
	  }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayScheduled');
	    kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayee1');
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayeeInformation');
	    kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeDetails');
	    kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmVerifyPayee');
	    kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayHistory');
	    kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivation');
	    kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivationAcknowledgement');
	    kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmNotificationsAndMessages');
	    kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmOnlineHelp');
	    kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmContactUsPrivacyTandC');
	    kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmProfileManagement');
	    kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMFATransactions","customheader","topmenu","flxaccounts"],5000)){
	    appLog('***Moving back from frmMFATransactions****');
	    kony.automation.flexcontainer.click(["frmMFATransactions","customheader","topmenu","flxaccounts"]);
	  }else{
	    appLog("Form name is not available");
	  }
	
	
	},240000);
	
	//Before each with flags
	
	// beforeEach(function() {
	
	//   var flgDashboard =  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000);
	//   appLog("Dashboard : "+flgDashboard);
	//   if(flgDashboard){
	//     appLog("Nothing to Do");
	//   }
	//   //Accounts Related
	//   var flgAccountsDetails =  await kony.automation.playback.waitFor(["frmAccountsDetails"],5000);
	//   appLog("flgAccountsDetails : "+flgAccountsDetails);
	//   if(flgAccountsDetails){
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Transfers Related
	//   var flgFastTransfers =  await kony.automation.playback.waitFor(["frmFastTransfers"],5000);
	//   appLog("FastTransfers : "+flgFastTransfers);
	//   if(flgFastTransfers){
	//     kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgReview =  await kony.automation.playback.waitFor(["frmReview"],5000);
	//   appLog("frmReview : "+flgReview);
	//   if(flgReview){
	//     kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	//   }
	//   var flgConfirmTransfer =  await kony.automation.playback.waitFor(["frmConfirmTransfer"],5000);
	//   appLog("frmConfirmTransfer : "+flgConfirmTransfer);
	//   if(frmConfirmTransfer){
	//     kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastTransfersActivites =  await kony.automation.playback.waitFor(["frmFastTransfersActivites"],5000);
	//   appLog("flgFastTransfersActivites : "+flgFastTransfersActivites);
	//   if(flgFastTransfersActivites){
	//     kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	//   }
	
	//   //Manage payee Related
	//   var flgFastManagePayee =  await kony.automation.playback.waitFor(["frmFastManagePayee"],5000);
	//   appLog("flgFastManagePayee : "+flgFastManagePayee);
	//   if(flgFastManagePayee){
	//     kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastP2P =  await kony.automation.playback.waitFor(["frmFastP2P"],5000);
	//   appLog("flgFastP2P : "+flgFastP2P);
	//   if(flgFastP2P){
	//     kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	//   }
	
	//   //BillPay Related
	
	//   var flgBulkPayees =  await kony.automation.playback.waitFor(["frmBulkPayees"],5000);
	//   appLog("flgBulkPayees : "+flgBulkPayees);
	//   if(flgBulkPayees){
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   } 
	
	//   var flgMakeOneTimePayee =  await kony.automation.playback.waitFor(["frmMakeOneTimePayee"],5000);
	//   appLog("flgMakeOneTimePayee : "+flgMakeOneTimePayee);
	//   if(flgMakeOneTimePayee){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgMakeOneTimePayment =  await kony.automation.playback.waitFor(["frmMakeOneTimePayment"],5000);
	//   appLog("flgMakeOneTimePayment : "+flgMakeOneTimePayment);
	//   if(flgMakeOneTimePayment){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentConfirm =  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm"],5000);
	//   appLog("flgOneTimePaymentConfirm : "+flgOneTimePaymentConfirm);
	//   if(flgOneTimePaymentConfirm){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentAcknowledgement =  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],5000);
	//   appLog("flgOneTimePaymentAcknowledgement : "+flgOneTimePaymentAcknowledgement);
	//   if(flgOneTimePaymentAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgManagePayees =  await kony.automation.playback.waitFor(["frmManagePayees"],5000);
	//   appLog("flgManagePayees : "+flgManagePayees);
	//   if(flgManagePayees){
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayABill =  await kony.automation.playback.waitFor(["frmPayABill"],5000);
	//   appLog("flgPayABill : "+flgPayABill);
	//   if(flgPayABill){
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillConfirm =  await kony.automation.playback.waitFor(["frmPayBillConfirm"],5000);
	//   appLog("flgPayBillConfirm : "+flgPayBillConfirm);
	//   if(flgPayBillConfirm){
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillAcknowledgement =  await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],5000);
	//   appLog("flgPayBillAcknowledgement : "+flgPayBillAcknowledgement);
	//   if(flgPayBillAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayScheduled =  await kony.automation.playback.waitFor(["frmBillPayScheduled"],5000);
	//   appLog("flgBillPayScheduled : "+flgBillPayScheduled);
	//   if(flgBillPayScheduled){
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayee1 =  await kony.automation.playback.waitFor(["frmAddPayee1"],5000);
	//   appLog("flgAddPayee1 : "+flgAddPayee1);
	//   if(flgAddPayee1){
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayeeInformation =  await kony.automation.playback.waitFor(["frmAddPayeeInformation"],5000);
	//   appLog("flgAddPayeeInformation : "+flgAddPayeeInformation);
	//   if(flgAddPayeeInformation){
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeDetails =  await kony.automation.playback.waitFor(["frmPayeeDetails"],5000);
	//   appLog("flgPayeeDetails : "+flgPayeeDetails);
	//   if(flgPayeeDetails){
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgVerifyPayee =  await kony.automation.playback.waitFor(["frmVerifyPayee"],5000);
	//   appLog("flgVerifyPayee : "+flgVerifyPayee);
	//   if(flgVerifyPayee){
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeAcknowledgement =  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement"],5000);
	//   appLog("flgPayeeAcknowledgement : "+flgPayeeAcknowledgement);
	//   if(flgPayeeAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayHistory =  await kony.automation.playback.waitFor(["frmBillPayHistory"],5000);
	//   appLog("flgBillPayHistory : "+flgBillPayHistory);
	//   if(flgBillPayHistory){
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayActivation =  await kony.automation.playback.waitFor(["frmBillPayActivation"],5000);
	//   appLog("flgBillPayActivation : "+flgBillPayActivation);
	//   if(flgBillPayActivation){
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }
	
	
	//   //Messages Related
	
	
	//   var flgNotificationsAndMessages =  await kony.automation.playback.waitFor(["frmNotificationsAndMessages"],5000);
	//   appLog("flgNotificationsAndMessages : "+flgNotificationsAndMessages);
	//   if(flgNotificationsAndMessages){
	//     kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Post Login Related
	
	//   var flgOnlineHelp =  await kony.automation.playback.waitFor(["frmOnlineHelp"],5000);
	//   appLog("flgOnlineHelp : "+flgOnlineHelp);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	//   var flgContactUsPrivacyTandC =  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC"],5000);
	//   appLog("flgContactUsPrivacyTandC : "+flgContactUsPrivacyTandC);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Settings Related
	
	//   var flgProfileManagement =  await kony.automation.playback.waitFor(["frmProfileManagement"],5000);
	//   appLog("flgProfileManagement : "+flgProfileManagement);
	//   if(flgProfileManagement){
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	
	// });
	
	
	async function verifyAccountsLandingScreen(){
	
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
	}
	
	async function SelectAccountsOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	
	  var Status=false;
	  
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
	        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(5000);
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	  
	  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnFirstCheckingAccount(){
	
	  appLog("Intiated method to click on First Checking account");
	  SelectAccountsOnDashBoard("Checking");
	  //appLog("Successfully Clicked on First Checking account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstSavingsAccount(){
	
	  appLog("Intiated method to click on First Savings account");
	  SelectAccountsOnDashBoard("Saving");
	  //appLog("Successfully Clicked on First Savings account");
	  //await kony.automation.playback.wait(5000);
	}
	
	
	async function clickOnFirstCreditCardAccount(){
	
	  appLog("Intiated method to click on First CreditCard account");
	  SelectAccountsOnDashBoard("Credit");
	  //appLog("Successfully Clicked on First CreditCard account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstDepositAccount(){
	
	  appLog("Intiated method to click on First Deposit account");
	  SelectAccountsOnDashBoard("Deposit");
	  //appLog("Successfully Clicked on First Deposit account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstLoanAccount(){
	
	  appLog("Intiated method to click on First Loan account");
	  SelectAccountsOnDashBoard("Loan");
	  //appLog("Successfully Clicked on First Loan account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnSearch_AccountDetails(){
	
	  appLog("Intiated method to click on Search Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Search Icon");
	}
	
	async function selectTranscationtype(TransactionType){
	
	  appLog("Intiated method to select Transcation type");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],TransactionType);
	  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
	}
	
	async function selectAmountRange(From,To){
	
	  appLog("Intiated method to select Amount Range");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],From);
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],To);
	
	  appLog("Successfully selected amount Range : ["+From+","+To+"]");
	}
	
	async function selectCustomdate(){
	
	  appLog("Intiated method to select Custom Date Range");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
	  appLog("Successfully selected Date Range");
	}
	
	async function clickOnbtnSearch(){
	
	  appLog("Intiated method to click on Search Button with given search criteria");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully Clicked on Search Button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function validateSearchResult() {
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	}
	
	async function MoveBackToLandingScreen_AccDetails(){
	
	  appLog("Move back to Account Dashboard from AccountsDetails");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	async function SelectContextualOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	  
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	  
	  expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnCheckingAccountContextMenu(){
	
	  appLog("Intiated method to click on Checking account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,0]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
	  //   appLog("Successfully clicked on Checking account context Menu");
	
	  SelectContextualOnDashBoard("Checking");
	}
	
	async function clickOnSavingsAccountContextMenu(){
	
	  appLog("Intiated method to click on Saving account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,1]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,1]","flxMenu"]);
	  //   appLog("Successfully clicked on Saving account context Menu");
	  SelectContextualOnDashBoard("Saving");
	}
	
	async function clickOnCreditCardAccountContextMenu(){
	
	  appLog("Intiated method to click on CreditCard account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,2]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,2]","flxMenu"]);
	  //   appLog("Successfully clicked on CreditCard account context Menu");
	
	  SelectContextualOnDashBoard("Credit");
	}
	
	async function clickOnDepositAccountContextMenu(){
	
	  appLog("Intiated method to click on Deposit account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,3]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,3]","flxMenu"]);
	  //   appLog("Successfully clicked on Deposit account context Menu");
	
	  SelectContextualOnDashBoard("Deposit");
	}
	
	async function clickOnLoanAccountContextMenu(){
	
	  appLog("Intiated method to click on Loan account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[1,0]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxMenu"]);
	  //   appLog("Successfully clicked on Loan account context Menu");
	
	  SelectContextualOnDashBoard("Loan");
	
	}
	
	async function verifyContextMenuOptions(myList_Expected){
	
	  //var myList_Expected = new Array();
	  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	  myList_Expected.push(myList_Expected);
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  var myList = new Array();
	
	  for(var x = 0; x <segLength-1; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+options);
	    myList.push(options);
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	
	  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	  //appLog("isFounded"+isFounded);
	  expect(isFounded).toBe(true);
	}
	async function MoveBackToLandingScreen_Accounts(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	}
	
	async function scrolltoTranscations_accountDetails(){
	
	  appLog("Intiated method to scroll to Transcations under account details");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	}
	
	async function verifyAccountSummary_CheckingAccounts(){
	
	  appLog("Intiated method to verify account summary for Checking Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_DepositAccounts(){
	
	  appLog("Intiated method to verify account summary for Deposit Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_CreditCardAccounts(){
	
	  appLog("Intiated method to verify account summary for CreditCard Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	
	}
	
	async function verifyAccountSummary_LoanAccounts(){
	
	  appLog("Intiated method to verify account summary for Loan Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_SavingsAccounts(){
	
	  appLog("Intiated method to verify account summary for Savings Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountsOrder_DashBoard(){
	
	  appLog("Intiated method to verify accounts order in Dashboard");
	
	  //Accounts Order can't be garunteed across different users. Hence checking all Types of accounts.
	  var myList = new Array();
	  var myList_Expected = new Array();
	  myList_Expected.push("Checking","Saving","Credit","Deposit","Loan");
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  for(var x = 0; x <segLength; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	      var seg="segAccounts["+x+","+y+"]";
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      myList.push(accountName);
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	}
	
	
	async function VerifyAccountOnDashBoard(AccountType){
	
	  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        appLog("Successfully verified : <b>"+accountName+"</b>");
	        myList.push("TRUE");
	        finished = true;
	        break;
	      }else{
	        myList.push("FALSE");
	      }
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	async function VerifyCheckingAccountonDashBoard(){
	
	  appLog("Intiated method to verify Checking account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
	  VerifyAccountOnDashBoard("Checking");
	}
	
	async function VerifySavingsAccountonDashBoard(){
	
	  appLog("Intiated method to verify Savings account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
	  VerifyAccountOnDashBoard("Saving");
	}
	async function VerifyCreditCardAccountonDashBoard(){
	
	  appLog("Intiated method to verify CreditCard account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
	  VerifyAccountOnDashBoard("Credit");
	}
	
	async function VerifyDepositAccountonDashBoard(){
	
	  appLog("Intiated method to verify Deposit account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
	  VerifyAccountOnDashBoard("Deposit");
	}
	
	async function VerifyLoanAccountonDashBoard(){
	
	  appLog("Intiated method to verify Loan account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
	  VerifyAccountOnDashBoard("Loan");
	}
	
	async function verifyViewAllTranscation(){
	
	  appLog("Intiated method to view all Tranx in AccountDetails");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	}
	
	async function verifyAdvancedSearch_AccountDetails(AmountRange1,AmountRange2){
	
	  appLog("Intiated method to verify Advanced Search in AccountDetails");
	
	  //   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnAll"],15000);
	  //   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnAll"]);
	  //   appLog("Successfully clicked on All button under AccountDetails");
	  //   await kony.automation.playback.wait(5000);
	
	  appLog("Intiated method to click on Seach Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Seach Icon");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], "Transfers");
	  appLog("Successfully selected Transcation Type");
	
	  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
	  appLog("Successfully selected amount range From");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
	  appLog("Successfully selected amount range To");
	
	  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	  appLog("Successfully selected date range");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	
	}
	
	
	async function selectContextMenuOption(Option){
	
	  appLog("Intiated method to select context menu option :: "+Option);
	
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment will be :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+TransfersText);
	
	    if(TransfersText===Option){
	      appLog("Option to be selected is :"+TransfersText);
	      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
	      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
	      await kony.automation.playback.wait(5000);
	      myList.push("TRUE");
	      break;
	    }else{
	      myList.push("FALSE");
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	
	async function verifyVivewStatementsHeader(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");
	
	}
	
	
	async function navigateToTransfers(){
	
	  appLog("Intiated method to Navigate FastTransfers Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransferMoney"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransferMoney"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmFastTransfers","lblTransfers"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","lblTransfers"], "text")).toEqual("Transfers");
	  appLog("Successfully Navigated to FastTransfers Screen");
	}
	
	async function SelectFromAccount(fromAcc){
	
	  appLog("Intiated method to Select From Account");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmFastTransfers","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmFastTransfers","txtTransferFrom"], [230,25],null,null);
	  kony.automation.textbox.enterText(["frmFastTransfers","txtTransferFrom"],fromAcc);
	  appLog("Successfully Entered From Account");
	  await kony.automation.playback.wait(5000);
	  kony.automation.flexcontainer.click(["frmFastTransfers","segTransferFrom[0,0]","flxAmount"]);
	  appLog("Successfully Selected From Account from List");
	}
	
	async function SelectToAccount(ToAccReciptent){
	
	  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastTransfers","txtTransferTo"],15000);
	  kony.automation.widget.touch(["frmFastTransfers","txtTransferTo"], [72,9],null,null);
	
	  //   if(ToAccReciptent==='OwnAcc'){
	  //     kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],"Saving");
	  //     appLog("Successfully Entered Default To Account : ");
	  //     await kony.automation.playback.wait(5000);
	  //     expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
	  //   }else{
	  //     kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],ToAccReciptent);
	  //     appLog("Successfully Entered To Account : <b>"+ToAccReciptent+"</b>");
	  //     await kony.automation.playback.wait(5000);
	  //     expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
	  //   }
	
	  kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],ToAccReciptent);
	  appLog("Successfully Entered To Account : <b>"+ToAccReciptent+"</b>");
	  await kony.automation.playback.wait(5000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
	
	  kony.automation.flexcontainer.click(["frmFastTransfers","segTransferTo[0,0]","flxAmount"]);
	  appLog("Successfully Selected To Account from List");
	
	}
	
	async function EnterAmount(amountValue) {
	
	  await kony.automation.playback.waitFor(["frmFastTransfers","tbxAmount"],15000);
	  kony.automation.textbox.enterText(["frmFastTransfers","tbxAmount"],amountValue);
	  appLog("Successfully Entered Amount as : <b>"+amountValue+"</b>");
	  await kony.automation.scrollToWidget(["frmFastTransfers","customfooternew","btnFaqs"]);
	}
	
	async function SelectFrequency(freqValue) {
	
	  kony.automation.flexcontainer.click(["frmFastTransfers","flxContainer4"]);
	  kony.automation.listbox.selectItem(["frmFastTransfers","lbxFrequency"], freqValue);
	  appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	}
	
	async function SelectDateRange() {
	
	  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
	  kony.automation.calendar.selectDate(["frmFastTransfers","calEndingOnNew"], [11,25,2021]);
	  appLog("Successfully Selected DateRange");
	}
	
	async function SelectSendOnDate() {
	
	  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
	  appLog("Successfully Selected SendOn Date");
	}
	
	async function SelectOccurences(occurences) {
	
	  kony.automation.listbox.selectItem(["frmFastTransfers","lbxForHowLong"], "NO_OF_RECURRENCES");
	  kony.automation.textbox.enterText(["frmFastTransfers","tbxNoOfRecurrences"],occurences);
	  appLog("Successfully Selected Occurences as <b>"+occurences+"</b>");
	  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
	  appLog("Successfully Selected SendOn Date");
	}
	async function EnterNoteValue(notes) {
	
	  await kony.automation.playback.waitFor(["frmFastTransfers","txtNotes"],10000);
	  kony.automation.textbox.enterText(["frmFastTransfers","txtNotes"],notes);
	  appLog("Successfully entered Note value as : <b>"+notes+"</b>");
	  await kony.automation.playback.waitFor(["frmFastTransfers","btnConfirm"],10000);
	  await kony.automation.scrollToWidget(["frmFastTransfers","btnConfirm"]);
	  kony.automation.button.click(["frmFastTransfers","btnConfirm"]);
	  appLog("Successfully Clicked on Continue Button");
	}
	
	async function ConfirmTransfer() {
	
	  appLog("Intiated method to Confirm Transfer Details");
	
	  await kony.automation.playback.waitFor(["frmReview","btnConfirm"],15000);
	  kony.automation.button.click(["frmReview","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	async function VerifyTransferSuccessMessage() {
	
	  appLog("Intiated method to Verify Transfer SuccessMessage ");
	
	  await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmConfirmTransfer"],30000);
	
	  if(success){
	    //expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).toContain("We successfully");
	    //   expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).not.toBe('');
	    //   await kony.automation.playback.waitFor(["frmConfirmTransfer","btnSavePayee"],15000);
	    //   kony.automation.button.click(["frmConfirmTransfer","btnSavePayee"]);
	    await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : rtxMakeTransferError");
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	async function CancelTransfer() {
	
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmReview","btnCancel"],15000);
	  kony.automation.button.click(["frmReview","btnCancel"]);
	  appLog("Successfully Clicked on CANCEL Button");
	  await kony.automation.playback.waitFor(["frmReview","CustomPopup","btnYes"],15000);
	  kony.automation.button.click(["frmReview","CustomPopup","btnYes"]);
	  appLog("Successfully Clicked on YES Button");
	}
	
	async function navigateToTransferActivities(){
	
	  appLog("Intiated method to navigate to Transfer Activities");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxPayBills"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxPayBills"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to TransferActivities");
	}
	
	async function navigateToPastTransfersTab(){
	
	  appLog("Intiated method to navigate to PastTransfer Tab");
	  await kony.automation.playback.waitFor(["frmFastTransfersActivites","btnRecent"],15000);
	  kony.automation.button.click(["frmFastTransfersActivites","btnRecent"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully navigated to PastTransfer Tab");
	}
	
	async function verifyRepeatTransferFunctionality(note){
	
	  appLog("Intiated method verify Repeat Transfer Functionality");
	
	  var noTransfers=await kony.automation.playback.waitFor(["frmFastTransfersActivites","rtxNoPaymentMessage"],10000);
	
	  if(noTransfers){
	
	    appLog('There are No Transactions Found');
	    await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	    await kony.automation.playback.wait(5000); 
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else{
	
	    //await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
	    var noReapeatBtn= await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","btnAction"],10000);
	
	    if(noReapeatBtn){
	      appLog('Intiating Repeat Transfer');
	      kony.automation.button.click(["frmFastTransfersActivites","segmentTransfers[0]","btnAction"]);
	      await kony.automation.playback.wait(5000);
	      appLog("Successfully Clicked on Repeat Button");
	      await EnterNoteValue(note);
	      await ConfirmTransfer();
	      await VerifyTransferSuccessMessage();
	    }else{
	      appLog('No Repeat Transfers available');
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	      await kony.automation.playback.wait(5000); 
	      appLog("Successfully Moved back to Accounts dashboard");
	    }
	  }
	
	
	}
	
	async function VerifyTranxUnderActivities(){
	
	  appLog("Intiated method verify Transfer under Activities");
	
	  var noTransfers=await kony.automation.playback.waitFor(["frmFastTransfersActivites","rtxNoPaymentMessage"],15000);
	
	  if(noTransfers){
	    appLog('There are No Transactions Found');
	    await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	    await kony.automation.playback.wait(5000);
	    await verifyAccountsLandingScreen();
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else{
	
	    //await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
	    var noTranxBtn= await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","flxDropdown"],10000);
	    if(noTranxBtn){
	
	      appLog('Intiating to verify Transfer Activity');
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","segmentTransfers[0]","flxDropdown"]);
	      appLog("Successfully Clicked on first Sheduled Transfer");
	
	      //No garuntee that same note will be there, other users also will perform Tranx
	      //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfersActivites","segmentTransfers[0]","flxFastPastTransfersSelected","lblNotesValue1"],"text")).toEqual(notevalue);
	
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	      await verifyAccountsLandingScreen();
	      appLog("Successfully Moved back to Accounts dashboard");
	    }else{
	
	      appLog('No Transfers activities available');
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	      await kony.automation.playback.wait(5000); 
	      appLog("Successfully Moved back to Accounts dashboard");
	    }
	
	  }
	}
	
	async function MoveBackToLandingScreen_Transfers(){
	
	  //Move back to landing Screen
	  appLog("Intiated method to move from frmFastTransfers screen");
	  await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	
	it("VerifyAccountsLandingPage", async function() {
	
	  await verifyAccountsLandingScreen();
	
	},30000);
	
	it("CheckingAcc_ContextMenuOptions", async function() {
	  
	  var myList_Expected = new Array();
	  myList_Expected.push(Accounts.checkingAcc.ContextMenuOptions[0].type,Accounts.checkingAcc.ContextMenuOptions[1].type,Accounts.checkingAcc.ContextMenuOptions[2].type,Accounts.checkingAcc.ContextMenuOptions[3].type,Accounts.checkingAcc.ContextMenuOptions[4].type,Accounts.checkingAcc.ContextMenuOptions[5].type);
	
	  await clickOnCheckingAccountContextMenu();
	  await verifyContextMenuOptions(myList_Expected);
	  await MoveBackToLandingScreen_Accounts();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   var myList = new Array();
	//   var myList_Expected = new Array();
	//   myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	//   for(var x = 0; x <segLength-1; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"]);
	//     var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	//     kony.print("Text is :: "+options);
	//     myList.push(options);
	//   }
	  
	//   kony.print("My Actual List is :: "+myList);
	//   kony.print("My Expected List is:: "+myList_Expected);
	
	//   let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	//   kony.print("isFounded"+isFounded);
	//   expect(isFounded).toBe(true);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},90000);
	
	it("CheckingAcc_ScrollToTranscations", async function() {
	
	  await clickOnFirstCheckingAccount();
	  await scrolltoTranscations_accountDetails();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lblTransactions"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","transactions","lblTransactions"],"text")).toEqual("Transactions");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","segTransactions"]);
	//   await kony.automation.scrollToWidget(["frmAccountsDetails","transactions","segTransactions"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},90000);
	
	it("VerifyAccountSummaryCheckingAccounts", async function() {
	  
	  await clickOnFirstCheckingAccount();
	  await verifyAccountSummary_CheckingAccounts();
	  await MoveBackToLandingScreen_AccDetails();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblCurrentBalanceValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblPendingDepositsValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblPendingWithdrawalsValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblAsOf"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblAvailableBalanceValue"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	},90000);
	
	it("VerifyCheckingAcc_AdvanceSearch", async function() {
	  
	  await clickOnFirstCheckingAccount();
	  await verifyAdvancedSearch_AccountDetails("1","1000");
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnFilter2"]);
	//   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnFilter2"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"]);
	//   kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], "Transfers");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],"1");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],"1000");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"]);
	//   kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	//   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],10000);
	//   if(noResult){
	//     kony.print("No Results found with given criteria..");
	//     expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	//   }else{
	//      await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	//      kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	//   }
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},90000);
	
	it("VerifyCheckingAcc_ContextualMenu_Transfer", async function() {
	
	  await clickOnCheckingAccountContextMenu();
	  await selectContextMenuOption("Transfer");
	  
	  //Do Transfer
	  await SelectToAccount(Accounts.checkingAcc.toAccount);
	  await EnterAmount(Accounts.checkingAcc.amountValue);
	  await EnterNoteValue("VerifyCheckingAcc_ContextualMenu_Transfer");
	  await ConfirmTransfer();
	  await VerifyTransferSuccessMessage();
	  await verifyAccountsLandingScreen();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   for(var x = 0; x <segLength; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"]);
	//     var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"], "text");
	//     kony.print("Text is :: "+TransfersText);
	//     if(TransfersText==="Transfer"){
	//       kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	//        break;
	//     }
	//   }
	  
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmFastTransfers","segTransferTo"]);
	//   kony.automation.flexcontainer.click(["frmFastTransfers","segTransferTo[0,0]","flxAccountListItemWrapper"]); 
	//   await kony.automation.playback.waitFor(["frmFastTransfers","tbxAmount"]);
	//   kony.automation.textbox.enterText(["frmFastTransfers","tbxAmount"],"1");
	//   await kony.automation.playback.waitFor(["frmFastTransfers","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmFastTransfers","txtNotes"],"VerifyCheckingAcc_ContextualMenu_Transfer");
	//   kony.automation.button.click(["frmFastTransfers","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmReview","btnConfirm"]);
	//   kony.automation.button.click(["frmReview","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmConfirmTransfer","lblTransactionMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).toContain("We successfully completed the transfer");
	//   await kony.automation.playback.waitFor(["frmConfirmTransfer","btnSavePayee"]);
	//   kony.automation.button.click(["frmConfirmTransfer","btnSavePayee"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},90000);
	
	it("VerifyCheckingAcc_ContextualMenu_ViewStatement", async function() {
	  
	  await clickOnCheckingAccountContextMenu();
	  await selectContextMenuOption("View Statements");
	  await verifyVivewStatementsHeader();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   for(var x = 0; x <segLength; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"]);
	//     var ViewStatements=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"], "text");
	//     kony.print("Text is :: "+ViewStatements);
	//     if(ViewStatements==="View Statements"){
	//       kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	//        break;
	//     }
	//   }
	
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"], "text")).toEqual("View Statements");
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},90000);
	
	it("ViewAllTranscations", async function() {
	
	  await clickOnFirstCheckingAccount();
	  await verifyViewAllTranscation();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnAll"]);
	//   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnAll"]);
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],10000);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],10000);
	//   await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},90000);
});