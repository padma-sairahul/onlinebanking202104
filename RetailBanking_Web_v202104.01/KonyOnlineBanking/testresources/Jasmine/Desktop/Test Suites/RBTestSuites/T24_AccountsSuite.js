describe("T24_AccountsSuite", function() {
	beforeEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  strLogger=[];
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside before Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	},480000);
	
	afterEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside after Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	  //strLogger=null;
	
	},480000);
	
	async function verifyAccountsLandingScreen(){
	
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
	}
	
	async function SelectAccountsOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
	        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(10000);
	        
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnFirstCheckingAccount(){
	
	  appLog("Intiated method to click on First Checking account");
	  SelectAccountsOnDashBoard("Checking");
	  //appLog("Successfully Clicked on First Checking account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstSavingsAccount(){
	
	  appLog("Intiated method to click on First Savings account");
	  SelectAccountsOnDashBoard("Saving");
	  //appLog("Successfully Clicked on First Savings account");
	  //await kony.automation.playback.wait(5000);
	}
	
	
	async function clickOnFirstCreditCardAccount(){
	
	  appLog("Intiated method to click on First CreditCard account");
	  SelectAccountsOnDashBoard("Credit");
	  //appLog("Successfully Clicked on First CreditCard account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstDepositAccount(){
	
	  appLog("Intiated method to click on First Deposit account");
	  SelectAccountsOnDashBoard("Deposit");
	  //appLog("Successfully Clicked on First Deposit account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstLoanAccount(){
	
	  appLog("Intiated method to click on First Loan account");
	  SelectAccountsOnDashBoard("Loan");
	  //appLog("Successfully Clicked on First Loan account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstAvailableAccount(){
	
	  appLog("Intiated method to click on First available account on DashBoard");
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	  kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	  appLog("Successfully Clicked on First available account on DashBoard");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickOnSearch_AccountDetails(){
	
	  appLog("Intiated method to click on Search Glass Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Seach Glass Icon");
	}
	
	async function selectTranscationtype(TransactionType){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], TransactionType);
	  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
	}
	
	async function enterKeywordtoSearch(keyword){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtKeyword"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtKeyword"],keyword);
	  appLog("Successfully entered keyword for search : <b>"+keyword+"</b>");
	}
	
	async function selectAmountRange(AmountRange1,AmountRange2){
	
	  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
	  appLog("Successfully selected amount range From");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
	  appLog("Successfully selected amount range To");
	
	  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");
	}
	
	async function selectCustomdate(){
	
	  appLog("Intiated method to select Custom Date Range");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
	  appLog("Successfully selected Date Range");
	}
	
	async function selectTimePeriod(){
	
	  appLog("Intiated method to select custom timeperiod");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	  appLog("Successfully selected Time period");
	}
	
	async function clickOnAdvancedSearchBtn(){
	
	  appLog("Intiated method to click on Search Button with given search criteria");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function validateSearchResult() {
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	}
	
	async function scrolltoTranscations_accountDetails(){
	
	  appLog("Intiated method to scroll to Transcations under account details");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	}
	
	async function VerifyAdvancedSearch_byAmount(AmountRange1,AmountRange2){
	
	  await clickOnSearch_AccountDetails();
	  await selectAmountRange(AmountRange1,AmountRange2);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byTranxType(TransactionType){
	
	  await clickOnSearch_AccountDetails();
	  await selectTranscationtype(TransactionType);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byDate(){
	
	  await clickOnSearch_AccountDetails();
	  await selectTimePeriod();
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function VerifyAdvancedSearch_byKeyword(keyword){
	
	  await clickOnSearch_AccountDetails();
	  await enterKeywordtoSearch(keyword);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function MoveBackToLandingScreen_AccDetails(){
	
	  appLog("Intiated method to Move back to Account Dashboard from AccountsDetails");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	async function VerifyAccountOnDashBoard(AccountType){
	
	  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        appLog("Successfully verified : <b>"+accountName+"</b>");
	        myList.push("TRUE");
	        finished = true;
	        break;
	      }else{
	        myList.push("FALSE");
	      }
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	
	async function VerifyCheckingAccountonDashBoard(){
	
	  appLog("Intiated method to verify Checking account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
	  VerifyAccountOnDashBoard("Checking");
	}
	
	async function VerifySavingsAccountonDashBoard(){
	
	  appLog("Intiated method to verify Savings account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
	  VerifyAccountOnDashBoard("Saving");
	}
	async function VerifyCreditCardAccountonDashBoard(){
	
	  appLog("Intiated method to verify CreditCard account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
	  VerifyAccountOnDashBoard("Credit");
	}
	
	async function VerifyDepositAccountonDashBoard(){
	
	  appLog("Intiated method to verify Deposit account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
	  VerifyAccountOnDashBoard("Deposit");
	}
	
	async function VerifyLoanAccountonDashBoard(){
	
	  appLog("Intiated method to verify Loan account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
	  VerifyAccountOnDashBoard("Loan");
	}
	
	async function verifyViewAllTranscation(){
	
	  appLog("Intiated method to view all Tranx in AccountDetails");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	}
	
	
	async function verifyContextMenuOptions(myList_Expected){
	
	  //var myList_Expected = new Array();
	  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	  myList_Expected.push(myList_Expected);
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  var myList = new Array();
	
	  for(var x = 0; x <segLength-1; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+options);
	    myList.push(options);
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	
	  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	  //appLog("isFounded"+isFounded);
	  expect(isFounded).toBe(true);
	}
	
	async function MoveBackToLandingScreen_Accounts(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	}
	
	
	async function verifyAccountSummary_CheckingAccounts(){
	
	  appLog("Intiated method to verify account summary for Checking Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_DepositAccounts(){
	
	  appLog("Intiated method to verify account summary for Deposit Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_CreditCardAccounts(){
	
	  appLog("Intiated method to verify account summary for CreditCard Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	
	}
	
	async function verifyAccountSummary_LoanAccounts(){
	
	  appLog("Intiated method to verify account summary for Loan Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_SavingsAccounts(){
	
	  appLog("Intiated method to verify account summary for Savings Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function VerifyPendingWithdrawls_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab1"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab1"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"], "text")).not.toBe("");
	
	}
	
	async function VerifyInterestdetails_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab2"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab2"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab2"], "text")).not.toBe("");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl7Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl7Tab2"], "text")).not.toBe("");
	
	}
	
	async function VerifySwiftCode_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab3"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab3"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab3"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab3"], "text")).not.toBe("");
	}
	
	async function selectContextMenuOption(Option){
	
	  appLog("Intiated method to select context menu option :: "+Option);
	
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	  var segLength=accounts_Size.length;
	  appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment will be :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg],15000);
	    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    appLog("Menu Item Text is :: "+TransfersText);
	    if(TransfersText===Option){
	      appLog("Option to be selected is :"+TransfersText);
	       await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes"],15000);
	      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
	      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
	      await kony.automation.playback.wait(10000);
	      myList.push("TRUE");
	      break;
	    }else{
	      myList.push("FALSE");
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	
	  expect(Status).toBe(true,"Failed to click on option <b>"+Option+"</b>");
	}
	
	// async function SelectContextualOnDashBoard(AccountType){
	
	//   appLog("Intiated method to analyze accounts data Dashboard");
	
	//   var Status=false;
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	//   var segLength=accounts_Size.length;
	
	//   var finished = false;
	//   for(var x = 0; x <segLength && !finished; x++) {
	
	//     var segHeaders="segAccounts["+x+",-1]";
	
	//     var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	//     var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	//     //appLog('Sub accounts size is '+subaccounts_Length);
	
	//     for(var y = 0; y <subaccounts_Length; y++){
	
	//       var seg="segAccounts["+x+","+y+"]";
	
	//       var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	//       var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	//       if(typeOfAccount.includes(AccountType)){
	//         await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
	//         kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	//         appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	//         await kony.automation.playback.wait(5000);
	//         finished = true;
	//         Status=true;
	//         break;
	//       }
	//     }
	//   }
	
	//   expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
	// }
	
	async function SelectContextualOnDashBoard(AccountNumber){
	
	  appLog("Intiated method to select Menu of account : "+AccountNumber);
	  await kony.automation.playback.wait(5000);
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      appLog("Account Name is : "+accountName);
	      if(accountName.includes(AccountNumber)){
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(5000);
	        // Validate really list is displayed or not
	        var menuList=await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	        expect(menuList).toBe(true,"Failed to display contextual menu list items");
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on Menu of AccNumber: <b>"+AccountNumber+"</b>");
	}
	
	async function verifyVivewStatementsHeader(){
	
	  appLog('Intiated method to verify account statement header');
	  var Status=await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
	  //kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");
	  expect(Status).toBe(true,"Failed to Navigate to Account Statements");
	  //appLog('Successfully Verified Account Statement Header');
	}
	
	async function VerifyPostedPendingTranscation(){
	
	  appLog('Intiated method to verify Pending and Posted Transcations');
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  var TransType=kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","segTransactions[0,-1]","lblTransactionHeader"], "text");
	  if(TransType.includes("Posted")||TransType.includes("Pending")){
	    //kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    //await kony.automation.playback.wait(5000);
	    appLog('Transfer Type is : '+TransType);
	  }else{
	    appLog('No Pending or Posted Transcation available');
	  }
	
	}
	
	
	async function VerifyStopChequePaymentScreen(){
	  
	  appLog("Intiated method to verify StopChequePayment Screen");
	  var Status=await kony.automation.playback.waitFor(["frmStopPayments","lblChequeBookRequests"],30000);
	  expect(Status).toBe(true,"Failed to Verify StopChequePayment screen");
	  appLog("Successfully Verified StopChequePayment");
	  
	}
	
	async function VerifyAccountAlertScreen(){
	  
	  appLog("Intiated method to verify AccountAlert Screen");
	  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAlertsHeading"],30000);
	  expect(Status).toBe(true,"Failed to Verify AccountAlert screen");
	  appLog("Successfully Verified AccountAlert");
	}
	
	async function MoveBack_Accounts_StopChequeBook(){
	  
	  appLog("Intiated method to Move back to Account Dashboard from StopPayments");
	  await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	
	}
	
	async function MoveBack_Accounts_ProfileManagement(){
	
	  // Move back to base state
	  appLog("Intiated method to Move back to Account Dashboard from ProfileManagement");
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	
	async function navigateToMakePayments(){
	
	  appLog("Intiated method to Navigate Payments Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransferMoney"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransferMoney"]);
	  await kony.automation.playback.wait(15000);
	
	  await VerifyPaymentsScreen();
	
	}
	
	async function navigateToTransfers(){
	
	  appLog("Intiated method to Navigate Transfers Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  //kony.automation.widget.touch(["frmDashboard","customheader","topmenu","flxTransfersAndPay"], [105,12],null,null);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxPayBills"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxPayBills"]);
	  await kony.automation.playback.wait(10000);
	
	  await VerifyTransfersScreen();
	
	}
	
	async function VerifyTransfersScreen(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],30000);
	  var Status=kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text");
	  expect(Status).toEqual("Transfers","Failed to Navigate to Transfers Screen");
	  appLog("Successfully Navigated to Transfers Screen");
	}
	
	async function VerifyPaymentsScreen(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],30000);
	  var Status=kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text");
	  expect(Status).toEqual("Payments","Failed to Navigate to Payments Screen");
	  appLog("Successfully Navigated to MakePayment Screen");
	}
	
	async function SelectFromAccount(fromAcc){
	
	  appLog("Intiated method to Select From Account :: <b>"+fromAcc+"</b>");
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferFrom"],30000);
	  kony.automation.widget.touch(["frmMakePayment","txtTransferFrom"], [241,25],null,null);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferFrom"],fromAcc);
	  await kony.automation.playback.wait(3000);
	  await kony.automation.playback.waitFor(["frmMakePayment","segTransferFrom"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","segTransferFrom[0,0]","flxAmount"]);
	  appLog("Successfully Selected From Account from List");
	}
	
	async function ReSelectFromAccount(fromAcc){
	
	  appLog("Intiated method to Re-Select From Account :: <b>"+fromAcc+"</b>");
	  await kony.automation.playback.waitFor(["frmMakePayment","flxDropdown"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxDropdown"]);
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferFrom"],30000);
	  kony.automation.widget.touch(["frmMakePayment","txtTransferFrom"], [189,19],null,null);
	  await kony.automation.playback.waitFor(["frmMakePayment","flxMainWrapper"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxMainWrapper"]);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferFrom"],fromAcc);
	  kony.automation.flexcontainer.click(["frmMakePayment","segTransferFrom[0,0]","flxAmount"]);
	
	  appLog("Successfully Selected From Account from List");
	}
	
	async function SelectToAccount(ToAccReciptent){
	
	  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferTo"],15000);
	  //kony.automation.widget.touch(["frmMakePayment","txtTransferTo"], [150,23],null,null);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferTo"],ToAccReciptent);
	  await kony.automation.playback.wait(3000);
	  await kony.automation.playback.waitFor(["frmMakePayment","segTransferTo"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","segTransferTo[0]","flxAccountListItemWrapper"]);
	  appLog("Successfully Selected To Account from List");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	
	}
	
	async function SelectOwnTransferToAccount(ToAccReciptent){
	
	  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferTo"],15000);
	  //kony.automation.widget.touch(["frmMakePayment","txtTransferTo"], [150,23],null,null);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferTo"],ToAccReciptent);
	  await kony.automation.playback.waitFor(["frmMakePayment","segTransferTo"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","segTransferTo[0,0]","flxAccountListItemWrapper"]);
	
	  appLog("Successfully Selected To Account from List");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	
	}
	
	async function verifyExistingSameBanBeneficiaryDetails(){
	
	
	}
	
	async function verifyExistingDomesticBeneficiaryDetails(){
	
	
	}
	
	async function verifyExistingInternationalBeneficiaryDetails(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtSwift"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtSwift"], "text")).not.toBe("");
	  //   await kony.automation.playback.waitFor(["frmMakePayment","txtAddressLine01"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtAddressLine01"], "text")).not.toBe("");
	  //   await kony.automation.playback.waitFor(["frmMakePayment","txtCity"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtCity"], "text")).not.toBe("");
	  //   await kony.automation.playback.waitFor(["frmMakePayment","txtPostCode"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtPostCode"], "text")).not.toBe("");
	
	}
	
	
	async function EnterAmount(amountValue) {
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amountValue);
	  appLog("Successfully Entered Amount as : <b>"+amountValue+"</b>");
	  await kony.automation.scrollToWidget(["frmMakePayment","customfooternew","btnFaqs"]);
	}
	
	async function SelectFrequency(freqValue) {
	
	  //kony.automation.flexcontainer.click(["frmFastTransfers","flxContainer4"]);
	  await kony.automation.playback.waitFor(["frmMakePayment","lbxFrequency"],15000);
	  kony.automation.listbox.selectItem(["frmMakePayment","lbxFrequency"], freqValue);
	  appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	}
	
	async function SelectDateRange() {
	
	  //var today = new Date();
	  //kony.automation.calendar.selectDate(["frmConsolidatedStatements","calFromDate"], [(today.getDate()-2),(today.getMonth()+1),today.getFullYear()]);
	  await kony.automation.playback.waitFor(["frmMakePayment","calSendOnNew"],15000);
	  kony.automation.calendar.selectDate(["frmMakePayment","calSendOnNew"], [4,25,2021]);
	  await kony.automation.playback.waitFor(["frmMakePayment","calEndingOnNew"],15000);
	  kony.automation.calendar.selectDate(["frmMakePayment","calEndingOnNew"], [4,25,2022]);
	  appLog("Successfully Selected DateRange");
	}
	
	async function SelectSendOnDate() {
	
	  await kony.automation.playback.waitFor(["frmMakePayment","calSendOnNew"],15000);
	  kony.automation.calendar.selectDate(["frmMakePayment","calSendOnNew"], [4,25,2021]);
	  appLog("Successfully Selected SendOn Date");
	}
	
	async function selectNormalPaymentRadio(){
	
	  appLog("Intiated method to select Payment mode as Normal");
	  await kony.automation.playback.waitFor(["frmMakePayment","flxRadioBtn5"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxRadioBtn5"]);
	  appLog("Successfully Selected Payment mode as Normal");
	}
	
	async function selectFeePaidRadio(){
	
	  // sync issue where it's not selecting properly some times
	  appLog("Intiated method to Selected Fee Pay Radio");
	//   await kony.automation.playback.waitFor(["frmMakePayment","flxtooltipFeesImg"],30000);
	//   kony.automation.flexcontainer.click(["frmMakePayment","flxtooltipFeesImg"]);
	  // - flxShared is t0 just come out of cursor
	  await kony.automation.playback.waitFor(["frmMakePayment","flxShared"],30000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxShared"]);
	
	  //await kony.automation.scrollToWidget(["frmMakePayment","lblRadioBtn3"]);
	  //kony.automation.widget.touch(["frmMakePayment","lblRadioBtn3"], null,null,[8,11]);
	  var isRadio=await kony.automation.playback.waitFor(["frmMakePayment","flxRadioBtn3"],45000);
	  expect(isRadio).toBe(true,"Failed to find Fee-Paid Radio");
	  //await kony.automation.scrollToWidget(["frmMakePayment","flxRadioBtn3"]);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxRadioBtn3"]);
	  appLog("Successfully Selected Fee Pay Radio");
	}
	
	async function EnterNoteValue(notes) {
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtPaymentReference"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtPaymentReference"],notes);
	  appLog("Successfully entered Note value as : <b>"+notes+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakePayment","btnConfirm"],15000);
	  var isEnable=kony.automation.widget.getWidgetProperty(["frmMakePayment","btnConfirm"], "enable");
	  if(isEnable){
	    appLog('Intiated method to click on Continue button');
	    await kony.automation.scrollToWidget(["frmMakePayment","btnConfirm"]);
	    kony.automation.button.click(["frmMakePayment","btnConfirm"]);
	    await kony.automation.playback.wait(5000);
	    appLog('Successfully Clicked on Continue button');
	
	    var Confirm=await kony.automation.playback.waitFor(["frmConfirmEuro","lblHeading"],45000);
	    if(Confirm){
	      appLog('Custom Message : Successfully moved to frmConfirmEuro Screen');
	    }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	      appLog('Custom Message : Failed with rtxMakeTransferError');
	      fail('Custom Message :'+kony.automation.widget.getWidgetProperty(["frmMakePayment","rtxMakeTransferError"], "text"));
	      await MoveBackToLandingScreen_Transfers();
	    }else{
	      appLog('Custom Message : Failed to Navigate to frmConfirmEuro Screen');
	      fail('Custom Message : Failed to Navigate to frmConfirmEuro Screen');
	      await MoveBackToLandingScreen_Transfers();
	    }
	  }else{
	    appLog('Custom Message : CONTINUE button is not enabled');
	  }
	
	}
	
	async function ValidatePaymentField_OwnTransfers(){
	
	  var Status=	await kony.automation.playback.waitFor(["frmConfirmEuro","lblPaymentMethodKey"],15000);
	  expect(Status).toBe(false,"Payment Method is not expected for Transfers");
	}
	
	async function ValidateSwiftCodeDetails_SameBankBenefeciary(){
	
	  var Status=	await kony.automation.playback.waitFor(["frmConfirmEuro","lblSWIFTBICKey"],15000);
	  expect(Status).toBe(false,"SWIFT Details are not expected for SameBank Benefeciary");
	
	}
	
	
	async function ConfirmTransfer() {
	
	  appLog("Intiated method to Confirm Transfer Details");
	
	  await kony.automation.playback.waitFor(["frmConfirmEuro","btnContinue"],30000);
	  kony.automation.button.click(["frmConfirmEuro","btnContinue"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	async function ClickOnModifyButton(){
	
	  await kony.automation.playback.waitFor(["frmConfirmEuro","btnModify"],15000);
	  kony.automation.button.click(["frmConfirmEuro","btnModify"]);
	  appLog("Successfully Clicked on btnModify Button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text")).not.toBe("");
	}
	
	async function VerifyTransferSuccessMessage() {
	
	  //await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblSuccessMessage"],90000);
	
	  if(success){
	    appLog("Intiated method to Verify Transfer SuccessMessage");
	    //await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblSuccessMessage"],15000);
	    //expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblSuccessMessage"], "text")).not.toBe("");
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text"));
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	async function verifyDataCutOff_Ackform(){
	
	  //await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro"],60000);
	
	  if(success){
	    appLog("Intiated method to Verify Transfer SuccessMessage");
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblFrequencyValue"],15000);
	    expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblFrequencyValue"], "text")).not.toBe("");
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblPaymentReferenceValue"],15000);
	    expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblPaymentReferenceValue"], "text")).not.toBe("");
	
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	
	  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text"));
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	}
	
	async function MoveBackToLandingScreen_Transfers(){
	
	  //Move back to landing Screen
	  appLog("Intiated method to move from frmMakePayment screen");
	  await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	  var DashBoard=await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(DashBoard).toBe(true,"Failed to navigate to DashBoard");
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function MoveBackToLandingScreen_TransferConfirm(){
	
	  //Move back to landing Screen
	  appLog("Intiated method to move from frmFastTransfers screen");
	  await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	  var DashBoard=await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(DashBoard).toBe(true,"Failed to navigate to DashBoard");
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	
	async function EnterNewToAccountName(ToAccReciptent){
	
	  appLog("Intiated method to enter new To Account :: <b>"+ToAccReciptent+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferTo"],15000);
	  kony.automation.widget.touch(["frmMakePayment","txtTransferTo"], [150,23],null,null);
	  kony.automation.textbox.enterText(["frmMakePayment","txtTransferTo"],ToAccReciptent);
	  appLog("Successfully entered New To acc name  as : <b>"+ToAccReciptent+"</b>");
	  // To laod dynamic data after selecting To account
	  //await kony.automation.playback.wait(5000);
	
	}
	
	async function clickOnNewButton_OneTimePay(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","lblNew"],15000);
	  kony.automation.widget.touch(["frmMakePayment","lblNew"], null,null,[12,11]);
	  await kony.automation.playback.waitFor(["frmMakePayment","flxCancelFilterTo"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxCancelFilterTo"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on new button for OneTimePay");
	
	}
	
	async function selectSameBankRadioBtn(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","flxBankOption1"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxBankOption1"]);
	  appLog("Successfully clicked on Radio button - This account is with us");
	}
	
	async function selectOtherBankRadioBtn(){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","flxBankOption2"],15000);
	  kony.automation.flexcontainer.click(["frmMakePayment","flxBankOption2"]);
	  appLog("Successfully clicked on Radio button - This account is Other bank");
	}
	
	async function enterOneTimePaymentDetails_SameBank(Accno,amount){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAccountNumber"],Accno);
	  appLog("Successfully entered acc no as : <b>"+Accno+"</b>");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amount);
	  appLog("Successfully entered amount as : <b>"+amount+"</b>");
	}
	
	async function enterOneTimePaymentDetails_Domestic(IBAN,amount){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAccountNumber"],IBAN);
	  appLog("Successfully entered acc no as : <b>"+IBAN+"</b>");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amount);
	  appLog("Successfully entered amount as : <b>"+amount+"</b>");
	}
	async function enterOneTimePaymentDetails_International(Accno,Swift,amount){
	
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAccountNumber"],Accno);
	  appLog("Successfully entered acc no as : <b>"+Accno+"</b>");
	  // To laod dynamic data after selecting To account
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmMakePayment","txtSwift"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtSwift"],Swift);
	  appLog("Successfully entered SWIFT as : <b>"+Swift+"</b>");
	  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amount);
	  appLog("Successfully entered amount as : <b>"+amount+"</b>");
	}
	
	async function VerifyOneTimePaymentSuccessMessage(){
	
	  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro"],60000);
	
	  if(success){
	    appLog("Intiated method to Verify OneTimePaymentSuccessMessage");
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmMakePayment","rtxMakeTransferError"], "text"));
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	async function SaveOneTimePaymentBenefeciary(){
	
	  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro","btnSaveBeneficiary"],60000);
	
	  if(success){
	    kony.automation.button.click(["frmAcknowledgementEuro","btnSaveBeneficiary"]);
	    appLog("Successfully clicked on btnSaveBeneficiary");
	    if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblSuccessmesg"],10000)){
	      //expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblSuccessmesg"], "text")).not.toBe("");
	      appLog("Info : Benefeciary is Saved Successfully");
	    }else{
	      await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblFailureMsg"],5000)
	      //expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblFailureMsg"], "text")).toEqual("jndj");
	      appLog("Warning : Same Benefeciary is Already Saved previousely");
	    }
	    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	
	  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : rtxMakeTransferError");
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	
	
	it("CheckingAcc_ContextMenuOptions", async function() {
	  
	 var myList_Expected = new Array();
	 myList_Expected.push(AllAccounts.Checking.MenuOptions[0].option,AllAccounts.Checking.MenuOptions[1].option);
	
	
	 await SelectContextualOnDashBoard(AllAccounts.Checking.Shortaccno);
	 await verifyContextMenuOptions(myList_Expected);
	 await MoveBackToLandingScreen_Accounts();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   var myList = new Array();
	//   var myList_Expected = new Array();
	//   myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	//   for(var x = 0; x <segLength-1; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"]);
	//     var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	//     kony.print("Text is :: "+options);
	//     myList.push(options);
	//   }
	  
	//   kony.print("My Actual List is :: "+myList);
	//   kony.print("My Expected List is:: "+myList_Expected);
	
	//   let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	//   kony.print("isFounded"+isFounded);
	//   expect(isFounded).toBe(true);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},TimeOuts.Accounts.Timeout);
	
	it("CheckingAcc_PostedPostedTranscation", async function() {
	  
	  await clickOnFirstCheckingAccount();
	  await VerifyPostedPendingTranscation();
	  await MoveBackToLandingScreen_AccDetails();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("CheckingAcc_ScrollToTranscations", async function() {
	
	  await clickOnFirstCheckingAccount();
	  await scrolltoTranscations_accountDetails();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lblTransactions"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","transactions","lblTransactions"],"text")).toEqual("Transactions");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","segTransactions"]);
	//   await kony.automation.scrollToWidget(["frmAccountsDetails","transactions","segTransactions"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},TimeOuts.Accounts.Timeout);
	
	it("DepositAcc_ContextMenuOptions", async function() {
	  
	  var myList_Expected = new Array();
	  myList_Expected.push(AllAccounts.Deposit.MenuOptions[0].option,AllAccounts.Deposit.MenuOptions[1].option);
	
	
	 await SelectContextualOnDashBoard(AllAccounts.Deposit.Shortaccno);
	 await verifyContextMenuOptions(myList_Expected);
	 await MoveBackToLandingScreen_Accounts();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[3,0]","flxMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   var myList = new Array();
	//   var myList_Expected = new Array();
	//   myList_Expected.push("View Statements","Update Account Settings","Account Alerts");
	//   for(var x = 0; x <segLength-1; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"]);
	//     var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	//     kony.print("Text is :: "+options);
	//     myList.push(options);
	//   }
	
	//   kony.print("My Actual List is :: "+myList);
	//   kony.print("My Expected List is:: "+myList_Expected);
	  
	//   let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	//   kony.print("isFounded"+isFounded);
	//   expect(isFounded).toBe(true);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	},TimeOuts.Accounts.Timeout);
	
	it("CheckingAcc_PostedPostedTranscation", async function() {
	  
	  await clickOnFirstDepositAccount();
	  await VerifyPostedPendingTranscation();
	  await MoveBackToLandingScreen_AccDetails();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("DepositAcc_ScrollToTranscation", async function() {
	
	  await clickOnFirstDepositAccount();
	  await scrolltoTranscations_accountDetails();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[3,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[3,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lblTransactions"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","transactions","lblTransactions"],"text")).toEqual("Transactions");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","segTransactions"]);
	//   await kony.automation.scrollToWidget(["frmAccountsDetails","transactions","segTransactions"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},TimeOuts.Accounts.Timeout);
	
	it("LoanAcc_ContextMenuOptions", async function() {
	
	  var myList_Expected = new Array();
	  myList_Expected.push(AllAccounts.Loan.MenuOptions[0].option,AllAccounts.Loan.MenuOptions[1].option);
	
	
	 await SelectContextualOnDashBoard(AllAccounts.Loan.Shortaccno);
	 await verifyContextMenuOptions(myList_Expected);
	 await MoveBackToLandingScreen_Accounts();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[4,0]","flxMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   var myList = new Array();
	//   var myList_Expected = new Array();
	//   myList_Expected.push("Pay Due Amount","View Statements","Update Account Settings","Account Alerts","Pay Off Loan");
	//   for(var x = 0; x <segLength-1; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"]);
	//     var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	//     kony.print("Text is :: "+options);
	//     myList.push(options);
	//   }
	
	//   kony.print("My Actual List is :: "+myList);
	//   kony.print("My Expected List is:: "+myList_Expected);
	  
	//   let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	//   kony.print("isFounded"+isFounded);
	//   expect(isFounded).toBe(true);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},TimeOuts.Accounts.Timeout);
	
	it("CheckingAcc_PostedPostedTranscation", async function() {
	  
	  await clickOnFirstLoanAccount();
	  await VerifyPostedPendingTranscation();
	  await MoveBackToLandingScreen_AccDetails();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("LoanAcc_ScrollToTranscations", async function() {
	
	  await clickOnFirstLoanAccount();
	  await scrolltoTranscations_accountDetails();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[4,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[4,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lblTransactions"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","transactions","lblTransactions"],"text")).toEqual("Transactions");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","segTransactions"]);
	//   await kony.automation.scrollToWidget(["frmAccountsDetails","transactions","segTransactions"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},TimeOuts.Accounts.Timeout);
	
	it("SavingAcc_ContextMenuOptions", async function() {
	
	  var myList_Expected = new Array();
	  myList_Expected.push(AllAccounts.Saving.MenuOptions[0].option,AllAccounts.Saving.MenuOptions[1].option);
	
	
	 await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
	 await verifyContextMenuOptions(myList_Expected);
	 await MoveBackToLandingScreen_Accounts();
	  
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   var myList = new Array();
	//   var myList_Expected = new Array();
	//   myList_Expected.push("Transfer","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	//   for(var x = 0; x <segLength-1; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"]);
	//     var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	//     kony.print("Text is :: "+options);
	//     myList.push(options);
	//   }
	
	//   kony.print("My Actual List is :: "+myList);
	//   kony.print("My Expected List is:: "+myList_Expected);
	  
	//   let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	//   kony.print("isFounded"+isFounded);
	//   expect(isFounded).toBe(true);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},TimeOuts.Accounts.Timeout);
	
	it("CheckingAcc_PostedPostedTranscation", async function() {
	  
	  await clickOnFirstSavingsAccount();
	  await VerifyPostedPendingTranscation();
	  await MoveBackToLandingScreen_AccDetails();
	  
	},TimeOuts.Accounts.Timeout);
	
	it("SavingsAcc_ScrolToTranscations", async function() {
	  
	  await clickOnFirstSavingsAccount();
	  await scrolltoTranscations_accountDetails();
	  await MoveBackToLandingScreen_AccDetails();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[1,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lblTransactions"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","transactions","lblTransactions"],"text")).toEqual("Transactions");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","segTransactions"]);
	//   await kony.automation.scrollToWidget(["frmAccountsDetails","transactions","segTransactions"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyAccountSummaryCheckingAccounts", async function() {
	  
	  await clickOnFirstCheckingAccount();
	  await verifyAccountSummary_CheckingAccounts();
	  await MoveBackToLandingScreen_AccDetails();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblCurrentBalanceValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblPendingDepositsValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblPendingWithdrawalsValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblAsOf"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblAvailableBalanceValue"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyAccountSummaryDepositAccounts", async function() {
	
	  await clickOnFirstDepositAccount();
	  await verifyAccountSummary_DepositAccounts();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[3,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[3,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblExtraFieldValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblPendingDepositsValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblAvailableBalanceValue"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyAccountSummaryForLoanAccounts", async function() {
	
	  await clickOnFirstLoanAccount();
	  await verifyAccountSummary_LoanAccounts();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[4,0]","flxAccountNameWrapper"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblExtraFieldValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblCurrentBalanceValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblPendingDepositsValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblPendingWithdrawalsValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblAsOf"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblAvailableBalanceValue"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyAccountSummarySavingsAccount", async function() {
	
	  await clickOnFirstSavingsAccount();
	  await verifyAccountSummary_SavingsAccounts();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxAccountNameWrapper"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblCurrentBalanceValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblPendingDepositsValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblPendingWithdrawalsValue"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblAsOf"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountSummary","lblAvailableBalanceValue"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyCheckingAccountonDashBoard", async function() {
	
	  await VerifyCheckingAccountonDashBoard();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyCheckingAcc_AdvanceSearch", async function() {
	  
	  await clickOnFirstCheckingAccount();
	  await VerifyAdvancedSearch_byAmount("1","1000");
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnFilter2"]);
	//   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnFilter2"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"]);
	//   kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], "Transfers");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],"1");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],"1000");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"]);
	//   kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	//   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],10000);
	//   if(noResult){
	//     kony.print("No Results found with given criteria..");
	//     expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	//   }else{
	//      await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	//      kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	//   }
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyCheckingAcc_ContextualMenu_Transfer", async function() {
	
	  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Current.MenuOptions[0].option);
	
	  //Do Transfer
	  await SelectOwnTransferToAccount(AllAccounts.Saving.accno);
	  await EnterAmount(Payments.OwnAcc.Amount);
	  await EnterNoteValue("VerifyCheckingAcc_ContextualMenu_Transfer");
	  await ConfirmTransfer();
	  await VerifyTransferSuccessMessage();
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	  //   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	  //   var segLength=accounts_Size.length;
	  //   kony.print("Length is :: "+segLength);
	  //   for(var x = 0; x <segLength; x++) {
	
	  //     var seg="segAccountListActions["+x+"]";
	  //     kony.print("Segment is :: "+seg);
	  //     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"]);
	  //     var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"], "text");
	  //     kony.print("Text is :: "+TransfersText);
	  //     if(TransfersText==="Transfer"){
	  //       kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	  //        break;
	  //     }
	  //   }
	
	  //   await kony.automation.playback.wait(5000);
	
	  //   await kony.automation.playback.waitFor(["frmFastTransfers","segTransferTo"]);
	  //   kony.automation.flexcontainer.click(["frmFastTransfers","segTransferTo[0,0]","flxAccountListItemWrapper"]); 
	  //   await kony.automation.playback.waitFor(["frmFastTransfers","tbxAmount"]);
	  //   kony.automation.textbox.enterText(["frmFastTransfers","tbxAmount"],"1");
	  //   await kony.automation.playback.waitFor(["frmFastTransfers","txtNotes"]);
	  //   kony.automation.textbox.enterText(["frmFastTransfers","txtNotes"],"VerifyCheckingAcc_ContextualMenu_Transfer");
	  //   kony.automation.button.click(["frmFastTransfers","btnConfirm"]);
	  //   await kony.automation.playback.waitFor(["frmReview","btnConfirm"]);
	  //   kony.automation.button.click(["frmReview","btnConfirm"]);
	
	  //   await kony.automation.playback.waitFor(["frmConfirmTransfer","lblTransactionMessage"]);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).toContain("We successfully completed the transfer");
	  //   await kony.automation.playback.waitFor(["frmConfirmTransfer","btnSavePayee"]);
	  //   kony.automation.button.click(["frmConfirmTransfer","btnSavePayee"]);
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyCheckingAcc_ContextualMenu_ViewStatement", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Current.MenuOptions[2].option);
	  await verifyVivewStatementsHeader();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   for(var x = 0; x <segLength; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"]);
	//     var ViewStatements=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"], "text");
	//     kony.print("Text is :: "+ViewStatements);
	//     if(ViewStatements==="View Statements"){
	//       kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	//        break;
	//     }
	//   }
	
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"], "text")).toEqual("View Statements");
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyDepositAccountonDashBoard", async function() {
	
	  await VerifyDepositAccountonDashBoard();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
	
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyDepositAcc_AdvanceSearch", async function() {
	  
	  await clickOnFirstDepositAccount();
	  await VerifyAdvancedSearch_byAmount("1","1000");
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[3,0]","flxAccountNameWrapper"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","btnTransfersChecking"]);
	//   kony.automation.button.click(["frmAccountsDetails","transactions","btnTransfersChecking"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","flxSearch"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","transactions","flxSearch"]);
	//   //await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lstbxTransactionType"]);
	//   //kony.automation.listbox.selectItem(["frmAccountsDetails","transactions","lstbxTransactionType"], "Transfers");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","txtAmountRangeFrom"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","transactions","txtAmountRangeFrom"],"1");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","txtAmountRangeTo"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","transactions","txtAmountRangeTo"],"1000");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lstbxTimePeriod"]);
	//   kony.automation.listbox.selectItem(["frmAccountsDetails","transactions","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","btnSearch"]);
	//   kony.automation.button.click(["frmAccountsDetails","transactions","btnSearch"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","rtxNoPaymentMessage"],10000);
	//   if(noResult){
	//     kony.print("No Results found with given criteria..");
	//     expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","transactions","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	//   }else{
	//      await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","segTransactions"]);
	//      kony.automation.flexcontainer.click(["frmAccountsDetails","transactions","segTransactions[0,0]","flxDropdown"]);
	//   }
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyDepositAcc_ContextualMenu_ViewStatement", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Deposit.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Deposit.MenuOptions[0].option);
	  await verifyVivewStatementsHeader();
	  await MoveBackToLandingScreen_AccDetails();
	  
	  
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[3,0]","flxMenu"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   for(var x = 0; x <segLength; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"]);
	//     var ViewStatements=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"], "text");
	//     kony.print("Text is :: "+ViewStatements);
	//     if(ViewStatements==="View Statements"){
	//       kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	//        break;
	//     }
	//   }
	
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"], "text")).toEqual("View Statements");
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyLoanAccountonDashBoard", async function() {
	
	  await VerifyLoanAccountonDashBoard();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
	
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyLoanAcc_AdvanceSearch", async function() {
	  
	  await clickOnFirstLoanAccount();
	  await VerifyAdvancedSearch_byAmount("1","1000");
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[4,0]","flxAccountNameWrapper"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","btnTransfersChecking"]);
	//   kony.automation.button.click(["frmAccountsDetails","transactions","btnTransfersChecking"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","flxSearch"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","transactions","flxSearch"]);
	//   //await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lstbxTransactionType"]);
	//   //kony.automation.listbox.selectItem(["frmAccountsDetails","transactions","lstbxTransactionType"], "Transfers");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","txtAmountRangeFrom"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","transactions","txtAmountRangeFrom"],"1");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","txtAmountRangeTo"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","transactions","txtAmountRangeTo"],"1000");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lstbxTimePeriod"]);
	//   kony.automation.listbox.selectItem(["frmAccountsDetails","transactions","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","btnSearch"]);
	//   kony.automation.button.click(["frmAccountsDetails","transactions","btnSearch"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","rtxNoPaymentMessage"],10000);
	//   if(noResult){
	//     kony.print("No Results found with given criteria..");
	//     expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","transactions","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	//   }else{
	//      await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","segTransactions"]);
	//      kony.automation.flexcontainer.click(["frmAccountsDetails","transactions","segTransactions[0,0]","flxDropdown"]);
	//   }
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},TimeOuts.Accounts.Timeout);
	
	it("VerifyLoanAcc_ContextualMenu_ViewStatement", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Loan.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Loan.MenuOptions[1].option);
	  await verifyVivewStatementsHeader();
	  await MoveBackToLandingScreen_AccDetails();
	  
	  
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[4,0]","flxMenu"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   for(var x = 0; x <segLength; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"]);
	//     var ViewStatements=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"], "text");
	//     kony.print("Text is :: "+ViewStatements);
	//     if(ViewStatements==="View Statements"){
	//       kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	//        break;
	//     }
	//   }
	
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"], "text")).toEqual("View Statements");
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},TimeOuts.Accounts.Timeout);
	
	it("VerifySavingsAccountonDashBoard", async function() {
	
	  await VerifySavingsAccountonDashBoard();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
	
	},TimeOuts.Accounts.Timeout);
	
	it("VerifySavingsAcc_AdvanceSearch", async function() {
	  
	  await clickOnFirstSavingsAccount();
	  await VerifyAdvancedSearch_byAmount("1","1000");
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxAccountNameWrapper"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","btnTransfersChecking"]);
	//   kony.automation.button.click(["frmAccountsDetails","transactions","btnTransfersChecking"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","flxSearch"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","transactions","flxSearch"]);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lstbxTransactionType"]);
	//   kony.automation.listbox.selectItem(["frmAccountsDetails","transactions","lstbxTransactionType"], "Transfers");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","txtAmountRangeFrom"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","transactions","txtAmountRangeFrom"],"1");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","txtAmountRangeTo"]);
	//   kony.automation.textbox.enterText(["frmAccountsDetails","transactions","txtAmountRangeTo"],"1000");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","lstbxTimePeriod"]);
	//   kony.automation.listbox.selectItem(["frmAccountsDetails","transactions","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","btnSearch"]);
	//   kony.automation.button.click(["frmAccountsDetails","transactions","btnSearch"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","rtxNoPaymentMessage"],10000);
	//   if(noResult){
	//     kony.print("No Results found with given criteria..");
	//     expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","transactions","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	//   }else{
	//      await kony.automation.playback.waitFor(["frmAccountsDetails","transactions","segTransactions"]);
	//      kony.automation.flexcontainer.click(["frmAccountsDetails","transactions","segTransactions[0,0]","flxDropdown"]);
	//   }
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},TimeOuts.Accounts.Timeout);
	
	it("VerifySavingsAcc_ContextualMenu_Transfer", async function() {
	
	  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[0].option);
	
	  //Do Transfer
	  await SelectOwnTransferToAccount(AllAccounts.Current.accno);
	  await EnterAmount(Payments.OwnAcc.Amount);
	  await EnterNoteValue("VerifySavingsAcc_ContextualMenu_Transfer");
	  await ConfirmTransfer();
	  await VerifyTransferSuccessMessage();
	
	  
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxMenu"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	  
	//    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   for(var x = 0; x <segLength; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"]);
	//     var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"], "text");
	//     kony.print("Text is :: "+TransfersText);
	//     if(TransfersText==="Transfer"){
	//       kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	//        break;
	//     }
	//   }
	  
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmFastTransfers","segTransferTo"]);
	//   kony.automation.flexcontainer.click(["frmFastTransfers","segTransferTo[0,0]","flxAccountListItemWrapper"]); 
	//   await kony.automation.playback.waitFor(["frmFastTransfers","tbxAmount"]);
	//   kony.automation.textbox.enterText(["frmFastTransfers","tbxAmount"],"1");
	//   await kony.automation.playback.waitFor(["frmFastTransfers","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmFastTransfers","txtNotes"],"VerifySavingsAcc_ContextualMenu_Transfer");
	//   kony.automation.button.click(["frmFastTransfers","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmReview","btnConfirm"]);
	//   kony.automation.button.click(["frmReview","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmConfirmTransfer","lblTransactionMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).toContain("We successfully completed the transfer");
	//   await kony.automation.playback.waitFor(["frmConfirmTransfer","btnSavePayee"]);
	//   kony.automation.button.click(["frmConfirmTransfer","btnSavePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},TimeOuts.Accounts.Timeout);
	
	it("VerifySavingsAcc_ContextualMenu_ViewStatement", async function() {
	  
	  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
	  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[2].option);
	  await verifyVivewStatementsHeader();
	  await MoveBackToLandingScreen_AccDetails();
	  
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxMenu"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"]);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  
	//   var segLength=accounts_Size.length;
	//   kony.print("Length is :: "+segLength);
	//   for(var x = 0; x <segLength; x++) {
	
	//     var seg="segAccountListActions["+x+"]";
	//     kony.print("Segment is :: "+seg);
	//     await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"]);
	//     var ViewStatements=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"flxAccountTypes","lblUsers"], "text");
	//     kony.print("Text is :: "+ViewStatements);
	//     if(ViewStatements==="View Statements"){
	//       kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	//        break;
	//     }
	//   }
	
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","ViewStatements","confirmHeaders","lblHeading"], "text")).toEqual("View Statements");
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},TimeOuts.Accounts.Timeout);
	
	it("ViewAllTranscations", async function() {
	
	  await clickOnFirstSavingsAccount();
	  await verifyViewAllTranscation();
	  await MoveBackToLandingScreen_AccDetails();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],10000);
	//   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnAll"]);
	//   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnAll"]);
	  
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],10000);
	//   expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],10000);
	//   await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	
	//   await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},90000);
});