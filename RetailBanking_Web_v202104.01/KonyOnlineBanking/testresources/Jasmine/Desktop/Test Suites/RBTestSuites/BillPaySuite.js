describe("BillPaySuite", function() {
	afterEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  appLog('Inside after Each function');
	
	  if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	    appLog('Already in dashboard');
	  }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	    appLog('Inside Login Screen');
	  }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmAccountsDetails');
	    kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastTransfers');
	    kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmReview","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmReview');
	    kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmConfirmTransfer');
	    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPastPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPastPaymentsNew');
	    kony.automation.flexcontainer.click(["frmPastPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmScheduledPaymentsNew');
	    kony.automation.flexcontainer.click(["frmScheduledPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmDirectDebits","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmDirectDebits');
	    kony.automation.flexcontainer.click(["frmDirectDebits","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastViewActivity","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastViewActivity');
	    kony.automation.flexcontainer.click(["frmFastViewActivity","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastManagePayee');
	    kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastP2P');
	    kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBulkPayees');
	    kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayee');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayment');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentConfirm');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentAcknowledgement');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmManagePayees');
	    kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayABill');
	    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillConfirm');
	    kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"])
	  }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayScheduled');
	    kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayee1');
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayeeInformation');
	    kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeDetails');
	    kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmVerifyPayee');
	    kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayHistory');
	    kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivation');
	    kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivationAcknowledgement');
	    kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmNotificationsAndMessages');
	    kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmOnlineHelp');
	    kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmContactUsPrivacyTandC');
	    kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmProfileManagement');
	    kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMFATransactions","customheader","topmenu","flxaccounts"],5000)){
	    appLog('***Moving back from frmMFATransactions****');
	    kony.automation.flexcontainer.click(["frmMFATransactions","customheader","topmenu","flxaccounts"]);
	  }else{
	    appLog("Form name is not available");
	  }
	
	
	
	},240000);
	
	//Before each with flags
	
	// beforeEach(function() {
	
	//   var flgDashboard =  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000);
	//   appLog("Dashboard : "+flgDashboard);
	//   if(flgDashboard){
	//     appLog("Nothing to Do");
	//   }
	//   //Accounts Related
	//   var flgAccountsDetails =  await kony.automation.playback.waitFor(["frmAccountsDetails"],5000);
	//   appLog("flgAccountsDetails : "+flgAccountsDetails);
	//   if(flgAccountsDetails){
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Transfers Related
	//   var flgFastTransfers =  await kony.automation.playback.waitFor(["frmFastTransfers"],5000);
	//   appLog("FastTransfers : "+flgFastTransfers);
	//   if(flgFastTransfers){
	//     kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgReview =  await kony.automation.playback.waitFor(["frmReview"],5000);
	//   appLog("frmReview : "+flgReview);
	//   if(flgReview){
	//     kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	//   }
	//   var flgConfirmTransfer =  await kony.automation.playback.waitFor(["frmConfirmTransfer"],5000);
	//   appLog("frmConfirmTransfer : "+flgConfirmTransfer);
	//   if(frmConfirmTransfer){
	//     kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastTransfersActivites =  await kony.automation.playback.waitFor(["frmFastTransfersActivites"],5000);
	//   appLog("flgFastTransfersActivites : "+flgFastTransfersActivites);
	//   if(flgFastTransfersActivites){
	//     kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	//   }
	
	//   //Manage payee Related
	//   var flgFastManagePayee =  await kony.automation.playback.waitFor(["frmFastManagePayee"],5000);
	//   appLog("flgFastManagePayee : "+flgFastManagePayee);
	//   if(flgFastManagePayee){
	//     kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastP2P =  await kony.automation.playback.waitFor(["frmFastP2P"],5000);
	//   appLog("flgFastP2P : "+flgFastP2P);
	//   if(flgFastP2P){
	//     kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	//   }
	
	//   //BillPay Related
	
	//   var flgBulkPayees =  await kony.automation.playback.waitFor(["frmBulkPayees"],5000);
	//   appLog("flgBulkPayees : "+flgBulkPayees);
	//   if(flgBulkPayees){
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   } 
	
	//   var flgMakeOneTimePayee =  await kony.automation.playback.waitFor(["frmMakeOneTimePayee"],5000);
	//   appLog("flgMakeOneTimePayee : "+flgMakeOneTimePayee);
	//   if(flgMakeOneTimePayee){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgMakeOneTimePayment =  await kony.automation.playback.waitFor(["frmMakeOneTimePayment"],5000);
	//   appLog("flgMakeOneTimePayment : "+flgMakeOneTimePayment);
	//   if(flgMakeOneTimePayment){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentConfirm =  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm"],5000);
	//   appLog("flgOneTimePaymentConfirm : "+flgOneTimePaymentConfirm);
	//   if(flgOneTimePaymentConfirm){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentAcknowledgement =  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],5000);
	//   appLog("flgOneTimePaymentAcknowledgement : "+flgOneTimePaymentAcknowledgement);
	//   if(flgOneTimePaymentAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgManagePayees =  await kony.automation.playback.waitFor(["frmManagePayees"],5000);
	//   appLog("flgManagePayees : "+flgManagePayees);
	//   if(flgManagePayees){
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayABill =  await kony.automation.playback.waitFor(["frmPayABill"],5000);
	//   appLog("flgPayABill : "+flgPayABill);
	//   if(flgPayABill){
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillConfirm =  await kony.automation.playback.waitFor(["frmPayBillConfirm"],5000);
	//   appLog("flgPayBillConfirm : "+flgPayBillConfirm);
	//   if(flgPayBillConfirm){
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillAcknowledgement =  await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],5000);
	//   appLog("flgPayBillAcknowledgement : "+flgPayBillAcknowledgement);
	//   if(flgPayBillAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayScheduled =  await kony.automation.playback.waitFor(["frmBillPayScheduled"],5000);
	//   appLog("flgBillPayScheduled : "+flgBillPayScheduled);
	//   if(flgBillPayScheduled){
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayee1 =  await kony.automation.playback.waitFor(["frmAddPayee1"],5000);
	//   appLog("flgAddPayee1 : "+flgAddPayee1);
	//   if(flgAddPayee1){
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayeeInformation =  await kony.automation.playback.waitFor(["frmAddPayeeInformation"],5000);
	//   appLog("flgAddPayeeInformation : "+flgAddPayeeInformation);
	//   if(flgAddPayeeInformation){
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeDetails =  await kony.automation.playback.waitFor(["frmPayeeDetails"],5000);
	//   appLog("flgPayeeDetails : "+flgPayeeDetails);
	//   if(flgPayeeDetails){
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgVerifyPayee =  await kony.automation.playback.waitFor(["frmVerifyPayee"],5000);
	//   appLog("flgVerifyPayee : "+flgVerifyPayee);
	//   if(flgVerifyPayee){
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeAcknowledgement =  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement"],5000);
	//   appLog("flgPayeeAcknowledgement : "+flgPayeeAcknowledgement);
	//   if(flgPayeeAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayHistory =  await kony.automation.playback.waitFor(["frmBillPayHistory"],5000);
	//   appLog("flgBillPayHistory : "+flgBillPayHistory);
	//   if(flgBillPayHistory){
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayActivation =  await kony.automation.playback.waitFor(["frmBillPayActivation"],5000);
	//   appLog("flgBillPayActivation : "+flgBillPayActivation);
	//   if(flgBillPayActivation){
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }
	
	
	//   //Messages Related
	
	
	//   var flgNotificationsAndMessages =  await kony.automation.playback.waitFor(["frmNotificationsAndMessages"],5000);
	//   appLog("flgNotificationsAndMessages : "+flgNotificationsAndMessages);
	//   if(flgNotificationsAndMessages){
	//     kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Post Login Related
	
	//   var flgOnlineHelp =  await kony.automation.playback.waitFor(["frmOnlineHelp"],5000);
	//   appLog("flgOnlineHelp : "+flgOnlineHelp);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	//   var flgContactUsPrivacyTandC =  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC"],5000);
	//   appLog("flgContactUsPrivacyTandC : "+flgContactUsPrivacyTandC);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Settings Related
	
	//   var flgProfileManagement =  await kony.automation.playback.waitFor(["frmProfileManagement"],5000);
	//   appLog("flgProfileManagement : "+flgProfileManagement);
	//   if(flgProfileManagement){
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	
	// });
	
	beforeEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  strLogger=[];
	  appLog('Inside before Each function');
	
	 if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	    appLog('Already in dashboard');
	  }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	    appLog('Inside Login Screen');
	  }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmAccountsDetails');
	    kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastTransfers');
	    kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmReview","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmReview');
	    kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmConfirmTransfer');
	    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPastPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPastPaymentsNew');
	    kony.automation.flexcontainer.click(["frmPastPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmScheduledPaymentsNew');
	    kony.automation.flexcontainer.click(["frmScheduledPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmDirectDebits","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmDirectDebits');
	    kony.automation.flexcontainer.click(["frmDirectDebits","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastViewActivity","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastViewActivity');
	    kony.automation.flexcontainer.click(["frmFastViewActivity","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastManagePayee');
	    kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastP2P');
	    kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBulkPayees');
	    kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayee');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayment');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentConfirm');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentAcknowledgement');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmManagePayees');
	    kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayABill');
	    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillConfirm');
	    kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"])
	  }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayScheduled');
	    kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayee1');
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayeeInformation');
	    kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeDetails');
	    kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmVerifyPayee');
	    kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayHistory');
	    kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivation');
	    kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivationAcknowledgement');
	    kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmNotificationsAndMessages');
	    kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmOnlineHelp');
	    kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmContactUsPrivacyTandC');
	    kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmProfileManagement');
	    kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMFATransactions","customheader","topmenu","flxaccounts"],5000)){
	    appLog('***Moving back from frmMFATransactions****');
	    kony.automation.flexcontainer.click(["frmMFATransactions","customheader","topmenu","flxaccounts"]);
	  }else{
	    appLog("Form name is not available");
	  }
	
	
	},240000);
	
	//Before each with flags
	
	// beforeEach(function() {
	
	//   var flgDashboard =  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000);
	//   appLog("Dashboard : "+flgDashboard);
	//   if(flgDashboard){
	//     appLog("Nothing to Do");
	//   }
	//   //Accounts Related
	//   var flgAccountsDetails =  await kony.automation.playback.waitFor(["frmAccountsDetails"],5000);
	//   appLog("flgAccountsDetails : "+flgAccountsDetails);
	//   if(flgAccountsDetails){
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Transfers Related
	//   var flgFastTransfers =  await kony.automation.playback.waitFor(["frmFastTransfers"],5000);
	//   appLog("FastTransfers : "+flgFastTransfers);
	//   if(flgFastTransfers){
	//     kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgReview =  await kony.automation.playback.waitFor(["frmReview"],5000);
	//   appLog("frmReview : "+flgReview);
	//   if(flgReview){
	//     kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	//   }
	//   var flgConfirmTransfer =  await kony.automation.playback.waitFor(["frmConfirmTransfer"],5000);
	//   appLog("frmConfirmTransfer : "+flgConfirmTransfer);
	//   if(frmConfirmTransfer){
	//     kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastTransfersActivites =  await kony.automation.playback.waitFor(["frmFastTransfersActivites"],5000);
	//   appLog("flgFastTransfersActivites : "+flgFastTransfersActivites);
	//   if(flgFastTransfersActivites){
	//     kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	//   }
	
	//   //Manage payee Related
	//   var flgFastManagePayee =  await kony.automation.playback.waitFor(["frmFastManagePayee"],5000);
	//   appLog("flgFastManagePayee : "+flgFastManagePayee);
	//   if(flgFastManagePayee){
	//     kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastP2P =  await kony.automation.playback.waitFor(["frmFastP2P"],5000);
	//   appLog("flgFastP2P : "+flgFastP2P);
	//   if(flgFastP2P){
	//     kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	//   }
	
	//   //BillPay Related
	
	//   var flgBulkPayees =  await kony.automation.playback.waitFor(["frmBulkPayees"],5000);
	//   appLog("flgBulkPayees : "+flgBulkPayees);
	//   if(flgBulkPayees){
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   } 
	
	//   var flgMakeOneTimePayee =  await kony.automation.playback.waitFor(["frmMakeOneTimePayee"],5000);
	//   appLog("flgMakeOneTimePayee : "+flgMakeOneTimePayee);
	//   if(flgMakeOneTimePayee){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgMakeOneTimePayment =  await kony.automation.playback.waitFor(["frmMakeOneTimePayment"],5000);
	//   appLog("flgMakeOneTimePayment : "+flgMakeOneTimePayment);
	//   if(flgMakeOneTimePayment){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentConfirm =  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm"],5000);
	//   appLog("flgOneTimePaymentConfirm : "+flgOneTimePaymentConfirm);
	//   if(flgOneTimePaymentConfirm){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentAcknowledgement =  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],5000);
	//   appLog("flgOneTimePaymentAcknowledgement : "+flgOneTimePaymentAcknowledgement);
	//   if(flgOneTimePaymentAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgManagePayees =  await kony.automation.playback.waitFor(["frmManagePayees"],5000);
	//   appLog("flgManagePayees : "+flgManagePayees);
	//   if(flgManagePayees){
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayABill =  await kony.automation.playback.waitFor(["frmPayABill"],5000);
	//   appLog("flgPayABill : "+flgPayABill);
	//   if(flgPayABill){
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillConfirm =  await kony.automation.playback.waitFor(["frmPayBillConfirm"],5000);
	//   appLog("flgPayBillConfirm : "+flgPayBillConfirm);
	//   if(flgPayBillConfirm){
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillAcknowledgement =  await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],5000);
	//   appLog("flgPayBillAcknowledgement : "+flgPayBillAcknowledgement);
	//   if(flgPayBillAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayScheduled =  await kony.automation.playback.waitFor(["frmBillPayScheduled"],5000);
	//   appLog("flgBillPayScheduled : "+flgBillPayScheduled);
	//   if(flgBillPayScheduled){
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayee1 =  await kony.automation.playback.waitFor(["frmAddPayee1"],5000);
	//   appLog("flgAddPayee1 : "+flgAddPayee1);
	//   if(flgAddPayee1){
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayeeInformation =  await kony.automation.playback.waitFor(["frmAddPayeeInformation"],5000);
	//   appLog("flgAddPayeeInformation : "+flgAddPayeeInformation);
	//   if(flgAddPayeeInformation){
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeDetails =  await kony.automation.playback.waitFor(["frmPayeeDetails"],5000);
	//   appLog("flgPayeeDetails : "+flgPayeeDetails);
	//   if(flgPayeeDetails){
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgVerifyPayee =  await kony.automation.playback.waitFor(["frmVerifyPayee"],5000);
	//   appLog("flgVerifyPayee : "+flgVerifyPayee);
	//   if(flgVerifyPayee){
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeAcknowledgement =  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement"],5000);
	//   appLog("flgPayeeAcknowledgement : "+flgPayeeAcknowledgement);
	//   if(flgPayeeAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayHistory =  await kony.automation.playback.waitFor(["frmBillPayHistory"],5000);
	//   appLog("flgBillPayHistory : "+flgBillPayHistory);
	//   if(flgBillPayHistory){
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayActivation =  await kony.automation.playback.waitFor(["frmBillPayActivation"],5000);
	//   appLog("flgBillPayActivation : "+flgBillPayActivation);
	//   if(flgBillPayActivation){
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }
	
	
	//   //Messages Related
	
	
	//   var flgNotificationsAndMessages =  await kony.automation.playback.waitFor(["frmNotificationsAndMessages"],5000);
	//   appLog("flgNotificationsAndMessages : "+flgNotificationsAndMessages);
	//   if(flgNotificationsAndMessages){
	//     kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Post Login Related
	
	//   var flgOnlineHelp =  await kony.automation.playback.waitFor(["frmOnlineHelp"],5000);
	//   appLog("flgOnlineHelp : "+flgOnlineHelp);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	//   var flgContactUsPrivacyTandC =  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC"],5000);
	//   appLog("flgContactUsPrivacyTandC : "+flgContactUsPrivacyTandC);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Settings Related
	
	//   var flgProfileManagement =  await kony.automation.playback.waitFor(["frmProfileManagement"],5000);
	//   appLog("flgProfileManagement : "+flgProfileManagement);
	//   if(flgProfileManagement){
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	
	// });
	
	
	async function verifyAccountsLandingScreen(){
	
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
	}
	
	async function SelectAccountsOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	
	  var Status=false;
	  
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
	        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(5000);
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	  
	  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnFirstCheckingAccount(){
	
	  appLog("Intiated method to click on First Checking account");
	  SelectAccountsOnDashBoard("Checking");
	  //appLog("Successfully Clicked on First Checking account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstSavingsAccount(){
	
	  appLog("Intiated method to click on First Savings account");
	  SelectAccountsOnDashBoard("Saving");
	  //appLog("Successfully Clicked on First Savings account");
	  //await kony.automation.playback.wait(5000);
	}
	
	
	async function clickOnFirstCreditCardAccount(){
	
	  appLog("Intiated method to click on First CreditCard account");
	  SelectAccountsOnDashBoard("Credit");
	  //appLog("Successfully Clicked on First CreditCard account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstDepositAccount(){
	
	  appLog("Intiated method to click on First Deposit account");
	  SelectAccountsOnDashBoard("Deposit");
	  //appLog("Successfully Clicked on First Deposit account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstLoanAccount(){
	
	  appLog("Intiated method to click on First Loan account");
	  SelectAccountsOnDashBoard("Loan");
	  //appLog("Successfully Clicked on First Loan account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnSearch_AccountDetails(){
	
	  appLog("Intiated method to click on Search Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Search Icon");
	}
	
	async function selectTranscationtype(TransactionType){
	
	  appLog("Intiated method to select Transcation type");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],TransactionType);
	  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
	}
	
	async function selectAmountRange(From,To){
	
	  appLog("Intiated method to select Amount Range");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],From);
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],To);
	
	  appLog("Successfully selected amount Range : ["+From+","+To+"]");
	}
	
	async function selectCustomdate(){
	
	  appLog("Intiated method to select Custom Date Range");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
	  appLog("Successfully selected Date Range");
	}
	
	async function clickOnbtnSearch(){
	
	  appLog("Intiated method to click on Search Button with given search criteria");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully Clicked on Search Button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function validateSearchResult() {
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	}
	
	async function MoveBackToLandingScreen_AccDetails(){
	
	  appLog("Move back to Account Dashboard from AccountsDetails");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	async function SelectContextualOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	  
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	  
	  expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnCheckingAccountContextMenu(){
	
	  appLog("Intiated method to click on Checking account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,0]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
	  //   appLog("Successfully clicked on Checking account context Menu");
	
	  SelectContextualOnDashBoard("Checking");
	}
	
	async function clickOnSavingsAccountContextMenu(){
	
	  appLog("Intiated method to click on Saving account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,1]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,1]","flxMenu"]);
	  //   appLog("Successfully clicked on Saving account context Menu");
	  SelectContextualOnDashBoard("Saving");
	}
	
	async function clickOnCreditCardAccountContextMenu(){
	
	  appLog("Intiated method to click on CreditCard account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,2]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,2]","flxMenu"]);
	  //   appLog("Successfully clicked on CreditCard account context Menu");
	
	  SelectContextualOnDashBoard("Credit");
	}
	
	async function clickOnDepositAccountContextMenu(){
	
	  appLog("Intiated method to click on Deposit account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,3]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,3]","flxMenu"]);
	  //   appLog("Successfully clicked on Deposit account context Menu");
	
	  SelectContextualOnDashBoard("Deposit");
	}
	
	async function clickOnLoanAccountContextMenu(){
	
	  appLog("Intiated method to click on Loan account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[1,0]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxMenu"]);
	  //   appLog("Successfully clicked on Loan account context Menu");
	
	  SelectContextualOnDashBoard("Loan");
	
	}
	
	async function verifyContextMenuOptions(myList_Expected){
	
	  //var myList_Expected = new Array();
	  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	  myList_Expected.push(myList_Expected);
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  var myList = new Array();
	
	  for(var x = 0; x <segLength-1; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+options);
	    myList.push(options);
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	
	  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	  //appLog("isFounded"+isFounded);
	  expect(isFounded).toBe(true);
	}
	async function MoveBackToLandingScreen_Accounts(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	}
	
	async function scrolltoTranscations_accountDetails(){
	
	  appLog("Intiated method to scroll to Transcations under account details");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	}
	
	async function verifyAccountSummary_CheckingAccounts(){
	
	  appLog("Intiated method to verify account summary for Checking Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_DepositAccounts(){
	
	  appLog("Intiated method to verify account summary for Deposit Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_CreditCardAccounts(){
	
	  appLog("Intiated method to verify account summary for CreditCard Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	
	}
	
	async function verifyAccountSummary_LoanAccounts(){
	
	  appLog("Intiated method to verify account summary for Loan Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_SavingsAccounts(){
	
	  appLog("Intiated method to verify account summary for Savings Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountsOrder_DashBoard(){
	
	  appLog("Intiated method to verify accounts order in Dashboard");
	
	  //Accounts Order can't be garunteed across different users. Hence checking all Types of accounts.
	  var myList = new Array();
	  var myList_Expected = new Array();
	  myList_Expected.push("Checking","Saving","Credit","Deposit","Loan");
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  for(var x = 0; x <segLength; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	      var seg="segAccounts["+x+","+y+"]";
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      myList.push(accountName);
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	}
	
	
	async function VerifyAccountOnDashBoard(AccountType){
	
	  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        appLog("Successfully verified : <b>"+accountName+"</b>");
	        myList.push("TRUE");
	        finished = true;
	        break;
	      }else{
	        myList.push("FALSE");
	      }
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	async function VerifyCheckingAccountonDashBoard(){
	
	  appLog("Intiated method to verify Checking account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
	  VerifyAccountOnDashBoard("Checking");
	}
	
	async function VerifySavingsAccountonDashBoard(){
	
	  appLog("Intiated method to verify Savings account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
	  VerifyAccountOnDashBoard("Saving");
	}
	async function VerifyCreditCardAccountonDashBoard(){
	
	  appLog("Intiated method to verify CreditCard account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
	  VerifyAccountOnDashBoard("Credit");
	}
	
	async function VerifyDepositAccountonDashBoard(){
	
	  appLog("Intiated method to verify Deposit account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
	  VerifyAccountOnDashBoard("Deposit");
	}
	
	async function VerifyLoanAccountonDashBoard(){
	
	  appLog("Intiated method to verify Loan account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
	  VerifyAccountOnDashBoard("Loan");
	}
	
	async function verifyViewAllTranscation(){
	
	  appLog("Intiated method to view all Tranx in AccountDetails");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	}
	
	async function verifyAdvancedSearch_AccountDetails(AmountRange1,AmountRange2){
	
	  appLog("Intiated method to verify Advanced Search in AccountDetails");
	
	  //   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnAll"],15000);
	  //   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnAll"]);
	  //   appLog("Successfully clicked on All button under AccountDetails");
	  //   await kony.automation.playback.wait(5000);
	
	  appLog("Intiated method to click on Seach Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Seach Icon");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], "Transfers");
	  appLog("Successfully selected Transcation Type");
	
	  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
	  appLog("Successfully selected amount range From");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
	  appLog("Successfully selected amount range To");
	
	  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	  appLog("Successfully selected date range");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	
	}
	
	
	async function selectContextMenuOption(Option){
	
	  appLog("Intiated method to select context menu option :: "+Option);
	
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment will be :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+TransfersText);
	
	    if(TransfersText===Option){
	      appLog("Option to be selected is :"+TransfersText);
	      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
	      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
	      await kony.automation.playback.wait(5000);
	      myList.push("TRUE");
	      break;
	    }else{
	      myList.push("FALSE");
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	
	async function verifyVivewStatementsHeader(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");
	
	}
	
	
	async function navigateToBillPay(){
	
	  appLog("Intiated method to navigate to BillPay");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	  appLog("Successfully clicked on BillPay tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function navigateToOneTimePayment(){
	
	  //await kony.automation.playback.waitFor(["frmBulkPayees","flxMakeOneTimePayment"],15000);
	  //kony.automation.flexcontainer.click(["frmBulkPayees","flxMakeOneTimePayment"]);
	
	  //Navigating using Menu
	  appLog("Intiated method to navigate to OneTimePayment");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  appLog("Successfully clicked on Menu on Dashboard");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"]);
	  appLog("Successfully clicked on Billpay option");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPay4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPay4flxMyAccounts"]);
	  appLog("Successfully clicked on OneTimePayment option");
	  await kony.automation.playback.wait(15000);
	
	}
	
	async function enterOneTimePayeeInformation(payeeName,zipcode,accno,accnoAgain,mobileno){
	
	  appLog("Intiated method to enter OneTime Payee Information");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],payeeName);
	  appLog("Successfully entered payee name to auto select : <b>"+payeeName+"</b>");
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"],15000);
	  kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);
	  appLog("Successfully selected payee name from list");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],zipcode);
	  appLog("Successfully entered zipcode : <b>"+zipcode+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],accno);
	  appLog("Successfully entered acc number : <b>"+accno+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],accnoAgain);
	  appLog("Successfully Re-entered account number : <b>"+accnoAgain+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],mobileno);
	  appLog("Successfully entered mobile number : <b>"+mobileno+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"],15000);
	  kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Next button");
	}
	
	async function enterOneTimePaymentdetails(amount,note){
	
	  appLog("Intiated method to enter details for OneTime payment");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],amount);
	  appLog("Successfully entered amount : <b>"+amount+"</b>");
	
	  appLog("Intiated method to Select Payee From Acc for OneTime payment");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmMakeOneTimePayment","txtTransferFrom"], [264,20],null,null);
	  kony.automation.flexcontainer.click(["frmMakeOneTimePayment","segTransferFrom[0,0]","flxAccountListItem"]);
	  appLog("Successfully selected Bill PayFrom");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],note);
	  appLog("Successfully entered note value : <b>"+note+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"],15000);
	  kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Next button");
	}
	
	async function confirmOneTimePaymnet(){
	
	  appLog("Intiated method to confirm OneTimePayment");
	
	  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	  appLog("Successfully accepted Checkbox");
	
	  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	  
	}
	
	async function verifyOneTimePaymentSuccessMsg(){
	
	  appLog("Intiated method to verify OneTimePayment SuccessMsg");
	  
	  await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],30000);
	
	  if(success){
	    //await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],15000);
	    //expect(kony.automation.widget.getWidgetProperty(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","rtxDowntimeWarning"],5000)){
	    //appLog("Logged in User is not authorized to perform this action");
	    //fail('Logged in User is not authorized to perform this action');
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmMakeOneTimePayment","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmMakeOneTimePayment","rtxDowntimeWarning"],"text"));
	
	
	    await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else{
	    appLog("Unable to perform OneTimePayment");
	  }
	
	}
	
	
	async function navigateToManagePayee(){
	
	  await navigateToBillPay();
	  appLog("Intiated method to navigate to Manage Payee list");
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	  appLog("Successfully clicked on Manage payee Button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function selectPayee_ManagePayeeList(payeename){
	
	  appLog("Intiated method to select Payee from Manage Payee list : <b>"+payeename+"</b>");
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmManagePayees","manageBiller","txtSearch"],payeename);
	  appLog("Successfully entered Payee "+payeename);
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","btnConfirm"],15000);
	  kony.automation.flexcontainer.click(["frmManagePayees","manageBiller","btnConfirm"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	
	  //   await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","txtSearch"],15000);
	  //   kony.automation.textbox.enterText(["frmManagePayees","manageBiller","txtSearch"], [ { modifierCapsLock:true, key : 'A' },
	  //                                                             { modifierCapsLock:true, key : 'B' },
	  // 															{ modifierCapsLock:true, key : 'C' },
	  //                                                             { modifierCapsLock:false, keyCode : 13 }
	  // 														]);
	
	  appLog("Intiated Method to verify Payee <b>"+payeename+"</b>");
	
	  var PayeeList=await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  if(PayeeList){
	    //expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","lblColumn1"],"text")).toEqual(payeename);
	    expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","lblColumn1"],"text")).not.toBe('');
	  }else if(await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],5000)){
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],"text"));
	
	  }else{
	    appLog("Unable to find Payee in ManagePayees List");
	  }
	
	}
	
	async function clickOnBillPayBtn_ManagePayees(){
	
	
	  // BillPay and Active ebill has same locator hence verifying text and doing operation accordingly, Instead of directly failing.
	  
	  appLog("Intiated method to click on Billpay button from Manage Payee list");
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	
	  var ButtonName=kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"], "text");
	
	  //appLog('Button Name is : '+ButtonName);
	  
	  if(ButtonName==='Activate ebill'){
	
	    appLog("Info : <b>"+ButtonName+"</b>"+" is Available instead of BillPay button");
	    //Activate e Bill to convert button to PayaBill. instead of failing we can proceed execution
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    await kony.automation.playback.waitFor(["frmManagePayees","btnProceedIC"],15000);
	    kony.automation.button.click(["frmManagePayees","btnProceedIC"]);
	    appLog('Successfully clicked on YES button');
	    await kony.automation.playback.wait(10000);
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    appLog("Successfully clicked on BillPay button");
	
	  }else{
	    
	    // We can directly click on BillPay button
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    appLog("Successfully clicked on BillPay button");
	    await kony.automation.playback.wait(5000);
	  }
	}
	
	
	async function enterAmount_SheduleBillPay(amount){
	
	  appLog("Intiated method to enter amount : <b>"+amount+"</b>");
	  await kony.automation.playback.waitFor(["frmPayABill","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmPayABill","txtSearch"],amount);
	  appLog("Successfully entered amount : <b>"+amount+"</b>");
	
	  await SelectPayFromAcc_SheduleBillPay();
	}
	
	async function SelectPayFromAcc_SheduleBillPay(){
	
	  appLog("Intiated method to Select Payee From");
	
	  await kony.automation.playback.waitFor(["frmPayABill","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmPayABill","txtTransferFrom"], [600,17],null,null);
	  kony.automation.flexcontainer.click(["frmPayABill","segTransferFrom[0,0]","flxAccountListItem"]);
	
	  appLog("Successfully selected Payee from the list");
	}
	
	async function selectfrequency_SheduledBillPay(freq){
	
	  appLog("Intiated method to select freq : <b>"+freq+"</b>");
	  await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"],15000);
	  kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"],freq);
	  appLog("Successfully selected freq : "+freq);
	}
	
	async function SelectDateRange_SheduledBillpay() {
	
	  //new chnage in 202010
	  //await kony.automation.playback.wait(5000);
	  appLog("Intiated method to select DateRange");
	  await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"],15000);
	  kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "ON_SPECIFIC_DATE");
	
	  await kony.automation.playback.waitFor(["frmPayABill","calSendOn"],15000);
	  kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	  appLog("Successfully selected sendOn Date");
	  await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"],15000);
	  kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,21,2021]);
	  appLog("Successfully selected EndOn Date");
	}
	
	async function SelectSendOnDate_SheduledBillpay() {
	
	  await kony.automation.playback.waitFor(["frmPayABill","calSendOn"],15000);
	  kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	  appLog("Successfully selected sendOn Date");
	}
	
	async function SelectOccurences_SheduledBillPay(occurences) {
	  //new chnage in 202010
	  appLog("Intiated method to select N.of Occurences");
	  await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"],15000);
	  kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "NO_OF_RECURRENCES");
	  await kony.automation.playback.waitFor(["frmPayABill","txtEndingOn"],15000);
	  kony.automation.textbox.enterText(["frmPayABill","txtEndingOn"],occurences);
	  appLog("Successfully selected Occurences : <b>"+occurences+"</b>");
	}
	
	async function EnterNoteValue_SheduledBillPay(notes) {
	
	  appLog("Intiated method to enter note value");
	  await kony.automation.playback.waitFor(["frmPayABill","txtNotes"],15000);
	  kony.automation.textbox.enterText(["frmPayABill","txtNotes"],notes);
	  appLog("Successfully entered Note value : <b>"+notes+"</b>");
	
	  appLog("Intiated method to click on Confirm button");
	  await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"],15000);
	  kony.automation.button.click(["frmPayABill","btnConfirm"]);
	  appLog("Successfully clicked on Confirm button");
	}
	
	async function confirmSheduledBillpay(){
	
	  appLog("Intiated method to Confirm Sheduled BillPayment");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	  appLog("Successfully accepted terms check box");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	  appLog("Successfully clicked on Confirm button");
	}
	
	async function cancelSheduledBillPay(){
	
	  appLog("Intiated method to CANCEL Sheduled BillPayment");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","btnCancel"],15000);
	  kony.automation.button.click(["frmPayBillConfirm","btnCancel"]);
	  appLog("Successfully clicked on Cancel button");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to cancel this transaction?");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","btnYes"],15000);
	  kony.automation.button.click(["frmPayBillConfirm","CancelPopup","btnYes"]);
	  appLog("Successfully clicked on YES button");
	
	  await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	  appLog("Successfully MovedBack to Account DashBoard");
	}
	
	async function verifySheduledBillpaySuccessMsg(){
	
	  appLog("Intiated method to verify Sheduled BillPay SuccessMsg");
	  
	  await kony.automation.playback.wait(5000);
	  var Success= await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],30000);
	  
	  if(Success){
	  //expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).not.toBe('');
	  await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	  appLog("Successfully MovedBack to Account DashBoard");
	  }else if(await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000)){
	    //Checking for exception message
	    //Move back to dashboard again there is an exception message
	    appLog("Exception while performing a Sheduled BillPay");
	    await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	    await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	    expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	    
	    //appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	    //fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	    appLog("Failed : Unable to Perform Successfull Transcation. Failed with rtxDowntimeWarning");
	    fail("Failed : Unable to Perform Successfull Transcation. Failed with rtxDowntimeWarning");
	  }else{
	     appLog("Unable to verify Success Message");
	  }
	
	}
	
	async function navigateToSheduledBillPay(){
	
	  await navigateToBillPay();
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnScheduled"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnScheduled"]);
	  appLog("Successfully clicked on Sheduled tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickOnEditButton_SheduledBillPayment(){
	
	  appLog("Intiated method to click on Edit button");
	  await kony.automation.playback.waitFor(["frmBillPayScheduled","segmentBillpay"],15000);
	  kony.automation.button.click(["frmBillPayScheduled","segmentBillpay[0]","btnEdit"])
	  appLog("Successfully clicked on Edit button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmPayABill","lblPayABill"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayABill","lblPayABill"],"text")).toEqual("Pay a Bill");
	
	}
	
	async function UpdatedSheduledBillPayment(notes){
	
	  await SelectPayFromAcc_SheduleBillPay();
	  await selectfrequency_SheduledBillPay("Once");
	  await EnterNoteValue_SheduledBillPay(notes);
	  await confirmSheduledBillpay();
	
	}
	async function EditSheduledBillPay(notes){
	
	  var nopayments=await kony.automation.playback.waitFor(["frmBillPayScheduled","rtxNoPaymentMessage"],15000);
	  if(nopayments){
	    appLog("There are no sheduled payments");
	    //Move back to accounts
	    await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],15000);
	    kony.automation.button.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	    appLog("Successfully MovedBack to Account DashBoard");
	  }else{
	
	    appLog("There are few sheduled payments");
	    await clickOnEditButton_SheduledBillPayment();
	    await UpdatedSheduledBillPayment(notes);
	    var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000);
	    if(warning){
	      await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	      await verifyAccountsLandingScreen();
	      appLog("Successfully MovedBack to Account DashBoard");
	      //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	      appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	      fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	
	    }else{
	      await verifySheduledBillpaySuccessMsg();
	      await verifyAccountsLandingScreen();
	    }  
	
	  }
	}
	
	async function clickOnAddPayeeLink(){
	
	  appLog("Intiated method to click on Add payee link");
	  await kony.automation.playback.waitFor(["frmBulkPayees","flxAddPayee"],15000);
	  kony.automation.flexcontainer.click(["frmBulkPayees","flxAddPayee"]);
	  appLog("Successfully Clicked on addPayee link");
	}
	
	async function enterPayeeDetails_UsingPayeeinfo(payeeName,address1,address2,city,zipcode,accno,note){
	
	  appLog("Intiated method to Add Payee Details");
	
	  await kony.automation.playback.waitFor(["frmAddPayee1","btnEnterPayeeInfo"],15000);
	  kony.automation.button.click(["frmAddPayee1","btnEnterPayeeInfo"]);
	  appLog("Successfully Clicked on EnterPayeeInfo Tab");
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterName"],15000);
	  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterName"],payeeName);
	  appLog("Successfully Entered Payee name as : <b>"+payeeName+"</b>");
	  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddress"],15000);
	  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddress"],address1);
	  appLog("Successfully Entered Address Line1 as : <b>"+address1+"</b>");
	  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddressLine2"],address2);
	  appLog("Successfully Entered Address Line2 as : <b>"+address2+"</b>");
	  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxCity"],city);
	  appLog("Successfully Entered CityName as : <b>"+city+"</b>");
	  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterZipCode"],15000);
	  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterZipCode"],zipcode);
	  appLog("Successfully Entered Zipcode as : <b>"+zipcode+"</b>");
	  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAccountNmber"],15000);
	  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAccountNmber"],accno);
	  appLog("Successfully Entered account number as : <b>"+accno+"</b>");
	  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxConfirmAccNumber"],15000);
	  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxConfirmAccNumber"],accno);
	  appLog("Successfully Re-Entered account number as : <b>"+accno+"</b>");
	  
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","flxClick"],15000);
	//   kony.automation.flexcontainer.click(["frmAddPayeeInformation","flxClick"]);
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxAdditionalNote"],15000);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxAdditionalNote"],note);
	//   appLog("Successfully Entered Note value as : <b>"+note+"</b>");
	  
	  await kony.automation.playback.waitFor(["frmAddPayeeInformation","btnReset"],15000);
	  kony.automation.button.click(["frmAddPayeeInformation","btnReset"]);
	  appLog("Successfully Clicked on Next button ");
	}
	
	async function clickOnNextButton_payeeDetails(){
	
	  appLog("Intiated method verify Payee Details");
	  await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"],15000);
	  kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	  appLog("Successfully Clicked on Next button ");
	
	  await linkPayee();
	}
	
	async function SelectPayeeBankingType_payeeDetails(BankingType){
	
	  appLog("Intiated method to click on AddRecepientContinue");
	  var btnAddRecepient=await kony.automation.playback.waitFor(["frmPayeeDetails","btnAddRecepientContinue"],15000);
	  if(btnAddRecepient){
	    kony.automation.button.click(["frmPayeeDetails","btnAddRecepientContinue"]);
	    appLog("Successfully Clicked on AddRecepientContinue button ");
	    await kony.automation.playback.wait(5000);
	  }else{
	    appLog("Selecting Banking type screen is not available");
	  }
	
	}
	
	async function linkPayee(){
	
	  var linkreciptent=await kony.automation.playback.waitFor(["frmPayeeDetails","contractList","lblHeader"],15000);
	
	  if(linkreciptent){
	    kony.automation.widget.touch(["frmPayeeDetails","contractList","lblCheckBoxSelectAll"], [8,6],null,null);
	    appLog("Successfully selected select All CheckBox");
	    await kony.automation.playback.waitFor(["frmPayeeDetails","contractList","flxCol4"],15000);
	    kony.automation.flexcontainer.click(["frmPayeeDetails","contractList","flxCol4"]);
	    await kony.automation.playback.waitFor(["frmPayeeDetails","contractList","btnAction6"],15000);
	    kony.automation.button.click(["frmPayeeDetails","contractList","btnAction6"]);
	    appLog("Successfully Clicked on Link Reciptent Continue Button");
	
	  }
	}
	
	async function clickOnConfirmButton_verifyPayee(){
	
	  appLog("Intiated method to confirm Payee Details");
	  await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"],15000);
	  kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm button ");
	}
	
	async function verifyAddPayeeSuccessMsg(){
	
	  appLog("Intiated method to verify Add payee SuccessMsg");
	  await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],30000);
	  
	  if(success){
	    await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],15000);
	    expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	    
	  }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","rtxDowntimeWarning"],5000)){
	    
	    appLog("Intiated method to verify DowntimeWarning");
	    await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	    
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmAddPayeeInformation","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmAddPayeeInformation","rtxDowntimeWarning"],"text"));
	
	  }else{
	    appLog("Unable to add Payee");
	  }
	
	}
	
	
	async function expandPayee_ManagePayee(){
	
	  appLog("Intiated method to Expand payee from Manage payee");
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  kony.automation.flexcontainer.click(["frmManagePayees","manageBiller","segmentBillPay[0]","flxDropdown"]);
	  appLog("Successfully clicked on Manage Payees dropdown arrow");
	}
	
	async function MoveBackToDashBoard_ManagePayees(){
	
	  await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	async function clickOnEditBtn_ManagePayees(){
	
	  await expandPayee_ManagePayee();
	  appLog("Intiated method to Edit Biller");
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btn3"]);
	  appLog("Successfully clicked on Editbutton under manage payee");
	}
	
	async function deletePayee_ManagePayee(){
	
	  appLog("Intiated method to Delete Payee");
	  await expandPayee_ManagePayee();
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btn4"]);
	  appLog("Successfully clicked on Delete button under manage payee");
	  await kony.automation.playback.waitFor(["frmManagePayees","btnYesIC"],15000);
	  kony.automation.button.click(["frmManagePayees","btnYesIC"]);
	  appLog("Successfully clicked on YES button on delete biller");
	  await kony.automation.playback.wait(5000);
	  await MoveBackToDashBoard_ManagePayees();
	}
	
	async function EditPayee_ManagePayee(){
	
	  appLog("Intiated method to Edit Payee");
	
	  await clickOnEditBtn_ManagePayees();
	
	  appLog("Intiated method to updated biller Zipcode");
	  await kony.automation.playback.waitFor(["frmManagePayees","tbxZipCode"],15000);
	  kony.automation.textbox.enterText(["frmManagePayees","tbxZipCode"],"123456");
	  appLog("Successfully Updated biller zipcode");
	
	  appLog("Intiated method to click on Continue button");
	  await kony.automation.playback.waitFor(["frmManagePayees","btnContinue"],15000);
	  kony.automation.button.click(["frmManagePayees","btnContinue"]);
	  appLog("Successfully Clicked on Continue button")
	
	  appLog("Intiated method to click on Savelink Continue button");
	  await kony.automation.playback.waitFor(["frmManagePayees","contractList","btnAction6"],15000);
	  kony.automation.button.click(["frmManagePayees","contractList","btnAction6"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Savelink Continue button");
	
	  await verifyUpdatePayeeSuccessMsg();
	
	}
	
	
	async function verifyUpdatePayeeSuccessMsg(){
	
	  appLog("Intiated method to verify Update payee SuccessMsg");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).not.toBe('');
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	
	}
	
	
	async function navigateToPastBillPay(){
	
	  appLog("Intiated method to navigate to Billpay History");
	  await navigateToBillPay();
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnHistory"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnHistory"]);
	  appLog("Successfully clicked on History tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickonRepeatButton_PastBillpay(){
	
	  appLog("Intiated method to click on Repeat button");
	  await kony.automation.playback.waitFor(["frmBillPayHistory","segmentBillpay"],15000);
	  kony.automation.button.click(["frmBillPayHistory","segmentBillpay[0]","btnRepeat"]);
	  appLog("Successfully clicked on Repeat tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function repeatPastBillPayment(note){
	
	  appLog("Intiated method to Repeat a BillPay");
	
	  var nopayments=await kony.automation.playback.waitFor(["frmBillPayHistory","rtxNoPaymentMessage"],15000);
	
	  if(nopayments){
	    appLog("There are no History payments");
	    //Move back to accounts
	    await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],15000);
	    kony.automation.button.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else{
	
	    await clickonRepeatButton_PastBillpay();
	    await SelectPayFromAcc_SheduleBillPay();
	    await EnterNoteValue_SheduledBillPay(note);
	    await confirmSheduledBillpay();
	
	    var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000);
	    if(warning){
	      await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	      await verifyAccountsLandingScreen();
	      appLog("Successfully Moved back to Accounts dashboard");
	      //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	      appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	      fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	
	
	    }else{
	      await verifySheduledBillpaySuccessMsg();
	      await verifyAccountsLandingScreen();
	      appLog("Successfully Moved back to Accounts dashboard");
	    }
	
	  }
	}
	
	async function clickOnAllpayeesTab(){
	
	  appLog("Intiated method to click on Allpayees tab");
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnAllPayees"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnAllPayees"]);
	  appLog("Successfully clicked on Allpayees tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function verifyAllPayeesList(){
	
	  appLog("Intiated method to verify Allpayees List");
	
	  var PayeeList=await kony.automation.playback.waitFor(["frmBulkPayees","segmentBillpay"],15000);
	
	  if(PayeeList){
	    kony.automation.flexcontainer.click(["frmBulkPayees","segmentBillpay[0]","flxDropdown"]);
	    appLog("Successfully verified on Allpayees List");
	  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","rtxNoPaymentMessage"],5000)){
	
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmBulkPayees","rtxNoPaymentMessage"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmBulkPayees","rtxNoPaymentMessage"],"text"));
	
	  }else {
	    appLog("Unable to verify Allpayees List");
	  }
	
	}
	
	async function MoveBackToDashBoard_AllPayees(){
	
	  await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	}
	
	async function clickOnSavePayeeButton_OneTimePay(){
	
	  appLog("Intiated method to Save Payee from OneTime Payment");
	
	  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","btnSavePayee"],15000);
	  kony.automation.button.click(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
	  appLog("Successfully Clicked on Save button");
	
	  //Continue Button
	  await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"],15000);
	  kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	  appLog("Successfully Clicked on Continue button");
	
	  //Confirm Button
	  await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"],15000);
	  kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm button");
	
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAddPayee"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAddPayee"],"text")).toEqual("Add Payee");
	
	
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
	  appLog("Successfully verified Added payee");
	
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","btnViewAllPayees"],15000);
	  kony.automation.button.click(["frmPayeeAcknowledgement","btnViewAllPayees"]);
	  appLog("Successfully clicked on ViewAll payees button");
	
	}
	
	async function activateBillPayTermsconditions(){
	
	  appLog("Intiated method to Activate Billpay TC's");
	
	  var warning=await kony.automation.playback.waitFor(["frmBillPayActivation","lblWarning"],15000);
	  if(warning){
	    //expect(kony.automation.widget.getWidgetProperty(["frmBillPayActivation","lblWarning"], "text")).toEqual("Please activate My Bills.");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","listbxAccountType"],15000);
	    kony.automation.listbox.selectItem(["frmBillPayActivation","listbxAccountType"], "160128223241511");
	    appLog("Successfully Selected Default BillPay Acc");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","lblFavoriteEmailCheckBox"],15000);
	    kony.automation.widget.touch(["frmBillPayActivation","lblFavoriteEmailCheckBox"], null,null,[14,13]);
	    appLog("Successfully accepted checkbox");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","flxAgree"],15000);
	    kony.automation.flexcontainer.click(["frmBillPayActivation","flxAgree"]);
	    appLog("Successfully clicked on AgreeFlex");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","btnProceed"],15000);
	    kony.automation.button.click(["frmBillPayActivation","btnProceed"]);
	    appLog("Successfully clicked on Proceed button");
	    var error=await kony.automation.playback.waitFor(["frmBillPayActivation","rtxErrorMessage"],15000);
	    if(error){
	      //expect(kony.automation.widget.getWidgetProperty(["frmBillPayActivation","rtxErrorMessage"], "text")).toEqual("Update operation failed on  customerpreference:No value specified for parameter 2");
	      await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	    }
	  }else{
	    appLog("Already accepted billpay activation..Moveback to dashboard");
	    await MoveBackToDashBoard_AllPayees();
	  }
	}
	
	async function activateNewlyAddedpayee(){
	
	  appLog('Intiated method to activate Newly Added Payee');
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	
	  var ButtonName=kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"], "text");
	
	  //appLog('Button Name is : '+ButtonName);
	
	  if(ButtonName==='Activate ebill'){
	
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    appLog('Successfully clicked on activate button');
	
	    var activate=await kony.automation.playback.waitFor(["frmManagePayees","lblWarningOneIC"],15000);
	    if(activate){
	      await kony.automation.playback.waitFor(["frmManagePayees","btnProceedIC"],15000);
	      kony.automation.button.click(["frmManagePayees","btnProceedIC"]);
	      appLog('Successfully clicked on YES button');
	      await kony.automation.playback.wait(10000);
	      await MoveBackToDashBoard_ManagePayees();
	    }else {
	      appLog('Failed : Unable to Activate Added Payee');
	      fail('Failed : Unable to Activate Added Payee');
	    }
	  }else {
	    appLog('Payee Already activated');
	  }
	
	
	}
	
	async function verifyMayBeLater(){
	
	  // Verifying may be later screen
	  appLog('Verifying MayBeLater Popup');
	  var mayBeLater=await kony.automation.playback.waitFor(["frmLogout","CustomFeedbackPopup","btnNo"],5000);
	  if(mayBeLater){
	    kony.automation.button.click(["frmLogout","CustomFeedbackPopup","btnNo"]);
	    await kony.automation.playback.waitFor(["frmLogout","logOutMsg","AlterneteActionsLoginNow"],10000);
	    kony.automation.flexcontainer.click(["frmLogout","logOutMsg","AlterneteActionsLoginNow"]);
	  }
	
	}
	
	async function verifyTermsandConditions(){
	
	  // Verifying Terms and conditions screen 
	  var termsconditions=await kony.automation.playback.waitFor(["frmPreTermsandCondition","flxAgree"],15000);
	
	  appLog('Verifying Terms and conditions');
	  appLog("Is terms and conditions : <b>"+termsconditions+"</b>");
	
	  if(termsconditions){
	
	    kony.automation.widget.touch(["frmPreTermsandCondition","lblFavoriteEmailCheckBox"], null,null,[15,9]);
	    kony.automation.flexcontainer.click(["frmPreTermsandCondition","flxAgree"]);
	
	    await kony.automation.playback.waitFor(["frmPreTermsandCondition","btnProceed"],10000);
	    kony.automation.button.click(["frmPreTermsandCondition","btnProceed"]);
	  }
	
	  await kony.automation.playback.waitFor(["frmDashboard"],15000);
	}
	
	async function verifyLoginFunctionality(userName,passWord){
	
	  // Login to the application
	  appLog('Initiating app login functionality');
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],10000);
	  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxUserName"],userName);
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxPassword"],10000);
	  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxPassword"],passWord);
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","btnLogin"],10000);
	  kony.automation.button.click(["frmLogin","loginComponent","btnLogin"]);
	  appLog('Successfully clicked on Sign in Button');
	  
	}
	
	
	
	async function NavigateToManageRecipitents(){
	
	  appLog("Intiated method to navigate to ManageRecipitents");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Clicked on ManageRecipients button");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","lblTitle"],20000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","BeneficiaryList","lblTitle"],"text")).toEqual("Manage Recipients");
	  appLog("Successfully verified ManageRecipients Header");
	}
	
	async function clickOnExternalRecipitentsTab(){
	
	  //External Acc list
	  appLog("Intiated method to navigate to External Reciptents");
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnTab1"],15000);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","btnTab1"]);
	  appLog("Clicked on External Recipients tab");
	  await kony.automation.playback.wait(5000);
	  //   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"],15000);
	  //   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	  //   appLog("Successfully Clicked on External Recipient from list");
	}
	async function clickOnP2PRecipitentsTab(){
	
	  //P2P Acc list
	  appLog("Intiated method to navigate to P2P Reciptents");
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnTab2"],15000);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","btnTab2"]);
	  appLog("Clicked on P2P Recipients tab");
	  await kony.automation.playback.wait(5000);
	  //   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"],15000);
	  //   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	  //   appLog("Successfully Clicked on P2P Recipient from list");
	}
	
	async function MoveBacktoDashboard_ManageRecipitent(){
	
	  appLog("Intiated method to Move back Accounts dashboard");
	  await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function clickonAddExternalAccounttab(){
	
	  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow2"],15000);
	  expect(status).toBe(true);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow2"]);
	  appLog("Successfully Clicked on Add External Account Flex");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickonAddinfinityBankAccounttab(){
	
	  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow1"],15000);
	  expect(status).toBe(true);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow1"]);
	  appLog("Successfully Clicked on Add Infinity Account Flex");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickonAddInternationalAccounttab(){
	
	  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow3"],15000);
	  expect(status).toBe(true);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow3"]);
	  appLog("Successfully Clicked on Add International Account Flex");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickonAddP2PAccounttab(){
	
	  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow4"],15000);
	  expect(status).toBe(true);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow4"]);
	  appLog("Successfully Clicked on Add P2P Account Flex");
	  await kony.automation.playback.wait(5000);
	}
	
	async function enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName){
	
	  appLog("Intiated method to add enterExternalBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],Routingno);
	  appLog("Successfully Entered Routing Number : <b>"+Routingno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],Accno);
	  appLog("Successfully Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],Accno);
	  appLog("Successfully Re-Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	}
	
	async function enterInternationalBankAccountDetails(swiftCode,Accno,unique_RecipitentName){
	
	  appLog("Intiated method to add enterInternationalBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],swiftCode);
	  appLog("Successfully Entered SwiftCode : <b>"+swiftCode+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],Accno);
	  appLog("Successfully Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],Accno);
	  appLog("Successfully Re-Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	}
	
	async function enterSameBankAccountDetails(Accno,unique_RecipitentName){
	
	  appLog("Intiated method to add enterSameBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],Accno);
	  appLog("Successfully Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],Accno);
	  appLog("Successfully Re-Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	
	}
	
	async function enterP2PAccountDetails_Email(unique_RecipitentName,email){
	
	  appLog("Intiated method to add enterSameBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","img2"],15000);
	  kony.automation.widget.touch(["frmFastP2P","addBenificiary","img2"], null,null,[10,19]);
	  appLog("Successfully Selected Email Radio button ");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],email);
	  appLog("Successfully Entered Email name : <b>"+email+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	
	}
	
	async function enterP2PAccountDetails_MobileNumber(unique_RecipitentName,phno){
	
	  appLog("Intiated method to add enterSameBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","img1"],15000);
	  kony.automation.widget.touch(["frmFastP2P","addBenificiary","img1"], null,null,[10,19]);
	  appLog("Successfully Selected Email Radio button ");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],phno);
	  appLog("Successfully Entered Email name : <b>"+phno+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	
	}
	
	
	async function linkReciptent(){
	
	  var linkreciptent=await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","lblHeader"],15000);
	
	  if(linkreciptent){
	    kony.automation.widget.touch(["frmFastP2P","addBenificiary","contractList","lblCheckBoxSelectAll"], [8,7],null,null);
	    appLog("Successfully selected Select All CheckBox");
	    await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","flxCol4"],15000);
	    kony.automation.flexcontainer.click(["frmFastP2P","addBenificiary","contractList","flxCol4"]);
	    await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","btnAction6"],15000);
	    kony.automation.button.click(["frmFastP2P","addBenificiary","contractList","btnAction6"]);
	    appLog("Successfully Clicked on Link Reciptent SaveReciptent Button");
	  }
	
	  appLog("Intiated Method to Click on AddAccount Button");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction6"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction6"]);
	  appLog("Successfully Clicked on AddAccount Button");
	}
	
	async function verifyAddingNewReciptientSuccessMsg(){
	
	  var success=await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblSection1Message"],30000);
	  if(success){
	    expect(kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","lblSection1Message"],"text")).not.toBe('');
	    appLog("Successfully verified Newly Added Reciptent");
	  }else if(await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","rtxDowntimeWarning"],5000)){
	    //appLog("Logged in User is not authorized to perform this action");
	    //fail('Logged in User is not authorized to perform this action');
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","rtxDowntimeWarning"],"text"));
	  }
	  await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	
	async function SearchforPayee_External(payeeName){
	
	  appLog("Intiated method to Search for a Payee :: <b>"+payeeName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmFastManagePayee","BeneficiaryList","txtSearch"],payeeName);
	  appLog("Successfully Entered payee name : <b>"+payeeName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnConfirm"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","btnConfirm"]);
	  appLog("Successfully Clicked on Search button");
	  await kony.automation.playback.wait(5000);
	
	}
	
	async function DeleteReciptent(){
	
	  appLog("Intiated method to Delete a Reciptent");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn3"]);
	  appLog("Successfully Clicked on RemoveRecipient button");
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","lblDescriptionIC"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblDescriptionIC"],"text")).not.toBe('');
	  appLog("Successfully Verified RemoveRecipient PopUp Msg");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","btnYesIC"],15000);
	  kony.automation.button.click(["frmFastManagePayee","btnYesIC"]);
	  appLog("Successfully Clicked on RemoveRecipient YES button");
	  //await kony.automation.playback.wait(5000);
	
	}
	
	async function EditReciptent(UniqueUpdatedName,UniqueUpdatedNickName){
	
	  appLog("Intiated method to Edit Reciptent");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn2"]);
	  appLog("Successfully Clicked on EditRecipient button");
	  //await kony.automation.playback.wait(5000);
	
	  // Line Items 3,4, and 5 will be different for External,International and SameBank acc.
	  //More over Searc button is not working hence Iterating over acc and Editing accordingly.
	
	  for(var i=3;i<=5;i++){
	
	    var key="lblDetailKey"+i;
	    var value="lblDetailValue"+i;
	
	    await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary",key],15000);
	    var keyLabel =kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary",key], "text");
	
	    if(keyLabel==='Recipient Name'){
	
	      kony.automation.textbox.enterText(["frmFastP2P","addBenificiary",value],UniqueUpdatedName);
	      appLog("Successfully Updated <b>"+keyLabel+"</b>");
	
	    }else if(keyLabel==='Account Nickname'){
	
	      kony.automation.textbox.enterText(["frmFastP2P","addBenificiary",value],UniqueUpdatedNickName);
	      appLog("Successfully Updated <b>"+keyLabel+"</b>");
	
	    }else{
	      appLog("Select Name or Nick name Text filed to Update");
	    }
	
	  }
	
	  //   if(AccType.toUpperCase() === SAMEBANK){
	
	  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
	  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],UniqueUpdatedName);
	
	  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],UniqueUpdatedNickName);
	
	  //   }else{
	
	  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],UniqueUpdatedName);
	
	  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],UniqueUpdatedNickName);
	  //   }
	
	
	  //Having intermittent issue in Save button
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on SAVE Button");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","btnAction6"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","contractList","btnAction6"]);
	  appLog("Successfully Clicked on Link Reciptent SaveReciptent Button");
	
	}
	
	async function EditP2PReciptent(UniqueUpdatedName,UniqueUpdatedNickName){
	
	  appLog("Intiated method to Edit P2P Reciptent");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn1"]);
	  appLog("Successfully Clicked on EditRecipient button");
	  //await kony.automation.playback.wait(5000);
	
	  appLog("Intiated method to Update Reciptent value");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],UniqueUpdatedName);
	  appLog("Successfully Updated Reciptent name value");
	
	  appLog("Intiated method to Update Reciptent value");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],UniqueUpdatedNickName);
	  appLog("Successfully Updated Reciptent nick name value");
	
	  //Having intermittent issue in Save button
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on SAVE Button");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","btnAction6"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","contractList","btnAction6"]);
	  appLog("Successfully Clicked on Link Reciptent SaveReciptent Button");
	
	}
	
	async function NavigateToMessages(){
	
	  appLog("Intiated method to Navigate to NotficationsAndMessages");
	  
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGESflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGESflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGES1flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ALERTSANDMESSAGES1flxMyAccounts"]);
	  appLog("Successfully Navigated to NotficationsAndMessages");
	  await kony.automation.playback.wait(5000);
	
	}
	
	async function ComposeNewMessage(){
	
	  appLog("Intiated method to Compose a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessage"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessage"]);
	  appLog("Successfully Clicked on NewMessage Button");
	  await kony.automation.playback.wait(5000);
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","listbxCategory"],15000);
	  kony.automation.listbox.selectItem(["frmNotificationsAndMessages","NotficationsAndMessages","listbxCategory"], "RCID_ONLINEBANKING");
	  appLog("Successfully Selected Category");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","tbxSubject"],15000);
	  kony.automation.textbox.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","tbxSubject"],"First Test Message");
	  appLog("Successfully Entered Message subject");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","textareaDescription"],15000);
	  kony.automation.textarea.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","textareaDescription"],"Test Message");
	  appLog("Successfully Entered Message content");
	  
	  //await kony.automation.scrollToWidget(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessageSend"]);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessageSend"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnNewMessageSend"]);
	  appLog("Successfully Clicked on SEND button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function deleteNewMessage(){
	
	  appLog("Intiated method to Delete a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","flxDelete"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","NotficationsAndMessages","flxDelete"]);
	  appLog("Successfully Clicked on Delete button");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","CustomPopup1","btnYes"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","CustomPopup1","btnYes"]);
	  appLog("Successfully Clicked on YES button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function replyNewMessage(){
	
	  appLog("Intiated method to Reply a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnSendReply"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnSendReply"]);
	  appLog("Successfully Clicked on REPLY button");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","txtAreaReply"],15000);
	  kony.automation.textarea.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","txtAreaReply"],"Reply to Message");
	  appLog("Successfully Entered Message content");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnSendReply"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnSendReply"]);
	  appLog("Successfully Clicked on Send REPLY button");
	  await kony.automation.playback.wait(15000);
	}
	
	async function restoreNewMessage(){
	
	  appLog("Intiated method to Restore a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnDeletedMessages"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnDeletedMessages"]);
	  appLog("Successfully Clicked on DELETE button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnRestore"],15000);
	  kony.automation.button.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnRestore"]);
	  appLog("Successfully Clicked on RESTORE button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function searchNewMessage(){
	
	  appLog("Intiated method to Search a newMessage");
	  
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmNotificationsAndMessages","NotficationsAndMessages","txtSearch"],"Test");
	  appLog("Successfully Entered Text to Search");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","btnSearch"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","NotficationsAndMessages","btnSearch"]);
	  appLog("Successfully Clicked on SEARCH button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function verifyRequestID(){
	
	  appLog("Intiated method to Verify Request ID");
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmNotificationsAndMessages","NotficationsAndMessages","segMessageAndNotification[0]","flxNotificationsAndMessages","lblRequestIdValue"],"text")).not.toBe('');
	}
	
	async function MoveBackToDashBoard_Messages(){
	
	  await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Accounts dashboard");
	
	}
	
	async function PostLogin_NavigateToAboutUs_FAQ(){
	
	  appLog("Intiated  method to Navigate to About US");
	  
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs4flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmOnlineHelp","help","lblHeading"],15000);
	  
	  appLog("Successfully Navigated to About US");
	}
	
	async function PostLogin_MoveBacktoDashboard_AboutUs_FAQ(){
	
	  await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function PostLogin_NavigateToPrivacyPolicy(){
	
	  appLog("Intiated method to Navigate to Privacypolicy");
	  
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs1flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs1flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"],15000);
	  
	  appLog("Successfully Navigated to Privacypolicy");
	  
	}
	
	async function PostLogin_MoveBacktoDashboard_PrivacyPolicy(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function PostLogin_NavigateToTermsConditions(){
	
	  appLog("Intiated method to Navigate to TC's");
	  
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs0flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"],15000);
	  
	  appLog("Successfully Navigated to TC's");
	}
	
	async function PostLogin_MoveBacktoDashboard_TermsConditions(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function PostLogin_NavigateToContactUs(){
	
	  appLog("Intiated method to Navigate to Contact US");
	  
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs2flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs2flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"],15000);
	  
	  appLog("Successfully Navigated to Contact US");
	}
	
	async function PostLogin_MoveBacktoDashboard_ContactUs(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  //await kony.automation.playback.wait(5000);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function PreLogin_NavigateToFAQ(){
	
	  appLog("Intiated method to Navigate to About US");
	  
	  await kony.automation.playback.waitFor(["frmLogin","btnFaqs"],10000);
	  kony.automation.button.click(["frmLogin","btnFaqs"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to About US");
	}
	async function MoveBacktoLogin_FAQ(){
	
	  await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmOnlineHelp","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	
	async function PreLogin_NavigateToPrivacyPolicy(){
	
	  appLog("Intiated method to Navigate to PrivacyPolicy");
	  
	  await kony.automation.playback.waitFor(["frmLogin","btnPrivacy"],10000);
	  kony.automation.button.click(["frmLogin","btnPrivacy"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to PrivacyPolicy");
	}
	async function MoveBacktoLogin_PrivacyPolicyScreen(){
	  
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	async function PreLogin_NavigateToTermsConditions(){
	
	  appLog("Intiated method to Navigate to TC's");
	  
	  await kony.automation.playback.waitFor(["frmLogin","btnTermsAndConditions"],10000);
	  kony.automation.button.click(["frmLogin","btnTermsAndConditions"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to TC's");
	}
	async function MoveBacktoLogin_TermsConditions(){
	  
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	async function PreLogin_NavigateToContactUs(){
	
	  appLog("Intiated method to Navigate to ContactUs");
	  
	  await kony.automation.playback.waitFor(["frmLogin","btnContactUs"],10000);
	  kony.automation.button.click(["frmLogin","btnContactUs"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to Contact US");
	}
	async function MoveBacktoLogin_ContactUsScreen(){
	  
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	
	
	async function NavigateToProfileSettings(){
	
	  appLog("Intiated method to Navigate to Personal Details");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);
	  await kony.automation.playback.wait(15000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPersonalDetailsHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPersonalDetailsHeading"], "text")).toEqual("Personal Details");
	
	  appLog("Successfully Navigated to Personal Details");
	}
	
	async function selectProfileSettings_PhoneNumber(){
	
	  appLog("Intiated method to Navigate to PhoneNumber Flex");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxPhone"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxPhone"]);
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPhoneNumbersHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPhoneNumbersHeading"], "text")).toEqual("Phone Number");
	
	  appLog("Successfully Navigated to PhoneNumber Flex");
	}
	
	async function selectProfileSettings_EmailAddress(){
	
	  appLog("Intiated method to Navigate to EmailID Flex");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEmail"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEmail"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEmailHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEmailHeading"], "text")).toEqual("Email");
	
	  appLog("Successfully Navigated to EmailID Flex");
	}
	
	async function selectProfileSettings_Address(){
	
	  appLog("Intiated method to Navigate to Address Flex");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddress"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddress"]);
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddressHeading"], "text")).toEqual("Address");
	
	  appLog("Successfully Navigated to Address Flex");
	}
	
	async function ProfileSettings_addNewPhoneNumberDetails(phoneNumber,isPrimary){
	
	  appLog("Intiated method to addNewPhoneNumberDetails");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddPhoneNumberHeading"], "text")).toEqual("Add Phone Number");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxAddPhoneNumberType"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Other");
	  appLog("Successfully Selected PhoneNumber Type");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],"91");
	  appLog("Successfully Entered CountryCode");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumber"],phoneNumber);
	  appLog("Successfully Entered Phone Number as : <b>"+phoneNumber+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakePrimayPhoneNumbercheckBox();
	  }
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberSave"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
	  appLog("Successfully clicked on SAVE button");
	  await kony.automation.playback.wait(5000);
	
	  var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
	
	  if(isAddHeader){
	
	    appLog("Custom Message :: Update Customer Details Failed");
	    fail("Custom Message :: Update Customer Details Failed");
	  }else{
	    appLog("Successfully Added Mobile Number");
	  }
	
	}
	
	async function selectMakePrimayPhoneNumbercheckBox(){
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddCheckBox3"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddCheckBox3"]);
	  appLog("Successfully Selected Entered Phone Number as Primary");
	}
	
	async function ProfileSettings_UpdatePhoneNumber(updatedPhonenum){
	
	  // Update Number
	  appLog("Intiated method to Update PhoneNumberDetails");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	
	  var segLength1=accounts_Size1.length;
	  kony.print("Segment length is :"+segLength1);
	
	  var isEditble=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers[0]","btnViewDetail"],15000);
	  if(isEditble){
	
	    kony.automation.button.click(["frmProfileManagement","settings","segPhoneNumbers[0]","btnViewDetail"]);
	    appLog("Successfully clicked on ViewDetails button");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxPhoneNumber"]);
	    kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxPhoneNumber"],updatedPhonenum);
	    appLog("Successfully Updated Phone number as : <b>"+updatedPhonenum+"</b>");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
	    kony.automation.button.click(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
	    appLog("Successfully clicked on SAVE button");
	    await kony.automation.playback.wait(5000);
	
	    var isEditHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditPhoneNumberHeading"],15000);
	
	    if(isEditHeader){
	
	      appLog("Custom Message :: Update Customer Details Failed");
	      fail("Custom Message :: Update Customer Details Failed");
	    }else{
	      appLog("Successfully Updated Mobile Number");
	    }
	  }else{
	    appLog("Unable to Update PhoneNumberDetails");
	  }
	}
	
	async function ProfileSettings_DeletePhoneNumber(phoneNumber){
	
	  //Delete already added Mobile Number
	  appLog("Intiated method to Delete PhoneNumberDetails");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	  var segLength=seg_Size.length;
	  //appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++){
	    var seg="segPhoneNumbers["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
	    var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
	    //appLog("Text is :: "+phonenum);
	    if(phonenum===phoneNumber){
	
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
	      appLog("Successfully Clicked on Delete Button");
	
	      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
	      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
	      appLog("Successfully Clicked on YES Button");
	      await kony.automation.playback.wait(5000);
	      break;
	    }
	  } 
	}
	
	async function ProfileSettings_VerifyaddNewPhoneNumberFunctionality(phoneNumber,isPrimary){
	
	  appLog("Intiated method to VerifyaddNewPhoneNumberFunctionality");
	
	  var isAddNewNumber=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewNumber"],15000);
	  //appLog("Button status is :"+isAddNewNumber);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	  var segLength=seg_Size.length;
	  //appLog("PhoneNumbers size is :: "+segLength);
	
	  if(segLength<3&&isAddNewNumber){
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewNumber"]);
	    appLog("Successfully Clicked on Add New Phone Number Button");
	    await ProfileSettings_addNewPhoneNumberDetails(phoneNumber,isPrimary);
	
	    var Header=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPhoneNumbersHeading"],15000);
	    if(Header&&isPrimary==='NO'){
	      await ProfileSettings_DeletePhoneNumber(phoneNumber);
	    }else{
	      appLog("Custom Message :: Update Customer Details Failed");
	      fail("Custom Message :: Custom Message :: Update Customer Details Failed");
	    }
	
	  }else{
	    appLog("Maximum phone numbers already added");
	  }
	}
	
	async function ProfileSettings_addNewAddressDetails(addressLine1,addressLine2,zipcode,isPrimary){
	
	  appLog("Intiated method to addNewAddressDetails");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine1"],addressLine1);
	  appLog("Successfully Entered AddressLine1 : <b>"+addressLine1+"</b>");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine2"],addressLine2);
	  appLog("Successfully Entered AddressLine2 : <b>"+addressLine2+"</b>");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCountry"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCountry"], "IN");
	  appLog("Successfully Selected Country Code");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxState"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxState"], "IN-TG");
	  appLog("Successfully Selected State Code");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","txtCity"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","txtCity"],"HYD");
	  appLog("Successfully Selected City Code");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxZipcode"],zipcode);
	  appLog("Successfully Entered Zipcode : <b>"+zipcode+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakeDefaultAddresscheckBox();
	  }
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddressAdd"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddressAdd"]);
	  appLog("Successfully Clicked on Add Address Button");
	  await kony.automation.playback.wait(5000);
	
	  var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewAddressHeader"],15000);
	
	  if(isAddHeader){
	    appLog("Custom Message :: Update Customer Details Failed");
	    fail("Custom Message :: Update Customer Details Failed");
	
	  }else{
	    appLog("Successfully Added new Address details");
	  }
	}
	
	
	async function selectMakeDefaultAddresscheckBox(){
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"]);
	  appLog("Successfully Selected Entered Address as Primary");
	}
	
	async function ProfileSettings_UpdateAddress(UpdatedZip){
	
	  // Update Address
	  appLog("Intiated method to update Address Details");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"]);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	  var segLength=accounts_Size.length;
	  kony.print("Segment length is : "+segLength);
	
	  var isEditble=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses[0]","btnEdit"],15000);
	  if(isEditble){
	    kony.automation.button.click(["frmProfileManagement","settings","segAddresses[0]","btnEdit"]);
	    appLog("Successfully clicked on Edit button");
	    await kony.automation.playback.wait(5000);
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditZipcode"],15000);
	    kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditZipcode"],UpdatedZip);
	    appLog("Successfully Entered Updated Zipcode as : <b>"+UpdatedZip+"</b>");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAddressSave"],15000);
	    kony.automation.button.click(["frmProfileManagement","settings","btnEditAddressSave"]);
	    appLog("Successfully clicked on SAVE button");
	    await kony.automation.playback.wait(5000);
	
	    var isEditHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAddressHeader"],15000);
	
	    if(isEditHeader){
	      appLog("Custom Message :: Update Customer Details Failed");
	      fail("Custom Message :: Update Customer Details Failed");
	
	    }else{
	      appLog("Successfully Updated Address Details");
	    }
	  }
	
	
	
	}
	
	async function ProfileSettings_DeleteAddress(addressLine1){
	
	  // Delete Address
	  appLog("Intiated method to Delete Address Details");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segAddresses["+x+"]";
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"flxRow","lblAddressLine1"],15000);
	    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"flxRow","lblAddressLine1"], "text");
	    //appLog("Text is :: "+address1);
	    if(address1===addressLine1){
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
	      appLog("Successfully clicked on DELETE button");
	      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
	      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
	      appLog("Successfully clicked on YES button");
	      await kony.automation.playback.wait(5000);
	      break;
	    }
	  }
	
	}
	
	async function ProfileSettings_VerifyaddNewAddressFunctionality(addressLine1,addressLine2,zipcode,isPrimary){
	
	
	  appLog("Intiated method to VerifyaddNewAddressFunctionality");
	
	  var isAddNewAddress=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddress"],15000);
	  //appLog("Button status is :"+isAddNewAddress);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	  var segLength=seg_Size.length;
	  //appLog("Address size is :: "+segLength);
	
	  if(segLength<3&&isAddNewAddress){
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddress"]);
	    appLog("Successfully clicked on AddNewAddress button");
	    await ProfileSettings_addNewAddressDetails(addressLine1,addressLine2,zipcode,isPrimary);
	
	    var Header=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"],15000);
	    if(Header&&isPrimary==='NO'){
	      await ProfileSettings_DeleteAddress(addressLine1);
	    }else{
	      appLog("Custom Message :: Update Customer Details Failed");
	      fail("Custom Message :: Custom Message :: Update Customer Details Failed");
	    }
	
	  }else{
	    appLog("Maximum Address already added");
	  }
	
	}
	
	async function ProfileSettings_addEmailAddressDetails(emailAddress,isPrimary){
	
	  // Add new email ID
	  appLog("Intiated method to add new Emai Address Details");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewEmailHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddNewEmailHeading"], "text")).toEqual("Add New Email");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEmailId"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEmailId"],emailAddress);
	  appLog("Successfully Entered new Email Address : <b>"+emailAddress+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakePrimayEmailIDcheckBox();
	  }
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddEmailIdAdd"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddEmailIdAdd"]);
	  appLog("Successfully clicked on Add Button");
	  await kony.automation.playback.wait(5000);
	
	  var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewEmailHeading"],15000);
	
	  if(isAddHeader){
	    appLog("Custom Message :: Update Customer Details Failed");
	    fail("Custom Message :: Update Customer Details Failed");
	  }else{
	    appLog("Successfully added new Email Address");
	  }
	}
	
	async function selectMakePrimayEmailIDcheckBox(){
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxMarkAsPrimaryEmailCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxMarkAsPrimaryEmailCheckBox"]);
	  appLog("Successfully Selected Entered EmailID as Primary");
	}
	
	async function ProfileSettings_UpdateEmailAddress(updatedemailid){
	
	  // Update email ID
	  appLog("Intiated method to Update Email address");
	  
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength1=accounts_Size1.length;
	  appLog("Length is :"+segLength1);
	
	  var isEditble=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds[0]","btnEdit"],15000);
	  if(isEditble){
	    kony.automation.button.click(["frmProfileManagement","settings","segEmailIds[0]","btnEdit"]);
	    appLog("Successfully clicked on Edit Button");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditEmailId"],15000);
	    kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditEmailId"],updatedemailid);
	    appLog("Successfully Entered Updated Email ID : <b>"+updatedemailid+"</b>");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditEmailIdSave"],15000);
	    kony.automation.button.click(["frmProfileManagement","settings","btnEditEmailIdSave"]);
	    appLog("Successfully Clicked on SAVE button");
	    await kony.automation.playback.wait(5000);
	
	    var isEditHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditEmailHeading"],15000);
	
	    if(isEditHeader){
	      appLog("Custom Message :: Update Customer Details Failed");
	      fail("Custom Message :: Update Customer Details Failed");
	    }else{
	      appLog("Successfully Updated email Address");
	    }
	  }
	
	}
	
	async function ProfileSettings_deleteEmailAddressDetails(emailAddress){
	
	  // Delete Address
	  appLog("Intiated method to Delete Email Details");
	  
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segEmailIds["+x+"]";
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"flxRow","lblEmail"],15000);
	    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"flxRow","lblEmail"], "text");
	    //appLog("Text is :: "+address1);
	    if(address1===emailAddress){
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
	      appLog("Successfully Clicked on Delete Button");
	      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
	      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
	      appLog("Successfully Clicked on YES Button");
	      await kony.automation.playback.wait(5000);
	      break;
	    }
	  }
	}
	
	async function ProfileSettings_VerifyaddEmailAddressFunctionality(emailAddress,isPrimary){
	
	  appLog("Intiated method to VerifyaddEmailAddressFunctionality");
	  
	  var isAddEmailAddress=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewEmail"],15000);
	  //appLog("Button status is :"+isAddEmailAddress);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	  var segLength=seg_Size.length;
	  //appLog("email size is :: "+segLength);
	
	  if(segLength<3&&isAddEmailAddress){
	    
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewEmail"]);
	    appLog("Successfully clicked on NewEmail Button");
	    await ProfileSettings_addEmailAddressDetails(emailAddress,isPrimary);
	
	    // if there is an error after saving phone number
	    var Header= await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEmailHeading"],15000);
	    if(Header&&isPrimary==='NO'){
	      await ProfileSettings_deleteEmailAddressDetails(emailAddress);
	    }else{
	
	      appLog("Custom Message :: Update Customer Details Failed");
	      fail("Custom Message :: Custom Message :: Update Customer Details Failed");
	      //await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberCancel"]);
	      //kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberCancel"]);
	    }
	
	  }else{
	    appLog("Maximum Email Address already added");
	  }
	
	}
	
	
	async function MoveBackToDashBoard_ProfileManagement(){
	
	  // Move back to base state
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function NavigateToAccountSettings(){
	
	  appLog("Intiated method to navigate to Account Settings");
	  
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAccountsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAccountsHeader"], "text")).toEqual("Accounts");
	
	  appLog("Successfully Navigated to AccountSettings");
	}
	
	
	async function clickonDefaultAccountstab(){
	
	  appLog("Intiated method to click on DefaultAccountstab");
	  
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxSetDefaultAccount"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxSetDefaultAccount"]);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblDefaultTransactionAccounttHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblDefaultTransactionAccounttHeading"], "text")).toEqual("Default Transaction Accounts");
	
	  appLog("Successfully clicked on DefaultAccountstab");
	  
	  await kony.automation.playback.wait(5000);
	}
	async function clickonAccountPreferencetab(){
	
	  appLog("Intiated method to click on AccountPreferencetab");
	  
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAccountPreferences"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAccountPreferences"]);
	  
	  appLog("Successfully clicked on AccountPreferencetab");
	  
	  await kony.automation.playback.wait(5000);
	}
	
	
	async function EditFavAccountPreferences(){
	
	  appLog("Intiated method to Edit FavAccountPreferences");
	  
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"]);
	  appLog("Successfully Clicked on Edit button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAccountsHeader"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEditAccountsHeader"], "text")).toEqual("Edit Account");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAccountNickNameValue"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAccountNickNameValue"],'My Checking');
	  appLog("Successfully Updated NickName value");
	  
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAccountsSave"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnEditAccountsSave"]);
	  appLog("Successfully Clicked on SAVE button");
	}
	
	async function SetDefaultAccountPreferences(){
	
	  appLog("Intiated method to Set DefaultAccountPreferences Tab");
	  
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"]);
	  appLog("Successfully clicked on Edit Button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblSelectedDefaultAccounts"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblSelectedDefaultAccounts"], "text")).not.toBe('');
	
	//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxBillPay"],15000);
	//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxBillPay"], "190128223241502");
	//   appLog("Successfully Selected Default BillPay acc");
	
	//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCheckDeposit"],15000);
	//   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCheckDeposit"], "190128223242830");
	//   appLog("Successfully Selected Default Deposit acc");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"]);
	  appLog("Successfully Clicked on SAVE Button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblSelectedDefaultAccounts"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblSelectedDefaultAccounts"], "text")).not.toBe('');
	  appLog("Successfully Verified Default accounts");
	}
	
	
	
	async function navigateToTransfers(){
	
	  appLog("Intiated method to Navigate FastTransfers Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransferMoney"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransferMoney"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmFastTransfers","lblTransfers"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","lblTransfers"], "text")).toEqual("Transfers");
	  appLog("Successfully Navigated to FastTransfers Screen");
	}
	
	async function SelectFromAccount(fromAcc){
	
	  appLog("Intiated method to Select From Account");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmFastTransfers","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmFastTransfers","txtTransferFrom"], [230,25],null,null);
	  kony.automation.textbox.enterText(["frmFastTransfers","txtTransferFrom"],fromAcc);
	  appLog("Successfully Entered From Account");
	  await kony.automation.playback.wait(5000);
	  kony.automation.flexcontainer.click(["frmFastTransfers","segTransferFrom[0,0]","flxAmount"]);
	  appLog("Successfully Selected From Account from List");
	}
	
	async function SelectToAccount(ToAccReciptent){
	
	  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastTransfers","txtTransferTo"],15000);
	  kony.automation.widget.touch(["frmFastTransfers","txtTransferTo"], [72,9],null,null);
	
	  //   if(ToAccReciptent==='OwnAcc'){
	  //     kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],"Saving");
	  //     appLog("Successfully Entered Default To Account : ");
	  //     await kony.automation.playback.wait(5000);
	  //     expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
	  //   }else{
	  //     kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],ToAccReciptent);
	  //     appLog("Successfully Entered To Account : <b>"+ToAccReciptent+"</b>");
	  //     await kony.automation.playback.wait(5000);
	  //     expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
	  //   }
	
	  kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],ToAccReciptent);
	  appLog("Successfully Entered To Account : <b>"+ToAccReciptent+"</b>");
	  await kony.automation.playback.wait(5000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
	
	  kony.automation.flexcontainer.click(["frmFastTransfers","segTransferTo[0,0]","flxAmount"]);
	  appLog("Successfully Selected To Account from List");
	
	}
	
	async function EnterAmount(amountValue) {
	
	  await kony.automation.playback.waitFor(["frmFastTransfers","tbxAmount"],15000);
	  kony.automation.textbox.enterText(["frmFastTransfers","tbxAmount"],amountValue);
	  appLog("Successfully Entered Amount as : <b>"+amountValue+"</b>");
	  await kony.automation.scrollToWidget(["frmFastTransfers","customfooternew","btnFaqs"]);
	}
	
	async function SelectFrequency(freqValue) {
	
	  kony.automation.flexcontainer.click(["frmFastTransfers","flxContainer4"]);
	  kony.automation.listbox.selectItem(["frmFastTransfers","lbxFrequency"], freqValue);
	  appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	}
	
	async function SelectDateRange() {
	
	  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
	  kony.automation.calendar.selectDate(["frmFastTransfers","calEndingOnNew"], [11,25,2021]);
	  appLog("Successfully Selected DateRange");
	}
	
	async function SelectSendOnDate() {
	
	  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
	  appLog("Successfully Selected SendOn Date");
	}
	
	async function SelectOccurences(occurences) {
	
	  kony.automation.listbox.selectItem(["frmFastTransfers","lbxForHowLong"], "NO_OF_RECURRENCES");
	  kony.automation.textbox.enterText(["frmFastTransfers","tbxNoOfRecurrences"],occurences);
	  appLog("Successfully Selected Occurences as <b>"+occurences+"</b>");
	  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
	  appLog("Successfully Selected SendOn Date");
	}
	async function EnterNoteValue(notes) {
	
	  await kony.automation.playback.waitFor(["frmFastTransfers","txtNotes"],10000);
	  kony.automation.textbox.enterText(["frmFastTransfers","txtNotes"],notes);
	  appLog("Successfully entered Note value as : <b>"+notes+"</b>");
	  await kony.automation.playback.waitFor(["frmFastTransfers","btnConfirm"],10000);
	  await kony.automation.scrollToWidget(["frmFastTransfers","btnConfirm"]);
	  kony.automation.button.click(["frmFastTransfers","btnConfirm"]);
	  appLog("Successfully Clicked on Continue Button");
	}
	
	async function ConfirmTransfer() {
	
	  appLog("Intiated method to Confirm Transfer Details");
	
	  await kony.automation.playback.waitFor(["frmReview","btnConfirm"],15000);
	  kony.automation.button.click(["frmReview","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	async function VerifyTransferSuccessMessage() {
	
	  appLog("Intiated method to Verify Transfer SuccessMessage ");
	
	  await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmConfirmTransfer"],30000);
	
	  if(success){
	    //expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).toContain("We successfully");
	    //   expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).not.toBe('');
	    //   await kony.automation.playback.waitFor(["frmConfirmTransfer","btnSavePayee"],15000);
	    //   kony.automation.button.click(["frmConfirmTransfer","btnSavePayee"]);
	    await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : rtxMakeTransferError");
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	async function CancelTransfer() {
	
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmReview","btnCancel"],15000);
	  kony.automation.button.click(["frmReview","btnCancel"]);
	  appLog("Successfully Clicked on CANCEL Button");
	  await kony.automation.playback.waitFor(["frmReview","CustomPopup","btnYes"],15000);
	  kony.automation.button.click(["frmReview","CustomPopup","btnYes"]);
	  appLog("Successfully Clicked on YES Button");
	}
	
	async function navigateToTransferActivities(){
	
	  appLog("Intiated method to navigate to Transfer Activities");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxPayBills"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxPayBills"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to TransferActivities");
	}
	
	async function navigateToPastTransfersTab(){
	
	  appLog("Intiated method to navigate to PastTransfer Tab");
	  await kony.automation.playback.waitFor(["frmFastTransfersActivites","btnRecent"],15000);
	  kony.automation.button.click(["frmFastTransfersActivites","btnRecent"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully navigated to PastTransfer Tab");
	}
	
	async function verifyRepeatTransferFunctionality(note){
	
	  appLog("Intiated method verify Repeat Transfer Functionality");
	
	  var noTransfers=await kony.automation.playback.waitFor(["frmFastTransfersActivites","rtxNoPaymentMessage"],10000);
	
	  if(noTransfers){
	
	    appLog('There are No Transactions Found');
	    await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	    await kony.automation.playback.wait(5000); 
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else{
	
	    //await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
	    var noReapeatBtn= await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","btnAction"],10000);
	
	    if(noReapeatBtn){
	      appLog('Intiating Repeat Transfer');
	      kony.automation.button.click(["frmFastTransfersActivites","segmentTransfers[0]","btnAction"]);
	      await kony.automation.playback.wait(5000);
	      appLog("Successfully Clicked on Repeat Button");
	      await EnterNoteValue(note);
	      await ConfirmTransfer();
	      await VerifyTransferSuccessMessage();
	    }else{
	      appLog('No Repeat Transfers available');
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	      await kony.automation.playback.wait(5000); 
	      appLog("Successfully Moved back to Accounts dashboard");
	    }
	  }
	
	
	}
	
	async function VerifyTranxUnderActivities(){
	
	  appLog("Intiated method verify Transfer under Activities");
	
	  var noTransfers=await kony.automation.playback.waitFor(["frmFastTransfersActivites","rtxNoPaymentMessage"],15000);
	
	  if(noTransfers){
	    appLog('There are No Transactions Found');
	    await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	    await kony.automation.playback.wait(5000);
	    await verifyAccountsLandingScreen();
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else{
	
	    //await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
	    var noTranxBtn= await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","flxDropdown"],10000);
	    if(noTranxBtn){
	
	      appLog('Intiating to verify Transfer Activity');
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","segmentTransfers[0]","flxDropdown"]);
	      appLog("Successfully Clicked on first Sheduled Transfer");
	
	      //No garuntee that same note will be there, other users also will perform Tranx
	      //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfersActivites","segmentTransfers[0]","flxFastPastTransfersSelected","lblNotesValue1"],"text")).toEqual(notevalue);
	
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	      await verifyAccountsLandingScreen();
	      appLog("Successfully Moved back to Accounts dashboard");
	    }else{
	
	      appLog('No Transfers activities available');
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	      await kony.automation.playback.wait(5000); 
	      appLog("Successfully Moved back to Accounts dashboard");
	    }
	
	  }
	}
	
	async function MoveBackToLandingScreen_Transfers(){
	
	  //Move back to landing Screen
	  appLog("Intiated method to move from frmFastTransfers screen");
	  await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	
	it("PayBills_AddPayee", async function() {
	
	 // Add payee and Then Delete same payee
	  var unique_RecipitentName=BillPay.addPayee.unique_RecipitentName+getRandomString(5);
	  var unique_Accnumber="0"+new Date().getTime();
	
	  await navigateToBillPay();
	  await clickOnAddPayeeLink();
	  await enterPayeeDetails_UsingPayeeinfo(unique_RecipitentName,BillPay.addPayee.address1,BillPay.addPayee.address2,BillPay.addPayee.city,BillPay.addPayee.zipcode,unique_Accnumber,"PayBills_AddPayee");
	  await clickOnNextButton_payeeDetails();
	  await clickOnConfirmButton_verifyPayee();
	  await verifyAddPayeeSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	  //Activate Payee to initilize payments
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(unique_RecipitentName);
	  await activateNewlyAddedpayee();
	  
	  
	   //Delete same payee
	  //await navigateToManagePayee();
	  //await selectPayee_ManagePayeeList("Test");
	  //await deletePayee_ManagePayee();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","flxAddPayee"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","flxAddPayee"]);
	//   await kony.automation.playback.waitFor(["frmAddPayee1","btnEnterPayeeInfo"]);
	//   kony.automation.button.click(["frmAddPayee1","btnEnterPayeeInfo"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterName"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterName"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddress"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddress"],"LR PALLI");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddressLine2"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddressLine2"],"ATMAKUR");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxCity"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxCity"],"ATMAKUR");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterZipCode"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterZipCode"],"500055");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAccountNmber"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAccountNmber"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxConfirmAccNumber"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxConfirmAccNumber"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","flxClick"]);
	//   kony.automation.flexcontainer.click(["frmAddPayeeInformation","flxClick"]);
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxAdditionalNote"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxAdditionalNote"],"ADD PAYYE");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","btnReset"]);
	//   kony.automation.button.click(["frmAddPayeeInformation","btnReset"]);
	//   await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
	//   kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	//   await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
	//   kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   //Delete same payee
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"Test");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDropdown"]);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnDeleteBiller"]);
	//   await kony.automation.playback.waitFor(["frmManagePayees","DeletePopup","btnYes"]);
	//   kony.automation.button.click(["frmManagePayees","DeletePopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("Cancel_BillPay", async function() {
	 
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Yearly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Yearly");
	  await cancelSheduledBillPay();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnCancel"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnCancel"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to cancel this transaction?");
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","btnYes"]);
	//   kony.automation.button.click(["frmPayBillConfirm","CancelPopup","btnYes"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("ModifyShedulePayment", async function() {
	  
	  
	  await navigateToSheduledBillPay();
	  await EditSheduledBillPay("Updated Sheduled BillPay");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnScheduled"]);
	//   kony.automation.button.click(["frmBulkPayees","btnScheduled"]);
	//   await kony.automation.playback.wait(5000);
	  
	 
	//   var nopayments=await kony.automation.playback.waitFor(["frmBillPayScheduled","rtxNoPaymentMessage"],10000);
	//   if(nopayments){
	//     kony.print("There are no sheduled payments");
	//     //Move back to accounts
	//     await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//     kony.automation.button.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else{
	
	//   kony.print("There are few sheduled payments");
	//   await kony.automation.playback.waitFor(["frmBillPayScheduled","segmentBillpay"]);
	//   kony.automation.button.click(["frmBillPayScheduled","segmentBillpay[0]","btnEdit"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","lblPayABill"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayABill","lblPayABill"],"text")).toEqual("Pay a Bill");
	
	
	//   //Update frequency and Notes as well
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Updated Sheduled BillPay");
	
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxCheckBoxTnC"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxCheckBoxTnC"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	
	//   var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],10000);
	//   if(warning){
	//     await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//     //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	    
	//   }else{
	//     await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	//     await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   }  
	// }
	  
	},120000);
	
	it("PayBills_DeletePayee", async function() {
	  
	  // Add payee and Then Delete same payee
	  var delete_RecipitentName=BillPay.deletePayee.delete_RecipitentName+getRandomString(5);
	  var unique_Accnumber="0"+new Date().getTime();
	
	  await navigateToBillPay();
	  await clickOnAddPayeeLink();
	  await enterPayeeDetails_UsingPayeeinfo(delete_RecipitentName,BillPay.deletePayee.address1,BillPay.deletePayee.address2,BillPay.deletePayee.city,BillPay.deletePayee.zipcode,unique_Accnumber,"PayBills_DeletePayee");
	  await clickOnNextButton_payeeDetails();
	  await clickOnConfirmButton_verifyPayee();
	  await verifyAddPayeeSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	   //Delete same payee
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(delete_RecipitentName);
	  await deletePayee_ManagePayee();
	
	//    var unique_RecipitentName="Test Automation_"+new Date().getTime();
	//   // Add payee and Then Delete same payee
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","flxdeletePayee"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","flxdeletePayee"]);
	//   await kony.automation.playback.waitFor(["frmdeletePayee1","btnEnterPayeeInfo"]);
	//   kony.automation.button.click(["frmdeletePayee1","btnEnterPayeeInfo"]);
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterName"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterName"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterAddress"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterAddress"],"LR PALLI");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterAddressLine2"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterAddressLine2"],"ATMAKUR");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxCity"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxCity"],"ATMAKUR");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterZipCode"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterZipCode"],"500055");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterAccountNmber"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterAccountNmber"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxConfirmAccNumber"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxConfirmAccNumber"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","flxClick"]);
	//   kony.automation.flexcontainer.click(["frmdeletePayeeInformation","flxClick"]);
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxAdditionalNote"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxAdditionalNote"],"ADD PAYYE");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","btnReset"]);
	//   kony.automation.button.click(["frmdeletePayeeInformation","btnReset"]);
	//   await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
	//   kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	//   await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
	//   kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   //Delete same payee
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"Test");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDropdown"]);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnDeleteBiller"]);
	//   await kony.automation.playback.waitFor(["frmManagePayees","DeletePopup","btnYes"]);
	//   kony.automation.button.click(["frmManagePayees","DeletePopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("PayBills_EditPayee", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.addPayee.unique_RecipitentName);
	  await clickOnEditBtn_ManagePayees();
	  await EditPayee_ManagePayee();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDropdown"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnEditBiller"]);
	  
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDetailsWrapper"]);
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","segmentBillpay[0]","tbxPinCode"],"123456");
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnSave"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("PayBills_MakeOneTimePayment", async function() {
	
	   await navigateToOneTimePayment();
	   await enterOneTimePayeeInformation(BillPay.oneTimePay.payeeName,BillPay.oneTimePay.zipcode,BillPay.oneTimePay.accno,BillPay.oneTimePay.accnoAgain,BillPay.oneTimePay.mobileno);
	   await enterOneTimePaymentdetails(BillPay.oneTimePay.amountValue,"PayBills_MakeOneTimePayment");
	   await confirmOneTimePaymnet();
	   await verifyOneTimePaymentSuccessMsg();
	   await verifyAccountsLandingScreen();
	  
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","flxMakeOneTimePayment"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","flxMakeOneTimePayment"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],"A");
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"]);
	//   kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],"500055");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"]);
	//   kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],"2.1");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],"test OneTime payment");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"]);
	//   kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);
	  
	//   //expect(kony.automation.widget.getWidgetProperty(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],"text")).toEqual("Success! Your transaction has been completed");
	  
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("RepeatBillPayment", async function() {
	  
	  await navigateToPastBillPay();
	  await repeatPastBillPayment("Repeat a BillPay");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnHistory"]);
	//   kony.automation.button.click(["frmBulkPayees","btnHistory"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var nopayments=await kony.automation.playback.waitFor(["frmBillPayHistory","rtxNoPaymentMessage"],10000);
	 
	//   if(nopayments){
	//     kony.print("There are no History payments");
	//     //Move back to accounts
	//     await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//     kony.automation.button.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else{
	//     await kony.automation.playback.waitFor(["frmBillPayHistory","segmentBillpay"]);
	//   kony.automation.button.click(["frmBillPayHistory","segmentBillpay[0]","btnRepeat"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Repeat a BillPay");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	
	//   var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],10000);
	//   if(warning){
	//     await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//     //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	
	//   }else{
	//     await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   }
	    
	//   }
	
	},120000);
	
	it("SheduledPayment_Daily", async function() {
	  
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Daily");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Daily");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Daily");
	
	//   //new chnage in 202010
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "ON_SPECIFIC_DATE");
	//       //kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "NO_OF_RECURRENCES");
	//   //new chnage in 202010
	  
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Daily");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("We successfully scheduled your transfer");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_EveryTwoWeeks", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("BiWeekly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Every Two Weeks");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Every Two Weeks");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Every Two Weeks");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_Monthly", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Monthly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Monthly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Monthly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Monthly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_Quarterly", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Quarterly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Quarterly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Quarterly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Quarterly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_Weekly", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Weekly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Weekly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Weekly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Weekly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_Yearly", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Yearly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Yearly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("VerifyAddPayee_BillPayFlow", async function() {
	  
	  // Add payee and Then Delete same payee
	  var unique_RecipitentName=BillPay.addPayee.unique_RecipitentName+getRandomString(5);
	  var unique_Accnumber="0"+new Date().getTime();
	
	  await navigateToBillPay();
	  await clickOnAddPayeeLink();
	  await enterPayeeDetails_UsingPayeeinfo(unique_RecipitentName,BillPay.addPayee.address1,BillPay.addPayee.address2,BillPay.addPayee.city,BillPay.addPayee.zipcode,unique_Accnumber,"PayBills_AddPayee");
	  await clickOnNextButton_payeeDetails();
	  await clickOnConfirmButton_verifyPayee();
	  await verifyAddPayeeSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	  //Activate Payee to initilize payments
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(unique_RecipitentName);
	  await activateNewlyAddedpayee();
	 
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","flxMakeOneTimePayment"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","flxMakeOneTimePayment"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],"A");
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"]);
	//   kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],"500055");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"]);
	//   kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],"2.1");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],"test OneTime payment");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"]);
	//   kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
	//   kony.automation.button.click(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
	  
	//   await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
	//   kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	  
	//   await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
	//   kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	  
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAddPayee"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAddPayee"],"text")).toEqual("Add Payee");
	  
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
	  
	  
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","btnViewAllPayees"]);
	//   kony.automation.button.click(["frmPayeeAcknowledgement","btnViewAllPayees"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("VerifyAllpayees", async function() {
	  
	  await navigateToBillPay();
	  await clickOnAllpayeesTab();
	  await verifyAllPayeesList();
	  await MoveBackToDashBoard_AllPayees();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnAllPayees"]);
	//   kony.automation.button.click(["frmBulkPayees","btnAllPayees"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","segmentBillpay[0]","flxDropdown"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("VerifyPayement_PayeeDetails", async function() {
	
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Yearly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Yearly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("VerifySearchFunctionality", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.addPayee.unique_RecipitentName);
	  await MoveBackToDashBoard_ManagePayees();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	 
	 
	//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
});