describe("T24_ProfileSettingsSuite", function() {
	beforeEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  strLogger=[];
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside before Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	},480000);
	
	afterEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside after Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	  //strLogger=null;
	
	},480000);
	
	async function verifyAccountsLandingScreen(){
	
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
	}
	
	async function SelectAccountsOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
	        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(10000);
	        
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnFirstCheckingAccount(){
	
	  appLog("Intiated method to click on First Checking account");
	  SelectAccountsOnDashBoard("Checking");
	  //appLog("Successfully Clicked on First Checking account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstSavingsAccount(){
	
	  appLog("Intiated method to click on First Savings account");
	  SelectAccountsOnDashBoard("Saving");
	  //appLog("Successfully Clicked on First Savings account");
	  //await kony.automation.playback.wait(5000);
	}
	
	
	async function clickOnFirstCreditCardAccount(){
	
	  appLog("Intiated method to click on First CreditCard account");
	  SelectAccountsOnDashBoard("Credit");
	  //appLog("Successfully Clicked on First CreditCard account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstDepositAccount(){
	
	  appLog("Intiated method to click on First Deposit account");
	  SelectAccountsOnDashBoard("Deposit");
	  //appLog("Successfully Clicked on First Deposit account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstLoanAccount(){
	
	  appLog("Intiated method to click on First Loan account");
	  SelectAccountsOnDashBoard("Loan");
	  //appLog("Successfully Clicked on First Loan account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstAvailableAccount(){
	
	  appLog("Intiated method to click on First available account on DashBoard");
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	  kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	  appLog("Successfully Clicked on First available account on DashBoard");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickOnSearch_AccountDetails(){
	
	  appLog("Intiated method to click on Search Glass Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Seach Glass Icon");
	}
	
	async function selectTranscationtype(TransactionType){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], TransactionType);
	  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
	}
	
	async function enterKeywordtoSearch(keyword){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtKeyword"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtKeyword"],keyword);
	  appLog("Successfully entered keyword for search : <b>"+keyword+"</b>");
	}
	
	async function selectAmountRange(AmountRange1,AmountRange2){
	
	  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
	  appLog("Successfully selected amount range From");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
	  appLog("Successfully selected amount range To");
	
	  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");
	}
	
	async function selectCustomdate(){
	
	  appLog("Intiated method to select Custom Date Range");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
	  appLog("Successfully selected Date Range");
	}
	
	async function selectTimePeriod(){
	
	  appLog("Intiated method to select custom timeperiod");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	  appLog("Successfully selected Time period");
	}
	
	async function clickOnAdvancedSearchBtn(){
	
	  appLog("Intiated method to click on Search Button with given search criteria");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function validateSearchResult() {
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	}
	
	async function scrolltoTranscations_accountDetails(){
	
	  appLog("Intiated method to scroll to Transcations under account details");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	}
	
	async function VerifyAdvancedSearch_byAmount(AmountRange1,AmountRange2){
	
	  await clickOnSearch_AccountDetails();
	  await selectAmountRange(AmountRange1,AmountRange2);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byTranxType(TransactionType){
	
	  await clickOnSearch_AccountDetails();
	  await selectTranscationtype(TransactionType);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byDate(){
	
	  await clickOnSearch_AccountDetails();
	  await selectTimePeriod();
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function VerifyAdvancedSearch_byKeyword(keyword){
	
	  await clickOnSearch_AccountDetails();
	  await enterKeywordtoSearch(keyword);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function MoveBackToLandingScreen_AccDetails(){
	
	  appLog("Intiated method to Move back to Account Dashboard from AccountsDetails");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	async function VerifyAccountOnDashBoard(AccountType){
	
	  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        appLog("Successfully verified : <b>"+accountName+"</b>");
	        myList.push("TRUE");
	        finished = true;
	        break;
	      }else{
	        myList.push("FALSE");
	      }
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	
	async function VerifyCheckingAccountonDashBoard(){
	
	  appLog("Intiated method to verify Checking account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
	  VerifyAccountOnDashBoard("Checking");
	}
	
	async function VerifySavingsAccountonDashBoard(){
	
	  appLog("Intiated method to verify Savings account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
	  VerifyAccountOnDashBoard("Saving");
	}
	async function VerifyCreditCardAccountonDashBoard(){
	
	  appLog("Intiated method to verify CreditCard account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
	  VerifyAccountOnDashBoard("Credit");
	}
	
	async function VerifyDepositAccountonDashBoard(){
	
	  appLog("Intiated method to verify Deposit account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
	  VerifyAccountOnDashBoard("Deposit");
	}
	
	async function VerifyLoanAccountonDashBoard(){
	
	  appLog("Intiated method to verify Loan account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
	  VerifyAccountOnDashBoard("Loan");
	}
	
	async function verifyViewAllTranscation(){
	
	  appLog("Intiated method to view all Tranx in AccountDetails");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	}
	
	
	async function verifyContextMenuOptions(myList_Expected){
	
	  //var myList_Expected = new Array();
	  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	  myList_Expected.push(myList_Expected);
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  var myList = new Array();
	
	  for(var x = 0; x <segLength-1; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+options);
	    myList.push(options);
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	
	  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	  //appLog("isFounded"+isFounded);
	  expect(isFounded).toBe(true);
	}
	
	async function MoveBackToLandingScreen_Accounts(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	}
	
	
	async function verifyAccountSummary_CheckingAccounts(){
	
	  appLog("Intiated method to verify account summary for Checking Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_DepositAccounts(){
	
	  appLog("Intiated method to verify account summary for Deposit Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_CreditCardAccounts(){
	
	  appLog("Intiated method to verify account summary for CreditCard Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	
	}
	
	async function verifyAccountSummary_LoanAccounts(){
	
	  appLog("Intiated method to verify account summary for Loan Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_SavingsAccounts(){
	
	  appLog("Intiated method to verify account summary for Savings Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function VerifyPendingWithdrawls_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab1"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab1"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"], "text")).not.toBe("");
	
	}
	
	async function VerifyInterestdetails_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab2"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab2"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab2"], "text")).not.toBe("");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl7Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl7Tab2"], "text")).not.toBe("");
	
	}
	
	async function VerifySwiftCode_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab3"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab3"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab3"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab3"], "text")).not.toBe("");
	}
	
	async function selectContextMenuOption(Option){
	
	  appLog("Intiated method to select context menu option :: "+Option);
	
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	  var segLength=accounts_Size.length;
	  appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment will be :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg],15000);
	    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    appLog("Menu Item Text is :: "+TransfersText);
	    if(TransfersText===Option){
	      appLog("Option to be selected is :"+TransfersText);
	       await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes"],15000);
	      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
	      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
	      await kony.automation.playback.wait(10000);
	      myList.push("TRUE");
	      break;
	    }else{
	      myList.push("FALSE");
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	
	  expect(Status).toBe(true,"Failed to click on option <b>"+Option+"</b>");
	}
	
	// async function SelectContextualOnDashBoard(AccountType){
	
	//   appLog("Intiated method to analyze accounts data Dashboard");
	
	//   var Status=false;
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	//   var segLength=accounts_Size.length;
	
	//   var finished = false;
	//   for(var x = 0; x <segLength && !finished; x++) {
	
	//     var segHeaders="segAccounts["+x+",-1]";
	
	//     var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	//     var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	//     //appLog('Sub accounts size is '+subaccounts_Length);
	
	//     for(var y = 0; y <subaccounts_Length; y++){
	
	//       var seg="segAccounts["+x+","+y+"]";
	
	//       var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	//       var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	//       if(typeOfAccount.includes(AccountType)){
	//         await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
	//         kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	//         appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	//         await kony.automation.playback.wait(5000);
	//         finished = true;
	//         Status=true;
	//         break;
	//       }
	//     }
	//   }
	
	//   expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
	// }
	
	async function SelectContextualOnDashBoard(AccountNumber){
	
	  appLog("Intiated method to select Menu of account : "+AccountNumber);
	  await kony.automation.playback.wait(5000);
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      appLog("Account Name is : "+accountName);
	      if(accountName.includes(AccountNumber)){
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(5000);
	        // Validate really list is displayed or not
	        var menuList=await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	        expect(menuList).toBe(true,"Failed to display contextual menu list items");
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on Menu of AccNumber: <b>"+AccountNumber+"</b>");
	}
	
	async function verifyVivewStatementsHeader(){
	
	  appLog('Intiated method to verify account statement header');
	  var Status=await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
	  //kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");
	  expect(Status).toBe(true,"Failed to Navigate to Account Statements");
	  //appLog('Successfully Verified Account Statement Header');
	}
	
	async function VerifyPostedPendingTranscation(){
	
	  appLog('Intiated method to verify Pending and Posted Transcations');
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  var TransType=kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","segTransactions[0,-1]","lblTransactionHeader"], "text");
	  if(TransType.includes("Posted")||TransType.includes("Pending")){
	    //kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    //await kony.automation.playback.wait(5000);
	    appLog('Transfer Type is : '+TransType);
	  }else{
	    appLog('No Pending or Posted Transcation available');
	  }
	
	}
	
	
	async function VerifyStopChequePaymentScreen(){
	  
	  appLog("Intiated method to verify StopChequePayment Screen");
	  var Status=await kony.automation.playback.waitFor(["frmStopPayments","lblChequeBookRequests"],30000);
	  expect(Status).toBe(true,"Failed to Verify StopChequePayment screen");
	  appLog("Successfully Verified StopChequePayment");
	  
	}
	
	async function VerifyAccountAlertScreen(){
	  
	  appLog("Intiated method to verify AccountAlert Screen");
	  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAlertsHeading"],30000);
	  expect(Status).toBe(true,"Failed to Verify AccountAlert screen");
	  appLog("Successfully Verified AccountAlert");
	}
	
	async function MoveBack_Accounts_StopChequeBook(){
	  
	  appLog("Intiated method to Move back to Account Dashboard from StopPayments");
	  await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	
	}
	
	async function MoveBack_Accounts_ProfileManagement(){
	
	  // Move back to base state
	  appLog("Intiated method to Move back to Account Dashboard from ProfileManagement");
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	
	async function NavigateToProfileSettings(){
	
	  appLog("Intiated method to Navigate to Personal Details");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);
	  await kony.automation.playback.wait(15000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPersonalDetailsHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPersonalDetailsHeading"], "text")).toEqual("Personal Details");
	
	  appLog("Successfully Navigated to Personal Details");
	}
	
	async function NavigateToAlertSettings(){
	
	  appLog("Intiated method to Navigate to Alert Settings");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings4flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAlertsHeading"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAlertsHeading"], "text")).toEqual("Security");
	  
	  appLog("Successfully Navigated to Alert Settings");
	}
	
	async function selectProfileSettings_PhoneNumber(){
	
	  appLog("Intiated method to Navigate to PhoneNumber Flex");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxPhone"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxPhone"]);
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPhoneNumbersHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPhoneNumbersHeading"], "text")).toEqual("Phone Number");
	
	  appLog("Successfully Navigated to PhoneNumber Flex");
	}
	
	async function selectProfileSettings_EmailAddress(){
	
	  appLog("Intiated method to Navigate to EmailID Flex");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEmail"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEmail"]);
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEmailHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEmailHeading"], "text")).toEqual("Email");
	
	  appLog("Successfully Navigated to EmailID Flex");
	}
	
	async function selectProfileSettings_Address(){
	
	  appLog("Intiated method to Navigate to Address Flex");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddress"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddress"]);
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddressHeading"], "text")).toEqual("Address");
	
	  appLog("Successfully Navigated to Address Flex");
	}
	
	async function ProfileSettings_addNewPhoneNumberDetails(phoneNumber,isPrimary){
	
	  appLog("Intiated method to addNewPhoneNumberDetails");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddPhoneNumberHeading"], "text")).toEqual("Add Phone Number");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxAddPhoneNumberType"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Other");
	  appLog("Successfully Selected PhoneNumber Type");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],"91");
	  appLog("Successfully Entered CountryCode");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumber"],phoneNumber);
	  appLog("Successfully Entered Phone Number as : <b>"+phoneNumber+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakePrimayPhoneNumbercheckBox();
	  }
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberSave"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
	  appLog("Successfully clicked on SAVE button");
	  await kony.automation.playback.wait(5000);
	
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	    var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	    var segLength=seg_Size.length;
	    //appLog("Length is :: "+segLength);
	    for(var x = 0; x <segLength; x++){
	      var seg="segPhoneNumbers["+x+"]";
	      //appLog("Segment is :: "+seg);
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
	      var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
	      appLog("Phone Number Text is :: "+phonenum);
	      if(phonenum===phoneNumber){
	        appLog("Successfully Added Mobile Number "+phoneNumber);
	        Status="TRUE";
	        break;
	      }
	    }
	  }catch(Exception){
	
	  }finally{
	    expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	  }
	
	}
	
	async function ProfileSettings_addInvalidPhoneNumberDetails(phoneNumber,isPrimary){
	
	  appLog("Intiated method to addNewPhoneNumberDetails");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddPhoneNumberHeading"], "text")).toEqual("Add Phone Number");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxAddPhoneNumberType"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxAddPhoneNumberType"], "Other");
	  appLog("Successfully Selected PhoneNumber Type");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumberCountryCode"],"91");
	  appLog("Successfully Entered CountryCode");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddPhoneNumber"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddPhoneNumber"],phoneNumber);
	  appLog("Successfully Entered Phone Number as : <b>"+phoneNumber+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakePrimayPhoneNumbercheckBox();
	  }
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberSave"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberSave"]);
	  appLog("Successfully clicked on SAVE button");
	  await kony.automation.playback.wait(5000);
	
	  var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddPhoneNumberHeading"],15000);
	
	  if(isAddHeader){
	
	    appLog("Custom Message :: Invalid characters entered");
	
	  }else{
	    appLog("Accepted Invalid Characters");
	    fail("Custom Message :: Accepted Invalid Characters");
	  }
	
	
	}
	
	async function ProfileSettings_VerifyaddInvalidPhoneNumberFunctionality(phoneNumber,isPrimary){
	
	  appLog("Intiated method to VerifyaddNewPhoneNumberFunctionality");
	
	  var isAddNewNumber=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewNumber"],15000);
	  //appLog("Button status is :"+isAddNewNumber);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	  var segLength=seg_Size.length;
	  //appLog("PhoneNumbers size is :: "+segLength);
	
	  if(segLength<3&&isAddNewNumber){
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewNumber"]);
	    appLog("Successfully Clicked on Add New Phone Number Button");
	    await ProfileSettings_addInvalidPhoneNumberDetails(phoneNumber,isPrimary);
	  }else{
	    appLog("Maximum phone numbers already added");
	  }
	}
	
	async function selectMakePrimayPhoneNumbercheckBox(){
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAddCheckBox3"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAddCheckBox3"]);
	  appLog("Successfully Selected Entered Phone Number as Primary");
	}
	
	async function ProfileSettings_UpdatePhoneNumber(updatedPhonenum){
	
	  // Update Number
	  appLog("Intiated method to Update PhoneNumberDetails");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	
	  var segLength1=accounts_Size1.length;
	  kony.print("Segment length is :"+segLength1);
	
	  var isEditble=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers[0]","btnViewDetail"],15000);
	  if(isEditble){
	
	    kony.automation.button.click(["frmProfileManagement","settings","segPhoneNumbers[0]","btnViewDetail"]);
	    appLog("Successfully clicked on ViewDetails button");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxPhoneNumber"]);
	    kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxPhoneNumber"],updatedPhonenum);
	    appLog("Successfully Updated Phone number as : <b>"+updatedPhonenum+"</b>");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
	    kony.automation.button.click(["frmProfileManagement","settings","btnEditPhoneNumberSave"]);
	    appLog("Successfully clicked on SAVE button");
	    await kony.automation.playback.wait(5000);
	
	    try{
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	      var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	      var segLength=seg_Size.length;
	      //appLog("Length is :: "+segLength);
	      for(var x = 0; x <segLength; x++){
	        var seg="segPhoneNumbers["+x+"]";
	        //appLog("Segment is :: "+seg);
	        await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
	        var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
	        appLog("Phone Number Text is :: "+phonenum);
	        if(phonenum===updatedPhonenum){
	          appLog("Successfully Updated Mobile Number to : "+updatedPhonenum);
	          Status="TRUE";
	          break;
	        }
	      }
	    }catch(exception){
	      Status="FALSE";
	    }finally{
	      expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	    }
	
	  }else{
	    appLog("Unable to Update PhoneNumberDetails");
	  }
	}
	
	async function ProfileSettings_DeletePhoneNumber(phoneNumber){
	
	  //Delete already added Mobile Number
	  appLog("Intiated method to Delete PhoneNumberDetails");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	  var segLength=seg_Size.length;
	  //appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++){
	    var seg="segPhoneNumbers["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblPhoneNumber"],15000);
	    var phonenum=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblPhoneNumber"], "text");
	    appLog("Phone Number Text is :: "+phonenum);
	    if(phonenum===phoneNumber){
	      appLog("Phone Number to be Deleted is : "+phoneNumber);
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
	      appLog("Successfully Clicked on Delete Button");
	
	      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
	      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
	      appLog("Successfully Clicked on YES Button");
	      await kony.automation.playback.wait(5000);
	      break;
	    }
	  } 
	}
	
	async function ProfileSettings_VerifyaddNewPhoneNumberFunctionality(phoneNumber,isPrimary){
	
	  appLog("Intiated method to VerifyaddNewPhoneNumberFunctionality");
	
	  var isAddNewNumber=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewNumber"],15000);
	  //appLog("Button status is :"+isAddNewNumber);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segPhoneNumbers"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segPhoneNumbers"],"data");
	  var segLength=seg_Size.length;
	  //appLog("PhoneNumbers size is :: "+segLength);
	
	  if(segLength<3&&isAddNewNumber){
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewNumber"]);
	    appLog("Successfully Clicked on Add New Phone Number Button");
	    await ProfileSettings_addNewPhoneNumberDetails(phoneNumber,isPrimary);
	
	    // No Need to delete after adding
	    
	//     var Header=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblPhoneNumbersHeading"],15000);
	//     if(Header&&isPrimary==='NO'){
	//       await ProfileSettings_DeletePhoneNumber(phoneNumber);
	//     }else{
	//       appLog("Custom Message :: Update Customer Details Failed");
	//       fail("Custom Message :: Custom Message :: Update Customer Details Failed");
	//     }
	     // No Need to delete after adding
	
	  }else{
	    appLog("Maximum phone numbers already added");
	  }
	}
	
	async function ProfileSettings_addNewAddressDetails(addressLine1,addressLine2,zipcode,isPrimary){
	
	  appLog("Intiated method to addNewAddressDetails");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine1"],addressLine1);
	  appLog("Successfully Entered AddressLine1 : <b>"+addressLine1+"</b>");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAddressLine2"],addressLine2);
	  appLog("Successfully Entered AddressLine2 : <b>"+addressLine2+"</b>");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCountry"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCountry"], "IN");
	  appLog("Successfully Selected Country Code");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxState"],15000);
	  kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxState"], "IN-TG");
	  appLog("Successfully Selected State Code");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","txtCity"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","txtCity"],"HYD");
	  appLog("Successfully Selected City Code");
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxZipcode"],zipcode);
	  appLog("Successfully Entered Zipcode : <b>"+zipcode+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakeDefaultAddresscheckBox();
	  }
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddressAdd"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddressAdd"]);
	  appLog("Successfully Clicked on Add Address Button");
	  await kony.automation.playback.wait(5000);
	
	  //   var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewAddressHeader"],15000);
	
	  //   if(isAddHeader){
	  //     appLog("Custom Message :: Update Customer Details Failed");
	  //     fail("Custom Message :: Update Customer Details Failed");
	
	  //   }else{
	  //     appLog("Successfully Added new Address details");
	  //   }
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	    var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	    var segLength=accounts_Size.length;
	    for(var x = 0; x <segLength; x++) {
	      var seg="segAddresses["+x+"]";
	      //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblAddressLine1"],15000);
	      var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblAddressLine1"], "text");
	      appLog("Address Text is :: "+address1);
	      if(address1===addressLine1){
	        appLog("Successfully added Address "+addressLine1);
	        Status="TRUE";
	        break;
	      }
	    }
	  }catch(Exception){
	    Status="FALSE";
	  }finally{
	    expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	  }
	}
	
	
	async function selectMakeDefaultAddresscheckBox(){
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxSetAsPreferredCheckBox"]);
	  appLog("Successfully Selected Entered Address as Primary");
	}
	
	async function ProfileSettings_UpdateAddress(UpdatedZip){
	
	  // Update Address
	  appLog("Intiated method to update Address Details");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"]);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segAddresses["+x+"]";
	    var ismailing=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblCommunicationAddress"], "text");
	    appLog("is Mailing Address : "+ismailing);
	    if(ismailing===""){
	
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnEdit"]);
	      appLog("Successfully clicked on Edit button");
	      await kony.automation.playback.wait(5000);
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditZipcode"],15000);
	      kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditZipcode"],UpdatedZip);
	      appLog("Successfully Entered Updated Zipcode as : <b>"+UpdatedZip+"</b>");
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAddressSave"],15000);
	      kony.automation.button.click(["frmProfileManagement","settings","btnEditAddressSave"]);
	      appLog("Successfully clicked on SAVE button");
	      await kony.automation.playback.wait(5000);
	
	      try{
	        await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	        var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	        var segLength1=accounts_Size1.length;
	        for(var x1 = 0; x1 <segLength1; x1++) {
	          var seg1="segAddresses["+x1+"]";
	          //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
	          await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"lblAddressLine1"],15000);
	          var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"lblAddressLine1"], "text");
	          appLog("Address Text is :: "+address1);
	          if(address1===addressLine1){
	            appLog("Successfully Updated Address to : "+addressLine1);
	            Status="TRUE";
	            break;
	          }
	        }
	
	      }catch(Exception){
	        Status="FALSE";
	      }finally{
	        expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	      }
	
	    }
	    break;
	  }
	
	}
	
	async function ProfileSettings_DeleteAddress(addressLine1){
	
	  // Delete Address
	  appLog("Intiated method to Delete Address Details");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segAddresses["+x+"]";
	    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblAddressLine1"],15000);
	    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblAddressLine1"], "text");
	    appLog("Address Text is :: "+address1);
	    if(address1===addressLine1){
	      appLog("Address to be Deleted is : "+addressLine1);
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
	      appLog("Successfully clicked on DELETE button");
	      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
	      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
	      appLog("Successfully clicked on YES button");
	      await kony.automation.playback.wait(5000);
	      break;
	    }
	  }
	
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	    var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	
	    var segLength1=accounts_Size1.length;
	    for(var x1 = 0; x1 <segLength1; x1++) {
	      var seg1="segAddresses["+x1+"]";
	      //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses[1]","lblAddressLine1"], "text")).toEqual("SOME");
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"lblAddressLine1"],15000);
	      var address2=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"lblAddressLine1"], "text");
	      appLog("Address Text is :: "+address2);
	      if(address2===addressLine1){
	        appLog("Address is not deleted");
	        Status="TRUE";
	        break;
	      }
	    }
	
	  }catch(Exception){
	    Status="FALSE";
	  }finally{
	    expect(Status).toContain("FALSE", "Custom Message :: Unable to delete address");
	  }
	
	}
	
	async function ProfileSettings_VerifyaddNewAddressFunctionality(addressLine1,addressLine2,zipcode,isPrimary){
	
	
	  appLog("Intiated method to VerifyaddNewAddressFunctionality");
	
	  var isAddNewAddress=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewAddress"],15000);
	  //appLog("Button status is :"+isAddNewAddress);
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAddresses"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segAddresses"],"data");
	  var segLength=seg_Size.length;
	  //appLog("Address size is :: "+segLength);
	
	  if(segLength<3&&isAddNewAddress){
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewAddress"]);
	    appLog("Successfully clicked on AddNewAddress button");
	    await ProfileSettings_addNewAddressDetails(addressLine1,addressLine2,zipcode,isPrimary);
	
	    // No Need to delete address after adding
	//     var Header=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddressHeading"],15000);
	//     if(Header&&isPrimary==='NO'){
	//       await ProfileSettings_DeleteAddress(addressLine1);
	//     }else{
	//       appLog("Custom Message :: Update Customer Details Failed");
	//       fail("Custom Message :: Update Customer Details Failed");
	//     }
	    // No Need to delete address after adding
	
	  }else{
	    appLog("Maximum Address already added");
	  }
	
	}
	
	async function ProfileSettings_addEmailAddressDetails(emailAddress,isPrimary){
	
	  // Add new email ID
	  appLog("Intiated method to add new Emai Address Details");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewEmailHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddNewEmailHeading"], "text")).toEqual("Add New Email");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEmailId"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEmailId"],emailAddress);
	  appLog("Successfully Entered new Email Address : <b>"+emailAddress+"</b>");
	
	  if(isPrimary==='YES'){
	
	    await selectMakePrimayEmailIDcheckBox();
	  }
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddEmailIdAdd"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnAddEmailIdAdd"]);
	  appLog("Successfully clicked on Add Button");
	  await kony.automation.playback.wait(5000);
	
	  //   var isAddHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewEmailHeading"],15000);
	
	  //   if(isAddHeader){
	  //     appLog("Custom Message :: Update Customer Details Failed");
	  //     fail("Custom Message :: Update Customer Details Failed");
	  //   }else{
	  //     appLog("Successfully added new Email Address");
	  //   }
	
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segEmailIds["+x+"]";
	    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblEmail"],15000);
	    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblEmail"], "text");
	    appLog("Email Text is :: "+address1);
	    if(address1===emailAddress){
	      appLog("Email Number to be Deleted is : "+emailAddress);
	      Status="TRUE";
	      break;
	    }
	  }
	
	  }catch(Exception){
	    Status="FALSE";
	  }finally{
	    expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	  }
	}
	
	async function selectMakePrimayEmailIDcheckBox(){
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxMarkAsPrimaryEmailCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxMarkAsPrimaryEmailCheckBox"]);
	  appLog("Successfully Selected Entered EmailID as Primary");
	}
	
	async function ProfileSettings_UpdateEmailAddress(updatedemailid){
	
	  // Update email ID
	  appLog("Intiated method to Update Email address");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength1=accounts_Size1.length;
	  appLog("Length is :"+segLength1);
	
	  var isEditble=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds[0]","btnEdit"],15000);
	  if(isEditble){
	    kony.automation.button.click(["frmProfileManagement","settings","segEmailIds[0]","btnEdit"]);
	    appLog("Successfully clicked on Edit Button");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditEmailId"],15000);
	    kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditEmailId"],updatedemailid);
	    appLog("Successfully Entered Updated Email ID : <b>"+updatedemailid+"</b>");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditEmailIdSave"],15000);
	    kony.automation.button.click(["frmProfileManagement","settings","btnEditEmailIdSave"]);
	    appLog("Successfully Clicked on SAVE button");
	    await kony.automation.playback.wait(5000);
	
	    //     var isEditHeader=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditEmailHeading"],15000);
	
	    //     if(isEditHeader){
	    //       appLog("Custom Message :: Update Customer Details Failed");
	    //       fail("Custom Message :: Update Customer Details Failed");
	    //     }else{
	    //       appLog("Successfully Updated email Address");
	    //     }
	
	    try{
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	    var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	    var segLength=accounts_Size.length;
	    for(var x = 0; x <segLength; x++) {
	      var seg="segEmailIds["+x+"]";
	      //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
	      await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblEmail"],15000);
	      var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblEmail"], "text");
	      appLog("Email Text is :: "+address1);
	      if(address1===updatedemailid){
	        appLog("Successfully updated EmailAddress : "+updatedemailid);
	        Status="TRUE";
	        break;
	      }
	    }
	
	    }catch(Exception){
	       Status="FALSE";
	    }finally{
	       expect(Status).toContain("TRUE", "Custom Message :: Update Customer Details Failed");
	    }
	  }
	
	}
	
	async function ProfileSettings_deleteEmailAddressDetails(emailAddress){
	
	  // Delete Address
	  appLog("Intiated method to Delete Email Details");
	
	  var Status="FALSE";
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength=accounts_Size.length;
	  for(var x = 0; x <segLength; x++) {
	    var seg="segEmailIds["+x+"]";
	    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"lblEmail"],15000);
	    var address1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"lblEmail"], "text");
	    appLog("Email Text is :: "+address1);
	    if(address1===emailAddress){
	      appLog("Email Number to be Deleted is : "+emailAddress);
	      kony.automation.button.click(["frmProfileManagement","settings",seg,"btnDelete"]);
	      appLog("Successfully Clicked on Delete Button");
	      await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"],15000);
	      kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
	      appLog("Successfully Clicked on YES Button");
	      await kony.automation.playback.wait(5000);
	      break;
	    }
	  }
	
	  try{
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	
	  var segLength1=accounts_Size1.length;
	  for(var x1 = 0; x1 <segLength1; x1++) {
	    var seg1="segEmailIds["+x1+"]";
	    //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds[0]","lblEmail"], "text")).toEqual("HH");
	    await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"lblEmail"],15000);
	    var address2=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"lblEmail"], "text");
	    appLog("Email Text is :: "+address2);
	    if(address2===emailAddress){
	      appLog("Failed to delete EmailAddress : "+emailAddress);
	      Status="TRUE";
	      break;
	    }
	  }
	
	  }catch(Exception){
	    Status="FALSE";
	  }finally{
	    expect(Status).toContain("FALSE", "Custom Message :: Unable to delete Email");
	  }
	}
	
	async function ProfileSettings_VerifyaddEmailAddressFunctionality(emailAddress,isPrimary){
	
	  appLog("Intiated method to VerifyaddEmailAddressFunctionality");
	
	  var isAddEmailAddress=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewEmail"],15000);
	  //appLog("Button status is :"+isAddEmailAddress);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"],15000);
	  var seg_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");
	  var segLength=seg_Size.length;
	  //appLog("email size is :: "+segLength);
	
	  if(segLength<3&&isAddEmailAddress){
	
	    kony.automation.button.click(["frmProfileManagement","settings","btnAddNewEmail"]);
	    appLog("Successfully clicked on NewEmail Button");
	    await ProfileSettings_addEmailAddressDetails(emailAddress,isPrimary);
	
	    // No need to delete after adding
	//     var Header= await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEmailHeading"],15000);
	//     if(Header&&isPrimary==='NO'){
	//       await ProfileSettings_deleteEmailAddressDetails(emailAddress);
	//     }else{
	
	//       appLog("Custom Message :: Update Customer Details Failed");
	//       fail("Custom Message :: Custom Message :: Update Customer Details Failed");
	//       //await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddPhoneNumberCancel"]);
	//       //kony.automation.button.click(["frmProfileManagement","settings","btnAddPhoneNumberCancel"]);
	//     }
	       // No need to delete after adding
	
	  }else{
	    appLog("Maximum Email Address already added");
	  }
	
	}
	
	
	async function MoveBackToDashBoard_ProfileManagement(){
	
	  // Move back to base state
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function NavigateToAccountSettings(){
	
	  appLog("Intiated method to navigate to Account Settings");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAccountsHeader"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAccountsHeader"], "text")).toEqual("Accounts");
	
	  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
	  expect(Status).toBe(true,"FAILED to load Accounts segment");
	  appLog("Successfully Navigated to AccountSettings");
	}
	
	
	async function clickonDefaultAccountstab(){
	
	  appLog("Intiated method to click on DefaultAccountstab");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxSetDefaultAccount"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxSetDefaultAccount"]);
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblDefaultTransactionAccounttHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblDefaultTransactionAccounttHeading"], "text")).toEqual("Default Transaction Accounts");
	
	  appLog("Successfully clicked on DefaultAccountstab");
	
	  await kony.automation.playback.wait(5000);
	}
	async function clickonAccountPreferencetab(){
	
	  appLog("Intiated method to click on AccountPreferencetab");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxAccountPreferences"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxAccountPreferences"]);
	
	  appLog("Successfully clicked on AccountPreferencetab");
	
	  await kony.automation.playback.wait(5000);
	}
	
	
	async function EditFavAccountPreferences(){
	
	  appLog("Intiated method to Edit FavAccountPreferences");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
	  kony.automation.button.click(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Successfully Clicked on Edit button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAccountsHeader"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEditAccountsHeader"], "text")).toEqual("Edit Account");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAccountNickNameValue"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAccountNickNameValue"],'My Checking');
	  appLog("Successfully Updated NickName value");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAccountsSave"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnEditAccountsSave"]);
	  appLog("Successfully Clicked on SAVE button");
	}
	
	async function CancelFavAccountPreferences(){
	
	  appLog("Intiated method to cancel FavAccountPreferences");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
	  kony.automation.button.click(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Successfully Clicked on Edit button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAccountsHeader"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEditAccountsHeader"], "text")).toEqual("Edit Account");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxAccountNickNameValue"],15000);
	  kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxAccountNickNameValue"],'My Checking');
	  appLog("Successfully Updated NickName value");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAccountsCancel"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnEditAccountsCancel"]);
	  appLog("Successfully Clicked on CANCEL button");
	}
	
	async function SetDefaultAccountPreferences(){
	
	  appLog("Intiated method to Set DefaultAccountPreferences Tab");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"],15000);
	  kony.automation.button.click(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Edit Button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblSelectedDefaultAccounts"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblSelectedDefaultAccounts"], "text")).not.toBe('');
	
	  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxBillPay"],15000);
	  //   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxBillPay"], "190128223241502");
	  //   appLog("Successfully Selected Default BillPay acc");
	
	  //   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lbxCheckDeposit"],15000);
	  //   kony.automation.listbox.selectItem(["frmProfileManagement","settings","lbxCheckDeposit"], "190128223242830");
	  //   appLog("Successfully Selected Default Deposit acc");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","settings","btnDefaultTransactionAccountEdit"]);
	  appLog("Successfully Clicked on SAVE Button");
	
	  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblSelectedDefaultAccounts"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblSelectedDefaultAccounts"], "text")).not.toBe('');
	  appLog("Successfully Verified Default accounts");
	}
	
	async function EnableDisableSecurityAlerts(){
	  
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAlerts"],15000);
		kony.automation.button.click(["frmProfileManagement","settings","btnEditAlerts"]);
	    appLog("Successfully clicked on Edit button");
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblStatus1"],15000);
		kony.automation.widget.touch(["frmProfileManagement","settings","lblStatus1"], null,null,[29,15]);
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEnableSwitch"],15000);
		kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEnableSwitch"]);
	    appLog("Successfully clicked on Radio button");
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblStatus1"],15000);
		kony.automation.widget.touch(["frmProfileManagement","settings","lblStatus1"], null,null,[14,17]);
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEnableSwitch"],15000);
		kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEnableSwitch"]);
	    appLog("Successfully clicked on Radio button");
		await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAlertSave"],15000);
		kony.automation.button.click(["frmProfileManagement","settings","btnAlertSave"]);
		await kony.automation.playback.wait(10000);
	    appLog("Successfully clicked on SAVE button");
		await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
		kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	}
	
	
	
	it("VerifyAddingNewAddress", async function() {
	  
	  var isPrimary='NO';
	  
	  var addressLine1=Settings.address.addressLine1+getRandomString(3);
	  var addressLine2=Settings.address.addressLine2+getRandomString(3);
	  var zipcode=Settings.address.zipcode+getRandomNumber(3);
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_Address();
	  await ProfileSettings_VerifyaddNewAddressFunctionality(addressLine1,addressLine2,zipcode,isPrimary);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("VerifyAddingNewPhoneno", async function() {
	  
	  var phoneNumber=Settings.phone.phoneNumber+getRandomNumber(3);
	  var isPrimary='NO';
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_PhoneNumber();
	  await ProfileSettings_VerifyaddNewPhoneNumberFunctionality(phoneNumber,isPrimary);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("VerifyAddnewEmailID", async function() {
	  
	  var isPrimary='NO';
	  var emailid=Settings.email.emailAddress+getRandomString(3)+"@infinity.com";
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_EmailAddress();
	  await ProfileSettings_VerifyaddEmailAddressFunctionality(emailid,isPrimary);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("VerifyDeleteAddress", async function() {
	  
	  var isPrimary='NO';
	  
	  var addressLine1=Settings.address.addressLine1+getRandomString(3);
	  var addressLine2=Settings.address.addressLine2+getRandomString(3);
	  var zipcode=Settings.address.zipcode+getRandomNumber(3);
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_Address();
	  await ProfileSettings_VerifyaddNewAddressFunctionality(addressLine1,addressLine2,zipcode,isPrimary);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("VerifyDeleteEmailID", async function() {
	  
	  var isPrimary='NO';
	  var emailid=Settings.email.emailAddress+getRandomString(3)+"@infinity.com";
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_EmailAddress();
	  await ProfileSettings_VerifyaddEmailAddressFunctionality(emailid,isPrimary);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("VerifyDeletePhoneno", async function() {
	  
	  var phoneNumber=Settings.phone.phoneNumber+getRandomNumber(3);
	  var isPrimary='NO';
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_PhoneNumber();
	  await ProfileSettings_VerifyaddNewPhoneNumberFunctionality(phoneNumber,isPrimary);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("VerifyInvalidCharsPhoneno", async function() {
	  
	  var phoneNumber="abcd"+getRandomString(5);
	  var isPrimary='NO';
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_PhoneNumber();
	  await ProfileSettings_VerifyaddInvalidPhoneNumberFunctionality(phoneNumber,isPrimary);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("VerifyUpdateAddress", async function() {
	  
	  var updatedZip=Settings.address.updatedZip;
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_Address();
	  await ProfileSettings_UpdateAddress(updatedZip);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("VerifyUpdatingNewEmailID", async function() {
	  
	  var updatedemailid=Settings.email.updatedemailid+getRandomString(3)+"@infinity.com";
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_EmailAddress();
	  await ProfileSettings_UpdateEmailAddress(updatedemailid);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("VerifyUpdatingNewPhoneno", async function() {
	  
	  var updatedPhonenum=Settings.phone.updatedPhonenum+getRandomNumber(3);
	  
	  await NavigateToProfileSettings();
	  await selectProfileSettings_PhoneNumber();
	  await ProfileSettings_UpdatePhoneNumber(updatedPhonenum);
	  await MoveBackToDashBoard_ProfileManagement();
	  await verifyAccountsLandingScreen();
	  
	},120000);
	
	it("EnableDisableSecurityAlert", async function() {
	  
	  await NavigateToAlertSettings();
	  await EnableDisableSecurityAlerts();
	  
	},120000);
});