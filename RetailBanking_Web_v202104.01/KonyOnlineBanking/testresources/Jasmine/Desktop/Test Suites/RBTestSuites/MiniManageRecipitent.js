describe("MiniManageRecipitent", function() {
	afterEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  appLog('Inside after Each function');
	
	  if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	    appLog('Already in dashboard');
	  }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	    appLog('Inside Login Screen');
	  }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmAccountsDetails');
	    kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastTransfers');
	    kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmReview","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmReview');
	    kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmConfirmTransfer');
	    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPastPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPastPaymentsNew');
	    kony.automation.flexcontainer.click(["frmPastPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmScheduledPaymentsNew');
	    kony.automation.flexcontainer.click(["frmScheduledPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmDirectDebits","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmDirectDebits');
	    kony.automation.flexcontainer.click(["frmDirectDebits","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastViewActivity","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastViewActivity');
	    kony.automation.flexcontainer.click(["frmFastViewActivity","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastManagePayee');
	    kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastP2P');
	    kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBulkPayees');
	    kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayee');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayment');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentConfirm');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentAcknowledgement');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmManagePayees');
	    kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayABill');
	    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillConfirm');
	    kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"])
	  }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayScheduled');
	    kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayee1');
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayeeInformation');
	    kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeDetails');
	    kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmVerifyPayee');
	    kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayHistory');
	    kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivation');
	    kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivationAcknowledgement');
	    kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmNotificationsAndMessages');
	    kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmOnlineHelp');
	    kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmContactUsPrivacyTandC');
	    kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmProfileManagement');
	    kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMFATransactions","customheader","topmenu","flxaccounts"],5000)){
	    appLog('***Moving back from frmMFATransactions****');
	    kony.automation.flexcontainer.click(["frmMFATransactions","customheader","topmenu","flxaccounts"]);
	  }else{
	    appLog("Form name is not available");
	  }
	
	
	
	},240000);
	
	//Before each with flags
	
	// beforeEach(function() {
	
	//   var flgDashboard =  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000);
	//   appLog("Dashboard : "+flgDashboard);
	//   if(flgDashboard){
	//     appLog("Nothing to Do");
	//   }
	//   //Accounts Related
	//   var flgAccountsDetails =  await kony.automation.playback.waitFor(["frmAccountsDetails"],5000);
	//   appLog("flgAccountsDetails : "+flgAccountsDetails);
	//   if(flgAccountsDetails){
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Transfers Related
	//   var flgFastTransfers =  await kony.automation.playback.waitFor(["frmFastTransfers"],5000);
	//   appLog("FastTransfers : "+flgFastTransfers);
	//   if(flgFastTransfers){
	//     kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgReview =  await kony.automation.playback.waitFor(["frmReview"],5000);
	//   appLog("frmReview : "+flgReview);
	//   if(flgReview){
	//     kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	//   }
	//   var flgConfirmTransfer =  await kony.automation.playback.waitFor(["frmConfirmTransfer"],5000);
	//   appLog("frmConfirmTransfer : "+flgConfirmTransfer);
	//   if(frmConfirmTransfer){
	//     kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastTransfersActivites =  await kony.automation.playback.waitFor(["frmFastTransfersActivites"],5000);
	//   appLog("flgFastTransfersActivites : "+flgFastTransfersActivites);
	//   if(flgFastTransfersActivites){
	//     kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	//   }
	
	//   //Manage payee Related
	//   var flgFastManagePayee =  await kony.automation.playback.waitFor(["frmFastManagePayee"],5000);
	//   appLog("flgFastManagePayee : "+flgFastManagePayee);
	//   if(flgFastManagePayee){
	//     kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastP2P =  await kony.automation.playback.waitFor(["frmFastP2P"],5000);
	//   appLog("flgFastP2P : "+flgFastP2P);
	//   if(flgFastP2P){
	//     kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	//   }
	
	//   //BillPay Related
	
	//   var flgBulkPayees =  await kony.automation.playback.waitFor(["frmBulkPayees"],5000);
	//   appLog("flgBulkPayees : "+flgBulkPayees);
	//   if(flgBulkPayees){
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   } 
	
	//   var flgMakeOneTimePayee =  await kony.automation.playback.waitFor(["frmMakeOneTimePayee"],5000);
	//   appLog("flgMakeOneTimePayee : "+flgMakeOneTimePayee);
	//   if(flgMakeOneTimePayee){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgMakeOneTimePayment =  await kony.automation.playback.waitFor(["frmMakeOneTimePayment"],5000);
	//   appLog("flgMakeOneTimePayment : "+flgMakeOneTimePayment);
	//   if(flgMakeOneTimePayment){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentConfirm =  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm"],5000);
	//   appLog("flgOneTimePaymentConfirm : "+flgOneTimePaymentConfirm);
	//   if(flgOneTimePaymentConfirm){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentAcknowledgement =  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],5000);
	//   appLog("flgOneTimePaymentAcknowledgement : "+flgOneTimePaymentAcknowledgement);
	//   if(flgOneTimePaymentAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgManagePayees =  await kony.automation.playback.waitFor(["frmManagePayees"],5000);
	//   appLog("flgManagePayees : "+flgManagePayees);
	//   if(flgManagePayees){
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayABill =  await kony.automation.playback.waitFor(["frmPayABill"],5000);
	//   appLog("flgPayABill : "+flgPayABill);
	//   if(flgPayABill){
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillConfirm =  await kony.automation.playback.waitFor(["frmPayBillConfirm"],5000);
	//   appLog("flgPayBillConfirm : "+flgPayBillConfirm);
	//   if(flgPayBillConfirm){
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillAcknowledgement =  await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],5000);
	//   appLog("flgPayBillAcknowledgement : "+flgPayBillAcknowledgement);
	//   if(flgPayBillAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayScheduled =  await kony.automation.playback.waitFor(["frmBillPayScheduled"],5000);
	//   appLog("flgBillPayScheduled : "+flgBillPayScheduled);
	//   if(flgBillPayScheduled){
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayee1 =  await kony.automation.playback.waitFor(["frmAddPayee1"],5000);
	//   appLog("flgAddPayee1 : "+flgAddPayee1);
	//   if(flgAddPayee1){
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayeeInformation =  await kony.automation.playback.waitFor(["frmAddPayeeInformation"],5000);
	//   appLog("flgAddPayeeInformation : "+flgAddPayeeInformation);
	//   if(flgAddPayeeInformation){
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeDetails =  await kony.automation.playback.waitFor(["frmPayeeDetails"],5000);
	//   appLog("flgPayeeDetails : "+flgPayeeDetails);
	//   if(flgPayeeDetails){
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgVerifyPayee =  await kony.automation.playback.waitFor(["frmVerifyPayee"],5000);
	//   appLog("flgVerifyPayee : "+flgVerifyPayee);
	//   if(flgVerifyPayee){
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeAcknowledgement =  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement"],5000);
	//   appLog("flgPayeeAcknowledgement : "+flgPayeeAcknowledgement);
	//   if(flgPayeeAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayHistory =  await kony.automation.playback.waitFor(["frmBillPayHistory"],5000);
	//   appLog("flgBillPayHistory : "+flgBillPayHistory);
	//   if(flgBillPayHistory){
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayActivation =  await kony.automation.playback.waitFor(["frmBillPayActivation"],5000);
	//   appLog("flgBillPayActivation : "+flgBillPayActivation);
	//   if(flgBillPayActivation){
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }
	
	
	//   //Messages Related
	
	
	//   var flgNotificationsAndMessages =  await kony.automation.playback.waitFor(["frmNotificationsAndMessages"],5000);
	//   appLog("flgNotificationsAndMessages : "+flgNotificationsAndMessages);
	//   if(flgNotificationsAndMessages){
	//     kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Post Login Related
	
	//   var flgOnlineHelp =  await kony.automation.playback.waitFor(["frmOnlineHelp"],5000);
	//   appLog("flgOnlineHelp : "+flgOnlineHelp);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	//   var flgContactUsPrivacyTandC =  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC"],5000);
	//   appLog("flgContactUsPrivacyTandC : "+flgContactUsPrivacyTandC);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Settings Related
	
	//   var flgProfileManagement =  await kony.automation.playback.waitFor(["frmProfileManagement"],5000);
	//   appLog("flgProfileManagement : "+flgProfileManagement);
	//   if(flgProfileManagement){
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	
	// });
	
	beforeEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  strLogger=[];
	  appLog('Inside before Each function');
	
	 if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	    appLog('Already in dashboard');
	  }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	    appLog('Inside Login Screen');
	  }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmAccountsDetails');
	    kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastTransfers');
	    kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmReview","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmReview');
	    kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmConfirmTransfer');
	    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPastPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPastPaymentsNew');
	    kony.automation.flexcontainer.click(["frmPastPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsNew","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmScheduledPaymentsNew');
	    kony.automation.flexcontainer.click(["frmScheduledPaymentsNew","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmDirectDebits","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmDirectDebits');
	    kony.automation.flexcontainer.click(["frmDirectDebits","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastViewActivity","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastViewActivity');
	    kony.automation.flexcontainer.click(["frmFastViewActivity","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastManagePayee');
	    kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmFastP2P');
	    kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBulkPayees');
	    kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayee');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmMakeOneTimePayment');
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentConfirm');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmOneTimePaymentAcknowledgement');
	    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmManagePayees');
	    kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayABill');
	    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillConfirm');
	    kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"])
	  }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayBillAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayScheduled');
	    kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayee1');
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmAddPayeeInformation');
	    kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeDetails');
	    kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmVerifyPayee');
	    kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmPayeeAcknowledgement');
	    kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayHistory');
	    kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivation');
	    kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	    appLog('Moving back from frmBillPayActivationAcknowledgement');
	    kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmNotificationsAndMessages');
	    kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmOnlineHelp');
	    kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmContactUsPrivacyTandC');
	    kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from frmProfileManagement');
	    kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmMFATransactions","customheader","topmenu","flxaccounts"],5000)){
	    appLog('***Moving back from frmMFATransactions****');
	    kony.automation.flexcontainer.click(["frmMFATransactions","customheader","topmenu","flxaccounts"]);
	  }else{
	    appLog("Form name is not available");
	  }
	
	
	},240000);
	
	//Before each with flags
	
	// beforeEach(function() {
	
	//   var flgDashboard =  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000);
	//   appLog("Dashboard : "+flgDashboard);
	//   if(flgDashboard){
	//     appLog("Nothing to Do");
	//   }
	//   //Accounts Related
	//   var flgAccountsDetails =  await kony.automation.playback.waitFor(["frmAccountsDetails"],5000);
	//   appLog("flgAccountsDetails : "+flgAccountsDetails);
	//   if(flgAccountsDetails){
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Transfers Related
	//   var flgFastTransfers =  await kony.automation.playback.waitFor(["frmFastTransfers"],5000);
	//   appLog("FastTransfers : "+flgFastTransfers);
	//   if(flgFastTransfers){
	//     kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgReview =  await kony.automation.playback.waitFor(["frmReview"],5000);
	//   appLog("frmReview : "+flgReview);
	//   if(flgReview){
	//     kony.automation.flexcontainer.click(["frmReview","customheadernew","flxAccounts"]);
	//   }
	//   var flgConfirmTransfer =  await kony.automation.playback.waitFor(["frmConfirmTransfer"],5000);
	//   appLog("frmConfirmTransfer : "+flgConfirmTransfer);
	//   if(frmConfirmTransfer){
	//     kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastTransfersActivites =  await kony.automation.playback.waitFor(["frmFastTransfersActivites"],5000);
	//   appLog("flgFastTransfersActivites : "+flgFastTransfersActivites);
	//   if(flgFastTransfersActivites){
	//     kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	//   }
	
	//   //Manage payee Related
	//   var flgFastManagePayee =  await kony.automation.playback.waitFor(["frmFastManagePayee"],5000);
	//   appLog("flgFastManagePayee : "+flgFastManagePayee);
	//   if(flgFastManagePayee){
	//     kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgFastP2P =  await kony.automation.playback.waitFor(["frmFastP2P"],5000);
	//   appLog("flgFastP2P : "+flgFastP2P);
	//   if(flgFastP2P){
	//     kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	//   }
	
	//   //BillPay Related
	
	//   var flgBulkPayees =  await kony.automation.playback.waitFor(["frmBulkPayees"],5000);
	//   appLog("flgBulkPayees : "+flgBulkPayees);
	//   if(flgBulkPayees){
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   } 
	
	//   var flgMakeOneTimePayee =  await kony.automation.playback.waitFor(["frmMakeOneTimePayee"],5000);
	//   appLog("flgMakeOneTimePayee : "+flgMakeOneTimePayee);
	//   if(flgMakeOneTimePayee){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgMakeOneTimePayment =  await kony.automation.playback.waitFor(["frmMakeOneTimePayment"],5000);
	//   appLog("flgMakeOneTimePayment : "+flgMakeOneTimePayment);
	//   if(flgMakeOneTimePayment){
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentConfirm =  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm"],5000);
	//   appLog("flgOneTimePaymentConfirm : "+flgOneTimePaymentConfirm);
	//   if(flgOneTimePaymentConfirm){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgOneTimePaymentAcknowledgement =  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],5000);
	//   appLog("flgOneTimePaymentAcknowledgement : "+flgOneTimePaymentAcknowledgement);
	//   if(flgOneTimePaymentAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgManagePayees =  await kony.automation.playback.waitFor(["frmManagePayees"],5000);
	//   appLog("flgManagePayees : "+flgManagePayees);
	//   if(flgManagePayees){
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayABill =  await kony.automation.playback.waitFor(["frmPayABill"],5000);
	//   appLog("flgPayABill : "+flgPayABill);
	//   if(flgPayABill){
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillConfirm =  await kony.automation.playback.waitFor(["frmPayBillConfirm"],5000);
	//   appLog("flgPayBillConfirm : "+flgPayBillConfirm);
	//   if(flgPayBillConfirm){
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayBillAcknowledgement =  await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],5000);
	//   appLog("flgPayBillAcknowledgement : "+flgPayBillAcknowledgement);
	//   if(flgPayBillAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayScheduled =  await kony.automation.playback.waitFor(["frmBillPayScheduled"],5000);
	//   appLog("flgBillPayScheduled : "+flgBillPayScheduled);
	//   if(flgBillPayScheduled){
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayee1 =  await kony.automation.playback.waitFor(["frmAddPayee1"],5000);
	//   appLog("flgAddPayee1 : "+flgAddPayee1);
	//   if(flgAddPayee1){
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgAddPayeeInformation =  await kony.automation.playback.waitFor(["frmAddPayeeInformation"],5000);
	//   appLog("flgAddPayeeInformation : "+flgAddPayeeInformation);
	//   if(flgAddPayeeInformation){
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeDetails =  await kony.automation.playback.waitFor(["frmPayeeDetails"],5000);
	//   appLog("flgPayeeDetails : "+flgPayeeDetails);
	//   if(flgPayeeDetails){
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgVerifyPayee =  await kony.automation.playback.waitFor(["frmVerifyPayee"],5000);
	//   appLog("flgVerifyPayee : "+flgVerifyPayee);
	//   if(flgVerifyPayee){
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgPayeeAcknowledgement =  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement"],5000);
	//   appLog("flgPayeeAcknowledgement : "+flgPayeeAcknowledgement);
	//   if(flgPayeeAcknowledgement){
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayHistory =  await kony.automation.playback.waitFor(["frmBillPayHistory"],5000);
	//   appLog("flgBillPayHistory : "+flgBillPayHistory);
	//   if(flgBillPayHistory){
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }
	
	//   var flgBillPayActivation =  await kony.automation.playback.waitFor(["frmBillPayActivation"],5000);
	//   appLog("flgBillPayActivation : "+flgBillPayActivation);
	//   if(flgBillPayActivation){
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }
	
	
	//   //Messages Related
	
	
	//   var flgNotificationsAndMessages =  await kony.automation.playback.waitFor(["frmNotificationsAndMessages"],5000);
	//   appLog("flgNotificationsAndMessages : "+flgNotificationsAndMessages);
	//   if(flgNotificationsAndMessages){
	//     kony.automation.flexcontainer.click(["frmNotificationsAndMessages","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Post Login Related
	
	//   var flgOnlineHelp =  await kony.automation.playback.waitFor(["frmOnlineHelp"],5000);
	//   appLog("flgOnlineHelp : "+flgOnlineHelp);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	//   var flgContactUsPrivacyTandC =  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC"],5000);
	//   appLog("flgContactUsPrivacyTandC : "+flgContactUsPrivacyTandC);
	//   if(flgOnlineHelp){
	//     kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	//   }
	
	//   //Settings Related
	
	//   var flgProfileManagement =  await kony.automation.playback.waitFor(["frmProfileManagement"],5000);
	//   appLog("flgProfileManagement : "+flgProfileManagement);
	//   if(flgProfileManagement){
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }
	
	
	
	// });
	
	
	async function verifyAccountsLandingScreen(){
	
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
	}
	
	async function SelectAccountsOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	
	  var Status=false;
	  
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
	        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(5000);
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	  
	  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnFirstCheckingAccount(){
	
	  appLog("Intiated method to click on First Checking account");
	  SelectAccountsOnDashBoard("Checking");
	  //appLog("Successfully Clicked on First Checking account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstSavingsAccount(){
	
	  appLog("Intiated method to click on First Savings account");
	  SelectAccountsOnDashBoard("Saving");
	  //appLog("Successfully Clicked on First Savings account");
	  //await kony.automation.playback.wait(5000);
	}
	
	
	async function clickOnFirstCreditCardAccount(){
	
	  appLog("Intiated method to click on First CreditCard account");
	  SelectAccountsOnDashBoard("Credit");
	  //appLog("Successfully Clicked on First CreditCard account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstDepositAccount(){
	
	  appLog("Intiated method to click on First Deposit account");
	  SelectAccountsOnDashBoard("Deposit");
	  //appLog("Successfully Clicked on First Deposit account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstLoanAccount(){
	
	  appLog("Intiated method to click on First Loan account");
	  SelectAccountsOnDashBoard("Loan");
	  //appLog("Successfully Clicked on First Loan account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnSearch_AccountDetails(){
	
	  appLog("Intiated method to click on Search Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Search Icon");
	}
	
	async function selectTranscationtype(TransactionType){
	
	  appLog("Intiated method to select Transcation type");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],TransactionType);
	  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
	}
	
	async function selectAmountRange(From,To){
	
	  appLog("Intiated method to select Amount Range");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],From);
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],To);
	
	  appLog("Successfully selected amount Range : ["+From+","+To+"]");
	}
	
	async function selectCustomdate(){
	
	  appLog("Intiated method to select Custom Date Range");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
	  appLog("Successfully selected Date Range");
	}
	
	async function clickOnbtnSearch(){
	
	  appLog("Intiated method to click on Search Button with given search criteria");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully Clicked on Search Button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function validateSearchResult() {
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	}
	
	async function MoveBackToLandingScreen_AccDetails(){
	
	  appLog("Move back to Account Dashboard from AccountsDetails");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	async function SelectContextualOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	  
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	  
	  expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnCheckingAccountContextMenu(){
	
	  appLog("Intiated method to click on Checking account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,0]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxMenu"]);
	  //   appLog("Successfully clicked on Checking account context Menu");
	
	  SelectContextualOnDashBoard("Checking");
	}
	
	async function clickOnSavingsAccountContextMenu(){
	
	  appLog("Intiated method to click on Saving account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,1]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,1]","flxMenu"]);
	  //   appLog("Successfully clicked on Saving account context Menu");
	  SelectContextualOnDashBoard("Saving");
	}
	
	async function clickOnCreditCardAccountContextMenu(){
	
	  appLog("Intiated method to click on CreditCard account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,2]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,2]","flxMenu"]);
	  //   appLog("Successfully clicked on CreditCard account context Menu");
	
	  SelectContextualOnDashBoard("Credit");
	}
	
	async function clickOnDepositAccountContextMenu(){
	
	  appLog("Intiated method to click on Deposit account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[0,3]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,3]","flxMenu"]);
	  //   appLog("Successfully clicked on Deposit account context Menu");
	
	  SelectContextualOnDashBoard("Deposit");
	}
	
	async function clickOnLoanAccountContextMenu(){
	
	  appLog("Intiated method to click on Loan account context Menu");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.scrollToWidget(["frmDashboard","accountList","segAccounts[1,0]"]);
	  //   kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[1,0]","flxMenu"]);
	  //   appLog("Successfully clicked on Loan account context Menu");
	
	  SelectContextualOnDashBoard("Loan");
	
	}
	
	async function verifyContextMenuOptions(myList_Expected){
	
	  //var myList_Expected = new Array();
	  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	  myList_Expected.push(myList_Expected);
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  var myList = new Array();
	
	  for(var x = 0; x <segLength-1; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+options);
	    myList.push(options);
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	
	  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	  //appLog("isFounded"+isFounded);
	  expect(isFounded).toBe(true);
	}
	async function MoveBackToLandingScreen_Accounts(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	}
	
	async function scrolltoTranscations_accountDetails(){
	
	  appLog("Intiated method to scroll to Transcations under account details");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	}
	
	async function verifyAccountSummary_CheckingAccounts(){
	
	  appLog("Intiated method to verify account summary for Checking Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_DepositAccounts(){
	
	  appLog("Intiated method to verify account summary for Deposit Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_CreditCardAccounts(){
	
	  appLog("Intiated method to verify account summary for CreditCard Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	
	}
	
	async function verifyAccountSummary_LoanAccounts(){
	
	  appLog("Intiated method to verify account summary for Loan Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_SavingsAccounts(){
	
	  appLog("Intiated method to verify account summary for Savings Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountsOrder_DashBoard(){
	
	  appLog("Intiated method to verify accounts order in Dashboard");
	
	  //Accounts Order can't be garunteed across different users. Hence checking all Types of accounts.
	  var myList = new Array();
	  var myList_Expected = new Array();
	  myList_Expected.push("Checking","Saving","Credit","Deposit","Loan");
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  for(var x = 0; x <segLength; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	      var seg="segAccounts["+x+","+y+"]";
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      myList.push(accountName);
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	}
	
	
	async function VerifyAccountOnDashBoard(AccountType){
	
	  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        appLog("Successfully verified : <b>"+accountName+"</b>");
	        myList.push("TRUE");
	        finished = true;
	        break;
	      }else{
	        myList.push("FALSE");
	      }
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	async function VerifyCheckingAccountonDashBoard(){
	
	  appLog("Intiated method to verify Checking account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
	  VerifyAccountOnDashBoard("Checking");
	}
	
	async function VerifySavingsAccountonDashBoard(){
	
	  appLog("Intiated method to verify Savings account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
	  VerifyAccountOnDashBoard("Saving");
	}
	async function VerifyCreditCardAccountonDashBoard(){
	
	  appLog("Intiated method to verify CreditCard account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
	  VerifyAccountOnDashBoard("Credit");
	}
	
	async function VerifyDepositAccountonDashBoard(){
	
	  appLog("Intiated method to verify Deposit account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
	  VerifyAccountOnDashBoard("Deposit");
	}
	
	async function VerifyLoanAccountonDashBoard(){
	
	  appLog("Intiated method to verify Loan account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
	  VerifyAccountOnDashBoard("Loan");
	}
	
	async function verifyViewAllTranscation(){
	
	  appLog("Intiated method to view all Tranx in AccountDetails");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	}
	
	async function verifyAdvancedSearch_AccountDetails(AmountRange1,AmountRange2){
	
	  appLog("Intiated method to verify Advanced Search in AccountDetails");
	
	  //   await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnAll"],15000);
	  //   kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnAll"]);
	  //   appLog("Successfully clicked on All button under AccountDetails");
	  //   await kony.automation.playback.wait(5000);
	
	  appLog("Intiated method to click on Seach Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Seach Icon");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], "Transfers");
	  appLog("Successfully selected Transcation Type");
	
	  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
	  appLog("Successfully selected amount range From");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
	  appLog("Successfully selected amount range To");
	
	  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	  appLog("Successfully selected date range");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	
	}
	
	
	async function selectContextMenuOption(Option){
	
	  appLog("Intiated method to select context menu option :: "+Option);
	
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment will be :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+TransfersText);
	
	    if(TransfersText===Option){
	      appLog("Option to be selected is :"+TransfersText);
	      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
	      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
	      await kony.automation.playback.wait(5000);
	      myList.push("TRUE");
	      break;
	    }else{
	      myList.push("FALSE");
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	
	async function verifyVivewStatementsHeader(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");
	
	}
	
	
	async function NavigateToManageRecipitents(){
	
	  appLog("Intiated method to navigate to ManageRecipitents");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Clicked on ManageRecipients button");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","lblTitle"],20000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","BeneficiaryList","lblTitle"],"text")).toEqual("Manage Recipients");
	  appLog("Successfully verified ManageRecipients Header");
	}
	
	async function clickOnExternalRecipitentsTab(){
	
	  //External Acc list
	  appLog("Intiated method to navigate to External Reciptents");
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnTab1"],15000);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","btnTab1"]);
	  appLog("Clicked on External Recipients tab");
	  await kony.automation.playback.wait(5000);
	  //   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"],15000);
	  //   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	  //   appLog("Successfully Clicked on External Recipient from list");
	}
	async function clickOnP2PRecipitentsTab(){
	
	  //P2P Acc list
	  appLog("Intiated method to navigate to P2P Reciptents");
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnTab2"],15000);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","btnTab2"]);
	  appLog("Clicked on P2P Recipients tab");
	  await kony.automation.playback.wait(5000);
	  //   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"],15000);
	  //   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	  //   appLog("Successfully Clicked on P2P Recipient from list");
	}
	
	async function MoveBacktoDashboard_ManageRecipitent(){
	
	  appLog("Intiated method to Move back Accounts dashboard");
	  await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	async function clickonAddExternalAccounttab(){
	
	  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow2"],15000);
	  expect(status).toBe(true);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow2"]);
	  appLog("Successfully Clicked on Add External Account Flex");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickonAddinfinityBankAccounttab(){
	
	  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow1"],15000);
	  expect(status).toBe(true);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow1"]);
	  appLog("Successfully Clicked on Add Infinity Account Flex");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickonAddInternationalAccounttab(){
	
	  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow3"],15000);
	  expect(status).toBe(true);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow3"]);
	  appLog("Successfully Clicked on Add International Account Flex");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickonAddP2PAccounttab(){
	
	  var status=await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow4"],15000);
	  expect(status).toBe(true);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow4"]);
	  appLog("Successfully Clicked on Add P2P Account Flex");
	  await kony.automation.playback.wait(5000);
	}
	
	async function enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName){
	
	  appLog("Intiated method to add enterExternalBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],Routingno);
	  appLog("Successfully Entered Routing Number : <b>"+Routingno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],Accno);
	  appLog("Successfully Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],Accno);
	  appLog("Successfully Re-Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	}
	
	async function enterInternationalBankAccountDetails(swiftCode,Accno,unique_RecipitentName){
	
	  appLog("Intiated method to add enterInternationalBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],swiftCode);
	  appLog("Successfully Entered SwiftCode : <b>"+swiftCode+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],Accno);
	  appLog("Successfully Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],Accno);
	  appLog("Successfully Re-Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	}
	
	async function enterSameBankAccountDetails(Accno,unique_RecipitentName){
	
	  appLog("Intiated method to add enterSameBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],Accno);
	  appLog("Successfully Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],Accno);
	  appLog("Successfully Re-Entered Acc Number : <b>"+Accno+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	
	}
	
	async function enterP2PAccountDetails_Email(unique_RecipitentName,email){
	
	  appLog("Intiated method to add enterSameBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","img2"],15000);
	  kony.automation.widget.touch(["frmFastP2P","addBenificiary","img2"], null,null,[10,19]);
	  appLog("Successfully Selected Email Radio button ");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],email);
	  appLog("Successfully Entered Email name : <b>"+email+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	
	}
	
	async function enterP2PAccountDetails_MobileNumber(unique_RecipitentName,phno){
	
	  appLog("Intiated method to add enterSameBankAccountDetails");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],unique_RecipitentName);
	  appLog("Successfully Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],unique_RecipitentName);
	  appLog("Successfully Re-Entered Reciptent name : <b>"+unique_RecipitentName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","img1"],15000);
	  kony.automation.widget.touch(["frmFastP2P","addBenificiary","img1"], null,null,[10,19]);
	  appLog("Successfully Selected Email Radio button ");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],phno);
	  appLog("Successfully Entered Email name : <b>"+phno+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on Continue Button");
	
	  await linkReciptent();
	
	}
	
	
	async function linkReciptent(){
	
	  var linkreciptent=await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","lblHeader"],15000);
	
	  if(linkreciptent){
	    kony.automation.widget.touch(["frmFastP2P","addBenificiary","contractList","lblCheckBoxSelectAll"], [8,7],null,null);
	    appLog("Successfully selected Select All CheckBox");
	    await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","flxCol4"],15000);
	    kony.automation.flexcontainer.click(["frmFastP2P","addBenificiary","contractList","flxCol4"]);
	    await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","btnAction6"],15000);
	    kony.automation.button.click(["frmFastP2P","addBenificiary","contractList","btnAction6"]);
	    appLog("Successfully Clicked on Link Reciptent SaveReciptent Button");
	  }
	
	  appLog("Intiated Method to Click on AddAccount Button");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction6"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction6"]);
	  appLog("Successfully Clicked on AddAccount Button");
	}
	
	async function verifyAddingNewReciptientSuccessMsg(){
	
	  var success=await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblSection1Message"],30000);
	  if(success){
	    expect(kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","lblSection1Message"],"text")).not.toBe('');
	    appLog("Successfully verified Newly Added Reciptent");
	  }else if(await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","rtxDowntimeWarning"],5000)){
	    //appLog("Logged in User is not authorized to perform this action");
	    //fail('Logged in User is not authorized to perform this action');
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","rtxDowntimeWarning"],"text"));
	  }
	  await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	
	async function SearchforPayee_External(payeeName){
	
	  appLog("Intiated method to Search for a Payee :: <b>"+payeeName+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmFastManagePayee","BeneficiaryList","txtSearch"],payeeName);
	  appLog("Successfully Entered payee name : <b>"+payeeName+"</b>");
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnConfirm"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","btnConfirm"]);
	  appLog("Successfully Clicked on Search button");
	  await kony.automation.playback.wait(5000);
	
	}
	
	async function DeleteReciptent(){
	
	  appLog("Intiated method to Delete a Reciptent");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn3"]);
	  appLog("Successfully Clicked on RemoveRecipient button");
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","lblDescriptionIC"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblDescriptionIC"],"text")).not.toBe('');
	  appLog("Successfully Verified RemoveRecipient PopUp Msg");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","btnYesIC"],15000);
	  kony.automation.button.click(["frmFastManagePayee","btnYesIC"]);
	  appLog("Successfully Clicked on RemoveRecipient YES button");
	  //await kony.automation.playback.wait(5000);
	
	}
	
	async function EditReciptent(UniqueUpdatedName,UniqueUpdatedNickName){
	
	  appLog("Intiated method to Edit Reciptent");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn2"]);
	  appLog("Successfully Clicked on EditRecipient button");
	  //await kony.automation.playback.wait(5000);
	
	  // Line Items 3,4, and 5 will be different for External,International and SameBank acc.
	  //More over Searc button is not working hence Iterating over acc and Editing accordingly.
	
	  for(var i=3;i<=5;i++){
	
	    var key="lblDetailKey"+i;
	    var value="lblDetailValue"+i;
	
	    await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary",key],15000);
	    var keyLabel =kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary",key], "text");
	
	    if(keyLabel==='Recipient Name'){
	
	      kony.automation.textbox.enterText(["frmFastP2P","addBenificiary",value],UniqueUpdatedName);
	      appLog("Successfully Updated <b>"+keyLabel+"</b>");
	
	    }else if(keyLabel==='Account Nickname'){
	
	      kony.automation.textbox.enterText(["frmFastP2P","addBenificiary",value],UniqueUpdatedNickName);
	      appLog("Successfully Updated <b>"+keyLabel+"</b>");
	
	    }else{
	      appLog("Select Name or Nick name Text filed to Update");
	    }
	
	  }
	
	  //   if(AccType.toUpperCase() === SAMEBANK){
	
	  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"],15000);
	  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],UniqueUpdatedName);
	
	  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],UniqueUpdatedNickName);
	
	  //   }else{
	
	  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"],15000);
	  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],UniqueUpdatedName);
	
	  //     await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"],15000);
	  //     kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],UniqueUpdatedNickName);
	  //   }
	
	
	  //Having intermittent issue in Save button
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on SAVE Button");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","btnAction6"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","contractList","btnAction6"]);
	  appLog("Successfully Clicked on Link Reciptent SaveReciptent Button");
	
	}
	
	async function EditP2PReciptent(UniqueUpdatedName,UniqueUpdatedNickName){
	
	  appLog("Intiated method to Edit P2P Reciptent");
	
	  await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"],15000);
	  kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
	  kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn1"]);
	  appLog("Successfully Clicked on EditRecipient button");
	  //await kony.automation.playback.wait(5000);
	
	  appLog("Intiated method to Update Reciptent value");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],UniqueUpdatedName);
	  appLog("Successfully Updated Reciptent name value");
	
	  appLog("Intiated method to Update Reciptent value");
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"],15000);
	  kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],UniqueUpdatedNickName);
	  appLog("Successfully Updated Reciptent nick name value");
	
	  //Having intermittent issue in Save button
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  appLog("Successfully Clicked on SAVE Button");
	
	  await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","contractList","btnAction6"],15000);
	  kony.automation.button.click(["frmFastP2P","addBenificiary","contractList","btnAction6"]);
	  appLog("Successfully Clicked on Link Reciptent SaveReciptent Button");
	
	}
	
	async function navigateToTransfers(){
	
	  appLog("Intiated method to Navigate FastTransfers Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransferMoney"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransferMoney"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmFastTransfers","lblTransfers"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","lblTransfers"], "text")).toEqual("Transfers");
	  appLog("Successfully Navigated to FastTransfers Screen");
	}
	
	async function SelectFromAccount(fromAcc){
	
	  appLog("Intiated method to Select From Account");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmFastTransfers","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmFastTransfers","txtTransferFrom"], [230,25],null,null);
	  kony.automation.textbox.enterText(["frmFastTransfers","txtTransferFrom"],fromAcc);
	  appLog("Successfully Entered From Account");
	  await kony.automation.playback.wait(5000);
	  kony.automation.flexcontainer.click(["frmFastTransfers","segTransferFrom[0,0]","flxAmount"]);
	  appLog("Successfully Selected From Account from List");
	}
	
	async function SelectToAccount(ToAccReciptent){
	
	  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
	
	  await kony.automation.playback.waitFor(["frmFastTransfers","txtTransferTo"],15000);
	  kony.automation.widget.touch(["frmFastTransfers","txtTransferTo"], [72,9],null,null);
	
	  //   if(ToAccReciptent==='OwnAcc'){
	  //     kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],"Saving");
	  //     appLog("Successfully Entered Default To Account : ");
	  //     await kony.automation.playback.wait(5000);
	  //     expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
	  //   }else{
	  //     kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],ToAccReciptent);
	  //     appLog("Successfully Entered To Account : <b>"+ToAccReciptent+"</b>");
	  //     await kony.automation.playback.wait(5000);
	  //     expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
	  //   }
	
	  kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],ToAccReciptent);
	  appLog("Successfully Entered To Account : <b>"+ToAccReciptent+"</b>");
	  await kony.automation.playback.wait(5000);
	  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
	
	  kony.automation.flexcontainer.click(["frmFastTransfers","segTransferTo[0,0]","flxAmount"]);
	  appLog("Successfully Selected To Account from List");
	
	}
	
	async function EnterAmount(amountValue) {
	
	  await kony.automation.playback.waitFor(["frmFastTransfers","tbxAmount"],15000);
	  kony.automation.textbox.enterText(["frmFastTransfers","tbxAmount"],amountValue);
	  appLog("Successfully Entered Amount as : <b>"+amountValue+"</b>");
	  await kony.automation.scrollToWidget(["frmFastTransfers","customfooternew","btnFaqs"]);
	}
	
	async function SelectFrequency(freqValue) {
	
	  kony.automation.flexcontainer.click(["frmFastTransfers","flxContainer4"]);
	  kony.automation.listbox.selectItem(["frmFastTransfers","lbxFrequency"], freqValue);
	  appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
	}
	
	async function SelectDateRange() {
	
	  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
	  kony.automation.calendar.selectDate(["frmFastTransfers","calEndingOnNew"], [11,25,2021]);
	  appLog("Successfully Selected DateRange");
	}
	
	async function SelectSendOnDate() {
	
	  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
	  appLog("Successfully Selected SendOn Date");
	}
	
	async function SelectOccurences(occurences) {
	
	  kony.automation.listbox.selectItem(["frmFastTransfers","lbxForHowLong"], "NO_OF_RECURRENCES");
	  kony.automation.textbox.enterText(["frmFastTransfers","tbxNoOfRecurrences"],occurences);
	  appLog("Successfully Selected Occurences as <b>"+occurences+"</b>");
	  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
	  appLog("Successfully Selected SendOn Date");
	}
	async function EnterNoteValue(notes) {
	
	  await kony.automation.playback.waitFor(["frmFastTransfers","txtNotes"],10000);
	  kony.automation.textbox.enterText(["frmFastTransfers","txtNotes"],notes);
	  appLog("Successfully entered Note value as : <b>"+notes+"</b>");
	  await kony.automation.playback.waitFor(["frmFastTransfers","btnConfirm"],10000);
	  await kony.automation.scrollToWidget(["frmFastTransfers","btnConfirm"]);
	  kony.automation.button.click(["frmFastTransfers","btnConfirm"]);
	  appLog("Successfully Clicked on Continue Button");
	}
	
	async function ConfirmTransfer() {
	
	  appLog("Intiated method to Confirm Transfer Details");
	
	  await kony.automation.playback.waitFor(["frmReview","btnConfirm"],15000);
	  kony.automation.button.click(["frmReview","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	async function VerifyTransferSuccessMessage() {
	
	  appLog("Intiated method to Verify Transfer SuccessMessage ");
	
	  await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmConfirmTransfer"],30000);
	
	  if(success){
	    //expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).toContain("We successfully");
	    //   expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).not.toBe('');
	    //   await kony.automation.playback.waitFor(["frmConfirmTransfer","btnSavePayee"],15000);
	    //   kony.automation.button.click(["frmConfirmTransfer","btnSavePayee"]);
	    await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
	    appLog("Successfully Clicked on Accounts Button");
	  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","rtxMakeTransferError"],5000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
	    appLog("Failed with : rtxMakeTransferError");
	    fail("Failed with : rtxMakeTransferError");
	
	    await MoveBackToLandingScreen_Transfers();
	
	  }else{
	
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to perform Successfull Transcation");
	    fail("Unable to perform Successfull Transcation");
	  }
	
	}
	
	async function CancelTransfer() {
	
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmReview","btnCancel"],15000);
	  kony.automation.button.click(["frmReview","btnCancel"]);
	  appLog("Successfully Clicked on CANCEL Button");
	  await kony.automation.playback.waitFor(["frmReview","CustomPopup","btnYes"],15000);
	  kony.automation.button.click(["frmReview","CustomPopup","btnYes"]);
	  appLog("Successfully Clicked on YES Button");
	}
	
	async function navigateToTransferActivities(){
	
	  appLog("Intiated method to navigate to Transfer Activities");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxPayBills"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxPayBills"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to TransferActivities");
	}
	
	async function navigateToPastTransfersTab(){
	
	  appLog("Intiated method to navigate to PastTransfer Tab");
	  await kony.automation.playback.waitFor(["frmFastTransfersActivites","btnRecent"],15000);
	  kony.automation.button.click(["frmFastTransfersActivites","btnRecent"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully navigated to PastTransfer Tab");
	}
	
	async function verifyRepeatTransferFunctionality(note){
	
	  appLog("Intiated method verify Repeat Transfer Functionality");
	
	  var noTransfers=await kony.automation.playback.waitFor(["frmFastTransfersActivites","rtxNoPaymentMessage"],10000);
	
	  if(noTransfers){
	
	    appLog('There are No Transactions Found');
	    await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	    await kony.automation.playback.wait(5000); 
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else{
	
	    //await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
	    var noReapeatBtn= await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","btnAction"],10000);
	
	    if(noReapeatBtn){
	      appLog('Intiating Repeat Transfer');
	      kony.automation.button.click(["frmFastTransfersActivites","segmentTransfers[0]","btnAction"]);
	      await kony.automation.playback.wait(5000);
	      appLog("Successfully Clicked on Repeat Button");
	      await EnterNoteValue(note);
	      await ConfirmTransfer();
	      await VerifyTransferSuccessMessage();
	    }else{
	      appLog('No Repeat Transfers available');
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	      await kony.automation.playback.wait(5000); 
	      appLog("Successfully Moved back to Accounts dashboard");
	    }
	  }
	
	
	}
	
	async function VerifyTranxUnderActivities(){
	
	  appLog("Intiated method verify Transfer under Activities");
	
	  var noTransfers=await kony.automation.playback.waitFor(["frmFastTransfersActivites","rtxNoPaymentMessage"],15000);
	
	  if(noTransfers){
	    appLog('There are No Transactions Found');
	    await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	    await kony.automation.playback.wait(5000);
	    await verifyAccountsLandingScreen();
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else{
	
	    //await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
	    var noTranxBtn= await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","flxDropdown"],10000);
	    if(noTranxBtn){
	
	      appLog('Intiating to verify Transfer Activity');
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","segmentTransfers[0]","flxDropdown"]);
	      appLog("Successfully Clicked on first Sheduled Transfer");
	
	      //No garuntee that same note will be there, other users also will perform Tranx
	      //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfersActivites","segmentTransfers[0]","flxFastPastTransfersSelected","lblNotesValue1"],"text")).toEqual(notevalue);
	
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	      await verifyAccountsLandingScreen();
	      appLog("Successfully Moved back to Accounts dashboard");
	    }else{
	
	      appLog('No Transfers activities available');
	      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
	      await kony.automation.playback.wait(5000); 
	      appLog("Successfully Moved back to Accounts dashboard");
	    }
	
	  }
	}
	
	async function MoveBackToLandingScreen_Transfers(){
	
	  //Move back to landing Screen
	  appLog("Intiated method to move from frmFastTransfers screen");
	  await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	
	
	it("AddExternalBankRecipitent", async function() {
	  
	  // Add a recipitent and Then delete the same recipitent
	  
	  var Routingno=ManageRecipients.externalAccount.Routingno;
	  var Accno="0"+new Date().getTime();
	  var unique_RecipitentName=ManageRecipients.externalAccount.unique_RecipitentName+getRandomString(5);
	  
	  await NavigateToManageRecipitents();
	  await clickonAddExternalAccounttab();
	  await enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	  //Delete Added Recipitent
	  
	//   await NavigateToManageRecipitents();
	//   await clickOnExternalRecipitentsTab();
	//   await SearchforPayee_External(unique_RecipitentName);
	//   await DeleteReciptent();
	//   await MoveBacktoDashboard_ManageRecipitent();
	
	  
	  
	  //var unique_RecipitentName="ExtAcc"+new Date().getTime();
	  
	//   var unique_RecipitentName="ExtAccJasmine";
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(10000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","lblTitle"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","BeneficiaryList","lblTitle"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","quicklinks","flxRow2"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","quicklinks","flxRow2"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue1"]);
	//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue1"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue2"]);
	//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue2"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue3"]);
	//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue3"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue4"]);
	//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue4"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblDetailValue5"]);
	//   kony.automation.textbox.enterText(["frmFastP2P","addBenificiary","lblDetailValue5"],unique_RecipitentName);
	  
	//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction2"]);
	//   kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction2"]);
	  
	//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","btnAction6"]);
	//   kony.automation.button.click(["frmFastP2P","addBenificiary","btnAction6"]);
	
	//   await kony.automation.playback.waitFor(["frmFastP2P","addBenificiary","lblSection1Message"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastP2P","addBenificiary","lblSection1Message"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastP2P","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastP2P","customheadernew","flxAccounts"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//    //Delete Same Bank Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","lblTitle"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","BeneficiaryList","lblTitle"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnTab1"]);
	//   kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","btnTab1"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","BeneficiaryList","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","BeneficiaryList","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","BeneficiaryList","segmentTransfers[0]","btn3"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnYesIC"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnYesIC"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	     
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("AddP2PRecipitent_Email", async function() {
	
	   // Add a recipitent and Then delete the same recipitent
	  
	  var unique_RecipitentName=ManageRecipients.p2pAccount.unique_RecipitentName+getRandomString(5);
	  var email=ManageRecipients.p2pAccount.email;
	  
	  await NavigateToManageRecipitents();
	  await clickonAddP2PAccounttab();
	  await enterP2PAccountDetails_Email(unique_RecipitentName,email);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	   //Delete Added Recipitent
	
	//   await NavigateToManageRecipitents();
	//   await clickOnP2PRecipitentsTab();
	//   await SearchforPayee_External(unique_RecipitentName);
	//   await DeleteReciptent();
	//   await MoveBacktoDashboard_ManageRecipitent();
	  
	//   var unique_RecipitentName="TestP2PRecipitentEmail_"+new Date().getTime();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddReciepient"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddReciepient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxRecipientNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxRecipientNameKA"],unique_RecipitentName);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxAccountNickNameKA"],unique_RecipitentName);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","flxNUORadioBtn2"]);
	//   kony.automation.flexcontainer.click(["frmFastAddRecipient","flxNUORadioBtn2"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxEmailAddressKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxEmailAddressKA"],"testauto@gmail.com");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddRecipient","btnAddAccountKA"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddRecipientConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   //Delete Same Bank Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search1","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search1","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup1","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup1","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this recipient?");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup1","btnYes"]);
	//   kony.automation.button.click(["frmFastManagePayee","CustomPopup1","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	//     //expect(true).toBe(false, 'There was a technical delay. Please try again.');
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("AddP2PRecipitent_Mobile", async function() {
	
	
	  // Add a recipitent and Then delete the same recipitent
	  
	  var unique_RecipitentName=ManageRecipients.p2pAccount.unique_RecipitentName+getRandomString(5);
	  var phno=ManageRecipients.p2pAccount.phno;
	  
	  await NavigateToManageRecipitents();
	  await clickonAddP2PAccounttab();
	  await enterP2PAccountDetails_MobileNumber(unique_RecipitentName,phno);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	  //Delete Added Recipitent
	  
	//   await NavigateToManageRecipitents();
	//   await clickOnP2PRecipitentsTab();
	//   await SearchforPayee_External(unique_RecipitentName);
	//   await DeleteReciptent();
	//   await MoveBacktoDashboard_ManageRecipitent();
	  
	  
	//   var unique_RecipitentName="TestP2PRecipitentMobile_"+new Date().getTime();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddReciepient"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddReciepient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxRecipientNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxRecipientNameKA"],unique_RecipitentName);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxAccountNickNameKA"],unique_RecipitentName);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","flxNUORadioBtn1"]);
	//   kony.automation.flexcontainer.click(["frmFastAddRecipient","flxNUORadioBtn1"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxPhoneNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxPhoneNumberKA"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddRecipient","btnAddAccountKA"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddRecipientConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//    //Delete Same Bank Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search1","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search1","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup1","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup1","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this recipient?");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup1","btnYes"]);
	//   kony.automation.button.click(["frmFastManagePayee","CustomPopup1","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("AddSameBankRecipitent", async function() {
	
	//    // Add a recipitent and Then delete the same recipitent
	  var Accno="0"+new Date().getTime();
	  var unique_RecipitentName=ManageRecipients.sameBankAccount.unique_RecipitentName+getRandomString(5);
	  
	  await NavigateToManageRecipitents();
	  await clickonAddinfinityBankAccounttab();
	  await enterSameBankAccountDetails(Accno,unique_RecipitentName);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen(); 
	  
	   //Delete Added Recipitent
	  
	//   await NavigateToManageRecipitents();
	//   await clickOnExternalRecipitentsTab();
	//   await SearchforPayee_External(unique_RecipitentName);
	//   await DeleteReciptent();
	//   await MoveBacktoDashboard_ManageRecipitent();
	  
	//   var unique_RecipitentName="TestSameBankRecipitent_"+new Date().getTime();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddBankAccount"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddBankAccount"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","lblTransfers"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccount","lblTransfers"],"text")).toEqual("Add Infinity Bank Account");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNickNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddBankAccount"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddBankAccount"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","lblTransfers"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccount","lblTransfers"],"text")).toEqual("Add Infinity Bank Account");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNickNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   //Delete Same Bank Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("DeleteExternalBankRecipitent", async function() {
	
	 // Add a recipitent and Then delete the same recipitent
	  
	  var Routingno=ManageRecipients.externalAccount.Routingno;
	  var Accno="0"+new Date().getTime();
	  var unique_RecipitentName=ManageRecipients.externalAccount.unique_RecipitentName+getRandomString(5);
	  
	  await NavigateToManageRecipitents();
	  await clickonAddExternalAccounttab();
	  await enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	  //Delete Added Recipitent
	  
	  await NavigateToManageRecipitents();
	  await clickOnExternalRecipitentsTab();
	  await SearchforPayee_External(unique_RecipitentName);
	  await DeleteReciptent();
	  await MoveBacktoDashboard_ManageRecipitent();
	  
	//   var unique_RecipitentName="TestExtAcc_"+new Date().getTime();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddKonyAccount"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddKonyAccount"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxRoutingNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxRoutingNumberKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNumberKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNumberAgainKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNumberAgainKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxBeneficiaryNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxBeneficiaryNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNickNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddExternalAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddExternalAccountConfirm","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddExternalAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddExternalAccountAcknowledgement","customheadernew","flxAccounts"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//    //Delete Ext Bank Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("DeleteP2PRecipitent", async function() {
	
	  // Add a recipitent and Then delete the same recipitent
	 
	 var unique_RecipitentName=ManageRecipients.p2pAccount.unique_RecipitentName+getRandomString(5);
	 var email=ManageRecipients.p2pAccount.email;
	  
	  await NavigateToManageRecipitents();
	  await clickonAddP2PAccounttab();
	  await enterP2PAccountDetails_Email(unique_RecipitentName,email);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	   //Delete Added Recipitent
	
	  await NavigateToManageRecipitents();
	  await clickOnP2PRecipitentsTab();
	  await SearchforPayee_External(unique_RecipitentName);
	  await DeleteReciptent();
	  await MoveBacktoDashboard_ManageRecipitent();
	
	//   var unique_RecipitentName="TestP2PRecipitentEmail_"+new Date().getTime();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddReciepient"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddReciepient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxRecipientNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxRecipientNameKA"],unique_RecipitentName);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxAccountNickNameKA"],unique_RecipitentName);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","flxNUORadioBtn2"]);
	//   kony.automation.flexcontainer.click(["frmFastAddRecipient","flxNUORadioBtn2"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxEmailAddressKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxEmailAddressKA"],"testauto@gmail.com");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddRecipient","btnAddAccountKA"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddRecipientConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   //Delete P2P Bank Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search1","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search1","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup1","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup1","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this recipient?");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup1","btnYes"]);
	//   kony.automation.button.click(["frmFastManagePayee","CustomPopup1","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("DeleteSameBankRecipitent", async function() {
	
	  // Add a recipitent and Then delete the same recipitent
	  
	 var Accno="0"+new Date().getTime();
	 var unique_RecipitentName=ManageRecipients.sameBankAccount.unique_RecipitentName+getRandomString(5);
	  
	  await NavigateToManageRecipitents();
	  await clickonAddinfinityBankAccounttab();
	  await enterSameBankAccountDetails(Accno,unique_RecipitentName);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen(); 
	  
	   //Delete Added Recipitent
	  
	  await NavigateToManageRecipitents();
	  await clickOnExternalRecipitentsTab();
	  await SearchforPayee_External(unique_RecipitentName);
	  await DeleteReciptent();
	  await MoveBacktoDashboard_ManageRecipitent();
	  
	  
	//   var unique_RecipitentName="TestSameBankRecipitent_"+new Date().getTime();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddBankAccount"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddBankAccount"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","lblTransfers"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccount","lblTransfers"],"text")).toEqual("Add Infinity Bank Account");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNickNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddBankAccount"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddBankAccount"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","lblTransfers"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccount","lblTransfers"],"text")).toEqual("Add Infinity Bank Account");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNickNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   //Delete Same Bank Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("EditExternalBankRecipitent", async function() {
	
	  // Add a recipitent and Then Edit the same recipitent
	  
	   // Add a recipitent and Then delete the same recipitent
	  
	  var Routingno=ManageRecipients.externalAccount.Routingno;
	  var Accno="0"+new Date().getTime();
	  var unique_RecipitentName=ManageRecipients.externalAccount.unique_RecipitentName+getRandomString(5);
	  
	  //var AccType="External";
	  var unique_EditRecipitentName=ManageRecipients.externalAccount.unique_EditRecipitentName+getRandomString(5);
	  var unique_EditNickName=ManageRecipients.externalAccount.unique_EditNickName+getRandomString(5);
	  
	  await NavigateToManageRecipitents();
	  await clickonAddExternalAccounttab();
	  await enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	  //Edit Added Recipitent
	  
	  await NavigateToManageRecipitents();
	  await clickOnExternalRecipitentsTab();
	  await SearchforPayee_External(unique_RecipitentName);
	  await EditReciptent(unique_EditRecipitentName,unique_EditNickName);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	//   var unique_RecipitentName="TestExtAccEdit_"+new Date().getTime();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddKonyAccount"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddKonyAccount"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxRoutingNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxRoutingNumberKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNumberKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNumberAgainKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNumberAgainKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxBeneficiaryNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxBeneficiaryNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNickNameKA"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddExternalAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddExternalAccountConfirm","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddExternalAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddExternalAccountAcknowledgement","customheadernew","flxAccounts"]);
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   // Edit the added Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnEdit"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","lblTransfers"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddExternalAccount","lblTransfers"],"text")).toContain("Edit");
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNickName"]);
	//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNickName"],"Auto Updated");
	  
	//   //Having intermittent issue in Save button
	//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","btnSave"]);
	//   kony.automation.button.click(["frmFastAddExternalAccount","btnSave"]);
	  
	//   var successMsg=await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],10000);
	//   if(!successMsg){
	//     // Move back to base state
	//     await kony.automation.playback.waitFor(["frmFastAddExternalAccount","customheadernew","flxAccounts"]);
	// 	kony.automation.flexcontainer.click(["frmFastAddExternalAccount","customheadernew","flxAccounts"]);
	//   }else{
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been successfully edited");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   }
	
	//  // Delete the recipitent to clean list
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	},120000);
	
	it("EditP2PRecipitent", async function() {
	
	   // Add a recipitent and Then delete the same recipitent
	  
	 var unique_RecipitentName=ManageRecipients.p2pAccount.unique_RecipitentName+getRandomString(5);
	 var email=ManageRecipients.p2pAccount.email;
	  
	  var unique_EditRecipitentName=ManageRecipients.p2pAccount.unique_EditRecipitentName+getRandomString(5);
	  var unique_EditNickName=ManageRecipients.p2pAccount.unique_EditNickName+getRandomString(5);
	  
	  await NavigateToManageRecipitents();
	  await clickonAddP2PAccounttab();
	  await enterP2PAccountDetails_Email(unique_RecipitentName,email);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	   //Edit Added Recipitent
	  await NavigateToManageRecipitents();
	  await clickOnP2PRecipitentsTab();
	  await SearchforPayee_External(unique_RecipitentName);
	  await EditP2PReciptent(unique_EditRecipitentName,unique_EditNickName);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddReciepient"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddReciepient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxRecipientNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxRecipientNameKA"],"TestP2PRecipitent");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxAccountNickNameKA"],"P2P");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","flxNUORadioBtn1"]);
	//   kony.automation.flexcontainer.click(["frmFastAddRecipient","flxNUORadioBtn1"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxPhoneNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxPhoneNumberKA"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddRecipient","btnAddAccountKA"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddRecipientConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   // Edit the added Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search1","txtSearch"],"TestP2PRecipitent");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search1","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnEdit"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","lblTransfers"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipient","lblTransfers"],"text")).toContain("Edit");
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","flxNUORadioBtn1"]);
	//   kony.automation.flexcontainer.click(["frmFastAddRecipient","flxNUORadioBtn1"]);
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxPhoneNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxPhoneNumberKA"],"1234567890");
	
	//   //Having issue in Save button
	//   await kony.automation.playback.waitFor(["frmFastAddRecipient","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddRecipient","btnAddAccountKA"]);
	
	//   var successMsg=await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],10000);
	
	//   if(!successMsg){
	//     // Move back to base state
	//     await kony.automation.playback.waitFor(["frmFastAddRecipient","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmFastAddRecipient","customheadernew","flxAccounts"]);
	//   }else{
	//     await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],"text")).toContain("has been successfully edited");
	//     await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   }
	  
	//   // Delete the recipitent to clean list
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("EditSameBankRecipitent", async function() {
	
	  // Add Recipitent and edit it
	  
	  // Add a recipitent and Then delete the same recipitent
	 var Accno="0"+new Date().getTime();
	 var unique_RecipitentName=ManageRecipients.sameBankAccount.unique_RecipitentName+getRandomString(5);
	  
	  //var AccType="SameBank";
	  var unique_EditRecipitentName=ManageRecipients.sameBankAccount.unique_EditRecipitentName+getRandomString(5);
	  var unique_EditNickName=ManageRecipients.sameBankAccount.unique_EditNickName+getRandomString(5);
	  
	  await NavigateToManageRecipitents();
	  await clickonAddinfinityBankAccounttab();
	  await enterSameBankAccountDetails(Accno,unique_RecipitentName);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen(); 
	  
	  //Edit Added Recipitent
	  await NavigateToManageRecipitents();
	  await clickOnExternalRecipitentsTab();
	  await SearchforPayee_External(unique_RecipitentName);
	  await EditReciptent(unique_EditRecipitentName,unique_EditNickName);
	  await verifyAddingNewReciptientSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddBankAccount"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddBankAccount"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","lblTransfers"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccount","lblTransfers"],"text")).toEqual("Add Infinity Bank Account");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNumberAgainKA"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxBeneficiaryNameKA"],"TestSameBankAcc");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNickNameKA"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNickNameKA"],"Auto");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   kony.automation.button.click(["frmFastAddDBXAccount","btnAddAccountKA"]);
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmFastAddDBXAccountConfirm","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   // Edit the added Recipitent
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],"Test");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnEdit"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","lblTransfers"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccount","lblTransfers"],"text")).toContain("Edit");
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","tbxAccountNickName"]);
	//   kony.automation.textbox.enterText(["frmFastAddDBXAccount","tbxAccountNickName"],"Auto Updated");
	
	//   //Having issue in Save button
	//   await kony.automation.playback.waitFor(["frmFastAddDBXAccount","btnSave"]);
	//   kony.automation.button.click(["frmFastAddDBXAccount","btnSave"]);
	
	//   var successMsg=await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],10000);
	
	//   if(!successMsg){
	//     // Move back to base state
	//     await kony.automation.playback.waitFor(["frmFastAddDBXAccount","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmFastAddDBXAccount","customheadernew","flxAccounts"]);
	//   }else{
	//     await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmFastAddDBXAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been successfully edited");
	//     await kony.automation.playback.waitFor(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmFastAddDBXAccountAcknowledgement","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   }
	// // Delete the recipitent to clean list
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
	//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
	//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
	  
	//   if(error){
	//     fail("There was a technical delay. Please try again.");
	//   }
	
	//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
});