var LoginDetails={
  username:'dbxJasmine',
  password:'Kony@1234'
};

var Accounts={

  checkingAcc : {

    toAccount : "Saving",
    amountValue : "1",

    "ContextMenuOptions":
    [
      { "type": "Transfer" },
      { "type": "Pay Bill" },
      { "type": "Stop Check Payment" },
      { "type": "Manage Cards" },
      { "type": "View Statements" },
      { "type": "Account Alerts" }
    ]
  },
  savingAcc : {

    toAccount : "Checking",
    amountValue : "1",

    "ContextMenuOptions":
    [
      { "type": "Transfer" },
      { "type": "Stop Check Payment" },
      { "type": "Manage Cards" },
      { "type": "View Statements" },
      { "type": "Account Alerts" }
    ]
  },
  creditcardAcc : {

    amountValue : "1",

    "ContextMenuOptions":
    [

      { "type": "Pay Bill" },
      { "type": "View Statements" },
      { "type": "Account Alerts" }
    ]
  },
  depositAcc : {
    "ContextMenuOptions":
    [

      { "type": "View Statements" },
      { "type": "Update Account Settings" },
      { "type": "Account Alerts" }
    ]
  },
  loanAcc : {
    "ContextMenuOptions":
    [
      { "type": "Pay Due Amount" },
      { "type": "View Statements" },
      { "type": "Update Account Settings" },
      { "type": "Account Alerts" },
      { "type": "Pay Off Loan" }
    ]
  },
};


var Transfers={

  p2pAccount : {
    fromAccount : "Check",
    toAccount : "PToPAccJasmine",
    amountValue : "1"
  },
  externalAccount : {
    fromAccount : "Check",
    toAccount : "ExtAccJasmine",
    amountValue : "1"
  },
  internationalAccount : {
    fromAccount : "Check",
    toAccount : "InterAccJasmine",
    amountValue : "1"
  },
  ownAccount : {
    fromAccount : "Check",
    toAccount : "Saving",
    amountValue : "1"
  },
  sameBankAccount : {
    fromAccount : "Check",
    toAccount : "SameBankAccJasmine",
    amountValue : "1"
  }
};

var ManageRecipients={

  p2pAccount : {
    unique_RecipitentName : "PToPAccJasmine",
    email : "PTOPAccJasmine@gmail.com",
    phno : "1234567890",
    unique_EditRecipitentName : "PToPAccJasmineEdit",
    unique_EditNickName : "PToPAccJasmineEditNick"
  },
  externalAccount : {
    Routingno : "1234567890",
    unique_RecipitentName : "ExtAccJasmine",
    unique_EditRecipitentName : "ExtAccJasmineEdit",
    unique_EditNickName : "ExtAccJasmineEditNick"
  },
  internationalAccount : {
    swiftCode : "BOFAUS3N",
    unique_RecipitentName : "InterAccJasmine",
    unique_EditRecipitentName : "InterAccJasmineEdit",
    unique_EditNickName : "InterAccJasmineEditNick"
  },
  sameBankAccount : {
    unique_RecipitentName : "SameBankAccJasmine",
    unique_EditRecipitentName : "PToPAccJasmineEdit",
    unique_EditNickName : "PToPAccJasmineEditNick"
  }
};

var BillPay={

  oneTimePay : {
    payeeName : "A",
    zipcode : "500055",
    accno : "1234567890",
    accnoAgain : "1234567890",
    mobileno : "1234567890",
    amountValue : "1"
  },
  addPayee : {
    unique_RecipitentName : "ABCJasmine",
    address1 : "LR PALLI",
    address2 : "ATMAKUR",
    city : "NELLORE",
    zipcode : "500055"
  },
  deletePayee : {
    delete_RecipitentName : "Test Automation",
    address1 : "LR PALLI",
    address2 : "ATMAKUR",
    city : "NELLORE",
    zipcode : "500055"
  },
  schedulePay : {
    payeeName : "ABCJasmine",
    amountValue : "1.2"
  }
};

var Settings={

  address : {
    addressLine1 : "Miyapur",
    addressLine2 : "alwyn",
    zipcode : "500055",
    primaryAddressLine1 : "MiyapurPrimary",
    primaryAddressLine2 : "alwynPrimary",
    primaryZipcode : "500049",
    updatedZip : "500011"
  },
  phone : {
    phoneNumber : "888456789",
    primaryPhoneNumber : "888456700",
    updatedPhonenum : "1234567130"
    
  },
  email : {
    emailAddress : "dbxjasmine@infinity.com",
    primaryEmailAddress : "dbxjasminePrimary@infinity.com",
    updatedemailid :"dbxjasmineUpdate@infinity.com"
 
  },
};