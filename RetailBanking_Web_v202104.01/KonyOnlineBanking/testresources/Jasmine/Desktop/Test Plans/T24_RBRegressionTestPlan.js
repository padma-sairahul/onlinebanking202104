require(["Test Suites/RBTestSuites/LoginSuite"], function() {
	require(["Test Suites/RBTestSuites/T24_ProfileSettingsSuite"], function() {
		require(["Test Suites/RBTestSuites/T24_ManageBeneficiarySuite"], function() {
			require(["Test Suites/RBTestSuites/T24_SearchBeneficiariesSuite"], function() {
				require(["Test Suites/RBTestSuites/T24_AccountsOverviewSuite"], function() {
					require(["Test Suites/RBTestSuites/T24_DomesticPaymentsSuite"], function() {
						require(["Test Suites/RBTestSuites/T24_AccountSettingsSuite"], function() {
							require(["Test Suites/RBTestSuites/T24_InternationalPaymentsSuite"], function() {
								require(["Test Suites/RBTestSuites/T24_SameBankPaymentSuite"], function() {
									require(["Test Suites/RBTestSuites/T24_TransferActivitiesSuite"], function() {
										require(["Test Suites/RBTestSuites/T24_TransferbetweenAccountsSuite"], function() {
											require(["Test Suites/RBTestSuites/T24_SearchTranscationSuite"], function() {
												require(["Test Suites/RBTestSuites/T24_CardManagement"], function() {
													require(["Test Suites/RBTestSuites/T24_ExchangeRateSuite"], function() {
														require(["Test Suites/RBTestSuites/T24_BillPaySuite"], function() {
															require(["Test Suites/RBTestSuites/T24_MessagesSuite"], function() {
																require(["Test Suites/RBTestSuites/T24_PostLoginSuite"], function() {
																	require(["Test Suites/RBTestSuites/T24_AccountPreferences"], function() {
																		require(["Test Suites/RBTestSuites/T24_CombinedStatements"], function() {
																			require(["Test Suites/RBTestSuites/T24_WireTransfer"], function() {
																				require(["Test Suites/RBTestSuites/T24_AccountsSuite"], function() {
																					require(["Test Suites/RBTestSuites/T24_CustomView"], function() {
																																												jasmine.getEnv().execute();
																					});
																				});
																			});
																		});
																	});
																});
															});
														});
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
});