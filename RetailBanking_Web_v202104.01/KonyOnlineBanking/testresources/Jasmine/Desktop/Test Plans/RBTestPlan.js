require(["Test Suites/RBTestSuites/PreLoginSuite"], function() {
	require(["Test Suites/RBTestSuites/LoginSuite"], function() {
		require(["Test Suites/RBTestSuites/TransferToExternalAcc"], function() {
			require(["Test Suites/RBTestSuites/PostLoginSuite"], function() {
				require(["Test Suites/RBTestSuites/TransferTointernationalAcc"], function() {
					require(["Test Suites/RBTestSuites/MessagesSuite"], function() {
						require(["Test Suites/RBTestSuites/TransferToP2P"], function() {
							require(["Test Suites/RBTestSuites/TransferToSameBank"], function() {
								require(["Test Suites/RBTestSuites/AccountsSuite"], function() {
									require(["Test Suites/RBTestSuites/TransferToOwnAccount"], function() {
										require(["Test Suites/RBTestSuites/BillPaySuite"], function() {
																						jasmine.getEnv().execute();
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
});