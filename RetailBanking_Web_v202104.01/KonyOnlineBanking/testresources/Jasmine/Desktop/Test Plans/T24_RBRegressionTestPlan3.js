require(["Test Suites/RBTestSuites/LoginSuite"], function() {
	require(["Test Suites/RBTestSuites/T24_MessagesSuite"], function() {
		require(["Test Suites/RBTestSuites/T24_PostLoginSuite"], function() {
			require(["Test Suites/RBTestSuites/T24_ProfileSettingsSuite"], function() {
				require(["Test Suites/RBTestSuites/T24_SearchTranscationSuite"], function() {
					require(["Test Suites/RBTestSuites/T24_TransferActivitiesSuite"], function() {
												jasmine.getEnv().execute();
					});
				});
			});
		});
	});
});