require(["Test Suites/RBTestSuites/PreLoginSuite"], function() {
	require(["Test Suites/RBTestSuites/LoginSuite"], function() {
		require(["Test Suites/RBTestSuites/MiniManageRecipitent"], function() {
			require(["Test Suites/RBTestSuites/MiniTransferSuite"], function() {
				require(["Test Suites/RBTestSuites/AccountsSuite"], function() {
					require(["Test Suites/RBTestSuites/MessagesSuite"], function() {
						require(["Test Suites/RBTestSuites/PostLoginSuite"], function() {
														jasmine.getEnv().execute();
						});
					});
				});
			});
		});
	});
});