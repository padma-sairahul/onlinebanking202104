//This is the entry point for automation. You can either:
//1.Require any one of the created test plans like this:



var app="T24RBSanity";
var userType = "_RB"; //valid values are "_RB" and "_SME"
var Env = "_T24";//valid values are "_T24" and ""

require(["testScripts/logger"]);
require(["testScripts/TestData/testData"+userType+Env]);
  
if(app==="RBRegression"){
  require(["Test Plans/RBTestPlan"]);
}else if(app==="SMERegression"){
  require(["Test Plans/SMETestPlan"]);
}else if(app==="RBSanity"){
  require(["Test Plans/RBSanityTestPlan"]);
}else if(app==="SMESanity"){
  require(["Test Plans/SMESanityTestPlan"]);
}else if(app==="RBMiniRegression"){
  require(["Test Plans/RBMiniRegressionPlan"]);
}else if(app==="SMEMiniRegression"){
  require(["Test Plans/SMEMiniRegressionPlan"]);
}else if(app==="T24RBRegression"){
  require(["Test Plans/T24_RBRegressionTestPlan"]);
  //require(["Test Plans/T24_RBRegressionTestPlan0"]);
  //require(["Test Plans/T24_RBRegressionTestPlan1"]);
  //require(["Test Plans/T24_RBRegressionTestPlan2"]);
  //require(["Test Plans/T24_RBRegressionTestPlan3"]);
  //require(["Test Plans/T24_RBRegressionTestPlan13"]);
}else if(app==="T24RBHealthCheck"){
  require(["Test Plans/T24_RB_HealthCheck"]);
}else if(app==="T24RBSanity"){
  require(["Test Plans/T24_RB_SanityTestPlan"]);
}else{
  require(["Test Plans/tempPlan"]);
}


//require(["Test Plans/SamplePlan"]);
//require(["Test Plans/CopyofRBTestPlan"]);

// or
//2.  require the test suites along with executing jasmine as below
//Nested require for test suites will ensure the order of test suite exectuion
// require([/*<Test Suites/test suite1>*/],function(){
//   require([/*<Test Suites/test suite2>*/], function(){
//     //and so on
//     	require([/*<Test Suites/last test suite>*/], function(){
//         		jasmine.getEnv().execute();  
//         });
//   });
// });

//Since this is file is to be manually edited, make sure to update 
//any changes (rename/delete) to the test suites/plans.