require(["Test Suites/RBTestSuites/LoginSuite"], function() {
	require(["Test Suites/RBTestSuites/T24_AccountPreferences"], function() {
		require(["Test Suites/RBTestSuites/T24_AccountSettingsSuite"], function() {
			require(["Test Suites/RBTestSuites/T24_AccountsOverviewSuite"], function() {
				require(["Test Suites/RBTestSuites/T24_BillPaySuite"], function() {
					require(["Test Suites/RBTestSuites/T24_CardManagement"], function() {
						require(["Test Suites/RBTestSuites/T24_CombinedStatements"], function() {
							require(["Test Suites/RBTestSuites/T24_ExchangeRateSuite"], function() {
																jasmine.getEnv().execute();
							});
						});
					});
				});
			});
		});
	});
});