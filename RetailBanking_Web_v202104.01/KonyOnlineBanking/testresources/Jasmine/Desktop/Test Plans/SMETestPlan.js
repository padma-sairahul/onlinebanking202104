require(["Test Suites/SMETestSuites/SME_PreLoginSuite"], function() {
	require(["Test Suites/SMETestSuites/SME_LoginSuite"], function() {
		require(["Test Suites/SMETestSuites/SME_TransferToExternalAcc"], function() {
			require(["Test Suites/SMETestSuites/SME_PostLoginSuite"], function() {
				require(["Test Suites/SMETestSuites/SME_TransferTointernationalAcc"], function() {
					require(["Test Suites/SMETestSuites/SME_MessagesSuite"], function() {
						require(["Test Suites/SMETestSuites/SME_TransferToSameBank"], function() {
							require(["Test Suites/SMETestSuites/SME_AccountsSuite"], function() {
								require(["Test Suites/SMETestSuites/SME_TransferToOwnAccount"], function() {
									require(["Test Suites/SMETestSuites/SME_BillPaySuite"], function() {
																				jasmine.getEnv().execute();
									});
								});
							});
						});
					});
				});
			});
		});
	});
});