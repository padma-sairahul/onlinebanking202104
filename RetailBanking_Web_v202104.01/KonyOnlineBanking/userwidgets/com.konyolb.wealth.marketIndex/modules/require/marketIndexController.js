define( function() {



  return {


    setDataMarket: function  (newsDetails) {
      var segData = [];
     
      var inresult = [];
      var innerdata = {};
      for (var i in newsDetails) {
        var subArray = [];
        subArray = newsDetails[i].Fields.Field;
        innerdata = {};
        for (var j in subArray) {
          var dt = subArray[j].DataType;
          var value = subArray[j][dt];
          var keyA = subArray[j].Name;

          innerdata[keyA] = value;
        }
        inresult.push(innerdata);
      }
      var storeData;
      for (var list in inresult) {

        var change = inresult[list].CF_NETCHNG;
        var percent = parseFloat(inresult[list].PCTCHNG).toFixed(2);
        var forUtility = applicationManager.getFormatUtilManager();
        var balance = forUtility.formatAmountandAppendCurrencySymbol(inresult[list].CF_LAST, inresult[list].CF_CURRENCY);
        if (parseFloat(inresult[list].CF_NETCHNG) < 0) {
          storeData = {
            marketName: inresult[list].CF_NAME,
            amount: balance,
            profitLoss: {
              "skin": "sknlblff000015px",
              "text": change + " (" + percent + "%" + ")"
            },
          }
        } else {
          storeData = {
            marketName: inresult[list].CF_NAME,
            amount: balance,
            profitLoss: {
              "skin": "sknIW2F8523",
              "text": "+" + change + " (" + "+" + percent + "%" + ")"
            },

          }
        }
        segData.push(storeData);
      }
      this.view.lblTitle.text= segData[0].marketName;
      this.view.lblChange.text= segData[0].profitLoss.text;
      this.view.lblChange.skin= segData[0].profitLoss.skin;
      this.view.lblValue.text= segData[0].amount;
      this.view.CopylblTitle0ef672967b4ee42.text= segData[1].marketName;
      this.view.CopylblChange0aaa00601b36246.text= segData[1].profitLoss.text;
      this.view.CopylblChange0aaa00601b36246.skin= segData[1].profitLoss.skin;
      this.view.CopylblValue0c1edac75bf5448.text= segData[1].amount;
      this.view.CopylblTitle0b7443f112ccb43.text= segData[2].marketName;
      this.view.CopylblChange0fb3be785321d42.text= segData[2].profitLoss.text;
      this.view.CopylblChange0fb3be785321d42.skin= segData[2].profitLoss.skin;
      this.view.CopylblValue0idfe1df65b1042.text= segData[2].amount;

    },

  };
});