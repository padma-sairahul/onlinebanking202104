define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** cantSignIn defined for rememberMe **/
    AS_UWI_g76aff0fddc34de8b7f411729b3212de: function AS_UWI_g76aff0fddc34de8b7f411729b3212de() {
        var self = this;
        self.cantSignIn();
    },
    /** onClick defined for btnCancel **/
    AS_Button_bc940255f8a74cc5a42098ee2bebfdf5: function AS_Button_bc940255f8a74cc5a42098ee2bebfdf5(eventobject) {
        var self = this;
        this.onSignInCancel();
    },
    /** preShow defined for loginComponent **/
    AS_FlexContainer_jb031eb3bc634ec494f5e5ff0e79aa6b: function AS_FlexContainer_jb031eb3bc634ec494f5e5ff0e79aa6b(eventobject) {
        var self = this;
        this.preshow();
    },
    /** onBreakpointChange defined for loginComponent **/
    AS_FlexContainer_e6128e7e3d734f608b3a15574ed68b16: function AS_FlexContainer_e6128e7e3d734f608b3a15574ed68b16(eventobject, breakpoint) {
        var self = this;
        return self.onBreakpointChange.call(this);
    }
});