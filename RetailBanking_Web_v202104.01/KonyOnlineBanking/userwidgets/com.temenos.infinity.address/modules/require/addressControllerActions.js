define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for address **/
    AS_FlexContainer_e0df4f63c9b348769d28344a2d682110: function AS_FlexContainer_e0df4f63c9b348769d28344a2d682110(eventobject) {
        var self = this;
        this.preShow();
    },
    /** postShow defined for address **/
    AS_FlexContainer_d28b0edeaaad421cae93952578c5c56a: function AS_FlexContainer_d28b0edeaaad421cae93952578c5c56a(eventobject) {
        var self = this;
        this.postShow();
    },
    /** onBreakpointChange defined for address **/
    AS_FlexContainer_j048705ed7c745ef95db99157ce754c1: function AS_FlexContainer_j048705ed7c745ef95db99157ce754c1(eventobject, breakpoint) {
        var self = this;
        this.onBreakPointChange();
    }
});