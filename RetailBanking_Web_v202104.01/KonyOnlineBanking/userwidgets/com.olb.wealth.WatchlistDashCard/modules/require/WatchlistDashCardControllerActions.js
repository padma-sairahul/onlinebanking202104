define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for WatchlistDashCard **/
    AS_FlexContainer_c27fbbb09f08458f952cf70f5b87e47d: function AS_FlexContainer_c27fbbb09f08458f952cf70f5b87e47d(eventobject) {
        var self = this;
        return self.preShow.call(this);
    },
    /** onClick defined for btnViewAll **/
    AS_Button_ee8f7c8128eb4d3dbfa034f8e440fc53: function AS_Button_ee8f7c8128eb4d3dbfa034f8e440fc53(eventobject) {
        var self = this;
        return self.viewWatchlist.call(this);
    }
});