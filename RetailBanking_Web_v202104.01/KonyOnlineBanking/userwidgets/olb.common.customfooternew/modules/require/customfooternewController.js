define(['OLBConstants'], function(OLBConstants) {
	var orientationHandler = new OrientationHandler();
	return {
        postshow : function(){
          var currBreakpoint = kony.application.getCurrentBreakpoint();
          this.onBreakpointChangeComponent(currBreakpoint);
          
          this.view.btnLocateUs.skin="sknBtnSSP0273e315px";
          this.view.btnLocateUs.hoverSkin="sknbtn0161C115pxFocus";
          this.view.btnContactUs.skin="sknBtnSSP0273e315px";
          this.view.btnContactUs.hoverSkin="sknbtn0161C115pxFocus";
          this.view.btnPrivacy.skin="sknBtnSSP0273e315px";
          this.view.btnPrivacy.hoverSkin="sknbtn0161C115pxFocus";
          this.view.btnTermsAndConditions.skin="sknBtnSSP0273e315px";
          this.view.btnTermsAndConditions.hoverSkin="sknbtn0161C115pxFocus";
          this.view.btnFaqs.skin="sknBtnSSP0273e315px";
          this.view.btnFaqs.hoverSkin="sknbtn0161C115pxFocus";
        },
        onBreakpointChangeComponent: function(width){
          if(width==undefined)
            width = kony.application.getCurrentBreakpoint();

          if(width===640 || orientationHandler.isMobile){
            var menuWidth = this.view.btnLocateUs.frame.width+43+this.view.btnContactUs.frame.width;
            if(menuWidth<100){
              menuWidth = 175;
            }
            this.view.width ="100%";
            this.view.flxFooterMenu.width = menuWidth+"dp";
             //this.view.flxFooterMenu.width = "100%";
            this.view.flxFooterMenu.left = "";
            this.view.flxFooterMenu.centerX = "50%";

            this.view.lblCopyright.centerX = "50%";
            this.view.lblCopyright.width = "100%";          
            this.view.lblCopyright.contentAlignment = constants.CONTENT_ALIGN_CENTER;
            
            this.view.flxVBar2.setVisibility(false);
            this.view.btnPrivacy.setVisibility(false);
            this.view.flxVBar3.setVisibility(false);
            this.view.btnTermsAndConditions.setVisibility(false);
            this.view.flxVBar4.setVisibility(false);
            this.view.btnFaqs.setVisibility(false);
            this.view.btnContactUs.left = "20dp";     
          }else if(width===1024 || orientationHandler.isTablet){
            this.view.width ="100%";
            this.view.lblCopyright.centerX = "";
            this.view.flxFooterMenu.centerX = "";
            this.view.btnLocateUs.setVisibility(false);
            this.view.flxVBar1.setVisibility(false);
            this.view.lblCopyright.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
            this.view.lblCopyright.left="2.55%";
            this.view.flxFooterMenu.left="2.55%";
            this.view.btnContactUs.left = "0dp";

          }else if(width===1366){
            this.view.width ="100%";
            //this.view.flxFooterMenu.width="82%";
            //this.view.lblCopyright.width="82%";
            this.view.lblCopyright.centerX = "";
            this.view.flxFooterMenu.centerX = "";
            this.view.lblCopyright.left = "9%";
            this.view.flxFooterMenu.left = "9%";
            this.view.lblCopyright.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
            this.view.btnContactUs.left = "20dp";
          }
          else{
            this.view.width ="1200dp";
            this.view.centerX="50%";
            //this.view.flxFooterMenu.width="82%";
            this.view.flxFooterMenu.left = "0%";
            this.view.lblCopyright.centerX = "";
            this.view.flxFooterMenu.centerX = "";
            //this.view.lblCopyright.width="82%";
            this.view.lblCopyright.left = "0%";
            this.view.lblCopyright.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
            this.view.btnContactUs.left = "20dp";
          }
          this.view.forceLayout();
        },
        setPosition: function(callback){
          this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;
                } else {
                    this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
                }
            } else {
                this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
            }
            this.view.forceLayout();

            if(callback!=null || callback!=undefined){
              callback();
            }
        },
      /**
        * Method to laad Information Module and show Locate us
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
		    showLocateUsPage : function() {
          var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
          locateUsModule.presentationController.showLocateUsPage();
        },
      
        /**
        * Method to laad Information Module and show FAQs
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
      	showFAQs : function(){
        var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
        InformationContentModule.presentationController.showFAQs();
        },
      	 /**
        * Method to laad Information Module and show terms and conditions page
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
      	showTermsAndConditions:function(){
          var termsAndConditionModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TermsAndConditionsModule");
          termsAndConditionModule.presentationController.showTermsAndConditions(OLBConstants.TNC_FLOW_TYPES.Footer_TnC);
        },
      	        /**
        * Method to laad Information Module and show ContactUs Page.
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */

      	showContactUsPage:function(){
          var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
          InformationContentModule.presentationController.showContactUsPage();
        },
              /**
        * Method to laad Information Module and show privacy policy page.
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
      
      	showPrivacyPolicyPage:function(){
       	var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
         InformationContentModule.presentationController.showPrivacyPolicyPage();
    	}
      	
	};
});