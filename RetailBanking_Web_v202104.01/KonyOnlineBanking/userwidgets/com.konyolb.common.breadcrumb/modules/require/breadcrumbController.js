define(function() {

	return {

      navigateForm:function(){
       var value=this.view.btnBreadcrumb1.text;
       var navObj;
        if(value==kony.i18n.getLocalizedString("i18n.hamburger.transfers"))
         {
           applicationManager.getModulesPresentationController("TransferModule").showTransferScreen();
         }
        else if(value==kony.i18n.getLocalizedString("i18n.billPay.BillPay"))
          {
           var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
           billPayModule.presentationController.showBillPayData();
          }
        else if(value==kony.i18n.getLocalizedString("i18n.topmenu.accounts"))
          {
           var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
		   accountModule.presentationController.showAccountsDashboard();
          }
		  else if(value== "ALERTS & MESSAGES")
          {
           var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
           alertsModule.presentationController.showAlertsPage();
          }
          else if(value== kony.i18n.getLocalizedString("i18n.transfers.wireTransfer"))
          {
            var WireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
            WireTransferModule.presentationController.showWireTransfer();
          }
        else 
          {
            //no action 
          }
      },
      
//       [{text:"TRANSFERS", callback: function () {}}, {text:"MAKE TRANSFER", callback: function () {}}, {text:"RECENT"}]
      setBreadcrumbData: function (data) {
          this.view.btnBreadcrumb1.text = data[0].text;
          this.view.btnBreadcrumb1.toolTip = data[0].text;
          if(data[0].callback !== undefined) {
            this.view.btnBreadcrumb1.onClick = data[0].callback;
          }
          if(data[1].callback === undefined){
            this.view.lblBreadcrumb2.setVisibility(true);
            this.view.lblBreadcrumb2.text=data[1].text;
            this.view.lblBreadcrumb2.toolTip=data[1].text;
            this.view.btnBreadcrumb2.setVisibility(false);
          }
          else{
            this.view.lblBreadcrumb2.setVisibility(false);
            this.view.btnBreadcrumb2.setVisibility(true);
            this.view.btnBreadcrumb2.text=data[1].text;
            this.view.btnBreadcrumb2.toolTip=data[1].text;
            this.view.btnBreadcrumb2.onClick = data[1].callback;
          }
        
          if(data.length > 2) {
            this.view.imgBreadcrumb2.setVisibility(true);
            this.view.lblBreadcrumb3.setVisibility(true);
            this.view.lblBreadcrumb3.text = data[2].text;
            this.view.lblBreadcrumb3.toolTip = data[2].text;
          } else{
            this.view.imgBreadcrumb2.setVisibility(false);
            this.view.lblBreadcrumb3.setVisibility(false);
          }
      }
	};
});