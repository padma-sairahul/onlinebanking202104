/*eslint require-jsdoc:0
          complexity:0
*/
define(['ViewConstants','CommonUtilities', 'TopbarConfig', 'HamburgerConfig', 'FormControllerUtility'], function(ViewConstants, CommonUtilities, TopbarConfig, HamburgerConfig, FormControllerUtility) {
  function getTotalHeight() {
    return kony.os.deviceInfo().screenHeight;
 //   var height = 0;
 //   var widgets = kony.application.getCurrentForm().widgets();
 //   for (var i = 0; i < 3; i++) {
 //     var widget = widgets[i];
 //     height += widget.frame.height;
 //   }
 //   height += kony.application.getCurrentForm().flxFooter.frame.y;
 //   return height;
  }

  var orientationHandler = new OrientationHandler();

  var MENU_CONTAINER = "flxHamburgerMenu";
  var SAMPLE_MENU = "flxMenuItem";
  var SAMPLE_SUB_MENU = "flxSubMenu";
  var SAMPLE_SUB_MENU_ITEM = "flxSubMenuItem";
  var onItemSelectListener = null;

  return {
    initialized: false,
    actiavteWhenInitialized: null,
    isPreLoginView: false,

    preShow: function() {
      FormControllerUtility.updateWidgetsHeightInInfo(this.view, ["flxMenuContainer","flxMenuWrapper","lblTransferAndPay"]);
      var configurationManager = applicationManager.getConfigurationManager();
      var isUserLoggedIn = applicationManager.getUserPreferencesManager().isUserLoggedin();
      if (configurationManager.enableAlertsIcon === "true" && isUserLoggedIn) {
        this.view.flxNotifications.isVisible = true;
      } else {
        this.view.flxNotifications.isVisible = false;
      }
      this.updateAlertIcon();

      if (!this.initialized && applicationManager.getUserPreferencesManager().isUserLoggedin()) {
        this.generateMenu();
        this.initialized = true;
        if (this.actiavteWhenInitialized) {
          this.activateMenu(this.actiavteWhenInitialized.parentId, this.actiavteWhenInitialized.childId)
          this.actiavteWhenInitialized = null;
        }
      }
      CommonUtilities.setText(this.view.imgKony, kony.i18n.getLocalizedString("i18n.a11y.common.logo"), CommonUtilities.getaccessibilityConfig());
       var messageText = this.view.imgMessages.text;
      CommonUtilities.setText(this.view.imgMessages, kony.i18n.getLocalizedString("i18n.a11y.common.messages"), CommonUtilities.getaccessibilityConfig());
      this.view.imgMessages.text = messageText;
      this.setupContextualActions();
      this.setupHamburger();      
      this.setupLogout();
      this.setupUserActions();
      this.setHoverSkins();
      flag = 0;
      this.view.lblTransferAndPay.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.lblMyBills.toolTip = kony.i18n.getLocalizedString("i18n.Pay.MyBills");
      this.view.imgLblTransfers.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.flxAccounts.onClick = TopbarConfig.config.ACCOUNTS.onClick;
      this.view.flxMyBills.onClick = TopbarConfig.config.BILLS.onClick;
      this.view.flxNotifications.onClick = TopbarConfig.config.NOTIFICATIONS.onClick;
      this.view.flxMessages.onClick = TopbarConfig.config.MESSAGES.onClick;
      this.view.flxHelp.onClick = TopbarConfig.config.HELP.onClick;
      this.view.flxFeedback.onClick = TopbarConfig.config.FEEDBACK.onClick;
      this.view.flxUser.onClick = this.showUserActions;
      this.view.imgKonyHamburger.onTouchEnd= this.showDashboardScreen;
      this.view.imgKony.onTouchEnd= this.showDashboardScreen;
      this.forceCloseHamburger();
      this.view.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
      this.view.flxContextualMenu.setVisibility(false);
      this.view.flxAccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;

      this.onBreakpointChangeComponent();
    },
    isBillPayEnabled: function () {
      return applicationManager.getConfigurationManager().checkUserFeature("BILL_PAY")
    },
    isTransfersAndPayEnabled: function () {
      return [
         "INTERNATIONAL_ACCOUNT_FUND_TRANSFER",
        "INTER_BANK_ACCOUNT_FUND_TRANSFER",
        "INTRA_BANK_FUND_TRANSFER",
        "TRANSFER_BETWEEN_OWN_ACCOUNT",
        "P2P"
    ].some(function (permission) {
      return applicationManager.getConfigurationManager().checkUserFeature(permission);
    })
    },
    postShow: function() {
      var configurationManager = applicationManager.getConfigurationManager();
      this.view.flxMenuRight.setVisibility(false);
      this.view.flxUserActions.left = 945 + this.view.flxMenuContainer.frame.x + "dp";
      var flex_menu_width = 50;
      this.view.flxMenu.width = flex_menu_width + "dp";
      var flex_accounts_width = 25 + this.view.lblAccounts.frame.width + 50;
      this.view.flxAccounts.width = flex_accounts_width + "dp";
      var flex_transfers_width;
      if(configurationManager.getDeploymentGeography() == "EUROPE"){
        flex_transfers_width = 150 + 80;
        this.view.lblTransferAndPay.text = kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentsAndTransfers");
        this.view.lblTransferAndPay.toolTip = kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentsAndTransfers");
      }else{
        flex_transfers_width = 50 + this.view.lblTransferAndPay.info.frame.width + 50;
      }
      this.view.flxTransfersAndPay.width = flex_transfers_width + "dp";
      //var left_for_contextual_menu = flex_transfers_width + 1;
      var left_for_contextual_menu = flex_transfers_width;
      left_for_contextual_menu = "-" + left_for_contextual_menu + "dp";
      this.view.flxContextualMenu.left = left_for_contextual_menu;
      this.view.flxContextualMenu.width = flex_transfers_width + "dp";
      this.setupUserProfile();

      this.view.flxHamburger.height = kony.os.deviceInfo().screenHeight + "px";
      if (configurationManager.isFastTransferEnabled == "true") {
              if (this.isTransfersAndPayEnabled()) {
                     if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile)
                    this.view.flxTransfersAndPay.setVisibility(false);
                    else
                       this.view.flxTransfersAndPay.setVisibility(true);
                } else {
                    this.view.flxTransfersAndPay.setVisibility(false);
                }
                if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile || !this.isBillPayEnabled())
                {
                this.view.flxSeparatorNew.setVisibility(false);
                this.view.flxMyBills.setVisibility(false);
                }
                else
                {
                  this.view.flxSeparatorNew.setVisibility(true);
                this.view.flxMyBills.setVisibility(true);  
                }
            }
            this.setCustomHeaderLogo();
      this.setHeaderLogo();
      this.view.forceLayout();
    },

    
     showDashboardScreen: function(){
      if(applicationManager.getUserPreferencesManager().isUserLoggedin()) {
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountsModule.presentationController.showAccountsDashboard();
        }
    },
    
    setupLogout: function() {
      this.view.flxLogout.onClick = this.showLogout.bind(this);
      if (TopbarConfig.config.LOGOUT.excludedForms.indexOf(kony.application.getCurrentForm().id) < 0) {
        this.view.btnLogout.text = "";
        this.view.btnLogout.setVisibility(true);
        this.view.imgLogout.setVisibility(true);
        this.view.btnLogout.onClick = this.showLogout.bind(this);
      }
    },
    showLogout: function() {
      var currentForm = kony.application.getCurrentForm();
      if (('flxLogout' in currentForm) && ('flxDialogs' in currentForm)) {
        this.closeHamburgerMenu();
        currentForm.flxDialogs.setVisibility(true);
        currentForm.flxLogout.setVisibility(true);
        currentForm.flxLogout.left = "0%";
        var popupComponent = currentForm.flxLogout.widgets()[0];
        popupComponent.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        popupComponent.lblHeading.accessibilityConfig = {
          "a11yLabel": kony.i18n.getLocalizedString("i18n.common.logout")
        };
        popupComponent.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        popupComponent.lblPopupMessage.accessibilityConfig = {
          "a11yLabel":  kony.i18n.getLocalizedString("i18n.common.LogoutMsg")
        };
        popupComponent.top = ((kony.os.deviceInfo().screenHeight / 2) - 135) + "px";
        popupComponent.btnYes.onClick = function() {
          FormControllerUtility.showProgressBar(kony.application.getCurrentForm());
          TopbarConfig.config.LOGOUT.onClick();
          currentForm.flxDialogs.setVisibility(false);
          currentForm.flxLogout.left = "-100%";
        };
        popupComponent.btnNo.onClick = function() {
          currentForm.flxDialogs.setVisibility(false);
          currentForm.flxLogout.left = "-100%";
        }
        popupComponent.flxCross.onClick = function() {
          currentForm.flxDialogs.setVisibility(false);
          currentForm.flxLogout.left = "-100%";
        }
      }
    },
    setupUserActions: function() {
      this.view.segUserActions.widgetDataMap = {
        lblSeparator: "lblSeparator",
        lblUsers: "lblUsers"
      };
      var actions = TopbarConfig.config.USER_ACTIONS.filter(function(action){
        return (!action.isVisible || action.isVisible());
      })
      this.view.segUserActions.setData(actions.map(function(configItem) {
        return {
          lblSeparator: "L",
          lblUsers: {
            "text": kony.i18n.getLocalizedString(configItem.text),
            "toolTip": kony.i18n.getLocalizedString(configItem.text)
          }
        }
      }));
      this.view.segUserActions.onRowClick = function(widget, section, rowIndex) {
        actions[rowIndex].onClick();
      }
    },
      
   /**
     * Method will set email attribute to  user from  entitlement configuration  
   **/
   setEmailForUserObj : function() {
    var userEntitlementsEmailIds = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
      for(var i = 0; i < userEntitlementsEmailIds.length; i++) {
           if (userEntitlementsEmailIds[i].isPrimary === "true") {
               return userEntitlementsEmailIds[i].Value;
           }
       }
       return "";
    },
    setupUserProfile: function() {
      var userObject = applicationManager.getUserPreferencesManager().getUserObj();   
      var userImageURL = applicationManager.getUserPreferencesManager().getUserImage();
      if(applicationManager.getConfigurationManager().getProfileImageAvailabilityFlag() === true && userImageURL && userImageURL.trim() != "") 
       this.view.imgUser.base64 = userImageURL;
     else
       this.view.imgUser.src =  ViewConstants.IMAGES.USER_DEFAULT_IMAGE;
     if(applicationManager.getConfigurationManager().getProfileImageAvailabilityFlag() === true && userImageURL && userImageURL.trim() != "") 
       this.view.CopyimgToolTip0i580d9acc07c42.base64 = userImageURL;
     else
       this.view.CopyimgToolTip0i580d9acc07c42.src =  ViewConstants.IMAGES.USER_DEFAULT_IMAGE;
      this.view.lblName.text = (userObject.userlastname === null) ? userObject.userfirstname : (userObject.userfirstname === null) ? userObject.userlastname : userObject.userfirstname + " " + userObject.userlastname;
      this.view.lblUserEmail.text = this.setEmailForUserObj();
      this.view.flxUserActions.isVisible = false;
    },
    setupContextualActions: function() {
      var scope = this;
      var configurationManager = applicationManager.getConfigurationManager();
      if(!this.isTransfersAndPayEnabled() && !this.isBillPayEnabled()){
        this.view.flxTransfersAndPay.setVisibility(false);
        this.view.imgLblTransfers.setVisibility(false);
        this.view.lblTransferAndPay.setVisibility(false);
      } else {
        this.view.flxTransfersAndPay.onClick = this.openContextualMenu.bind(this);
        this.view.flxTransfersAndPay.onTouchStart = function() {
          //return;
          if (scope.view.flxContextualMenu.isVisible) {
             scope.view.flxTransfersAndPay.origin = true;
             if (kony.application.getCurrentBreakpoint() == 640 || kony.application.getCurrentBreakpoint() == 1024) {
               scope.view.flxContextualMenu.isVisible = false;
               scope.view.flxTransfersAndPay.skin = "flxHoverSkinPointer";
               scope.view.imgLblTransfers.text = "O";
             }
           }
        }
        TopbarConfig.config.getContextualMenu().forEach(function(contextualItem) {
          scope.view[contextualItem.widget].setVisibility(!contextualItem.isVisible || contextualItem.isVisible())
          scope.view[contextualItem.widget].onClick = contextualItem.onClick;
        });
      }
    },
    openContextualMenu: function() {
      var scope = this;
      if (scope.view.flxTransfersAndPay.skin === "sknFlxHeaderTransfersSelected") {
        scope.view.flxTransfersAndPay.skin = "flxHoverSkinPointer";
        scope.view.flxTransfersAndPay.hoverSkin = "flxHoverSkinPointer000000op10";
        scope.view.imgLblTransfers.text = "O";
      } else {
        scope.view.flxTransfersAndPay.skin = "sknFlxHeaderTransfersSelected";
        scope.view.imgLblTransfers.text = "P";
      }
      scope.view.showContextualMenu();
    },
    setupHamburger: function() {
      var scope = this;
      this.setItemSelectListener(this.closeHamburgerMenu.bind(this));
      this.view.flxMenu.onClick = function() {
        scope.openHamburgerMenu();
      }
      this.view.flxHamburgerBack.onClick = this.closeHamburgerMenu.bind(this);
      this.view.flxClose.onClick = this.closeHamburgerMenu.bind(this);
    },
    openHamburgerMenu: function() {
      this.view.imgKony.setFocus(true);
      this.view.flxHamburger.height = kony.os.deviceInfo().screenHeight + "dp";
      this.view.flxHamburger.height = (kony.os.deviceInfo().screenHeight) + "dp";
      this.view.flxHamburgerMenu.height = (kony.os.deviceInfo().screenHeight - 160) + "dp";

      // this.view.flxLogout.top = (kony.os.deviceInfo().screenHeight - 80) + "dp";

      this.showBackFlex();
      var scope = this;
      var animationDefinition = {
        100: {
          "left": 0
        }
      };
      var animationConfiguration = {
        duration: 0.8,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function() {
          scope.view.forceLayout();
        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      this.view.flxHamburger.animate(animationDef, animationConfiguration, callbacks);
    },
    closeHamburgerMenu: function() {
      var self = this;
      var animationDefinition = {
        100: {
          "left": "-200.13%"
        }
      };
      var animationConfiguration = {
        duration: 1.5,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function() {
          self.hideBackFlex()
        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      this.view.flxHamburger.animate(animationDef, animationConfiguration, callbacks);
    },
    showBackFlex: function() {
      this.view.flxHamburgerBack.height = getTotalHeight() + "px";
      this.view.flxHamburgerBack.isVisible = true;
      this.view.flxHamburger.isVisible = true;
      this.view.forceLayout();
    },
    hideBackFlex: function() {
      this.view.flxHamburgerBack.height = "0px";
      this.view.forceLayout();
    },
    showUserActions: function() {
      if (this.view.flxMenuContainer.info.frame.x == 0) {
        this.view.flxUserActions.left = -410 + this.view.flxMenuWrapper.info.frame.width + "dp";
        if (kony.application.getCurrentBreakpoint() == 1024 || orientationHandler.isTablet) {
          this.view.flxUserActions.left = -365 + this.view.flxMenuWrapper.info.frame.width + "dp";
        }
      } else {
        this.view.flxUserActions.left = 945 + this.view.flxMenuWrapper.info.frame.x + "dp";
      }
      if (this.view.flxUserActions.isVisible === false) {
        this.view.flxUserActions.isVisible = true;
        this.view.lblName.setFocus(true);
      } else {
        this.view.flxUserActions.isVisible = false;
      }
    },
    forceCloseHamburger: function() {
      this.hideBackFlex();
      this.view.flxHamburger.left = "-200%";
      this.view.forceLayout();
    },

    setHoverSkins: function() {
      var currForm = kony.application.getCurrentForm().id;
      this.view.imgKony.cursorType="pointer";
      this.setDefaultHoverSkins();
      this.view.flxContextualMenu.isVisible = false;
      this.view.imgLblTransfers.text = "O";
      this.setDefaultSkins();
      if (currForm === 'frmAccountsDetails' || currForm === 'frmAccountsLanding' || currForm === 'frmBBAccountsLanding' || currForm === 'frmExternalAccounts' || currForm === 'frmDashboard')
        this.view.flxAccounts.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
      else if (currForm === 'frmTransfers' || currForm === 'frmPayAPerson' || currForm === 'frmWireTransfer' || currForm === 'frmAcknowledgement' || currForm === 'frmConfirm' || currForm === 'frmVerifyAccount')
        this.view.flxTransfersAndPay.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
      else if (currForm === 'frmCustomerFeedback' || currForm === 'frmCustomerFeedbackSurvey') {
        this.view.flxFeedback.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
        this.view.flxFeedback.skin = "sknFlxFFFFFbrdr3343a8Pointer";
        this.view.lblFeedback.skin = "sknLblffffff15pxSSP";
      } else if (currForm === 'frmOnlineHelp')
        this.view.flxHelp.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
       else if (kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule").moduleConfig.Forms.desktop[currForm]) {
         this.view.flxMyBills.skin = "sknFlxFFFFFbrdr3343a8Pointer";
         this.view.flxMyBills.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer"; 
         this.view.flxTransfersAndPay.skin = "slFbox";
        }
       else if (kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule").moduleConfig.Forms.desktop[currForm] || kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferEurModule").moduleConfig.Forms.desktop[currForm]) { 
         this.view.flxMyBills.skin = "slFbox";
         this.view.flxAccounts.skin = "slFbox";
         this.view.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8Pointer";
         this.view.flxTransfersAndPay.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer"; 
        }
    },
    setDefaultHoverSkins: function() {
      this.view.flxAccounts.hoverSkin = "flxHoverSkinPointer000000op10";
      this.view.flxTransfersAndPay.hoverSkin = "flxHoverSkinPointer000000op10";
      this.view.flxMyBills.hoverSkin = "flxHoverSkinPointer000000op10";
    },
     setDefaultSkins: function(){
       this.view.flxMyBills.skin = "flxHoverSkinPointer";
    },
    onBreakpointChangeComponent: function(width) {
      if (width === undefined || width === null || typeof(width) === "object") {
        width = kony.application.getCurrentBreakpoint();
      }
      var scope = this;

      this.setLayoutProperties(width);
      this.view.flxHamburger.height = kony.os.deviceInfo().screenHeight + "dp";
      this.view.flxHamburgerMenu.height = (kony.os.deviceInfo().screenHeight - 160) + "dp";

      this.view.flxUserActions.setVisibility(false);
      this.view.lblHeaderMobile.setVisibility(false);

      if (this.isPreLoginView) {
        this.showPreLoginView();
      } else {
        this.showPostLoginView();
      }

      if (width === 640 || orientationHandler.isMobile) {
        scope.view.flxHamburger.width = "90%";
        scope.view.imgKonyHamburger.isVisible = true;
        this.view.lblHeaderMobile.setVisibility(true);
        this.view.flxShadowContainer.setVisibility(false);
      } else if (width === 1024 || width === 768 || orientationHandler.isTablet) {
        scope.view.flxHamburger.width = "60%";
        scope.view.imgKonyHamburger.isVisible = true;
      } else {
        if(width === 1366){
          scope.view.flxHamburger.width = "500dp";
        }
        else{
          scope.view.flxHamburger.width = "28%";
        }
        scope.view.imgKonyHamburger.isVisible = true;
      }
      if (width == 640 || orientationHandler.isMobile) {
        this.view.flxTransfersAndPay.onTouchStart = null;
      } else if (width == 1024 || orientationHandler.isTablet) {
        this.view.flxTransfersAndPay.onTouchStart = null;
      } else {

      }
    },
    setLayoutProperties: function(width){
      if(width==640 || orientationHandler.isMobile){
        this.view.height = "50dp";
        this.view.parent.height = "50dp";
        this.view.flxLogoAndActionsWrapper.width = "100%";
        this.view.flxMenuWrapper.width = "100%";
        this.view.flxLogoAndActions.isVisible = false;
        this.view.flxMenuContainer.top = "0dp";
        this.view.lblHeaderMobile.isVisible = true;
        this.view.flxMenuLeft.left = "10dp";
        this.view.flxMenuRight.isVisible = false;
        this.view.flxAccounts.isVisible = false;
        this.view.flxSeperator1.isVisible = false;
        this.view.flxTransfersAndPay.isVisible = false;
        this.view.flxContextualMenu.isVisible = false;
      }else if(width==1024 || orientationHandler.isTablet){
        this.view.height = "120dp";
        this.view.parent.height = "120dp";
        this.view.flxLogoAndActionsWrapper.width = "100%";
        this.view.flxMenuWrapper.width = "100%";
        this.view.flxLogoAndActions.isVisible = true;
        this.view.flxMenuContainer.top = "70dp";
        this.view.lblHeaderMobile.isVisible = false;
        this.view.flxMenuRight.isVisible = false;
        this.view.flxAccounts.isVisible = true;
        this.view.flxSeperator1.isVisible = true;
        this.view.flxTransfersAndPay.isVisible = true;
        this.view.flxContextualMenu.isVisible = false;
        this.view.imgKony.left = "20dp";
        this.view.flxActionsMenu.right = "15dp";
        this.view.flxMenuLeft.left = "17dp";
        this.view.flxMenuRight.right = "22dp";
        this.view.flxMenuRight.width = "40%";

        this.view.flxFeedback.right="-4%";
        this.view.flxFeedback.width="130dp";
        this.view.lblFeedback.right = "0px";
        this.view.flxverseperator2.left = "17dp"
        this.view.flxHelp.right = "114dp";
        this.view.flxHelp.width = "50dp";
        this.view.lblHelp.centerx="50%";
        this.view.lblHelp.centerX = "50%";
        
      }else if(width==1366 && orientationHandler.isDesktop){
        this.view.flxLogout.width = "87.09%";
        this.view.flxLogout.centerX = "50%";
        this.view.height = "120dp";
        this.view.parent.height = "120dp";
        this.view.flxLogoAndActionsWrapper.width = "100%";
        this.view.flxMenuWrapper.width = "100%";
        this.view.flxLogoAndActions.isVisible = true;
        this.view.flxMenuContainer.top = "70dp";
        this.view.lblHeaderMobile.isVisible = false;
        this.view.flxMenuRight.isVisible = false;
        this.view.flxAccounts.isVisible = true;
        this.view.flxSeperator1.isVisible = true;
        this.view.flxTransfersAndPay.isVisible = true;
        this.view.flxContextualMenu.isVisible = false;
        this.view.imgKony.left = "83dp";
        this.view.flxMenuRight.width = "30%";
        this.view.flxActionsMenu.right = "71dp";
        this.view.flxMenuLeft.left = "78dp";
        this.view.flxMenuRight.right = "55dp";
        this.view.flxHamburger.width = "500dp";
        this.view.flxFeedback.right="-4%";
        this.view.flxFeedback.width="130dp";
        this.view.lblFeedback.right = "0px";
        this.view.flxverseperator2.left = "17dp"
        this.view.flxHelp.right = "114dp";
        this.view.flxHelp.width = "50dp";
        this.view.lblHelp.centerx="50%";
        this.view.lblHelp.centerX = "50%";
      }else{
        this.view.flxLogout.width = "87.09%";
        this.view.flxLogout.centerX = "50%";
        this.view.height = "120dp";
        this.view.parent.height = "122dp";
        this.view.flxLogoAndActionsWrapper.width = "1366dp";
        this.view.flxMenuWrapper.width = "1366dp";
        this.view.flxLogoAndActions.isVisible = true;
        this.view.flxMenuContainer.top = "70dp";
        this.view.lblHeaderMobile.isVisible = false;
        this.view.flxMenuRight.isVisible = false;
        this.view.flxAccounts.isVisible = true;
        this.view.flxSeperator1.isVisible = true;
        this.view.flxTransfersAndPay.isVisible = true;
        this.view.flxContextualMenu.isVisible = false;
        this.view.imgKony.left = "83dp";
        this.view.flxActionsMenu.right = "76dp";
        this.view.flxMenuRight.width = "30%"
        this.view.flxMenuLeft.left = "78dp";
        this.view.flxMenuRight.right = "70dp";

        this.view.flxFeedback.right="-4%";
        this.view.flxFeedback.width="130dp";
        this.view.lblFeedback.right = "0px";
        this.view.flxverseperator2.left = "17dp"
        this.view.flxHelp.right = "114dp";
        this.view.flxHelp.width = "50dp";
        this.view.lblHelp.centerx="50%";
        this.view.lblHelp.centerX = "50%";
      }
    },
    setHeaderLogo : function() {
      var configurationManager = applicationManager.getConfigurationManager();
      if(configurationManager.isSMEUser === "true") {
        this.view.imgKony.src = ViewConstants.INFINITY_LOGOS.SME;
        this.setHeaderLogoResponsiveForSMEandMBB();
        }
      else if(configurationManager.isMBBUser === "true") {
        this.view.imgKony.src = ViewConstants.INFINITY_LOGOS.MBB;
        this.setHeaderLogoResponsiveForSMEandMBB();
      }
      else if(configurationManager.isRBUser === "true")this.view.imgKony.src = ViewConstants.INFINITY_LOGOS.RB;
      else this.view.imgKony.src = ViewConstants.INFINITY_LOGOS.GENERIC;
    },
    setHeaderLogoResponsiveForSMEandMBB : function() {
      var width = kony.application.getCurrentBreakpoint();
      var orientationHandler = new OrientationHandler();
      
      if(orientationHandler.isDesktop) {
        if(width === 1366){
          this.view.imgKony.width = "100dp";
          this.view.imgKony.left = "83dp";
        }
        else if(width === 1380 || width === constants.BREAKPOINT_MAX_VALUE){
          this.view.imgKony.width = "100dp";
          this.view.imgKony.left = "83dp";
        }
      }
      else if(orientationHandler.isTablet){
        if(width === 1024){
          this.view.imgKony.width = "175dp";
          this.view.imgKony.left = "70dp";
        }
        else if(width === 1366){
          this.view.imgKony.width = "100dp";
          this.view.imgKony.left = "83dp";
        }
        else if(width === 1380 || width === constants.BREAKPOINT_MAX_VALUE || width > 1380){
          this.view.imgKony.width = "100dp";
          this.view.imgKony.left = "83dp";
        }
      }
  },
    showPreLoginView: function() {
      this.isPreLoginView = true;

      if (orientationHandler.isMobile || kony.application.getCurrentBreakpoint() == 640) {
        this.view.flxMenuContainer.setVisibility(true);
        this.view.flxMenuWrapper.setVisibility(false);
      } else {
        this.view.height = "70dp";
        this.view.flxMenuContainer.setVisibility(false);
        this.view.flxShadowContainer.top = "70dp";
        this.view.flxNotifications.isVisible = false;
        this.view.flxMessages.isVisible = false;
        this.view.flxSeperator1.isVisible = false;
        this.view.flxUser.isVisible = false;
        this.view.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.login");
        this.view.btnLogout.setVisibility(true);
        this.view.imgLogout.src = "login_icon_locateus.png";
        this.view.flxWarning.setVisibility(true);
      }
    },
    showPostLoginView: function() {
      var isRetailUser = applicationManager.getConfigurationManager().getConfigurationValue('isRBUser') === "true";
      this.isPreLoginView = false;

      if (orientationHandler.isMobile || kony.application.getCurrentBreakpoint() == 640) {
        this.view.flxMenuContainer.setVisibility(true);
        this.view.flxMenuWrapper.setVisibility(true);
      } else {
        this.view.height = "120dp";
        this.view.flxMenuContainer.setVisibility(true);
        this.view.flxShadowContainer.top = "120dp";
        this.view.flxNotifications.isVisible = true;
        if(isRetailUser){
          this.view.flxVerticalSeperator3.isVisible = true;
          this.view.flxMessages.isVisible = true;
          this.view.flxMessages.right = "210dp";
        }
        this.view.flxSeperator1.isVisible = true;
        this.view.flxUser.isVisible = true;
        this.view.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.logout");
        this.view.imgMessages.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Message");
        this.view.lblNewNotifications.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts");
        this.view.btnLogout.setVisibility(true);
        this.view.imgLogout.src = "logout.png";
        this.view.flxWarning.setVisibility(false);
      }
    },

    setMessageBadgeSize: function() {
      var numberOfMessages = parseInt(this.view.lblNewNotifications.text);
      if (numberOfMessages <= 99) {
        this.view.lblNewNotifications.width = "15dp";
        this.view.lblNewNotifications.height = "15dp";
      } else {
        this.view.lblNewNotifications.width = "20dp";
        this.view.lblNewNotifications.height = "20dp";
      }
    },
    updateAlertIcon: function() {
      this.view.lblNewNotifications.setVisibility(false);
      var unreadCount = applicationManager.getConfigurationManager().getUnreadMessageCount();
      if (unreadCount.count > 0) {
        this.view.imgNotifications.src = ViewConstants.IMAGES.NOTIFICATION_FLAG;
      } else {
        this.view.imgNotifications.src = ViewConstants.IMAGES.NOTIFICATION_ICON;
      }
    },

    navigateToBillPay: function() {
      this.view.flxContextualMenu.isVisible = false;
      this.view.flxMenu.skin = "slFbox";
      this.view.flxAccounts.skin = "slFbox";
      this.view.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.forceLayout();
    },
    showContextualMenu: function() {
      var configurationManager = applicationManager.getConfigurationManager();
      if (this.view.flxTransfersAndPay.origin) {
        this.view.flxTransfersAndPay.origin = false;
        this.view.flxTransfersAndPay.skin = "flxHoverSkinPointer";
        this.view.imgLblTransfers.text = "O";
        return;
      }
      if (this.view.flxContextualMenu.isVisible === true) {
        this.view.flxTransfersAndPay.skin = "flxHoverSkinPointer";
        this.view.flxContextualMenu.isVisible = false;
        this.view.imgLblTransfers.text = "O";
        this.view.forceLayout();
      } else {
        this.view.flxContextualMenu.isVisible = true;
        this.view.flxTransfersAndPay.skin = "sknFlxHeaderTransfersSelected";
        this.view.imgLblTransfers.text = "P";
        this.view.forceLayout();
      }
       this.view.flxPayMultipleBeneficiaries.isVisible = false;
      if (configurationManager.isFastTransferEnabled == "true") {
                if (this.isTransfersAndPayEnabled()) {
                    this.view.Label0dcf00103bdba46.text = kony.i18n.getLocalizedString("i18n.hamburger.transfer");
                    this.view.Label0dcf00103bdba46.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfer");
                    this.view.CopyLabel0bb648d916e554d.text = kony.i18n.getLocalizedString("i18n.Transfers.TRANSFERACTIVITIES");
                    this.view.CopyLabel0bb648d916e554d.toolTip = kony.i18n.getLocalizedString("i18n.Transfers.TRANSFERACTIVITIES");
                    this.view.CopyLabel0a8b12f7002084d.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient");
                    this.view.CopyLabel0a8b12f7002084d.toolTip = kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient");
                    this.view.CopyLabel0h9e71b8a76ad44.text = kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient");
                    this.view.CopyLabel0h9e71b8a76ad44.toolTip = kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient");
            }
           
        }
        if(configurationManager.getDeploymentGeography() == "EUROPE"){
          this.view.Label0dcf00103bdba46.text = kony.i18n.getLocalizedString("i18n.TransfersEur.MakePayment");
          this.view.Label0dcf00103bdba46.toolTip = kony.i18n.getLocalizedString("i18n.TransfersEur.MakePayment");
          this.view.CopyLabel0bb648d916e554d.text = kony.i18n.getLocalizedString("i18n.TransfersEur.TransferBetweenAccounts");
          this.view.CopyLabel0bb648d916e554d.toolTip =  kony.i18n.getLocalizedString("i18n.TransfersEur.TransferBetweenAccounts");
          this.view.CopyLabel0a8b12f7002084d.text = kony.i18n.getLocalizedString("i18n.TransfersEur.ManageBeneficiaries");
          this.view.CopyLabel0a8b12f7002084d.toolTip = kony.i18n.getLocalizedString("i18n.TransfersEur.ManageBeneficiaries");
          this.view.CopyLabel0h9e71b8a76ad44.text = kony.i18n.getLocalizedString("i18n.TransfersEur.ManageTransactions");
          this.view.CopyLabel0h9e71b8a76ad44.toolTip = kony.i18n.getLocalizedString("i18n.TransfersEur.ManageTransactions");
          this.view.lblPayMultipleBeneficiaries.text = kony.i18n.getLocalizedString("i18n.Transfers.PayMultipleBeneficiaries");
          this.view.lblPayMultipleBeneficiaries.toolTip = kony.i18n.getLocalizedString("i18n.Transfers.PayMultipleBeneficiaries");
           this.view.flxPayMultipleBeneficiaries.isVisible = true;
        }
    },

    navigateToTransfers: function() {
      this.view.flxContextualMenu.isVisible = false;
      this.view.flxMenu.skin = "slFbox";
      this.view.flxAccounts.skin = "slFbox";
      this.view.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.forceLayout();
    },
    navigatetoAccounts: function() {
      this.view.flxMenu.skin = "slFbox";
      this.view.flxAccounts.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.flxTransfersAndPay.skin = "copyslfbox1";
      this.view.forceLayout();
    },
    setItemSelectListener: function(listener) {
      onItemSelectListener = listener;
    },

    /**
     * Activates the menu
     * @param {string} parentId of parent menu
     * @param {string} childId of parent menu
     */
    activateMenu: function(parentId, childId) {
      if (!this.initialized) {
        this.actiavteWhenInitialized = {
          parentId: parentId,
          childId: childId
        }
        return;
      }
      var parentIndex = 0;
      var menuObject = null;
      var visibleIndex = 1;  //One as offset for sample widget 
      HamburgerConfig.config.forEach(function(menuItem) {
        if (!menuItem.isVisible || menuItem.isVisible()) {
          if (menuItem.id.toLowerCase() === parentId.toLowerCase()) {
            parentIndex = visibleIndex;
            menuObject = menuItem;
          }
          visibleIndex++;
        }
      });
      if (menuObject) {
        this.activeMenu = menuObject.id;
        var childIndex = -1;
        var visibleChildIndex = 1;  //One as offset for sample widget 
        var children = this.view[MENU_CONTAINER].widgets();
        this.collapseAll();
        this.resetSkins();
        this.expandWithoutAnimation(children[parentIndex * 2 + 1]);
        children[parentIndex * 2].widgets()[2].widgets()[0].src = "chevron_up.png";
        children[parentIndex*2].widgets()[2].widgets()[0].accessibilityConfig = {
          "a11yLabel" :"Collapse "+menuObject.id+" Menu"
        };
        if(menuObject.subMenu.children.length === 0){
          children[parentIndex * 2].widgets()[2].widgets()[0].src = "right_arrow.png";
          children[parentIndex * 2].widgets()[2].widgets()[0].width = "11dp";
          children[parentIndex * 2].widgets()[2].widgets()[0].height = "12dp";
        }
        if (childId) {
          var childObject = null;
          menuObject.subMenu.children.forEach(function(childItem) {
            if (!childItem.isVisible || childItem.isVisible()) {
              if (childItem.id.toLowerCase() === childId.toLowerCase()) {
                childIndex = visibleChildIndex;
                childObject = childItem;
              }
              visibleChildIndex++;
            }
          });
          if (childObject) {
            children[parentIndex * 2 + 1].widgets()[childIndex].skin = "skncursor";
            children[parentIndex * 2 + 1].widgets()[childIndex].hoverSkin = "skncursor";
            children[parentIndex * 2 + 1].widgets()[childIndex].widgets()[0].skin = "sknLblHamburgerSelected";
          }
        }
      }

    },

    expandWithoutAnimation: function(widget) {
      widget.height = (widget.widgets().length - 1) * 60;
      this.view.forceLayout();
    },

    resetSkins: function() {
      var subMenus = this.view[MENU_CONTAINER].widgets().filter(function(child, i) {
        return i % 2 !== 0;
      })

      subMenus.forEach(function(subMenu) {
        subMenu.widgets().forEach(function(subMenuItem) {
          subMenuItem.skin = "skncursor";
          subMenuItem.hoverSkin = "sknFlxHoverHamburger";
          subMenuItem.widgets()[0].skin = "sknLblHamburgerUnSelected";
        })
      })

    },

    /**
     * Generate prefix by removing whitespace
     * @param {string} id Id of ite,
     * @returns {string} id with whitespace removed
     */
    getPrefix: function(id) {
      return id.replace(/ /g, '')
    },

    /**
     * Toggles the sub menu
     * @param {kony.ui.FlexContainer} widget Submenu widget to toggle 
     * @param {kony.ui.FlexContainer} imgArrow imgArrow to toggle
     */
    toggle: function (widget, imgArrow) {
      var menuText = widget.id.split("flx")[0];
      if (widget.frame.height > 0) {
        this.collapseAll();
        imgArrow.src = "arrow_down.png";
        imgArrow.toolTip="Expand";
        imgArrow.accessibilityConfig = {
          "a11yLabel" :  "Expand "+menuText+" Menu"
        };
      } else {
        this.collapseAll(this.activeMenu);
        //if(imgArrow.src === "arrow_down.png"){
        this.activeMenu = menuText;
        imgArrow.src = "chevron_up.png";
        imgArrow.toolTip="Collapse";
        imgArrow.accessibilityConfig = {
          "a11yLabel" :  "Collapse "+menuText+" Menu"
        };
        this.view.forceLayout();
        // }
        this.expand(widget);
      }
      this.view.forceLayout();
    },

    /**
     * Collapses the subMenu
     * @param {kony.ui.FlexContainer} widget Submenu widget to expand
     */
    expand: function(widget) {
      var scope = this;
      var animationDefinition = {
        100: {
          "height": (widget.widgets().length -1) * 60
        }
      };
      var animationConfiguration = {
        duration: 0.5,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function() {
          // scope.checkLogoutPosition();
          widget.widgets()[0].setFocus(true);
          scope.view.forceLayout();
        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      widget.animate(animationDef, animationConfiguration, callbacks);
    },

    /**
     * Collapse All the sub menu items
     */
    collapseAll: function (menuText) {
      var self = this;
      var menuItems = this.view[MENU_CONTAINER].widgets();
      menuItems.forEach(function (menuItem, i) {
        if (i % 2 !== 0) {
          self.collapseWithoutAnimation(menuItem);
          var imageWidget = menuItems[i-1].widgets()[2].widgets()[0];
          if(imageWidget.src === "chevron_up.png"){
            imageWidget.src = "arrow_down.png";
            imageWidget.toolTip = "Expand";
            if(menuText !== undefined){
              imageWidget.accessibilityConfig = {
                "a11yLabel" :  "Expand "+menuText+" Menu"
              };
            }
            //                      imageWidget.accessibilityConfig = {
            //                     "a11yLabel" :  "Expand Menu"
            //                 };
          }

        }

      })
      self.view.forceLayout();
    },

    /**
     * Collapse the submenu item
     * @param {kony.ui.FlexContainer} widget submenu flex to collapse
     */
    collapseWithoutAnimation: function(widget) {
      widget.height = 0;
      this.view.forceLayout();
    },

    /**
     * Generates View of the menu dynamically
     */
    generateMenu: function() {
      var self = this;
      HamburgerConfig.config.forEach(function(hamburgerItem) {
        if (!hamburgerItem.isVisible || hamburgerItem.isVisible()) {
          var parentMenuView = self.getParentMenuItemView(hamburgerItem);
          var subMenuView = self.getSubMenuView(hamburgerItem);
           //Added for frame updations on Animation End 
           subMenuView.doLayout = function (widget) {}
          var imgArrow = parentMenuView.widgets()[2].widgets()[0];
          var flxArrow = parentMenuView.widgets()[2];
          if(hamburgerItem.subMenu.children.length === 0){
            flxArrow.isVisible = true;
            imgArrow.isVisible = true;
            imgArrow.src="right_arrow.png";
            imgArrow.accessibilityConfig = {
              "a11yLabel" :"Expand "+hamburgerItem.id+" Menu"
            }
              			imgArrow.width="11dp";
              			imgArrow.height="12dp";
						if (kony.application.getCurrentBreakpoint() == 640) {
                            flxArrow.left = "88%";
                            self.view.flxClose.left = "88%";
                        } else {
                          flxArrow.left = "92%";
                          self.view.flxClose.left = "92%";
                        }
                self.view.forceLayout();
                parentMenuView.onClick = hamburgerItem.onClick;
                }
          else{
          if (kony.application.getCurrentBreakpoint() == 640) {
            flxArrow.left = "88%";
            self.view.flxClose.left = "88%";
          } else {
            flxArrow.left = "92%";
            self.view.flxClose.left = "92%";
            imgArrow.src="arrow_down.png";
            imgArrow.accessibilityConfig = {
              "a11yLabel" :"Expand "+hamburgerItem.id+" Menu"
            }
          }
          self.view.forceLayout();
          parentMenuView.onClick = self.toggle.bind(self, subMenuView, imgArrow);
          }
          	self.view[MENU_CONTAINER].add(parentMenuView, subMenuView);
        }

      });
    },

    /**
     * Generates view of sub menu item
     * @param {object} subMenuItem Sub Menu Item Config
     * @param {string} id Id for prefixing
     * @param {boolean} removeSeperator Removes the seperator 
     * @returns {kony.ui.FlexContainer} Sub menu Item view
     */
    getSubMenuItemView: function(subMenuItem, id, removeSeperator) {
      var subMenuItemView = this.view[SAMPLE_SUB_MENU_ITEM].clone(id);
      subMenuItemView.widgets()[0].text = kony.i18n.getLocalizedString(subMenuItem.text);
      subMenuItemView.widgets()[0].toolTip = kony.i18n.getLocalizedString(subMenuItem.toolTip);
      if (removeSeperator) {
        subMenuItemView.removeAt(1);
      }
      subMenuItemView.onClick = this.bindAction(subMenuItem.onClick)
      return subMenuItemView;
    },

    /**
     * Bind onclick to hamburger item
     * @param {function} originalOnclick Original OnClick from config
     * @returns {function} Composed function
     */
    bindAction: function(originalOnclick) {
      return function() {
        if (onItemSelectListener) {
          onItemSelectListener();
        }
        originalOnclick();
      }
    },

    /**
     * Generates the view of sub menu
     * @param {object} hamburgerItem config of item
     * @returns {kony.ui.FlexContainer} returns the view of submenu
     */
    getSubMenuView: function(hamburgerItem) {
      var self = this;
      var subMenuView = this.view[SAMPLE_SUB_MENU].clone(this.getPrefix(hamburgerItem.id));
      hamburgerItem.subMenu.children.forEach(function(subMenuItem, index) {
        if (!subMenuItem.isVisible || subMenuItem.isVisible.call(HamburgerConfig)) {
          var subMenuItemView = self.getSubMenuItemView(subMenuItem, self.getPrefix(hamburgerItem.id) + index, index !== hamburgerItem.subMenu.children.length - 1);
          subMenuItemView.isVisible = true;
          subMenuView.add(subMenuItemView);
        }

      })
      // Hide Sample Widget
      subMenuView.widgets()[0].isVisible = false;
      subMenuView.isVisible = true;
      return subMenuView;
    },

    /**
     * Generate View for Parent menu Item
     * @param {string} hamburgerItem Title of Parent Menu Item
     * @returns {kony.ui.FlexContainer} Returns the flex container object
     */
    getParentMenuItemView: function(hamburgerItem) {
      var parentMenuFlex = this.view[SAMPLE_MENU].clone(this.getPrefix(hamburgerItem.id));
      var childWidgets = parentMenuFlex.widgets();
       if (typeof hamburgerItem.icon === "function") {
                childWidgets[0].text = hamburgerItem.icon();              
            }
            else {
                childWidgets[0].text = hamburgerItem.icon;
            }
            if (typeof hamburgerItem.text === "function") {
                childWidgets[1].text = kony.i18n.getLocalizedString(hamburgerItem.text());
                childWidgets[0].accessibilityConfig = {
                    "a11yLabel" : kony.i18n.getLocalizedString(hamburgerItem.text()) + " Menu"
                } 
            }
            else {
                childWidgets[1].text = kony.i18n.getLocalizedString(hamburgerItem.text);
                childWidgets[0].accessibilityConfig = {
                    "a11yLabel" : kony.i18n.getLocalizedString(hamburgerItem.text) + " Menu"
                } 
            }
            if (typeof hamburgerItem.toolTip === "function") {
                childWidgets[1].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip());
                childWidgets[0].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip());

            }
            else {
                childWidgets[1].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip);
                childWidgets[0].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip);

            }
            if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
              childWidgets[3].left = "0%";
              childWidgets[3].width = "100%";
          }
      if (hamburgerItem.id == "TRANSFERS") {
        childWidgets[0].skin = "sknLblFontType0273E330px";
      }
      if (hamburgerItem.id == "About Us") {
        childWidgets[0].skin = "sknLblFontType0273E320px";
      }
      parentMenuFlex.isVisible = true;      
      return parentMenuFlex;
    },

    setCustomHeaderLogo: function () {
      var configurationManager = applicationManager.getConfigurationManager();
      if (configurationManager.isSMEUser === "true") this.view.imgKony.src = "sbb.png";
      else if (configurationManager.isRBUser === "true") this.view.imgKony.src = "kony_logo.png";
      else if (configurationManager.isMBBUser === "true") this.view.imgKony.src = "mbb.png";
      else this.view.imgKony.src = "kony_logo_white.png";
    },

    /**
     * For Change Language Support
     */
    forceInitializeHamburger: function() {
      this.generateMenu();
      this.initialized = true;
    }
  };
});