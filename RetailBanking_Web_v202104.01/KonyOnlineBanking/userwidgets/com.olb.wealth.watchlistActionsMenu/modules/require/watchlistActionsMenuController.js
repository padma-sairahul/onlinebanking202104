define(['ViewConstants','CommonUtilities'], function(ViewConstants,CommonUtilities) {

	return {
        init: function(){
           this.view.segWatchlistActionsMenu.onRowClick = this.getWatchlistActionLocation.bind(this);
        },
		getWatchlistActionLocation: function() {
            var selectedRow = this.view.segWatchlistActionsMenu.selectedRowItems[0];
          
			var lblTransferCashText = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.wealth.TransferCash"));
            var lblConvertCurrencyText = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.wealth.ConvertCurrency"));

            if (selectedRow.lblAction.toLowerCase() === lblTransferCashText.toLowerCase()) {
				this.navigateToTransfer();
            }
            else if (selectedRow.lblAction.toLowerCase() === lblConvertCurrencyText.toLowerCase()) {
				this.navigateToConvertCurrency();
            }
        },
        navigateToTransfer: function() {
            var configurationManager = applicationManager.getConfigurationManager();
            if (configurationManager.getDeploymentGeography() === "EUROPE") {
                applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({
                    context: "MakePaymentOwnAccounts",
                    isTransferCashWealth: true
                });
            } else {
                applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen({
                    isTransferCashWealth: true
                });
            }
        },
        navigateToConvertCurrency: function() {
            scope_WealthPresentationController.instrumentAction = "";
            var navManager = applicationManager.getNavigationManager();
            navManager.setCustomInfo("frmCurrencyConverter", portfolioId);
            navManager.navigateTo("frmCurrencyConverter");
        }
	};
});