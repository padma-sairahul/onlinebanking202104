define(function () {

  function UnifiedTransferDAO(){
    this.client = kony.sdk.getCurrentInstance();
  }

  UnifiedTransferDAO.prototype.fetchFromAccounts = function(objServiceName,objName,operationName,criteria,onSuccess,onError){
    kony.application.showLoadingScreen();
    var objSvc = this.client.getObjectService(objServiceName, {
      "access": "online"
    });
    var dataObject = new kony.sdk.dto.DataObject(objName);
    for(var key in criteria){
      dataObject.addField(key,criteria[key]);
    }
    var options1 = {
      "dataObject": dataObject
    };
    objSvc.customVerb(operationName, options1,
                      function(response) {
      onSuccess(response);
      kony.print("Fetch Performed Successfully: " + JSON.stringify(response));
    },
                      function(error) {

      var errObj = {
        "errorInfo" : "Error in fetching from accounts.",
        "error": error
      };
      onError(errObj);

    });
  },

    UnifiedTransferDAO.prototype.fetchToAccounts = function(objServiceName,objName,operationName,criteria,onSuccess,onError){
    kony.application.showLoadingScreen();
    var objSvc = this.client.getObjectService(objServiceName, {
      "access": "online"
    });
    var dataObject = new kony.sdk.dto.DataObject(objName);
    for(var key in criteria){
      dataObject.addField(key,criteria[key]);
    }
    var options1 = {
      "dataObject": dataObject
    };
    objSvc.customVerb(operationName, options1,
                      function(response) {
      onSuccess(response);
      kony.print("Fetch To Account Performed Successfully: " + JSON.stringify(response));
    },
                      function(error) {

      var errObj = {
        "errorInfo" : "Error in fetching to accounts.",
        "error": error
      };
      onError(errObj);

    });
  },
    UnifiedTransferDAO.prototype.validateIBAN = function(objServiceName,objName,operationName,criteria,onSuccess,onError){
    kony.application.showLoadingScreen();
    var objSvc = this.client.getObjectService(objServiceName, {
      "access": "online"
    });
    var dataObject = new kony.sdk.dto.DataObject(objName);
    for(var key in criteria){
      dataObject.addField(key,criteria[key]);
    }
    var options1 = {
      "dataObject": dataObject
    };
    objSvc.customVerb(operationName, options1,
                      function(response) {
      onSuccess(response);
      kony.print("IBAN Validated Successfully: " + JSON.stringify(response));
    },
                      function(error) {

      var errObj = {
        "errorInfo" : "Error in validating IBAN.",
        "error": error
      };
      onError(errObj);

    });
  },
      UnifiedTransferDAO.prototype.getBankDate = function(objServiceName,objName,operationName,criteria,onSuccess,unicode,onError){
    kony.application.showLoadingScreen();
    var objSvc = this.client.getObjectService(objServiceName, {
      "access": "online"
    });
    var dataObject = new kony.sdk.dto.DataObject(objName);
    for(var key in criteria){
      dataObject.addField(key,criteria[key]);
    }
    var options1 = {
      "dataObject": dataObject
    };
    objSvc.customVerb(operationName, options1,
                      function(response) {
      onSuccess(response,unicode);
      kony.print("Bank Date Fetch Performed Successfully: " + JSON.stringify(response));
    },
                      function(error) {

      var errObj = {
        "errorInfo" : "Error in fetchin Bank Date.",
        "error": error
      };
      onError(errObj);

    });
  },
    UnifiedTransferDAO.prototype.getBeneficiaryName = function(objServiceName,objName,operationName,criteria,onSuccess,onError){
    kony.application.showLoadingScreen();
    var objSvc = this.client.getObjectService(objServiceName, {
      "access": "online"
    });
    var dataObject = new kony.sdk.dto.DataObject(objName);
    for(var key in criteria){
      dataObject.addField(key,criteria[key]);
    }
    var options1 = {
      "dataObject": dataObject
    };
    objSvc.customVerb(operationName, options1,
                      function(response) {
      onSuccess(response);
      kony.print("getBeneficiaryName Successfully validated: " + JSON.stringify(response));
    },
                      function(error) {

      var errObj = {
        "errorInfo" : "Error in validating getBeneficiaryName.",
        "error": error
      };
      onError(errObj);

    });
  },
    /**
     * @api : createPayee
     * @description :  method to invoke save this Payee service
     * @param : objServiceName{String} -object service name
     * @param : objName{String} -object name
     * @param : operationname{String} -operation name
     * @param : criteria{JSON} -criteria
     * @param : onSuccess{function} -function to be invoekd on success
     * @param : unicode{String} -service response
     */
  UnifiedTransferDAO.prototype.validateData = function(objServiceName,objName,operationName,criteria,onSuccess,unicode,onError) {
    kony.application.showLoadingScreen();
    var objSvc = this.client.getObjectService(objServiceName, {
      "access": "online"
    });
    var dataObject = new kony.sdk.dto.DataObject(objName);
    for(var key in criteria){
      dataObject.addField(key,criteria[key]);
    }
    var options = {
      "dataObject": dataObject
    };
    objSvc.customVerb(operationName, options,function(response) {
      onSuccess(response,unicode);
      kony.print("Fetch Performed Successfully: " + JSON.stringify(response));
      kony.application.dismissLoadingScreen();
    },
     function(error) {
     var errObj = {
            "errorInfo" : "Error in the validate data method of the component.",
            "error": error
          };
          onError(errObj);
     });
  },
      UnifiedTransferDAO.prototype.getCreditCardAccounts = function(objServiceName,objName,operationName,criteria,onSuccess,onError){
    kony.application.showLoadingScreen();
    var objSvc = this.client.getObjectService(objServiceName, {
      "access": "online"
    });
    var dataObject = new kony.sdk.dto.DataObject(objName);
    for(var key in criteria){
      dataObject.addField(key,criteria[key]);
    }
    var options1 = {
      "dataObject": dataObject
    };
    objSvc.customVerb(operationName, options1,
                      function(response) {
      onSuccess(response);
      kony.print("getCreditCardAccounts Successfully validated: " + JSON.stringify(response));
											  
    },
                      function(error) {

      var errObj = {
        "errorInfo" : "Error in getCreditCardAccounts api call",
        "error": error
      };
      onError(errObj);

    });
  },   
  UnifiedTransferDAO.prototype.getAccountDetails = function(objServiceName,objName,operationName,criteria,onSuccess,onError){
    kony.application.showLoadingScreen();
    var objSvc = this.client.getObjectService(objServiceName, {
      "access": "online"
    });
    var dataObject = new kony.sdk.dto.DataObject(objName);
    for(var key in criteria){
      dataObject.addField(key,criteria[key]);
    }
    var options1 = {
      "dataObject": dataObject
    };
    objSvc.customVerb(operationName, options1,
                      function(response) {
      onSuccess(response);
      kony.print("getAccountDetails Successfully validated: " + JSON.stringify(response));

    },
                      function(error) {

      var errObj = {
        "errorInfo" : "Error in getAccountDetails api call",
        "error": error
      };
      onError(errObj);

    });
  },
       UnifiedTransferDAO.prototype.getSwiftCode = function(objServiceName,objName,operationName,criteria,onSuccess,onError){
    kony.application.showLoadingScreen();
    var objSvc = this.client.getObjectService(objServiceName, {
      "access": "online"
    });
    var dataObject = new kony.sdk.dto.DataObject(objName);
    for(var key in criteria){
      dataObject.addField(key,criteria[key]);
    }
    var options1 = {
      "dataObject": dataObject
    };
    objSvc.customVerb(operationName, options1,
                      function(response) {
      onSuccess(response);
      kony.print(" Swift Code fetched Successfully : " + JSON.stringify(response));										  
    },
                      function(error) {

      onError(error);

    });
  }
 return UnifiedTransferDAO;
});