define([],function() {

  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {
      this.scopes ={
        "LOGIN" : "LOGIN",
        "ACTIVATION" : "ACTIVATION",
        "IDV" : "IDV",
        "KYC" : "KYC",
        "CHANGEPIN"  : "CHANGEPIN"
      };
      this.authenticationJSON = {
        "Meta": {
          "EventType": "urn:com:temenos:security:event:login:v1",
          "RiskScore": ""
        },
        "urn:com:temenos:security:event:login:v1": {
          "Scope": "",
          "Name": "EuroBank Internet Banking",
          "UserName": "",
          "Password": "",
          "authType":"",
          "isMfa": "false"
        }
      };
      this._identityServiceName = "";
      this._riskScore = "";
      this._labelSkin = "";
      this._flxSkins = "";
      this._primaryBtnEnableSkin = {};
      this._primaryBtnDisableSkin = {};
      this._username = "";
      this._breakpoints = "";
      this._lblWelcomeMobile = "";
      this._rtxWithUsername = "";
      this._lblVerifiedUser = "";
      
    },
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {
      defineSetter(this, "identityServiceName", function(val) {
        if((typeof val === 'string') && (val !== "")){
          this._identityServiceName=val;
        }
      });
      defineGetter(this, "identityServiceName", function() {
        return this._identityServiceName;
      });
      defineSetter(this, "riskScore", function(val) {
        if((typeof val === 'string') && (val !== "")){
          this._riskScore=val;
        }
      });
      defineGetter(this, "riskScore", function() {
        return this._riskScore;
      });
      defineSetter(this, "labelSkin", function(val) {
        if((typeof val === 'string') && (val !== "")){
          this._labelSkin=val;
        }
      });
      defineGetter(this, "labelSkin", function() {
        return this._labelSkin;
      });
      defineSetter(this, "flxSkins", function(val) {
        if((typeof val === 'string') && (val !== "")){
          this._flxSkins=val;
        }
      });
      defineGetter(this, "flxSkins", function() {
        return this._flxSkins;
      });
      defineSetter(this, "primaryBtnEnableSkin", function(val) {
        if((typeof val === 'string') && (val !== "")){
          this._primaryBtnEnableSkin=val;
        }
      });
      defineGetter(this, "primaryBtnEnableSkin", function() {
        return this._primaryBtnEnableSkin;
      });
      defineSetter(this, "primaryBtnDisableSkin", function(val) {
        if((typeof val === 'string') && (val !== "")){
          this._primaryBtnDisableSkin=val;
        }
      });
      defineGetter(this, "primaryBtnDisableSkin", function() {
        return this._primaryBtnDisableSkin;
      });
      defineSetter(this, "username", function(val) {
        if((typeof val === 'string') && (val !== "")){
          this._username=val;
        }
      });
      defineGetter(this, "username", function() {
        return this._username;
      });
      defineSetter(this, "breakpoints", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._breakpoints=val;
        }
      });
      defineGetter(this, "breakpoints", function() {
        return this._breakpoints;
      });
      defineSetter(this, "lblWelcomeMobile", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._lblWelcomeMobile=val;
        }
      });
      defineGetter(this, "lblWelcomeMobile", function() {
        return this._lblWelcomeMobile;
      });
      defineSetter(this, "rtxWithUsername", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._rtxWithUsername=val;
        }
      });
      defineGetter(this, "rtxWithUsername", function() {
        return this._rtxWithUsername;
      });
      defineSetter(this, "lblVerifiedUser", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._lblVerifiedUser=val;
        }
      });
      defineGetter(this, "lblVerifiedUser", function() {
        return this._lblVerifiedUser;
      });
    },

    setContext: function(context) {
      this.setRiskscore(context.riskScore);
    },

    setRiskscore: function(value){
      if(value){
        this._riskScore = value;
      }
    },

    preshow : function(){
      let self = this;
      self.setFlowActions();
      let OLBLogoutStatus = kony.store.getItem('OLBLogoutStatus');
      if (OLBLogoutStatus || kony.store.getItem('UserLoginStatus')) {
        self.view.tbxUserName.text = OLBLogoutStatus ? OLBLogoutStatus.userName : "";
        kony.store.removeItem('OLBLogoutStatus');
        kony.store.setItem('UserLoginStatus', false);
      } 
      // After Login error from forgot password flow we need to persist the user name
      if(self.view.flxLoginUser.isVisible){
        self.view.tbxUserName.text = self.view.lblVerifiedUser.text;
      }
      self.restoreOriginalMainLoginUIChanges(self.view.tbxUserName.text);
    },

    setFlowActions : function(){
      let self = this;

      self.view.btnLogin.onClick = function(){
        self.onLoginClick();
      };

      self.view.tbxUserName.onKeyUp = function(){
        self.enableLogin();
        self.checkifUserNameContainsMaskCharacter();
        self.view.lblUsernameCapsLocIndicator.setVisibility(kony.application.getCurrentBreakpoint() > 1024 && event.getModifierState && event.getModifierState("CapsLock"));
      };

      self.view.tbxUserName.onTouchStart = function(){
        self.setFocusSkin(self.view.flxUserName);
        self.view.lblUsernameCapsLocIndicator.setVisibility(kony.application.getCurrentBreakpoint() > 1024 && event.getModifierState && event.getModifierState("CapsLock"));
        if (self.view.tbxUserName.text.trim().length === 0) {
          self.showUserNames();
        }
      };

      self.view.tbxUserName.onEndEditing = function(){
        self.hideUserNames();
        self.setNormalSkin(self.view.flxUserName);
        self.view.lblUsernameCapsLocIndicator.setVisibility(false);
      };

      self.view.tbxPassword.onKeyUp = function(){
        self.enableLogin();
        self.view.lblPasswordCapsLocIndicator.setVisibility(kony.application.getCurrentBreakpoint() > 1024 && event.getModifierState && event.getModifierState("CapsLock"));
      };

      self.view.tbxPassword.onTouchStart = function(){
        self.setFocusSkin(self.view.flxPassword);
        self.view.lblPasswordCapsLocIndicator.setVisibility(kony.application.getCurrentBreakpoint() > 1024 && event.getModifierState && event.getModifierState("CapsLock"));
      };

      self.view.tbxPassword.onEndEditing = function(){
        self.setNormalSkin(self.view.flxPassword);
        self.view.lblPasswordCapsLocIndicator.setVisibility(false);
      };

      self.view.tbxPassword.onDone = function(){
        self.enableLogin();
        if(self.view.btnLogin.enable){
          self.onLoginClick();
        }
      };

      self.view.imgViewPassword.onTouchStart = function(){
        let isSecuredText = self.view.tbxPassword.secureTextEntry;
        self.view.tbxPassword.secureTextEntry = !isSecuredText;
      };

      self.view.segUsers.onRowClick = function(){
        var val = self.view.segUsers.selectedRowItems[0].lblusers;
        self.view.tbxUserName.text = val;
        self.view.flxUserDropdown.isVisible = false;
        self.enableLogin();
      };

    },

    resetUI: function(){
      let self = this;
      self.view.rtxErrorMsg.isVisible = false;
      self.view.flxUserName.setVisibility(true);
      self.view.lblUsername.setVisibility(true);
      self.view.rtxErrorMsgUser.isVisible = false;
      self.view.tbxUserName.text = "";
      self.view.tbxPassword.text = "";
      self.view.tbxPassword.secureTextEntry = true;
      self.view.tbxUserName.text = self.username ? self.username : self.view.tbxUserName.text;
    },

    setFocusSkin: function (flexWidget) {
      flexWidget.skin = JSON.parse(this._flxSkins).focusSkin;
    },

    setNormalSkin: function (flexWidget) {
      flexWidget.skin = JSON.parse(this._flxSkins).normalSkin;
    },

    setErrorSkin: function (flexWidget) {
      flexWidget.skin = JSON.parse(this._flxSkins).errorSkin;
    },

    isUserNameMasked : function(userName) {
      var userNameText = userName.substring(3, userName.length - 2);
      for (var i = 0; i < userNameText.length; i++) {
        if (userNameText.charAt(i) !== '*') {
          return false;
        }
      }
      return true;
    },

    getUnMaskedUserName: function (enteredUserName) {
      var names = JSON.parse(applicationManager.getStorageManager().getStoredItem("olbNames")) || [];
      var maskedUserValues = [];
      var unmaskedUserValues = [];
      maskedUserValues = names.map(function (nameObj) {
        unmaskedUserValues.push(Object.keys(nameObj)[0]);
        return nameObj[Object.keys(nameObj)[0]];
      });
      var index = maskedUserValues.indexOf(enteredUserName);
      if (index >= 0) {
        return unmaskedUserValues[index];
      }
      return null;
    },

    enableLogin: function () {
      let self = this;
      let username = self.view.tbxUserName.text;
      let password = self.view.tbxPassword.text;
      let isEnabled = (username && password) ? true : false ;
      self.view.btnLogin.setEnabled(isEnabled);
      let skins = isEnabled ? JSON.parse(self.primaryBtnEnableSkin) : JSON.parse(self.primaryBtnDisableSkin) ;
      self.view.btnLogin.skin = skins.normal;
      self.view.btnLogin.hoverSkin = skins.hoverSkin;
      self.view.btnLogin.focusSkin = skins.focusSkin;
    },

    onLoginClick : function(){
      let self = this;
      self.view.rtxErrorMsg.setVisibility(false);
      let status = self.view.rememberMe.isRememberMe();
      var enteredUserName = self.view.tbxUserName.text.trim();
      let isUsernameMasked = self.isUserNameMasked(enteredUserName);
      let usernamePasswordJSON = {
        "UserName": isUsernameMasked ? self.getUnMaskedUserName(enteredUserName) : enteredUserName,
        "Password": self.view.tbxPassword.text,
        "rememberMe": status
      };
      self.username = usernamePasswordJSON.UserName;
      // self.authenticationJSON["urn:com:temenos:security:event:login:v1"].UserName = usernamePasswordJSON.UserName;
      // self.authenticationJSON["urn:com:temenos:security:event:login:v1"].Password = usernamePasswordJSON.Password;
      // self.authenticationJSON["urn:com:temenos:security:event:login:v1"].Scope = "LOGIN"; 
      // self.authenticationJSON.Meta.RiskScore = self._riskScore;
      kony.application.showLoadingScreen();
      kony.mvc.MDAApplication.getSharedInstance().appContext.rememberMeStatus = status;
      let authClient = KNYMobileFabric.getIdentityService(self.identityServiceName);
      authClient.login(usernamePasswordJSON, self.onLoginSuccess, self.onLoginFailure);
    },

    onLoginSuccess: function(response){
      let self = this;
      let params = kony.sdk.getCurrentInstance().tokens[self.identityServiceName].provider_token.params;
      if(params && params.is_mfa_enabled) {
        self.MFANavigation(params.mfa_meta);
      } else {
        response = {"username":self.username,"rememberMe":kony.mvc.MDAApplication.getSharedInstance().appContext.rememberMeStatus};
        self.onSuccessCallback(response);
      }
    },

    onLoginFailure: function(response){
      let self = this;
      kony.application.dismissLoadingScreen();
      if (kony.mvc.MDAApplication.getSharedInstance().appContext.isCSR_Assist_Mode && self.showServerErrorPage) {
          self.showServerErrorPage();
      } else {
        let errorMsg = (response.details && response.details.errmsg)  ? response.details.errmsg : ((response.errmsg && response.errmsg.errorMessage) ? response.errmsg.errorMessage: "User does not exist.");
        self.showLoginError(errorMsg);
      }
    },

    showLoginError: function(errorMsg){
      let self = this;
      self.view.rtxErrorMsg.text = errorMsg;
      self.view.rtxErrorMsg.setVisibility(true);
      self.view.tbxPassword.text = "";
      self.view.tbxPassword.secureTextEntry = true;
      self.view.setVisibility(true);
      self.setErrorSkin(self.view.flxUserName);
      self.setErrorSkin(self.view.flxPassword);
      //self.enableLogin();
      this.view.btnLogin.skin = "sknBtnBlockedSSPFFFFFF15Px";
      if(self.forceLayout){
        self.forceLayout();
      }
    },

    onBreakpointChange : function(){
      let self = this;
      let breakpoint = self.breakPointParser();
      self.setCSS(breakpoint, "lblWelcomeMobile", self.view.lblWelcomeMobile);
      self.setCSS(breakpoint, "rtxWithUsername", self.view.rtxWithUsername);
      self.setCSS(breakpoint, "lblVerifiedUser", self.view.lblVerifiedUser);
      self.setSkins(breakpoint);
      self.view.forceLayout();
    },
    
    setCSS: function(breakpoint, property, widget){
      let self = this;
      let css = JSON.parse(self[property])[breakpoint];
      for(var key in css){
        if(css[key]){
          widget[key] = css[key];
        }
      }
    },

    setSkins: function(breakpoint){
      let self = this;
      let labelSkin = JSON.parse(self.labelSkin)[breakpoint].skin;
      self.view.lblUsername.skin = labelSkin;
      self.view.lblPassword.skin = labelSkin;
    },
    
    breakPointParser:function(){
      let self = this;
      let currentBreakPoint = kony.application.getCurrentBreakpoint();
      let avaialbleBreakpoints = JSON.parse(self.breakpoints);
      for(var key in avaialbleBreakpoints){
        if(avaialbleBreakpoints[key] == currentBreakPoint){
          return avaialbleBreakpoints[key];
        }
      }
      return "default";
    },

    hideUserNames: function () {
      var self = this;
      self.enableLogin();
      if (self.view.tbxUserName.text.trim()) {
        self.view.flxUserDropdown.setVisibility(false);
      }
    },

    removeSpecialCharacter: function () {
      var str = this.view.tbxUserName.text;
      if (str) {
        if (str.charAt(str.length - 1) === '*') {
          this.view.tbxUserName.text = str.substring(0, str.length - 1);
        }
      }
    },

    checkifUserNameContainsMaskCharacter: function () {
      var self = this;
      self.removeSpecialCharacter();
      if (self.view.tbxUserName.text.trim()) {
        self.view.flxUserDropdown.setVisibility(false);
      } else {
        self.showUserNames();
      }
      self.enableLogin();
    },

    /**
     * This  function checks whether there are any usernames saved or not..if there are no usernames then it hides the dropdown or shows the masked usernames iin the dropdown
     */
    showUserNames: function () {
      var storagaManager = applicationManager.getStorageManager();
      var savedUserNames = JSON.parse(storagaManager.getStoredItem("olbNames"));
      if (savedUserNames === null || savedUserNames.length === 0) {
        this.view.flxUserDropdown.isVisible = false;
      } else {
        this.showUserNameSuggestions(JSON.parse(storagaManager.getStoredItem("olbNames")));
      }
    },

    /**
     * This  function shows the list of the usernames stored in the local Storage
     * @param {string} enteredText text entered by user in user name field
    */
    showUserNameSuggestions: function (enteredText) {
      if (enteredText !== null && enteredText !== undefined) {
        this.view.segUsers.removeAll();
        var x = [];
        for (var index in enteredText) {
          for (var attr in enteredText[index]) {
            var y = {
              "lblusers": enteredText[index][attr]
            };
            x.push(y);
          }
        }
        this.view.segUsers.addAll(x);
      }
      this.view.flxUserDropdown.setVisibility(true);
      this.view.flxUserDropdown.parent.forceLayout();
    },

    /**
     * This function checks if the user already present in local Storage or not
     * @param {string} username - user name entered by user
     * @returns {boolean}  true --if the username is present ,false if the username is not present
      */
     chkUserInLocal: function (username) {
      let self = this;
      var usernames = applicationManager.getStorageManager().getStoredItem('olbNames');
      if (usernames !== null && usernames !== '') {
        usernames = JSON.parse(usernames);
//         for (var index in usernames) {
//           if (self.getUnMaskedUserName(usernames[index][username]) === username) {
//             return true;
//           }
//         }
        for( var i = 0; i < usernames.length; i++ ) {
           var obj = usernames[i];
           for (var user in obj) {
               if(user.toLowerCase()===username){
                  return usernames[i][user];
               }
           }
       }
      }
      return false;
    },

    maskUserName: function (userName) {
      var maskedUserName = '';
      var firstThree = userName.substring(0, 3);
      var lastTwo = userName.substring(userName.length - 2, userName.length);
      var xLength = userName.length - 5;
      var maskString = '';
      for (var i = 0; i < xLength; i++) {
        maskString = maskString + '*';
      }
      maskedUserName = firstThree + maskString + lastTwo;
      return maskedUserName;
    },
    /**
    * This UI function is for recovered Username UI Changes after user verified
    */
    recoveredUsernameUIChanges: function(username){
      let self = this;
      self.view.rtxErrorMsg.isVisible = false;
      self.view.flxLoginUser.isVisible = true;
      self.view.rtxWithUsername.skin = "sknSSPLight42424218Px";
      self.view.rtxWithUsername.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
      self.view.lblVerifiedUser.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
      self.view.lblVerifiedUser.text = username;
      self.view.flxUserName.setVisibility(false);
      self.view.lblUsername.setVisibility(false);
      self.view.flxUserDropdown.setVisibility(false);
      self.view.tbxUserName.text = username;
      self.view.tbxPassword.text = "";
      self.view.tbxPassword.secureTextEntry = true;
      self.enableLogin();
    },

    restoreOriginalMainLoginUIChanges: function(userName){
      let self = this;
      self.view.flxLoginUser.setVisibility(false);
      if (self.chkUserInLocal(userName)) {
       // self.view.tbxUserName.text = self.maskUserName(userName);
	      self.view.tbxUserName.text = self.chkUserInLocal(userName);
      } else {
        self.view.tbxUserName.text = "";
      }
      self.view.tbxPassword.text = "";
      self.view.rtxErrorMsgUser.isVisible = false;
      self.view.tbxPassword.secureTextEntry = true;
      self.enableLogin();
    },

  };
});