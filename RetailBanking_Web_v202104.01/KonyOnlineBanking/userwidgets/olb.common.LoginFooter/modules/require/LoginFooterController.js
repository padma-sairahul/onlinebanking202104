define(function () {
  return {
    onBreakpointChangeComponent: function (breakpoint) {
      kony.print("footer breakpoint change called, breakpoint : " + breakpoint);
      if (breakpoint === 640) {
        this.showMobileView();
      } else if (breakpoint === 1024) {
        this.showTabletView();
      } else {
        this.showDesktopView();
      }
    },
    showDesktopView: function () {
      this.toggleButtonView(true);
      // this.view.flxFooterButtons.left = "60dp";
      this.view.flxFooterButtons.width = "";
      this.view.flxFooterButtons.centerX = "";
      this.view.rtxCopyRight.left = "60dp";
      this.view.rtxCopyRight.right = "10dp";
      this.view.rtxCopyRight.centerX = "";
      this.view.btnLocateUs.left = "0dp";
    },
    showMobileView: function () {
      this.toggleButtonView(false);
      this.view.flxFooterButtons.width = "360dp";
      this.view.flxFooterButtons.centerX = "50%";
      this.view.rtxCopyRight.maxWidth = "100%";
      this.view.rtxCopyRight.centerX = "50%";
      this.view.rtxCopyRight.left = "";
      this.view.rtxCopyRight.right = "";

      var btnsWidth = this.view.btnContactUs.frame.width + this.view.btnContactUs.frame.x - this.view.btnLocateUs.frame.x;
      var parentWidth = 360; //this.view.flxFooterButtons.width is the parent width which we have set to 360dp;
      diffOfCenters = (parentWidth - btnsWidth) / 2;
      this.view.btnLocateUs.left = Math.round(diffOfCenters) + "dp";
    },
    showTabletView: function () {
      this.toggleButtonView(true);
      this.view.flxFooterButtons.width = "640dp";
      this.view.flxFooterButtons.centerX = "50%";
      this.view.rtxCopyRight.maxWidth = "100%";
      this.view.rtxCopyRight.centerX = "50%";
      this.view.rtxCopyRight.left = "";
      this.view.rtxCopyRight.right = "";

      var btnsWidth = this.view.btnFAQ.frame.width + this.view.btnFAQ.frame.x - this.view.btnLocateUs.frame.x;
      var parentWidth = 640; //this.view.flxFooterButtons.width is the parent width which we have set to 640dp;
      diffOfCenters = (parentWidth - btnsWidth) / 2;
      this.view.btnLocateUs.left = Math.round(diffOfCenters) + "dp";
    },
    toggleButtonView: function (isVisible) {
      this.view.flxSeparator2.isVisible = isVisible;
      this.view.flxSeparator3.isVisible = isVisible;
      this.view.flxSeparator4.isVisible = isVisible;
      this.view.btnPrivacyPolicy.isVisible = isVisible;
      this.view.btnTandC.isVisible = isVisible;
      this.view.btnFAQ.isVisible = isVisible;
    }
  };
});