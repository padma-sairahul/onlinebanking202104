define(['./ParserUtilsManager','FormatUtil'],function(ParserUtilsManager,FormatUtil) {

  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {
	  this.segmentData="";
      this.recordsArray=[];
      this.allRecordsSegData=[];
      this.searchApplied=false;
      this.parserUtilsManager = new ParserUtilsManager();
      this.FormatUtils = new FormatUtil();
      this.contextSet = false;
      this.Icons = {
        "clearText" : "",
        "dropdownExpand" : "",
		"dropdownCollapse" : "",
        "CITI_BANK_IMAGE" : "",
        "CHASE_BANK_IMAGE" : "",
        "BOA_BANK_IMAGE" : "",
        "HDFC_BANK_IMAGE" : "",
        "INFINITY_BANK_IMAGE" : "",
        "EXTERNAL_BANK_IMAGE" : ""
      };

 
      this._groupIdentifier="";

      //declaration for List Object in the group:Context
      this._listObject="";

      this._BREAKPTS="";

      this._iconDropdownExpand="";
	  
	  this._iconDropdownCollapse="";

      this._sknTxtBoxFocusSkin="";

      this._sknEmptyRecordLabel="";

      this._sknEmptyRecordActionLabel="";

      //declaration for Text Box in the group:Context
      this._txtBox="";

      //declaration for Type Lists to Filter in the group:Context
      this._filterTypeList="";

      //declaration for Dropdown List Field1 in the group:Context
      this._dropdownListField1="";

      //declaration for Dropdown List Field2 in the group:Context
      this._dropdownListField2="";

      //declaration for Dropdown List Field3 in the group:Context
      this._dropdownListField3="";

      //declaration for Empty Record Message in the group:Context
      this._emptyRecordMessage="";

      //declaration for Empty Recor Action Item Text in the group:Context
      this._emptyRecordActionTxt="";

      //declaration for Text Box Value Skin in the group:Context
      this._sknTxtBoxValue="";

      //declaration for Text Box Placeholder Skin in the group:Context
      this._sknTxtBoxPlaceholder="";

      //declaration for Dropdown List Field1 Skin in the group:Context
      this._sknListField1="";

      //declaration for Dropdown List Field2 Skin in the group:Context
      this._sknListField2="";

      //declaration for Dropdown List Field3 Skin in the group:Context
      this._sknListField3="";

      //declaration for List Field Type Skin in the group:Context
      this._sknListFieldType="";

      //declaration for Clear Search Filter Icon in the group:Context
      this._iconClearFilter="";

      //declaration for City Bank Icon in the group:Context
      this._iconCityBank="";

      //declaration for Chase Bank Icon in the group:Context
      this._iconChaseBank="";

      //declaration for BAO Bank Icon in the group:Context
      this._iconBAOBank="";

      //declaration for HDFC Bank Icon in the group:Context
      this._iconHDFCBank="";

      //declaration for Infinity Bank Icon in the group:Context
      this._iconInfinityBank="";

      //declaration for External Bank Icon in the group:Context
      this._iconExternalBank="";
    },
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {

      defineSetter(this, "groupIdentifier", function(val) {
        if((typeof val=='string') && (val != "")){
          this._groupIdentifier=val;
        }
      });

      //getter method for List Object in the group:Context
      defineGetter(this, "groupIdentifier", function() {
        return this._groupIdentifier;
      });


      defineSetter(this, "sknEmptyRecordLabel", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknEmptyRecordLabel=val;
        }
      });

      //getter method for List Object in the group:Context
      defineGetter(this, "sknEmptyRecordLabel", function() {
        return this._sknEmptyRecordLabel;
      });

      defineSetter(this, "sknEmptyRecordActionLabel", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknEmptyRecordActionLabel=val;
        }
      });

      //getter method for List Object in the group:Context
      defineGetter(this, "sknEmptyRecordActionLabel", function() {
        return this._sknEmptyRecordActionLabel;
      });


      defineSetter(this, "sknTxtBoxFocusSkin", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknTxtBoxFocusSkin=val;
        }
      });

      //getter method for List Object in the group:Context
      defineGetter(this, "sknTxtBoxFocusSkin", function() {
        return this._sknTxtBoxFocusSkin;
      });

      defineSetter(this, "iconDropdownExpand", function(val) {
        if((typeof val=='string') && (val != "")){
          this._iconDropdownExpand=val;
        }
      });

      //getter method for List Object in the group:Context
      defineGetter(this, "iconDropdownExpand", function() {
        return this._iconDropdownExpand;
      });

      defineSetter(this, "iconDropdownCollapse", function(val) {
        if((typeof val=='string') && (val != "")){
          this._iconDropdownCollapse=val;
        }
      });

      //getter method for List Object in the group:Context
      defineGetter(this, "iconDropdownCollapse", function() {
        return this._iconDropdownCollapse;
      });
      defineSetter(this, "BREAKPTS", function(val) {
        if((typeof val=='string') && (val != "")){
          this._BREAKPTS=val;
        }
      });

      //getter method for List Object in the group:Context
      defineGetter(this, "BREAKPTS", function() {
        return this._BREAKPTS;
      });
      //setter method for List Object in the group:Context
      defineSetter(this, "listObject", function(val) {
        if((typeof val=='string') && (val != "")){
          this._listObject=val;
        }
      });

      //getter method for List Object in the group:Context
      defineGetter(this, "listObject", function() {
        return this._listObject;
      });

      //setter method for Text Box in the group:Context
      defineSetter(this, "txtBox", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtBox=val;
        }
      });

      //getter method for Text Box in the group:Context
      defineGetter(this, "txtBox", function() {
        return this._txtBox;
      });

      //setter method for Type Lists to Filter in the group:Context
      defineSetter(this, "filterTypeList", function(val) {
        if((typeof val=='string') && (val != "")){
          this._filterTypeList=val;
        }
      });

      //getter method for Type Lists to Filter in the group:Context
      defineGetter(this, "filterTypeList", function() {
        return this._filterTypeList;
      });

      //setter method for Dropdown List Field1 in the group:Context
      defineSetter(this, "dropdownListField1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dropdownListField1=val;
        }
      });

      //getter method for Dropdown List Field1 in the group:Context
      defineGetter(this, "dropdownListField1", function() {
        return this._dropdownListField1;
      });

      //setter method for Dropdown List Field2 in the group:Context
      defineSetter(this, "dropdownListField2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dropdownListField2=val;
        }
      });

      //getter method for Dropdown List Field2 in the group:Context
      defineGetter(this, "dropdownListField2", function() {
        return this._dropdownListField2;
      });

      //setter method for Dropdown List Field3 in the group:Context
      defineSetter(this, "dropdownListField3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dropdownListField3=val;
        }
      });

      //getter method for Dropdown List Field3 in the group:Context
      defineGetter(this, "dropdownListField3", function() {
        return this._dropdownListField3;
      });

      //setter method for Empty Record Message in the group:Context
      defineSetter(this, "emptyRecordMessage", function(val) {
        if((typeof val=='string') && (val != "")){
          this._emptyRecordMessage=val;
        }
      });

      //getter method for Empty Record Message in the group:Context
      defineGetter(this, "emptyRecordMessage", function() {
        return this._emptyRecordMessage;
      });

      //setter method for Empty Recor Action Item Text in the group:Context
      defineSetter(this, "emptyRecordActionTxt", function(val) {
        if((typeof val=='string') && (val != "")){
          this._emptyRecordActionTxt=val;
        }
      });

      //getter method for Empty Recor Action Item Text in the group:Context
      defineGetter(this, "emptyRecordActionTxt", function() {
        return this._emptyRecordActionTxt;
      });

      //setter method for Text Box Value Skin in the group:Context
      defineSetter(this, "sknTxtBoxValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknTxtBoxValue=val;
        }
      });

      //getter method for Text Box Value Skin in the group:Context
      defineGetter(this, "sknTxtBoxValue", function() {
        return this._sknTxtBoxValue;
      });

      //setter method for Text Box Placeholder Skin in the group:Context
      defineSetter(this, "sknTxtBoxPlaceholder", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknTxtBoxPlaceholder=val;
        }
      });

      //getter method for Text Box Placeholder Skin in the group:Context
      defineGetter(this, "sknTxtBoxPlaceholder", function() {
        return this._sknTxtBoxPlaceholder;
      });

      //setter method for Dropdown List Field1 Skin in the group:Context
      defineSetter(this, "sknListField1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknListField1=val;
        }
      });

      //getter method for Dropdown List Field1 Skin in the group:Context
      defineGetter(this, "sknListField1", function() {
        return this._sknListField1;
      });

      //setter method for Dropdown List Field2 Skin in the group:Context
      defineSetter(this, "sknListField2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknListField2=val;
        }
      });

      //getter method for Dropdown List Field2 Skin in the group:Context
      defineGetter(this, "sknListField2", function() {
        return this._sknListField2;
      });

      //setter method for Dropdown List Field3 Skin in the group:Context
      defineSetter(this, "sknListField3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknListField3=val;
        }
      });

      //getter method for Dropdown List Field3 Skin in the group:Context
      defineGetter(this, "sknListField3", function() {
        return this._sknListField3;
      });

      //setter method for List Field Type Skin in the group:Context
      defineSetter(this, "sknListFieldType", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknListFieldType=val;
        }
      });

      //getter method for List Field Type Skin in the group:Context
      defineGetter(this, "sknListFieldType", function() {
        return this._sknListFieldType;
      });

      //setter method for Clear Search Filter Icon in the group:Context
      defineSetter(this, "iconClearFilter", function(val) {
        if((typeof val=='string') && (val != "")){
          this._iconClearFilter=val;
        }
      });

      //getter method for Clear Search Filter Icon in the group:Context
      defineGetter(this, "iconClearFilter", function() {
        return this._iconClearFilter;
      });

      //setter method for City Bank Icon in the group:Context
      defineSetter(this, "iconCityBank", function(val) {
        if((typeof val=='string') && (val != "")){
          this._iconCityBank=val;
        }
      });

      //getter method for City Bank Icon in the group:Context
      defineGetter(this, "iconCityBank", function() {
        return this._iconCityBank;
      });

      //setter method for Chase Bank Icon in the group:Context
      defineSetter(this, "iconChaseBank", function(val) {
        if((typeof val=='string') && (val != "")){
          this._iconChaseBank=val;
        }
      });

      //getter method for Chase Bank Icon in the group:Context
      defineGetter(this, "iconChaseBank", function() {
        return this._iconChaseBank;
      });

      //setter method for BAO Bank Icon in the group:Context
      defineSetter(this, "iconBAOBank", function(val) {
        if((typeof val=='string') && (val != "")){
          this._iconBAOBank=val;
        }
      });

      //getter method for BAO Bank Icon in the group:Context
      defineGetter(this, "iconBAOBank", function() {
        return this._iconBAOBank;
      });

      //setter method for HDFC Bank Icon in the group:Context
      defineSetter(this, "iconHDFCBank", function(val) {
        if((typeof val=='string') && (val != "")){
          this._iconHDFCBank=val;
        }
      });

      //getter method for HDFC Bank Icon in the group:Context
      defineGetter(this, "iconHDFCBank", function() {
        return this._iconHDFCBank;
      });

      //setter method for Infinity Bank Icon in the group:Context
      defineSetter(this, "iconInfinityBank", function(val) {
        if((typeof val=='string') && (val != "")){
          this._iconInfinityBank=val;
        }
      });

      //getter method for Infinity Bank Icon in the group:Context
      defineGetter(this, "iconInfinityBank", function() {
        return this._iconInfinityBank;
      });

      //setter method for External Bank Icon in the group:Context
      defineSetter(this, "iconExternalBank", function(val) {
        if((typeof val=='string') && (val != "")){
          this._iconExternalBank=val;
        }
      });

      //getter method for External Bank Icon in the group:Context
      defineGetter(this, "iconExternalBank", function() {
        return this._iconExternalBank;
      });
    },
    preShow	:	function(){
      this.setComponentConfigs();
      this.initActions();
    },
    initActions : function(){
      this.view.flxTextBox.onTouchStart =  this.showHideDropdown;
      this.view.segRecords.onRowClick = this.segRowClick;
      this.view.txtBox.onKeyUp = this.txtBoxOnKeyUp;
      this.view.flxClearText.onTouchStart = this.clearTextBoxTexts;
      this.view.lblNoRecordsAction.onTouchStart = this.noRecordsAction;
      this.view.flxClearText.setVisibility(false);
      this.view.txtBox.onEndEditing = this.getTxtBoxValue;
    },
    setComponentConfigs: function() {
      this.parserUtilsManager.setBreakPointConfig(JSON.parse(this._BREAKPTS));
    },
    postShow : function(){
      try{
        this.setTextBoxContracts();
        this.setLabelTexts();
        this.storeIconValues();
        this.setIcons();
        this.setSkins();
        this.setListValuesFromContext();
        this.filterRecordsList();
        this.setSegmentData();
        											  
        this.setDropdownHeight();
      }
      catch(err){
        {
          var errorObj =
              {
                "errorInfo" : "Error in setting field property" ,
                "errorLevel" : "Business",
                "error": err
              };
          self.onError(errorObj);
        }
      }
    },
    onBreakPointChange	:	function(){
      if(this.contextSet){
        this.setTextBoxContracts();
        this.setLabelTexts();
        this.storeIconValues();
        this.setIcons();
        this.setSkins();
        this.setSegmentData();
        this.setDropdownHeight();
        this.view.forceLayout();
      }
    },
    /*
     * Component setContext
     * Responsible to set context data.
     */
    setContext	:	function(context, scope){
      var self = this;
      try{
        this.context = context;
        if(this.parentScope == ""){
          this.parentScope = scope;
        }
        this.parserUtilsManager.setContext(this.context);
        this.postShow();
        this.contextSet = true;
      }
      catch(err){
        {
          var errorObj =
              {
                "errorInfo" : "Error in setting context for autocomplete component" ,
                "errorLevel" : "Business",
                "error": err
              };
          self.onError(errorObj);
        }
      }
    },
    /*
     * Component showHideDropdown
     * Responsible to show or hide dropdown list
     */
    showHideDropdown	:	function(){
      var self = this;
      try{
        this.view.flxClearText.setVisibility(false);
        if(!(this.view.flxSegment.isVisible) && !(this.view.flxNoRecords.isVisible) ){

          this.view.txtBox.setVisibility(true);
          this.view.txtBox.setFocus(true);
          var segData = this.view.segRecords.data;
          if(segData.length != 0){
            this.view.flxSegment.setVisibility(true);
          }
          else{
            this.view.flxNoRecords.setVisibility(true);
          }
          this.view.lblSelectedRecordField1.setVisibility(false);
          this.view.lblSelectedRecordField2.setVisibility(false);
        }else{
          this.view.flxSegment.setVisibility(false);
          this.view.flxNoRecords.setVisibility(false);
          if(this.view.lblSelectedRecordField1.text != ""){
            this.view.lblSelectedRecordField1.setVisibility(true);
          }
          else{
            this.view.lblSelectedRecordField1.setVisibility(false);

          }
          if(this.view.lblSelectedRecordField2.text != ""){
            this.view.lblSelectedRecordField2.setVisibility(true);
          }
          else{
            this.view.lblSelectedRecordField2.setVisibility(false);

          }
          if(this.view.lblSelectedRecordField1.text != "" && this.view.lblSelectedRecordField2.text != ""){
            this.view.txtBox.setVisibility(false);
          }else{
            this.view.txtBox.setVisibility(true);
          }

        }
        this.view.forceLayout();
      }
      catch(err){
        {
          var errorObj =
              {
                "errorInfo" : "Error in setting dropdown visibility for autocomplete component" ,
                "errorLevel" : "Business",
                "error": err
              };
          self.onError(errorObj);
        }
      }

    },
    /*
     * Component setTextBoxContracts
     * Responsible to set the text box contract received from context
     */
    setTextBoxContracts	:	function(){
      var self = this;
      try{
        var txtBoxProps = JSON.parse(this.getParsedValue(this._txtBox));


        if(!this.isNullOrUndefinedOrEmpty(txtBoxProps)) {
          //Placeholder
          if(!this.isNullOrUndefinedOrEmpty(txtBoxProps.placeHolder)){
            var placeHolderValue = this.getParsedValue(txtBoxProps.placeHolder,kony.application.getCurrentBreakpoint());
            this.view.txtBox.placeholder =
              placeHolderValue ? placeHolderValue : "";
          }
          //tooltip
          if(!this.isNullOrUndefinedOrEmpty(txtBoxProps.tooltip)) {
            this.view.txtBox.toolTip = this.getParsedValue(txtBoxProps.tooltip,kony.application.getCurrentBreakpoint());
          }
          //inputMode
          if(!this.isNullOrUndefinedOrEmpty(txtBoxProps.inputMode)){
            if(txtBoxProps.inputMode === "NUMERIC" && txtBoxProps.isMaskingEnabled === true){
              this.view.txtBox.restrictCharactersSet = 
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_-\\?/+={[]}:;,.<>'`|\"";
              this.view.txtBox.secureTextEntry = true;
            }
            else if(txtBoxProps.inputMode === "NUMERIC" && txtBoxProps.isMaskingEnabled === false) {
              this.view.txtBox.textInputMode 
                = constants.TEXTBOX_INPUT_MODE_NUMERIC;
              this.view.txtBox.secureTextEntry = false;
            }
            else {
              this.view.txtBox.restrictCharactersSet
                = "";
              this.view.txtBox.textInputMode 
                = constants.TEXTBOX_INPUT_MODE_ANY;
              this.view.txtBox.secureTextEntry = txtBoxProps.isMaskingEnabled ? txtBoxProps.isMaskingEnabled : false;
            }
            return;
          }
          this.view.txtBox.restrictCharactersSet = "";
          this.view.txtBox.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
          this.view.txtBox.secureTextEntry = txtBoxProps.isMaskingEnabled ? txtBoxProps.isMaskingEnabled : false;
        }
      }
      catch(err){
        {
          var errorObj =
              {
                "errorInfo" : "Error in setting textbox contracts  autocomplete component" ,
                "errorLevel" : "Business",
                "error": err
              };
          self.onError(errorObj);
        }
      }
    },
    /*
     * Component setTextBoxContracts
     * Responsible to set the label texts received from context
     */
    setLabelTexts : function(){
      var self = this;
      try{
        var emptyRecordMessage = this.getParsedValue(this.getParsedValue(this._emptyRecordMessage),kony.application.getCurrentBreakpoint());
        if(!this.isNullOrUndefinedOrEmpty(emptyRecordMessage)){
          this.view.lblNoRecords.text = emptyRecordMessage;
        }
        else{
          this.view.lblNoRecords.setVisibility(false);
        }
        var emptyRecordActionMessage = this.getParsedValue(this.getParsedValue(this._emptyRecordActionTxt),kony.application.getCurrentBreakpoint());
        if(!this.isNullOrUndefinedOrEmpty(emptyRecordActionMessage)){
          this.view.lblNoRecordsAction.text = emptyRecordActionMessage;
        }else{
          this.view.lblNoRecordsAction.setVisibility(false);
        }
      }
      catch(err){
        {
          var errorObj =
              {
                "errorInfo" : "Error in setting label texts for autocomplete component" ,
                "errorLevel" : "Business",
                "error": err
              };
          self.onError(errorObj);
        }
      }
    },    
    /*
     * Component isNullOrUndefinedOrEmpty
     * Responsible to return given val is null or empty
     * return boolean
     */
    isNullOrUndefinedOrEmpty	:	function(val){
      if (val === null || val === undefined || val ==="") {
        return true;
      } else {
        return false;
      }
    },
    /*
     * Component getParsedValue
     * Responsible to get the parsed text for the property
     * return String
     */
    getParsedValue: function(property, selectedValue) {
      var self = this;
      try{
        property = JSON.parse(property);
      }
      catch(e){
        property = property;
        kony.print(e);
      }
      if(typeof(property) === "string")
        return this.getProcessedText(property);
      else{
        if(selectedValue)
          return this.getProcessedText(this.parserUtilsManager.getComponentConfigParsedValue(property,selectedValue));
        else
          return property;
      }
    },
    getProcessedText: function(text) {
      return this.parserUtilsManager.getParsedValue(text);
    },
    /*
     * Component storeIconValues
     * Responsible to get the parsed images values and store in icon lists
     */
    storeIconValues: function(){
      var self = this;
      try{
        this.Icons = {
          "clearText" : this.getParsedValue(this._iconClearFilter),
          "dropdownExpand" : this.getParsedValue(this._iconDropdownExpand),
		  "dropdownCollapse" : this.getParsedValue(this._iconDropdownCollapse),
          "CITI_BANK_IMAGE" : this.getParsedValue(this._iconCityBank),
          "CHASE_BANK_IMAGE" :this.getParsedValue(this._iconChaseBank),
          "BOA_BANK_IMAGE" : this.getParsedValue(this._iconBAOBank),
          "HDFC_BANK_IMAGE" : this.getParsedValue(this._iconHDFCBank),
          "INFINITY_BANK_IMAGE" : this.getParsedValue(this._iconInfinityBank),
          "EXTERNAL_BANK_IMAGE" : this.getParsedValue(this._iconExternalBank)
        }
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in setting the parsed icon value for autocomplete dropdown",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);

      }
    },
    /*
     * Component getParsedImageValue
     * Responsible to get the parsed images values from contracts
     * return String
     */
    getParsedImageValue: function(contractJSON){
      var self = this;
      try{
        contractJSON = JSON.parse(contractJSON);
        if(contractJSON.hasOwnProperty("img")){
          contractJSON = contractJSON["img"];
        }
      }
      catch(err){
        var errorObj =
            {
              "errorInfo" : "Error in getting the parsed icon value",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);

      }
      return contractJSON;
    },
    /*
     * Component setIcons
     * Responsible to set the icons source to respecctive widgets
     */
    setIcons: function(){
      var self = this;
      try
      {
        this.view.imgClearText.src = this.Icons.clearText;
      }
      catch(err){
        var errorObj =
            {
              "errorInfo" : "Error in setting the icons",
              "errorLevel" : "Configuration",
              "error": err
            };
        self.onError(errorObj);

      }
    },
    /*
     * Component setSkins
     * Responsible to set the skins to respecctive widgets
     */
    setSkins: function(){
      var self = this;
      try
      {
        this.view.txtBox.skin= this.getParsedValue(this.getParsedValue(this._sknTxtBoxValue),kony.application.getCurrentBreakpoint());
        this.view.txtBox.focusSkin=this.getParsedValue(this.getParsedValue(this._sknTxtBoxFocusSkin),kony.application.getCurrentBreakpoint());
        this.view.txtBox.placeholderSkin = this.getParsedValue(this.getParsedValue(this._sknTxtBoxPlaceholder),kony.application.getCurrentBreakpoint());
        this.view.lblNoRecords.skin=this.getParsedValue(this.getParsedValue(this._sknEmptyRecordLabel),kony.application.getCurrentBreakpoint());
        this.view.lblNoRecordsAction.skin=this.getParsedValue(this.getParsedValue(this._sknEmptyRecordActionLabel),kony.application.getCurrentBreakpoint()); 
      }
      catch(e){
        var errorObj =
            {
              "errorInfo" : "Error in setting the skins",
              "errorLevel" : "Configuration",
              "error": e
            };
        self.onError(errorObj);
      }

    },
    /*
     * Component setListValuesFromContext
     * Responsible to set the listvalue from context
     */
    setListValuesFromContext	:	function(){
      var contextObject = this._listObject.substring(5,this._listObject.length-1);
      var listObject =  this.context[contextObject];
      this.recordsArray = listObject;
    },
    /*
     * Component filterRecordsList
     * Responsible to perform filter operation based on the contract
     */
    filterRecordsList	:	function(){
      var self = this;
      try{
        if(!this.isNullOrUndefinedOrEmpty(this.getParsedValue(this._filterTypeList))){
          var filterListObject = JSON.parse(this.getParsedValue(this._filterTypeList));
          if(!this.isNullOrUndefinedOrEmpty(filterListObject.list) && !this.isNullOrUndefinedOrEmpty(filterListObject.identifier)){
            var filterList = filterListObject.list.split(",");
            var filterVariable = filterListObject.identifier;
            var tempRecords = this.recordsArray;
            var filteredRecords =tempRecords.filter(function (record) {
              var removeRecord =  false;
              for(var i=0;i<filterList.length;i++){
                if(record[filterVariable] == filterList[i])
                {
                  removeRecord =  true;
                }
              }  
              return !removeRecord;
            });
            this.recordsArray = filteredRecords;
          }

        }
      }
      catch(e){
        var errorObj =
            {
              "errorInfo" : "Error in performing filter operation",
              "errorLevel" : "Buisness",
              "error": e
            };
        self.onError(errorObj);
      }
    },
    /*
     * Component setSegmentData
     * Responsible to set dropdown segment data
     */
    setSegmentData	:	function(){
      var self  = this;
      try{
        var segment = this.view.segRecords;
        this.setSegmentTemplate();
        segment.widgetDataMap = {
          "lblRecordsType" : "lblRecordsType",
          "imgExpandCollapse" : "imgExpandCollapse",
          "lblField1" : "lblField1",
          "lblField2" : "lblField2",
          "flxFieldTypeIcon1" : "flxFieldTypeIcon1",
          "flxFieldTypeIcon2" : "flxFieldTypeIcon2",
          "flxFieldType3" : "flxFieldType3",
          "lblFieldTypeIcon1" : "lblFieldTypeIcon1",
          "imgFieldTypeIcon2" : "imgFieldTypeIcon2",
          "lblField3" : "lblField3",
          "flxFieldType" : "flxFieldType"
        };
        this.mapRecords();
        if(!this.isNullOrUndefinedOrEmpty(this.getParsedValue(this._groupIdentifier))){
          this.allRecordsSegData = this.prepareSegmentData(this.recordsArray);
          segment.setData(this.allRecordsSegData);
        }else{
          this.allRecordsSegData = this.recordsArray;
          segment.setData(this.allRecordsSegData);
        }

      }
      catch(e){
        var errorObj =
            {
              "errorInfo" : "Error in setting the segment data",
              "errorLevel" : "Buisness",
              "error": e
            };
        self.onError(errorObj);
      }
    },
    /*
     * Component prepareSegmentData
     * Responsible to group and prepare segment data 
     * return list Object 
     */
    prepareSegmentData	:	function(recordsList){
      var self = this;
      try{
        var data=[];
        var groupedRecordsList = this.groupResponseData(recordsList);
        var types = Object.keys(groupedRecordsList);
        var headerLabelSkin = this.getParsedValue(this.getParsedValue(this._sknListFieldType),kony.application.getCurrentBreakpoint());
        if(types.length != 0 ){
          for(var i=0;i<types.length;i++){            
            var displayText;           
            if(types[i] != "undefined") {
               displayText = this.getDisplayText(types[i]);
            }else{
              displayText = this.getDisplayText("default");
            }
            if(this.isNullOrUndefinedOrEmpty(displayText)){
              displayText = types[i];
            }
            data[i] = [{
              "lblRecordsType" : {"text" : displayText,"skin":headerLabelSkin},
              "imgExpandCollapse" : this.Icons.dropdownCollapse
            },
                       groupedRecordsList[types[i]]]
          }
        }
        return data;
      }
      catch(e){
        var errorObj =
            {
              "errorInfo" : "Error in preparing segment data",
              "errorLevel" : "Buisness",
              "error": e
            };
        self.onError(errorObj);
      }
    },
    /*
     * Component setSegmentTemplate
     * Responsible to segement template 
     */
    setSegmentTemplate	:	function(){
      var self = this;
      try{
        if(kony.application.getCurrentBreakpoint() != 640)
        {
          this.view.segRecords.sectionHeaderTemplate = "flxACDropdownRecordHeader";
          this.view.segRecords.rowTemplate = "flxACDropdownRecord";
        }else{
          this.view.segRecords.sectionHeaderTemplate = "flxMobileACDropdownRecordHeader";
          this.view.segRecords.rowTemplate = "flxMobileACDropdownRecord";
        }
        if(this.isNullOrUndefinedOrEmpty(this.getParsedValue(this._groupIdentifier))){
          this.view.segRecords.sectionHeaderTemplate = "";
        }
      }
      catch(e){
        var errorObj =
            {
              "errorInfo" : "Error in setting the segment template",
              "errorLevel" : "Buisness",
              "error": e
            };
        self.onError(errorObj);
      }
    },
    /*
     * Component mapRecords
     * Responsible to map widget texts with list fields
     */
    mapRecords	:	function(){
      var self = this;
      try{
        var recordsList = this.recordsArray;
        for(var i=0;i<recordsList.length;i++){
          var field1Data = this.getFormattedData(this.getParsedValue(this._dropdownListField1),recordsList[i]);
          var field1DataSkin = this.getParsedValue(this.getParsedValue(this._sknListField1),kony.application.getCurrentBreakpoint());
          var field2Data = this.getFormattedData(this.getParsedValue(this._dropdownListField2),recordsList[i]);
          var field2DataSkin = this.getParsedValue(this.getParsedValue(this._sknListField2),kony.application.getCurrentBreakpoint());
          var field3Data = this.getFormattedData(this.getParsedValue(this._dropdownListField3),recordsList[i]);
          var field3DataSkin = this.getParsedValue(this.getParsedValue(this._sknListField3),kony.application.getCurrentBreakpoint());
          var bankIcon = (!kony.sdk.isNullOrUndefined(this.getBankIcon(recordsList[i].bankName)))?this.getBankIcon(recordsList[i].bankName):"null";
          var icon1Visibility = false;
          icon1Visibility = this.hasMixAccounts(recordsList);
          var field1Value,field2Value,field3FlxValue,field3Value;
          if(this.isNullOrUndefinedOrEmpty(field1Data)){
            field1Value = {"text" : "", isVisible : false};
          }else{
            field1Value = {"text" : field1Data, "skin" : field1DataSkin };
          }
          if(this.isNullOrUndefinedOrEmpty(field2Data)){
            field2Value = {"text" : "", isVisible : false};
          }else{
            field2Value = {"text" : field2Data, "skin" : field2DataSkin };
          }
          if(this.isNullOrUndefinedOrEmpty(field3Data)){
            field3FlxValue = {isVisible : false};
            field3Value = {"text" : "", isVisible : false};
            if(this.isNullOrUndefinedOrEmpty(field1Data)){
              field1Value = {"text" : "", isVisible : false};
            }else{
              field1Value = {"text" : field1Data, "skin" : field1DataSkin,"centerY": "50%" };
            }
            if(this.isNullOrUndefinedOrEmpty(field2Data)){
              field2Value = {"text" : "", isVisible : false};
            }else{
              field2Value = {"text" : field2Data, "skin" : field2DataSkin,"centerY": "50%" };
            }
          }else{
            field3Value = {"text" : field3Data, "skin" : field3DataSkin};           
            field3FlxValue = {isVisible : true};          
          }
          recordsList[i]["lblField1"] = field1Value;
          recordsList[i]["lblField2"] = field2Value ;
          recordsList[i]["lblField3"] = field3Value;
          recordsList[i]["imgFieldTypeIcon2"] = {"src" :bankIcon};
          recordsList[i]["flxFieldTypeIcon2"] = { "isVisible": recordsList[i].isSameBankAccount === "false" && !kony.sdk.isNullOrUndefined(bankIcon)  ? true : false };
          recordsList[i]["lblFieldTypeIcon1"] = (recordsList[i].isBusinessAccount==="true")|| (recordsList[i].isBusinessPayee==="1") ? "r" :"s";
          recordsList[i]["flxFieldTypeIcon1"] =  {"isVisible" :  icon1Visibility};
          recordsList[i]["flxFieldType"] = field3FlxValue;
        }
        this.recordsArray = recordsList;
        return recordsList;
      }
      catch(e){
        var errorObj =
            {
              "errorInfo" : "Error in mapping records",
              "errorLevel" : "Buisness",
              "error": e
            };
        self.onError(errorObj);
      }
    },
    /*
     * Component segRowClick
     * Responsible to get the selected row record detail
     * return list Object 
     */
    segRowClick: function () {
      var self = this;
      try{
        var selectedRecord  = this.view.segRecords.selectedRowItems[0];
        this.view.flxClearText.setVisibility(false);
        this.view.txtBox.text =  selectedRecord.lblField1.text;
        this.view.lblSelectedRecordField1.text = selectedRecord.lblField1.text;
        this.view.lblSelectedRecordField2.text = selectedRecord.lblField2.text;  
        this.showHideDropdown();
        if(!kony.sdk.isNullOrUndefined(this.fetchSelectedRecordDetail)){
          this.fetchSelectedRecordDetail(selectedRecord);
        }
        if(this.searchApplied){
          this.view.segRecords.removeAll();
          this.view.segRecords.setData(this.allRecordsSegData);
        }
        this.view.forceLayout();
      }
      catch(e){
        var errorObj =
            {
              "errorInfo" : "Error in getting selected row detail",
              "errorLevel" : "Buisness",
              "error": e
            };
        self.onError(errorObj);
      }
    },
    /*
     * Component txtBoxOnKeyUp
     * Responsible to perform search operation and update segment data 
     */
    txtBoxOnKeyUp	:	function(){
      var self = this;
      try{
        this.view.flxClearText.setVisibility(true);
        var searchTxt = this.view.txtBox.text.toLowerCase();
        this.searchApplied = false;
        if(searchTxt != ""){
          var result=[];
          var data = this.recordsArray;
          for(var i=0;i<data.length;i++){
            if(data[i].lblField1.text.toLowerCase().indexOf(searchTxt) != -1 || data[i].lblField2.text.toLowerCase().indexOf(searchTxt) != -1 || data[i].lblField3.text.toLowerCase().indexOf(searchTxt) != -1){
              result.push(data[i]);
            }
          }
          if(!(result.length > 0)){
            this.view.flxSegment.setVisibility(false);
            this.view.flxNoRecords.setVisibility(true);
          }else{
            this.searchApplied = true;
            result = this.prepareSegmentData(result);
            this.view.segRecords.removeAll();
            this.view.segRecords.setData(result);
            this.view.flxSegment.setVisibility(true);
            this.view.flxNoRecords.setVisibility(false);
          }
        }
        else{
          this.searchApplied = false;
          this.view.segRecords.removeAll();
          this.view.segRecords.setData(this.allRecordsSegData);
          this.view.flxSegment.setVisibility(true);
          this.view.flxNoRecords.setVisibility(false);
          this.view.flxClearText.setVisibility(false);
          this.view.lblSelectedRecordField1.text= "";
          this.view.lblSelectedRecordField2.text="";
        }
        this.view.forceLayout();
      }
      catch(e){
        var errorObj =
            {
              "errorInfo" : "Error in performing search",
              "errorLevel" : "Buisness",
              "error": e
            };
        self.onError(errorObj);
      }

    },
    /*
     * Component clearTextBoxTexts
     * Responsible to clear text box texts  
     */
    clearTextBoxTexts	:	function(){
      this.view.txtBox.text="";
      this.view.flxClearText.setVisibility(false);
      this.searchApplied = false;
      this.view.segRecords.removeAll();
      this.view.segRecords.setData(this.allRecordsSegData);
      this.view.flxSegment.setVisibility(true);
      this.view.flxNoRecords.setVisibility(false);
    },
    /*
     * Component groupResponseData
     * Responsible to group list data based on the identifier 
     * return list Object 
     */
    groupResponseData: function (data) {
      var self = this;
      var contract = JSON.parse(this.getParsedValue(this._groupIdentifier));
      var internalContract = this.getFieldValue(contract, "internal");
      if(!this.isNullOrUndefinedOrEmpty(internalContract))
      {
        var interalKey = this.getParsedValue(internalContract.identifier);
      }
      var externalContract = this.getFieldValue(contract, "external");
      if(!this.isNullOrUndefinedOrEmpty(externalContract))
      {
        var externalKey = this.getParsedValue(externalContract.identifier);
      }
      try{
        if(data!==undefined){
          var hasBuisnessAccounts = this.checkforBuisnessAccount(data);
          if(hasBuisnessAccounts){
            return data.reduce(function (value, obj) {
              if(obj.isBusinessAccount == "true"){
                (value[obj["MembershipName"]] = value[obj["MembershipName"]] || []).push(obj);
                return value;
              }else{
                if(!(obj.isBusinessAccount === null || obj.isBusinessAccount === undefined || obj.isBusinessAccount ==="")){
                  (value[obj["otherAccounts"]] = value[obj["otherAccounts"]] || []).push(obj);
                  return value;
                }else{
                  (value[obj["GroupField"]] = value[obj["GroupField"]] || []).push(obj);
                  return value;
                }
              }
            }, {});
          }
          else{
            return data.reduce(function (value, obj) {
              if(obj.isExternalAccount == false){
                if(!(interalKey === null || interalKey === undefined || interalKey ===""))
                {
                  (value[obj[interalKey]] = value[obj[interalKey]] || []).push(obj);
                  return value;
                }else{
                  (value[obj["GroupField"]] = value[obj["GroupField"]] || []).push(obj);
                  return value;
                }
              }else{
                if(!(externalKey === null || externalKey === undefined || externalKey ===""))
                {
                  if(!externalKey.includes(",")){
                  (value[obj[externalKey]] = value[obj[externalKey]] || []).push(obj);
                  return value;
                  }else{
                    var indentifierKey = externalKey.split(",");
                    for(var i=0;i<indentifierKey.length;i++){
                      if(obj[indentifierKey[i]]){
                         (value[obj[indentifierKey[i]]] = value[obj[indentifierKey[i]]] || []).push(obj);
                         return value;
                      }
                    }
                  }
                }else{
                  (value[obj["GroupField"]] = value[obj["GroupField"]] || []).push(obj);
                  return value;
                }
              }
            }, {});}
        }
        else return {};
      }catch(err){
        var errorObj =
            {
              "errorInfo" : "Error in  grouping data based on identifier",
              "errorLevel" : "Configuration",
              "error": err
            };
        self.onError(errorObj);
      }
    },
    /*
     * Component noRecordsAction
     * Responsible to show no records found flex
     */
    noRecordsAction : function(){
      this.view.flxNoRecords.setVisibility(false);
      this.emptyRecordAction();
    },
    /*
     * Component getFieldValue
     * Responsible to return the field value from JSON
     * return String 
     */
    getFieldValue: function(Value,key) {
      try 
      {
        var value = Value;
        if(typeof(Value) == "string") {
          value = JSON.parse(Value);
        }
        if(value[this.accountType]) {
          value = value[this.accountType];
        }
        if(!this.isNullOrUndefinedOrEmpty(value) && !this.isNullOrUndefinedOrEmpty(key)) {
          value = value[key];
        }
        if (value !== null && value !== "" && value !== undefined) {
          if(typeof(value)=="string")
            return this.getProcessedText(value);
          else {
            var text=this.breakPointParser(value,kony.application.getCurrentBreakpoint());
            return this.getProcessedText(text);
          }
        } else return "";
      }  
      catch(err)
      {
        kony.print(err);
      }
      return value;
    },
    /*
     * Component getFormattedData
     * Responsible to format data based on the format contract
     * return String 
     */
    getFormattedData : function(contractJSON,record){
      var self = this;
      try{
        if(!this.isNullOrUndefinedOrEmpty(contractJSON)){
          contractJSON = JSON.parse(contractJSON);
          var isMaskingRequired = contractJSON["isMaskingEnabled"];
          if(record["isExternalAccount"]){
            contractJSON = this.getFieldValue(contractJSON,"external");
          }else{
            if(record["accountType"] == "CreditCard"){
              contractJSON = this.getFieldValue(contractJSON["internal"],"CreditCard");
            }else{
              contractJSON = this.getFieldValue(contractJSON["internal"],"default");
            }
          }
          if(!this.isNullOrUndefinedOrEmpty(contractJSON)){

            if(contractJSON.fieldType == "AccountName"){
              //var isMaskingRequired = this.getParsedValue(this._txtBox); 
              //isMaskingRequired = JSON.parse(isMaskingRequired);
              var fields = contractJSON["mapping"];
              var data = "";
              for(var i=0;i<fields.length;i++){
                var lblFieldMapping = fields[i]["fieldMap"];
                var fieldData = record[lblFieldMapping];
                if(!this.isNullOrUndefinedOrEmpty(fields[i].format))
                {
                  if(!this.isNullOrUndefinedOrEmpty(fields[i].format.truncateLength)){
                    data = data + fieldData.substring(0,fields[i].format.truncateLength) + fields[i].format.appendString;
                  } 
                  if(!this.isNullOrUndefinedOrEmpty(fields[i].format.sliceLength)){
                    if(isMaskingRequired === true) {
                    data = data + fieldData.slice(fields[i].format.sliceLength);
                  } else {
                    data = data + fieldData;
                  }}
                }else{
                  data = data + fieldData;
                }
            }
              return data;
            }else if(contractJSON.fieldType == "Amount"){
              var data = "";
              var balancePrefix = contractJSON["typeToDisplay"];
              var fieldMapping = contractJSON.mapping[0]["fieldMap"];
              var fieldData = record[fieldMapping];
              var currencyMapping =  this.getParsedValue(contractJSON.currency);
              var currencyCode = record[currencyMapping];
              if(!this.isNullOrUndefinedOrEmpty(fieldData)){
                if(!this.isNullOrUndefinedOrEmpty(contractJSON.mapping[0].format)){
                  if(balancePrefix === "currencySymbol") {
                  data = this.FormatUtils.formatAmountandAppendCurrencySymbol(fieldData,currencyCode,contractJSON.mapping[0].format);
                  data = data.text;
                  } else
                  {
                  data = this.FormatUtils.formatAmount(fieldData,contractJSON.mapping[0].format);
                  data = currencyCode + " " + data;   
                  }
                }
                else{
                  if(balancePrefix === "currencySymbol") {
                  data = this.FormatUtils.getCurrencySymbol(currencyCode) + fieldData;
                  } else {
                  data = currencyCode + " " + fieldData;
                  }
                }
              }
              else{
                return data;
              }
            }
            else{
              var data = "";
              if(!this.isNullOrUndefinedOrEmpty(contractJSON.mapping[0])){
                if(!contractJSON.mapping[0]["fieldMap"].includes(",")){
                  var fieldMapping = contractJSON.mapping[0]["fieldMap"];
                  data = record[fieldMapping];
                }else{
                  var fieldMapList = contractJSON.mapping[0]["fieldMap"].split(",");
                  for(var i=0;i<fieldMapList.length;i++){
                    if(record[fieldMapList[i]])
                      data = record[fieldMapList[i]];
                  }
                }
              }

            }
            return data;
          }
        else{
          return "";
        }
      }
      }
      catch(err){
        var errorObj =
            {
              "errorInfo" : "Error in formatting data ",
              "errorLevel" : "Configuration",
              "error": err
            };
        self.onError(errorObj);
      }
    },
    /*
     * Component getBankIcon
     * Responsible to get bank icon based on bank name
     * return image
     */
    getBankIcon : function(bankName){
      var self = this;
      var img ;
      try{
        if(!kony.sdk.isNullOrUndefined(bankName)){
          if(bankName.toLowerCase().includes("citi"))
            img= this.Icons.CITI_BANK_IMAGE;
          else if(bankName.toLowerCase().includes("chase"))
            img= this.IconsCHASE_BANK_IMAGE;
          else if(bankName.toLowerCase().includes("boa") || bankName.toLowerCase().includes("america") )
            img= this.Icons.BOA_BANK_IMAGE;
          else if(bankName.toLowerCase().includes("hdfc"))
            img = this.Icons.maHDFC_BANK_IMAGE;
          else if(bankName.toLowerCase().includes("infintiy"))
            img = this.Icons.INFINITY_BANK_IMAGE;
          else
            img= this.Icons.EXTERNAL_BANK_IMAGE;
        }
        return img;
      }
      catch(err){
        var errorObj =
            {
              "errorInfo" : "Error in getting Bank Icon ",
              "errorLevel" : "Configuration",
              "error": err
            };
        self.onError(errorObj);
      }

    },
    updateList : function(listObj){
      this.recordsArray = listObj;
      this.setSegmentData();
      this.view.txtBox.text = "";
      this.view.lblSelectedRecordField1.text = "";
      this.view.lblSelectedRecordField2.text = "";
      this.view.lblSelectedRecordField1.setVisibility(false);
      this.view.lblSelectedRecordField2.setVisibility(false);
      this.view.txtBox.setVisibility(true);
      this.view.flxTextBox.forceLayout();
    },
    setDropdownHeight : function(){
      var segData = this.view.segRecords.data;
      var recordsheight = 0;
      var totalHeight = 0;
      if(!this.isNullOrUndefinedOrEmpty(segData)){
        for(var i=0;i<segData.length;i++){
          recordsheight = recordsheight + 40;
          var group = segData[i];
          var recordsLength = group[1].length;
          recordsheight = recordsheight + (recordsLength * 60);
        }
        totalHeight = recordsheight;       
      }
      var breakPoint = kony.application.getCurrentBreakpoint();
      if(breakPoint == 640){
        if(totalHeight < 572){
          this.view.flxSegment.height = totalHeight +"dp";
        }else{
          this.view.flxSegment.height = "572dp";
        }
      }
      else if (breakPoint == 1024){
        if(totalHeight < 520){
          this.view.flxSegment.height = totalHeight +"dp";
        }else{
          this.view.flxSegment.height = "520dp";
        }
      }
      else{
        if(totalHeight < 546){
          this.view.flxSegment.height = totalHeight +"dp";
        }else{
          this.view.flxSegment.height = "546dp";
        }
      }
    },
    getDisplayText : function(key){
      var contract = JSON.parse(this.getParsedValue(this._groupIdentifier));
      var obj = contract.segregation;
      if(!this.isNullOrUndefinedOrEmpty(obj[key]))
      {
        return obj[key];
      }else{
        return "";
      }
    },
    resetSelectedValue : function(){
      this.view.txtBox.text = "";
      this.view.lblSelectedRecordField1.text = "";
      this.view.lblSelectedRecordField2.text = "";
	  this.view.lblSelectedRecordField1.setVisibility(false);
      this.view.lblSelectedRecordField2.setVisibility(false);
      this.view.txtBox.setVisibility(true);
    },
    getTxtBoxValue : function(){
      if(!this.isNullOrUndefinedOrEmpty(this.getTxtBoxData))
      {
        var data = this.view.txtBox.text;
        this.getTxtBoxData(data);
      }
    },
	 /**     
	 * Component rowExpandCollapse
     * To expand and collapse group
    **/ 
    rowExpandCollapse: function (context,scope) {
      var self = this;
      try{
        var sectionIndex = context.section;
        if (self.segmentData === '') 
          self.segmentData = JSON.parse(JSON.stringify(self.view.segRecords.data));
        var data = self.view.segRecords.data;
        var selectedHeaderData = data[sectionIndex][0];

        if (selectedHeaderData["imgExpandCollapse"] === self.Icons.dropdownCollapse) {
          selectedHeaderData["imgExpandCollapse"] = self.Icons.dropdownExpand;
          data[sectionIndex][1] = [];
          self.view.segRecords.setData(data);
        } else {
          selectedHeaderData["imgExpandCollapse"] = self.Icons.dropdownCollapse;
          data[sectionIndex][1] = self.segmentData[sectionIndex][1];
          self.view.segRecords.setData(data);
        }
        this.setDropdownHeight();

      }catch (err) {
        var errorObj = {
          "errorInfo": "Error in rowExpandCollapse",
          "errorLevel": "Configuration",
          "error": err
        };
        self.onError(errorObj);
      }
    },
	    checkforBuisnessAccount : function(data){
      var flag = false;
      for(var i=0;i<data.length;i++){
        if(!(this.isNullOrUndefinedOrEmpty(data[i]["isBusinessAccount"]))){
          if(data[i]["isBusinessAccount"] == "true"){
            flag  = true;
            return flag;
          }
        }
      }
      return flag;
    },
    hasMixAccounts : function(data){
      var businessAccountFlag = false;
      var personalAccountFlag = false;
      for(var i=0;i<data.length;i++){
        if(!(this.isNullOrUndefinedOrEmpty(data[i]["isBusinessAccount"]))){
          if(data[i]["isBusinessAccount"] == "true"){
            businessAccountFlag  = true;
          }else{
            personalAccountFlag = true;
          }
        }
      }
      if(businessAccountFlag && personalAccountFlag){
        return true;
      }else{
        return false;
      }
    }
  };
});