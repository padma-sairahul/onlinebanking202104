define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for AutoComplete **/
    AS_FlexContainer_b227a7597b9943678492f5ada9c459dc: function AS_FlexContainer_b227a7597b9943678492f5ada9c459dc(eventobject) {
        var self = this;
        return self.preShow.call(this);
    },
    /** onBreakpointChange defined for AutoComplete **/
    AS_FlexContainer_b346436df61e42ab82ac58166ea9ca55: function AS_FlexContainer_b346436df61e42ab82ac58166ea9ca55(eventobject, breakpoint) {
        var self = this;
        return self.onBreakPointChange.call(this);
    }
});