define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for foreignExchange **/
    AS_FlexContainer_d514bf68a727459ea62653765aa63a7c: function AS_FlexContainer_d514bf68a727459ea62653765aa63a7c(eventobject) {
        var self = this;
        return self.preShow.call(this);
    },
    /** postShow defined for foreignExchange **/
    AS_FlexContainer_b001584dbed248f08805896a6961f547: function AS_FlexContainer_b001584dbed248f08805896a6961f547(eventobject) {
        var self = this;
        return self.postShow.call(this);
    }
});