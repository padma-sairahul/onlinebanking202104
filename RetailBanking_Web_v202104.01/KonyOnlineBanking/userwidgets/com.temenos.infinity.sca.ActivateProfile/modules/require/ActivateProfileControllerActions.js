define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for ActivateProfile **/
    AS_FlexContainer_d662c2a667604cc48559eaa5c8b7fba6: function AS_FlexContainer_d662c2a667604cc48559eaa5c8b7fba6(eventobject) {
        var self = this;
        return self.preShow.call(this);
    },
    /** onBreakpointChange defined for ActivateProfile **/
    AS_FlexContainer_b568d050bc0c46deb4d14d37ba8a686d: function AS_FlexContainer_b568d050bc0c46deb4d14d37ba8a686d(eventobject, breakpoint) {
        var self = this;
        return self.onBreakpointChange.call(this, breakpoint);
    }
});