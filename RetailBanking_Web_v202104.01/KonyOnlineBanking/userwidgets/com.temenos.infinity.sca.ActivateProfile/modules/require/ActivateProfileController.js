define(function() {

  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {
      this._breakpoints = "";
      this._headingtext  ="";
      this._descriptiontext="";

      this.currentBreakpoint=1366;
    },
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {
      defineSetter(this, "breakpoints", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._breakpoints=val;
        }
      });
      defineGetter(this, "breakpoints", function() {
        return this._breakpoints;
      });
      defineSetter(this, "headingtext", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._headingtext=val;
        }
      });
      defineGetter(this, "headingtext", function() {
        return this._headingtext;
      });
      defineSetter(this, "descriptiontext", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._descriptiontext=val;
        }
      });
      defineGetter(this, "descriptiontext", function() {
        return this._descriptiontext;
      });
    },
    
    preShow : function(){
      this.assignDefaultText();
    },
    
    assignDefaultText: function(){
      this.view.lblBlueAlertText.text = this._descriptiontext;
      this.view.lblActivateProfile.text= this._headingtext;
    },
    
    onBreakpointChange : function(breakpoint){      
      this.currentBreakpoint = breakpoint;      
      if (breakpoint <= 640) {
        this.setMobileView();
      } else if (breakpoint <= 1024) {
        this.setTabletView();
      } else {
        this.setDesktopView();
      }
      this.view.forceLayout();
    },
    
    setMobileView: function(){
      this.view.flxActivateProfileQRScan.top = "100dp";      
      this.view.flxActivateProfileQRScan.left = "16dp";
      this.view.flxActivateProfileQRScan.right = "16dp";
      
      this.view.lblActivateProfile.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
      this.view.lblActivateProfile.centerX = "";
      this.view.lblActivateProfile.skin = "bbSknLbl424242SSP13Px";      
      this.view.lblActivateProfile.width = "225px";
      
      this.view.flxAlertText.top = "21dp";
      this.view.flxAlertTextInner.layoutType = kony.flex.FLOW_HORIZONTAL;
      this.view.imgBlueAlert.centerX = "";
      this.view.lblBlueAlertText.centerX = "";
      this.view.lblBlueAlertText.top = "0px";
      this.view.lblBlueAlertText.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
      this.view.lblBlueAlertText.skin = "sknBBLabelSSP42424211px";
      this.view.lblBlueAlertText.width = "218px";
      
      this.view.flxQRScanner.top = "33px";
      this.view.flxQRScanner.height = "134px";
      
      this.view.lblCallUs.bottom = "16px";
      this.view.lblCallUs.skin = "bbSknLbl424242SSP13Px";
    },
    
    setTabletView: function(){
      this.view.flxActivateProfileQRScan.top = "158dp";    
      this.view.flxActivateProfileQRScan.left = "102dp";
      this.view.flxActivateProfileQRScan.right = "102dp";
            
      this.view.lblActivateProfile.contentAlignment = constants.CONTENT_ALIGN_CENTER;
      this.view.lblActivateProfile.centerX = "50%";
      this.view.lblActivateProfile.skin = "bbSknLbl424242SSP20Px";
      this.view.lblActivateProfile.width = "327";
      
      this.view.flxAlertText.top = "16dp";
      this.view.flxAlertTextInner.layoutType = kony.flex.FLOW_VERTICAL;
      this.view.imgBlueAlert.centerX  = "50%";
      this.view.lblBlueAlertText.centerX = "50%";
      this.view.lblBlueAlertText.top = "8px";
      this.view.lblBlueAlertText.contentAlignment = constants.CONTENT_ALIGN_CENTER;
      this.view.lblBlueAlertText.skin = "bbSknLbl424242SSP13Px";
      this.view.lblBlueAlertText.width = "318px";
      
      this.view.flxQRScanner.top = "30px";
      this.view.flxQRScanner.height = "206px";
      
      this.view.lblCallUs.bottom = "23px";
      this.view.lblCallUs.skin = "bbSknLbl424242SSP15Px";
    },
    
    setDesktopView: function(){
      this.view.flxActivateProfileQRScan.top = "46dp";
      this.view.flxActivateProfileQRScan.left = "74dp";
      this.view.flxActivateProfileQRScan.right = "74dp";
      
      this.view.lblActivateProfile.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
      this.view.lblActivateProfile.centerX = "";
      this.view.lblActivateProfile.skin = "bbSknLbl424242SSP20Px";
      this.view.lblActivateProfile.width = "327px";
      
      this.view.flxAlertText.top = "35dp";
      this.view.flxAlertTextInner.layoutType = kony.flex.FLOW_HORIZONTAL;
      this.view.imgBlueAlert.centerX = "";
      this.view.lblBlueAlertText.centerX = "";
      this.view.lblBlueAlertText.top = "0px";
      this.view.lblBlueAlertText.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
      this.view.lblBlueAlertText.skin = "bbSknLbl424242SSP13Px";
      this.view.lblBlueAlertText.width = "318px";
      
      this.view.flxQRScanner.top = "75px";
      this.view.flxQRScanner.height = "206px";
      
      this.view.lblCallUs.bottom = "120px";
      this.view.lblCallUs.skin = "bbSknLbl424242SSP15Px";
    },
  };
});