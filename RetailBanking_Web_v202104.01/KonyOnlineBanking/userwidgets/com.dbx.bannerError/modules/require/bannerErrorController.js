define(function () {
	var skins = {
		labelDW: "lblTitle15",
		labelM: "lblTitle13"
	};
	return {
		preShow : function(){
			this.view.flxClose.onClick = this.hideError;
		},
		onBreakpointChange: function (width) {
			if (width <= 640) {
				this.view.height = "60dp";
				this.view.imgError.left = "10dp";
				this.view.imgError.height = "30dp";
				this.view.imgError.width = "30dp";
				this.view.lblError.skin = skins.labelM;
				this.view.lblError.left = "50dp";
				this.view.lblError.right = "45dp";
				this.view.flxClose.width = "30dp";
			} else {
				this.view.height = "70dp"
				this.view.imgError.left = "20dp";
				this.view.imgError.height = "40dp";
				this.view.imgError.width = "40dp";
				this.view.lblError.skin = skins.labelDW;
				this.view.lblError.left = "75dp";
				this.view.lblError.right = "65dp";
				this.view.flxClose.width = "40dp";
			}
		},
		showError : function(msg){
			this.view.lblError.text = msg;
			this.view.parent.setVisibility(true);
		},
		hideError : function(){
			this.view.parent.setVisibility(false);
		}
	};
});