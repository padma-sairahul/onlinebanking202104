define(['./watchlistDAO','./ParserUtilsManager','./FormatUtils','CommonUtilities', './CacheUtils','./ViewConstants'],function(watchlistDAO,ParserUtilsManager,FormatUtils,CommonUtilities,CacheUtils,ViewConstants) {

  return {

    constructor: function(baseConfig, layoutConfig, pspConfig) {
      //custom properties
      this.accountsListObj = {},
      this.portFolioId = "",
      this.instruData = {};
      this.offset = "0";
      this.noOfRecords = "10";
      this._objService="";
      this._objName="";
      this._operation="";
      this._GAcriteria="";
      this.parserUtilsManager = new ParserUtilsManager();
      this.watchlistDAO = new watchlistDAO();
      this.FormatUtils = new FormatUtils();
      this.selectedRadioOptionText = "";
      this.selectedRadioOption = "";
      this.prefix = "";
      this._criteria = {};
      this._GAobjectServiceName="";
      this._GAobjectName="";
      this._GAoperationName="";
      this.sortData="instrumentName";
      this.sortBy = "instrumentName";
      this.sorting= "asc";
      this.sortType="asc";
      this.sortNone=ViewConstants.IMAGES.SORTING;
      this.sortAsc=ViewConstants.IMAGES.SORTING_PREVIOUS;
      this.sortDesc=ViewConstants.IMAGES.SORTING_NEXT;
      this.view.imgSortInstrument.onTouchEnd = this.onClickSort.bind(this, 0, this.view.imgSortInstrument);
      this.view.imgSortCurrency.onTouchEnd = this.onClickSort.bind(this, 1, this.view.imgSortCurrency);
      this.view.imgSortLatest.onTouchEnd = this.onClickSort.bind(this, 2, this.view.imgSortLatest);
      this.view.imgSortChange.onTouchEnd = this.onClickSort.bind(this, 3, this.view.imgSortChange);
      this.view.imgSortDateTime.onTouchEnd = this.onClickSort.bind(this, 4, this.view.imgSortDateTime);
      this.view.imgSortBid.onTouchEnd = this.onClickSort.bind(this, 5, this.view.imgSortBid);
      this.view.imgSortAsk.onTouchEnd = this.onClickSort.bind(this, 6, this.view.imgSortAsk);
      this.view.imgSortVolume.onTouchEnd = this.onClickSort.bind(this, 7, this.view.imgSortVolume);
      // for tablet
      this.view.imgSortTabletInstrument.onTouchEnd = this.onClickSort.bind(this, 0, this.view.imgSortTabletInstrument);
      this.view.imgSortTabletCurrency.onTouchEnd = this.onClickSort.bind(this, 1, this.view.imgSortTabletCurrency);
      this.view.imgSortTabletLatest.onTouchEnd = this.onClickSort.bind(this, 2, this.view.imgSortTabletLatest);
      this.view.imgSortTabletChange.onTouchEnd = this.onClickSort.bind(this, 3, this.view.imgSortTabletChange);
      this.view.imgSortTabletDateTime.onTouchEnd = this.onClickSort.bind(this, 4, this.view.imgSortTabletDateTime);
      this.view.imgSortTabletBid.onTouchEnd = this.onClickSort.bind(this, 5, this.view.imgSortTabletBid);
      this.view.imgSortTabletAsk.onTouchEnd = this.onClickSort.bind(this, 6, this.view.imgSortTabletAsk);
      this.view.imgSortTabletVolume.onTouchEnd = this.onClickSort.bind(this, 7, this.view.imgSortTabletVolume);

      defineSetter(this, 'objService', function (val) {
        if (typeof val === 'string' && val !== '') {
          this._objService = val;
        }
      });
      defineGetter(this, 'objService', function () {
        return this._objService;
      });
      defineSetter(this, 'objName', function (val) {
        if (typeof val === 'string' && val !== '') {
          this._objName = val;
        }
      });
      defineGetter(this, 'objName', function () {
        return this._objName;
      });
      defineSetter(this, 'operation', function (val) {
        if (typeof val === 'string' && val !== '') {
          this._operation = val;
        }
      });
      defineGetter(this, 'operation', function () {
        return this._operation;
      });
      defineSetter(this, 'criteria', function (val) {
        if (typeof val === 'string' && val !== '') {
          this._criteria = val;
        }
      });
      defineGetter(this, 'criteria', function () {
        return this._criteria;
      });
      defineSetter(this, 'GAobjectServiceName', function (val) {
        if (typeof val === 'string' && val !== '') {
          this._GAobjectServiceName = val;
        }
      });
      defineGetter(this, 'GAobjectServiceName', function () {
        return this._GAobjectServiceName;
      });
      defineSetter(this, 'GAobjectName', function (val) {
        if (typeof val === 'string' && val !== '') {
          this._GAobjectName = val;
        }
      });
      defineGetter(this, 'GAobjectName', function () {
        return this._GAobjectName;
      });
      defineSetter(this, 'GAoperationName', function (val) {
        if (typeof val === 'string' && val !== '') {
          this._GAoperationName = val;
        }
      });
    },
    makeDaoDeleteWatchlist: function(){
      try{
        let objectName = "FavouriteInstruments";
        let objectServiceName = "WealthObjects";
        let operationName = "updateUserFavouriteInstruments";
        let serviceResponseIdentifier = "S1";
        var criteria = {
          "RICCode": this.instruData.RICCode,
          "instrumentId": this.instruData.instrumentId,
          "operation": "Remove"
        };

        this.watchlistDAO.updateUserFavouriteInstruments(objectServiceName,operationName,objectName,criteria,serviceResponseIdentifier,this.onDeleteServiceSuccess,this.onError);
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in making service call.",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);
      }
    },

    makeDaoCallWatchList: function(){
      try{
        let objectName = this.getFieldValue(eval("this._objName"));
        let objectServiceName = this.getFieldValue(eval("this._objService"));
        let operationName = this.getFieldValue(eval("this._operation"));
        let serviceResponseIdentifier = "S1";
        var criteria = {
          "sortBy" : this.sortData,
          "sortOrder" : this.sorting,
          "pageOffset": this.offset,
          "pageSize": this.noOfRecords,
          "portfolioId": this.portFolioId
        };

        this.watchlistDAO.fetchDetails(objectServiceName,operationName,objectName,criteria,serviceResponseIdentifier,this.onServiceSuccess,this.onError);
        this.requestStart();
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in making service call.",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);
      }
    },
    onDeleteServiceSuccess: function(response){
      var data = response.favoriteInstruments;
      this.makeDaoCallWatchList();
      this.view.watchlistActionsMenu.setVisibility(false);
    },
    onServiceSuccess: function(response){
      var data = response.favoriteInstruments;
      this.bindWatchlistInstruments(data);
      this.view.txtSear.onDone = this.onSearchBtnClick.bind(this, data);
      this.view.imgSear.onTouchEnd = this.onSearchBtnClick.bind(this, data);
      this.view.watchlistActionsMenu.segWatchlistActionsMenu.onRowClick = this.onRowclickWatchlistActions;
      this.view.watchlistActionsMenuTablet.segWatchlistActionsMenu.onRowClick = this.onRowclickWatchlistActions;
      this.view.paginationFooter.updatePaginationBar(Number(this.offset),Number(response.totalCount));
      this.requestEnd();
    },
    preShow:function(){  
      this.criteria = {};
      var scope = this;
      this.sortData = "instrumentName";
      this.sorting = "asc";
      this.view.paginationFooter.resetStartIndex();
      this.view.paginationFooter.collapseDropDown();
      scope.initActions();

    },
    postShow:function(){  
      this.checkPermission();
      this.view.paginationFooter.height = 100;  
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      this.accountsListObj = wealthModule.getAccountsListObj();
      var portoId = this.accountsListObj[0].portfolioId;      
      if(wealthModule.getPortfolioId()){
        this.portFolioId = wealthModule.getPortfolioId();
      }else{
        wealthModule.setPortfolioId(portoId);
        this.portFolioId = portoId;
      }
    },
    getTabServiceIdentifier: function(){
      var serIdentifier;
      if(this.prefix === "Three"){
        if(this.selectedRadioOption !== ""){
          serIdentifier = this.getFieldValue(eval("this._tab"+this.prefix+this.selectedRadioOption+"Identifier"));
        }
      }
      else{
        serIdentifier = this.getFieldValue(eval("this._tab"+this.prefix+"Identifier"));
      }
      return serIdentifier;
    },

    setCriteriaBasedOnTab: function(){

      var criterion; var pref ;
      if(this.prefix == "Three"){
        if(this.selectedRadioOption !== ""){
          criterion = this.getFieldValue(eval("this._tab"+this.prefix+this.selectedRadioOption+"Criteria"));
          pref = this.prefix+this.selectedRadioOption;
        }
      }
      else{
        criterion = this.getFieldValue(eval("this._tab"+this.prefix+"Criteria"));
        pref = this.prefix;
      }
      this.setCriteria(criterion,pref);
    },

    makeDaoCallForPrintDownload: function(params){
      var self =this;
      try{
        let objectName = this.getFieldValue(eval("this._GAobjectName"));
        let objectServiceName = this.getFieldValue(eval("this._GAobjectServiceName"));
        let operationName = this.getFieldValue(eval("this._GAoperationName"));
        let serviceResponseIdentifier = this.getTabServiceIdentifier() || "S1";
        var criteria = {
          "navPage": "Watchlist",
          "sortBy": this.sortData,
          "sortOrder": this.sorting,
          "pageOffset": this.offset,
          "pageSize": this.noOfRecords,
          "portfolioId": this.portFolioId
        };
        if(params==="Print")
        {
          this.watchlistDAO.fetchDetails(objectServiceName,operationName,objectName,criteria,serviceResponseIdentifier,this.onSuccessPrint,this.onError);
        }
        else{
          this.watchlistDAO.fetchDetails(objectServiceName,operationName,objectName,criteria,serviceResponseIdentifier,this.onSuccessDownloadWl,this.onError);
        }
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in making service call.",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);
      }
    },  
    /**
     * Component getCriteria
     * Parse the criteria from configuration
     * @return : {JSONObject} - jsonvalue for criteria
     */
    getCriteria:function(){
      var self = this;
      try{
        return this.criteria;
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in returning criteria",
              "errorLevel" : "Configuration",
              "error": err
            };
        self.onError(errorObj);
      }
      return "";
    },
    //onSuccessDownloadWl
    onSuccessDownloadWl: function(response,unicode){
      const linkSource = `data:application/pdf;base64,${response.base64}`;
      const downloadLink = document.createElement("a");
      const fileName = "Watchlist.pdf";
      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
    },
    // method which converts base64 to Blob
    b64toBlob:function(content,contentType){
      contentType = contentType || '';
      const sliceSize = 512;
      // method which converts base64 to binary
      const byteCharacters = window.atob(content); 
      const byteArrays = [];
      for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);
        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }
      const blob = new Blob(byteArrays, {
        type: contentType
      }); // statement which creates the blob
      return blob;
    },
    //PrintPreview
    printPreview:function(data,type){
      let blob = null;
      blob = this.b64toBlob(data, type);
      const blobURL = URL.createObjectURL(blob);
      const theWindow = window.open(blobURL);
      const theDoc = theWindow.document;
      const theScript = document.createElement('script');
      function injectThis() {
        window.onafterprint = window.close;
        kony.os.print();
      }
      theScript.innerHTML = `window.onload = ${injectThis.toString()};`;
      theDoc.body.appendChild(theScript);
    },

    //OnSuccessprint
    onSuccessPrint: function(response,unicode) {	
      this.printPreview(response.base64,'application/pdf');
    }, 

    initActions: function(){
      var self = this;
      try
      {
        this.view.imgDownload.onTouchEnd=this.watchlistDownload;
        this.view.imgPrint.onTouchEnd=this.watchlistPrint;
        this.makeDaoCallWatchList();
        this.view.paginationFooter.fetchPaginatedRecords=this.footerPage;

      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in setting the actions to columns.",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);
      }
    },
    onSearchBtnClick: function(dataList) {
      var scopeObj = this;
      var data1 = scopeObj.getSearchData(dataList);
      this.bindWatchlistInstruments(data1);
      scopeObj.view.forceLayout();
    },
    //watchlistDownload
    watchlistDownload: function(){
      this.makeDaoCallForPrintDownload("Download");
    },

    //watchlistPrint
    watchlistPrint: function(){
      this.makeDaoCallForPrintDownload("Print");
    },

    getAttachments: function() {
      var self =this;
      try
      {
        this.requestStart();
        var criteriaObject = JSON.parse(this._GAcriteria);
        for(var key in  criteriaObject){
          criteriaObject[key] = this.getFieldValue(criteriaObject[key]);
        }
        if(this._GAobjectServiceName && this._GAobjectName && this._GAoperationName){
          this.transactionListDAO.fetchAttachments(this._GAobjectServiceName,this._GAoperationName,this._GAobjectName,criteriaObject,this.setRowData,this.setRowData);
        }
        else{
          this.setRowData();
        }    
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in doing service call to  Get attachments",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);
      }
    },
    /**
     * Component setCriteria
     * Update the criteria based on accountType ans filter
     * criteria {string} - value collected from exposed contract
     */
    setCriteria:function(criteria, pref){
      var self = this;
      try
      {
        var criteriaObject = JSON.parse(eval("this._tab"+pref+"Criteria"));
        for(var key in  criteriaObject){
          criteriaObject[key] = this.getFieldValue(criteriaObject[key]);
        }
        var criteriaJSON = criteria;
        if(typeof(criteria) == "string"){
          criteriaJSON = JSON.parse(criteria);
        }
        for(var key in  criteriaJSON){
          criteriaObject[key] = this.getFieldValue(criteriaJSON[key]);
        }
        this.criteria = criteriaObject;

        if(this.criteria["searchByInstrumentName"] ==  "searchByInstrumentName"){
          delete this.criteria["searchByInstrumentName"];
        }
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in setting the criteria",
              "errorLevel" : "Configuration",
              "error": err
            };
        this.onError(errorObj);
      }
    },
    /**
     * Component getFieldValue
     * Parse the exposed contract value based on accountType selected and breakpoint consideration
     * @param: Value{string} - value collected from exposed contract
     * @param: key{string} - lookup key in the JSON string
     * @return : {string} - Processed value
     */
    getFieldValue: function(Value,key) {
      try 
      {
        var value = Value;
        if(typeof(Value) === "string"){
          value = JSON.parse(Value);
        }
        if(value[this.accountType]){
          value = value[this.accountType];
        }
        if(!this.isEmptyNullUndefined(value) && !this.isEmptyNullUndefined(key)){
          value = value[key];
        }
        if (value !== null && value !== "" && value !== undefined) {
          if(typeof(value)==="string")
            return this.getProcessedText(value);
          else{
            var text=this.breakPointParser(value,kony.application.getCurrentBreakpoint());
            return this.getProcessedText(text);
          }
        } else return "";
      }  
      catch(err)
      {
        kony.print(err);
      }
      return this.getProcessedText(Value);
    },

    /**
     * Component breakPointParser
     * Helper method to parse the exposed contract based on the current breakpoint
     * inputJSON {JSONObject} - object containing information about various breakpoints and associated texts
     * lookUpKey {string}     - current breakpoint value to be looked upon the above object
     * @return : value of the lookup key in the input object
     */
    breakPointParser:function(inputJSON,lookUpKey){
      var self = this;
      try
      {
        if(inputJSON.hasOwnProperty(lookUpKey)){
          return inputJSON[lookUpKey];
        }
        else if(inputJSON.hasOwnProperty("default")){
          return inputJSON["default"];
        }
        return inputJSON;
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in parsing th break point",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);
      }
    },

    /**
     * Component getProcessedText
     * Pass the text to format util to obtained the processed value.
     * text {string} - value to be processed
     * @return : {string} - processed value
     */
    getProcessedText:function(text){
      return this.parserUtilsManager.getParsedValue(text);
    },
    /**
     * Component isEmptyNullUndefined
     * Verifies if the value is empty, null or undefined
     * data {string} - value to be verified
     * @return : {boolean} - validity of the value passed
     */
    isEmptyNullUndefined:function(data){
      if(data === null || data === undefined || data === "")
        return true;
      return false;
    },
    onError: function(errorObj){
      // error fetch
    },
    getSearchData: function(dataList1) {
      var scopeObj = this; 
      var searchQuery = scopeObj.view.txtSear.text.trim();
      if (searchQuery !== "") {
        var data2 = dataList1;
        var searchresults = [];
        if (!kony.sdk.isNullOrUndefined(searchQuery) && searchQuery !== "") {
          var j = 0;
          for (var i = 0; i < data2.length; i++) {
            var rowdata = null;
            if ((data2[i].instrumentName && data2[i].instrumentName.toUpperCase().indexOf(searchQuery.toUpperCase()) !== -1) ||
                (data2[i].ISINCode && data2[i].ISINCode.toUpperCase().indexOf(searchQuery.toUpperCase()) !== -1))
            {
              rowdata = data2[i];
            }
            if (kony.sdk.isNullOrUndefined(rowdata)) {
              data2[i].isNoRecords = true;
              data2[i].lblNoResultsFound = {
                "text": kony.i18n.getLocalizedString("i18n.FastTransfers.NoResultsFound")
              };
              var noRecordsData = data2[i];
              if (data2[i].isNoRecords === false) {
                searchresults[j].push(noRecordsData);
                j++;
              }
            } else {
              searchresults[j] = rowdata;
              j++;
            }
          }
        }
        return searchresults;
      } else {
        return dataList1;
      }
    },
    onRowclickWatchlistActions: function() {
      var scopeObj = this;
      var data = {
        "searchByInstrumentName": this.instruData.lblInstruName,
        "portfolioId": this.portFolioId,
        "sortBy": "",
        "navPage": "Watchlist",
        "ISIN": this.instruData.ISINCode?this.instruData.ISINCode:'',
        "RICCode":this.instruData.RICCode?this.instruData.RICCode:'',
        "instrumentId": this.instruData.instrumentId?this.instruData.instrumentId:''
      };
      if (kony.application.getCurrentBreakpoint() > 1024) {
        var action = scopeObj.view.watchlistActionsMenu.segWatchlistActionsMenu.selectedRowItems[0];
        if (action.lblAction === "View") {
          scope_WealthPresentationController.instrumentAction = 'Watchlist';
          applicationManager.getModulesPresentationController("WealthModule").getProductDetailsById(data);
          applicationManager.getModulesPresentationController("WealthModule").getHoldings(data);
          this.view.watchlistActionsMenu.setVisibility(false);
        } else if (action.lblAction === "Delete") {
          this.makeDaoDeleteWatchlist();
          this.view.forceLayout();
        } else if (action.lblAction === "Buy") {
          data.operation = "Buy";
          applicationManager.getNavigationManager().setCustomInfo('frmPlaceOrder', data);
          applicationManager.getNavigationManager().navigateTo('frmPlaceOrder');
          this.view.watchlistActionsMenu.setVisibility(false);
        } else if (action.lblAction === "Sell") {
          data.operation = "Sell";
          applicationManager.getNavigationManager().setCustomInfo('frmPlaceOrder', data);
          applicationManager.getNavigationManager().navigateTo('frmPlaceOrder');
          this.view.watchlistActionsMenu.setVisibility(false);          
        }
      } else {
        var actionTablet = scopeObj.view.watchlistActionsMenuTablet.segWatchlistActionsMenu.selectedRowItems[0];
        if (actionTablet.lblAction === "View") {
          scope_WealthPresentationController.instrumentAction = 'Watchlist';
          applicationManager.getModulesPresentationController("WealthModule").getProductDetailsById(data);
          applicationManager.getModulesPresentationController("WealthModule").getHoldings(data);        
          this.view.watchlistActionsMenuTablet.setVisibility(false);
        } else if (actionTablet.lblAction === "Delete") {
          this.makeDaoDeleteWatchlist();
          this.view.watchlistActionsMenuTablet.setVisibility(false);
          this.view.forceLayout();
        } else if (actionTablet.lblAction === "Buy") {
          data.operation = "Buy";
          applicationManager.getNavigationManager().setCustomInfo('frmPlaceOrder', data);
          applicationManager.getNavigationManager().navigateTo('frmPlaceOrder');
          this.view.watchlistActionsMenuTablet.setVisibility(false);
        } else if (actionTablet.lblAction === "Sell") {
          data.operation = "Sell";
          applicationManager.getNavigationManager().setCustomInfo('frmPlaceOrder', data);
          applicationManager.getNavigationManager().navigateTo('frmPlaceOrder');
          this.view.watchlistActionsMenuTablet.setVisibility(false);
        }
      }
    },
    bindWatchlistInstruments: function(data) {
      if (data === 0 || data === undefined) {
        this.view.segWatchlist.removeAll();
        this.view.segWatchlistTabletInstrument.removeAll();
        this.view.segWatchlistTabletRemaining.removeAll();
        this.view.flxHeadings.setVisibility(false);
        this.view.flxSeparatorHeadings.setVisibility(false);
        this.view.flxExtendedWatchlistWrapperTablet.setVisibility(false);
        this.view.flxNoResults.setVisibility(true);
      } else {
        this.view.flxNoResults.setVisibility(false);
        scopeObj = this;
        var widgetDataMap = {
          "lblInstruName": "lblInstruName",
          "lblRICCode": "lblRICCode",
          "lblInstrumentId": "lblInstrumentId",
          "lblISIN": "lblISIN",
          "lblCurrency": "lblCurrency",
          "lblLatest": "lblLatest",
          "lblChange": "lblChange",
          "lblDateTime": "lblDateTime",
          "lblBid": "lblBid",
          "lblAsk": "lblAsk",
          "lblVolume": "lblVolume",
          "flxDots": "flxDots",
          "img3Dot": "img3Dot",
          "flxWatchlistExtended": "flxWatchlistExtended"
        };
        var formUtilityMan = applicationManager.getFormatUtilManager();	
        var watchlistSegmentData = data.map(function(itemData) {
          var amnLatest = formUtilityMan.formatAmountandAppendCurrencySymbol(itemData.lastRate,itemData.referenceCurrency);
          var amnBid = "0.00";    
          if (itemData.bidRate !== ""){   
             amnBid = formUtilityMan.formatAmountandAppendCurrencySymbol(itemData.bidRate,itemData.referenceCurrency);
          }
          var amnAsk = "0.00";    
          if (itemData.askRate !== "") {
             amnAsk = formUtilityMan.formatAmountandAppendCurrencySymbol(itemData.askRate,itemData.referenceCurrency);
          }
          var percentChange = itemData.percentageChange;
          if (percentChange === "") {
            percentChange = "0.00";
          }
          var objectData = {
            "lblInstruName": itemData.instrumentName,
            "lblRICCode": itemData.RIC,
            "lblInstrumentId": itemData.instrumentId,
            "lblISIN": itemData.ISINCode + " | " + itemData.exchange,
            "lblCurrency": itemData.referenceCurrency,
            "lblLatest": amnLatest,
            "lblChange": {
              "text": itemData.percentageChange.includes("-") ? percentChange + "%" : "+" + percentChange + "%",
              "skin": itemData.percentageChange.includes("-") ? "sknLblChange0055IW15px" : "sknLblChange15px"
            },
            "lblDateTime": itemData.dateReceived + ' at: ' + itemData.timeReceived,
            "lblBid": amnBid,
            "lblAsk": amnAsk,
            "lblVolume": itemData.volume.toString(),
            "img3Dot": {
              "text": ViewConstants.FONT_ICONS.THREE_DOTS_ACCOUNTS,
              "skin": ViewConstants.SKINS.THREE_DOTS_IMAGE,
              "isVisible": true,
            },
            "flxWatchlistExtended": "flxWatchlistExtended",
            "flxDots": {
              "onClick": function() {
                scopeObj.toggleWatchlistActions(itemData);
              }
            }
          };
          return objectData;
        });
        scopeObj.view.segWatchlist.widgetDataMap = widgetDataMap;
        if (data.length > 0) {
          if (kony.application.getCurrentBreakpoint() > 1024) {       
            this.view.flxWatchlistSegmentTablet.setVisibility(false);
            this.view.flxExtendedWatchlistWrapper.setVisibility(true);
            scopeObj.view.segWatchlist.setData(watchlistSegmentData);           
          } else {
            this.view.flxExtendedWatchlistWrapper.setVisibility(false);
            this.view.flxHeadings.setVisibility(false);
            this.view.flxSeparatorHeadings.setVisibility(false);
            this.view.flxWatchlistSegmentTablet.setVisibility(true);
            this.view.segWatchlistTabletInstrument.setData(watchlistSegmentData);
            this.view.segWatchlistTabletRemaining.setData(watchlistSegmentData);
          }
        }
        scopeObj.view.forceLayout();
      }
    },
    toggleWatchlistActions: function() {
      if (kony.application.getCurrentBreakpoint() > 1024) {
        var rowSeg = scopeObj.view.segWatchlist.selectedRowItems[0];
        var formUtilityMan = applicationManager.getFormatUtilManager();
        rowSeg.latestPrice = Number(formUtilityMan.deFormatAmount(rowSeg.lblLatest));
        scope_WealthPresentationController.selectedInstrDetails = rowSeg;        
        this.instruData = {
          "ISINCode": rowSeg.lblISIN,
          "lblInstruName": rowSeg.lblInstruName,
          "RICCode": rowSeg.lblRICCode,
          "instrumentId": rowSeg.lblInstrumentId
        };
        var row = this.view.segWatchlist.selectedRowIndex[1];
        this.view.watchlistActionsMenu.top = 231 + row * 62 + "dp";
        var toggleActions = this.view.watchlistActionsMenu.isVisible ? false : true;
        this.view.watchlistActionsMenu.setVisibility(toggleActions);
      } else {
        var rowSegTablet = scopeObj.view.segWatchlistTabletRemaining.selectedRowItems[0];
        this.formUtilityMan = applicationManager.getFormatUtilManager();
        rowSegTablet.latestPrice = Number(this.formUtilityMan.deFormatAmount(rowSegTablet.lblLatest));
        scope_WealthPresentationController.selectedInstrDetails = rowSegTablet;
        this.instruData = {
          "ISINCode": rowSegTablet.lblISIN,
          "lblInstruName": rowSegTablet.lblInstruName,
          "RICCode": rowSegTablet.lblRICCode,
          "instrumentId": rowSegTablet.lblInstrumentId
        };
        var rowTablet = this.view.segWatchlistTabletRemaining.selectedRowIndex[1];
        this.view.watchlistActionsMenuTablet.top = 231 + rowTablet * 50 + "dp";
        var toggleActionsTablet = this.view.watchlistActionsMenuTablet.isVisible ? false : true;
        this.view.watchlistActionsMenuTablet.setVisibility(toggleActionsTablet);
      }
    },
    onClickSort: function(option, widget) {
      switch(option) {
        case 0:
          this.sortData = "instrumentName";
          break;
        case 1:
          this.sortData = "referenceCurrency";
          break;
        case 2:
          this.sortData = "lastRate";
          break;
        case 3:
          this.sortData = "percentageChange";
          break;
        case 4:
          this.sortData = "dateTime";
          break;
        case 5:
          this.sortData = "bidRate"; 
          break;          
        case 6:
          this.sortData = "askRate";
          break;
        case 7:
          this.sortData = "volume";
          break;          
        default:
      }
      this.setImage(option, widget);
      this.sorting = this.sortType;
      this.refreshSegment(this.sorting);
    },
    setImage: function(option, widget) {
      var tempSrc;
      if (widget.src === this.sortNone) {
        tempSrc =  this.sortAsc;
        this.sortType = "asc";
      } else if (widget.src === this.sortAsc) {
        tempSrc = this.sortDesc;
        this.sortType = "desc";
      } else {
        tempSrc = this.sortAsc;
        this.sortType = "asc";
      }
      this.resetWidgetsrc();
      widget.src = tempSrc;
    },
    resetWidgetsrc: function(){
      this.view.imgSortInstrument.src = this.sortNone;
      this.view.imgSortCurrency.src = this.sortNone;
      this.view.imgSortLatest.src = this.sortNone;
      this.view.imgSortChange.src = this.sortNone;
      this.view.imgSortDateTime.src = this.sortNone;
      this.view.imgSortBid.src = this.sortNone;
      this.view.imgSortAsk.src = this.sortNone;
      this.view.imgSortVolume.src = this.sortNone;
      // tablet  
      this.view.imgSortTabletInstrument.src = this.sortNone;
      this.view.imgSortTabletCurrency.src = this.sortNone;
      this.view.imgSortTabletLatest.src = this.sortNone;
      this.view.imgSortTabletChange.src = this.sortNone;
      this.view.imgSortTabletDateTime.src = this.sortNone;
      this.view.imgSortTabletBid.src = this.sortNone;
      this.view.imgSortTabletAsk.src = this.sortNone;
      this.view.imgSortTabletVolume.src = this.sortNone;
    },//
    refreshSegment: function(){
      this.makeDaoCallWatchList();
    },
    footerPage:function(offset,limit){
      this.offset = offset;
      this.noOfRecords=limit;
      this.refreshSegment();
    },
    checkPermission: function() {
      var configManager =  applicationManager.getConfigurationManager();
      var checkUserPermission = function (permission) {
        return applicationManager.getConfigurationManager().checkUserPermission(permission);
      }; 
      let watchListSellPermission = configManager.sellOrderPermissions().some(checkUserPermission);
      let watchListBuyPermission = configManager.buyOrderPermissions().some(checkUserPermission);
      let watchListViewPermission = configManager.watchlistViewInstrumentPermissions().some(checkUserPermission);
      let watchListDeletePermission = configManager.addToWatchlistPermissions().some(checkUserPermission);

      var dataBuy =this.view.watchlistActionsMenu.segWatchlistActionsMenu.data[1];
      this.view.watchlistActionsMenu.segWatchlistActionsMenu.removeAt(1);
      var dataSell =this.view.watchlistActionsMenu.segWatchlistActionsMenu.data[1];
      this.view.watchlistActionsMenu.segWatchlistActionsMenu.removeAt(1); 
      var dataDel =this.view.watchlistActionsMenu.segWatchlistActionsMenu.data[1];
      this.view.watchlistActionsMenu.segWatchlistActionsMenu.removeAt(1); 
      var dataView =this.view.watchlistActionsMenu.segWatchlistActionsMenu.data[0];
      this.view.watchlistActionsMenu.segWatchlistActionsMenu.removeAt(0); 
      var posMenu = 0;
      if (watchListViewPermission === true) {
        this.view.watchlistActionsMenu.segWatchlistActionsMenu.addDataAt(dataView, posMenu);
        posMenu++;
      }
      if (watchListBuyPermission === true) {
        this.view.watchlistActionsMenu.segWatchlistActionsMenu.addDataAt(dataBuy, posMenu);
        posMenu++;
      } 	  
      if (watchListSellPermission === true) {
        this.view.watchlistActionsMenu.segWatchlistActionsMenu.addDataAt(dataSell, posMenu);
        posMenu++;
      }
      if (watchListDeletePermission === true) {
        this.view.watchlistActionsMenu.segWatchlistActionsMenu.addDataAt(dataDel, posMenu);
      }     
    }
  };
});