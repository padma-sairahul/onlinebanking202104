define(['HamburgerConfig'], function (HamburgerConfig) {
    var MENU_CONTAINER = "flxMenuWrapper";
    var SAMPLE_MENU = "flxAccountsMenu";
    var SAMPLE_SUB_MENU = "flxAccountsSubMenu";
    var SAMPLE_SUB_MENU_ITEM = "flxMyAccounts";
    var onItemSelectListener = null;
    var i18nMap = {};
    var orientationHandler = new OrientationHandler();
    return {

        initialized: false,
        actiavteWhenInitialized: null,

        /**
         * Sets onItemSelect Listener
         */
        setItemSelectListener: function (listener) {
            onItemSelectListener = listener;
        },
        

        /**
         * Activates the menu
         * @param {string} parentId of parent menu
         * @param {string} childId of parent menu
         */
        activateMenu: function (parentId, childId) {
            if (!this.initialized) {
                this.actiavteWhenInitialized = {
                    parentId: parentId,
                    childId: childId
                }
                return;
            }
            var parentIndex = 0;
            var menuObject = null;
            var visibleIndex = 1;  //One as offset for sample widget 
            HamburgerConfig.config.forEach(function (menuItem) {
                 if (!menuItem.isVisible || menuItem.isVisible()) {
                    if( menuItem.id.toLowerCase() === parentId.toLowerCase()) {
                        parentIndex = visibleIndex;
                        menuObject = menuItem;                        
                    }
                    visibleIndex++;
                 }
            });
            if (menuObject) {
              this.activeMenu = menuObject.id;
                var childIndex = -1;
                var visibleChildIndex = 1;   //One as offset for sample widget 
                var children = this.view[MENU_CONTAINER].widgets();
                this.collapseAll();
                this.resetSkins();
                this.expandWithoutAnimation(children[parentIndex*2 +1]);
                children[parentIndex*2].widgets()[2].widgets()[0].text = "P";
              children[parentIndex*2].widgets()[2].widgets()[0].accessibilityConfig = {
                "a11yLabel" :"Collapse "+menuObject.id+" Menu"
              }
              if(menuObject.subMenu.children.length === 0){
                children[parentIndex * 2].widgets()[2].widgets()[0].text = "Q";
                children[parentIndex * 2].widgets()[2].widgets()[0].width = "11dp";
                children[parentIndex * 2].widgets()[2].widgets()[0].height = "12dp";
              }
                if (childId) {
                    var childObject = null;
                     menuObject.subMenu.children.forEach(function (childItem) {
                         if (!childItem.isVisible || childItem.isVisible()) {
                            if (childItem.id.toLowerCase() === childId.toLowerCase()) {
                                childIndex = visibleChildIndex;
                                childObject = childItem;
                             }
                             visibleChildIndex ++;
                         }
                    });
                    if (childObject) {
                        children[parentIndex*2+1].widgets()[childIndex].skin = "skncursor";
                        children[parentIndex*2+1].widgets()[childIndex].hoverSkin = "skncursor";
                        children[parentIndex*2+1].widgets()[childIndex].widgets()[0].skin = "sknLblHamburgerSelected";
                    }
                }
            }
          
        },

        /**
         * EXpand WIthout Animation
         */
        expandWithoutAnimation: function (widget) {
            widget.height = (widget.widgets().length - 1) * 60;
            this.view.forceLayout();
        },

        /**
         * Resets the skin
         */
        resetSkins: function () {
            var subMenus = this.view[MENU_CONTAINER].widgets().filter(function(child, i) {
                return i % 2 !== 0;
            })

            subMenus.forEach(function (subMenu) {
                subMenu.widgets().forEach(function (subMenuItem) {
                    subMenuItem.skin = "skncursor";
                    subMenuItem.hoverSkin = "sknFlxHoverHamburger";
                    subMenuItem.widgets()[0].skin = "sknLblHamburgerUnSelected";
                })
            })
            
        },

        /**
         * Generate prefix by removing whitespace
         * @param {string} id Id of ite,
         * @returns {string} id with whitespace removed
         */
        getPrefix: function (id) {
            return id.replace(/ /g,'')
        },

        /**
         * Toggles the sub menu
         * @param {kony.ui.FlexContainer} widget Submenu widget to toggle 
         * @param {kony.ui.FlexContainer} imgArrow imgArrow to toggle
         */
      toggle: function (widget, imgArrow) {
        var menuText = widget.id.split("flx")[0];
        if (widget.frame.height > 0) {
          this.collapseAll();
          imgArrow.text = "O";
          imgArrow.toolTip="Expand";
          imgArrow.accessibilityConfig = {
            "a11yLabel" :  "Expand "+menuText+" Menu"
          };
        } else {
          this.collapseAll(this.activeMenu);
          //if(imgArrow.src === "arrow_down.png"){
          this.activeMenu = menuText;
          imgArrow.text = "P";
          imgArrow.toolTip="Collapse";
          imgArrow.accessibilityConfig = {
            "a11yLabel" :  "Collapse "+menuText+" Menu"
          };
          this.view.forceLayout();
         // }
          this.expand(widget);
        }
        this.view.forceLayout();
      },

        /**
         * Collapses the subMenu
         * @param {kony.ui.FlexContainer} widget Submenu widget to expand
         */
        expand: function (widget) {
            var scope = this;
            var animationDefinition = {
                100: {
                    "height":(widget.widgets().length - 1) * 60
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var callbacks = {
                animationEnd: function () {
                    // scope.checkLogoutPosition();
                    widget.widgets()[0].setFocus(true);
                    scope.view.forceLayout();
                }
            };
            var animationDef = kony.ui.createAnimation(animationDefinition);
            widget.animate(animationDef, animationConfiguration, callbacks);
        },

        /**
         * Collapse All the sub menu items
         */
        collapseAll: function (menuText) {
            var self = this;
            var menuItems = this.view[MENU_CONTAINER].widgets();
            menuItems.forEach(function (menuItem, i) {
                if (i % 2 !== 0) {
                    self.collapseWithoutAnimation(menuItem);
                    var imageWidget = menuItems[i-1].widgets()[2].widgets()[0];
                    if(imageWidget.text === "P"){
                       imageWidget.text = "O";
                        imageWidget.toolTip = "Expand";
                      if(menuText !== undefined){
                        imageWidget.accessibilityConfig = {
                          "a11yLabel" :  "Expand "+menuText+" Menu"
                        };
                        //imageWidget.forceLayout();
                      }
//                      imageWidget.accessibilityConfig = {
//                     "a11yLabel" :  "Expand Menu"
//                 };
                    }
                  
                }
               
            })
            self.view.forceLayout();
        },

        /**
         * Collapse the submenu item
         * @param {kony.ui.FlexContainer} widget submenu flex to collapse
         */
        collapseWithoutAnimation: function (widget) {
            widget.height = 0;
            this.view.forceLayout();
        },

        /**
         * Generates View of the menu dynamically
         */
        generateMenu: function () {
            var self = this;
            HamburgerConfig.config.forEach(function (hamburgerItem) {
                if (!hamburgerItem.isVisible || hamburgerItem.isVisible()) {
                    var parentMenuView = self.getParentMenuItemView(hamburgerItem);
                    var subMenuView = self.getSubMenuView(hamburgerItem);
                    //Added for frame updations on Animation End 
                    subMenuView.doLayout = function (widget) {}
                    var imgArrow = parentMenuView.widgets()[2].widgets()[0];
                    var flxArrow = parentMenuView.widgets()[2];
                    if(hamburgerItem.subMenu.children.length === 0){
                        flxArrow.isVisible = true;
                        imgArrow.isVisible = true;
						imgArrow.text="Q";
                      imgArrow.accessibilityConfig = {
                        "a11yLabel" :"Expand "+hamburgerItem.id+" Menu"
                      }
                        imgArrow.width="11dp";
                        imgArrow.height="12dp";
						if (kony.application.getCurrentBreakpoint() == 640) {
                            flxArrow.left = "88%";
                            self.view.flxClose.left = "88%";
                        } else {
                            flxArrow.left = "92%";
                            self.view.flxClose.left = "92%";
                        }
                         self.view.forceLayout();
                          parentMenuView.onClick = hamburgerItem.onClick;
                          }
                    else{
                    if(kony.application.getCurrentBreakpoint()==640){
                        flxArrow.left = "88%";
                        self.view.flxClose.left = "88%";
                    }else{
                        flxArrow.left = "92%";
                        self.view.flxClose.left = "92%";
                      imgArrow.text="O";
                      imgArrow.accessibilityConfig = {
                        "a11yLabel" :"Expand "+hamburgerItem.id+" Menu"
                      }
                    }
                    self.view.forceLayout();
                    parentMenuView.onClick = self.toggle.bind(self, subMenuView, imgArrow);
                  }
                    self.view[MENU_CONTAINER].add(parentMenuView, subMenuView);
                }
                
            });
        },

        postShowHamburger: function () {
            this.view.flxMenu.height = kony.os.deviceInfo().screenHeight + "px";
            this.setCustomHeaderLogo();
            this.view.forceLayout();
          },

      setCustomHeaderLogo : function() {
        var configurationManager = applicationManager.getConfigurationManager();
        if(configurationManager.isSMEUser === "true")this.view.imgKony.src = "sbb_white.png";
        else if(configurationManager.isRBUser === "true"){
            this.view.imgKony.src = "kony_logo_white.png";
            this.view.imgKony.width = "72dp"; 
        }
        else if(configurationManager.isMBBUser === "true")this.view.imgKony.src = "mbb_white.png";
        else this.view.imgKony.src = "kony_logo_white.png";
      },
        /**
         * Generates view of sub menu item
         * @param {object} subMenuItem Sub Menu Item Config
         * @param {string} id Id for prefixing
         * @param {boolean} removeSeperator Removes the seperator 
         * @returns {kony.ui.FlexContainer} Sub menu Item view
         */
        getSubMenuItemView: function (subMenuItem, id, removeSeperator) {
            var subMenuItemView = this.view[SAMPLE_SUB_MENU_ITEM].clone(id);
            subMenuItemView.widgets()[0].text = kony.i18n.getLocalizedString(subMenuItem.text);
            i18nMap[subMenuItemView.widgets()[0].id] =subMenuItem.text;
            subMenuItemView.widgets()[0].toolTip = kony.i18n.getLocalizedString(subMenuItem.toolTip);
            if (removeSeperator) {
                subMenuItemView.removeAt(1);
            }
            subMenuItemView.onClick = this.bindAction(subMenuItem.onClick)
            subMenuItemView.isVisible = true;
            return subMenuItemView;
        },

        /**
         * Bind onclick to hamburger item
         * @param {function} originalOnclick Original OnClick from config
         * @returns {function} Composed function
         */
        bindAction: function (originalOnclick) {
            return function () {
                if (onItemSelectListener) {
                    onItemSelectListener();
                }
                originalOnclick();
            }
        },

        /**
         * Generates the view of sub menu
         * @param {object} hamburgerItem config of item
         * @returns {kony.ui.FlexContainer} returns the view of submenu
         */
        getSubMenuView: function (hamburgerItem) {
            var self = this;
            var subMenuView = this.view[SAMPLE_SUB_MENU].clone(this.getPrefix(hamburgerItem.id));
            hamburgerItem.subMenu.children.forEach(function (subMenuItem, index) {
                if (!subMenuItem.isVisible || subMenuItem.isVisible.call(HamburgerConfig)) {
                    var subMenuItemView = self.getSubMenuItemView(subMenuItem, self.getPrefix(hamburgerItem.id)+ index, index !== hamburgerItem.subMenu.children.length - 1);
                    subMenuItemView.isVisible = true;
                    subMenuView.add(subMenuItemView);
                }
               
            })
            //Hiding Sample Sub Menu Item
            subMenuView.widgets()[0].isVisible = false;
            subMenuView.isVisible = true;
            return subMenuView;
        },

        /**
         * Generate View for Parent menu Item
         * @param {string} hamburgerItem Title of Parent Menu Item
         * @returns {kony.ui.FlexContainer} Returns the flex container object
         */
        getParentMenuItemView: function (hamburgerItem) {
            var configurationManager = applicationManager.getConfigurationManager();
            var parentMenuFlex = this.view[SAMPLE_MENU].clone(this.getPrefix(hamburgerItem.id));
            var childWidgets = parentMenuFlex.widgets();
            if (typeof hamburgerItem.icon === "function") {
                childWidgets[0].text = hamburgerItem.icon();                
            }
            else {
                childWidgets[0].text = hamburgerItem.icon;
            }
            if (typeof hamburgerItem.text === "function") {
                childWidgets[1].text = kony.i18n.getLocalizedString(hamburgerItem.text());
				childWidgets[0].accessibilityConfig = {
                    "a11yLabel" : kony.i18n.getLocalizedString(hamburgerItem.text()) + " Menu"
                };
                i18nMap[childWidgets[1].id] = hamburgerItem.text();
            }
            else {
                childWidgets[1].text = kony.i18n.getLocalizedString(hamburgerItem.text);
				childWidgets[0].accessibilityConfig = {
                    "a11yLabel" : kony.i18n.getLocalizedString(hamburgerItem.text) + " Menu"
                };
                i18nMap[childWidgets[1].id] = hamburgerItem.text;
            }
            if (typeof hamburgerItem.toolTip === "function") {
                childWidgets[1].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip());
                childWidgets[0].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip());
            }
            else {
                childWidgets[1].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip);
                childWidgets[0].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip);

            }
            if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
                childWidgets[3].left = "0%";
                childWidgets[3].width = "100%";
            }
          if(hamburgerItem.id == "TRANSFERS")
            {
              childWidgets[0].skin ="sknLblFontType0273E330px";
            }
            if(hamburgerItem.id == "About Us")
            {
              childWidgets[0].skin = "sknLblFontType0273E320px";
            }
         	 parentMenuFlex.isVisible = true;
            return parentMenuFlex;
        },

        /**
         * Preshow of Hamburger Component
         */
        initHamburger: function () {
            if (!this.initialized && applicationManager.getUserPreferencesManager().isUserLoggedin()) {
                this.generateMenu();
                this.view.imgKony.onTouchEnd=this.showDashboardScreen;
                this.initialized = true;
                if (this.actiavteWhenInitialized) {
                    this.activateMenu(this.actiavteWhenInitialized.parentId, this.actiavteWhenInitialized.childId)
                    this.actiavteWhenInitialized = null;
                }
            }
        },
      
      showDashboardScreen: function(){
      var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
      accountsModule.presentationController.showAccountsDashboard();
    },
        /**
         * For Change Language Support
         */
        forceInitializeHamburger: function () {
            this.generateMenu();
            this.initialized = true;
        },
        updateTextsHamburger: function () {
            this.updatesTextsHamburgerHelper(this.view[MENU_CONTAINER].widgets());
        },

        updatesTextsHamburgerHelper: function (widgets) {
            for (var i = 0; i < widgets.length; i++) {
                if (widgets[i] instanceof kony.ui.Label) {
                    if(i18nMap[widgets[i].id]) {
                        widgets[i].text = kony.i18n.getLocalizedString(i18nMap[widgets[i].id]);
                    }
                } else if (widgets[i] instanceof kony.ui.FlexContainer || widgets[i] instanceof kony.ui.FlexScrollContainer) {
                    this.updatesTextsHamburgerHelper(widgets[i].widgets());
                }
            }
        }
    }
})