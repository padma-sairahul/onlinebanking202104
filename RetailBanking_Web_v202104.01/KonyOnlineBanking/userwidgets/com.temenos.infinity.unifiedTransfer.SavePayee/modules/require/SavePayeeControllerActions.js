define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for SavePayee **/
    AS_FlexContainer_b08925747b2d4786a8ff7369666ec67a: function AS_FlexContainer_b08925747b2d4786a8ff7369666ec67a(eventobject) {
        var self = this;
        return self.preShow.call(this);
    },
    /** postShow defined for SavePayee **/
    AS_FlexContainer_f1b76e3e061549b9976a18811988bf05: function AS_FlexContainer_f1b76e3e061549b9976a18811988bf05(eventobject) {
        var self = this;
        return self.postShow.call(this);
    },
    /** onBreakpointChange defined for SavePayee **/
    AS_FlexContainer_f804691ab3034e7fb8927a81a4325044: function AS_FlexContainer_f804691ab3034e7fb8927a81a4325044(eventobject, breakpoint) {
        var self = this;
        return self.onBreakPointChange.call(this);
    }
});