define(function() {

	return {
		enableSearchButton: function () {
			this.view.btnSearch.skin = "sknBtnSSPffffff15pxBg0273e3";
			this.view.btnSearch.setEnabled(true);
			this.view.btnSearch.hoverSkin = "sknBtnSSPffffff15pxBg0273e3";
			this.view.btnSearch.focusSkin="sknBtnSSPffffff15pxBg0273e3";
		},
		disableSearchButton: function () {
			this.view.btnSearch.setEnabled(false);
			this.view.btnSearch.skin="sknBtnBlockedSSP0273e315px";
			this.view.btnSearch.hoverSkin = "sknBtnBlockedSSP0273e315px";  
			this.view.btnSearch.focusSkin="sknBtnBlockedSSP0273e315px"; 
		},
		showByDateWidgets: function () {
			this.view.lblByDate.setVisibility(true);
			this.view.flxByDate.setVisibility(true);
			this.view.flxBlankSpace.setVisibility(false);
		},
		hideByDateWidgets: function () {
			this.view.lblByDate.setVisibility(false);
			this.view.flxByDate.setVisibility(false);
			this.view.flxBlankSpace.setVisibility(true);
		},
		setSearchVisible: function (isVisible,isMobile) {
			this.view.flxSeparatorSearch.setVisibility(isVisible);
			this.view.flxSearchContainer.setVisibility(isVisible);
            if(isVisible === true  && isMobile === true){
               // this.view.txtKeyword.setFocus(true);
               // this.view.lblByKeyword.setFocus(true);
            }
			this.view.imgSearch.src = (isVisible || this.view.flxSearchResults.isVisible) ? "selecetd_search.png": "search_blue.png"  			
		},
		setSearchResultsVisible : function(isVisible) {
			this.view.imgSearch.src = (isVisible || this.view.flxSearchContainer.isVisible) ? "selecetd_search.png": "search_blue.png"
			this.view.flxSeparatorSearch.setVisibility(isVisible);
			this.view.flxSearchResults.setVisibility(isVisible);
            if(isVisible === true){
              this.view.lblYouHaveSearchedFor.setFocus(true);
            }
		}
	};
});