define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onBreakpointChange defined for summary **/
    AS_FlexContainer_a704b00e168149d8a6278d3aa0bc4e42: function AS_FlexContainer_a704b00e168149d8a6278d3aa0bc4e42(eventobject, breakpoint) {
        var self = this;
        this.onBreakPointChange();
    },
    /** postShow defined for summary **/
    AS_FlexContainer_d558df92fc1346eda11b327201a79544: function AS_FlexContainer_d558df92fc1346eda11b327201a79544(eventobject) {
        var self = this;
        this.postShow();
    }
});