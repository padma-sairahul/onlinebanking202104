define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_gaddb01b1f3043628f53df137a2ca616: function AS_Button_gaddb01b1f3043628f53df137a2ca616(eventobject) {
        var self = this;
        this.welcomeVerifiedUser();
    },
    AS_Button_e17f949884e7466ab6edad6b7950df3d: function AS_Button_e17f949884e7466ab6edad6b7950df3d(eventobject) {
        var self = this;
        this.showEnterOTPPage();
    },
    AS_Button_f6ae0b2a53834a05bf629f2c4d365bc7: function AS_Button_f6ae0b2a53834a05bf629f2c4d365bc7(eventobject) {
        var self = this;
        this.showResetPasswordPage();
    },
    AS_Button_c767e8de65594495bfda0c20ac8dec9f: function AS_Button_c767e8de65594495bfda0c20ac8dec9f(eventobject) {
        var self = this;
        this.showResetPasswordPage();
    },
    AS_Button_f633f48b8ed641268435a2261b434276: function AS_Button_f633f48b8ed641268435a2261b434276(eventobject) {
        var self = this;
        this.showResetConfirmationPage();
    }
});