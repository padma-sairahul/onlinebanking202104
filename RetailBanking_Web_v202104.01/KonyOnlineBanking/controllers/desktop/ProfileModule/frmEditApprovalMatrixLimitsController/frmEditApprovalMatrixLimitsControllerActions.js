define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmEditApprovalMatrixLimits **/
    AS_Form_a0bfe0d720154c10b4e7576fdc87f871: function AS_Form_a0bfe0d720154c10b4e7576fdc87f871(eventobject) {
        var self = this;
        return self.onInit.call(this);
    },
    /** onDeviceBack defined for frmEditApprovalMatrixLimits **/
    AS_Form_b8058a9398264b1bb7cbfc2aa1c2e262: function AS_Form_b8058a9398264b1bb7cbfc2aa1c2e262(eventobject) {
        var self = this;
        this.doLogout("Logout");
    },
    /** postShow defined for frmEditApprovalMatrixLimits **/
    AS_Form_d883d022656d4c54a2d32eefb5fb65c8: function AS_Form_d883d022656d4c54a2d32eefb5fb65c8(eventobject) {
        var self = this;
        return self.onPostShow.call(this);
    },
    /** preShow defined for frmEditApprovalMatrixLimits **/
    AS_Form_hadea4e5727b42edaccda9c01cfd8c29: function AS_Form_hadea4e5727b42edaccda9c01cfd8c29(eventobject) {
        var self = this;
        return self.onPreShow.call(this);
    }
});