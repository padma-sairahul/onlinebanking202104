define(['CommonUtilities', 'CSRAssistUI', 'FormControllerUtility', 'OLBConstants', 'ViewConstants', 'CampaignUtility'], function(CommonUtilities, CSRAssistUI, FormControllerUtility, OLBConstants, ViewConstants, CampaignUtility) {
    var orientationHandler = new OrientationHandler();
    var flag = 0;
    var update = "";
    var showOTP = 1;
    var prevApprovalMatrixData = {};
    var alert = "Account";
    var finalData = [];
    var totalAddress = 0;
    var editContext = [];
    var consentManagementData = [];
    var consentEdit = false;

    var widgetsMap = [{
            menu: "flxProfileSettings",
            subMenu: {
                parent: "flxProfileSettingsSubMenu",
                children: [{
                        "configuration": "enableProfileSettings",
                        "widget": "flxProfile"
                    },
                    {
                        "configuration": "enablePhoneSettings",
                        "widget": "flxPhone"
                    },
                    {
                        "configuration": "enableEmailSettings",
                        "widget": "flxEmail"
                    },
                    {
                        "configuration": "enableAddressSettings",
                        "widget": "flxAddress"
                    },
                    {
                        "configuration": "enableUsernameAndPasswordSettings",
                        "widget": "flxUsernameAndPassword"
                    },
                    {
                        "configuration": "enableEBankingAccessSettings",
                        "widget": "flxEBankingAccess"
                    },
                    {
                        "configuration": "",
                        "widget": "flxChangeLanguage"
                    },
                ]
            },
            image: "lblProfileSettingsCollapse"
        },
        {
            menu: "flxSecuritySettings",
            subMenu: {
                parent: "flxSecuritySettingsSubMenu",
                children: [{
                        "configuration": "enableSecurityQuestionsSettings",
                        "widget": "flxSecurityQuestions"
                    },
                    {
                        "configuration": "enableSecureAccessCodeSettings",
                        "widget": "flxSecureAccessCode"
                    }
                ]
            },
            image: "lblSecuritySettingsCollapse"
        },
        {
            menu: "flxAccountSettings",
            subMenu: {
                parent: "flxAccountSettingsSubMenu",
                children: [{
                        "configuration": "enablePreferredAccountSettings",
                        "widget": "flxAccountPreferences"
                    },
                    {
                        "configuration": "enableDefaultAccounts",
                        "widget": "flxSetDefaultAccount"
                    }
                ]
            },
            image: "lblAccountSettingsCollapse"
        },
        {
            menu: "flxAlerts",
            subMenu: {
                parent: "flxAlertsSubMenu",
                children: [{
                        "configuration": "",
                        "widget": "flxAccountAlerts"
                    },
                    {
                        "configuration": "enableAlertSettings",
                        "widget": "flxTransactionAndPaymentsAlerts"
                    },
                    {
                        "configuration": "",
                        "widget": "flxSecurityAlerts"
                    },
                    {
                        "configuration": "",
                        "widget": "flxPromotionalAlerts"
                    },
                ]
            },
            image: "lblAlertsCollapse"
        },
        {
            menu: "flxApprovalMatrix",
            subMenu: {
                parent : "flxApprovalMatrixSubMenu",
				children : []
            },
            image: "lblApprovalMatrixCollapse"
        },
        {
            menu: "flxConsentManagement",
            subMenu: {
                parent: "flxConsentManagementSubMenu",
                children: [{
                    "configuration": "",
                    "widget": "flxYourConsent"
                }, ]
            },
            image: "lblConsentManagementCollapse"
        },
        {
            menu: "flxManageAccountAccess",
            subMenu: {
                parent: "flxManageAccountAccessSubMenu",
                children: [{
                    "configuration": "",
                    "widget": "flxFromThirdParties"
                }, ]
            },
            image: "lblManageAccountAccessCollapse"
        }
    ];

    return {
        /**
         * @function
         *
         */
        profileAccess: "",
        primaryCustomerId: {},
        isFavAccAvailable: false,
        isExtAccAvailable: false,
		contractsApprovalMatrix : null,
        willUpdateUI: function() {
            kony.print("still in the old navigation form");
        },
        showUsernameVerificationByChoice: function() {
            this.view.settings.imgUsernameVerificationcheckedRadio.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
            this.view.settings.imgUsernameVerificationcheckedRadioOption2.src = ViewConstants.IMAGES.ICON_RADIOBTN;
            this.showViews(["flxUsernameVerificationWrapper"]);
        },
        updateFormUI: function(viewModel) {
            if (viewModel !== undefined) {
                this.view.flxDowntimeWarning.setVisibility(false);
				this.view.settings.AllForms1.isVisible = false;
                if (viewModel.showDefautUserAccounts) this.showDefaultUserAccount(viewModel.showDefautUserAccounts);
                if (viewModel.showPrefferedView) this.showViews(['flxAccountsWrapper']);
                if (viewModel.getAccountsList) this.showAccountsList(viewModel.getAccountsList);
                if (viewModel.getPreferredAccountsList) this.showPreferredAccountsList(viewModel.getPreferredAccountsList);
                if (viewModel.errorSaveExternalAccounts) this.onSaveExternalAccount(viewModel.errorSaveExternalAccounts);
                if (viewModel.errorEditPrefferedAccounts) this.onPreferenceAccountEdit(null, viewModel.errorEditPrefferedAccounts);
                if (viewModel.userProfile) this.updateUserProfileSetttingsView(viewModel.userProfile);

                if (viewModel.emailList) this.updateEmailList(viewModel.emailList);
                if (viewModel.emails) this.setEmailsToLbx(viewModel.emails);
                if (viewModel.phoneList) this.updatePhoneList(viewModel.phoneList);
                if (viewModel.addressList === null) {
                    viewModel.addressList = [];
                    FormControllerUtility.hideProgressBar(this.view);
                }
                if (viewModel.addressList) this.updateAddressList(viewModel.addressList);
                if (viewModel.addPhoneViewModel) this.updateAddPhoneViewModel(viewModel.addPhoneViewModel);
                if (viewModel.editPhoneViewModel) this.updateEditPhoneViewModel(viewModel.editPhoneViewModel);
                if (viewModel.alertCommunication) this.navigateToAlertComm();
                if (viewModel.showVerificationByChoice) {
                    this.updateRequirements();
                }
                if (viewModel.requestOtp) this.enterOtp();
                if (viewModel.verifyOtp) this.updateRequirements();
                if (viewModel.verifyQuestion) this.updateRequirements();
                if (viewModel.update) this.acknowledge();
                if (viewModel.securityAccess) this.showSecureAccessOptions(viewModel.securityAccess);
                if (viewModel.secureAccessOption) kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecureAccess();
                if (viewModel.SecurityQuestionExists) this.showSecurityQuestions(viewModel.SecurityQuestionExists);
                if (viewModel.answerSecurityQuestion) this.showSecurityVerification(viewModel.answerSecurityQuestion);
                if (viewModel.passwordExists) this.showExistingPasswordError();
                if (viewModel.wrongPassword) this.showWrongPasswordError();
                if (viewModel.requestOtpError) this.showRequestOtpError();
                if (viewModel.verifyOtpServerError) this.showVerifyOtpServerError(); //validate this statement.
                if (viewModel.verifyQuestionAnswerError) this.showVerifyQuestionAnswerError();
                if (viewModel.verifyQuestionServerAnswerError) this.showVerifyQuestionAnswerServerError(); //validate this statement.
                if (viewModel.getAnswerSuecurityQuestionError) this.showGetAnswerSecurityQuestionError();
                if (viewModel.updateUsernameError) this.showUpdateUsernameError(viewModel.updateUsernameError);
                if (viewModel.updateUsernameServerError) this.showUpdateUsernameServerError(); //validate this statement.
                if (viewModel.SecurityQuestionExistsError) this.showSecurityQuestionExistsError();
                if (viewModel.updateSecurityQuestionError) this.showUpdateSecurityQuestionError();
                if (viewModel.fetchSecurityQuestionsError) this.showFetchSecurityQuestionsError(); //validate this statement.
                if (viewModel.checkSecurityAccessError) this.showCheckSecurityAccessError();
                if (viewModel.verifyOtpError) this.showVerifyOtpError();
                if (viewModel.addNewAddress) this.showAddNewAddressForm(viewModel.addNewAddress);
                if (viewModel.editAddress) this.showEditAddressForm(viewModel.editAddress);
                if (viewModel.secureAccessOptionError) this.showSecureAccessOptionError();
                if (viewModel.passwordExistsServerError) this.showPasswordExistsServerError();
                if (viewModel.passwordServerError) this.showPasswordServerError(viewModel.passwordServerError);
                if (viewModel.emailError) this.showEmailError(viewModel.emailError);
                if (viewModel.addressEditError) this.showEditAddressError(viewModel.addressEditError);
                if (viewModel.addressError) this.showAddAddressError(viewModel.addressError);
                if (viewModel.phoneDetails) this.showPhoneDetails(viewModel.phoneDetails);
                if (viewModel.editEmailError) this.showEditEmailError(viewModel.editEmailError);
                if (viewModel.editAlertCommError) this.showEditAlertCommError(viewModel.editAlertCommError);
                if (viewModel.Alerts) this.showAlerts(viewModel.Alerts);
                if (viewModel.accountAlertsData) this.setAccountAlerts(viewModel.accountAlertsData);
                if (viewModel.AlertsError) this.showAlertsError(viewModel.AlertsError);
                if (viewModel.AlertsDataById) this.showAlertsDataByID(viewModel.AlertsDataById);
                if (viewModel.editExternalAccounts) this.goToEditExternalAccounts(viewModel.editExternalAccounts);
                if (viewModel.secureAccessSettings) this.showSecureAccessSettings();
                if (viewModel.usernamepasswordRules) this.getPasswordRules(viewModel.usernamepasswordRules);
                if (viewModel.isLoading !== undefined) this.changeProgressBarState(viewModel.isLoading);
                if (viewModel.usernamepolicies) {
                    this.getUsernameRules(viewModel.usernamepolicies);
                }
                if (viewModel.usernamepasswordRules) {
                    this.getPasswordRules(viewModel.usernamepasswordRules);
                }
                if (viewModel.fetchFirstAlert) this.fetchFirstAlertType();
                if (viewModel.fetchThirdAlert) this.fetchthirdAlertType(viewModel.fetchThirdAlert.accountID);
                if (viewModel.usernameAndPasswordLanding) this.showusernameAndPasswordLanding();
                if (viewModel.alertsSaved) this.saveAlertSuccess(viewModel.alertsSaved);
                if (viewModel.showSecurityQuestion) this.showSecurityQuestionsScreen();
                if (viewModel.TnCcontent) {
                    this.bindTnCData(viewModel.TnCcontent);
                }
                if (viewModel.companyAccounts) {
                    //this.prepareApprovalMatrixSubmenus(viewModel.companyAccounts);
                }
                if (viewModel.fetchFirstAccountApprovalMatrix) {
                    //this.getApprovalMatrixOfFirstAccount(viewModel.fetchFirstAccountApprovalMatrix.companyAccounts);
                }

                if (viewModel.approversResponse) {
                    //this.showAddRemoveApproversPopup(viewModel.approversResponse);
                }

                if (viewModel.approvalMatrix) {
                    //this.showApprvalMatrixWrapper(viewModel.approvalMatrix);
                }
                if (viewModel.approvalMatrixFailure) {
                    //this.showApprovalMatrix([], viewModel.approvalMatrixFailure.errorMessage);
                }

                if (viewModel.approvalMatrixUpdated) {
                    //this.showApprovalMatrixAfterSuccessfulEditing(viewModel.approvalMatrixUpdated);
                }
				if(viewModel.approvalMatrixContractDetails) {
				  this.showApprovalMatrixContractDetails(viewModel.approvalMatrixContractDetails);
				}
				if(viewModel.approvalMatrixError) {
				  this.showErrorMessageForApprovalMatrix(viewModel.approvalMatrixError);
				}
                if (viewModel.EditModelSuccess) {
                    this.navigateToEditApprovalMatrix(viewModel.EditModelSuccess);
                }
                if (viewModel.EditModelError) {
                    var errResp = kony.sdk.isNullOrUndefined(viewModel.EditModelError.error) ?
                        kony.i18n.getLocalizedString("i18n.common.errorCodes.21009") :
                        viewModel.EditModelError.error;
                    this.showAlertsError(errResp);
                }
                if (viewModel.campaign) {
                    CampaignUtility.showCampaign(viewModel.campaign, this.view, "flxContainer");
                }


                if (viewModel.consentResponse) {
                    this.setConsentManagementData(viewModel.consentResponse);
                }
                if (viewModel.consentError) {
                    this.showErrorConsent(viewModel.consentError, viewModel.action);
                }
                if (viewModel.manageAccountAccessResponse) {
                    this.setManageAccountAccessData(viewModel.manageAccountAccessResponse);
                }
                if (viewModel.managePSD2Error) {
                    this.setNoMaaData(viewModel.managePSD2Error, viewModel.action);
                }

                if (viewModel.termsAndConditionsContent) {
                    this.setTncContent(viewModel.termsAndConditionsContent);
                }
                if (viewModel.showRegisteredDevices) this.showRegisteredDevices();
                this.AdjustScreen();
                this.view.forceLayout();

            }
        },
        showusernameAndPasswordLanding: function() {
            this.showUsernameAndPassword();
            this.setSelectedSkin("flxUsernameAndPassword");
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Calls the method assigned to first alert comming from backend.
         */
        fetchFirstAlertType: function() {
            if (this.view.settings.flxAlertCommunications.isVisible)
                this.view.settings.flxAlertCommunications.onClick();
            else
                this.onCategoryMenuClick();
        },
        fetchthirdAlertType: function(accountID) {
            //             this.view.settings.flxSecurityAlerts.onClick();
            //             this.view.settings.flxIndicatorSecurityAlerts.isvisible= true;
            //             this.view.settings.flxIndicatorSecurityAlerts.skin="sknbg0273e3";
            this.onViewModifyAlertByID(accountID, "ALERT_CAT_ACCOUNTS");
        },
        setLanguages: function() {
            var langlist = this.getLanguageMasterData();
            var scopeObj = this;
            var languages = [];
            for (var lang in langlist) {
                if (langlist.hasOwnProperty(lang)) {
                    var temp = ([langlist[lang], lang]);
                    languages.push(temp);
                }
            }
            this.view.settings.lbxSelectLanguage.masterData = languages;
            this.view.settings.lbxSelectLanguage.selectedKey = langlist[scopeObj.getFrontendLanguage(applicationManager.getStorageManager().getStoredItem("langObj").language)];
            if (this.view.settings.lbxSelectLanguage.selectedKey === langlist[scopeObj.getFrontendLanguage(applicationManager.getStorageManager().getStoredItem("langObj").language)]) {
                this.disableButton(this.view.settings.btnChangeLanguageSave);
            } else {
                this.enableButton(this.view.settings.btnChangeLanguageSave);
            }
        },
        disableorEnableSaveButton: function() {
            var scopeObj = this;
            var selectedLang = this.view.settings.lbxSelectLanguage.selectedKey;
            var langlist = this.getLanguageMasterData();
            if (this.view.settings.lbxSelectLanguage.selectedKey === langlist[scopeObj.getFrontendLanguage(applicationManager.getStorageManager().getStoredItem("langObj").language)]) {
                this.disableButton(this.view.settings.btnChangeLanguageSave);
            } else {
                this.enableButton(this.view.settings.btnChangeLanguageSave);
            }
        },
        /**
         * Method to change the selected language to backend language string
         * @param {String} lang - selected language
         */
        getFrontendLanguage: function(lang) {
            var languageData = this.getLanguageMasterData();
            var configManager = applicationManager.getConfigurationManager();
            var langObject = configManager.locale;
            for (var key in langObject) {
                if (langObject.hasOwnProperty(key)) {
                    if (key === lang) {
                        return this.getValueFromKey(langObject[key], languageData);
                    }
                }
            }
        },
        /**
         * Method to fetch languages JSON
         */
        getLanguageMasterData: function() {
            return {
                "US English": "en_US",
                "UK English": "en_GB",
                "Spanish": "es_ES",
                "German": "de_DE",
                "French": "fr_FR"
            }
        },
        /**
         * Method to fetch language from key
         * @param {String} value - selected language
         * @param {Object} langObject - language Object
         */
        getValueFromKey: function(value, langObject) {
            for (var key in langObject) {
                if (langObject.hasOwnProperty(key)) {
                    var shortLang = langObject[key];
                    if (shortLang === value) {
                        return key;
                    }
                }
            }
        },
        /**
         * Method to change the selected language to backend language string
         * @param {String} lang - selected language
         */
        getBackendLanguage: function(lang) {
            var languageData = this.getLanguageMasterData();
            var configManager = applicationManager.getConfigurationManager();
            var langObject = configManager.locale;
            for (var key in languageData) {
                if (languageData.hasOwnProperty(key)) {
                    if (key === lang) {
                        return this.getValueFromKey(languageData[key], langObject);
                    }
                }
            }
        },
        /**
         * Preshow of Profile Management
         */
        preShowProfileManagement: function() {
            var self = this;
            this.profileAccess = applicationManager.getUserPreferencesManager().profileAccess;
            this.view.customheader.forceCloseHamburger();
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Profile");
            this.setLanguages();
            this.view.settings.flxSecureAccessCode.setVisibility(false);
			this.view.settings.flxApprovalMatrixSubMenu.setVisibility(false);
            this.view.settings.flxChangeLanguageWrapper.setVisibility(false);
            //this.view.settings.flxOption4.setVisibility(false);
            this.view.settings.btnNameChangeRequest.isVisible = false;
            this.view.settings.btnAddPhoto.isVisible = false;
            this.view.flxEditApprovalMatrixWrapper.setVisibility(false);
            this.view.flxSettingsWrapper.setVisibility(true);
            this.primaryCustomerId = applicationManager.getUserPreferencesManager().primaryCustomerId;
			this.view.settings.imgDefaultTransactionAccountWarning.setVisibility(false);
			this.view.settings.txtDefaultTransactionAccountWarning.left = "0%";
            this.view.forceLayout();
            CampaignUtility.fetchPopupCampaigns();
            if (!applicationManager.getConfigurationManager().checkUserFeature("ONLINE_BANKING_ACCESS")) {
                self.view.settings.flxEBankingAccess.setVisibility(false);
            }
          	this.view.deviceRegistration.setVisibility(false);
        },
        /**
         * Method to hide all the flex of main body
         * @param {Object} editAddressViewModel - None
         */
        showEditAddressForm: function(editAddressViewModel) {
            var self = this;
            this.showEditAddress();
            this.view.settings.lbxEditType.masterData = editAddressViewModel.addressTypes;
            if(editAddressViewModel.addressTypeSelected=="ADR_TYPE_WORK" || editAddressViewModel.addressTypeSelected=="ADR_TYPE_HOME")
              this.view.settings.lbxEditType.selectedKey = editAddressViewModel.addressTypeSelected;
            else if(editAddressViewModel.addressTypeSelected.toLowerCase()=="home")
              this.view.settings.lbxEditType.selectedKey="ADR_TYPE_HOME";
            else if(editAddressViewModel.addressTypeSelected.toLowerCase()=="work")
              this.view.settings.lbxEditType.selectedKey="ADR_TYPE_WORK";
            this.view.settings.tbxEditAddressLine1.text = editAddressViewModel.addressLine1 || "";
            this.view.settings.tbxEditAddressLine2.text = editAddressViewModel.addressLine2 || "";
            if(editAddressViewModel.countrySelected)
            data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("country", editAddressViewModel.countrySelected, editAddressViewModel.stateNew);
            self.view.settings.lbxEditState.masterData = (editAddressViewModel.countrySelected)?data.states:editAddressViewModel.stateNew;
            self.view.settings.lbxEditState.selectedKey = (editAddressViewModel.stateSelected) ? editAddressViewModel.stateSelected : editAddressViewModel.stateNew[0][0];
            //data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", editAddressViewModel.stateSelected, editAddressViewModel.cityNew);
            //self.view.settings.tbxEditCity.masterData = data.cities;
            //self.view.settings.tbxEditCity.selectedKey = editAddressViewModel.citySelected;
            this.view.settings.lbxEditCountry.masterData = editAddressViewModel.countryNew;
            this.view.settings.lbxEditCountry.selectedKey = (editAddressViewModel.countrySelected) ? editAddressViewModel.countrySelected : (editAddressViewModel.CountryCode)?(editAddressViewModel.CountryCode):editAddressViewModel.countryNew[0][0];
            //this.view.settings.lbxEditState.selectedKey = editAddressViewModel.stateSelected;
            //this.view.settings.tbxEditCity.selectedKey = editAddressViewModel.citySelected;
            this.view.settings.lbxEditCountry.onSelection = function() {
                var data = [];
                var countryId = self.view.settings.lbxEditCountry.selectedKeyValue[0];
                if (countryId == "1") {
                    self.checkUpdateAddressForm();
                    self.view.settings.lbxEditState.masterData = editAddressViewModel.stateNew;
                    self.view.settings.lbxEditState.selectedKey = editAddressViewModel.stateNew[0][0];
                    self.view.settings.tbxEditCity.masterData = editAddressViewModel.cityNew;
                    self.view.settings.tbxEditCity.selectedKey = editAddressViewModel.cityNew[0][0];
                    self.view.settings.tbxEditCity.setEnabled(false);
                    self.view.settings.lbxEditState.setEnabled(false);
                } else {
                    self.view.settings.tbxEditCity.setEnabled(false);
                    self.view.settings.lbxEditState.setEnabled(true);
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("country", countryId, editAddressViewModel.stateNew);
                    self.view.settings.lbxEditState.masterData = data.states;
                    self.view.settings.lbxEditState.selectedKey = data.states[0][0];
                    var stateId = self.view.settings.lbxEditState.selectedKeyValue[0];
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", stateId, editAddressViewModel.cityNew);
                    self.view.settings.tbxEditCity.masterData = data.cities;
                    self.view.settings.tbxEditCity.selectedKey = data.cities[0][0];
                    self.checkUpdateAddressForm();
                }
            };
            this.view.settings.lbxEditState.onSelection = function() {
                var data = [];
                var stateId = self.view.settings.lbxEditState.selectedKeyValue[0];
                if (stateId == "lbl1") {
                    self.checkUpdateAddressForm();
                    self.view.settings.tbxEditCity.masterData = editAddressViewModel.cityNew;
                    self.view.settings.tbxEditCity.selectedKey = editAddressViewModel.cityNew[0][0];
                    self.view.settings.tbxEditCity.setEnabled(false);
                } else {
                    self.view.settings.tbxEditCity.setEnabled(true);
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", stateId, editAddressViewModel.cityNew);
                    self.view.settings.tbxEditCity.masterData = data.cities;
                    self.view.settings.tbxEditCity.selectedKey = data.cities[0][0];
                    self.checkUpdateAddressForm();
                }
            };
            this.view.settings.tbxEditCity.onSelection = function() {
                self.checkUpdateAddressForm();
            };
            self.view.settings.txtEditCity.text = editAddressViewModel.city || "";
            this.view.settings.tbxEditZipcode.text = editAddressViewModel.zipcode || "";
            this.view.settings.lblEditSetAsPreferredCheckBox.skin = editAddressViewModel.isPreferredAddress ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            CommonUtilities.setText(this.view.settings.lblEditSetAsPreferredCheckBox, editAddressViewModel.isPreferredAddress ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxEditSetAsPreferred.setVisibility(!editAddressViewModel.isPreferredAddress);
            this.checkUpdateAddressForm();
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnEditAddressSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnEditAddressSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnEditAddressSave.onClick = function() {
                    FormControllerUtility.showProgressBar(self.view);
                    var data = this.getUpdateAddressData();
                    data.addressId = editAddressViewModel.addressId;
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateAddress(data);
                }.bind(this);
            }
            this.view.settings.forceLayout();
        },
        /**
         * Method to show server error while editing username
         */
        showUpdateUsernameServerError: function() {
            CommonUtilities.setText(this.view.settings.lblError0, kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxErrorEditUsername.setVisibility(true);
            this.view.settings.tbxUsername.text = "";
            this.showEditUsername();
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while requestind OTP
         */
        showRequestOtpError: function() {
            //warning flex not there
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while verifying OTP
         */
        showVerifyOtpServerError: function() {
            this.view.settings.flxErrorSecuritySettingsVerification.setVisibility(true);
            CommonUtilities.setText(this.view.settings.lblErrorSecuritySettingsVerification, kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError"), CommonUtilities.getaccessibilityConfig());
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while verifying Question and answer
         */
        showVerifyQuestionAnswerError: function() {
            this.view.settings.flxErrorEditSecurityQuestions.setVisibility(true);
            CommonUtilities.setText(this.view.settings.lblErrorSecurityQuestions, kony.i18n.getLocalizedString("i18n.ProfileManagement.invalidAnswers"), CommonUtilities.getaccessibilityConfig());
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show server error while displaying Question and answer
         */
        showVerifyQuestionAnswerServerError: function() {
            this.view.settings.flxErrorEditSecurityQuestions.setVisibility(true);
            CommonUtilities.setText(this.view.settings.lblErrorSecurityQuestions, kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError"), CommonUtilities.getaccessibilityConfig());
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while fetching Question and answer
         */
        showGetAnswerSecurityQuestionError: function() {
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error updating the username when service is hit
         */
        showUpdateUsernameError: function(error) {
            if (error.errorMessage && error.errorMessage != "") {
                CommonUtilities.setText(this.view.settings.lblError0, error.errorMessage, CommonUtilities.getaccessibilityConfig());
            } else {
                CommonUtilities.setText(this.view.settings.lblError0, error.serverErrorRes.dbpErrMsg, CommonUtilities.getaccessibilityConfig());
            }
            this.view.settings.flxErrorEditUsername.setVisibility(true);
            this.view.settings.tbxUsername.text = "";
            this.showEditUsername();
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
            this.disableButton(this.view.settings.btnEditUsernameProceed);
        },
        /**
         * Method to show error while displaying Question and answer
         */
        showSecurityQuestionExistsError: function() {
            CommonUtilities.setText(this.view.settings.lblErrorSecuritySettings, kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxErrorEditSecuritySettings.setVisibility(true);
            this.showEditSecuritySettings();
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while updating Question and answer at the service side
         */
        showUpdateSecurityQuestionError: function() {
            CommonUtilities.setText(this.view.settings.lblErrorSecuritySettings, kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxErrorEditSecuritySettings.setVisibility(true);
            this.showEditSecuritySettings();
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while fetching Question and answer from backend
         */
        showFetchSecurityQuestionsError: function() {
            CommonUtilities.setText(this.view.settings.lblErrorSecuritySettings, kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxErrorEditSecuritySettings.setVisibility(true);
            this.showViews(["flxEditSecuritySettingsWrapper"]);
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while fetching the secure access
         */
        showCheckSecurityAccessError: function() {
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while verifying OTP from backend
         */
        showVerifyOtpError: function() {
            this.view.settings.flxErrorSecuritySettingsVerification.setVisibility(true);
            CommonUtilities.setText(this.view.settings.lblErrorSecuritySettingsVerification, kony.i18n.getLocalizedString("i18n.ProfileManagement.invalidCode"), CommonUtilities.getaccessibilityConfig());
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while updating the secure access options
         */
        showSecureAccessOptionError: function() {
            //error flx not there
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while updating the password and server is down
         */
        showPasswordExistsServerError: function() {
            this.view.settings.flxErrorEditPassword.setVisibility(true);
            this.view.settings.tbxExistingPassword.text = "";
            this.view.settings.tbxNewPassword.text = "";
            this.view.settings.tbxConfirmPassword.text = "";
            CommonUtilities.setText(this.view.settings.lblError1, kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError"), CommonUtilities.getaccessibilityConfig());
            this.showViews(["flxEditPasswordWrapperComp"]);
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        showPasswordServerError: function(error) {
            let scopeObj = this;
            if (scopeObj.view.settings.updatePassword && scopeObj.view.settings.updatePassword.showErrorMessage) {
                scopeObj.view.settings.updatePassword.showErrorMessage(error.dbpErrMsg);
            }
        },
        /**
         * Method to show error while updating the password and ad password is not same for the existing password entered by user
         */
        showExistingPasswordError: function() {
            this.disableButton(this.view.settings.btnEditPasswordProceed);
            this.view.settings.flxErrorEditPassword.setVisibility(true);
            this.view.settings.tbxExistingPassword.text = "";
            this.view.settings.tbxNewPassword.text = "";
            this.view.settings.tbxConfirmPassword.text = "";
            CommonUtilities.setText(this.view.settings.lblError1, kony.i18n.getLocalizedString("i18n.ProfileManagement.passwordExists"), CommonUtilities.getaccessibilityConfig());
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method used to show the invalid credentials error message when trying to change password.
         */
        showWrongPasswordError: function() {
            this.disableButton(this.view.settings.btnEditPasswordProceed);
            this.view.settings.flxErrorEditPassword.setVisibility(true);
            this.view.settings.tbxExistingPassword.text = "";
            this.view.settings.tbxNewPassword.text = "";
            this.view.settings.tbxConfirmPassword.text = "";
            CommonUtilities.setText(this.view.settings.lblError1, kony.i18n.getLocalizedString("i18n.idm.existingPasswordMismatch"), CommonUtilities.getaccessibilityConfig());
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to convert question and answer to JSON
         */
        onSaveAnswerSecurityQuestions: function() {
            var data = [{
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }];
            var quesData = "";
            quesData = this.view.settings.lblAnswerSecurityQuestion1.text;
            data[0].customerAnswer = this.view.settings.tbxAnswers1.text;
            data[0].questionId = this.getQuestionIDForAnswer(quesData);
            quesData = this.view.settings.lblAnswerSecurityQuestion2.text;
            data[1].customerAnswer = this.view.settings.tbxAnswers2.text;
            data[1].questionId = this.getQuestionIDForAnswer(quesData);
            return data;
        },
        /**
         * Method to return the ID of question sent from backend
         * @param {String} quesData- it is the question who's ID is to be searched
         */
        getQuestionIDForAnswer: function(quesData) {
            var qData;
            for (var i = 0; i < this.responseBackend.length; i++) {
                if (quesData === this.responseBackend[i].Question) {
                    qData = this.responseBackend[i].SecurityQuestion_id;
                }
            }
            return qData;
        },
        /**
         * Method to show security questions for verification
         * @param {Object} viewModel- Set of questions to be shown
         */
        showSecurityVerification: function(data) {
            this.responseBackend = data;
            var self = this;
            CommonUtilities.setText(this.view.settings.lblAnswerSecurityQuestion1, data[0].Question, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblAnswerSecurityQuestion2, data[1].Question, CommonUtilities.getaccessibilityConfig());
            this.view.settings.btnBackAnswerSecurityQuestions.onClick = function() {
                self.showUsernameVerificationByChoice();
            };
            this.showAnswerSecurityQuestions();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to handle the configurations at account settings
         */
        configurationAccountSettings: function() {
            if (applicationManager.getConfigurationManager().enableAccountSettings === "true") {
                this.view.settings.flxAccountSettings.setVisibility(true);
                this.view.settings.flxAccountSettingsSubMenu.setVisibility(true);
            } else {
                this.view.settings.flxAccountSettings.setVisibility(false);
                this.view.settings.flxAccountSettingsSubMenu.setVisibility(false);
            }
            if (applicationManager.getConfigurationManager().enableDefaultAccounts === "true") {
                this.view.settings.flxSetDefaultAccount.setVisibility(true);
            } else {
                this.view.settings.flxSetDefaultAccount.setVisibility(false);
            }
            if (applicationManager.getConfigurationManager().enablePreferredAccounts === "true") {
                this.view.settings.flxAccountPreferences.setVisibility(true);
            } else {
                this.view.settings.flxAccountPreferences.setVisibility(false);
            }
            if (applicationManager.getConfigurationManager().enablePreferredAccounts === "false" && applicationManager.getConfigurationManager().enableDefaultAccounts === "false") {
                this.view.settings.flxAccountSettings.setVisibility(false);
                this.view.settings.flxAccountSettingsSubMenu.setVisibility(false);
            } else {
                this.view.settings.flxAccountSettings.setVisibility(true);
                this.view.settings.flxAccountSettingsSubMenu.setVisibility(true);
            }
        },
        /**
         *AdjustScreen- function to adjust the footer
         */
        AdjustScreen: function() {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.info.frame.height + this.view.flxContainer.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0)
                    this.view.flxFooter.top = mainheight + diff + "dp";
                else
                    this.view.flxFooter.top = mainheight + "dp";
            } else {
                this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
            this.initializeResponsiveViews();
        },
        /**
         * Method to handle UI of account alerts
         */
        setAccountAlerts: function(alertsData) {
            this.view.settings.flxTransactionalAndPaymentsAlertsWrapper.setVisibility(false);
            this.view.settings.flxAlertsCommunication.setVisibility(false);
            this.view.settings.flxAlertsHeaderContainer.setVisibility(false);
            this.view.settings.flxVerticalSeperator1.setVisibility(false);
            this.view.settings.flxAccountAlertsWrapper.setVisibility(true);
            this.showViews(["flxAccountAlertsWrapper"]);
            if (applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true" || applicationManager.getConfigurationManager().getConfigurationValue('isSMEUser') === "true") {
                this.setAccountAlertsForCombinedAndBusinessUser(alertsData);
            } else if (applicationManager.getConfigurationManager().getAccountIDLevelAlertsFlag() === true) {
                this.setAccountAlertsByNumber(alertsData);
            } else {
                this.setAccountAlertsByType(alertsData);
            }
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },

        /**
         * Method to set account alerts when the user is a combined user
         * @param {String} alertsData- walerts view model
         */

        setAccountAlertsForCombinedAndBusinessUser: function(alerts) {
            var self = this;
            var alertsData = alerts.accountAlerts;
            var accounts = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.accounts;
            //var isCombinedUser = applicationManager.getConfigurationManager().isCombinedUser==="true";
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            var businessAccountList = [];
            var personalAccountList = [];
            var businessAlertList = [];
            var personalAlertList = [];
            var len = accounts.length;
            var i;

            var widgetdatamap = {
                "btnModifyAlerts": "btnModifyAlerts",
                "flxAccountAlertsCombinedUser": "flxAccountAlertsCombinedUser",
                "flxAccountAlertsHeaderCombinedUser": "flxAccountAlertsHeaderCombinedUser",
                "flxAccountDetails": "flxAccountDetails",
                "flxAccountName": "flxAccountName",
                "flxAlerts": "flxAlerts",
                "flxAlertsStatusCheckbox": "flxAlertsStatusCheckbox",
                "flxSeperator": "flxSeperator",
                "lblAccountIcon": "lblAccountIcon",
                "lblAccountName": "lblAccountName",
                "lblAccountType": "lblAccountType",
                "lblAlertStatusIndicator": "lblAlertStatusIndicator",
                "lblAlertsStatus": "lblAlertsStatus",
                "lblSeperator": "lblSeperator",
                "lblSeperatorDown": "lblSeperatorDown",
                "lblSeperatorTop": "lblSeperatorTop",
                "membershipName": "membershipName"
            };

            for (i = 0; i < len; i++) {
                if (accounts[i]["isBusinessAccount"] === "true")
                    businessAccountList.push(cloneJSON(accounts[i]["accountID"]));
                else
                    personalAccountList.push(cloneJSON(accounts[i]["accountID"]));
            }

            for (var row in alertsData) {
                var isBusiness = businessAccountList.includes(alertsData[row].accountID);
                if (isBusiness) {
                    var membershipName = businessAccountList.includes(alertsData[row].membershipName) ? alertsData[row].membershipName : null;
                }
                var temp = {
                    lblAccountName: {
                        text: alertsData[row].nickName,
                        "accessibilityconfig": {
                            "a11yLabel": alertsData[row].nickName,
                        },
                    },
                    btnModifyAlerts: {
                        text: kony.i18n.getLocalizedString("kony.alerts.viewModify"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("kony.alerts.viewModify"),
                        },
                        onClick: self.onViewModifyAlerts.bind(this, alertsData[row].accountID, alerts.alertID)
                    },
                    lblAlertStatusIndicator: {
                        skin: alertsData[row].isEnabled === "false" ? ViewConstants.SKINS.DEACTIVATE_STATUS_SKIN : ViewConstants.SKINS.ACTIVE_STATUS_SKIN
                    },
                    lblAlertsStatus: {
                        text: alertsData[row].isEnabled === "false" ? kony.i18n.getLocalizedString("i18n.Alerts.DisabledAlerts") : kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled"),
                        "accessibilityconfig": {
                            "a11yLabel": alertsData[row].isEnabled === "false" ? kony.i18n.getLocalizedString("i18n.Alerts.DisabledAlerts") : kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled"),
                        },
                    },
                    lblAccountIcon: {
                        //isVisible: isCombinedUser?true:false,
                        isVisible: this.profileAccess === "both" ? true : false,
                        text: isBusiness ? "r" : "s",
                        "accessibilityconfig": {
                            "a11yLabel": isBusiness ? kony.i18n.getLocalizedString("kony.pfm.business") : kony.i18n.getLocalizedString("kony.pfm.personal"),
                        },
                    },
                    lblAccountType: {
                        text: alertsData[row].accountType,
                        "accessibilityconfig": {
                            "a11yLabel": alertsData[row].accountType,
                        },
                    },
                    lblSeperator: {
                        "text": ".",
                        "accessibilityconfig": {
                            "a11yLabel": ".",
                        },
                    },
                    membershipName: membershipName,
                    isBusiness: isBusiness
                };
                if (isBusiness)
                    businessAlertList.push(temp);
                else
                    personalAlertList.push(temp);
            }
            if (personalAlertList.length != 0)
                var data = [
                    [{
                            "lblSeperatorTop": {
                                "text": ".",
                                "accessibilityconfig": {
                                    "a11yLabel": ".",
                                },
                            },
                            "lblSeperatorDown": {
                                "text": ".",
                                "accessibilityconfig": {
                                    "a11yLabel": ".",
                                },
                            },
                            "lblAccountType": {
                                "text": kony.i18n.getLocalizedString("kony.pfm.personal"),
                                "accessibilityconfig": {
                                    "a11yLabel": kony.i18n.getLocalizedString("kony.pfm.personal"),
                                },
                            }
                        },
                        personalAlertList
                    ]
                ];
            else
                var data = [];
            var businessAlerts = this.getBusinessAlertsDataWithSections(businessAlertList);
            data.push(businessAlerts[0]);
            self.view.settings.accountAlerts.segAlerts.sectionHeaderTemplate = "flxAccountAlertsHeaderCombinedUser";
            self.view.settings.accountAlerts.segAlerts.rowTemplate = "flxAccountAlertsCombinedUser";
            self.view.settings.accountAlerts.segAlerts.widgetDataMap = widgetdatamap;
            self.view.settings.accountAlerts.segAlerts.separatorThickness = 0;
            self.view.settings.accountAlerts.segAlerts.setData(data);
        },

        getBusinessAlertsDataWithSections: function(data) {
            var scopeObj = this;
            var finalData = {};
            var prioritizeAccountTypes = [];
            data.forEach(function(alert) {
                var accountType = "Personal Accounts";
                if (alert.isBusiness === true) {
                    if (kony.sdk.isNullOrUndefined(alert.membershipName))
                        accountType = "Business Accounts";
                    else
                        accountType = alert.membershipName;
                }

                if (finalData.hasOwnProperty(accountType)) {
                    if (finalData[accountType][1][finalData[accountType][1].length - 1].length === 0) {
                        finalData[accountType][1].pop();
                    }
                    finalData[accountType][1].push(alert);
                } else {
                    prioritizeAccountTypes.push(accountType);
                    finalData[accountType] = [{
                            "lblSeperatorTop": {
                                "text": ".",
                                "accessibilityconfig": {
                                    "a11yLabel": ".",
                                },
                            },
                            "lblSeperatorDown": {
                                "text": ".",
                                "accessibilityconfig": {
                                    "a11yLabel": ".",
                                },
                            },
                            "lblAccountType": {
                                "text": accountType,
                                "accessibilityconfig": {
                                    "a11yLabel": accountType,
                                },
                            },
                            template: "flxAccountAlertsHeaderCombinedUser"
                        },
                        [alert]
                    ];
                }
            });
            var alertsData = [];

            for (var key in prioritizeAccountTypes) {
                var accountType = prioritizeAccountTypes[key];
                if (finalData.hasOwnProperty(accountType)) {
                    alertsData.push(finalData[accountType]);
                }
            }
            return alertsData;
        },
        /**
         * Method to set account alerts when alerts are categorised w.r.t account type
         * @param {String} alertsData- walerts view model
         */
        setAccountAlertsByType: function(alerts) {
            var scopeObj = this;
            var dataMap = {
                flxAccountTypeAlerts: "flxAccountTypeAlerts",
                flxRow2: "flxRow2",
                lblAccountType: "lblAccountType",
                btnModifyAlerts: "btnModifyAlerts",
                flxAlertsRow1: "flxAlertsRow1",
                flxAlertsStatusCheckbox: "flxAlertsStatusCheckbox",
                lblAlertStatusIndicator: "lblAlertStatusIndicator",
                lblAlertsStatus: "lblAlertsStatus",
                flxSeperator: "flxSeperator",
                lblSeperator: "lblSeperator"
            };
            scopeObj.view.settings.accountAlerts.segAlerts.widgetDataMap = dataMap;
            var data = [];
            var alertsData = alerts.accountAlerts;
            for (var row in alertsData) {
                var temp = {
                    lblAccountType: {
                        text: alertsData[row].accountType,
                        "accessibilityconfig": {
                            "a11yLabel": alertsData[row].accountType,
                        },
                    },
                    btnModifyAlerts: {
                        text: kony.i18n.getLocalizedString("kony.alerts.viewModify"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("kony.alerts.viewModify"),
                        },
                        onClick: scopeObj.onViewModifyAlerts.bind(this, alertsData[row].accountTypeId, alerts.alertID)
                    },
                    lblAlertStatusIndicator: {
                        skin: alertsData[row].isEnabled === "false" ? ViewConstants.SKINS.DEACTIVATE_STATUS_SKIN : ViewConstants.SKINS.ACTIVE_STATUS_SKIN
                    },
                    lblAlertsStatus: {
                        text: alertsData[row].isEnabled === "false" ? kony.i18n.getLocalizedString("i18n.Alerts.DisabledAlerts") : kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled"),
                        "accessibilityconfig": {
                            "a11yLabel": alertsData[row].isEnabled === "false" ? kony.i18n.getLocalizedString("i18n.Alerts.DisabledAlerts") : kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled"),
                        },
                    },
                    lblSeperator: {
                        "text": ".",
                        "accessibilityconfig": {
                            "a11yLabel": ".",
                        },
                    },
                    rowTemplate: "flxAccountTypeAlerts"
                };
                data.push(temp);
            }
            scopeObj.view.settings.accountAlerts.segAlerts.setData(data);
        },
        onViewModifyAlerts: function(accountTypeId, alertID) {
            this.view.settings.flxTransactionalAndPaymentsAlertsWrapper.setVisibility(true);
            this.view.settings.flxAccountAlertsWrapper.setVisibility(false);
            var params = {
                "AlertCategoryId": alertID,
                "AccountId": accountTypeId
            };
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(params);
        },
        /**
         * Method to set account alerts when alerts are categorised w.r.t account number
         * @param {String} alertsData- walerts view model
         */
        setAccountAlertsByNumber: function(alerts) {
            var scopeObj = this;
            var alertsData = alerts.accountAlerts;
            var accountTypes = alertsData.map(function(dataItem) {
                return dataItem.accountType
            })
            accountTypes = accountTypes.filter(function(dataItem, i) {
                return accountTypes.indexOf(dataItem) == i
            })
            var finalOutput = {};
            accountTypes.forEach(function(accountType) {
                finalOutput[accountType] = alertsData.filter(function(dataItem) {
                    if (dataItem.accountType === accountType) return true;
                    else return false;
                });
            });
            var dataMap1 = {
                flxAccountTypeAlerts: "flxAccountTypeAlerts",
                flxRow2: "flxRow2",
                lblAccountType: "lblAccountType",
                btnModifyAlerts: "btnModifyAlerts",
                flxAlertsRow1: "flxAlertsRow1",
                flxAlertsStatusCheckbox: "flxAlertsStatusCheckbox",
                lblAlertStatusIndicator: "lblAlertStatusIndicator",
                lblAlertsStatus: "lblAlertsStatus",
                flxSeperator: "flxSeperator",
                lblSeperator: "lblSeperator",
                lblAccountName: "lblAccountName",
                lblAccountNumber: "lblAccountNumber"
            };
            scopeObj.view.settings.accountAlerts.segAlerts.widgetDataMap = dataMap1;
            var data = [];
            for (var typeModel in finalOutput) {
                var temp = {
                    lblAccountType: {
                        text: typeModel,
                        "accessibilityconfig": {
                            "a11yLabel": typeModel,
                        },
                    },
                    lblSeperator: {
                        "text": ".",
                        "accessibilityconfig": {
                            "a11yLabel": ".",
                        },
                    },
                    template: "flxAccountIdAlertsHeader"
                };
                data.push(temp);
                var rowData = finalOutput[typeModel];
                for (var row in rowData) {
                    var temp2 = {
                        lblAccountName: {
                            text: rowData[row].nickName,
                            "accessibilityconfig": {
                                "a11yLabel": rowData[row].nickName,
                            },
                        },
                        btnModifyAlerts: {
                            text: kony.i18n.getLocalizedString("kony.alerts.viewModify"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("kony.alerts.viewModify"),
                            },
                            onClick: scopeObj.onViewModifyAlertByID.bind(this, rowData[row].accountID, alerts.alertID)
                        },
                        lblAlertStatusIndicator: {
                            skin: rowData[row].isEnabled === "false" ? ViewConstants.SKINS.DEACTIVATE_STATUS_SKIN : ViewConstants.SKINS.ACTIVE_STATUS_SKIN
                        },
                        lblAlertsStatus: {
                            text: rowData[row].isEnabled === "false" ? kony.i18n.getLocalizedString("i18n.Alerts.DisabledAlerts") : kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled"),
                            "accessibilityconfig": {
                                "a11yLabel": rowData[row].isEnabled === "false" ? kony.i18n.getLocalizedString("i18n.Alerts.DisabledAlerts") : kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled"),
                            },
                        },
                        lblSeperator: {
                            "text": ".",
                            "accessibilityconfig": {
                                "a11yLabel": ".",
                            },
                        },
                        lblAccountNumber: {
                            "text": kony.i18n.getLocalizedString("i18n.common.accountNumber") + " " + rowData[row].accountID,
                            "accessibilityconfig": {
                                "a11yLabel": rowData[row].accountID,
                            },
                        },
                        template: "flxAccountIdAlerts"
                    };
                    data.push(temp2);
                }
            }
            scopeObj.view.settings.accountAlerts.segAlerts.setData(data);
        },
        onViewModifyAlertByID: function(accountID, alertID) {
            this.view.settings.flxTransactionalAndPaymentsAlertsWrapper.setVisibility(true);
            this.view.settings.flxAccountAlertsWrapper.setVisibility(false);
            var params = {
                "AlertCategoryId": alertID,
                "AccountId": accountID
            };
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(params);
        },
        /**
         * Method to trigger entry method of presentation layer basing on alert category ID
         * @param {String} alertID- alert category ID
         */
        navigateByAlertId: function(alertID, isAccountLevel) {
            if (isAccountLevel == true) {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAccountAlerts(alertID);
            } else {
                var params = {
                    "AlertCategoryId": alertID
                };
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(params);
            }
        },
        /**
         *setAlertsFlowActions- function to add actions for alerts flow
         */
        setAlertsFlowActions: function() {
            //functions for menu flow
            var scopeObj = this;
            this.view.settings.flxTransactionAndPaymentsAlerts.onClick = function() {
                //alert = "Transactional";
                //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlerts("Transactional");
                CommonUtilities.setText(scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading, scopeObj.view.settings.flxTransactionAndPaymentsAlerts.lblTransactionAndPaymentsAlerts.text, CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(scopeObj.view.settings.accountAlerts.lblAlertsHeading, scopeObj.view.settings.flxTransactionAndPaymentsAlerts.lblTransactionAndPaymentsAlerts.text, CommonUtilities.getaccessibilityConfig());
                scopeObj.expand(scopeObj.view.settings.flxAlertsSubMenu);
                scopeObj.setSelectedSkin("flxTransactionAndPaymentsAlerts");
                scopeObj.navigateByAlertId(scopeObj.view.settings.flxTransactionAndPaymentsAlerts.lblReferenceTPA.text);
            };
            this.view.settings.flxSecurityAlerts.onClick = function() {
                CommonUtilities.setText(scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading, scopeObj.view.settings.flxSecurityAlerts.lblSecurityAlerts.text, CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(scopeObj.view.settings.accountAlerts.lblAlertsHeading, scopeObj.view.settings.flxSecurityAlerts.lblSecurityAlerts.text, CommonUtilities.getaccessibilityConfig());
                scopeObj.expand(scopeObj.view.settings.flxAlertsSubMenu);
                scopeObj.setSelectedSkin("flxSecurityAlerts");
                scopeObj.navigateByAlertId(scopeObj.view.settings.flxSecurityAlerts.lblreferenceSA.text);
            };
            this.view.settings.flxPromotionalAlerts.onClick = function() {
                CommonUtilities.setText(scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading, scopeObj.view.settings.flxPromotionalAlerts.lblPromotionalAlerts.text, CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(scopeObj.view.settings.accountAlerts.lblAlertsHeading, scopeObj.view.settings.flxPromotionalAlerts.lblPromotionalAlerts.text, CommonUtilities.getaccessibilityConfig());
                scopeObj.expand(scopeObj.view.settings.flxAlertsSubMenu);
                scopeObj.setSelectedSkin("flxPromotionalAlerts");
                scopeObj.navigateByAlertId(scopeObj.view.settings.flxPromotionalAlerts.lblReferencePA.text);
            };
            this.view.settings.flxAccountAlerts.onClick = function() {
                CommonUtilities.setText(scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading, scopeObj.view.settings.flxAccountAlerts.lblAccountAlerts.text, CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(scopeObj.view.settings.accountAlerts.lblAlertsHeading, scopeObj.view.settings.flxAccountAlerts.lblAccountAlerts.text, CommonUtilities.getaccessibilityConfig());
                scopeObj.expand(scopeObj.view.settings.flxAlertsSubMenu);
                scopeObj.setSelectedSkin("flxAccountAlerts");
                scopeObj.navigateByAlertId(scopeObj.view.settings.flxAccountAlerts.lblReferenceAA.text);
            };
            this.view.settings.flxAlertCommunications.onClick = function() {
                CommonUtilities.setText(scopeObj.view.settings.lblAlertCommHeading, kony.i18n.getLocalizedString("i18n.alertSettings.Communication"), CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(scopeObj.view.settings.accountAlerts.lblAlertsHeading, kony.i18n.getLocalizedString("i18n.alertSettings.Communication"), CommonUtilities.getaccessibilityConfig());
                scopeObj.expand(scopeObj.view.settings.flxAlertsSubMenu);
                scopeObj.navigateToAlertComm();
            };
            this.view.settings.flxChannel1.onClick = function() {
                var info = scopeObj.view.settings.lblChannel1.info;
                scopeObj.view.settings.flxChannels.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                if (info && info.isSelected === true) {
                    scopeObj.view.settings.flxChannel1.skin = "slFbox";
                    scopeObj.view.settings.lblChannel1.info.isSelected = false;
                    scopeObj.view.settings.lblChannel1.skin = scopeObj.view.settings.lblChannel1.info.normalSkin;
                    scopeObj.view.settings.flxChannel2.skin = scopeObj.view.settings.flxChannel2.skin === "bbSknFlxffffffWithShadow2" ? "bbSknFlxffffffWithShadow1" : "slFbox";
                } else {
                    scopeObj.view.settings.flxChannel1.skin = "bbSknFlxffffffWithShadow1";
                    scopeObj.view.settings.lblChannel1.info.isSelected = true;
                    scopeObj.view.settings.lblChannel1.skin = scopeObj.view.settings.lblChannel1.info.selectedSkin;
                    scopeObj.view.settings.flxChannel2.skin = scopeObj.view.settings.flxChannel2.skin === "bbSknFlxffffffWithShadow1" ? "bbSknFlxffffffWithShadow2" : "slFbox";
                }
            };
            this.view.settings.flxChannel2.onClick = function() {
                var info = scopeObj.view.settings.lblChannel2.info;
                scopeObj.view.settings.flxChannels.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                if (info && info.isSelected === true) {
                    scopeObj.view.settings.flxChannel2.skin = "slFbox";
                    scopeObj.view.settings.lblChannel2.info.isSelected = false;
                    scopeObj.view.settings.lblChannel2.skin = scopeObj.view.settings.lblChannel2.info.normalSkin;
                    scopeObj.view.settings.flxChannel3.skin = scopeObj.view.settings.flxChannel3.skin === "bbSknFlxffffffWithShadow2" ? "bbSknFlxffffffWithShadow1" : "slFbox";
                } else {
                    scopeObj.view.settings.flxChannel2.skin = scopeObj.view.settings.flxChannel1.skin === "bbSknFlxffffffWithShadow1" ? "bbSknFlxffffffWithShadow2" : "bbSknFlxffffffWithShadow1";
                    scopeObj.view.settings.lblChannel2.info.isSelected = true;
                    scopeObj.view.settings.lblChannel2.skin = scopeObj.view.settings.lblChannel2.info.selectedSkin;
                    scopeObj.view.settings.flxChannel3.skin = scopeObj.view.settings.flxChannel3.skin === "bbSknFlxffffffWithShadow1" ? "bbSknFlxffffffWithShadow2" : "slFbox";
                }
            };
            this.view.settings.flxChannel3.onClick = function() {
                var info = scopeObj.view.settings.lblChannel3.info;
                scopeObj.view.settings.flxChannels.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                if (info && info.isSelected === true) {
                    scopeObj.view.settings.flxChannel3.skin = "slFbox";
                    scopeObj.view.settings.lblChannel3.info.isSelected = false;
                    scopeObj.view.settings.lblChannel3.skin = scopeObj.view.settings.lblChannel3.info.normalSkin;
                    scopeObj.view.settings.flxChannel4.skin = scopeObj.view.settings.flxChannel4.skin === "bbSknFlxffffffWithShadow2" ? "bbSknFlxffffffWithShadow1" : "slFbox";
                } else {
                    scopeObj.view.settings.flxChannel3.skin = scopeObj.view.settings.flxChannel2.skin !== "slFbox" ? "bbSknFlxffffffWithShadow2" : "bbSknFlxffffffWithShadow1";
                    scopeObj.view.settings.lblChannel3.info.isSelected = true;
                    scopeObj.view.settings.lblChannel3.skin = scopeObj.view.settings.lblChannel3.info.selectedSkin;
                    scopeObj.view.settings.flxChannel4.skin = scopeObj.view.settings.flxChannel4.skin === "bbSknFlxffffffWithShadow1" ? "bbSknFlxffffffWithShadow2" : "slFbox";
                }
            };
            this.view.settings.flxChannel4.onClick = function() {
                var info = scopeObj.view.settings.lblChannel4.info;
                scopeObj.view.settings.flxChannels.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                if (info && info.isSelected === true) {
                    scopeObj.view.settings.flxChannel4.skin = "slFbox";
                    scopeObj.view.settings.lblChannel4.skin = scopeObj.view.settings.lblChannel4.info.normalSkin;
                    scopeObj.view.settings.lblChannel4.info.isSelected = false;
                } else {
                    scopeObj.view.settings.flxChannel4.skin = scopeObj.view.settings.flxChannel3.skin !== "slFbox" ? "bbSknFlxffffffWithShadow2" : "bbSknFlxffffffWithShadow1";
                    scopeObj.view.settings.lblChannel4.info.isSelected = true;
                    scopeObj.view.settings.lblChannel4.skin = scopeObj.view.settings.lblChannel4.info.selectedSkin;
                }
            };
            this.view.settings.flxChannel1Tab.onClick = function() {
                var info = scopeObj.view.settings.lblChannel1Tab.info;
                scopeObj.view.settings.flxChannelsTab.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                if (info && info.isSelected === true) {
                    scopeObj.view.settings.flxChannel1Tab.skin = "slFbox";
                    scopeObj.view.settings.lblChannel1Tab.info.isSelected = false;
                    scopeObj.view.settings.lblChannel1Tab.skin = scopeObj.view.settings.lblChannel1Tab.info.normalSkin;
                    scopeObj.view.settings.flxChannel2Tab.skin = scopeObj.view.settings.flxChannel2Tab.skin === "bbSknFlxffffffWithShadow2" ? "bbSknFlxffffffWithShadow1" : "slFbox";
                } else {
                    scopeObj.view.settings.flxChannel1Tab.skin = "bbSknFlxffffffWithShadow1";
                    scopeObj.view.settings.lblChannel1Tab.info.isSelected = true;
                    scopeObj.view.settings.lblChannel1Tab.skin = scopeObj.view.settings.lblChannel1Tab.info.selectedSkin;
                    scopeObj.view.settings.flxChannel2Tab.skin = scopeObj.view.settings.flxChannel2Tab.skin === "bbSknFlxffffffWithShadow1" ? "bbSknFlxffffffWithShadow2" : "slFbox";
                }
            };
            this.view.settings.flxChannel2Tab.onClick = function() {
                var info = scopeObj.view.settings.lblChannel2Tab.info;
                scopeObj.view.settings.flxChannelsTab.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                if (info && info.isSelected === true) {
                    scopeObj.view.settings.flxChannel2Tab.skin = "slFbox";
                    scopeObj.view.settings.lblChannel2Tab.info.isSelected = false;
                    scopeObj.view.settings.lblChannel2Tab.skin = scopeObj.view.settings.lblChannel2Tab.info.normalSkin;
                    scopeObj.view.settings.flxChannel3Tab.skin = scopeObj.view.settings.flxChannel3Tab.skin === "bbSknFlxffffffWithShadow2" ? "bbSknFlxffffffWithShadow1" : "slFbox";
                } else {
                    scopeObj.view.settings.flxChannel2Tab.skin = scopeObj.view.settings.flxChannel1Tab.skin === "bbSknFlxffffffWithShadow1" ? "bbSknFlxffffffWithShadow2" : "bbSknFlxffffffWithShadow1";
                    scopeObj.view.settings.lblChannel2Tab.info.isSelected = true;
                    scopeObj.view.settings.lblChannel2Tab.skin = scopeObj.view.settings.lblChannel2Tab.info.selectedSkin;
                    scopeObj.view.settings.flxChannel3Tab.skin = scopeObj.view.settings.flxChannel3Tab.skin === "bbSknFlxffffffWithShadow1" ? "bbSknFlxffffffWithShadow2" : "slFbox";
                }
            };
            this.view.settings.flxChannel3Tab.onClick = function() {
                var info = scopeObj.view.settings.lblChannel3Tab.info;
                scopeObj.view.settings.flxChannelsTab.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                if (info && info.isSelected === true) {
                    scopeObj.view.settings.flxChannel3Tab.skin = "slFbox";
                    scopeObj.view.settings.lblChannel3Tab.info.isSelected = false;
                    scopeObj.view.settings.lblChannel3Tab.skin = scopeObj.view.settings.lblChannel3Tab.info.normalSkin;
                    scopeObj.view.settings.flxChannel4Tab.skin = scopeObj.view.settings.flxChannel4Tab.skin === "bbSknFlxffffffWithShadow2" ? "bbSknFlxffffffWithShadow1" : "slFbox";
                } else {
                    scopeObj.view.settings.flxChannel3Tab.skin = scopeObj.view.settings.flxChannel2Tab.skin !== "slFbox" ? "bbSknFlxffffffWithShadow2" : "bbSknFlxffffffWithShadow1";
                    scopeObj.view.settings.lblChannel3Tab.info.isSelected = true;
                    scopeObj.view.settings.lblChannel3Tab.skin = scopeObj.view.settings.lblChannel3Tab.info.selectedSkin;
                    scopeObj.view.settings.flxChannel4Tab.skin = scopeObj.view.settings.flxChannel4Tab.skin === "bbSknFlxffffffWithShadow1" ? "bbSknFlxffffffWithShadow2" : "slFbox";
                }
            };
            this.view.settings.flxChannel4Tab.onClick = function() {
                var info = scopeObj.view.settings.lblChannel4Tab.info;
                scopeObj.view.settings.flxChannelsTab.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                if (info && info.isSelected === true) {
                    scopeObj.view.settings.flxChannel4Tab.skin = "slFbox";
                    scopeObj.view.settings.lblChannel4Tab.info.isSelected = false;
                    scopeObj.view.settings.lblChannel4Tab.skin = scopeObj.view.settings.lblChannel4Tab.info.normalSkin;
                } else {
                    scopeObj.view.settings.flxChannel4Tab.skin = scopeObj.view.settings.flxChannel3Tab.skin !== "slFbox" ? "bbSknFlxffffffWithShadow2" : "bbSknFlxffffffWithShadow1";
                    scopeObj.view.settings.lblChannel4Tab.info.isSelected = true;
                    scopeObj.view.settings.lblChannel4Tab.skin = scopeObj.view.settings.lblChannel4Tab.info.selectedSkin;
                }
            };
            // Frequency listboxes actions
            this.view.settings.lstBoxFrequency1.onSelection = function() {
                scopeObj.displayBasedOnSelectedFrequency();
            };
            this.view.settings.lstBoxFrequency2.onSelection = function() {
                scopeObj.view.settings.lstBoxFrequency2.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
            };
            this.view.settings.lstBoxFrequency3.onSelection = function() {
                scopeObj.view.settings.lstBoxFrequency3.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
            };
            this.view.settings.lstBoxFrequency1Tab.onSelection = function() {
                scopeObj.displayBasedOnSelectedFrequency();
            };
            this.view.settings.lstBoxFrequency2Tab.onSelection = function() {
                scopeObj.view.settings.lstBoxFrequency2Tab.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
            };
            this.view.settings.lstBoxFrequency3Tab.onSelection = function() {
                scopeObj.view.settings.lstBoxFrequency3Tab.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
            };

            /*
                      //security alerts button actions
                      this.view.settings.securityAlerts.flxChannel1.onClick = function() {
                          scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.securityAlerts);
                      };
                      this.view.settings.securityAlerts.flxChannel2.onClick = function() {
                          scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.securityAlerts);
                      };
                      this.view.settings.securityAlerts.flxChannel3.onClick = function() {
                          scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.securityAlerts);
                      };
                      this.view.settings.promotionalAlerts.flxChannel1.onClick = function() {
                          scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.promotionalAlerts);
                      };
                      this.view.settings.promotionalAlerts.flxChannel2.onClick = function() {
                          scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.promotionalAlerts);
                      };
                      this.view.settings.promotionalAlerts.flxChannel3.onClick = function() {
                          scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.promotionalAlerts);
                      };
                      this.view.settings.flxAccountAlerts.onClick = function() {
                          alert = "Account";
                          kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlerts("Account");
                      };
                  */
        },
        /**
       *toggleChannel1Checkboxes- Method to toggle the checkbox on click of channels
       * @param {String} alertWidget - ID of the Channel
       *
      toggleChannel1Checkboxes: function(alertWidget, alert) {
          if (alertWidget.lblCheckBoxChannel1.text === "C")
              alertWidget.lblCheckBoxChannel1.text = "D";
          else
              alertWidget.lblCheckBoxChannel1.text = "C";
          var newData = alertWidget.segAlerts.data;
          var alertData = alert.alertTypes[0].alerts;
          for (var i = 0; i < alertData.length; i++) {
              if ((alertData[i].canSmsBeSelected === "true" || alertData[i].canSmsBeSelected === true) && (alertData[i].canBeSelected === "true" || alertData[i].canBeSelected === true) && (alertWidget.id === "securityAlerts" || newData[i].lblAlertsStatusCheckBox.text === "C")) {
                  newData[i].lblCheckBoxChannel1.text = alertWidget.lblCheckBoxChannel1.text;
              }
          }
          if(kony.application.getCurrentBreakpoint() === 1024){
              for(var j=0;j<newData.length;j++){
              newData[j].template = "flxAlertsTablet";
           }
          }
          alertWidget.segAlerts.setData(newData);
      },
      /**
       *toggleChannel2Checkboxes- Method to toggle the checkbox on click of channels
       * @param {String} alertWidget - ID of the Channel
       *
      toggleChannel2Checkboxes: function(alertWidget, alert) {
          if (alertWidget.lblCheckBoxChannel2.text === "C")
              alertWidget.lblCheckBoxChannel2.text = "D";
          else
              alertWidget.lblCheckBoxChannel2.text = "C";
          var newData = alertWidget.segAlerts.data;
          var alertData = alert.alertTypes[0].alerts;
          for (var i = 0; i < alertData.length; i++) {
              if ((alertData[i].canEmailBeSelected === "true" || alertData[i].canEmailBeSelected === true) && (alertData[i].canBeSelected === "true" || alertData[i].canBeSelected === true) && (alertWidget.id === "securityAlerts" || newData[i].lblAlertsStatusCheckBox.text === "C")) {
                  newData[i].lblCheckBoxChannel2.text = alertWidget.lblCheckBoxChannel2.text;
              }
          }
          if(kony.application.getCurrentBreakpoint() === 1024){
              for(var j=0;j<newData.length;j++){
              newData[j].template = "flxAlertsTablet";
           }
          }
          alertWidget.segAlerts.setData(newData);
      },
      /**
       *toggleChannel3Checkboxes- Method to toggle the checkbox on click of channels
       * @param {String} alertWidget - ID of the Channel
       *
      toggleChannel3Checkboxes: function(alertWidget, alert) {
          if (alertWidget.lblCheckBoxChannel3.text === "C")
              alertWidget.lblCheckBoxChannel3.text = "D";
          else
              alertWidget.lblCheckBoxChannel3.text = "C";
          var newData = alertWidget.segAlerts.data;
          var alertData = alert.alertTypes[0].alerts;
          for (var i = 0; i < alertData.length; i++) {
              if ((alertData[i].canPushBeSelected === "true" || alertData[i].canPushBeSelected === true) && (alertData[i].canBeSelected === "true" || alertData[i].canBeSelected === true) && (alertWidget.id === "securityAlerts" || newData[i].lblAlertsStatusCheckBox.text === "C")) {
                  newData[i].lblCheckBoxChannel3.text = alertWidget.lblCheckBoxChannel3.text;
              }
          }
          if(kony.application.getCurrentBreakpoint() === 1024){
              for(var j=0;j<newData.length;j++){
              newData[j].template = "flxAlertsTablet";
           }
          }
          alertWidget.segAlerts.setData(newData);
      },
*/
        /**
         * Method to navigate to terms and Conditions
         */
        NavToTermsAndConditions: function() {
            var currForm = this.view;
            currForm.flxTermsAndConditions.height = currForm.flxHeader.info.frame.height + currForm.flxContainer.info.frame.height + currForm.flxFooter.info.frame.height;
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.lblTermsAndConditions.setFocus(true);
        },
        /**
         * Method to close Terms and Conditions
         */
        CloseTermsAndConditions: function() {
            this.view.flxTermsAndConditions.setVisibility(false);
        },
        /**
         * Method to enable/disable the terms and condition button
         */
        SaveTermsAndConditions: function() {
            if (this.view.lblTCContentsCheckbox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                CommonUtilities.setText(this.view.settings.lblTCContentsCheckbox, ViewConstants.FONT_ICONS.CHECBOX_SELECTED, CommonUtilities.getaccessibilityConfig());
                this.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                this.enableButton(this.view.settings.btnEditAccountsSave);
            } else {
                CommonUtilities.setText(this.view.settings.lblTCContentsCheckbox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                this.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                this.disableButton(this.view.settings.btnEditAccountsSave);
            }
            this.view.flxTermsAndConditions.setVisibility(false);
        },
        /**
         * Method to change the the progress bar state
         * @param {Object} isLoading- Consists of the loading indicator status.
         */
        changeProgressBarState: function(isLoading) {
            if (isLoading) {
                FormControllerUtility.showProgressBar(this.view);
            } else {
                FormControllerUtility.hideProgressBar(this.view);
            }
        },
        updatePhoneNumber: function(editPhoneViewModel, formData) {
            var servicesToSend = [];
            /* editPhoneViewModel.services.forEach(function(account) {
                 if (!account.selected && formData.services[account.id]) {
                     servicesToSend.push({
                         id: account.id,
                         selected: true
                     });
                 } else if (account.selected && !formData.services[account.id]) {
                     servicesToSend.push({
                         id: account.id,
                         selected: false
                     });
                 }
             });
             formData.services = servicesToSend;*/
            formData.isTypeBusiness = editPhoneViewModel.isTypeBusiness;
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.editPhoneNumber(editPhoneViewModel.id, formData);
        },
        /**
         * Method to validate the phone umber after editing
         */
        validateEditPhoneNumberForm: function() {
            var phoneData = this.getEditPhoneFormData();
            if (!applicationManager.getValidationUtilManager().isValidPhoneNumber(phoneData.phoneNumber) || (phoneData.phoneNumber.length < 7 && phoneData.phoneNumber.length > 15)) {
                this.view.settings.flxError.setVisibility(true);
                CommonUtilities.setText(this.view.settings.lblError, kony.i18n.getLocalizedString("i18n.profile.notAValidPhoneNumber"), CommonUtilities.getaccessibilityConfig());
                return false;
            }
          if (!applicationManager.getValidationUtilManager().isValidPhoneNumberCountryCode(phoneData.phoneCountryCode)) {
			this.view.settings.flxError.setVisibility(true);
            CommonUtilities.setText(this.view.settings.lblError, kony.i18n.getLocalizedString("i18n.profile.enterPhoneNumberCountryCode"), CommonUtilities.getaccessibilityConfig());
                return false;
           } else {
                this.view.settings.flxError.setVisibility(false);
                return true;
            }
        },
        /**
         * Method to show error while updating email
         * @param {String} errorMessage- Message to be shown
         */
        showEmailError: function(errorMessage) {
            this.view.settings.flxErrorAddNewEmail.setVisibility(true);
            let errmsg = errorMessage.errorMessage ? errorMessage.errorMessage : errorMessage;
            CommonUtilities.setText(this.view.settings.CopylblError0e8bfa78d5ffc48, errmsg, CommonUtilities.getaccessibilityConfig());
        },
        /**
         * Method to error while updating the edit email scenario
         * @param {String} errorMessage- Message to be shown
         */
        showEditEmailError: function(errorMessage) {
            this.view.settings.flxErrorEditEmail.setVisibility(true);
            CommonUtilities.setText(this.view.settings.CopylblError0e702a95b68d041, errorMessage.errorMessage, CommonUtilities.getaccessibilityConfig());
        },
        /**
         * Method to show data after updating the New Address scenario
         */
        getUpdateAddressData: function() {
          var addrLine1 = (this.view.settings.tbxEditAddressLine1.text)?this.view.settings.tbxEditAddressLine1.text.trim():"";
          var addrLine2 = (this.view.settings.tbxEditAddressLine2.text)?this.view.settings.tbxEditAddressLine2.text.trim():""; 
          var Country_id = this.view.settings.lbxEditCountry.selectedKey;
          var Region_id = (this.view.settings.lbxEditState.selectedKey!== 'lbl1')?this.view.settings.lbxEditState.selectedKey:"";
          var City_id = (this.view.settings.txtEditCity.text)?this.view.settings.txtEditCity.text.trim():"";
          var ZipCode = (this.view.settings.tbxEditZipcode.text)?this.view.settings.tbxEditZipcode.text.trim():"";
          var Addr_type = (this.view.settings.lbxEditType.selectedKey)?this.view.settings.lbxEditType.selectedKey:"";
          return {
                addrLine1: addrLine1,
                addrLine2: addrLine2,
                Country_id: Country_id,
                countrySelected: Country_id,
                Region_id: Region_id,
                City_id: City_id,
                ZipCode: ZipCode,
                isPrimary: this.view.settings.lblEditSetAsPreferredCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                Addr_type: Addr_type
            };
        },
        /**
         * Method to show data related to New Email scenario
         */
        getNewEmailData: function() {
            this.view.customheader.headermenu.setFocus(true);
            return {
                value: this.view.settings.tbxEmailId.text,
                isPrimary: this.view.settings.lblMarkAsPrimaryEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                isAlertsRequired: this.view.settings.lblMarkAsAlertCommEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED
            };
        },
        /**
         * Method to reset the fields while adding email
         */
        resetAddEmailForm: function() {
            this.view.settings.tbxEmailId.text = "";
            this.view.settings.lblMarkAsPrimaryEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            CommonUtilities.setText(this.view.settings.lblMarkAsPrimaryEmailCheckBox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());

            this.view.settings.lblMarkAsAlertCommEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            CommonUtilities.setText(this.view.settings.lblMarkAsAlertCommEmailCheckBox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxErrorAddNewEmail.setVisibility(false);
            this.disableButton(this.view.settings.btnAddEmailIdAdd);
            //show /hide alert communication option
            if (this.enableSeparateContact === true) {
                this.view.settings.flxAlertCommAddEmail.setVisibility(true);
            } else {
                this.view.settings.flxAlertCommAddEmail.setVisibility(false);
            }
        },
         /**
         * Method to show error while updating address
         * @param {String} errorMessage- Message to be shown
         */
        showEditAddressError: function(errorMessage) {
            this.view.settings.flxErrorEditAddress.setVisibility(true);
            CommonUtilities.setText(this.view.settings.lblErrorEditAddress, errorMessage.errorMessage, CommonUtilities.getaccessibilityConfig());
        },
         /**
         * Method to show error while adding address
         * @param {String} errorMessage- Message to be shown
         */
        showAddAddressError: function(errorMessage) {
            this.view.settings.flxErrorAddAddress.setVisibility(true);
            CommonUtilities.setText(this.view.settings.lblErrortxt, errorMessage.errorMessage, CommonUtilities.getaccessibilityConfig());
        },
        /**
         * Method to enable/disable the button of terms and condition
         */
        closeTermsAndConditions: function() {
            this.view.flxTermsAndConditions.isVisible = false;
        },
        /**
         * Method to toggle the buttons of terms and condition
         */
        toggleTC: function() {
            CommonUtilities.toggleFontCheckbox(this.view.lblTCContentsCheckbox);
            this.enableButton(this.view.btnSave);
        },
        /**
         * Method on Click of button Save of TnC Popup
         */
        onSaveTnC: function() {
            var isEnabledTnCPop = CommonUtilities.isFontIconChecked(this.view.lblTCContentsCheckbox);
            var isEnabledTnC = CommonUtilities.isFontIconChecked(this.view.settings.lblTCContentsCheckbox);
            if (isEnabledTnC !== isEnabledTnCPop) {
                CommonUtilities.toggleFontCheckbox(this.view.settings.lblTCContentsCheckbox);
            }
            if (isEnabledTnCPop === true) {
                this.enableButton(this.view.settings.btnEditAccountsSave);
            } else {
                this.disableButton(this.view.settings.btnEditAccountsSave);
            }
            this.view.flxTermsAndConditions.isVisible = false;
        },
        /**
         * Method to manupulate the UI of profile management on post show
         */
        postShowProfileManagement: function() {
            var scopeObj = this;
            this.view.flxTCContentsCheckbox.onClick = this.toggleTC;
            this.view.flxClose.onClick = this.closeTermsAndConditions;
            this.view.btnCancel.onClick = this.closeTermsAndConditions;
            this.view.settings.lbxSelectLanguage.onSelection = this.disableorEnableSaveButton;
            this.view.btnSave.onClick = this.onSaveTnC;
            if (kony.os.deviceInfo().screenHeight >= "900") {
                var mainheight = 0;
                var screenheight = kony.os.deviceInfo().screenHeight;
                mainheight = this.view.customheader.info.frame.height;
                mainheight = mainheight + this.view.flxContainer.info.frame.height;
                var diff = screenheight - mainheight;
                if (diff > 0)
                    this.view.flxFooter.top = diff + "dp";
            }
            this.view.settings.btnChangeLanguageSave.onClick = function() {
                var langSelected = scopeObj.view.settings.lbxSelectLanguage.selectedKeyValue[1];
                CommonUtilities.setText(scopeObj.view.CustomChangeLanguagePopup.lblPopupMessage, kony.i18n.getLocalizedString("i18n.common.changeLanguageMessage") + " " + langSelected + "?", CommonUtilities.getaccessibilityConfig());
                scopeObj.view.flxChangeLanguage.isVisible = true;
                scopeObj.view.CustomChangeLanguagePopup.flxCross.onClick = function() {
                    scopeObj.view.flxChangeLanguage.isVisible = false;
                    scopeObj.view.forceLayout();
                }
                scopeObj.view.flxChangeLanguage.height = scopeObj.view.flxHeader.info.frame.height + scopeObj.view.flxContainer.info.frame.height + scopeObj.view.flxFooter.info.frame.height + "dp";
                scopeObj.view.forceLayout();
            };
            this.view.CustomChangeLanguagePopup.btnNo.onClick = function() {
                scopeObj.view.flxChangeLanguage.isVisible = false;
                scopeObj.view.forceLayout();
            };
            this.view.CustomChangeLanguagePopup.btnYes.onClick = function() {
                var localeCode = scopeObj.view.settings.lbxSelectLanguage.selectedKey;
                var langSelected = scopeObj.view.settings.lbxSelectLanguage.selectedKeyValue[1];
                kony.i18n.setCurrentLocaleAsync(localeCode, function() {
                    applicationManager.getStorageManager().setStoredItem("langObj", {
                        language: scopeObj.getBackendLanguage(langSelected)
                    });
                    applicationManager.getConfigurationManager().setLocaleAndDateFormat({
                        "data": {}
                    });
                    applicationManager.getNavigationManager().navigateTo("frmLoginLanguage");
                    scopeObj.showViews(["flxChangeLanguageWrapper"]);
                    scopeObj.setSelectedSkin("flxChangeLanguage");
                    scopeObj.view.customheader.customhamburger.updateTextsHamburger();
                }, function() {});
                scopeObj.view.flxChangeLanguage.isVisible = false;
                scopeObj.showViews(["flxChangeLanguageWrapper"]);
                scopeObj.setSelectedSkin("flxChangeLanguage");
                scopeObj.view.forceLayout();
            };
            this.view.customheader.imgKony.setFocus(true);
            this.view.settings.imgAddRadioBtnUS.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
            this.view.settings.imgAddRadioBtnInternational.src = ViewConstants.IMAGES.ICON_RADIOBTN;
            // UI code
            this.view.flxDeletePopUp.height = this.view.flxHeader.info.frame.height + this.view.flxContainer.info.frame.height + this.view.flxFooter.info.frame.height + "dp";
            // Checking if it is External Account edit as there is no service call hence because of async
            // nature of js init preshow and postshow gets called at later stage tha updateUI api.
            var extAccountData = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getEditExternalAccount();
            if (extAccountData && extAccountData.flow === 'editExternalAccounts') {
                this.goToEditExternalAccounts(extAccountData.data);
            }
            applicationManager.getNavigationManager().applyUpdates(this);
            this.setDefaultUserPhoto();
            this.AdjustScreen();
        },
        /**
         * Method to show a particular view among all
         * @param {String} views - ID of the view to be shown
         */
        showViews: function(views) {
            this.view.deviceRegistration.setVisibility(views && views.isComponent && views.componentName === "deviceRegistration");
			if(views && views.constructor === Array){
			  if(views[0] === "flxApprovalMatrixDetailsWrapper"){
				this.view.settings.flxContents.left = "2dp";
				this.view.settings.flxContents.width = "100%";
			  }
			  else{
				this.view.settings.flxContents.left = "2.23%";
				this.view.settings.flxContents.width = "95.6%";
			  }
			}
            if (this.currentVisiblePage) {
                this.view.settings[this.currentVisiblePage].isVisible = false;
            }
            if (views && views.constructor === Array) {
                this.view.settings[views[0]].isVisible = true;
                this.currentVisiblePage = views[0];
            }
            if (views && views[0] === "flxAlertsHeaderContainer") {
                this.view.settings.flxRight.setVisibility(false);
                this.view.settings.flxAlertsHeaderContainer.setVisibility(true);
                this.view.settings.flxVerticalSeperator1.setVisibility(true);
            } else {
                this.view.settings.flxRight.setVisibility(true);
                this.view.settings.flxAlertsHeaderContainer.setVisibility(false);
                this.view.settings.flxVerticalSeperator1.setVisibility(false);
            }
            this.view.settings.forceLayout();
            this.AdjustScreen();
        },
        showNameChangeRequest: function() {
            this.showViews(["flxNameChangeRequestWrapper"]);
        },
        /**
         * Method used to set the guidelines for name change request screen.
         */
        setNameChangeRequestGuidelines: function() {
            CommonUtilities.setText(this.view.settings.lblRules1, kony.i18n.getLocalizedString("i18n.ProfileManagement.GuidelinesNameRequest"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblRule1, kony.i18n.getLocalizedString("i18n.ProfileManagement.GuidelineNameRequest1"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblRule2, kony.i18n.getLocalizedString("i18n.ProfileManagement.GuidelineNameRequest2"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblRule3, kony.i18n.getLocalizedString("i18n.ProfileManagement.GuidelineNameRequest3"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxRule4.setVisibility(false);
        },
        /**
         * Method to Check Dots in Username Entered by the User
         * @param {String} enteredUserName - username entered by the User
         */
        checkDot: function(enteredUserName) {
            var count = 0;
            if (enteredUserName.charAt(0) === "." || enteredUserName.charAt(enteredUserName.length - 1) === ".") return false;
            var index = enteredUserName.indexOf(".");
            for (var i = index + 1; i < enteredUserName.length - 1; i++) {
                if (enteredUserName[i] === ".") {
                    return false;
                }
            }
            return true;
        },
        /**
         * Method to check whether the username is valid or not
         * @param {String} enteredUserName - username entered by the User
         */
        isUserNameValid: function(enteredUserName) {
            var validationUtility = applicationManager.getValidationUtilManager();
            var isValidUsername = validationUtility.isUsernameValidForPolicy(enteredUserName);
            if (isValidUsername) {
                return true;
            }
            return false;
        },
        /**
         * Method to Check whether the password is valid or not
         */
        isPasswordValid: function(enteredPassword) {
            var validationUtility = applicationManager.getValidationUtilManager();
            var isValidPassword = validationUtility.isPasswordValidForPolicy(enteredPassword);
            var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
            if (isValidPassword && !this.hasConsecutiveDigits(enteredPassword) && enteredPassword !== userName && this.view.settings.tbxExistingPassword.text.length > 0) {
                return true;
            }
            return false;
        },
        /**
         * Method to Check whether the entered text has consecutive digits or not
         * @param {Int} input - field entered by the User
         */
        hasConsecutiveDigits: function(input) {
            var i;
            var count = 0;
            for (i = 0; i < input.length; i++) {
                // alert(abc[i]);
                if (input[i] >= 0 && input[i] <= 9)
                    count++;
                else
                    count = 0;
                if (count === 9)
                    return true;
            }
            return false;
        },
        /**
         * Method to Check whether the username is valid or not and if valid then enable/disable the button
         */
        ValidateUsername: function() {
            if (this.isUserNameValid(this.view.settings.tbxUsername.text)) {
                this.enableButton(this.view.settings.btnEditUsernameProceed);
                this.view.settings.flxErrorEditUsername.setVisibility(false);
            } else {
                this.disableButton(this.view.settings.btnEditUsernameProceed);
            }
        },
        showEditUsername: function() {
            this.showViews(["flxEditUsernameWrapper"]);
        },
        /**
         * Method to verify OTP by hitting backend
         */
        verifyOtp: function() {
            if (this.view.settings.tbxEnterOTP.text !== "" && this.view.settings.tbxEnterOTP.text != null) {
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.verifyOtp(this.view.settings.tbxEnterOTP.text);
            }
        },
        showAddNewEmail: function() {
            this.showViews(["flxAddNewEmailWrapper"]);
        },
        showEditAddress: function() {
            this.showViews(["flxEditAddressWrapper"]);
        },
        showAccounts: function() {
            this.showViews(["flxAccountsWrapper"]);
        },
        showEditAccounts: function() {
            this.showViews(["flxEditAccountsWrapper"]);
        },
        showAcknowledgement: function() {
            this.showViews(["flxAcknowledgementWrapper"]);
        },
        /**
         * Method to Update the next step according to the the flag UPDATE
         */
        updateRequirements: function() {
            this.view.settings.flxErrorSecuritySettingsVerification.setVisibility(false);
            this.view.settings.flxErrorEditSecurityQuestions.setVisibility(false);
            var self = this;
            FormControllerUtility.showProgressBar(self.view);
            if (update === "password")
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updatePassword(self.view.settings.tbxExistingPassword.text, self.view.settings.tbxNewPassword.text);
            else if (update === "username")
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateUsername(self.view.settings.tbxUsername.text);
            else
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.requestOtp(self.onSaveSecurityQuestions());
        },
        /**
         * Method to Update the acknowledgment according to the the flag UPDATE
         */
        acknowledge: function(viewModel) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            if (update === "password") {
                this.view.settings.btnAcknowledgementDone.onClick = function() {
                    scopeObj.showUsernameAndPassword();
                    scopeObj.setSelectedSkin("flxUsernameAndPassword");
                }
                CommonUtilities.setText(scopeObj.view.settings.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.ProfileManagement.passwordUpdatedSuccessfully"), CommonUtilities.getaccessibilityConfig());
            } else if (update === "username") {
                this.view.settings.btnAcknowledgementDone.onClick = function() {
                    scopeObj.showUsernameAndPassword();
                    scopeObj.setSelectedSkin("flxUsernameAndPassword");
                }
                kony.mvc.MDAApplication.getSharedInstance().appContext.username = this.view.settings.tbxUsername.text;
                CommonUtilities.setText(scopeObj.view.settings.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.ProfileManagement.usernameUpdatedSuccess"), CommonUtilities.getaccessibilityConfig());
            } else {
                this.view.settings.btnAcknowledgementDone.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
                    scopeObj.setSelectedSkin("flxSecurityQuestions");
                }
                CommonUtilities.setText(scopeObj.view.settings.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.ProfileManagement.YourSecurityQuestionswereupdatedSuccessfully"), CommonUtilities.getaccessibilityConfig());
            }
            scopeObj.showAcknowledgement();
            update = "";
            FormControllerUtility.hideProgressBar(scopeObj.view);
        },
        showOTP: function() {
            this.view.settings.tbxEnterOTP.secureTextEntry = false;
        },
        /**
         * Method to show secure access options
         */
        showSecureAccessOptions: function(viewModel) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            if (viewModel.isPhoneEnabled === "true") {
                CommonUtilities.setText(scopeObj.view.settings.lblPhoneUnchecked, ViewConstants.FONT_ICONS.CHECBOX_SELECTED, CommonUtilities.getaccessibilityConfig());
                scopeObj.view.settings.lblPhoneUnchecked.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
            } else {
                CommonUtilities.setText(scopeObj.view.settings.lblPhoneUnchecked, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                scopeObj.view.settings.lblPhoneUnchecked.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            }
            if (viewModel.isEmailEnabled === "true") {
                CommonUtilities.setText(scopeObj.view.settings.lblemailunchecked, ViewConstants.FONT_ICONS.CHECBOX_SELECTED, CommonUtilities.getaccessibilityConfig());
                scopeObj.view.settings.lblemailunchecked.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
            } else {
                CommonUtilities.setText(scopeObj.view.settings.lblemailunchecked, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                scopeObj.view.settings.lblemailunchecked.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            }
            scopeObj.view.settings.flxPhoneUnchecked.onClick = function() {};
            scopeObj.view.settings.flxemailunchecked.onClick = function() {};
            scopeObj.view.settings.flxPhoneUnchecked.setEnabled = false;
            scopeObj.view.settings.flxemailunchecked.setEnabled = false;
            scopeObj.view.settings.lblPhoneUnchecked.setEnabled = false;
            scopeObj.view.settings.lblemailunchecked.setEnabled = false;
            scopeObj.setSelectedSkin("flxSecureAccessCode");
            scopeObj.collapseAll();
            scopeObj.expandWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
            scopeObj.view.settings.lblSecuritySettingsCollapse.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
            scopeObj.showSecureAccessCode();
            scopeObj.view.forceLayout();
            FormControllerUtility.hideProgressBar(scopeObj.view);
        },
        /**
         * Method to handle the Enter OTP flow
         */
        enterOtp: function() {
            this.setFlowActions();
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            scopeObj.view.settings.btnSecuritySettingVerificationCancel.setEnabled(false);
            scopeObj.view.settings.btnSecuritySettingVerificationCancel.skin = ViewConstants.SKINS.BUTTON_BLOCKED;
            scopeObj.view.settings.btnSecuritySettingVerificationCancel.hoverSkin = ViewConstants.SKINS.BUTTON_BLOCKED;
            this.resendOtpTimer = setTimeout(
                function() {
                    scopeObj.view.settings.btnSecuritySettingVerificationCancel.setEnabled(true);
                    scopeObj.view.settings.btnSecuritySettingVerificationCancel.skin = ViewConstants.SKINS.NORMAL;
                    scopeObj.view.settings.btnSecuritySettingVerificationCancel.hoverSkin = ViewConstants.SKINS.HOVER;
                }, 10000);
            CommonUtilities.setText(scopeObj.view.settings.lblSendingOTP, kony.i18n.getLocalizedString("i18n.Enrollnow.EnterOTPMsg"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.settings.lblSendingOTPMessage, kony.i18n.getLocalizedString("i18n.ProfileManagement.IfYouHavenotRecievedOTP"), CommonUtilities.getaccessibilityConfig());
            scopeObj.view.settings.flxOTPtextbox.setVisibility(true);
            scopeObj.view.settings.tbxEnterOTP.text = "";
            scopeObj.view.settings.btnSecuritySettingVerificationCancel.toolTip = kony.i18n.getLocalizedString("i18n.login.ResendOtp");
            scopeObj.view.settings.btnSecuritySettingVerificationSend.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
            CommonUtilities.setText(scopeObj.view.settings.btnSecuritySettingVerificationCancel, kony.i18n.getLocalizedString("i18n.login.ResendOtp"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.settings.btnSecuritySettingVerificationSend, kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify"), CommonUtilities.getaccessibilityConfig());
            scopeObj.view.settings.btnSecuritySettingVerificationSend.onClick = function() {
                scopeObj.hideOTP();
                scopeObj.verifyOtp();
            };
            if (CommonUtilities.isCSRMode()) {
                scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = CommonUtilities.disableButtonActionForCSRMode();
                scopeObj.view.settings.btnSecuritySettingVerificationCancel.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = function() {
                    if (update === "username" || update === "password") {
                        scopeObj.showUsernameAndPassword();
                        scopeObj.setSelectedSkin("flxUsernameAndPassword");
                        scopeObj.view.forceLayout();
                    } else {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
                        scopeObj.setSelectedSkin("flxSecurityQuestions");
                        scopeObj.view.forceLayout();
                    }
                };
            }
            scopeObj.showSecuritySettingVerification();
            scopeObj.disableButton(scopeObj.view.settings.btnSecuritySettingVerificationSend);
            if (scopeObj.view.settings.btnSecuritySettingVerificationSend.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify")) {
                scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = function() {
                    FormControllerUtility.showProgressBar(scopeObj.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.requestOtp();
                };
            }
            FormControllerUtility.hideProgressBar(scopeObj.view);
            scopeObj.view.forceLayout();
        },
        /**
         * Method to check the validation of username
         */
        checkUsername: function() {
            var self = this;
            if (this.view.settings.tbxUsername.text.length >= 7 && this.isUserNameValid(this.view.settings.tbxUsername.text) && (this.view.settings.tbxUsername.text !== kony.mvc.MDAApplication.getSharedInstance().appContext.username))
                this.enableButton(self.view.settings.btnEditUsernameProceed);
            else
                this.disableButton(self.view.settings.btnEditUsernameProceed);
        },
        /**
         * Method to check the validation of OTP
         */
        checkOTP: function() {
            if (CommonUtilities.isCSRMode())
                CommonUtilities.disableButtonActionForCSRMode();
            else {
                if (this.view.settings.tbxEnterOTP.text.length >= 6)
                    this.enableButton(this.view.settings.btnSecuritySettingVerificationSend);
            }
        },
        showUsernameVerification: function() {
            this.showViews(["flxUsernameVerificationWrapper2"]);
        },
        showAnswerSecurityQuestions: function() {
            this.showViews(["flxAnswerSecurityQuestionsWrapper"]);
        },
        showSecureAccessCode: function() {
            this.showViews(["flxSecureAccessCodeWrapper"]);
            flag = 1;
        },
        /**
         * Method to enable/disable button based on the email entered
         */
        checkAddEmailForm: function() {
            if (applicationManager.getValidationUtilManager().isValidEmail(this.view.settings.tbxEmailId.text)) {
                this.enableButton(this.view.settings.btnAddEmailIdAdd);
            } else {
                this.disableButton(this.view.settings.btnAddEmailIdAdd);
            }
        },
        /**
         * Method to set the validation function while entering email
         */
        setAddEmailValidationActions: function() {
            var scopeObj = this;
            this.disableButton(this.view.settings.btnAddEmailIdAdd);
            this.view.settings.tbxEmailId.onKeyUp = function() {
                scopeObj.checkAddEmailForm();
            };
        },
        /**
         * Method to set the validation function while editing email
         */
        setUpdateEmailValidationActions: function() {
            var scopeObj = this;
            this.disableButton(this.view.settings.btnEditEmailIdSave);
            this.view.settings.tbxEditEmailId.onKeyUp = function() {
                scopeObj.checkUpdateEmailForm();
            };
        },
        /**
         * Method to validate the edited address
         */
        checkUpdateAddressForm: function() {
            if (!CommonUtilities.isCSRMode()) {
                var addAddressFormData = this.getUpdateAddressData();
                if (addAddressFormData.addrLine1 === '') {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } else if (addAddressFormData.ZipCode === '') {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } else if (addAddressFormData.Country_id === "1") {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } /*else if (addAddressFormData.Region_id === "lbl1") {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                }*/ else if (addAddressFormData.City_id === "") {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } else {
                    this.enableButton(this.view.settings.btnEditAddressSave);
                }
            }
        },
        /**
         * Method to assign validation action on the address fields
         */
        setNewAddressValidationActions: function() {
            this.disableButton(this.view.settings.btnAddNewAddressAdd);
            this.view.settings.tbxAddressLine1.onKeyUp = this.checkNewAddressForm.bind(this);
            this.view.settings.tbxAddressLine2.onKeyUp = this.checkNewAddressForm.bind(this);
            this.view.settings.tbxZipcode.onKeyUp = this.checkNewAddressForm.bind(this);
            this.view.settings.txtCity.onKeyUp = this.checkNewAddressForm.bind(this);
        },
        /**
         * Method to assign validation action on the edit address fields
         */
        setUpdateAddressValidationActions: function() {
            this.disableButton(this.view.settings.btnEditAddressSave);
            this.view.settings.tbxEditAddressLine1.onKeyUp = this.checkUpdateAddressForm.bind(this);
            this.view.settings.tbxEditAddressLine2.onKeyUp = this.checkUpdateAddressForm.bind(this);
            this.view.settings.tbxEditZipcode.onKeyUp = this.checkUpdateAddressForm.bind(this);
            this.view.settings.txtEditCity.onKeyUp = this.checkUpdateAddressForm.bind(this);
        },
        /**
         * Method to assign validation action on the edit address fields
         */
        getBase64: function(file, successCallback) {
            var reader = new FileReader();
            reader.onloadend = function() {
                successCallback(reader.result);
            };
            reader.readAsDataURL(file);
        },
        /**
         * Method to assign validation action on the new phone entered fields
         */
        setAddNewPhoneValidationActions: function() {
            this.view.settings.tbxAddPhoneNumber.onKeyUp = this.checkAddNewPhoneForm.bind(this);
        },
        /**
         * Method to assign validation action while editing phone number  fields
         */
        setEditNewPhoneValidationActions: function() {
            this.view.settings.tbxPhoneNumber.onKeyUp = this.checkAddNewPhoneForm.bind(this);
        },
        /**
         * Method to assign validation action on the edit address fields
         */
        selectedFileCallback: function(events, files) {
            var scopeObj = this;
            if (files[0].file.type === "image/jpeg" || files[0].file.type === "image/png" || files[0].file.type === "image/jpg") {
                var image = files[0].file.size / 1048576;
                if (image <= 2) {
                    this.getBase64(files[0].file, function(base64String) {
                        var base64 = base64String.replace(/data:image\/(png|jpeg);base64\,/, "");
                        FormControllerUtility.showProgressBar(self.view);
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.userImageUpdate(base64);
                    });
                } else {
                    this.view.settings.flxImageError.setVisibility(true);
                    CommonUtilities.setText(this.view.settings.lblImageError, kony.i18n.getLocalizedString("i18n.profile.Imagesize"), CommonUtilities.getaccessibilityConfig());
                    this.view.forceLayout();
                }
            } else {
                this.view.settings.flxImageError.setVisibility(true);
                CommonUtilities.setText(this.view.settings.lblImageError, kony.i18n.getLocalizedString("i18n.profile.notAValidImage"), CommonUtilities.getaccessibilityConfig());
                this.view.forceLayout();
            }
        },
        /**
         * Method to assign Profile image 
         */
        bindUploadedImage: function(userImage) {
            if (applicationManager.getConfigurationManager().getProfileImageAvailabilityFlag() === true && userImage) {
                this.view.settings.imgProfile.base64 = userImage;
                this.view.settings.btnEditPhoto.isVisible = true;
                this.view.settings.btnDeletephoto.isVisible = true;
                this.view.settings.btnAddPhoto.isVisible = false;
                this.view.settings.flxWhatIsSSN.isVisible = false;
                this.view.forceLayout();
            } else if (applicationManager.getConfigurationManager().getProfileImageAvailabilityFlag() === false) {
                this.view.settings.imgProfile.src = ViewConstants.IMAGES.USER_GREY_IMAGE;
                this.view.settings.btnEditPhoto.isVisible = false;
                this.view.settings.btnDeletephoto.isVisible = false;
                this.view.settings.btnAddPhoto.isVisible = false;
                this.view.settings.flxWhatIsSSN.isVisible = false;
                this.view.forceLayout();
            } else {
                this.view.settings.imgProfile.src = ViewConstants.IMAGES.USER_GREY_IMAGE;
                this.view.settings.btnEditPhoto.isVisible = false;
                this.view.settings.btnDeletephoto.isVisible = false;
                this.view.settings.btnAddPhoto.isVisible = true;
                this.view.settings.flxWhatIsSSN.isVisible = true;
                this.view.forceLayout();
            }
            this.view.customheader.setupUserProfile();
            if (!applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")) {
                this.view.settings.btnEditPhoto.isVisible = false;
                this.view.settings.btnDeletephoto.isVisible = false;
                this.view.settings.btnAddPhoto.isVisible = false;
                this.view.settings.flxWhatIsSSN.isVisible = false;
                this.view.forceLayout();
            }
        },
        /**
         * Method to assign the general flow action throughout the profile  module
         */
        setFlowActions: function() {
            var scopeObj = this;
            var self = this;
            this.view.settings.lbxQuestion1.onSelection = this.setQuestionForListBox1.bind(this);
            this.view.settings.lbxQuestion2.onSelection = this.setQuestionForListBox2.bind(this);
            this.view.settings.lbxQuestion3.onSelection = this.setQuestionForListBox3.bind(this);
            this.view.settings.lbxQuestion4.onSelection = this.setQuestionForListBox4.bind(this);
            this.view.settings.lbxQuestion5.onSelection = this.setQuestionForListBox5.bind(this);
            this.view.settings.tbxAnswer1.onBeginEditing = this.onEditingAnswer1.bind(this);
            this.view.settings.tbxAnswer2.onBeginEditing = this.onEditingAnswer2.bind(this);
            this.view.settings.tbxAnswer3.onBeginEditing = this.onEditingAnswer3.bind(this);
            this.view.settings.tbxAnswer4.onBeginEditing = this.onEditingAnswer4.bind(this);
            this.view.settings.tbxAnswer5.onBeginEditing = this.onEditingAnswer5.bind(this);
            this.view.settings.tbxAnswer1.onEndEditing = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer2.onEndEditing = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer3.onEndEditing = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer4.onEndEditing = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer5.onEndEditing = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer1.onKeyUp = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer2.onKeyUp = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer3.onKeyUp = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer4.onKeyUp = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer5.onKeyUp = function() {
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.btnEditSecuritySettingsCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
                self.setSelectedSkin("flxSecurityQuestions");
            };
            this.view.settings.btnEditSecuritySettingsProceed.onClick = this.onProceedSQ.bind(this);
            this.view.settings.tbxEnterOTP.onKeyUp = this.checkOTP.bind(this);
            FormControllerUtility.showProgressBar(scopeObj.view);
            if (!CommonUtilities.isCSRMode()) {
                this.setAddEmailValidationActions();
                this.setUpdateEmailValidationActions();
                this.setNewAddressValidationActions();
                this.setUpdateAddressValidationActions();
                this.setAddNewPhoneValidationActions();
                this.setEditNewPhoneValidationActions();
            }
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnAddPhoto.setEnabled(false);
                this.view.settings.btnAddPhoto.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
            } else {
                this.view.settings.btnAddPhoto.onClick = function() {
                    scopeObj.view.settings.flxImageError.setVisibility(false);
                    if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                        this.onClickChooseFromGallery();
                    } else {
                        var config = {
                            selectMultipleFiles: true,
                            filter: ["image/png", "image/jpeg"]
                        };
                        kony.io.FileSystem.browse(config, this.selectedFileCallback.bind(this));
                    }

                }.bind(this);
            }
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnEditPhoto.setEnabled(false);
                this.view.settings.btnEditPhoto.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
            } else {
                this.view.settings.btnEditPhoto.onClick = function() {
                    scopeObj.view.settings.flxImageError.setVisibility(false);
                    var config = {
                        selectMultipleFiles: true,
                        filter: ["image/png", "image/jpeg"]
                    };
                    kony.io.FileSystem.browse(config, this.selectedFileCallback.bind(this));
                }.bind(this);
            }
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnDeletephoto.setEnabled(false);
                this.view.settings.btnDeletephoto.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
            } else {
                this.view.settings.btnDeletephoto.onClick = function() {
                    scopeObj.view.settings.flxImageError.setVisibility(false);
                    scopeObj.view.flxProfileDeletePopUp.isVisible = true;
                }
            }
            //Menu flow
            this.view.settings.flxProfile.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserProfile();
            };
            this.view.settings.flxPhone.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserPhones();
            };
            this.view.settings.flxEmail.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserEmail();
            };
            this.view.settings.flxAddress.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserAddresses();
            };
            this.view.settings.flxUsernameAndPassword.onClick = function() {
                scopeObj.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
                scopeObj.showUsernameAndPassword();
                scopeObj.setSelectedSkin("flxUsernameAndPassword");
            };
            this.view.settings.flxEBankingAccess.onClick = function() {
                scopeObj.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
                scopeObj.showEBankingAccess();
                scopeObj.setSelectedSkin("flxEBankingAccess");
            };
            this.view.settings.flxChangeLanguage.onClick = function() {
                scopeObj.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
                scopeObj.showViews(["flxChangeLanguageWrapper"]);
                scopeObj.setSelectedSkin("flxChangeLanguage");
                scopeObj.view.forceLayout();
            };
          	this.view.settings.flxRegisterDevices.onClick = function() {
                scopeObj.showRegisteredDevices();
            };
            this.view.settings.flxSecurityQuestions.onClick = function() {
                scopeObj.showSecurityQuestionsScreen();
            };
            this.view.settings.flxSecureAccessCode.onClick = this.showSecureAccessSettings.bind(this);
            this.view.settings.flxAccountPreferences.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getPreferredAccounts();
            };
            this.view.settings.flxSetDefaultAccount.onClick = function() {
                var configurationManager = applicationManager.getConfigurationManager();
                FormControllerUtility.showProgressBar(scopeObj.view);
                scopeObj.showDefaultScreen();
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getDefaultUserProfile();
                if (configurationManager.isFastTransferEnabled == "true") {
                    scopeObj.view.settings.flxTransfers.setVisibility(false);
                    scopeObj.view.settings.flxPayAPerson.setVisibility(false);
                }
            };
            this.view.settings.btnNameChangeRequest.onClick = function() {
                scopeObj.setNameChangeRequestGuidelines();
                scopeObj.showNameChangeRequest();
            };
            this.view.settings.btnAddNewNumber.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAddPhoneNumberView();
            };
            this.view.settings.btnAddNewPersonalNumber.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAddPhoneNumberView();
            };
            this.view.settings.btnAddPhoneNumberCancel.onClick = function() {
                scopeObj.showPhoneNumbers();
            };
            this.view.settings.btnAddPhoneNumberSave.onClick = function() {
                //Add code for Saving the Phone Number
                scopeObj.showPhoneNumbers();
            };
            this.view.settings.btnEditPhoneNumberCancel.onClick = function() {
                scopeObj.showPhoneNumbers();
            };
            this.view.settings.btnEditPhoneNumberSave.onClick = function() {
                //Add code for saving the edited Phone Number
                scopeObj.showPhoneNumbers();
            };
            this.view.settings.btnEditCommAlerts.onClick = function() {
                scopeObj.showEditAlertCommunications();
            };
            this.view.settings.btnAlertCommSave.onClick = function() {
                scopeObj.saveAlertCommunications();
            };
            this.view.settings.btnAlertCommCancel.onClick = function() {
                scopeObj.showAlertCommView();
            };
            this.view.settings.flxPhoneDropDown.onClick = function() {
                if (scopeObj.view.settings.flxPhoneNumbersSegment.isVisible) {
                    scopeObj.view.settings.flxPhoneNumbersSegment.setVisibility(false);
                    scopeObj.view.settings.lblImgDropdown11.text = "O";
                } else {
                    scopeObj.view.settings.flxPhoneNumbersSegment.setVisibility(true);
                    scopeObj.view.settings.lblImgDropdown11.text = "P";
                }
            };
            this.view.settings.flxEmailDropDown.onClick = function() {
                if (scopeObj.view.settings.flxEmailsSegment.isVisible) {
                    scopeObj.view.settings.flxEmailsSegment.setVisibility(false);
                    scopeObj.view.settings.lblImgDropdown21.text = "O";
                } else {
                    scopeObj.view.settings.flxEmailsSegment.setVisibility(true);
                    scopeObj.view.settings.lblImgDropdown21.text = "P";
                }
            };
            this.view.settings.segPhoneComm.onRowClick = function() {
                scopeObj.setSelectedContactInfo(scopeObj.view.settings.segPhoneComm, scopeObj.view.settings.lblSelectPhoneNumber);
                scopeObj.view.settings.flxPhoneNumbersSegment.setVisibility(false);
                scopeObj.view.settings.lblImgDropdown11.text = "O";
            };
            this.view.settings.segEmailComm.onRowClick = function() {
                scopeObj.setSelectedContactInfo(scopeObj.view.settings.segEmailComm, scopeObj.view.settings.lblSelectEmailNumber);
                scopeObj.view.settings.flxEmailsSegment.setVisibility(false);
                scopeObj.view.settings.lblImgDropdown21.text = "O";
            };
            this.view.flxDeleteClose.onClick = function() {
                scopeObj.view.flxDelete.setFocus(true);
                scopeObj.view.flxDeletePopUp.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.btnDeleteNo.onClick = function() {
                scopeObj.view.flxDelete.setFocus(true);
                scopeObj.view.flxDeletePopUp.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.settings.flxWhatIsSSN.onClick = function() {
                scopeObj.view.settings.ProfileInfo.isVisible = true;
                scopeObj.view.settings.ProfileInfo.left = "10%";
                scopeObj.view.settings.ProfileInfo.top = "270dp";
                scopeObj.view.forceLayout();
            };
            this.view.settings.ProfileInfo.flxCross.onClick = function() {
                scopeObj.view.settings.ProfileInfo.isVisible = false;
                scopeObj.view.forceLayout();
            };
            this.view.settings.flxCheckBox3.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblCheckBox3);
            };
            this.view.settings.flxCheckBox4.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblCheckBox4);
            };
            this.view.settings.flxAddCheckBox3.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblAddCheckBox3);
            };
            this.view.settings.flxAddCheckBox4.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblAddCheckBox4);
            };
            this.view.settings.flxRadioBtnInternational.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgRadioBtnInternational, scopeObj.view.settings.imgRadioBtnUS);
            };
            this.view.settings.flxRadioBtnUS.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgRadioBtnUS, scopeObj.view.settings.imgRadioBtnInternational);
            };
            this.view.settings.flxAddRadioBtnInternational.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgAddRadioBtnInternational, scopeObj.view.settings.imgAddRadioBtnUS);
            };
            this.view.settings.flxAddRadioBtnUS.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgAddRadioBtnUS, scopeObj.view.settings.imgAddRadioBtnInternational);
            };
            //Email flow
            this.view.settings.btnAddNewEmail.onClick = function() {
                scopeObj.resetAddEmailForm();
                scopeObj.showAddNewEmail();
            };
            this.view.settings.btnAddNewPersonalEmail.onClick = function() {
                scopeObj.resetAddEmailForm();
                scopeObj.showAddNewEmail();
            };
            this.view.settings.btnAddEmailIdCancel.onClick = function() {
                scopeObj.showEmail();
            };
            this.view.settings.btnAddEmailIdAdd.onClick = function() {
                //add code to ADD new email
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveEmail(scopeObj.getNewEmailData());
                // scopeObj.showEmail();
            };
            this.view.settings.btnEditAddNewEmail.onClick = function() {
                scopeObj.showAddNewEmail();
            };
            this.view.settings.btnEditEmailIdCancel.onClick = function() {
                scopeObj.showEmail();
            };
            this.view.settings.flxEditMarkAsPrimaryEmailCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblEditMarkAsPrimaryEmailCheckBox);
            };
            this.view.settings.flxMarkAsPrimaryEmailCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblMarkAsPrimaryEmailCheckBox);
            };
            this.view.settings.flxEditMarkAsAlertCommEmailCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblEditMarkAsAlertCommEmailCheckBox);
            };
            this.view.settings.flxMarkAsAlertCommEmailCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblMarkAsAlertCommEmailCheckBox);
            };
            //Address flow
            this.view.settings.btnAddNewAddress.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAddNewAddressView();
            };
            this.view.settings.btnAddNewPersonalAddress.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAddNewAddressView();
            };
            this.view.settings.btnAddNewAddressCancel.onClick = function() {
                scopeObj.showAddresses();
            };
            this.view.settings.btnAddNewAddressAdd.onClick = function() {
                //write code to ADD new Address
                scopeObj.showAddresses();
            };
            this.view.settings.btnEditAddressCancel.onClick = function() {
                scopeObj.showAddresses();
            };
            this.view.settings.btnEditAddressSave.onClick = function() {
                //Add code to save new address
                scopeObj.showAddresses();
            };
            this.view.settings.flxSetAsPreferredCheckBox.onClick = function() {
                if (totalAddress != 0) {
                    scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblSetAsPreferredCheckBox);
                }
            };
            this.view.settings.flxEditSetAsPreferredCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblEditSetAsPreferredCheckBox);
            };
            //Username & Password
            this.view.settings.btnUsernameEdit.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getUsernameRulesAndPolicies();
                var userPreferencesManager = applicationManager.getUserPreferencesManager();
                scopeObj.view.settings.flxErrorEditUsername.setVisibility(false);
                scopeObj.view.settings.tbxUsername.text = userPreferencesManager.getCurrentUserName();
                scopeObj.view.settings.placeholder = "";
                if (CommonUtilities.isCSRMode()) {
                    scopeObj.view.settings.btnEditUsernameProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    scopeObj.view.settings.btnEditUsernameProceed.skin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    scopeObj.enableButton(scopeObj.view.settings.btnEditUsernameProceed);
                    scopeObj.view.settings.tbxUsername.onKeyUp = function() {
                        scopeObj.ValidateUsername();
                    }.bind(this);
                    scopeObj.view.settings.tbxUsername.onEndEditing = function() {
                        if (scopeObj.isUserNameValid(scopeObj.view.settings.tbxUsername.text)) {
                            scopeObj.enableButton(scopeObj.view.settings.btnEditUsernameProceed);
                            scopeObj.view.settings.flxErrorEditUsername.setVisibility(false);
                        } else {
                            scopeObj.disableButton(scopeObj.view.settings.btnEditUsernameProceed);
                            scopeObj.view.settings.flxErrorEditUsername.setVisibility(true);
                            CommonUtilities.setText(scopeObj.view.settings.lblError0, kony.i18n.getLocalizedString("i18n.enrollNow.validUsername"), CommonUtilities.getaccessibilityConfig());
                            scopeObj.view.forceLayout();
                        }
                    }.bind(this);
                    scopeObj.view.settings.btnEditUsernameProceed.onClick = function() {
                        if (scopeObj.isUserNameValid(scopeObj.view.settings.tbxUsername.text)) {
                            scopeObj.enableButton(scopeObj.view.settings.btnEditUsernameProceed);
                            scopeObj.view.settings.flxErrorEditUsername.setVisibility(false);
                            update = "username";
                            scopeObj.updateRequirements();
                        } else {
                            scopeObj.disableButton(scopeObj.view.settings.btnEditUsernameProceed);
                            scopeObj.view.settings.flxErrorEditUsername.setVisibility(true);
                            CommonUtilities.setText(scopeObj.view.settings.lblError0, kony.i18n.getLocalizedString("i18n.enrollNow.validUsername"), CommonUtilities.getaccessibilityConfig());
                            scopeObj.view.forceLayout();
                        }
                    }.bind(this);
                }
                scopeObj.showEditUsername();
            };
            this.view.settings.btnEditUsernameCancel.onClick = function() {
                scopeObj.showUsernameAndPassword();
            };
            this.view.settings.btnEditPasswordCancel.onClick = function() {
                scopeObj.showUsernameAndPassword();
            };
            this.view.settings.btnUsernameVerification2Proceed.onClick = function() {
                if (scopeObj.view.settings.btnUsernameVerification2Proceed.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify")) {
                    CommonUtilities.setText(scopeObj.view.settings.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.ProfileManagement.usernameUpdatedSuccess"), CommonUtilities.getaccessibilityConfig());
                    scopeObj.showAcknowledgement();
                } else {
                    CommonUtilities.setText(scopeObj.view.settings.btnUsernameVerification2Cancel, kony.i18n.getLocalizedString("i18n.login.ResendOtp"), CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(scopeObj.view.settings.btnUsernameVerification2Proceed, kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify"), CommonUtilities.getaccessibilityConfig());
                    scopeObj.disableButton(scopeObj.view.settings.btnUsernameVerificationSend);
                    scopeObj.view.settings.tbxEnterOTP.text = "";
                    CommonUtilities.setText(scopeObj.view.settings.lblUsernameVerification2SendingYouTheOTP, kony.i18n.getLocalizedString("i18n.ProfileManagement.EnterOTPsentOnYourMobilePhone"), CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(scopeObj.view.settings.lblUsernameVerificationtext2, kony.i18n.getLocalizedString("i18n.ProfileManagement.IfYouHavenotRecievedOTP"), CommonUtilities.getaccessibilityConfig());
                    scopeObj.view.settings.flxOTPtextbox.setVisibility(true);
                }
            };
            this.view.settings.btnUsernameVerificationCancel.onClick = function() {
                scopeObj.view.settings.tbxExistingPassword.text = "";
                scopeObj.view.settings.tbxNewPassword.text = "";
                scopeObj.view.settings.tbxConfirmPassword.text = "";
                scopeObj.view.settings.tbxUsername = "";
                scopeObj.disableButton(scopeObj.view.settings.btnEditPasswordProceed);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showProfileSettings();
            };
            this.view.settings.btnUsernameVerificationProceed.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                scopeObj.showOTPAction();
                scopeObj.view.settings.flxErrorSecuritySettingsVerification.setVisibility(false);
                scopeObj.view.settings.flxErrorEditSecurityQuestions.setVisibility(false);
                if (scopeObj.view.settings.imgUsernameVerificationcheckedRadio.src === ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE) {
                    scopeObj.view.settings.tbxEnterOTP.onKeyUp = function() {
                        scopeObj.checkOTP();
                    };
                    CommonUtilities.setText(scopeObj.view.settings.lblSendingOTP, kony.i18n.getLocalizedString("i18n.ProfileManagement.SendingYouTheOtp"), CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(scopeObj.view.settings.lblSendingOTPMessage, kony.i18n.getLocalizedString("i18n.common.sendingOTP"), CommonUtilities.getaccessibilityConfig());
                    scopeObj.view.settings.flxOTPtextbox.setVisibility(false);
                    CommonUtilities.setText(scopeObj.view.settings.btnSecuritySettingVerificationCancel, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(scopeObj.view.settings.btnSecuritySettingVerificationSend, kony.i18n.getLocalizedString("i18n.common.send"), CommonUtilities.getaccessibilityConfig());
                    scopeObj.view.settings.btnSecuritySettingVerificationSend.onClick = function() {
                        scopeObj.assignOTPSendAction();
                        //FormControllerUtility.hideProgressBar(scopeObj.view);
                    };
                    scopeObj.enableButton(scopeObj.view.settings.btnSecuritySettingVerificationSend);
                    //scopeObj.enableButton(scopeObj.view.settings.btnUsernameVerificationSend);
                    scopeObj.showViews(["flxSecuritySettingVerificationWrapper"]);
                    FormControllerUtility.hideProgressBar(scopeObj.view);
                } else
                    scopeObj.getAndShowAnswerSecurityQuestions();
            };
            this.view.settings.btnAcknowledgementDone.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showProfileSettings();
            };
            this.view.settings.btnBackAnswerSecurityQuestions.onClick = function() {
                scopeObj.view.settings.tbxAnswers1.text = " ";
                scopeObj.view.settings.tbxAnswers2.text = " ";
            };
            this.view.settings.btnVerifyAnswerSecurityQuestions.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.verifyQuestionsAnswer(scopeObj.onSaveAnswerSecurityQuestions());
                //FormControllerUtility.hideProgressBar(scopeObj.view);
            };
            //Secure Access Code
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnSecureAccessCodeModify.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnSecureAccessCodeModify.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnSecureAccessCodeModify.onClick = function() {
                    var isPhoneEnabled, isEmailenabled;
                    var data = [];
                    if (scopeObj.view.settings.btnSecureAccessCodeModify.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Save")) {
                        if (scopeObj.view.settings.lblPhoneUnchecked.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED)
                            data.isPhoneEnabled = "true";
                        else
                            data.isPhoneEnabled = "false";
                        if (scopeObj.view.settings.lblemailunchecked.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED)
                            data.isEmailEnabled = "true";
                        else
                            data.isEmailEnabled = "false";
                        if (data.isPhoneEnabled == 'false' && data.isEmailEnabled == 'false') {
                            scopeObj.showEditSecureAccessSettingsError(kony.i18n.getLocalizedString("i18n.profile.editSecureAccessSettingsError"));
                        } else {
                            scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);
                            scopeObj.view.settings.btnSecureAccessCodeModify.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
                            CommonUtilities.setText(scopeObj.view.settings.btnSecureAccessCodeModify, kony.i18n.getLocalizedString("i18n.billPay.Edit"), CommonUtilities.getaccessibilityConfig());
                            FormControllerUtility.showProgressBar(scopeObj.view);
                            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateSecureAccessOptions(data);
                        }
                    } else {
                        scopeObj.view.settings.flxPhoneUnchecked.onClick = function() {
                            scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblPhoneUnchecked);
                        };
                        scopeObj.view.settings.flxemailunchecked.onClick = function() {
                            scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblemailunchecked);
                        };
                        scopeObj.view.settings.btnSecureAccessCodeCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                        scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(true);
                        scopeObj.view.settings.btnSecureAccessCodeModify.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
                        CommonUtilities.setText(scopeObj.view.settings.btnSecureAccessCodeModify, kony.i18n.getLocalizedString("i18n.ProfileManagement.Save"), CommonUtilities.getaccessibilityConfig());
                    }
                };
            }
            this.view.settings.flxPhoneUnchecked.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblPhoneUnchecked);
            };
            this.view.settings.flxemailunchecked.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblemailunchecked);
            };
            this.view.settings.btnSecuritySettingVerificationSend.onClick = function() {
                scopeObj.assignOTPSendAction();
            };
            this.view.settings.flxUsernameVerificationRadioOption1.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgUsernameVerificationcheckedRadio, scopeObj.view.settings.imgUsernameVerificationcheckedRadioOption2);
            };
            this.view.settings.flxUsernameVerificationRadioOption2.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgUsernameVerificationcheckedRadioOption2, scopeObj.view.settings.imgUsernameVerificationcheckedRadio);
            };
            this.view.settings.btnEditAccountsCancel.onClick = function() {
              if(scopeObj.view.settings.flxErrorEditAccounts.isVisible)
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showPreferredAccounts();
              else
                scopeObj.showAccounts();
            };
            this.view.settings.btnEditAccountsSave.onClick = function() {
                scopeObj.showAccounts();
            };
            this.view.settings.flxFavoriteEmailCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblFavoriteEmailCheckBox);
            };
            this.view.settings.flximgEnableEStatementsCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBoxEditAccounts(scopeObj.view.settings.lblEnableEStatementsCheckBox);
            };
            this.view.settings.flxTCContentsCheckbox.onClick = function() {
                if (scopeObj.view.settings.lblTCContentsCheckbox.text === ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
                    CommonUtilities.setText(scopeObj.view.settings.lblTCContentsCheckbox, ViewConstants.FONT_ICONS.CHECBOX_SELECTED, CommonUtilities.getaccessibilityConfig());
                    scopeObj.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                    scopeObj.enableButton(scopeObj.view.settings.btnEditAccountsSave);
                } else {
                    CommonUtilities.setText(scopeObj.view.settings.lblTCContentsCheckbox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                    scopeObj.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                    scopeObj.disableButton(scopeObj.view.settings.btnEditAccountsSave);
                }
            };
            //Set Default Accounts
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnDefaultTransactionAccountEdit.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnDefaultTransactionAccountEdit.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnDefaultTransactionAccountEdit.onClick = this.onSaveDefaultAccounts;
            }
            CommonUtilities.setText(this.view.settings.btnDefaultTransctionAccountsCancel, kony.i18n.getLocalizedString('i18n.transfers.Cancel'), CommonUtilities.getaccessibilityConfig());
            this.view.settings.btnDefaultTransctionAccountsCancel.onClick = function() {
                self.showDefaultScreen();
            };
            //Info Actions
            this.view.settings.flxImgInfoIcon.onClick = function() {
                var scrollpos = scopeObj.view.contentOffsetMeasured;
                if (scopeObj.view.settings.AllForms.isVisible === false) {
                    scopeObj.view.settings.AllForms.isVisible = true;
                } else
                    scopeObj.view.settings.AllForms.isVisible = false;
                scopeObj.view.forceLayout();
                scopeObj.view.setContentOffset(scrollpos);
                scopeObj.AdjustScreen();
            };
            this.view.settings.AllForms1.flxCross.onClick = function() {
                var scrollpos = scopeObj.view.contentOffsetMeasured;
                scopeObj.view.settings.AllForms1.isVisible = false;
                scopeObj.view.forceLayout();
                scopeObj.view.setContentOffset(scrollpos);
            };
            this.view.settings.flxYourConsent.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showConsentManagement();
            };

            this.view.settings.flxManageAccountAccessSubMenu.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showManageAccountAccess();
            };

            this.view.settings.btnEditConsent.onClick = scopeObj.showEditConsent.bind(this);

            this.view.settings.btnConsentManagementCancel.onClick = function() {
                consentEdit = false;
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showConsentManagement();
            }.bind(this);

            this.view.settings.btnConsentManagementSave.onClick = function() {
                consentEdit = false;
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.editConsentManagement(consentManagementData[0]);

            }.bind(this);
            // AAC-7257 - update password componentization changes
            scopeObj.view.settings.btnPasswordEdit.onClick = function() {
                if (scopeObj.view.settings.updatePassword) {
                    scopeObj.view.settings.updatePassword.removeFromParent();
                    scopeObj.view.settings.flxEditPasswordWrapperComp.removeAll();
                    scopeObj.view.settings.forceLayout();
                }
                let updatePassword = new com.temenos.infinity.updatePassword({
                    "autogrowMode": kony.flex.AUTOGROW_NONE,
                    "clipBounds": true,
                    "height": "100%",
                    "id": "updatePassword",
                    "isVisible": true,
                    "layoutType": kony.flex.FLOW_VERTICAL,
                    "left": "0dp",
                    "masterType": constants.MASTER_TYPE_USERWIDGET,
                    "isModalContainer": false,
                    "skin": "slFbox",
                    "top": "0dp",
                    "width": "100%",
                    "overrides": {
                        "updatePassword": {
                            "right": "viz.val_cleared",
                            "bottom": "viz.val_cleared",
                            "minWidth": "viz.val_cleared",
                            "minHeight": "viz.val_cleared",
                            "maxWidth": "viz.val_cleared",
                            "maxHeight": "viz.val_cleared",
                            "centerX": "viz.val_cleared",
                            "centerY": "viz.val_cleared"
                        }
                    }
                }, {
                    "overrides": {}
                }, {
                    "overrides": {}
                });
                updatePassword.primaryBtnDisableSkin = "{\"640\" : {\"normalSkin\": \"sknBtnBlockedSSP0273e313px\", \"hoverSkin\": \"sknBtnBlockedSSP0273e313px\", \"focusSkin\": \"sknBtnBlockedSSP0273e313px\"}, \"768\": {\"normalSkin\": \"sknBtnBlockedSSP0273e313px\", \"hoverSkin\": \"sknBtnBlockedSSP0273e313px\", \"focusSkin\": \"sknBtnBlockedSSP0273e313px\"},\"default\": {\"normalSkin\": \"sknBtnBlockedSSP0273e315px\", \"hoverSkin\": \"sknBtnBlockedSSP0273e315px\", \"focusSkin\": \"sknBtnBlockedSSP0273e315px\"}}";
                updatePassword.getPasswordRulesAndPolicy = "getPasswordRulesAndPolicy";
                updatePassword.isCSRAssit = CommonUtilities.isCSRMode();
                updatePassword.primaryBtnEnableSkin = "{\"640\" : {\"normalSkin\": \"sknBtnNormalSSPFFFFFF13Px\", \"hoverSkin\": \"sknBtnNormalSSPFFFFFFHover13Px\", \"focusSkin\": \"sknBtnNormalSSPFFFFFF13PxFocus\"}, \"768\": {\"normalSkin\": \"sknBtnNormalSSPFFFFFF13Px\", \"hoverSkin\": \"sknBtnNormalSSPFFFFFFHover13Px\", \"focusSkin\": \"sknBtnNormalSSPFFFFFF13PxFocus\"}, \"default\": {\"normalSkin\": \"sknBtnNormalSSPFFFFFF15Px\", \"hoverSkin\": \"sknBtnNormalSSPFFFFFFHover15Px\", \"focusSkin\": \"sknBtnNormalSSPFFFFFF15PxFocus\"}}";
                updatePassword.dataModel = "User";
                updatePassword.objectService = "RBObjects";
                updatePassword.textboxSkins = "{\"640\":\"sknTbxSSP42424213PxWithoutBorder\", \"768\":\"sknTbxSSP42424213PxWithoutBorder\", \"1024\":\"sknTbxSSP42424215PxWithoutBorder\", \"default\":\"sknTbxSSP42424215PxWithoutBorder\"}";
                updatePassword.updatePassword = "updateDBXUserPassword";
                updatePassword.flexSkins = "{\"normalSkin\": \"sknBorderE3E3E3\", \"focusSkin\": \"sknFlxBorder4A90E23px\", \"errorSkin\": \"sknborderff0000error\"}";
                updatePassword.verifyExistingPassword = "verifyExistingPassword";
                updatePassword.dbxUserDataModel = "DbxUser";
                updatePassword.onSuccessCallback = function() {
                    let authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                    authModule.presentationController.onPasswordChange();
                };
                updatePassword.onFailureCallback = function() {};
                updatePassword.onCancel = function() {
                    scopeObj.showUsernameAndPassword();
                };
                scopeObj.view.settings.flxEditPasswordWrapperComp.add(updatePassword);
                scopeObj.showViews(["flxEditPasswordWrapperComp"]);
            };
            // END - AAC-7257 - update password componentization changes
        },
        /**
         * Method to assign action show OTP
         */
        showOTPAction: function() {
            var scopeObj = this;
            this.view.settings.imgViewOTP.onTouchEnd = function() {
                if (showOTP) {
                    scopeObj.showOTP();
                    showOTP = 0;
                } else {
                    scopeObj.hideOTP();
                    showOTP = 1;
                }
            };
        },
        /**
         * Method to show default screen
         */
        showDefaultScreen: function() {
            var isCombinedUser = applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true";
            var isBusinessUser = applicationManager.getConfigurationManager().getConfigurationValue('isSMEUser') === "true";
            //         if(!isCombinedUser&&!isBusinessUser){
            //           this.view.settings.lbxTransfers.setVisibility(false);
            //           this.view.settings.lbxBillPay.setVisibility(false);
            //           this.view.settings.lbxPayAPreson.setVisibility(false);
            //           this.view.settings.lbxCheckDeposit.setVisibility(false);
            //         }
            //else{
            this.view.settings.flxBillPaySelectedValue.setVisibility(false);
            this.view.settings.flxCheckDepositSelectedValue.setVisibility(false);
            // }
            this.view.settings.flxTransfers.setVisibility(false);
            this.view.settings.flxPayAPerson.setVisibility(false);
            this.view.settings.flxTransfersValue.setVisibility(true);
            this.view.settings.flxBillPayValue.setVisibility(true);
            this.view.settings.flxPayAPersonValue.setVisibility(false);
            this.view.settings.flxCheckDepositValue.setVisibility(true);
            this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
            this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
            this.view.settings.btnDefaultTransctionAccountsCancel.setVisibility(false);
            CommonUtilities.setText(this.view.settings.btnDefaultTransactionAccountEdit, kony.i18n.getLocalizedString("i18n.billPay.Edit"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.btnDefaultTransactionAccountEdit.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("ACCOUNT_SETTINGS_EDIT"));
            this.view.settings.btnDefaultTransactionAccountEdit.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
        },
        showPassword: function() {
            this.view.settings.tbxEnterOTP.secureTextEntry = false;
        },
        hidePassword: function() {
            this.view.settings.tbxEnterOTP.secureTextEntry = true;
        },
        showEditSecureAccessSettingsError: function(errorMessage) {
            this.view.flxDowntimeWarning.setVisibility(true);
            this.view.rtxDowntimeWarning.text = errorMessage;
            this.view.flxDowntimeWarning.forceLayout();
        },
        hideEditSecureAccessSettingsError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
            this.view.flxDowntimeWarning.forceLayout();
        },
        /**
         * Method to Capture photo using camera
         */
        onClickTakePicture: function() {
            var rawBytes = this.view.cameraWidget.rawBytes; //kony.convertToBase64(imgData.rawBytes);
            this.imageRawBytes = rawBytes;
            var imageObject = kony.image.createImage(rawBytes);
            imageObject.scale(0.5);
            imageObject.compress(0.5);
            var rawBytesAfterCompression = imageObject.getImageAsRawBytes();
            var base64Image = kony.convertToBase64(rawBytesAfterCompression); //rawBytesAfterCompression);
            if (base64Image) {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.userImageUpdate(base64);
            }
        },
        /**
         * Method to open tablet media gallery
         */
        onClickChooseFromGallery: function() {
            var scope = this;
            var queryContext = {
                mimeType: "image/*"
            };
            kony.phone.openMediaGallery(gallerySelectionCallback, queryContext);

            function gallerySelectionCallback(rawBytes, permissionStatus) {
                if (rawBytes !== null && rawBytes !== "") {
                    scope.imageRawBytes = rawBytes;
                    var base64 = kony.convertToBase64(rawBytes);
                    //  var base64 = kony.convertToBase64(rawBytes);
                    if (base64 !== null && base64 !== undefined && base64 !== "") {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.userImageUpdate(base64);
                    }

                }
            }
        },
        /**
         * Method to toggle arrows when menu is expanded
         * @param {String} widget- widget ID
         * @param {String} imgArrow- ID of the arrow to be changed
         */
        toggle: function(widget, imgArrow) {
          var scope = this;
          if (widget.frame.height > 0) {
            this.collapseAll();
          } else {
            this.collapseAll();
            // imgArrow.src = ViewConstants.IMAGES.ARRAOW_UP;
            if(imgArrow.text !== "Q"){
              imgArrow.text = "P";
              imgArrow.toolTip = "Collapse";
              imgArrow.accessibilityConfig = {
                "a11yLabel": "Collapse"
              };
              this.expand(widget);
            }
          }
        },
    
        /**
         * Method to assign images when checkbox is clicked
         * @param {String} imgCheckBox- ID of the checkbox
         */
        toggleCheckBox: function(imgCheckBox) {
            if (imgCheckBox.src === ViewConstants.IMAGES.UNCHECKED_IMAGE) {
                imgCheckBox.src = ViewConstants.IMAGES.CHECKED_IMAGE;
            } else {
                imgCheckBox.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
            }
        },
        toggleFontCheckBox: function(imgCheckBox) {
            if (imgCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                imgCheckBox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                imgCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            } else {
                imgCheckBox.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                imgCheckBox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
            }
        },
        setDataForEmailListBox: function(response) {
            var data = [];
            var list = [];
            var i;
            for (i = 0; i < response.length; i++) {
                list = [];
                if (response[i].isPrimary === "true") {
                    list.push("primaryemail");
                    list.push(response[i].Value);
                    data.push(list);
                }
            }
            for (i = 0; i < response.length; i++) {
                list = [];
                if (data[0][1] !== response[i].Value) {
                    (response[i].id)?list.push(response[i].id):list.push("");
                    list.push(response[i].Value);
                    data.push(list);
                }
            }
            this.view.settings.lbxEmailForReceiving.masterData = data;
            this.view.settings.lbxEmailForReceiving.selectedKey = "primaryemail";
        },
        /**
         * Method to assign images when checkbox is clicked in accounts
         * @param {String} imgCheckBox- ID of the checkbox
         */
        toggleCheckBoxEditAccounts: function(imgCheckBox) {
            this.toggleFontCheckBox(imgCheckBox);
            if (CommonUtilities.isFontIconChecked(imgCheckBox) === true) {
                this.view.settings.flxTCCheckBox.setVisibility(true);
                this.view.settings.flxEmailForReceiving.setVisibility(true);
                this.view.settings.flxTCContentsCheckbox.setVisibility(true);
                this.view.settings.flxPleaseNoteTheFollowingPoints.height = "160dp";
                var emailObj = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
                var emailObj = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
                this.setDataForEmailListBox(emailObj);
                this.view.settings.lbxEmailForReceiving.setVisibility(true);
                if (CommonUtilities.isFontIconChecked(this.view.settings.lblTCContentsCheckbox) === true) {
                    this.enableButton(this.view.settings.btnEditAccountsSave);
                } else {
                    this.disableButton(this.view.settings.btnEditAccountsSave);
                }
            } else {
                this.view.settings.flxTCCheckBox.setVisibility(false);
                this.view.settings.flxTCContentsCheckbox.setVisibility(false);
                this.view.settings.lbxEmailForReceiving.setVisibility(false);
                this.view.settings.flxEmailForReceiving.setVisibility(false);
                this.view.settings.flxPleaseNoteTheFollowingPoints.height = "120dp";
                this.enableButton(this.view.settings.btnEditAccountsSave);
                if (CommonUtilities.isFontIconChecked(this.view.settings.lblTCContentsCheckbox) === true) {
                    CommonUtilities.toggleFontCheckbox(this.view.settings.lblTCContentsCheckbox);
                }
            }
        },
        /**
         * Method to expand menu without animation
         * @param {String} widget- ID of the widget
         */
        expandWithoutAnimation: function(widget) {
            widget.height = widget.widgets().length * 40;
            this.view.settings.forceLayout();
        },
        /**
         * Method to activate a menu item
         * @param {String} parentIndex- parent to be exapnded
         * @param {String} childrenIndex- child to be expanded in parent
         */
        activateMenu: function(parentIndex, childrenIndex) {
            var menuObject = widgetsMap[parentIndex];
            this.collapseAll();
            this.expandWithoutAnimation(this.view.settings[menuObject.subMenu.parent]);
            this.view.settings[menuObject.subMenu.children[childrenIndex]].widget.skin = "sknMenuSelected";
            this.view.settings[menuObject.subMenu.children[childrenIndex]].widget.hoverSkin = "sknMenuSelected";
        },
        /**
         * Method to assign initial action on onclick of widgets
         */
        initProfileSettingsMenu: function() {
            var scopeObj = this;
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxContainer', 'customheader', 'flxFooter', 'flxHeader', 'flxRight', 'flxErrorMessage', 'flxFeatureActionHeader', 'settings', 'settings.flxAlertsBody', 'settings.flxRight','flxFormContent']);
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
            FormControllerUtility.setRequestUrlConfig(this.view.brwBodyTnC);
            this.view.settings.lblChangeLanguage.toolTip = kony.i18n.getLocalizedString("i18n.Profile.Language");
            this.view.customheader.forceCloseHamburger();
            this.view.settings.rtxRulesPassword.text = "";
            if (applicationManager.getConfigurationManager().editPassword === "false") {
                this.view.settings.btnPasswordEdit.setVisibility(false);
            } else
                this.view.settings.btnPasswordEdit.setVisibility(true);
            // AAC-7267 : Disable username edit
            // if (applicationManager.getConfigurationManager().isSMEUser === "true" || applicationManager.getConfigurationManager().editUsername === "false" || !applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")) {
            //     this.view.settings.btnUsernameEdit.setVisibility(false);
            // } else
            //     this.view.settings.btnUsernameEdit.setVisibility(true);
            // Resetting Top Menu Skin
            this.view.customheader.topmenu.flxMenu.skin = "slFbox";
            this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
            CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.btnSave, kony.i18n.getLocalizedString("i18n.ProfileManagement.Save"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.btnCancel, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.transactionalAndPaymentsAlerts.btnSave.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
            this.view.settings.transactionalAndPaymentsAlerts.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            CommonUtilities.setText(this.view.lblAlertsDisableContent, kony.i18n.getLocalizedString("i18n.Profilemanagement.AlertsDisablePopup"), CommonUtilities.getaccessibilityConfig());
            this.view.rtxAlertsDisableWarning.text = kony.i18n.getLocalizedString("i18n.Profilemanagement.AlertsDisableWarning");
            this.view.btnAlertsDisableNo.onClick = function() {
                scopeObj.view.flxAlertsDisablePopUp.setVisibility(false);
                scopeObj.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);
            };
            this.view.flxAlertsDisableClose.onClick = function() {
                scopeObj.view.flxAlertsDisablePopUp.setVisibility(false);
                scopeObj.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);
            };
            this.view.btnDeletePopupYes.onClick = function() {
                scopeObj.view.flxProfileDeletePopUp.setVisibility(false);
                FormControllerUtility.showProgressBar(self.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.userImageDelete();
            };
            this.view.btnDeletePopupNo.onClick = function() {
                scopeObj.view.flxProfileDeletePopUp.setVisibility(false);
                scopeObj.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);
            };
            this.view.flxprofiledeleteClose.onClick = function() {
                scopeObj.view.flxProfileDeletePopUp.setVisibility(false);
                scopeObj.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);
            };
            var isLanguage = applicationManager.getConfigurationManager().getLanguageSelectionFlag();
            scopeObj.setLeftMenuUI(isLanguage);
//			since this.view.settings.TabsHeaderNewCopy is nerver used and deleted
//             this.view.settings.TabsHeaderNewCopy.btnTab1.text = kony.i18n.getLocalizedString("i18n.Settings.ApprovalMatrix.perTransactionLimit");
//             this.view.settings.TabsHeaderNewCopy.btnTab2.text = kony.i18n.getLocalizedString("i18n.Settings.ApprovalMatrix.dailyTransactionLimit");
//             this.view.settings.TabsHeaderNewCopy.btnTab3.text = kony.i18n.getLocalizedString("i18n.Settings.ApprovalMatrix.weeklyTransactionLimit");
           //changing this code to resolve separator issue in the bottom of left menu (DBB-8134)
            if (!applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_VIEW")) {
                this.view.settings.flxProfileSettings.isVisible = false;
                this.view.settings.flxProfileSettingsSubMenu.isVisible = false;
            }
            if (!applicationManager.getConfigurationManager().checkUserPermission("ACCOUNT_SETTINGS_VIEW")) {
                this.view.settings.flxAccountSettings.isVisible = false;
                this.view.settings.flxAccountSettingsSubMenu.isVisible = false;
            }
            if (!applicationManager.getConfigurationManager().checkUserPermission("ALERT_MANAGEMENT")) {
                this.view.settings.flxAlerts.isVisible = false;
                this.view.settings.flxAlertsSubMenu.isVisible = false;
            }
            if (!applicationManager.getConfigurationManager().checkUserPermission("CDP_CONSENT_VIEW")) {
                this.view.settings.flxConsentManagement.isVisible = false;
                this.view.settings.flxConsentManagementSubMenu.isVisible = false;
            }
            if (!applicationManager.getConfigurationManager().checkUserPermission("PSD2_TPP_CONSENT_VIEW")) {
                this.view.settings.flxManageAccountAccess.isVisible = false;
                this.view.settings.flxManageAccountAccessSubMenu.isVisible = false;
            }
            this.view.settings.forceLayout();
        },
        setLeftMenuUI: function(flag) {
			var scopeObj = this;
            function applyConfigurations(menuObject) {
                var children = menuObject.subMenu.children;
				var invisibleCount;
				if(children.length === 0)
				  invisibleCount = 0;
				else{
				  invisibleCount = children.reduce(function(count, child) {
					var configuration = applicationManager.getConfigurationManager().undefined;
					var isSMEUser = applicationManager.getConfigurationManager().isSMEUser === "true" ? true : false;
					if (configuration === "false" || (child.widget === "flxChangeLanguage" && !flag) 
						|| (child.widget === "flxPhone" && isSMEUser || (child.widget === "flxEmail" && isSMEUser) 
							|| (child.widget === "flxAddress" && isSMEUser)))
					{
					  count += 1;
					  scopeObj.view.settings[child.widget].setVisibility(false);
					}
					return count;
					}, 0)
				}
				var visibleCount = children.length - invisibleCount;
				scopeObj.view.settings[menuObject.subMenu.parent].height = 40 * visibleCount + "px";
				if (invisibleCount === children.length) {
				  if(scopeObj.view.settings[menuObject.image].text !== "Q"){
					scopeObj.view.settings[menuObject.menu].setVisibility(false);
				  }
				}
            }
            let presentationUtility = applicationManager.getPresentationUtility();
            if (presentationUtility.MFA && presentationUtility.MFA.isSCAEnabled && presentationUtility.MFA.isSCAEnabled()) {
              let item = {
                menu: "flxDeviceManagement",
                subMenu: {
                  parent: "flxDeviceManagementSubMenu",
                  children: [{
                    "configuration": "enableRegisterDevicesSettings",
                    "widget": "flxRegisterDevices"
                  }]
                },
                image: "lblDeviceManagementCollapse"
              };
              widgetsMap.splice(1, 0, item);
            }
          	var component = this.view.settings;
            for (var i = 0; i < widgetsMap.length; i++) {
                var menuObject = widgetsMap[i];
                applyConfigurations(menuObject);
                scopeObj.view.settings.forceLayout();
				if(this.view.settings[menuObject.image].text === "O" || this.view.settings[menuObject.image].text === "P"){
					scopeObj.view.settings[menuObject.menu].onClick = this.getMenuHandler(this.view.settings[menuObject.subMenu.parent], this.view.settings[menuObject.image]);
				  }
				  else if(this.view.settings[menuObject.image].text === "Q"){
					if(menuObject.menu === "flxApprovalMatrix"){
                      scopeObj.view.settings[menuObject.menu].isVisible = (applicationManager.getConfigurationManager().checkUserPermission('APPROVAL_MATRIX_VIEW')
                                                                          && kony.application.getCurrentBreakpoint() !== 1024 && !orientationHandler.isTablet);
					  scopeObj.view.settings[menuObject.menu].onClick = function(){
						var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController;
						profileModule.enterProfileSettings("approvalMatrix");
					  }.bind(this);
					}
				  }
            }
            this.setFlowActions();
            this.collapseAll();
            this.showViews();
            this.configurationContactSettings();
            this.expand(this.view.settings.flxProfileSettingsSubMenu);
            this.view.settings.lblProfileSettingsCollapse.text = "P";
            this.view.settings.lblProfileSettingsCollapse.toolTip = "Collapse";
            CommonUtilities.setText(this.view.settings.lblProfileSettingsCollapse, "P", CommonUtilities.getaccessibilityConfig());
            //this.resetSkins();
        },
        /**
         * Method to manage Configuration of Address fields and phone number fields
         */
        configurationContactSettings: function() {
            this.view.settings.btnAddNewAddress.setVisibility(applicationManager.getConfigurationManager().additionalAddressAllowed === "true" && applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE"));
            this.view.settings.btnAddNewNumber.setVisibility(applicationManager.getConfigurationManager().additionalPhoneAllowed === "true" && applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE"))
        },
        /**
         * Method to highlight the menu item and collapse others on the left side
         * @param {String} submenu- Item of the menu to be highlighted
         * @param {String} collapseImage- Arrow to be expanded
         */
        getMenuHandler: function(subMenu, collapseImage) {
            return function() {
                this.toggle(subMenu, collapseImage);
            }.bind(this);
        },
        /**
         * Method to collapse the menu item with animation
         * @param {String} widget- ID of the widget to be collapsed
         */
        collapse: function(widget) {
            var scope = this;
            var animationDefinition = {
                100: {
                    "height": 0
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var callbacks = {
                animationEnd: function() {
                    // scope.checkLogoutPosition();
                    scope.view.forceLayout();
                }
            };
            var animationDef = kony.ui.createAnimation(animationDefinition);
            widget.animate(animationDef, animationConfiguration, callbacks);
        },
        selectedQuestions: {
            ques: ["Select a Question", "Select a Question", "Select a Question", "Select a Question", "Select a Question"],
            key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
        },
        selectedQuestionsTemp: {
            securityQuestions: [],
            flagToManipulate: []
        },
        responseBackend: [{
            question: "",
            SecurityID: ""
        }],
        /**
         * Method to assign initial action on onclick of widgets
         * @param {Object} response- JSON of the questions fetched from MF
         * @param {Object} data- JSON of the questions
         */
        staticSetQuestions: function(response, data) {
            if (!data.errmsg) {
                FormControllerUtility.hideProgressBar(this.view);
                this.responseBackend = data;
                this.successCallback(response);
                this.showSetSecurityQuestions();
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.showFetchSecurityQuestionsError();
            }
            this.AdjustScreen();
        },
        /**
         * Method to show all the security questions
         */
        showSetSecurityQuestions: function() {
            //this.view.flxUsernameAndPasswordAck.setVisibility(false);
            this.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(false);
            this.view.settings.flxSecurityQuestionSet.setVisibility(true);
            this.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(true);
            this.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(true);
            this.view.settings.flxSecurityQASet1.setVisibility(true);
            this.view.settings.flxSecurityQASet2.setVisibility(true);
            this.view.settings.flxSecurityQASet3.setVisibility(true);
            this.view.settings.flxSecurityQASet4.setVisibility(true);
            this.view.settings.flxSecurityQASet5.setVisibility(true);
            this.view.settings.lblEditSecuritySettingsHeader.setFocus(true);
            CommonUtilities.setText(this.view.settings.btnEditSecuritySettingsProceed, kony.i18n.getLocalizedString("i18n.common.proceed"), CommonUtilities.getaccessibilityConfig());
            this.view.forceLayout();
        },
        /**
         * Method to set data to all the list boxes initially
         * @param {Object} response- JSON of questions which should get set
         */
        successCallback: function(response) {
            var data = [];
            this.selectedQuestionsTemp = response;
            data = this.getQuestions(response);
            this.view.settings.lbxQuestion1.masterData = data;
            this.view.settings.lbxQuestion2.masterData = data;
            this.view.settings.lbxQuestion3.masterData = data;
            this.view.settings.lbxQuestion4.masterData = data;
            this.view.settings.lbxQuestion5.masterData = data;
            this.view.settings.lbxQuestion1.selectedKey = "lb0";
            this.view.settings.lbxQuestion2.selectedKey = "lb0";
            this.view.settings.lbxQuestion3.selectedKey = "lb0";
            this.view.settings.lbxQuestion4.selectedKey = "lb0";
            this.view.settings.lbxQuestion5.selectedKey = "lb0";
            this.view.settings.tbxAnswer1.text = "";
            this.view.settings.tbxAnswer2.text = "";
            this.view.settings.tbxAnswer3.text = "";
            this.view.settings.tbxAnswer4.text = "";
            this.view.settings.tbxAnswer5.text = "";
            this.disableButton(this.view.settings.btnEditSecuritySettingsProceed);
        },
        /**
         * Method to manipulate data in listbox
         * @param {Object} data- JSON of all the questions
         * @returns {Object} selectedData- JSON of seleted question and answer
         */
        flagManipulation: function(data, selectedData) {
            var tempData1 = [],
                tempData2 = [];
            if (selectedData[0] !== "lb0") {
                tempData1[0] = selectedData;
                tempData2 = tempData1.concat(data);
            } else
                tempData2 = data;
            return tempData2;
        },
        /**
         * Method  to manipulate the flag of the question selected
         * @param {Object} data - JSON of all the questions with answers
         * @param {Object} selectedQues - JSON of the selected question with its answer
         * @param {Object} key - ID of the question selected
         */
        getQuestionsAfterSelected: function(data, selectedQues, key) {
            var response = [];
            var temp = 10;
            if (data[1] !== "Select a Question") {
                for (var i = 0; i < this.selectedQuestionsTemp.securityQuestions.length; i++) {
                    if (this.selectedQuestionsTemp.securityQuestions[i] === data[1]) {
                        if (this.selectedQuestionsTemp.flagToManipulate[i] === "false") {
                            this.selectedQuestionsTemp.flagToManipulate[i] = "true";
                            temp = i;
                        }
                    }
                    if (this.selectedQuestionsTemp.securityQuestions[i] === selectedQues.ques) {
                        if (this.selectedQuestionsTemp.flagToManipulate[i] === "true") {
                            this.selectedQuestionsTemp.flagToManipulate[i] = "false";
                            this.selectedQuestions.key[key] = "lb" + (i + 1);
                        }
                    }
                }
            } else {
                this.disableButton(this.view.settings.btnEditSecuritySettingsProceed);
                if (key === 0) {
                    this.view.settings.tbxAnswer1.text = "";
                } else if (key === 1) {
                    this.view.settings.tbxAnswer2.text = "";
                } else if (key === 2) {
                    this.view.settings.tbxAnswer3.text = "";
                } else if (key === 3) {
                    this.view.settings.tbxAnswer4.text = "";
                } else if (key === 4) {
                    this.view.settings.tbxAnswer5.text = "";
                }
                for (var ij = 0; ij < this.selectedQuestionsTemp.securityQuestions.length; ij++) {
                    if (this.selectedQuestionsTemp.securityQuestions[ij] === selectedQues.ques) {
                        if (this.selectedQuestionsTemp.flagToManipulate[ij] === "true") {
                            this.selectedQuestionsTemp.flagToManipulate[ij] = "false";
                            this.selectedQuestions.key[key] = "lb" + (ij + 1);
                        }
                    }
                }
            }
            if (temp !== 10) {
                this.selectedQuestions.ques[key] = this.selectedQuestionsTemp.securityQuestions[temp];
                this.selectedQuestions.key[key] = "lb" + (temp + 1);
            } else {
                this.selectedQuestions.ques[key] = "Select a Question";
                this.selectedQuestions.key[key] = "lb0";
            }
            var questions = [];
            questions = this.getQuestions(this.selectedQuestionsTemp);
            return questions;
        },
        /**
         * Method that changes questions into key-value pairs basing on the flagManipulation
         * @param {Object} response- JSON of all the questions
         */
        getQuestions: function(response) {
            var temp = [];
            temp[0] = ["lb0", "Select a Question"];
            for (var i = 0, j = 1; i < response.securityQuestions.length; i++) {
                var arr = [];
                if (response.flagToManipulate[i] === "false") {
                    arr[0] = "lb" + (i + 1);
                    arr[1] = response.securityQuestions[i];
                    temp[j] = arr;
                    j++;
                }
            }
            return temp;
        },
        /**
         * Method to enable Proceed button
         * @param {String} question1- First question selected
         * @param {String} question1- Second question selected
         * @param {String} question1- Third question selected
         * @param {String} question1- Fourth question selected
         * @param {String} question1- Fifth question selected
         */
        enableSecurityQuestions: function(question1, question2, question3, question4, question5) {
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnEditSecuritySettingsProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnEditSecuritySettingsProceed.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                if (question1 !== null && question1 !== "" && question2 !== null && question2 !== "" && question3 !== null && question3 !== "" && question4 !== null && question4 !== "" && question5 !== null && question5 !== "") {
                    this.btnSecurityQuestions(true);
                } else {
                    this.btnSecurityQuestions(false);
                }
            }
        },
        /**
         * Method to toggle the proceed button visibility
         * @param {Boolean} status - Status of the button to be enabled or disabled
         */
        btnSecurityQuestions: function(status) {
            if (status === true) {
                this.enableButton(this.view.settings.btnEditSecuritySettingsProceed);
            } else {
                this.disableButton(this.view.settings.btnEditSecuritySettingsProceed);
            }
        },
        /**
         * Method to set Question for lbxQuestion1
         */
        setQuestionForListBox1: function() {
            var value = [];
            value = this.view.settings.lbxQuestion1.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[0];
            selectedQues.key = this.selectedQuestions.key[0];
            var position = 0;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion2.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion2.masterData = mainData;
            this.view.settings.lbxQuestion2.selectedKey = selectedData[0];
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion3.masterData = mainData2;
            this.view.settings.lbxQuestion3.selectedKey = selectedData2[0];
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion4.masterData = mainData3;
            this.view.settings.lbxQuestion4.selectedKey = selectedData3[0];
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion5.masterData = mainData4;
            this.view.settings.lbxQuestion5.selectedKey = selectedData4[0];
        },
        /**
         * Method to set Question for lbxQuestion4
         */
        setQuestionForListBox4: function() {
            var value = [];
            value = this.view.settings.lbxQuestion4.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[3];
            selectedQues.key = this.selectedQuestions.key[3];
            var position = 3;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion1.masterData = mainData;
            this.view.settings.lbxQuestion1.selectedKey = selectedData[0];
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion3.masterData = mainData2;
            this.view.settings.lbxQuestion3.selectedKey = selectedData2[0];
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion2.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion2.masterData = mainData3;
            this.view.settings.lbxQuestion2.selectedKey = selectedData3[0];
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion5.masterData = mainData4;
            this.view.settings.lbxQuestion5.selectedKey = selectedData4[0];
        },
        /**
         * Method to set Question for lbxQuestion3
         */
        setQuestionForListBox3: function() {
            var value = [];
            value = this.view.settings.lbxQuestion3.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[2];
            selectedQues.key = this.selectedQuestions.key[2]
            var position = 2;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion1.masterData = mainData;
            this.view.settings.lbxQuestion1.selectedKey = selectedData[0];
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion2.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion2.masterData = mainData2;
            this.view.settings.lbxQuestion2.selectedKey = selectedData2[0];
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion4.masterData = mainData3;
            this.view.settings.lbxQuestion4.selectedKey = selectedData3[0];
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion5.masterData = mainData4;
            this.view.settings.lbxQuestion5.selectedKey = selectedData4[0];
        },
        /**
         * Method to set Question for lbxQuestion2
         */
        setQuestionForListBox2: function() {
            var value = [];
            value = this.view.settings.lbxQuestion2.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[1];
            selectedQues.key = this.selectedQuestions.key[1]
            var position = 1;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion1.masterData = mainData;
            this.view.settings.lbxQuestion1.selectedKey = selectedData[0];
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion3.masterData = mainData2;
            this.view.settings.lbxQuestion3.selectedKey = selectedData2[0];
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion4.masterData = mainData3;
            this.view.settings.lbxQuestion4.selectedKey = selectedData3[0];
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion5.masterData = mainData4;
            this.view.settings.lbxQuestion5.selectedKey = selectedData4[0];
        },
        /**
         * Method to set Question for lbxQuestion5
         */
        setQuestionForListBox5: function() {
            var value = [];
            value = this.view.settings.lbxQuestion5.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[4];
            selectedQues.key = this.selectedQuestions.key[4];
            var position = 4;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion1.masterData = mainData;
            this.view.settings.lbxQuestion1.selectedKey = selectedData[0];
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion3.masterData = mainData2;
            this.view.settings.lbxQuestion3.selectedKey = selectedData2[0];
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion4.masterData = mainData3;
            this.view.settings.lbxQuestion4.selectedKey = selectedData3[0];
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion2.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion2.masterData = mainData4;
            this.view.settings.lbxQuestion2.selectedKey = selectedData4[0];
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer1
         */
        onEditingAnswer1: function() {
            var data = [];
            data = this.view.settings.lbxQuestion1.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer1.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer1.maxTextLength = 50;
            }
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer2
         */
        onEditingAnswer2: function() {
            var data = [];
            data = this.view.settings.lbxQuestion2.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer2.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer2.maxTextLength = 50;
            }
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer3
         */
        onEditingAnswer3: function() {
            var data = [];
            data = this.view.settings.lbxQuestion3.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer3.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer3.maxTextLength = 50;
            }
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer4
         */
        onEditingAnswer4: function() {
            var data = [];
            data = this.view.settings.lbxQuestion4.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer4.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer4.maxTextLength = 50;
            }
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer5
         */
        onEditingAnswer5: function() {
            var data = [];
            data = this.view.settings.lbxQuestion5.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer5.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer5.maxTextLength = 50;
            }
        },
        /**
         * Method for saving security questions
         */
        onSaveSecurityQuestions: function() {
            var data = [{
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }];
            var quesData = "";
            quesData = this.view.settings.lbxQuestion1.selectedKeyValue;
            data[0].customerAnswer = this.view.settings.tbxAnswer1.text;
            data[0].questionId = this.getQuestionID(quesData);
            quesData = this.view.settings.lbxQuestion2.selectedKeyValue;
            data[1].customerAnswer = this.view.settings.tbxAnswer2.text;
            data[1].questionId = this.getQuestionID(quesData);
            quesData = this.view.settings.lbxQuestion3.selectedKeyValue;
            data[2].customerAnswer = this.view.settings.tbxAnswer3.text;
            data[2].questionId = this.getQuestionID(quesData);
            quesData = this.view.settings.lbxQuestion4.selectedKeyValue;
            data[3].customerAnswer = this.view.settings.tbxAnswer4.text;
            data[3].questionId = this.getQuestionID(quesData);
            quesData = this.view.settings.lbxQuestion5.selectedKeyValue;
            data[4].customerAnswer = this.view.settings.tbxAnswer5.text;
            data[4].questionId = this.getQuestionID(quesData);
            return data;
        },
        /**
         * Method to get questionID from question
         */
        getQuestionID: function(quesData) {
            var qData;
            for (var i = 0; i < this.responseBackend.length; i++) {
                if (quesData[1] === this.responseBackend[i].SecurityQuestion) {
                    qData = this.responseBackend[i].SecurityQuestion_id;
                }
            }
            return qData;
        },
        /**
         * Method to edit phone number options
         */
        setEditPhoneNoOption2SegmentData: function() {
            var dataMap = {
                "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
                "flxCheckbox": "flxCheckbox",
                "lblCheckBox": "lblCheckBox",
                "lblAccounts": "lblAccounts"
            };
            var data = [{
                    "lblCheckBox": {
                        "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                        "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN,
                        "accessibilityconfig": {
                            "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                        }
                    },
                    "lblAccounts": {
                        "text": "Joint Checking Ã¢â‚¬Â¦.4323  ",
                        "accessibilityconfig": {
                            "a11yLabel": "Joint Checking Ã¢â‚¬Â¦.4323  ",
                        }
                    }
                },
                {
                    "lblCheckBox": {
                        "text": ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                        "accessibilityconfig": {
                            "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED
                        },
                        "skin": ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN
                    },
                    "lblAccounts": {
                        "text": "Joint Checking Ã¢â‚¬Â¦.4323  ",
                        "accessibilityconfig": {
                            "a11yLabel": "Joint Checking Ã¢â‚¬Â¦.4323  ",
                        }
                    },
                },
                {
                    "lblCheckBox": {
                        "text": ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                        "accessibilityconfig": {
                            "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                        },
                        "skin": ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN
                    },
                    "lblAccounts": {
                        "text": "Joint Checking Ã¢â‚¬Â¦.4323  ",
                        "accessibilityconfig": {
                            "a11yLabel": "Joint Checking Ã¢â‚¬Â¦.4323  ",
                        }
                    }
                }
            ];
            this.view.settings.segEditPhoneNumbersOption2.widgetDataMap = dataMap;
            this.view.settings.segEditPhoneNumbersOption2.setData(data);
            this.view.forceLayout();
        },
        /**
         * Method onSaveExternalAccount
         * @param {boolean} errorScenario - contains the errorScenario.
         */
        onSaveExternalAccount: function(errorScenario) {
            var self = this;
            if (errorScenario === true) {
                this.view.settings.flxErrorEditAccounts.setVisibility(true);
                CommonUtilities.setText(this.view.settings.lblErrorEditAccounts, kony.i18n.getLocalizedString("i18n.ProfileManagement.weAreUnableToProcess"), CommonUtilities.getaccessibilityConfig());
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.view.settings.flxErrorEditAccounts.setVisibility(false);
                self.showViews();
                self.view.settings.btnEditAccountsCancel.onClick = function() {
                    self.showAccounts();
                };
                self.view.settings.btnEditAccountsSave.onClick = function() {
                    self.showAccounts();
                };
            }
        },
        /**
         * Method to go to edit accounts
         * @param {Object} data - contains account
         */
        goToEditExternalAccounts: function(data) {
            var self = this;
            this.collapseAll();
            this.expandWithoutAnimation(this.view.settings.flxAccountSettingsSubMenu);
            this.view.settings.lblAccountSettingsCollapse.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
            this.setSelectedSkin("flxAccountPreferences");
            this.view.settings.flxProfileWrapper.setVisibility(false);
            this.view.settings.flxErrorEditAccounts.setVisibility(false);
            this.view.settings.tbxAccountNickNameValue.text = data.nickName;
            CommonUtilities.setText(this.view.settings.lblFullNameValue, data.accountHolder, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblAccountTypeValue, data.accountType, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblAccountNumberValue, data.accountID, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblFavoriteEmailCheckBox, (data.favouriteStatus === "1") ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            this.view.settings.lblFavoriteEmailCheckBox.skin = (data.favouriteStatus === "1") ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.view.settings.lblFavoriteEmailCheckBox.onTouchEnd = function() {
                self.toggleFontCheckBox(self.view.settings.lblFavoriteEmailCheckBox);
            };
            //this.view.settings.flxEmailForReceiving.setVisibility(false);
            this.view.settings.flxTCCheckBox.setVisibility(false);
            this.view.settings.flxTCContentsCheckbox.setVisibility(false);
            this.view.settings.lblTCContentsCheckbox.setVisibility(false);
            this.view.settings.flxPleaseNoteTheFollowingPoints.setVisibility(false);
            this.view.settings.flxEnableEStatementsCheckBox.setVisibility(false);
            this.view.settings.btnTermsAndConditions.setVisibility(false);
            this.enableButton(this.view.settings.btnEditAccountsSave);
            this.view.settings.flxTCCheckBox.setVisibility(true);
            this.enableButton(this.view.btnSave);
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnEditAccountsSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnEditAccountsSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.settings.btnEditAccountsSave.focus = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnEditAccountsSave.onClick = function(data) {
                    FormControllerUtility.showProgressBar(this.view);
                    var newdata = {
                        Account_id: data.externalAccountId,
                        NickName: self.view.settings.tbxAccountNickNameValue.text,
                        FavouriteStatus: self.view.settings.lblFavoriteEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? "true" : "false",
                    };
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveExternalAccountsData(newdata);
                }.bind(this, data);
                this.view.settings.btnEditAccountsCancel.onClick = function() {
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showAccountsDashboard();
                    self.showViews();
                    self.view.settings.btnEditAccountsCancel.onClick = function() {
                        self.showAccounts();
                    }
                    self.view.settings.btnEditAccountsSave.onClick = function() {
                        //Write Code to SAVE the changes of accounts
                        self.showAccounts();
                    };
                };
            }
            this.showViews(["flxEditAccountsWrapper"]);
            if (applicationManager.getConfigurationManager().editNickNameAccountSettings === "true") {
                this.view.settings.flxAccountNickName.setVisibility(true);
            } else {
                this.view.settings.flxAccountNickName.setVisibility(false);
            }
            this.AdjustScreen();
            this.changeProgressBarState(false);
        },
        /**
         * Method used to show the email view.
         */
        showEmail: function() {
            this.showViews(["flxEmailWrapper"]);
          	this.view.customheader.headermenu.setFocus(true);
        },
        /**
         * Method to update the list of emails
         * @param {Object} emailListViewModel- list of emails
         */
        updateEmailList: function(emailListViewModel) {
            this.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
            this.showEmail();
            if (emailListViewModel.length >= 3 || !applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")) {
                this.view.settings.btnAddNewEmail.setVisibility(false);
                this.view.settings.btnEditAddNewEmail.setVisibility(false);
            } else {
                this.view.settings.btnAddNewEmail.setVisibility(true);
                this.view.settings.btnEditAddNewEmail.setVisibility(true);
                this.view.settings.btnAddNewPersonalEmail.setVisibility(true);
            }

            this.setEmailSegmentData(emailListViewModel);
            this.setSelectedSkin("flxEmail");
        },
        /**
         * Method to set all the Data comming from backend to the email module
         * @param {Object} emailList - List of all the emails of the user
         */
        setEmailSegmentData: function(emailList) {
            var isCombinedUser = applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true";
            var scopeObj = this;
            var personalEmailList = [];
            var businessEmailList = [];
            if (isCombinedUser) {
                emailList.forEach(function(item) {
                    if (!kony.sdk.isNullOrUndefined(item.isTypeBusiness) && item.isTypeBusiness === "1")
                        businessEmailList.push(item);
                    else
                        personalEmailList.push(item);
                });
                this.view.settings.flxCombinedEmail.setVisibility(true);
                this.view.settings.flxEmails.setVisibility(false);
            } else {
                this.view.settings.flxCombinedEmail.setVisibility(false);
                this.view.settings.flxEmails.setVisibility(true);
            }

            function getDeleteEmailListener(emailObj) {
                return function() {
                    var currForm = scopeObj.view;
                    currForm.flxDeletePopUp.height = currForm.flxHeader.info.frame.height + currForm.flxContainer.info.frame.height + currForm.flxFooter.frame.height + "dp";
                    currForm.flxDeletePopUp.setVisibility(true);
                    currForm.flxDeletePopUp.setFocus(true);
                    currForm.lblDeleteHeader.setFocus(true);
                    CommonUtilities.setText(currForm.lblDeleteHeader, kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"), CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(currForm.lblConfirmDelete, kony.i18n.getLocalizedString("i18n.ProfileManagement.deleteEmail"), CommonUtilities.getaccessibilityConfig());
                    currForm.forceLayout();
                    currForm.btnDeleteYes.onClick = function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.deleteEmail(emailObj);
                        scopeObj.view.flxDelete.setFocus(true);
                        scopeObj.view.flxDeletePopUp.setVisibility(false);
                        scopeObj.view.forceLayout();
                    };
                }
            }
            var dataMap = {
                "btnDelete": "btnDelete",
                "btnEdit": "btnEdit",
                "flxDeleteAction": "flxDeleteAction",
                "flxEdit": "flxEdit",
                "flxEmail": "flxEmail",
                "flxPrimary": "flxPrimary",
                "flxUsedFor": "flxUsedFor",
                "flxProfileManagementEmail": "flxProfileManagementEmail",
                "flxRow": "flxRow",
                "lblSeperator": "lblSeperator",
                "lblPrimary": "lblPrimary",
                "lblUsedFor": "lblUsedFor",
                "lblEmail": "lblEmail",
                "template": "template"
            };
            var flexWidth = "135px";
            var textToShow = "";
            if (isCombinedUser) {
                var personalData = personalEmailList.map(function(emailObj) {
                    if (emailObj.isPrimary === "true" && emailObj.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "175px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryAlertComm");
                        this.view.customheader.lblUserEmail.text = emailObj.Value;
                    } else if (emailObj.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "120px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.alertComm");
                    } else if (emailObj.isPrimary === "true") {
                        flexWidth = "135px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryComm");
                    }
                    var dataObject = {
                        "lblEmail": {
                            "text": emailObj.Value,
                            "accessibilityconfig": {
                                "a11yLabel": emailObj.Value,
                            }
                        },
                        "flxUsedFor": {
                            "isVisible": emailObj.isPrimary === "true" || (emailObj.isAlertsRequired === "true" && scopeObj.enableSeparateContact) ? true : false
                        },
                        "flxPrimary": {
                            "width": flexWidth
                        },
                        "lblPrimary": {
                            "text": textToShow,
                            "accessibilityconfig": {
                                "a11yLabel": textToShow,
                            },
                        },
                        "lblUsedFor": {
                            "text": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor")
                            }
                        },
                        "btnDelete": {
                            "text": emailObj.isPrimary == "true" ? " " : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            "accessibilityconfig": {
                                "a11yLabel": emailObj.isPrimary == "true" ? " " : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            },
                            "onClick": CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : getDeleteEmailListener(emailObj),
                            "skin": CommonUtilities.isCSRMode() ? CommonUtilities.disableSegmentButtonSkinForCSRMode(13) : "sknBtnSSPFont4176a415px",
                            "isVisible": emailObj.isPrimary == "true" ? false : true
                        },
                        "btnEdit": {
                            "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            },
                            "onClick": (emailObj.isPrimary == "true") ? scopeObj.editPrimaryEmail.bind(scopeObj, emailObj) : scopeObj.editEmail.bind(scopeObj, emailObj),
                            "isVisible": applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")
                        },
                        "lblSeperator": "lblSeperator",
                        "Extension": emailObj.Extension,
                        "template": "flxProfileManagementEmail"
                    }
                    if (CommonUtilities.isCSRMode()) {
                        dataObject.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                    }
                    return dataObject;
                })
                var businessData = businessEmailList.map(function(emailObj) {
                    if (emailObj.isPrimary === "true" && emailObj.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "175px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryAlertComm");
                        this.view.customheader.lblUserEmail.text = emailObj.Value;
                    } else if (emailObj.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "120px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.alertComm");
                    } else if (emailObj.isPrimary === "true") {
                        flexWidth = "135px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryComm");
                    }
                    var dataObject = {
                        "lblEmail": {
                            "text": emailObj.Value,
                            "accessibilityconfig": {
                                "a11yLabel": emailObj.Value,
                            }
                        },
                        "flxUsedFor": {
                            "isVisible": emailObj.isPrimary === "true" || (emailObj.isAlertsRequired === "true" && scopeObj.enableSeparateContact) ? true : false
                        },
                        "flxPrimary": {
                            "width": flexWidth
                        },
                        "lblPrimary": {
                            "text": textToShow,
                            "accessibilityconfig": {
                                "a11yLabel": textToShow,
                            },
                        },
                        "lblUsedFor": {
                            "text": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor")
                            }
                        },
                        "btnDelete": {
                            "text": emailObj.isPrimary == "true" ? " " : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            "accessibilityconfig": {
                                "a11yLabel": emailObj.isPrimary == "true" ? " " : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount")
                            },
                            "onClick": CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : getDeleteEmailListener(emailObj),
                            "skin": CommonUtilities.isCSRMode() ? CommonUtilities.disableSegmentButtonSkinForCSRMode(13) : "sknBtnSSPFont4176a415px",
                            "isVisible": false
                        },
                        "btnEdit": {
                            "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            },
                            "onClick": (emailObj.isPrimary == "true") ? scopeObj.editPrimaryEmail.bind(scopeObj, emailObj) : scopeObj.editEmail.bind(scopeObj, emailObj),
                            "isVisible": applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")
                        },
                        "lblSeperator": "lblSeperator",
                        "Extension": emailObj.Extension,
                        "template": "flxProfileManagementEmail"
                    }
                    if (CommonUtilities.isCSRMode()) {
                        dataObject.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                    }
                    return dataObject;
                })
                this.view.settings.segPersonalEmailIds.widgetDataMap = dataMap;
                this.view.settings.segBusinessEmailIds.widgetDataMap = dataMap;
                if (kony.application.getCurrentBreakpoint() === 1024) {
                    for (var i = 0; i < personalData.length; i++) {
                        personalData[i].template = "flxProfileManagementEmailTablet";
                    }
                };
                if (kony.application.getCurrentBreakpoint() === 1024) {
                    for (var i = 0; i < businessData.length; i++) {
                        businessData[i].template = "flxProfileManagementEmailTablet";
                    }
                };
                if (personalData.length === 3)
                    this.view.settings.btnAddNewPersonalEmail.setVisibility(false);
                else
                    this.view.settings.btnAddNewPersonalEmail.setVisibility(true);
                if (personalData.length === 0)
                    this.view.settings.flxPersonalEmailBody.setVisibility(false);
                else
                    this.view.settings.flxPersonalEmailBody.setVisibility(true);
                if (businessData.length === 0)
                    this.view.settings.flxBusinessEmailBody.setVisibility(false);
                else
                    this.view.settings.flxBusinessEmailBody.setVisibility(true);
                this.view.settings.segPersonalEmailIds.setData(personalData);
                this.view.settings.segBusinessEmailIds.setData(businessData);
            } else {
                var data = emailList.map(function(emailObj) {
                    if (emailObj.isPrimary === "true" && emailObj.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "175px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryAlertComm");
                        this.view.customheader.lblUserEmail.text = emailObj.Value;
                    } else if (emailObj.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "120px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.alertComm");
                    } else if (emailObj.isPrimary === "true") {
                        flexWidth = "135px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryComm");
                    }
                    var dataObject = {
                        "lblEmail": {
                            "text": emailObj.Value,
                            "accessibilityconfig": {
                                "a11yLabel": emailObj.Value,
                            }
                        },
                        "flxUsedFor": {
                            "isVisible": emailObj.isPrimary === "true" || (emailObj.isAlertsRequired === "true" && scopeObj.enableSeparateContact) ? true : false
                        },
                        "flxPrimary": {
                            "width": flexWidth
                        },
                        "lblPrimary": {
                            "text": textToShow,
                            "accessibilityconfig": {
                                "a11yLabel": textToShow,
                            },
                        },
                        "lblUsedFor": {
                            "text": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor")
                            }
                        },
                        "btnDelete": {
                            "text": emailObj.isPrimary == "true" ? " " : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            "accessibilityconfig": {
                                "a11yLabel": emailObj.isPrimary == "true" ? " " : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            },
                            "onClick": CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : getDeleteEmailListener(emailObj),
                            "skin": CommonUtilities.isCSRMode() ? CommonUtilities.disableSegmentButtonSkinForCSRMode(13) : "sknBtnSSPFont4176a415px",
                            "isVisible": emailObj.isPrimary == "true" ? false : true
                        },
                        "btnEdit": {
                            "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            },
                            "onClick": (emailObj.isPrimary == "true") ? scopeObj.editPrimaryEmail.bind(scopeObj, emailObj) : scopeObj.editEmail.bind(scopeObj, emailObj),
                            "isVisible": applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")
                        },
                        "lblSeperator": "lblSeperator",
                        "Extension": emailObj.Extension,
                        "template": "flxProfileManagementEmail"
                    }
                    if (CommonUtilities.isCSRMode()) {
                        dataObject.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                    }
                    return dataObject;
                })
                this.view.settings.segEmailIds.widgetDataMap = dataMap;
                if (kony.application.getCurrentBreakpoint() === 1024) {
                    for (var i = 0; i < data.length; i++) {
                        data[i].template = "flxProfileManagementEmailTablet";
                    }
                };
                this.view.settings.segEmailIds.setData(data);
            }

            FormControllerUtility.hideProgressBar(this.view);
            this.view.forceLayout();
        },
        toggleContextualMenuEmail: function() {
            var index = this.view.settings.segEmailIds.selectedRowIndex[1];
            var scrollpos = this.view.contentOffsetMeasured;
            var hgt = ((index) * 51) + 108;
            if (this.view.settings.AllForms1.isVisible === false)
                this.view.settings.AllForms1.isVisible = true;
            else
                this.view.settings.AllForms1.isVisible = false;
            this.view.settings.AllForms1.top = hgt + "dp";
            leftInfo = this.view.settings.segEmailIds.clonedTemplates[index].flxInfo.frame.x + this.view.settings.segEmailIds.clonedTemplates[index].flxPrimary.frame.x - 130;
            this.view.settings.AllForms1.left = leftInfo + "dp";
            this.view.settings.AllForms1.right = "";
            this.view.settings.AllForms1.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.msgInfoEmail");
            this.view.forceLayout();
            this.view.setContentOffset(scrollpos);
        },
        toggleContextualMenuAddress: function() {
            var index = this.view.settings.segAddresses.selectedRowIndex[1];
            var scrollpos = this.view.contentOffsetMeasured;
            var hgt = ((index) * 144) + 108;
            if (this.view.settings.AllForms1.isVisible === false)
                this.view.settings.AllForms1.isVisible = true;
            else
                this.view.settings.AllForms1.isVisible = false;
            this.view.settings.AllForms1.top = hgt + "dp";
            this.view.settings.AllForms1.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.msgInfoAddress");
            leftInfo = this.view.settings.segAddresses.clonedTemplates[index].flxInfo.frame.x + this.view.settings.segAddresses.clonedTemplates[index].flxCommunicationAddress.frame.x - 130;
            this.view.settings.AllForms1.left = leftInfo + "dp";
            this.view.settings.AllForms1.right = "";
            this.view.forceLayout();
            this.view.setContentOffset(scrollpos);
        },
        /**
         * Method to edit the email which is already set
         * @param {Object} emailObj- JSON object of the email with all fields comminf from backend
         */
        editEmail: function(emailObj) {
            var scopeObj = this;
            this.view.settings.lblEditMarkAsPrimaryEmail.setVisibility(!(emailObj.isPrimary == "true"));
            this.view.settings.flxEditMarkAsPrimaryEmailCheckBox.setVisibility(!(emailObj.isPrimary == "true"));
            CommonUtilities.setText(scopeObj.view.settings.lblEditMarkAsPrimaryEmailCheckBox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            scopeObj.view.settings.lblEditMarkAsPrimaryEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.view.settings.flxEditMarkAsAlertCommEmailCheckBox.top = this.view.settings.lblEditMarkAsPrimaryEmail.isVisible ? "110px" : "80px";
            this.view.settings.lblEditMarkAsAlertCommEmail.top = this.view.settings.lblEditMarkAsPrimaryEmail.isVisible ? "115px" : "85px";
            this.view.settings.lblEditMarkAsAlertCommEmail.setVisibility((emailObj.isAlertsRequired !== "true" && scopeObj.enableSeparateContact));
            this.view.settings.flxEditMarkAsAlertCommEmailCheckBox.setVisibility((emailObj.isAlertsRequired !== "true" && scopeObj.enableSeparateContact));
            this.view.settings.lblEditMarkAsAlertCommEmailCheckBox.text = emailObj.isAlertsRequired ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            this.view.settings.lblEditMarkAsAlertCommEmailCheckBox.skin = emailObj.isAlertsRequired ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            scopeObj.showEditEmail();
            if (emailObj.isTypeBusiness === "1") {
                this.view.settings.tbxEditEmailId.setVisibility(false);
                this.view.settings.lblEmailValue.setVisibility(true);
                this.view.settings.lblEmailValue.text = emailObj.Value;
            }
            this.view.forceLayout();
            scopeObj.view.settings.tbxEditEmailId.text = emailObj.Value;
            this.checkUpdateEmailForm();
            if (CommonUtilities.isCSRMode()) {
                scopeObj.view.settings.btnEditEmailIdSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                scopeObj.view.settings.btnEditEmailIdSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                scopeObj.view.settings.btnEditEmailIdSave.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.editEmail({
                        id: emailObj.id,
                        extension: emailObj.Extension,
                        email: scopeObj.view.settings.tbxEditEmailId.text,
                        isPrimary: scopeObj.view.settings.lblEditMarkAsPrimaryEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? true : false,
                        isAlertsRequired: scopeObj.view.settings.lblEditMarkAsAlertCommEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? true : false,
                        isTypeBusiness: emailObj.isTypeBusiness
                    });
                scopeObj.view.customheader.headermenu.setFocus(true);
                };
            }
            scopeObj.checkAddEmailForm();
        },
        /**
         * Method to reset fields in the UI of Email module and show the module
         */
        showEditEmail: function() {
            this.resetUpdateEmailForm();
            this.showViews(["flxEditEmailWrapper"]);
        },
        /**
         * Method to reset fields in the UI of Email module
         */
        resetUpdateEmailForm: function() {
            this.view.settings.tbxEditEmailId.text = "";
            CommonUtilities.setText(this.view.settings.lblEditMarkAsPrimaryEmailCheckBox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            this.view.settings.lblEditMarkAsPrimaryEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            CommonUtilities.setText(this.view.settings.lblEditMarkAsAlertCommEmailCheckBox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            this.view.settings.lblEditMarkAsAlertCommEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.disableButton(this.view.settings.btnEditEmailIdSave);
            this.view.settings.flxErrorEditEmail.setVisibility(false);
            this.view.settings.lblEmailValue.setVisibility(false);
            this.view.settings.tbxEditEmailId.setVisibility(true);
        },
        /**
         * Method to Disable a button
         * @param {String} button - ID of the button to be disabled
         */
        disableButton: function(button) {
            button.setEnabled(false);
            button.skin = "sknBtnBlockedSSPFFFFFF15Px";
            button.hoverSkin = "sknBtnBlockedSSPFFFFFF15Px";
            button.focusSkin = "sknBtnBlockedSSPFFFFFF15Px";
        },
        /**
         * Method to edit the primary Email of the User
         * @param {JSON} emailObj- JSON object with email and its ID
         */
        editPrimaryEmail: function(emailObj) {
            var scopeObj = this;
            this.view.settings.lblEditMarkAsPrimaryEmail.setVisibility(false);
            this.view.settings.flxEditMarkAsPrimaryEmailCheckBox.setVisibility(false);
            this.view.settings.lblEditMarkAsAlertCommEmail.setVisibility((emailObj.isAlertsRequired !== "true" && scopeObj.enableSeparateContact));
            this.view.settings.flxEditMarkAsAlertCommEmailCheckBox.setVisibility((emailObj.isAlertsRequired !== "true" && scopeObj.enableSeparateContact));
            this.view.settings.flxEditMarkAsAlertCommEmailCheckBox.top = this.view.settings.lblEditMarkAsPrimaryEmail.isVisible ? "110px" : "80px";
            this.view.settings.lblEditMarkAsAlertCommEmail.top = this.view.settings.lblEditMarkAsPrimaryEmail.isVisible ? "115px" : "85px";
            CommonUtilities.setText(this.view.settings.lblEditMarkAsAlertCommEmailCheckBox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            this.view.settings.lblEditMarkAsAlertCommEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.view.forceLayout();
            scopeObj.showEditEmail();
            if (emailObj.isTypeBusiness === "1") {
                this.view.settings.tbxEditEmailId.setVisibility(false);
                this.view.settings.lblEmailValue.setVisibility(true);
                this.view.settings.lblEmailValue.text = emailObj.Value;
            }
            scopeObj.view.settings.tbxEditEmailId.text = emailObj.Value;
            this.checkUpdateEmailForm();
            if (CommonUtilities.isCSRMode()) {
                scopeObj.view.settings.btnEditEmailIdSave.onClick = CommonUtilities.disableButtonSkinForCSRMode();
                scopeObj.view.settings.btnEditEmailIdSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                scopeObj.view.settings.btnEditEmailIdSave.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.editEmail({
                        id: emailObj.id,
                        extension: emailObj.extension,
                        email: scopeObj.view.settings.tbxEditEmailId.text,
                        isPrimary: true,
                        isAlertsRequired: scopeObj.view.settings.lblEditMarkAsAlertCommEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? true : false,
                        isTypeBusiness: emailObj.isTypeBusiness
                    });
                scopeObj.view.customheader.headermenu.setFocus(true);
                };
            }
            scopeObj.checkUpdateEmailForm();
        },
        /**
         * Method to enable/disable button based on the email entered after editing
         */
        checkUpdateEmailForm: function() {
            if (!CommonUtilities.isCSRMode()) {
                if (applicationManager.getValidationUtilManager().isValidEmail(this.view.settings.tbxEditEmailId.text)) {
                    this.enableButton(this.view.settings.btnEditEmailIdSave);
                } else {
                    this.disableButton(this.view.settings.btnEditEmailIdSave);
                }
            }
        },
        /**
         * Method to Enable a button
         * @param {String} button - ID of the button to be enabled
         */
        enableButton: function(button) {
            if (!CommonUtilities.isCSRMode()) {
                button.setEnabled(true);
                button.skin = "sknbtnSSPffffff15px0273e3bg";
                button.hoverSkin = "sknBtnFocusSSPFFFFFF15Px0273e3";
                button.focusSkin = "sknBtnHoverSSPFFFFFF15Px0273e3";
            }
        },
        /**
         * Method to update the module while adding a new phone number
         * @param {Object} addPhoneViewModel- responce from backend after fetching phone number
         */
        updateAddPhoneViewModel: function(addPhoneViewModel) {
            this.checkAddNewPhoneForm();
            if (addPhoneViewModel.serverError) {
                this.view.settings.flxErrorAddPhoneNumber.setVisibility(true);
                CommonUtilities.setText(this.view.settings.CopylblError0f2f036aaf7534c, addPhoneViewModel.serverError.errorMessage, CommonUtilities.getaccessibilityConfig());
            } else {
                this.showAddPhonenumber();
                this.view.settings.flxErrorAddPhoneNumber.setVisibility(false);
                //this.view.settings.imgAddRadioBtnUS.src = addPhoneViewModel.countryType === 'domestic' ?  ViewConstants.IMAGES.ICON_RADIOBTN: ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE ;
                this.showPhoneRadiobtn(this.view.settings.imgAddRadioBtnUS, this.view.settings.imgAddRadioBtnInternational);
                this.view.settings.lbxAddPhoneNumberType.masterData = addPhoneViewModel.phoneTypes;
                this.view.settings.lbxAddPhoneNumberType.selectedKey = addPhoneViewModel.phoneTypeSelected;
                this.view.settings.tbxAddPhoneNumber.text = addPhoneViewModel.phoneNumber;
                this.view.settings.tbxAddPhoneNumberCountryCode.text = addPhoneViewModel.phoneCountryCode;
                this.view.settings.tbxAddExtension.text = addPhoneViewModel.ext;
                this.view.settings.lblAddCheckBox3.skin = addPhoneViewModel.isPrimary ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                CommonUtilities.setText(this.view.settings.lblAddCheckBox3, addPhoneViewModel.isPrimary ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                this.view.settings.lblAddCheckBox4.skin = addPhoneViewModel.isAlertsRequired ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                CommonUtilities.setText(this.view.settings.lblAddCheckBox4, addPhoneViewModel.isAlertsRequired ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                // this.setAddPhoneServicesData(addPhoneViewModel.services);
                this.view.settings.btnAddPhoneNumberCancel.onClick = function() {
                    this.showPhoneNumbers();
                }.bind(this);
                if (CommonUtilities.isCSRMode()) {
                    this.view.settings.btnAddPhoneNumberSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    this.view.settings.btnAddPhoneNumberSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    this.view.settings.btnAddPhoneNumberSave.onClick = function() {
                        if (this.validateAddPhoneNumberForm()) {
                            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.savePhoneNumber(this.getNewPhoneFormData());
                            this.view.settings.flxHeader.setFocus(true);
                        }
                    }.bind(this);
                }
                this.view.settings.flxEditPhoneNumberButtons.setVisibility(true);
            }
        },
        /**
         * Method to enable/disable button based on the new phone number entered
         */
        checkAddNewPhoneForm: function() {
            var formdata = this.getNewPhoneFormData();
            if (formdata.phoneNumber === "") {
                this.disableButton(this.view.settings.btnAddPhoneNumberSave);
            } else {
                this.enableButton(this.view.settings.btnAddPhoneNumberSave);
            }
        },
        /**
         * Method to show the UI of Add Phone Number
         */
        showAddPhonenumber: function() {
            this.showViews(["flxAddPhoneNumbersWrapper"]);
            FormControllerUtility.hideErrorForTextboxFields(this.view.settings.tbxAddPhoneNumber, this.view.flxError);
            FormControllerUtility.hideErrorForTextboxFields(this.view.settings.tbxAddPhoneNumberCountryCode, this.view.settings.flxError);
            this.checkAddNewPhoneForm();
            this.view.settings.flxAddPhoneNumberCountryCode.setVisibility(true);
            if (applicationManager.getConfigurationManager().getCountryCodeFlag() == true) {
                this.view.settings.tbxAddPhoneNumberCountryCode.setVisibility(true);
            } else {
                this.view.settings.tbxAddPhoneNumberCountryCode.setVisibility(false);
            }
            this.view.settings.flxAddOption2.setVisibility(false);
            if (this.enableSeparateContact === true) {
                this.view.settings.flxAddOption4.setVisibility(true);
            } else {
                this.view.settings.flxAddOption4.setVisibility(false);
            }
        },
        showPhoneNumbers: function() {
            this.showViews(["flxPhoneNumbersWrapper"]);
        },
        /**
         * Method to update add phone number UI based on the error or success senario
         */
        validateAddPhoneNumberForm: function() {
            var phoneData = this.getNewPhoneFormData();
            if ((!applicationManager.getValidationUtilManager().isValidPhoneNumber(phoneData.phoneNumber)) || (phoneData.phoneNumber.length < 7 && phoneData.phoneNumber.length > 15)) {
                FormControllerUtility.showErrorForTextboxFields(this.view.settings.tbxAddPhoneNumber, this.view.settings.flxErrorAddPhoneNumber);
                CommonUtilities.setText(this.view.settings.CopylblError0f2f036aaf7534c, kony.i18n.getLocalizedString("i18n.profile.notAValidPhoneNumber"), CommonUtilities.getaccessibilityConfig());
                return false;
            } else {
                FormControllerUtility.hideErrorForTextboxFields(this.view.settings.tbxAddPhoneNumber, this.view.flxErrorAddPhoneNumber);
            }
            if (!applicationManager.getValidationUtilManager().isValidPhoneNumberCountryCode(phoneData.phoneCountryCode)) {
                FormControllerUtility.showErrorForTextboxFields(this.view.settings.tbxAddPhoneNumberCountryCode, this.view.settings.flxErrorAddPhoneNumber);
                CommonUtilities.setText(this.view.settings.CopylblError0f2f036aaf7534c, kony.i18n.getLocalizedString("i18n.profile.enterPhoneNumberCountryCode"), CommonUtilities.getaccessibilityConfig());
                return false;
            } else {
                FormControllerUtility.hideErrorForTextboxFields(this.view.settings.tbxAddPhoneNumberCountryCode, this.view.settings.flxErrorAddPhoneNumber);
            }
            return true;
        },
        /**
         * Method to change the image of radio button
         * @param {String} imgRadio1- path of the first radio button
         * @param {String} imgRadio2- path of the second radio button
         */
        showPhoneRadiobtn: function(imgRadio1, imgRadio2) {
            if (imgRadio1.src === ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE) {
                imgRadio1.src = ViewConstants.IMAGES.ICON_RADIOBTN;
                imgRadio2.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
            } else {
                imgRadio1.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
                imgRadio2.src = ViewConstants.IMAGES.ICON_RADIOBTN;
            }
            this.view.forceLayout();
        },
        /**
         * Method to set the mapping to save phone data
         * @param {JSON} services - Array of accounts
         */
        /*setAddPhoneServicesData: function(services) {
            var scopeObj = this;
            var dataMap = {
                "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
                "flxCheckbox": "flxCheckbox",
                "lblCheckBox": "lblCheckBox",
                "lblAccounts": "lblAccounts"
            };
            function getCheckBoxListener(serviceObj, index) {
                return function() {
                    var data = scopeObj.view.settings.segAddPhoneNumbersOption1.data[index];
                    data.lblCheckBox.skin = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                    data.lblCheckBox.text = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED : ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                    scopeObj.view.settings.segAddPhoneNumbersOption1.setDataAt(data, index);
                }
            }
            var data = services.map(function(serviceObj, index) {
                return {
                    "id": serviceObj.id,
                    "lblCheckBox": {"text":ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,"skin":ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN},
                    "lblAccounts": serviceObj.name,
                    "flxCheckbox": {
                        "onClick": getCheckBoxListener(serviceObj, index)
                    }
                }
            })
            this.view.settings.segAddPhoneNumbersOption1.widgetDataMap = dataMap;
            this.view.settings.segAddPhoneNumbersOption1.setData(data);
            this.view.forceLayout();
        },*/
        /**
         * Method to show data related to phone numbers
         * @returns {Object} - contains the type, phoneNumber, extension, isPrimary, services, receivePromotions attributes.
         */
        getNewPhoneFormData: function() {
            return {
                type: this.view.settings.lbxAddPhoneNumberType.selectedKey,
                phoneNumber: this.view.settings.tbxAddPhoneNumber.text.trim(),
                phoneCountryCode: this.view.settings.tbxAddPhoneNumberCountryCode.text.trim(),
                extension: this.view.settings.tbxAddExtension.text.trim(),
                isPrimary: this.view.settings.lblAddCheckBox3.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                isAlertsRequired: this.view.settings.lblAddCheckBox4.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                /* services: this.view.settings.segAddPhoneNumbersOption1.data == undefined ? {} : this.view.settings.segAddPhoneNumbersOption1.data
                     .filter(function(data) {
                         return data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED
                     })
                     .map(function(data) {
                         return data.id
                     }),*/
                receivePromotions: false,
            }
        },
        /**
         * Method to update the list of phone number
         * @param {Object} phoneListViewModel- list of phone numbers
         */
        updatePhoneList: function(phoneListViewModel) {
            this.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
            this.showPhoneNumbers();
            if (phoneListViewModel.length >= 3 || !applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")) {
                this.view.settings.btnAddNewNumber.setVisibility(false);
            } else {
                this.view.settings.btnAddNewNumber.setVisibility(true);
                this.view.settings.btnAddNewPersonalNumber.setVisibility(true);
            }
            this.setPhoneSegmentData(phoneListViewModel);
            this.setSelectedSkin("flxPhone");
        },
        getPhoneNumber: function(phoneObj) {
            if (applicationManager.getConfigurationManager().getCountryCodeFlag() == true) {
                return phoneObj.phoneCountryCode + "-" + phoneObj.phoneNumber;
            } else {
                return phoneObj.phoneNumber;
            }
        },
        /**
         * Method to set data  of all the phone numbers
         * @param {Object} phoneListViewModel - List of all the phone numbers got from backend
         */
        setPhoneSegmentData: function(phoneListViewModel) {
            var isCombinedUser = applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true";
            var scopeObj = this;
            var phoneBusinessListViewModel = [];
            var phonePersonalListViewModel = [];
            if (isCombinedUser) {
                phoneListViewModel.forEach(function(dataItem) {
                    if (!kony.sdk.isNullOrUndefined(dataItem.isTypeBusiness) && dataItem.isTypeBusiness === "1") phoneBusinessListViewModel.push(dataItem);
                    else phonePersonalListViewModel.push(dataItem);
                });
                this.view.settings.flxCombinedPhoneNumbers.setVisibility(true);
                this.view.settings.flxPhoneNumbers.setVisibility(false);
            } else {
                this.view.settings.flxCombinedPhoneNumbers.setVisibility(false);
                this.view.settings.flxPhoneNumbers.setVisibility(true);
            }
            var dataMap = {
                "btnViewDetail": "btnViewDetail",
                "btnDelete": "btnDelete",
                "flxCheckBox1": "flxCheckBox1",
                "flxCheckBox2": "flxCheckBox2",
                "flxCheckBox3": "flxCheckBox3",
                "flxCheckBox4": "flxCheckBox4",
                "flxCollapsible": "flxCollapsible",
                "flxDeleteAction": "flxDeleteAction",
                "flxEdit": "flxEdit",
                "flxOption1": "flxOption1",
                "flxOption2": "flxOption2",
                "flxOption3": "flxOption3",
                "flxOption4": "flxOption4",
                "flxOptions": "flxOptions",
                "flxPhoneNumber": "flxPhoneNumber",
                "flxPrimary": "flxPrimary",
                "flxUsedFor": "flxUsedFor",
                "flxRow": "flxRow",
                "flxSelectedPhoneNumbers": "flxSelectedPhoneNumbers",
                "lblCheckBox1": "lblCheckBox1",
                "lblCheckBox2": "lblCheckBox2",
                "lblCheckBox3": "lblCheckBox3",
                "lblCheckBox4": "lblCheckBox4",
                "imgCollapsible": "imgCollapsible",
                "lblHome": "lblHome",
                "lblOption1": "lblOption1",
                "lblOption2": "lblOption2",
                "lblOption3": "lblOption3",
                "lblOption4": "lblOption4",
                "lblOptionSeperator": "lblOptionSeperator",
                "lblPhoneNumber": "lblPhoneNumber",
                "lblPleaseChoose": "lblPleaseChoose",
                "lblPrimary": "lblPrimary",
                "lblUsedFor": "lblUsedFor",
                "lblSelectedSeperator": "lblSelectedSeperator",
                "lblSeperator": "lblSeperator",
                "template": "template",
                "lblCountryCode": "lblCountryCode"
            };

            function getShowDetailListener(phoneModel) {
                return function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getPhoneDetails(phoneModel);
                }
            }
            var isCountryCodeEnabled = applicationManager.getConfigurationManager().getCountryCodeFlag();
            var flexWidth = "135px";
            var textToShow = "";
            if (isCombinedUser) {
                var personalData = phonePersonalListViewModel.map(function(phoneModel) {
                    if (phoneModel.isPrimary === "true" && phoneModel.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "175px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryAlertComm");
                    } else if (phoneModel.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "120px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.alertComm");
                    } else if (phoneModel.isPrimary === "true") {
                        flexWidth = "135px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryComm");
                    }
                    return {
                        "imgCollapsible": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "lblHome": {
                            "text": CommonUtilities.changedataCase(phoneModel.Extension),
                            "accessibilityconfig": {
                                "a11yLabel": CommonUtilities.changedataCase(phoneModel.Extension),
                            },
                        },
                        "lblPhoneNumber": {
                            "text": (phoneModel.phoneNumber) ? phoneModel.phoneNumber : " ",
                            "accessibilityconfig": {
                                "a11yLabel": (phoneModel.phoneNumber) ? phoneModel.phoneNumber : " ",
                            },
                        },
                        "lblCountryCode": {
                            "text": isCountryCodeEnabled == true && !(kony.sdk.isNullOrUndefined(phoneModel.phoneCountryCode)) ? phoneModel.phoneCountryCode : "",
                            "accessibilityconfig": {
                                "a11yLabel": isCountryCodeEnabled == true && !(kony.sdk.isNullOrUndefined(phoneModel.phoneCountryCode)) ? phoneModel.phoneCountryCode : "",
                            },
                        },
                        "flxUsedFor": {
                            "isVisible": phoneModel.isPrimary === "true" || (phoneModel.isAlertsRequired === "true" && scopeObj.enableSeparateContact) ? true : false
                        },
                        "flxPrimary": {
                            "width": flexWidth
                        },
                        "lblPrimary": {
                            "text": textToShow,
                            "accessibilityconfig": {
                                "a11yLabel": textToShow,
                            },
                        },
                        "lblUsedFor": {
                            "text": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor")
                            }
                        },
                        "btnViewDetail": {
                            text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            },
                            toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            onClick: scopeObj.editPhone.bind(scopeObj, phoneModel)
                        },
                        "btnDelete": {
                            text: "Delete",
                            toolTip: "Delete",
                            onClick: scopeObj.deletePhone.bind(scopeObj, phoneModel),
                            isVisible: phoneModel.isPrimary === "true" ? false : true
                        },
                        "lblPleaseChoose": {
                            "text": "Please choose what service you like to use this number for:",
                            "accessibilityconfig": {
                                "a11yLabel": "Please choose what service you like to use this number for:",
                            },
                        },
                        "lblOption1": {
                            "text": "Joint Savings ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            "accessibilityconfig": {
                                "a11yLabel": "Joint Savings ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            },
                        },
                        "lblOption2": {
                            "text": "Joint Checking ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            "accessibilityconfig": {
                                "a11yLabel": "Joint Checking ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            },
                        },
                        "lblOption3": {
                            "text": "Make my primary phone number",
                            "accessibilityconfig": {
                                "a11yLabel": "Make my primary phone number",
                            },
                        },
                        "lblOption4": {
                            "text": "Allow to recieve text messages",
                            "accessibilityconfig": {
                                "a11yLabel": "Allow to recieve text messages",
                            },
                        },
                        "lblCheckBox1": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblCheckBox2": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblCheckBox3": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblCheckBox4": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblSelectedSeperator": "lblSelectedSeperator",
                        "lblSeperator": "lblSeperator",
                        "lblOptionSeperator": "lblOptionSeperator",
                        "template": "flxProfileManagementPhoneNumbers",
                        "phoneCountryCode": phoneModel.phoneCountryCode,
                        "Extension": phoneModel.Extension,
                        "phoneNumber": phoneModel.Value
                    };
                })
                var businessData = phoneBusinessListViewModel.map(function(phoneModel) {
                    if (phoneModel.isPrimary === "true" && phoneModel.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "175px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryAlertComm");
                    } else if (phoneModel.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "120px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.alertComm");
                    } else if (phoneModel.isPrimary === "true") {
                        flexWidth = "135px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryComm");
                    }
                    return {
                        "imgCollapsible": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "lblHome": {
                            "text": CommonUtilities.changedataCase(phoneModel.Extension),
                            "accessibilityconfig": {
                                "a11yLabel": CommonUtilities.changedataCase(phoneModel.Extension),
                            },
                        },
                        "lblPhoneNumber": {
                            "text": (phoneModel.phoneNumber) ? phoneModel.phoneNumber : " ",
                            "accessibilityconfig": {
                                "a11yLabel": (phoneModel.phoneNumber) ? phoneModel.phoneNumber : " ",
                            },
                        },
                        "lblCountryCode": {
                            "text": isCountryCodeEnabled == true && !(kony.sdk.isNullOrUndefined(phoneModel.phoneCountryCode)) ? phoneModel.phoneCountryCode : "",
                            "accessibilityconfig": {
                                "a11yLabel": isCountryCodeEnabled == true && !(kony.sdk.isNullOrUndefined(phoneModel.phoneCountryCode)) ? phoneModel.phoneCountryCode : "",
                            },
                        },
                        "flxUsedFor": {
                            "isVisible": phoneModel.isPrimary === "true" || (phoneModel.isAlertsRequired === "true" && scopeObj.enableSeparateContact) ? true : false
                        },
                        "flxPrimary": {
                            "width": flexWidth
                        },
                        "lblPrimary": {
                            "text": textToShow,
                            "accessibilityconfig": {
                                "a11yLabel": textToShow,
                            },
                        },
                        "lblUsedFor": {
                            "text": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor")
                            }
                        },
                        "btnViewDetail": {
                            text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            },
                            toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            onClick: scopeObj.editPhone.bind(scopeObj, phoneModel)
                        },
                        "btnDelete": {
                            text: "Delete",
                            toolTip: "Delete",
                            onClick: scopeObj.deletePhone.bind(scopeObj, phoneModel),
                            isVisible: false
                        },
                        "lblPleaseChoose": {
                            "text": "Please choose what service you like to use this number for:",
                            "accessibilityconfig": {
                                "a11yLabel": "Please choose what service you like to use this number for:",
                            },
                        },
                        "lblOption1": {
                            "text": "Joint Savings ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            "accessibilityconfig": {
                                "a11yLabel": "Joint Savings ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            },
                        },
                        "lblOption2": {
                            "text": "Joint Checking ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            "accessibilityconfig": {
                                "a11yLabel": "Joint Checking ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            },
                        },
                        "lblOption3": {
                            "text": "Make my primary phone number",
                            "accessibilityconfig": {
                                "a11yLabel": "Make my primary phone number",
                            },
                        },
                        "lblOption4": {
                            "text": "Allow to recieve text messages",
                            "accessibilityconfig": {
                                "a11yLabel": "Allow to recieve text messages",
                            },
                        },
                        "lblCheckBox1": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblCheckBox2": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblCheckBox3": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblCheckBox4": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblSelectedSeperator": "lblSelectedSeperator",
                        "lblSeperator": "lblSeperator",
                        "lblOptionSeperator": "lblOptionSeperator",
                        "template": "flxProfileManagementPhoneNumbers",
                        "phoneCountryCode": phoneModel.phoneCountryCode,
                        "Extension": phoneModel.Extension,
                        "phoneNumber": phoneModel.Value
                    };
                })
                if (personalData.length === 3)
                    this.view.settings.btnAddNewPersonalNumber.setVisibility(false);
                else
                    this.view.settings.btnAddNewPersonalNumber.setVisibility(true);
                if (personalData.length === 0)
                    this.view.settings.flxPersonalPhoneNumbersBody.setVisibility(false);
                else
                    this.view.settings.flxPersonalPhoneNumbersBody.setVisibility(true);
                if (businessData.length === 0)
                    this.view.settings.flxBusinessPhoneNumbersBody.setVisibility(false);
                else
                    this.view.settings.flxBusinessPhoneNumbersBody.setVisibility(true);
                this.view.settings.segPersonalPhoneNumbers.widgetDataMap = dataMap;
                this.view.settings.segBusinessPhoneNumbers.widgetDataMap = dataMap;
                if (kony.application.getCurrentBreakpoint() === 1024) {
                    for (var i = 0; i < personalData.length; i++) {
                        CommonUtilities.setText(personalData[i].btnViewDetail, "Edit", CommonUtilities.getaccessibilityConfig());
                    }
                };
                if (kony.application.getCurrentBreakpoint() === 1024) {
                    for (var i = 0; i < businessData.length; i++) {
                        CommonUtilities.setText(businessData[i].btnViewDetail, "Edit", CommonUtilities.getaccessibilityConfig());
                    }
                };
                this.view.settings.segPersonalPhoneNumbers.setData(personalData);
                this.view.settings.segBusinessPhoneNumbers.setData(businessData);
            } else {
                var data = phoneListViewModel.map(function(phoneModel) {
                    if (phoneModel.isPrimary === "true" && phoneModel.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "175px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryAlertComm");
                    } else if (phoneModel.isAlertsRequired === "true" && scopeObj.enableSeparateContact) {
                        flexWidth = "120px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.alertComm");
                    } else if (phoneModel.isPrimary === "true") {
                        flexWidth = "135px";
                        textToShow = kony.i18n.getLocalizedString("i18n.alertSettings.PrimaryComm");
                    }
                    return {
                        "imgCollapsible": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "lblHome": {
                            "text": CommonUtilities.changedataCase(phoneModel.Extension),
                            "accessibilityconfig": {
                                "a11yLabel": CommonUtilities.changedataCase(phoneModel.Extension),
                            },
                        },
                        "lblPhoneNumber": {
                            "text": (phoneModel.phoneNumber) ? phoneModel.phoneNumber : " ",
                            "accessibilityconfig": {
                                "a11yLabel": (phoneModel.phoneNumber) ? phoneModel.phoneNumber : " ",
                            },
                        },
                        "lblCountryCode": {
                            "text": isCountryCodeEnabled == true && !(kony.sdk.isNullOrUndefined(phoneModel.phoneCountryCode)) ? phoneModel.phoneCountryCode : "",
                            "accessibilityconfig": {
                                "a11yLabel": isCountryCodeEnabled == true && !(kony.sdk.isNullOrUndefined(phoneModel.phoneCountryCode)) ? phoneModel.phoneCountryCode : "",
                            },
                        },
                        "flxUsedFor": {
                            "isVisible": phoneModel.isPrimary === "true" || (phoneModel.isAlertsRequired === "true" && scopeObj.enableSeparateContact) ? true : false
                        },
                        "flxPrimary": {
                            "width": flexWidth
                        },
                        "lblPrimary": {
                            "text": textToShow,
                            "accessibilityconfig": {
                                "a11yLabel": textToShow,
                            },
                        },
                        "lblUsedFor": {
                            "text": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.alertSettings.Usedfor")
                            }
                        },
                        "btnViewDetail": {
                            text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            },
                            toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            onClick: scopeObj.editPhone.bind(scopeObj, phoneModel)
                        },
                        "btnDelete": {
                            text: "Delete",
                            toolTip: "Delete",
                            onClick: scopeObj.deletePhone.bind(scopeObj, phoneModel),
                            isVisible: phoneModel.isPrimary === "true" ? false : true
                        },
                        "lblPleaseChoose": {
                            "text": "Please choose what service you like to use this number for:",
                            "accessibilityconfig": {
                                "a11yLabel": "Please choose what service you like to use this number for:",
                            },
                        },
                        "lblOption1": {
                            "text": "Joint Savings ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            "accessibilityconfig": {
                                "a11yLabel": "Joint Savings ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            },
                        },
                        "lblOption2": {
                            "text": "Joint Checking ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            "accessibilityconfig": {
                                "a11yLabel": "Joint Checking ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦. 1234",
                            },
                        },
                        "lblOption3": {
                            "text": "Make my primary phone number",
                            "accessibilityconfig": {
                                "a11yLabel": "Make my primary phone number",
                            },
                        },
                        "lblOption4": {
                            "text": "Allow to recieve text messages",
                            "accessibilityconfig": {
                                "a11yLabel": "Allow to recieve text messages",
                            },
                        },
                        "lblCheckBox1": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblCheckBox2": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblCheckBox3": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblCheckBox4": {
                            "text": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            "accessibilityconfig": {
                                "a11yLabel": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                            },
                            "skin": ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN
                        },
                        "lblSelectedSeperator": "lblSelectedSeperator",
                        "lblSeperator": "lblSeperator",
                        "lblOptionSeperator": "lblOptionSeperator",
                        "template": "flxProfileManagementPhoneNumbers",
                        "phoneCountryCode": phoneModel.phoneCountryCode,
                        "Extension": phoneModel.Extension,
                        "phoneNumber": phoneModel.Value
                    };
                })

                this.view.settings.segPhoneNumbers.widgetDataMap = dataMap;
                if (kony.application.getCurrentBreakpoint() === 1024) {
                    for (var i = 0; i < data.length; i++) {
                        CommonUtilities.setText(data[i].btnViewDetail, "Edit", CommonUtilities.getaccessibilityConfig());
                    }
                };
                this.view.settings.segPhoneNumbers.setData(data);
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.view.forceLayout();
        },
        toggleContextualMenuPhoneNumbers: function() {
            var index = this.view.settings.segPhoneNumbers.selectedRowIndex[1];
            var scrollpos = this.view.contentOffsetMeasured;
            var hgt = ((index) * 51) + 130;
            if (this.view.settings.AllForms1.isVisible === false)
                this.view.settings.AllForms1.isVisible = true;
            else
                this.view.settings.AllForms1.isVisible = false;
            this.view.settings.AllForms1.top = hgt + "dp";
            leftInfo = this.view.settings.segPhoneNumbers.clonedTemplates[index].flxInfo.frame.x + this.view.settings.segPhoneNumbers.clonedTemplates[index].flxPrimary.frame.x - 130;
            this.view.settings.AllForms1.left = leftInfo + "dp";
            this.view.settings.AllForms1.right = "";
            this.view.settings.AllForms1.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.msgInfoPhone");
            this.view.forceLayout();
            this.view.setContentOffset(scrollpos);
        },
        /**
         * Method to assign details of the phone number
         * @param {Object} viewModel- JSON of the details of the phone number got from backend
         */
        /**
         * @function
         *
         * @param viewModel
         */
        constructPhoneNumber: function(phoneObj) {
            var phoneNumber = "";
            if (applicationManager.getConfigurationManager().getCountryCodeFlag() == true) {
                if (phoneObj.phoneCountryCode) {
                    phoneNumber = phoneNumber + phoneObj.phoneCountryCode + "-" + phoneObj.phoneNumber;
                } else {
                    phoneNumber = phoneNumber + phoneObj.phoneNumber;
                }
            } else {
                phoneNumber = phoneObj.phoneNumber;
            }
            if (phoneNumber === "undefined") {
                return "";
            } else {
                return phoneNumber;
            }
        },
        showPhoneDetails: function(viewModel) {
            this.view.settings.flxError.setVisibility(false);
            var phoneModel = viewModel.phone;
            var scopeObj = this;
            this.showDetailPhoneNumber();
            if (phoneModel.isTypeBusiness === "1") {
                this.view.settings.btnDelete.setVisibility(false);
            }
            this.view.settings.btnEdit.setVisibility(phoneModel.isPrimary !== "true" && applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE"));
            this.view.settings.flxEditPhoneNumberButtons.setVisibility(false);
            CommonUtilities.setText(this.view.settings.btnDelete, phoneModel.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.billPay.Edit") : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.btnDelete.toolTip = phoneModel.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.billPay.Edit") : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
            CommonUtilities.setText(this.view.settings.lblTypeValue, CommonUtilities.changedataCase(phoneModel.Extension), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblPhoneNumberValue, scopeObj.constructPhoneNumber(phoneModel), CommonUtilities.getaccessibilityConfig());
            this.view.settings.lblCheckBox3.skin = phoneModel.isPrimary === "true" ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            CommonUtilities.setText(this.view.settings.lblCheckBox3, phoneModel.isPrimary === "true" ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            this.view.settings.lblCheckBox4.skin = phoneModel.isAlertsRequired === "true" ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            CommonUtilities.setText(this.view.settings.lblCheckBox4, phoneModel.isAlertsRequired === "true" ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxOptions.setVisibility(phoneModel.isPrimary !== "true" || phoneModel.isAlertsRequired !== "true");
            this.view.settings.flxOption3.setVisibility(phoneModel.isPrimary !== "true");
            this.view.settings.flxWarning.setVisibility(phoneModel.isPrimary !== "true");
            this.view.settings.flxOption4.setVisibility(phoneModel.isAlertsRequired !== "true" && scopeObj.enableSeparateContact);
            this.view.settings.flxCheckBox3.onClick = null;
            CommonUtilities.setText(this.view.settings.lblExtensionUnEditable, phoneModel.Extension, CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxCheckBox4.onClick = null;
            this.view.settings.btnEditPhoneNumberSave.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserPhones;
            // this.setViewPhoneServicesData(viewModel.services, viewModel.phone);
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnDelete.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
            } else {
                this.view.settings.btnDelete.onClick = phoneModel.isPrimary === "true" ? scopeObj.editPhone.bind(scopeObj, phoneModel) : scopeObj.deletePhone.bind(scopeObj, phoneModel);
            }
            this.view.settings.btnEdit.onClick = scopeObj.editPhone.bind(scopeObj, phoneModel);
        },
        /**
         * Method to show the details at the phone number flex
         */
        showDetailPhoneNumber: function() {
            this.view.settings.flxEditPhoneNumberCountryCode.setVisibility(false);
            CommonUtilities.setText(this.view.settings.lblEditPhoneNumberHeading, kony.i18n.getLocalizedString("i18n.profilemanagement.phoneNumberDetail"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.flxOption2.setVisibility(false);
            this.showViews(["flxEditPhoneNumbersWrapper"]);
            if (applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")) {
                this.view.settings.btnDelete.isVisible = true;
                this.view.settings.btnEdit.isVisible = true;
            } else {
                this.view.settings.btnDelete.isVisible = false;
                this.view.settings.btnEdit.isVisible = false;
            }
            this.view.settings.flxEditPhoneNumberButtons.setVisibility(false);
            this.view.settings.lblExtensionUnEditable.isVisible = false;
            this.view.settings.lblCountryValue.isVisible = false;
            this.view.settings.lblTypeValue.isVisible = true;
            this.view.settings.lblCountry.setVisibility(false);
            this.view.settings.lblCountryValue.setVisibility(false);
            this.view.settings.lblPhoneNumberValue.isVisible = true;
            this.view.settings.flxRadioBtnUS.isVisible = false;
            this.view.settings.flxRadioBtnInternational.isVisible = false;
            this.view.settings.tbxPhoneNumber.isVisible = false;
            this.view.settings.lbxPhoneNumberType.isVisible = false;
            this.view.settings.lblRadioInternational.isVisible = false;
            this.view.settings.lblRadioUS.isVisible = false;
            this.view.settings.tbxAddExtensionEditPhoneNumber.isVisible = false;
            this.view.settings.flxWarning.isVisible = false;
            this.view.settings.btnEditPhoneNumberCancel.isVisible = false;
            // this.view.settings.lblEditPhoneNumberHeading.setFocus(true);
            this.view.settings.btnEditPhoneNumberSave.toolTip = kony.i18n.getLocalizedString('i18n.transfers.Cancel');
            CommonUtilities.setText(this.view.settings.btnEditPhoneNumberSave, kony.i18n.getLocalizedString('i18n.transfers.Cancel'), CommonUtilities.getaccessibilityConfig());
        },
        /**
         * Method to assign initial action on onclick of widgets
         * @param {Object} services - Array of accounts
         * @param {Object} phoneObj - JSON object of phone number
         */
        /*  setViewPhoneServicesData: function(services, phoneObj) {
              var scopeObj = this;
              function getServiceToggleListener(account, index) {
                  return function() {
                      var data = scopeObj.view.settings.segEditPhoneNumberOption1.data[index];
                      data.lblCheckBox.skin = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                      data.lblCheckBox.text = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED : ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                      scopeObj.view.settings.segEditPhoneNumberOption1.setDataAt(data, index);
                  }
              }
              var dataMap = {
                  "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
                  "flxCheckbox": "flxCheckbox",
                  "lblCheckBox": "lblCheckBox",
                  "lblAccounts": "lblAccounts"
              };
              var data = services.map(function(account, index) {
                  return {
                      "id": account.id,
                      "lblCheckBox": account.selected ? {"text":ViewConstants.FONT_ICONS.CHECBOX_SELECTED,"skin":ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN} : {"text":ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,"skin":ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN},
                      "lblAccounts": account.name,
                  }
              })
              this.view.settings.segEditPhoneNumberOption1.widgetDataMap = dataMap;
              this.view.settings.segEditPhoneNumberOption1.setData(data);
              this.view.forceLayout();
          },*/
        /**
         * Method to delete a particular phone number
         * @param {Object} phoneObj- JSON object of the phone to be deleted
         */
        deletePhone: function(phoneObj) {
            var scopeObj = this;
            var currForm = scopeObj.view;
            currForm.flxDeletePopUp.setVisibility(true);
            currForm.flxDeletePopUp.setFocus(true);
            CommonUtilities.setText(currForm.lblDeleteHeader, kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(currForm.lblConfirmDelete, kony.i18n.getLocalizedString("i18n.ProfileManagement.deletePhoneNum"), CommonUtilities.getaccessibilityConfig());
            currForm.forceLayout();
            currForm.btnDeleteYes.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.deletePhone(phoneObj);
                scopeObj.view.flxDelete.setFocus(true);
                scopeObj.view.flxDeletePopUp.setVisibility(false);
                scopeObj.view.forceLayout();
            }
        },
        /**
         * Method to edit a phone number
         * @param {Object} phoneObj - JSON object of the phone number to be edited
         */
        editPhone: function(phoneObj) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.editPhoneView(phoneObj);
        },
        /**
         * Method to update the list of address
         * @param {Object} addressListViewModel- list of addresses
         */
        updateAddressList: function(addressListViewModel) {
            this.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
            this.showAddresses();
            if (addressListViewModel.length >= 3 || !applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")) {
                this.view.settings.btnAddNewAddress.setVisibility(false);
            } else {
                this.view.settings.btnAddNewAddress.setVisibility(true);
                this.view.settings.btnAddNewPersonalAddress.setVisibility(true);
            }
            this.setAddressSegmentData(addressListViewModel);
            this.setSelectedSkin("flxAddress");
        },
        showAddresses: function() {
            this.showViews(["flxAddressesWrapper"]);
        },
        /**
         * Method to set all the data of the address module
         * @param {Object} userAddresses- List of all the addresses related to user
         */
        setAddressSegmentData: function(userAddresses) {
            totalAddress = userAddresses.length;
            var isCombinedUser = applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true";
            var scopeObj = this;
            var userPersonalAddresses = [];
            var userBusinessAddresses = [];
            this.view.settings.AllForms1.isVisible = false;
            if (isCombinedUser) {
                userAddresses.forEach(function(item) {
                    if (item.isTypeBusiness === "true")
                        userBusinessAddresses.push(item);
                    else
                        userPersonalAddresses.push(item);
                });
                this.view.settings.flxAddresses.setVisibility(false);
                this.view.settings.flxCombinedAddresses.setVisibility(true);
            } else {
                this.view.settings.flxAddresses.setVisibility(true);
                this.view.settings.flxCombinedAddresses.setVisibility(false);
            }

            function getDeleteAddressEmailListener(address) {
                return function() {
                    var currForm = scopeObj.view;
                    currForm.flxDeletePopUp.height = currForm.flxHeader.info.frame.height + currForm.flxContainer.info.frame.height + currForm.flxFooter.info.frame.height;
                    currForm.flxDeletePopUp.setVisibility(true);
                    currForm.flxDeletePopUp.setFocus(true);
                    currForm.lblDeleteHeader.setFocus(true);
                    CommonUtilities.setText(currForm.lblDeleteHeader, kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"), CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(currForm.lblConfirmDelete, kony.i18n.getLocalizedString("i18n.ProfileManagement.deleteAddress"), CommonUtilities.getaccessibilityConfig());
                    currForm.forceLayout();
                    currForm.btnDeleteYes.onClick = function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.deleteAddress(address);
                        scopeObj.view.flxDelete.setFocus(true);
                        scopeObj.view.flxDeletePopUp.setVisibility(false);
                        scopeObj.view.forceLayout();
                    };
                }
            }

            function getEditAddressListener(address) {
                return function() {
                    scopeObj.editAddress(address, userAddresses);
                };
            }
            var dataMap = {
                "btnEdit": "btnEdit",
                "btnDelete": "btnDelete",
                "flxAddress": "flxAddress",
                "flxAddressWrapper": "flxAddressWrapper",
                "flxCommunicationAddress": "flxCommunicationAddress",
                "flxRow": "flxRow",
                "flxInfo": "flxInfo",
                "lblSeperator": "lblSeperator",
                "imgCommunicationAddressInfo": "imgCommunicationAddressInfo",
                "lblAddessLine2": "lblAddessLine2",
                "lblAddressLine1": "lblAddressLine1",
                "lblAddressLine3": "lblAddressLine3",
                "lblAddressType": "lblAddressType",
                "lblSeperatorActions": "lblSeperatorActions",
                "lblCommunicationAddress": "lblCommunicationAddress"
            };
            var addressTypes = {
                "ADR_TYPE_WORK": 'Work',
                "ADR_TYPE_HOME": 'Home'
            }
            if (isCombinedUser) {
                var personalData = userPersonalAddresses.map(function(address) {
                    var state = "";
                    if (address.RegionName === undefined) {
                        state = "";
                    } else {
                        state = address.RegionName + ", ";
                    }
                    var city = (address && address.City_id) ? address.City_id : (address.CityName)?address.CityName:"";
                    var dataObject = {
                        "lblAddressType": {
                            "text": addressTypes[address.AddressType],
                            "accessibilityconfig": {
                                "a11yLabel": addressTypes[address.AddressType],
                            },
                        },
                        "lblAddressLine1": {
                            "text": address.AddressLine1,
                            "accessibilityconfig": {
                                "a11yLabel": address.AddressLine1,
                            },
                        },
                        "lblAddessLine2": {
                            "text": address.AddressLine2 ? address.AddressLine2 : (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")),
                            "accessibilityconfig": {
                                "a11yLabel": address.AddressLine2 ? address.AddressLine2 : (((city)?city + ', ':"")  + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")),
                            },
                        },
                        "lblAddressLine3": {
                            "text": address.AddressLine2 ? (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")) : "",
                            "accessibilityconfig": {
                                "a11yLabel": address.AddressLine2 ? (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")) : "",
                            },
                        },
                        "lblCommunicationAddress": {
                            "text": address.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.ProfileManagement.CommunicationAddress") : "",
                            "accessibilityconfig": {
                                "a11yLabel": address.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.ProfileManagement.CommunicationAddress") : "",
                            },
                        },
                        "lblSeperatorActions": {
                            "isVisible": address.isPrimary === "true" ? false : true,
                            "accessibilityconfig": {
                                "a11yLabel": address.isPrimary === "true" ? "" : "lblSeperatorActions",
                            },
                        },
                        "imgCommunicationAddressInfo": address.isPrimary === "true" ? ViewConstants.IMAGES.INFO_GREY : {
                            "isVisible": false
                        },
                        "btnEdit": {
                            text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            },
                            toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            onClick: getEditAddressListener(address),
                            isVisible: applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")
                        },
                        "btnDelete": {
                            text: address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            "accessibilityconfig": {
                                "a11yLabel": address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            },
                            toolTip: address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            onClick: CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : getDeleteAddressEmailListener(address),
                            isVisible: applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")
                        },
                        "flxInfo": {
                            onClick: scopeObj.toggleContextualMenuAddress.bind(scopeObj)
                        },
                        "lblSeperator": "lblSeperator",
                        "template": "flxRow"
                    }
                    if (CommonUtilities.isCSRMode()) {
                        dataObject.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                    }
                    return dataObject;
                })
                var businessData = userBusinessAddresses.map(function(address) {
                    var state = "";
                    if (address.RegionName === undefined) {
                        state = "";
                    } else {
                        state = address.RegionName + ", ";
                    }
                    var city = (address && address.City_id) ? address.City_id : (address.CityName)? address.CityName : "";
                    var dataObject = {
                        "lblAddressType": {
                            "text": addressTypes[address.AddressType],
                            "accessibilityconfig": {
                                "a11yLabel": addressTypes[address.AddressType],
                            },
                        },
                        "lblAddressLine1": {
                            "text": address.AddressLine1,
                            "accessibilityconfig": {
                                "a11yLabel": address.AddressLine1,
                            },
                        },
                        "lblAddessLine2": {
                            "text": address.AddressLine2 ? address.AddressLine2 : (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")),
                            "accessibilityconfig": {
                                "a11yLabel": address.AddressLine2 ? address.AddressLine2 : (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")),
                            },
                        },
                        "lblAddressLine3": {
                            "text": address.AddressLine2 ? (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")) : "",
                            "accessibilityconfig": {
                                "a11yLabel": address.AddressLine2 ? (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")) : "",
                            },
                        },
                        "lblCommunicationAddress": {
                            "text": address.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.ProfileManagement.CommunicationAddress") : "",
                            "accessibilityconfig": {
                                "a11yLabel": address.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.ProfileManagement.CommunicationAddress") : "",
                            },
                        },
                        "lblSeperatorActions": {
                            "isVisible": address.isPrimary === "true" ? false : true,
                            "accessibilityconfig": {
                                "a11yLabel": address.isPrimary === "true" ? "" : "lblSeperatorActions",
                            },
                        },
                        "imgCommunicationAddressInfo": address.isPrimary === "true" ? ViewConstants.IMAGES.INFO_GREY : {
                            "isVisible": false
                        },
                        "btnEdit": {
                            text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            },
                            toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            onClick: getEditAddressListener(address),
                            isVisible: applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")
                        },
                        "btnDelete": {
                            text: address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            "accessibilityconfig": {
                                "a11yLabel": address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            },
                            toolTip: address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            onClick: CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : getDeleteAddressEmailListener(address),
                            isVisible: applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")
                        },
                        "flxInfo": {
                            onClick: scopeObj.toggleContextualMenuAddress.bind(scopeObj)
                        },
                        "lblSeperator": "lblSeperator",
                        "template": "flxRow"
                    }
                    if (CommonUtilities.isCSRMode()) {
                        dataObject.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                    }
                    return dataObject;
                })
                if (personalData.length === 3)
                    this.view.settings.btnAddNewPersonalAddress.setVisibility(false);
                else
                    this.view.settings.btnAddNewPersonalAddress.setVisibility(true);
                if (personalData.length === 0)
                    this.view.settings.flxPersonalAddressBody.setVisibility(false);
                else
                    this.view.settings.flxPersonalAddressBody.setVisibility(true);
                this.view.settings.segPersonalAddresses.widgetDataMap = dataMap;
                this.view.settings.segBusinessAddresses.widgetDataMap = dataMap;
                this.view.settings.segPersonalAddresses.setData(personalData);
                this.view.settings.segBusinessAddresses.setData(businessData);
            } else {
                var data = userAddresses.map(function(address) {
                    var state = "";
                    if (address.RegionName === undefined) {
                        state = "";
                    } else {
                        state = address.RegionName + ", ";
                    }
                    var city = (address && address.City_id) ? address.City_id : (address.CityName)? address.CityName : "";
                    var dataObject = {
                        "lblAddressType": {
                            "text": addressTypes[address.AddressType],
                            "accessibilityconfig": {
                                "a11yLabel": addressTypes[address.AddressType],
                            },
                        },
                        "lblAddressLine1": {
                            "text": address.AddressLine1,
                            "accessibilityconfig": {
                                "a11yLabel": address.AddressLine1,
                            },
                        },
                        "lblAddessLine2": {
                            "text": address.AddressLine2 ? address.AddressLine2 : (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")),
                            "accessibilityconfig": {
                                "a11yLabel": address.AddressLine2 ? address.AddressLine2 : (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")),
                            },
                        },
                        "lblAddressLine3": {
                            "text": address.AddressLine2 ? (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")) : "",
                            "accessibilityconfig": {
                                "a11yLabel": address.AddressLine2 ? (((city)?city + ', ':"") + state + ((address.CountryCode)?address.CountryCode:"") + ((address.ZipCode)? ', ' + address.ZipCode : "")) : "",
                            },
                        },
                        "lblCommunicationAddress": {
                            "text": address.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.ProfileManagement.CommunicationAddress") : "",
                            "accessibilityconfig": {
                                "a11yLabel": address.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.ProfileManagement.CommunicationAddress") : "",
                            },
                        },
                        "lblSeperatorActions": {
                            "isVisible": address.isPrimary === "true" ? false : true,
                            "accessibilityconfig": {
                                "a11yLabel": address.isPrimary === "true" ? "" : "lblSeperatorActions",
                            },
                        },
                        "imgCommunicationAddressInfo": address.isPrimary === "true" ? ViewConstants.IMAGES.INFO_GREY : {
                            "isVisible": false
                        },
                        "btnEdit": {
                            text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            },
                            toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            onClick: getEditAddressListener(address),
                            isVisible: applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")
                        },
                        "btnDelete": {
                            text: address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            "accessibilityconfig": {
                                "a11yLabel": address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            },
                            toolTip: address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            onClick: CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : getDeleteAddressEmailListener(address),
                            isVisible: applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")
                        },
                        "flxInfo": {
                            onClick: scopeObj.toggleContextualMenuAddress.bind(scopeObj)
                        },
                        "lblSeperator": "lblSeperator",
                        "template": "flxRow"
                    }
                    if (CommonUtilities.isCSRMode()) {
                        dataObject.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                    }
                    return dataObject;
                })
                this.view.settings.segAddresses.widgetDataMap = dataMap;
                this.view.settings.segAddresses.setData(data);
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.view.forceLayout();
        },
        /**
         * Method to assign skin to the menu item selected
         * @param {String} name - menu item selected
         */
        setSelectedSkin: function(name) {
            var scope = this;
            if (this.currentSelectedSubMenu) {
                if (this.currentSelectedSubMenu === "flxAlertsSubMenu") {
                    if (this.view.settings.flxAlertCommunications.isVisible && this.view.settings.flxIndicatorAlertComm.isVisible) {
                        this.view.settings.flxAlertCommunications.skin = "sknFlxffffffBottomCursornoBottom";
                        this.view.settings.flxAlertCommunications.hoverSkin = "sknhoverf7f7f7";
                        this.view.settings.lblAlertComm.skin = "sknLblSSP72727213px";
                        this.view.settings.flxIndicatorAlertComm.setVisibility(false);
                    } else {
                        var data = this.view.settings.segAlertsCategory.data;
                        data[this.selectedCategoryIndex].flxIndicatorAlertsCategory.isVisible = false;
                        data[this.selectedCategoryIndex].lblAlertsCategory.skin = "sknLblSSP72727213px";
                        data[this.selectedCategoryIndex].flxAlertCategory.hoverSkin = "sknhoverf7f7f7";
                        data[this.selectedCategoryIndex].flxAlertCategory.skin = "sknFlxffffffBottomCursornoBottom";
                        this.view.settings.segAlertsCategory.setData(data);
                    }
                } else {
                    flxName = this.currentSelectedSubMenu;
                    var temp = this.view.settings[flxName].widgets();
                    if (temp.length >2) {
						temp[2].setVisibility(false);
					}
                    if (flxName === "flxProfile") {
                        scope.view.settings.ProfileInfo.setVisibility(false);
                    }
					if (temp.length >1) {
                    temp[1].skin = "sknLblSSP72727213px";
					}
                }
            }
            flxName = name;
            var flxChildren = this.view.settings[flxName].widgets();
            flxChildren[2].setVisibility(true);
            flxChildren[1].skin = "lblSSP42424213px";
            this.currentSelectedSubMenu = name;
        },
        /**
         * Method to validate the address entered
         */
        checkNewAddressForm: function() {
            var addAddressFormData = this.getNewAddressFormData();
            if (addAddressFormData.addressLine1 === '') {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else if (addAddressFormData.zipcode === '') {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else if (addAddressFormData.city === '') {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else if (addAddressFormData.countrySelected === "1") {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            }/* else if (addAddressFormData.stateSelected === "lbl1") {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            }*/ else if (addAddressFormData.citySelected === "") {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else {
                this.enableButton(this.view.settings.btnAddNewAddressAdd);
            }
        },
        /**
         * Method to hide all the flex of main body
         * @param {Object} addAddressViewModel - None
         */
        showAddNewAddressForm: function(addAddressViewModel) {
            var self = this;
            this.showAddNewAddress();
            if (addAddressViewModel.serverError) {
                this.view.settings.flxErrorAddNewEmail.setVisibility(true);
                CommonUtilities.setText(this.view.settings.CopylblError0e8bfa78d5ffc48, addAddressViewModel.serverError.errorMessage, CommonUtilities.getaccessibilityConfig());
            } else {
                this.view.settings.flxErrorAddNewEmail.setVisibility(false);
            }
            this.view.settings.lbxType.masterData = addAddressViewModel.addressTypes;
            this.view.settings.lbxType.selectedKey = addAddressViewModel.addressTypeSelected;
            this.view.settings.tbxAddressLine1.text = addAddressViewModel.addressLine1;
            this.view.settings.tbxAddressLine2.text = addAddressViewModel.addressLine2;
            this.view.settings.lbxCountry.masterData = addAddressViewModel.countryNew;
            this.view.settings.lbxCountry.selectedKey = addAddressViewModel.countrySelected;
            this.view.settings.lbxState.masterData = addAddressViewModel.stateNew;
            this.view.settings.lbxState.selectedKey = addAddressViewModel.stateSelected;
            this.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
            this.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
            var countryId = self.view.settings.lbxCountry.selectedKeyValue[0];
            if (countryId == "1") {
                self.view.settings.tbxCity.setEnabled(false);
                self.view.settings.lbxState.setEnabled(false);
            }
            this.view.settings.lbxCountry.onSelection = function() {
                var data = [];
                var countryId = self.view.settings.lbxCountry.selectedKeyValue[0];
                if (countryId == "1") {
                    self.checkNewAddressForm();
                    self.view.settings.lbxState.masterData = addAddressViewModel.stateNew;
                    self.view.settings.lbxState.selectedKey = addAddressViewModel.stateSelected;
                    self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                    self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                    self.view.settings.lbxCountry.selectedKey = countryId;
                    self.view.settings.tbxCity.setEnabled(false);
                    self.view.settings.lbxState.setEnabled(false);
                } else {
                    self.view.settings.tbxCity.setEnabled(true);
                    self.view.settings.lbxState.setEnabled(true);
                    self.view.settings.lbxCountry.selectedKey = countryId;
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("country", countryId, addAddressViewModel.stateNew);
                    self.view.settings.lbxState.masterData = data.states;
                    self.view.settings.lbxState.selectedKey = addAddressViewModel.stateSelected;
                    var stateId = self.view.settings.lbxState.selectedKeyValue[0];
                    if (stateId == "lbl1") {
                        self.checkNewAddressForm();
                        self.view.settings.lbxCountry.masterData = addAddressViewModel.countryNew;
                        self.view.settings.lbxCountry.selectedKey = countryId;
                        self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                        self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                        self.view.settings.tbxCity.setEnabled(false);
                    } else {
                        self.view.settings.lbxState.setEnabled(true);
                        self.view.settings.tbxCity.setEnabled(true);
                        data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", stateId, addAddressViewModel.cityNew);
                        if (data.length == 0) {
                            self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                            self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                        } else {
                            self.view.settings.tbxCity.masterData = data.cities;
                            self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                        }
                        self.checkNewAddressForm();
                    }
                }
            };
            this.view.settings.lbxState.onSelection = function() {
                var data = [];
                var stateId = self.view.settings.lbxState.selectedKeyValue[0];
                if (stateId == "lbl1") {
                    self.checkNewAddressForm();
                    self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                    self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                    self.view.settings.tbxCity.setEnabled(false);
                } else {
                    self.view.settings.lbxState.setEnabled(true);
                    self.view.settings.tbxCity.setEnabled(true);
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", stateId, addAddressViewModel.cityNew);
                    self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                    self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                    self.view.settings.tbxCity.masterData = data.cities;
                    self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                }
                self.checkNewAddressForm();
            };
            this.view.settings.tbxCity.onSelection = function() {
                var cityId = self.view.settings.tbxCity.selectedKeyValue[0];
                self.checkNewAddressForm();
            };
            self.view.settings.txtCity.text = addAddressViewModel.city;
            this.view.settings.tbxZipcode.text = addAddressViewModel.zipcode;
            this.view.settings.lblSetAsPreferredCheckBox.skin = addAddressViewModel.isPreferredAddress ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            if (totalAddress == 0) {
                this.view.settings.lblSetAsPreferredCheckBox.skin = addAddressViewModel.isPreferredAddress ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                CommonUtilities.setText(this.view.settings.lblSetAsPreferredCheckBox, addAddressViewModel.isPreferredAddress ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_SELECTED, CommonUtilities.getaccessibilityConfig());
            } else {
                CommonUtilities.setText(this.view.settings.lblSetAsPreferredCheckBox, addAddressViewModel.isPreferredAddress ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
            }
            this.checkNewAddressForm();
            this.view.settings.btnAddNewAddressAdd.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveAddress(this.getNewAddressFormData());
            }.bind(this);
        },
        showAddNewAddress: function() {
            this.showViews(["flxAddNewAddressWrapper"]);
        },
        /**
         * Method to show data related to New Address scenario
         * @param {Object} - which sets the data
         */
        getNewAddressFormData: function() {
          var addrLine1 = (this.view.settings.tbxAddressLine1.text)?this.view.settings.tbxAddressLine1.text.trim():"";
          var addrLine2 = (this.view.settings.tbxAddressLine2.text)?this.view.settings.tbxAddressLine2.text.trim():"";
          var countrySelected = this.view.settings.lbxCountry.selectedKey
          var zipcode = (this.view.settings.tbxZipcode.text)?this.view.settings.tbxZipcode.text.trim():"";
          var Addr_type = (this.view.settings.lbxType.selectedKey)?this.view.settings.lbxType.selectedKey:"";
          var stateSelected = (this.view.settings.lbxState.selectedKey!=='lbl1')?this.view.settings.lbxState.selectedKey:"";
          var citySelected = (this.view.settings.txtCity.text)?this.view.settings.txtCity.text.trim():"";
          return {
                addrLine1: addrLine1,
                addrLine2: addrLine2,
                countrySelected: countrySelected,
                zipcode: zipcode,
                isPreferredAddress: this.view.settings.lblSetAsPreferredCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                Addr_type: Addr_type,
                stateSelected: stateSelected,
                citySelected: citySelected,
            };
        },
        /**
         * Method to edit the address which is already set
         * @param {Object} address- All the fields of the Address
         */
        editAddress: function(address) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getEditAddressView(address);
        },
        /**
         * Method to Check whether the password is valid and matches with the re entered password
         */
        ValidatePassword: function() {
            if ((this.isPasswordValid(this.view.settings.tbxNewPassword.text)) && (this.isPasswordValid(this.view.settings.tbxConfirmPassword.text))) {
                if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
                    this.enableButton(this.view.settings.btnEditPasswordProceed);
                    this.view.settings.flxErrorEditPassword.setVisibility(false);
                } else {
                    this.disableButton(this.view.settings.btnEditPasswordProceed);
                    this.view.settings.flxErrorEditPassword.setVisibility(true);
                    CommonUtilities.setText(this.view.settings.lblError1, kony.i18n.getLocalizedString("i18n.idm.newPasswordMismatch"), CommonUtilities.getaccessibilityConfig());
                }
            } else {
                this.disableButton(this.view.settings.btnEditPasswordProceed);
                this.view.settings.flxErrorEditPassword.setVisibility(true);
                CommonUtilities.setText(this.view.settings.lblError1, kony.i18n.getLocalizedString("i18n.enrollNow.validPassword"), CommonUtilities.getaccessibilityConfig());
            }
            this.view.forceLayout();
        },
        /**
         * Method to Check whether the password is valid and matches with the re entered password
         */
        isPasswordValidAndMatchedWithReEnteredValue: function() {
            if (this.view.settings.tbxNewPassword.text && this.view.settings.tbxConfirmPassword.text) {
                if (this.view.settings.tbxNewPassword.text === this.view.settings.tbxConfirmPassword.text) {
                    return true;
                }
            }
            return false;
        },
        getPasswordRules: function(data) {
            FormControllerUtility.hideProgressBar(this.view);
            if (data) {
                this.view.settings.rtxRulesPassword.text = data.passwordpolicies.content; //
            } else {
                CommonUtilities.showServerDownScreen();
            }
        },
        showUsernameAndPassword: function() {
            var userPreferencesManager = applicationManager.getUserPreferencesManager();
            if (kony.mvc.MDAApplication.getSharedInstance().appContext.userName) {
                CommonUtilities.setText(this.view.settings.lblUsernameValue, kony.mvc.MDAApplication.getSharedInstance().appContext.userName, CommonUtilities.getaccessibilityConfig());
            } else {
                CommonUtilities.setText(this.view.settings.lblUsernameValue, userPreferencesManager.getCurrentUserName(), CommonUtilities.getaccessibilityConfig());
            }
            this.showViews(["flxUsernameAndPasswordWrapper"]);
            this.view.flxHeader.setFocus(true);
        },
        showEBankingAccess: function() {
            var self = this;
            var scopObj = this;
            this.showViews(["flxEBankingAccessWrapper"]);
            this.view.flxHeader.setFocus(true);
            scopObj.view.settings.lblRememberMeIcon.text = "D";
            scopObj.view.settings.lblRememberMeIcon.skin = OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            scopObj.disableButton(scopObj.view.settings.btnEBankingAccessSave);
            this.view.settings.flxCheckbox.onClick = this.toggleTnC.bind(this, self.view.settings.lblRememberMeIcon);
            this.view.settings.btnTermsAndConditionsEbankingaccess.onClick = self.showTermsAndConditionsForEBA.bind(self);
            scopObj.view.settings.btnEBankingAccessSave.onClick = this.showEbankingAccessPopup.bind(this);
        },
        toggleTnC: function(widget) {
            CommonUtilities.toggleFontCheckbox(this.view.settings.lblRememberMeIcon);
            if (widget.text === OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
                CommonUtilities.disableButton(this.view.settings.btnEBankingAccessSave);
                widget.skin = OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            } else {
                CommonUtilities.enableButton(this.view.settings.btnEBankingAccessSave);
                widget.skin = OLBConstants.SKINS.CHECKBOX_SELECTED_SKIN;
            }
        },
        showTermsAndConditionsForEBA: function() {
            FormControllerUtility.showProgressBar(this.view);
            var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
            profileModule.presentationController.getTncContent();
        },
        setTncContent: function(TncContent) {
            var scopeObj = this;
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.forceLayout();
            this.view.flxTermsAndConditionsHeader.setFocus(true);
            FormControllerUtility.setHtmlToBrowserWidget(this, this.view.brwBodyTnC, TncContent);
            FormControllerUtility.hideProgressBar(this.view);
            this.view.flxClose.onClick = function() {
                scopeObj.view.flxTermsAndConditions.setVisibility(false);
            };
        },
        showEbankingAccessPopup: function() {
            var scopeObj = this;
            this.view.flxDeletePopUp.setVisibility(true);
            this.view.lblDeleteHeader.skin = "sknSSPSemiBold42424215px";
            CommonUtilities.setText(this.view.lblDeleteHeader, kony.i18n.getLocalizedString("i18n.profile.eBankingDisable"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblConfirmDelete, kony.i18n.getLocalizedString("i18n.profile.eBankingAlertMsg"), CommonUtilities.getaccessibilityConfig());
            this.view.forceLayout();
            this.view.flxDeleteHeader.setFocus(true);
            this.view.btnDeleteNo.onClick = function() {
                scopeObj.view.flxDeletePopUp.setVisibility(false);
            };
            this.view.flxDeleteClose.onClick = function() {
                scopeObj.view.flxDeletePopUp.setVisibility(false);
            };
            this.view.btnDeleteYes.onClick = function() {
                FormControllerUtility.showProgressBar(this.view);
                var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                let presentationUtility = applicationManager.getPresentationUtility();
                if (presentationUtility.MFA && presentationUtility.MFA.isSCAEnabled && presentationUtility.MFA.isSCAEnabled()) {
                  let param = {
                    "serviceName":"SUSPEND_USER"
                  };
                  scopeObj.view.flxDeletePopUp.setVisibility(false);
                  scopeObj.view.authValidation.objSeviceName = "SCAObjects";
                  scopeObj.view.authValidation.objName = "SCAUser";
                  scopeObj.view.authValidation.operationName = "updateMyStatus";
				  scopeObj.view.authValidation.sendSCANotificationRequest(param, profileModule.presentationController.disableEBankingAccessSuccess, profileModule.presentationController.disableEBankingAccessError);
                }
                else
                  profileModule.presentationController.disableEBankingAccess();
            };
        },

        /**
         * Method to fetch 2 security question and show them
         */
        getAndShowAnswerSecurityQuestions: function() {
            FormControllerUtility.showProgressBar(this.view);
            this.view.settings.tbxAnswers1.text = "";
            this.view.settings.tbxAnswers2.text = "";
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAnswerSecurityQuestions();
        },
        /**
         * Method to assign action to send button to send OTP
         */
        assignOTPSendAction: function() {
            this.hideOTP();
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.requestOtp();
        },
        /**
         * Method to assign OnClick function of button Proceed in SecurityQuestions
         */
        onProceedSQ: function() {
            FormControllerUtility.showProgressBar(this.view);
            finaldata = this.onSaveSecurityQuestions();
            FormControllerUtility.hideProgressBar(this.view);
            //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveSecurityQuestions(data,this.saveSQCallback,this.saveSQfailureCallback);
        },
        hideOTP: function() {
            this.view.settings.tbxEnterOTP.secureTextEntry = true;
        },
        showSecuritySettingVerification: function() {
            this.showViews(["flxSecuritySettingVerificationWrapper"]);
        },
        /**
         * Method to show security questions which should get edit
         */
        showEditSecuritySettings: function() {
            var self = this;
            if (flag === 0) {
                this.showViews(["flxEditSecuritySettingsWrapper"]);
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchSecurityQuestions(this.selectedQuestionsTemp, this.staticSetQuestions.bind(this));
            } else {
                this.showViews(["flxEditSecuritySettingsWrapper"]);
                this.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(false);
                this.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(true);
                this.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(false);
                this.view.settings.flxSecurityQASet1.setVisibility(false);
                this.view.settings.flxSecurityQASet2.setVisibility(false);
                this.view.settings.flxSecurityQASet3.setVisibility(false);
                this.view.settings.flxSecurityQASet4.setVisibility(false);
                this.view.settings.flxSecurityQASet5.setVisibility(false);
                this.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
                CommonUtilities.setText(this.view.settings.btnEditSecuritySettingsProceed, kony.i18n.getLocalizedString("i18n.enrollNow.proceed"), CommonUtilities.getaccessibilityConfig());
                this.enableButton(self.view.settings.btnEditSecuritySettingsProceed);
            }
        },
        /**
         * Method to show all the security questions screen after fetching from backend
         * @param {Object} viewModel- None
         */
        showSecurityQuestions: function(viewModel) {
            FormControllerUtility.showProgressBar(this.view);
            var self = this;
            if (viewModel !== "Questions Exist") {
                flag = 0;
                this.showEditSecuritySettings();
                this.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
                this.AdjustScreen();
            } else {
                this.showViews(["flxEditSecuritySettingsWrapper"]);
                this.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(false);
                this.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(true);
                this.view.settings.flxSecurityQuestionSet.setVisibility(false);
                if (kony.application.getCurrentBreakpoint() == 1024) {
                    this.view.settings.flxEditSecuritySettingsButtons.top = "290dp";
                } else {
                    this.view.settings.flxEditSecuritySettingsButtons.top = "380dp";
                }
                this.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
                CommonUtilities.setText(this.view.settings.btnEditSecuritySettingsProceed, kony.i18n.getLocalizedString("i18n.enrollNow.proceed"), CommonUtilities.getaccessibilityConfig());
                this.view.settings.btnEditSecuritySettingsProceed.toolTip = kony.i18n.getLocalizedString("i18n.enrollNow.proceed");
                this.enableButton(this.view.settings.btnEditSecuritySettingsProceed);
            }
            this.view.settings.btnEditSecuritySettingsProceed.onClick = function() {
                self.view.settings.flxErrorEditSecuritySettings.setVisibility(false);
                flag = 0;
                if (self.view.settings.btnEditSecuritySettingsProceed.text === kony.i18n.getLocalizedString("i18n.enrollNow.proceed")) {
                    self.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(true);
                    self.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(false);
                    self.view.settings.flxSecurityQuestionSet.setVisibility(false);
                    self.view.settings.flxEditSecuritySettingsButtons.top = "0dp";
                    self.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(true);
                    self.view.settings.flxSecurityQASet1.setVisibility(true);
                    self.view.settings.flxSecurityQASet2.setVisibility(true);
                    self.view.settings.flxSecurityQASet3.setVisibility(true);
                    self.view.settings.flxSecurityQASet4.setVisibility(true);
                    self.view.settings.flxSecurityQASet5.setVisibility(true);
                    self.view.settings.btnEditSecuritySettingsCancel.setVisibility(true);
                    self.view.settings.btnEditSecuritySettingsProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
                    CommonUtilities.setText(self.view.settings.btnEditSecuritySettingsProceed, kony.i18n.getLocalizedString("i18n.common.proceed"), CommonUtilities.getaccessibilityConfig());
                    self.showEditSecuritySettings();
                    self.AdjustScreen();
                } else {
                    update = "security questions";
                    self.updateRequirements();
                }
            }
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show secure access code settings
         */
        showSecureAccessSettings: function() {
            var scopeObj = this;
            CommonUtilities.setText(scopeObj.view.settings.btnSecureAccessCodeModify, kony.i18n.getLocalizedString("i18n.billPay.Edit"), CommonUtilities.getaccessibilityConfig());
            scopeObj.view.settings.btnSecureAccessCodeModify.setVisibility(true);
            scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);
            scopeObj.view.settings.lblViewEmailSettings.onTouchEnd = function() {
                scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
                scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
                scopeObj.view.settings.imgSecuritySettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                scopeObj.view.settings.imgSecuritySettingsCollapse.toolTip = "Expand";
                scopeObj.view.settings.imgProfileSettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_UP;
                scopeObj.view.settings.imgProfileSettingsCollapse.toolTip = "Collapse";
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserEmail();
                scopeObj.view.forceLayout();
            }
            scopeObj.view.settings.lblViewPhoneSettings.onTouchEnd = function() {
                scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
                scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
                scopeObj.view.settings.imgSecuritySettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                scopeObj.view.settings.imgSecuritySettingsCollapse.toolTip = "Expand";
                scopeObj.view.settings.imgProfileSettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_UP;
                scopeObj.view.settings.imgProfileSettingsCollapse.toolTip = "Collapse";
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserPhones();
                scopeObj.view.forceLayout();
            }
            scopeObj.view.settings.btnSecureAccessCodeCancel.onClick = function() {
                CommonUtilities.setText(scopeObj.view.settings.btnSecureAccessCodeModify, kony.i18n.getLocalizedString("i18n.billPay.Edit"), CommonUtilities.getaccessibilityConfig());
                scopeObj.view.settings.btnSecureAccessCodeModify.setVisibility(true);
                scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);
                scopeObj.hideEditSecureAccessSettingsError();
                scopeObj.view.settings.lblViewEmailSettings.onTouchEnd = function() {
                    scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
                    scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
                    scopeObj.view.settings.imgSecuritySettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                    scopeObj.view.settings.imgSecuritySettingsCollapse.toolTip = "Expand";
                    scopeObj.view.settings.imgProfileSettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_UP;
                    scopeObj.view.settings.imgProfileSettingsCollapse.toolTip = "Collapse";
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserEmail();
                    scopeObj.view.forceLayout();
                }
                scopeObj.view.settings.lblViewPhoneSettings.onTouchEnd = function() {
                    scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
                    scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
                    scopeObj.view.settings.imgSecuritySettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                    scopeObj.view.settings.imgSecuritySettingsCollapse.toolTip = "Expand";
                    scopeObj.view.settings.imgProfileSettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_UP;
                    scopeObj.view.settings.imgProfileSettingsCollapse.toolTip = "Collapse";
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserPhones();
                    scopeObj.view.forceLayout();
                }
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecureAccess();
                scopeObj.view.forceLayout();
            }
            scopeObj.collapseAll();
            scopeObj.expandWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
            scope.view.settings.lblSecuritySettingsCollapse.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecureAccess();
            scopeObj.view.forceLayout();
            FormControllerUtility.hideProgressBar(scopeObj.view);
        },
        /**
         * Method to close all menu options without animation
         */
        collapseAll: function() {
            for (var i = 0; i < widgetsMap.length; i++) {
				var menuObject = widgetsMap[i];
				this.collapseWithoutAnimation(this.view.settings[menuObject.subMenu.parent]);
				 // this.view.settings[menuObject.image].src = ViewConstants.IMAGES.ARRAOW_DOWN;
				if(this.view.settings[menuObject.image].text !== "Q"){
				  this.view.settings[menuObject.image].text = "O";
				  this.view.settings[menuObject.image].toolTip = "Expand";
				}
            }
        },
        /**
         * Method to assign animation to menu while collapsing
         * @param {String} widget- ID of the widget
         */
        collapseWithoutAnimation: function(widget) {
            widget.height = 0;
            this.view.settings.forceLayout();
        },
        /**
         * Method to expand a menu item with animation
         * @param {String} widget- ID of the widget to be expanded
         */
        expand: function(widget) {
            var scope = this;

            function getVisibleChildrenLength(widget) {
                var children = widget.widgets();
                var visible = children.reduce(function(count, child) {
                    if (child.isVisible) {
                        count += 1
                    }
                    return count;
                }, 0)
                if (widget.id === "flxAlertsSubMenu") {
                    visible = visible + scope.view.settings.segAlertsCategory.data.length;
                }
                return visible;
            }
            var animationDefinition = {
                100: {
                    "height": getVisibleChildrenLength(widget) * 40
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var callbacks = {
                animationEnd: function() {
                    // scope.checkLogoutPosition();
                    scope.view.forceLayout();
                }
            };
            var animationDef = kony.ui.createAnimation(animationDefinition);
            widget.animate(animationDef, animationConfiguration, callbacks);
        },
        /**
         *onClickModifyAlerts- function that triggers on click of button Modify of  alerts
         * @param {Object} alertsViewModel - alerts payload
         * @param {Object} channelsPayLoad - Channels payload
         **/
        onClickModifyAlerts: function(alertCategoryId, channelsPayLoad) {
            FormControllerUtility.showProgressBar(this.view);
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.isVisible = false;
            this.showModifyScreen(alertCategoryId);
            if (this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text === "C") {
                this.setChannelsDataonModify(channelsPayLoad);
                this.changeCheckBoxOnModify(this.view.settings.transactionalAndPaymentsAlerts.segAlerts);
            }
            this.setSeperatorHeight();
            this.view.settings.transactionalAndPaymentsAlerts.flxAlertsStatusCheckbox.setFocus(true);
            this.view.settings.transactionalAndPaymentsAlerts.flxButtons.bottom = "3dp";
            this.view.settings.transactionalAndPaymentsAlerts.flxButtons.top = undefined;
            this.view.settings.transactionalAndPaymentsAlerts.flxAlertsCheckBoxChannels.left = "14px";

            this.callEnableOrDisableAlertsSave();
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         *onClickCancelAlerts- function that triggers on click of button Modify of  alerts
         *@param {Object} alertsViewModel - alerts payload
         */
        onClickCancelAlerts: function(alertsViewModel) {
            this.clearCategoryLevelValidation();
            var data = this.view.settings.segAlertsCategory.data;
            if (data[this.selectedCategoryIndex].isAccountLevel === true) {
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAccountAlerts(alertsViewModel.AlertCategoryId);
                return;
            }
            if (!this.isServiceCallNeeded) {
                this.view.settings.flxAlertButtons.setVisibility(false);
                this.view.settings.btnEditAlerts.setVisibility(true);
                this.view.settings.segAlertsListing.setEnabled(false);
                this.setAlertsDataToUI(alertsViewModel);
                this.view.settings.flxAlertsBody.forceLayout();
                this.AdjustScreen();
            } else {
                FormControllerUtility.showProgressBar(this.view);
                var params = {
                    "AlertCategoryId": alertsViewModel.AlertCategoryId
                };
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(params);
            }
        },
        /**
         *onClickSaveAlerts- function that triggers on click of button save of  alerts
         * @param {Object} alertsViewModel - alerts payload
         **/
        onClickSaveAlerts: function(alertsViewModel) {
            var scopeObj = this;
            this.onAlertSaveClick(scopeObj.view.settings, alertsViewModel);
            this.view.settings.flxAlertButtons.setVisibility(false);
            this.view.settings.btnEditAlerts.setVisibility(true);
            this.view.settings.segAlertsListing.setEnabled(false);
            this.setAlertsDataToUI(alertsViewModel);
            this.setSeperatorHeight();
            this.AdjustScreen();
        },
        /**
         * Method to set UI and initial stages
         * @param {Object} alertsViewModel - alerts payload
         **/
        showAlerts: function(alertsModel) {
            this.selectedCategoryIndex = null;
            this.view.customheader.customhamburger.activateMenu("Settings", "Alert Settings");
            this.collapseAll();
            var showAlertComm = alertsModel.alertConfiguration[0].enableSeparateContact === "1" ? true : false;
            this.enableSeparateContact = showAlertComm;
            this.view.settings.flxAlertCommunications.setVisibility(showAlertComm);
            this.view.settings.lblAlertsCollapse.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
            this.view.settings.lblAlertsCollapse.toolTip = "Collapse";
            this.view.settings.lblAlertsCollapse.accessibilityConfig = {
              "a11yLabel" : "Collapse"
            };
            var alertsViewModel = alertsModel.records;
            var filteredCategories = [];
            for (var i = 0; i < alertsViewModel.length; i++) {
                if ((alertsViewModel[i].Groups_count !== "0" || (alertsViewModel[i].Alerts_count && alertsViewModel[i].Alerts_count !== "0")) &&
                    alertsViewModel[i].alertcategory_status_id === "SID_ACTIVE")
                    filteredCategories.push(alertsViewModel[i]);
            }
            var self = this;
            var categoryWidgetMap = {
                "flxAlertCategory": "flxAlertCategory",
                "imgIndicatorAlertsCategory": "imgIndicatorAlertsCategory",
                "lblAlertsCategory": "lblAlertsCategory",
                "flxIndicatorAlertsCategory": "flxIndicatorAlertsCategory",
                "lblReference": "lblReference",
                "isAccountLevel": "isAccountLevel"
            };
            var categoryData = filteredCategories.map(function(category) {
                return {
                    "flxAlertCategory": {
                        "onClick": self.onCategoryMenuClick
                    },
                    "lblAlertsCategory": {
                        "text": category.alertcategorytext_DisplayName,
                        "toolTip": category.alertcategorytext_DisplayName
                    },
                    "lblReference": {
                        "text": category.alertcategory_id
                    },
                    "flxIndicatorAlertsCategory": {
                        "isVisible": false
                    },
                    "isAccountLevel": category.alertcategory_accountLevel === "true" ? true : false
                }
            });
            this.view.settings.segAlertsCategory.widgetMap = categoryWidgetMap;
            this.view.settings.segAlertsCategory.setData(categoryData);
            this.view.settings.flxAlertsSubMenu.height = this.view.settings.flxAlertCommunications.isVisible ? (categoryData.length + 1) * 40 : categoryData.length * 40;
            this.setFrequencyData();
            this.setAlertsFlowActions();
        },
        /**
         * Method to set UI after async call for fetching channels and alerts Data
         * @param {Object} alertsViewModel - alerts payload
         **/
        showAlertsDataByID: function(alertsViewModel) {
            this.disableButton(this.view.settings.btnAlertSave);
            this.view.customheader.customhamburger.activateMenu("Settings", "Alert Settings");
            //           CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts, kony.i18n.getLocalizedString("i18n.billPay.Edit") , CommonUtilities.getaccessibilityConfig());
            //           this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
            //           this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(true);
            //           this.setAlertsToUI(alertsViewModel);
            CommonUtilities.setText(this.view.settings.btnEditAlerts, kony.i18n.getLocalizedString("i18n.billPay.Edit"), CommonUtilities.getaccessibilityConfig());
            this.view.settings.btnEditAlerts.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
            this.view.settings.btnEditAlerts.setVisibility(true);
            this.view.settings.flxInvalidAmount.setVisibility(false);
            this.view.settings.flxNoAlertsFound.setVisibility(false);
            this.view.settings.flxAlertsSegment.setVisibility(true);
            this.view.settings.flxAlertButtons.setVisibility(false);
            this.setAlertsDataToUI(alertsViewModel);
            FormControllerUtility.hideProgressBar(this.view);
        },
        setAlertMessageByCategoryId: function(alertId) {
            if (alertId === "ALERT_CAT_SECURITY") {
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew, kony.i18n.getLocalizedString("i18n.alerts.recieveSecurityAlerts"), CommonUtilities.getaccessibilityConfig());
            } else if (alertId == "ALERT_CAT_TRANSACTIONAL") {
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew, kony.i18n.getLocalizedString("i18n.alerts.recieveTransactionAndPaymentAlerts"), CommonUtilities.getaccessibilityConfig());
            } else {
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew, kony.i18n.getLocalizedString("i18n.alerts.receiveAlerts"), CommonUtilities.getaccessibilityConfig());
            }
        },
        showModifyScreen: function(alertId) {
            var isEnabledScreen = this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.skin === ViewConstants.SKINS.DEACTIVATE_STATUS_SKIN ? false : true;
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.setVisibility(true);
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.imgAlertsWarningImageNew.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningImageNew.setVisibility(true);
            if (isEnabledScreen) {
                this.setAlertMessageByCategoryId(alertId);
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text = "C";
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus, kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled"), CommonUtilities.getaccessibilityConfig());
            } else {
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew, kony.i18n.getLocalizedString("i18n.alerts.alertsTimelyUpdates"), CommonUtilities.getaccessibilityConfig());
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text = "D";
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.toolTip = "Enable Alerts";
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus, kony.i18n.getLocalizedString("i18n.Alerts.ENABLEALERTS"), CommonUtilities.getaccessibilityConfig());
            }
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.flxButtons.setVisibility(true);
            //this.view.settings.transactionalAndPaymentsAlerts.flxClickBlocker.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.flxAlertsSegment.enable = false;
        },
        showDisabledScreen: function(isEnabledScreen, alertId) {
            //this.view.settings.transactionalAndPaymentsAlerts.flxClickBlocker.setVisibility(true);
            this.view.settings.transactionalAndPaymentsAlerts.flxAlertsSegment.enable = true;
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.setVisibility(true);
            this.view.settings.transactionalAndPaymentsAlerts.flxButtons.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(true);
            if (isEnabledScreen === false) {
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningImageNew.setVisibility(false);
                this.view.settings.transactionalAndPaymentsAlerts.imgAlertsWarningImageNew.setVisibility(true);
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew, kony.i18n.getLocalizedString("i18n.alerts.alertsDisabledModifyAlerts"), CommonUtilities.getaccessibilityConfig());
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.skin = ViewConstants.SKINS.DEACTIVATE_STATUS_SKIN;
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus, kony.i18n.getLocalizedString("i18n.Alerts.DisabledAlerts"), CommonUtilities.getaccessibilityConfig());
            } else {
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningImageNew.setVisibility(true);
                this.view.settings.transactionalAndPaymentsAlerts.imgAlertsWarningImageNew.setVisibility(false);
                this.setAlertMessageByCategoryId(alertId);
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus, kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled"), CommonUtilities.getaccessibilityConfig());
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.skin = ViewConstants.SKINS.ACTIVE_STATUS_SKIN;
            }
        },
        /**
         * Method to show error UI for invalid input
         *@param {boolean} isShow - to determine whether to show  error or not
         */
        showInvalidAlertsUI: function(isShow, alertWidget, alerts, index, errorMessage) {
            if (isShow) {
                this.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setVisibility(true);
                if (errorMessage)
                    CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblInvalidAmount, errorMessage, CommonUtilities.getaccessibilityConfig());
                this.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setFocus(true);
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.setEnabled(false);
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.skin = ViewConstants.SKINS.BUTTON_BLOCKED;
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.focusSkin = ViewConstants.SKINS.BUTTON_BLOCKED;
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.hoverSkin = ViewConstants.SKINS.BUTTON_BLOCKED;
            } else {
                if (index !== undefined) {
                    this.resetSegmentRedSkins(alertWidget, alerts, index);
                }
                this.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setVisibility(false);
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.setEnabled(true);
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.skin = ViewConstants.SKINS.NORMAL;
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.focusSkin = ViewConstants.SKINS.FOCUS;
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.hoverSkin = ViewConstants.SKINS.HOVER;
            }
            this.view.settings.transactionalAndPaymentsAlerts.forceLayout();
        },
        /**
         * Method to reset error skin for alerts segment
         */
        resetSegmentRedSkins: function(alertWidget, alerts, index) {
            var data = alertWidget.segAlerts.data[index];
            if (data != undefined && data.tbxMinLimit != undefined) {
                if (data.tbxMinLimit.skin !== ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX || data.tbxMaxLimit.skin !== ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX || data.tbxAmount.skin !== ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX) {
                    data.tbxMinLimit.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
                    data.tbxMaxLimit.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
                    data.tbxAmount.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
                    alertWidget.segAlerts.setDataAt(data, index);
                }
            }
        },
        /**
         * Method to set UI of alerts
         *@param {Object} alertsViewModel - alerts payload
         */
        setAlertsToUI: function(alertsViewModel) {
            var scopeObj = this;
            scopeObj.showViews(["flxTransactionalAndPaymentsAlertsWrapper"]);
            if (alertsViewModel.alertsData.length === 0) {
                scopeObj.showNoAlertsFoundUI();
            } else {
                this.view.settings.transactionalAndPaymentsAlerts.flxAlertsBody.setVisibility(true);
                this.view.settings.transactionalAndPaymentsAlerts.flxNoAlertsFound.setVisibility(false);
                var isEnabledScreen = (alertsViewModel.isAlertTypeSelected === true || alertsViewModel.isAlertTypeSelected === "true") ? true : false;
                var isModifyScreen = this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.isVisible === true ? false : true;
                this.showDisabledScreen(isEnabledScreen, alertsViewModel.AlertCategoryId);
                this.setChannelsDataToUI(alertsViewModel.channels, isModifyScreen);
                this.setAlertsSegmentData(this.view.settings.transactionalAndPaymentsAlerts, alertsViewModel.alertsData);
                scopeObj.showInvalidAlertsUI(false, this.view.settings.transactionalAndPaymentsAlerts, alertsViewModel);
                // this.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);
                // this.view.settings.transactionalAndPaymentsAlerts.flxAlertsStatusCheckbox.setFocus(true);
                this.isServiceCallNeeded = false;
                this.view.settings.transactionalAndPaymentsAlerts.flxChannel1.onClick = scopeObj.onToggleChannel1.bind(this);
                this.view.settings.transactionalAndPaymentsAlerts.flxChannel2.onClick = scopeObj.onToggleChannel2.bind(this);
                this.view.settings.transactionalAndPaymentsAlerts.flxChannel3.onClick = scopeObj.onToggleChannel3.bind(this);
                this.view.settings.transactionalAndPaymentsAlerts.flxChannel4.onClick = scopeObj.onToggleChannel4.bind(this);
                this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.onClick = scopeObj.onClickModifyAlerts.bind(this, alertsViewModel.AlertCategoryId, alertsViewModel.channels);
                this.view.settings.transactionalAndPaymentsAlerts.btnCancel.onClick = scopeObj.onClickCancelAlerts.bind(this, alertsViewModel);
                this.view.settings.transactionalAndPaymentsAlerts.flxAlertsStatusCheckbox.onClick = scopeObj.onToggleAlert.bind(this, alertsViewModel);
                if (CommonUtilities.isCSRMode()) {
                    this.view.settings.transactionalAndPaymentsAlerts.btnSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    this.view.settings.transactionalAndPaymentsAlerts.btnSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                    this.view.settings.transactionalAndPaymentsAlerts.btnSave.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    this.view.settings.transactionalAndPaymentsAlerts.btnSave.onClick = scopeObj.onClickSaveAlerts.bind(this, alertsViewModel);
                }
                var data = this.view.settings.segAlertsCategory.data;
                if (data[this.selectedCategoryIndex].isAccountLevel === true) {
                    this.onClickModifyAlerts(alertsViewModel.AlertCategoryId, alertsViewModel.channels);
                }
            }
            this.setSeperatorHeight();
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },
        showNoAlertsFoundUI: function() {
            this.view.settings.transactionalAndPaymentsAlerts.flxAlertsBody.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.flxNoAlertsFound.setVisibility(true);
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(false);
        },
        /**
         * Method to set channels UI of channels
         *@param {Object} channelsViewModel - channels payload
         *@param {Boolean} isModifyScreen - flag variable that determines whether the flow is in modify state or non-editable state
         */
        setChannelsDataToUI: function(channelsViewModel, isModifyScreen) {
            for (var i = 1, j = 0; i <= channelsViewModel.length; i++, j++) {
                this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].isVisible = channelsViewModel[j].isChannelSupported === "true" ? true : false;
                this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].isVisible = channelsViewModel[j].isChannelSupported === "true" ? true : false;
                this.view.settings.transactionalAndPaymentsAlerts["lblChannel" + i].isVisible = channelsViewModel[j].isChannelSupported === "true" ? true : false;
                this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].skin = channelsViewModel[j].isChannelSupported === "true" && isModifyScreen ? "flxHoverSkinPointer" : OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i], channelsViewModel[j].isSubscribed === "true" ? "C" : "D", CommonUtilities.getaccessibilityConfig());
                //this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].toolTip = kony.i18n.getLocalizedString("i18n.NUO.Select") + " " + channelsViewModel[j].channeltext_Description;
                this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].skin = channelsViewModel[j].isChannelSupported === "true" && isModifyScreen ? "skn0273e320pxolbfonticons" : OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts["lblChannel" + i], channelsViewModel[j].channeltext_Description, CommonUtilities.getaccessibilityConfig());
            }
        },
        /**
         * Method to set channels UI of channels
         *@param {Object} channelsPayLoad - channels payload
         *@param {Boolean} isModifyScreen - flag variable that determines whether the flow is in modify state or non-editable state
         */
        setChannelsDataonModify: function(channelsPayLoad) {
            var widget = this.view.settings.transactionalAndPaymentsAlerts;
            for (var i = 1, j = 0; widget["flxChannel" + i] !== undefined; i++, j++) {
                if (widget["flxChannel" + i].isVisible === true || widget["flxChannel" + i].isVisible === "true") {
                    this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].skin = channelsPayLoad[j].isChannelSupported === "true" ? "flxHoverSkinPointer" : OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                    this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].skin = channelsPayLoad[j].isChannelSupported === "true" ? "skn0273e320pxolbfonticons" : OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                }
                this.view.settings.transactionalAndPaymentsAlerts["lblChannel" + i].isVisible = channelsPayLoad[j].isChannelSupported === "true" ? true : false;
            }
        },
        /**
         * on Click action of enable/disable button
         * @param {Object} alertsViewModel- alerts Data
         */
        onToggleAlert: function(alertsViewModel) {
            var scopeObj = this;
            if (this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text === "C") {
                this.view.flxAlertsDisablePopUp.height = this.view.flxHeader.info.frame.height + this.view.flxContainer.info.frame.height + this.view.flxFooter.info.frame.height + ViewConstants.POSITIONAL_VALUES.DP;
                this.view.flxAlertsDisablePopUp.setVisibility(true);
                if (CommonUtilities.isCSRMode()) {
                    this.view.btnAlertsDisableYes.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    this.view.btnAlertsDisableYes.skin = CommonUtilities.disableButtonSkinForCSRMode();
                    this.view.btnAlertsDisableYes.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
                    this.view.btnAlertsDisableYes.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    this.view.btnAlertsDisableYes.onClick = scopeObj.onDisableAlerts.bind(this, alertsViewModel);
                }
            } else {
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text = "C";
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.toolTip = "Alerts Enabled";
                CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus, kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled"), CommonUtilities.getaccessibilityConfig());
                this.setChannelsDataonModify(alertsViewModel.channels);
                this.changeCheckBoxOnModify(this.view.settings.transactionalAndPaymentsAlerts.segAlerts);
                this.setAlertMessageByCategoryId(alertsViewModel.alertId);
                this.changeCheckBoxOnEnableAlerts(this.view.settings.transactionalAndPaymentsAlerts, alertsViewModel.isInitialLoad);
            }
            this.callEnableOrDisableAlertsSave();
        },
        /**
         * on Click action of Yes button of popup confirmation to disable alerts
         * @param {Object} alertsViewModel- alerts Data
         */
        onDisableAlerts: function(alertsViewModel) {
            this.view.flxAlertsDisablePopUp.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text = "D";
            var data = this.view.settings.transactionalAndPaymentsAlerts.segAlerts.data;
            for (var index = 0; index < data.length; index++) {
                data[index].lblEnabledIcon.text = "D";
            }
            this.view.settings.transactionalAndPaymentsAlerts.segAlerts.setData(data);
            alertsViewModel.isAlertTypeSelected = "false";
            this.setAlertsToUI(alertsViewModel);
            //this.setAlertsDataToUI(alertsViewModel);
            CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus, kony.i18n.getLocalizedString("i18n.Alerts.ENABLEALERTS"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew, kony.i18n.getLocalizedString("i18n.alerts.alertsTimelyUpdates"), CommonUtilities.getaccessibilityConfig());
            this.updateAlerts(this.view.settings.transactionalAndPaymentsAlerts, alertsViewModel);
            this.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);
        },
        updateAlerts: function(alertWidget, alertsViewModel) {
            alertWidget.flxAlertsHeader.setFocus(true);
            alertWidget.flxAlertsStatusCheckbox.setFocus(true);
            var alertsData = {
                isSubscribed: alertWidget.lblAlertsStatusCheckBox.text === "C" ? true : false,
                AlertCategoryId: alertsViewModel.AlertCategoryId,
            }
            var alertResponseData = {
                alertWidget: alertWidget,
                alertsViewModel: alertsViewModel
            };
            if (alertsViewModel.AlertCategoryId == "ALERT_CAT_ACCOUNTS") {
                if (alertsViewModel.accountId) {
                    alertsData.AccountId = alertsViewModel.accountId;
                } else if (alertsViewModel.accountTypeId) {
                    alertsData.AccountTypeId = alertsViewModel.accountTypeId;
                }
            }
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateAlerts(alertsData, alertResponseData);
        },
        /**
         * on Click action of channel flex (flxChannel1)
         */
        onToggleChannel1: function() {
            if (this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel1.skin === "skn0273e320pxolbfonticons") {
                this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel1.text = this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel1.text === "C" ? "D" : "C";
                this.callEnableOrDisableAlertsSave();
            } else {}
        },
        /**
         * on Click action of channel flex (flxChannel2)
         */
        onToggleChannel2: function() {
            if (this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel2.skin === "skn0273e320pxolbfonticons") {
                this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel2.text = this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel2.text === "C" ? "D" : "C";
                this.callEnableOrDisableAlertsSave();
            } else {}
        },
        /**
         * on Click action of channel flex (flxChannel3)
         */
        onToggleChannel3: function() {
            if (this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel3.skin === "skn0273e320pxolbfonticons") {
                this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel3.text = this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel3.text === "C" ? "D" : "C";
                this.callEnableOrDisableAlertsSave();
            } else {}
        },
        /**
         * on Click action of channel flex (flxChannel4)
         */
        onToggleChannel4: function() {
            if (this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel4.skin === "skn0273e320pxolbfonticons") {
                this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel4.text = this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel4.text === "C" ? "D" : "C";
                this.callEnableOrDisableAlertsSave();
            } else {}
        },
        /**
         * Method to show downtime error in alerts
         */
        showAlertsError: function(errorMessage) {
            this.view.flxDowntimeWarning.setVisibility(true);
            this.view.rtxDowntimeWarning.text = errorMessage;
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * UI function to manage alerts UI
         */
        setSeperatorHeight: function() {
            this.view.settings.flxSeperator3.height = this.view.settings.flxRight.info.frame.height + "px";
            this.view.forceLayout();
        },
        /**
         *alertInputType- function that returns input type for specific alert value
         * @param {Object} rowAlert - alert object
         */
        alertInputType: function(rowAlert) {
            if (rowAlert.alertCondition) {
                if (rowAlert.alertCondition.NoOfFields === "2") {
                    return ViewConstants.ALERTS_INPUT_TYPES.Range;
                } else if (rowAlert.alertCondition.NoOfFields === "1") {
                    if (rowAlert.alertAttribute && rowAlert.alertAttribute.values) {
                        return ViewConstants.ALERTS_INPUT_TYPES.List;
                    } else {
                        return ViewConstants.ALERTS_INPUT_TYPES.EqualTo;
                    }
                }
            }
            return ViewConstants.ALERTS_INPUT_TYPES.NoInputNeeded;
        },
        /**
         *setAlertsSegmentData- function that sets data to segment segAlerts
         * @param {Object} alerts - alerts array
         * @param {Object} alertWidget - widget
         */
        setAlertsSegmentData: function(alertWidget, alerts) {
            var scopeObj = this;
            var rowData;
            var dataMap = {
                "flxAccountAlertsCategoryDetails": "flxAccountAlertsCategoryDetails",
                "flxEnabledIcon": "flxEnabledIcon",
                "lblEnabledIcon": "lblEnabledIcon",
                "lblAlertDescription": "lblAlertDescription",
                "lblDesc": "lblDesc",
                "flxAccBalanceInBetween": "flxAccBalanceInBetween",
                "tbxMinLimit": "tbxMinLimit",
                "tbxMaxLimit": "tbxMaxLimit",
                "flxHorSeparator": "flxHorSeparator",
                "lblHorSeparator": "lblHorSeparator",
                "flxAccBalanceCreditedDebited": "flxAccBalanceCreditedDebited",
                "flxDescription": "flxDescription",
                "tbxAmount": "tbxAmount",
                "flxNotifyBalance": "flxNotifyBalance",
                "lstNotify": "lstNotify",
                "lblSeperator": "lblSeperator",
                "alerttype_id": "alerttype_id",
                "template": "template"
            };
            var list = [];
            alerts.forEach(function(rowAlert, index) {
                var type = scopeObj.alertInputType(rowAlert);
                var currencySymbol = applicationManager.getFormatUtilManager().getCurrencySymbol(rowAlert.alerttype_transactionCurrency);
                data = {};
                var alerttype_Value1;
                if (rowAlert.alerttype_Value1 == undefined)
                    alerttype_Value1 = "";
                data = {
                    "lblEnabledIcon": {
                        "text": rowAlert.isSubscribed === "true" ? "C" : "D",
                        "accessibilityconfig": {
                            "a11yLabel": rowAlert.isSubscribed === "true" ? "C" : "D",
                        },
                        "skin": OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN,
                        "onClick": function() {
                            if (scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text === "C") {
                                var rowData = alertWidget.segAlerts.data[index];
                                if (rowData.lblEnabledIcon.text === "C") {
                                    rowData.lblEnabledIcon.text = "D";
                                } else {
                                    rowData.lblEnabledIcon.text = "C";
                                }
                                alertWidget.segAlerts.setDataAt(rowData, index);
                                scopeObj.showOrHideAlertsInputField(alertWidget, rowData, type, index);
                                scopeObj.showInvalidAlertsUI(false, alertWidget, alerts, index);
                                scopeObj.callEnableOrDisableAlertsSave();
                            }
                        }
                    },
                    "flxEnabledIcon": {
                        "skin": OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN
                    },
                    "lblAlertDescription": {
                        "text": rowAlert.alerttypetext_DisplayName,
                        "accessibilityconfig": {
                            "a11yLabel": rowAlert.alerttypetext_DisplayName,
                        },
                    },
                    "lblDesc": {
                        "text": rowAlert.alerttypetext_Description,
                        "accessibilityconfig": {
                            "a11yLabel": rowAlert.alerttypetext_Description,
                        },
                    },
                    "flxDescription": {
                        isVisible: rowAlert.isSubscribed === "true" ? true : false
                    },
                    "flxAccBalanceInBetween": {
                        isVisible: type === ViewConstants.ALERTS_INPUT_TYPES.Range && rowAlert.isSubscribed === "true" ? true : false
                    },
                    "tbxMinLimit": {
                        "text": (type === ViewConstants.ALERTS_INPUT_TYPES.Range && rowAlert.preference && rowAlert.preference.Value1) ? rowAlert.preference.Value1 : "",
                        "skin": ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX,
                        "onTextChange": scopeObj.showInvalidAlertsUI.bind(scopeObj, false, alertWidget, alerts, index)
                    },
                    "tbxMaxLimit": {
                        "text": (type === ViewConstants.ALERTS_INPUT_TYPES.Range && rowAlert.preference && rowAlert.preference.Value2) ? rowAlert.preference.Value2 : "",
                        "skin": ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX,
                        "onTextChange": scopeObj.showInvalidAlertsUI.bind(scopeObj, false, alertWidget, alerts, index)
                    },
                    "flxHorSeparator": {
                        isVisible: true
                    },
                    "lblHorSeparator": {
                        "text": " ",
                        "accessibilityconfig": {
                            "a11yLabel": " "
                        },
                    },
                    "flxAccBalanceCreditedDebited": {
                        isVisible: type === ViewConstants.ALERTS_INPUT_TYPES.EqualTo && rowAlert.isSubscribed === "true" ? true : false,
                    },
                    "tbxAmount": {
                        "text": (type === ViewConstants.ALERTS_INPUT_TYPES.EqualTo && rowAlert.alerttype_Value1 && rowAlert.alertAttribute.alertattribute_type === "AMOUNT") ? CommonUtilities.formatCurrencyWithCommas(rowAlert.alerttype_Value1, false, currencySymbol) : alerttype_Value1,
                        "skin": ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX,
                        "validAmount": CommonUtilities.formatCurrencyWithCommas(rowAlert.alerttype_Value1, false, currencySymbol),
                        "validDay": rowAlert.alerttype_Value1,
                        "onTextChange": function() {
                            var rowData = alertWidget.segAlerts.data[index];
                            if (rowAlert.alertAttribute.alertattribute_type === "AMOUNT") {
                                rowData.tbxAmount.text = CommonUtilities.formatCurrencyWithCommas(rowData.tbxAmount.text, false, currencySymbol);
                                alertWidget.segAlerts.setDataAt(rowData, index);
                                var amount = Number(CommonUtilities.deFormatAmount(rowData.tbxAmount.text));
                                if (isNaN(amount) || amount <= 0) {
                                    var errorMessage = kony.i18n.getLocalizedString("i18n.login.EnrollAlert");
                                    scopeObj.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setVisibility(true);
                                    CommonUtilities.setText(scopeObj.view.settings.transactionalAndPaymentsAlerts.lblInvalidAmount, errorMessage, CommonUtilities.getaccessibilityConfig());
                                    scopeObj.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setFocus(true);
                                    scopeObj.showInvalidAlertsUI(true, alertWidget, alerts, index, errorMessage);
                                    rowData.tbxAmount.text = rowData.tbxAmount.validAmount;
                                    alertWidget.segAlerts.setDataAt(rowData, index);
                                    rowData.tbxAmount.validAmount = CommonUtilities.formatCurrencyWithCommas(rowData.tbxAmount.text, false, currencySymbol);
                                    scopeObj.callEnableOrDisableAlertsSave();
                                } else {
                                    scopeObj.showInvalidAlertsUI(false, alertWidget, alerts, index);
                                    rowData.tbxAmount.validAmount = CommonUtilities.formatCurrencyWithCommas(rowData.tbxAmount.text, false, currencySymbol);
                                    scopeObj.callEnableOrDisableAlertsSave();
                                }
                            } else {
                                var days = Number(rowData.tbxAmount.text);
                                if (isNaN(days) || days <= 0) {
                                    var errorMessage = kony.i18n.getLocalizedString("i18n.alerts.errormessages.invalidDays");
                                    scopeObj.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setVisibility(true);
                                    CommonUtilities.setText(scopeObj.view.settings.transactionalAndPaymentsAlerts.lblInvalidAmount, errorMessage, CommonUtilities.getaccessibilityConfig());
                                    scopeObj.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setFocus(true);
                                    scopeObj.showInvalidAlertsUI(true, alertWidget, alerts, index, errorMessage);
                                    rowData.tbxAmount.text = rowData.tbxAmount.validDay;
                                    alertWidget.segAlerts.setDataAt(rowData, index);
                                    rowData.tbxAmount.validDay = rowData.tbxAmount.text;
                                    scopeObj.callEnableOrDisableAlertsSave();
                                } else {
                                    scopeObj.showInvalidAlertsUI(false, alertWidget, alerts, index);
                                    rowData.tbxAmount.validDay = rowData.tbxAmount.text;
                                    scopeObj.callEnableOrDisableAlertsSave();
                                }
                            }
                        }
                    },
                    "flxNotifyBalance": {
                        isVisible: type === ViewConstants.ALERTS_INPUT_TYPES.List && rowAlert.isSubscribed === "true" ? true : false,
                    },
                    //Needs Refactor
                    "lstNotify": {
                        selectedKey: (type === ViewConstants.ALERTS_INPUT_TYPES.List && rowAlert.preference) ? rowAlert.preference.Value1 : type === ViewConstants.ALERTS_INPUT_TYPES.List ? scopeObj.returnMasterDataFromAlerts(rowAlert.alertAttribute.values)[0][0] : null,
                        masterData: type === ViewConstants.ALERTS_INPUT_TYPES.List ? scopeObj.returnMasterDataFromAlerts(rowAlert.alertAttribute.values) : null,
                    },
                    "lblSeperator": {
                        "text": "",
                        "accessibilityconfig": {
                            "a11yLabel": ""
                        },
                    },
                    "alerttype_id": rowAlert.alerttype_id,
                    "template": "flxAccountAlertsCategoryDetails"
                };
                if (kony.application.getCurrentBreakpoint() === 1024) {
                    for (var j = 0; j < data.length; j++) {
                        data[j].template = "flxAccountAlertsCategoryDetails";
                    }
                }
                list.push(data);
            });
            alertWidget.segAlerts.widgetDataMap = dataMap;
            alertWidget.segAlerts.setData(list);
            scopeObj.view.settings.transactionalAndPaymentsAlerts.flxAlertsSegment.setVisibility(true);
            scopeObj.view.settings.transactionalAndPaymentsAlerts.flxButtons.bottom = "3dp";
            this.view.forceLayout();
        },
        showOrHideAlertsInputField: function(alertWidget, rowData, type, index) {
            var data = rowData;
            rowData.flxAccBalanceInBetween.isVisible = (type === ViewConstants.ALERTS_INPUT_TYPES.Range && rowData.lblEnabledIcon.text === "C") ? true : false;
            rowData.flxAccBalanceCreditedDebited.isVisible = (type === ViewConstants.ALERTS_INPUT_TYPES.EqualTo && rowData.lblEnabledIcon.text === "C") ? true : false;
            rowData.flxDescription.isVisible = (rowData.lblEnabledIcon.text === "C") ? true : false;
            rowData.flxNotifyBalance.isVisible = (type === ViewConstants.ALERTS_INPUT_TYPES.List && rowData.lblEnabledIcon.text === "C") ? true : false;
            alertWidget.segAlerts.setDataAt(data, index);
        },
        returnMasterDataFromAlerts: function(values) {
            var data = [];
            values.forEach(function(value) {
                var list = [];
                list.push(value.alertattributelistvalues_id);
                list.push(value.alertattributelistvalues_name);
                data.push(list);
            });
            return data;
        },
        /**
         *changeCheckBoxOnModify- function that will change skin of all the checkboxes to selected skin
         * @param {Object} alertWidget - alerts segment widget Id
         */
        changeCheckBoxOnModify: function(alertWidget) {
            var data = alertWidget.data;
            for (var index = 0; index < data.length; index++) {
                data[index].lblEnabledIcon.skin = "skn0273e320pxolbfonticons";
                data[index].flxEnabledIcon.skin = "flxHoverSkinPointer";
            }
            alertWidget.setData(data);
        },
        /**
         *changeCheckBoxOnEnableAlerts - function that will change state of all the alerts based on enabling and disabling of parent level alert
         * @param {Object} alertWidget - alerts segment widget Id
         * @param {Boolean} isEnabled - Flag to identify if parent level alert is enabled or disabled
         * @param {Boolean} isInitialLoad - Flag to identify if alert will be selected for first time
         */
        changeCheckBoxOnEnableAlerts: function(alertWidget, isInitialLoad) {
            if (isInitialLoad === "true" || isInitialLoad === true) {
                this.enableAllCheckboxes(alertWidget.segAlerts);
            } else {
                this.changeCheckBoxOnModify(alertWidget.segAlerts);
            }
        },
        /**
         *enableAllCheckboxes- function that enable all alerts
         * @param {Object} alertWidget - alerts segment widget Id
         */
        enableAllCheckboxes: function(alertWidget) {
            var data = alertWidget.data;
            for (var index = 0; index < data.length; index++) {
                data[index].lblEnabledIcon.onClick();
            }
        },
        checkIfAtleastOneChannelSelected: function(alertWidget) {
            for (var i = 1; alertWidget["flxChannel" + i] !== undefined; i++) {
                if (alertWidget["lblCheckBoxChannel" + i].text === "C") {
                    return true;
                }
            }
            return false;
        },
        checkIfAtleastOneAlertSelected: function(alertWidget) {
            var data = alertWidget.segAlerts.data;
            for (var index = 0; index < data.length; index++) {
                if (data[index].lblEnabledIcon.text === "C") {
                    return true;
                }
            }
            return false;
        },
        enableOrDisableSaveOption: function(enable) {
            if (enable && !CommonUtilities.isCSRMode()) {
                FormControllerUtility.enableButton(this.view.settings.transactionalAndPaymentsAlerts.btnSave);
            } else {
                FormControllerUtility.disableButton(this.view.settings.transactionalAndPaymentsAlerts.btnSave);
            }
        },
        callEnableOrDisableAlertsSave: function() {
            var alertWidget = this.view.settings.transactionalAndPaymentsAlerts;
            this.enableOrDisableSaveOption(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text === "C" &&
                this.checkIfAtleastOneChannelSelected(alertWidget) &&
                this.checkIfAtleastOneAlertSelected(alertWidget));
        },
        /**
         *fetchAlertsSegmentData- function to fetch all alerts and their status to form payload for set service
         * @param {Object} alertWidget - alerts segment widget Id
         */
        fetchAlertsSegmentData: function(alertWidget, alertsData) {
            var scopeObj = this;
            var data = alertWidget.data;
            typePreference = [];
            data.forEach(function(rowAlert, i) {
                var type = scopeObj.alertInputType(alertsData[i]);
                var list = {};
                list.typeId = rowAlert.alerttype_id;
                list.isSubscribed = rowAlert.lblEnabledIcon.text === "C" ? "true" : "false";
                if (type === ViewConstants.ALERTS_INPUT_TYPES.Range) {
                    list.value1 = CommonUtilities.deFormatAmount(rowAlert.tbxMinLimit.text);
                    list.value2 = CommonUtilities.deFormatAmount(rowAlert.tbxMaxLimit.text);
                } else if (type === ViewConstants.ALERTS_INPUT_TYPES.EqualTo) {
                    list.value1 = CommonUtilities.deFormatAmount(rowAlert.tbxAmount.text);
                } else if (type === ViewConstants.ALERTS_INPUT_TYPES.List) {
                    list.value1 = rowAlert.lstNotify.selectedKey;
                }
                typePreference.push(list);
            });
            return typePreference;
        },
        /**
         * validateAlertsSegmentData - function to validate inputs for alerts
         */
        validateAlertsSegmentData: function(alertWidget, alertsData) {
            var scopeObj = this;
            var data = alertWidget.data;
            var isValid = true;
            data.forEach(function(rowAlert, i) {
                var type = scopeObj.alertInputType(alertsData[i]);
                if (type === ViewConstants.ALERTS_INPUT_TYPES.Range) {
                    if (rowAlert.lblEnabledIcon.text === "C" && (rowAlert.tbxMinLimit.text.trim().length === 0 || rowAlert.tbxMaxLimit.text.trim().length === 0)) {
                        rowAlert.tbxMinLimit.skin = ViewConstants.SKINS.BORDER;
                        rowAlert.tbxMaxLimit.skin = ViewConstants.SKINS.BORDER;
                        alertWidget.setDataAt(rowAlert, i);
                        isValid = false;
                    }
                } else if (type === ViewConstants.ALERTS_INPUT_TYPES.EqualTo) {
                    if (rowAlert.lblEnabledIcon.text === "C" && rowAlert.tbxAmount.text.trim().length === 0) {
                        rowAlert.tbxAmount.skin = ViewConstants.SKINS.BORDER;
                        alertWidget.setDataAt(rowAlert, i);
                        isValid = false;
                    }
                }
            });
            return isValid;
        },
        /**
         *getAlertsFromForm- function that fetches data of alerts changed
         * @param {Object} widget - widget
         */
        getAlertsFromForm: function(widget, channelData) {
            var channelPreferences = [];
            for (var i = 1, j = 0; widget["flxChannel" + i] !== undefined; i++, j++) {
                if (widget["flxChannel" + i] !== undefined && (widget["flxChannel" + i].isVisible === true || this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].isVisible === "true")) {
                    var temp = {};
                    temp.isSubscribed = widget["lblCheckBoxChannel" + i].text === "C" ? true : false;
                    temp.channelId = channelData[j].channel_id;
                    channelData[j].isSubscribed = widget["lblCheckBoxChannel" + i].text === "C" ? "true" : "false";
                    channelPreferences.push(temp);
                }
            }
            return channelPreferences;
        },
        /**
         *saveAlerts- function that saves/updates alerts changed
         * @param {Object} alerts - alerts array
         */
        saveAlerts: function(alertWidget, alertsViewModel) {
            var alertsChannelData = this.getAlertsFromForm(alertWidget, alertsViewModel.channels);
            var isvalidAlertTypePreference = this.validateAlertsSegmentData(alertWidget.segAlerts, alertsViewModel.alertsData);
            if (isvalidAlertTypePreference) {
                var alertTypePreference = this.fetchAlertsSegmentData(alertWidget.segAlerts, alertsViewModel.alertsData);
                var alertsData = {
                    channelPreference: alertsChannelData,
                    typePreference: alertTypePreference,
                    isSubscribed: alertWidget.lblAlertsStatusCheckBox.text === "C" ? true : false,
                    AlertCategoryId: alertsViewModel.AlertCategoryId,
                }
                var alertResponseData = {
                    alertWidget: alertWidget,
                    alertsViewModel: alertsViewModel
                };
                if (alertsViewModel.AlertCategoryId == "ALERT_CAT_ACCOUNTS") {
                    if (alertsViewModel.accountId) {
                        alertsData.AccountId = alertsViewModel.accountId;
                    } else if (alertsViewModel.accountTypeId) {
                        alertsData.AccountTypeId = alertsViewModel.accountTypeId;
                    }
                }
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateAlerts(alertsData, alertResponseData);
            } else {
                this.showInvalidAlertsUI(true, alertWidget, alertsViewModel);
            }
            //this.view.forceLayout();
        },
        saveAlertSuccess: function(alertsViewModel) {
            FormControllerUtility.showProgressBar(this.view);
            var data = this.view.settings.segAlertsCategory.data;
            if (data[this.selectedCategoryIndex].isAccountLevel === true) {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAccountAlerts(alertsViewModel.AlertCategoryId);
                return;
            }
            var params = {
                "AlertCategoryId": alertsViewModel.AlertCategoryId
            };
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(params);
        },
        disableAllCheckboxes: function(alertWidget, channelsViewModel) {
            var data = alertWidget.data;
            for (var index = 0; index < data.length; index++) {
                data[index].lblEnabledIcon.skin = OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                data[index].flxEnabledIcon.skin = OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            }
            alertWidget.setData(data);
            for (var i = 1, j = 0; i <= channelsViewModel.length; i++, j++) {
                this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].skin = OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].skin = OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            }
        },
        /**
         *hideAllWidgetsAlerts- function to hide widgets
         * @param {String} alertWidget - ID of the widget
         */
        hideAllWidgetsAlerts: function(alertWidget) {
            alertWidget.btnModifyAlerts.setVisibility(false);
            alertWidget.flxWhiteSpace.setVisibility(false);
            alertWidget.flxClickBlocker.setVisibility(false);
            alertWidget.flxAlertsStatusCheckbox.setVisibility(false);
            alertWidget.flxAlertsRow1.setVisibility(false);
            alertWidget.flxAlertsRow2.setVisibility(false);
            alertWidget.flxAlertsSegment.setVisibility(false);
            alertWidget.flxButtons.setVisibility(false);
            alertWidget.btnEnableAlerts.setVisibility(false);
            alertWidget.btnCancel.setVisibility(false);
            alertWidget.btnSave.setVisibility(false);
        },
        /**
         * Method to update edit phone number UI based on the responce form backend
         * @param {Object} addPhoneViewModel - responce from backend after fetching phone number
         */
        updateEditPhoneViewModel: function(addPhoneViewModel) {
            var scopeObj = this;
            if (addPhoneViewModel.serverError) {
                this.view.settings.flxError.setVisibility(true);
                CommonUtilities.setText(this.view.settings.lblError, addPhoneViewModel.serverError.errorMessage, CommonUtilities.getaccessibilityConfig());
            } else {
                this.view.settings.flxErrorAddPhoneNumber.setVisibility(false);
                this.showEditPhonenumber();
                if (addPhoneViewModel.isTypeBusiness === "1") {
                    this.view.settings.lblTypeValue.setVisibility(true);
                    CommonUtilities.setText(this.view.settings.lblTypeValue, CommonUtilities.changedataCase(addPhoneViewModel.Extension), CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(this.view.settings.lblPhoneNumberValue, scopeObj.constructPhoneNumber(addPhoneViewModel), CommonUtilities.getaccessibilityConfig());
                    this.view.settings.lblPhoneNumberValue.setVisibility(true);
                    this.view.settings.lbxPhoneNumberType.setVisibility(false);
                    this.view.settings.flxEditPhoneNumberCountryCode.setVisibility(false);
                }
                this.view.settings.flxError.setVisibility(false);
                this.view.settings.lbxPhoneNumberType.masterData = addPhoneViewModel.phoneTypes;
                this.view.settings.lbxPhoneNumberType.selectedKey = addPhoneViewModel.phoneTypeSelected;
                this.view.settings.tbxPhoneNumber.text = addPhoneViewModel.phoneNumber;
                this.view.settings.tbxEditPhoneNumberCountryCode.text = addPhoneViewModel.phoneCountryCode || "";
                this.view.settings.tbxAddExtensionEditPhoneNumber.text = addPhoneViewModel.ext || "";
                this.view.settings.flxOptions.setVisibility(!addPhoneViewModel.isPrimary || !addPhoneViewModel.isAlertsRequired);
                this.view.settings.flxOption3.setVisibility(!addPhoneViewModel.isPrimary);
                this.view.settings.flxWarning.setVisibility(!addPhoneViewModel.isPrimary);
                this.view.settings.flxOption4.setVisibility((!addPhoneViewModel.isAlertsRequired) && scopeObj.enableSeparateContact);
                this.view.settings.flxEditPhoneNumberButtons.setVisibility(true);
              	this.view.settings.lblPleaseChoose.setVisibility(addPhoneViewModel.isPrimary !== "true" || ((!addPhoneViewModel.isAlertsRequired !== "true") && scopeObj.enableSeparateContact));
                this.view.settings.flxOptionSeperator.setVisibility(addPhoneViewModel.isPrimary !== "true" || ((!addPhoneViewModel.isAlertsRequired !== "true") && scopeObj.enableSeparateContact));
                this.view.settings.lblCheckBox3.skin = addPhoneViewModel.isPrimary ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                CommonUtilities.setText(this.view.settings.lblCheckBox3, addPhoneViewModel.isPrimary ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                this.view.settings.lblCheckBox4.skin = addPhoneViewModel.isAlertsRequired ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                CommonUtilities.setText(this.view.settings.lblCheckBox4, addPhoneViewModel.isAlertsRequired ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                this.view.settings.flxCheckBox3.onClick = function() {
                    this.toggleFontCheckBox(this.view.settings.lblCheckBox3);
                }.bind(this);
                this.view.settings.flxCheckBox4.onClick = function() {
                    this.toggleFontCheckBox(this.view.settings.lblCheckBox4);
                }.bind(this);
                this.view.settings.btnEditPhoneNumberCancel.onClick = function() {
                    scopeObj.showViews(["flxPhoneNumbersWrapper"]);
                    var isCombinedUser = applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true";
                    if (isCombinedUser) {
                        scopeObj.view.settings.flxCombinedPhoneNumbers.setVisibility(true);
                        scopeObj.view.settings.flxPhoneNumbers.setVisibility(false);
                    } else {
                        scopeObj.view.settings.flxCombinedPhoneNumbers.setVisibility(false);
                        scopeObj.view.settings.flxPhoneNumbers.setVisibility(true);
                    }
                }
                this.view.settings.lblCheckBox4.skin = addPhoneViewModel.isAlertsRequired ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                CommonUtilities.setText(this.view.settings.lblCheckBox4, addPhoneViewModel.isAlertsRequired ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                this.view.settings.btnEditPhoneNumberSave.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
                CommonUtilities.setText(this.view.settings.btnEditPhoneNumberSave, kony.i18n.getLocalizedString("i18n.ProfileManagement.Save"), CommonUtilities.getaccessibilityConfig());
                if (CommonUtilities.isCSRMode()) {
                    this.view.settings.btnEditPhoneNumberSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    this.view.settings.btnEditPhoneNumberSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    this.view.settings.btnEditPhoneNumberSave.onClick = function() {
                        if (this.validateEditPhoneNumberForm()) {
                            this.updatePhoneNumber(addPhoneViewModel, this.getEditPhoneFormData());
                            this.view.settings.flxHeader.setFocus(true);
                        }
                    }.bind(this);
                }
                this.view.settings.flxEditPhoneNumberButtons.setVisibility(true);
                this.checkUpdateEditPhoneForm();
                //  this.setEditPhoneServicesData(addPhoneViewModel.services);
            }
        },
        /**
         * Method to show data related to edit phone numbers scenario
         * @param {Object} - which sets the data
         */
        getPhoneNumberFromForm: function(data) {
            var self = this;
            data.phoneNumber = self.view.settings.tbxPhoneNumber.text;
            if (applicationManager.getConfigurationManager().getCountryCodeFlag() == true) {
                data.phoneCountryCode = self.view.settings.tbxEditPhoneNumberCountryCode.text;
            } else {
                data.phoneCountryCode = "";
            }
            return data;
        },
        getEditPhoneFormData: function() {
            var self = this;
            var data = {
                Extension: this.view.settings.lbxPhoneNumberType.selectedKey,
                isPrimary: this.view.settings.lblCheckBox3.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                isAlertsRequired: this.view.settings.lblCheckBox4.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                extension: this.view.settings.tbxAddExtensionEditPhoneNumber.text.trim(),
                isTypeBusiness: 0
            }
            self.getPhoneNumberFromForm(data);
            /*  var services = {};
              this.view.settings.segEditPhoneNumberOption1.data.forEach(function(data) {
                  services[data.id] = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED
              })
              data.services = services;*/
            return data;
        },
        /**
         * Method to show the details while editing the phone number
         */
        showEditPhonenumber: function() {
            CommonUtilities.setText(this.view.settings.lblEditPhoneNumberHeading, kony.i18n.getLocalizedString("i18n.ProfileManagement.EditPhoneNumber"), CommonUtilities.getaccessibilityConfig());
            //  this.setEditPhoneNoOption2SegmentData();
            this.showViews(["flxEditPhoneNumbersWrapper"]);
            this.view.settings.btnDelete.isVisible = false;
            this.view.settings.btnEdit.isVisible = false;
            this.view.settings.lblExtensionUnEditable.isVisible = false;
            this.view.settings.lblCountryValue.isVisible = false;
            this.view.settings.lblTypeValue.isVisible = false;
            this.view.settings.lblPhoneNumberValue.isVisible = false;
            this.view.settings.flxRadioBtnUS.isVisible = false;
            this.view.settings.flxRadioBtnInternational.isVisible = false;
            this.view.settings.tbxPhoneNumber.isVisible = true;
            this.view.settings.tbxAddExtensionEditPhoneNumber.isVisible = false;
            this.view.settings.lbxPhoneNumberType.isVisible = true;
            this.view.settings.lblRadioInternational.isVisible = false;
            this.view.settings.lblRadioUS.isVisible = false;
            this.view.settings.flxOption2.setVisibility(false);
            this.view.settings.flxWarning.isVisible = true;
            this.view.settings.btnEditPhoneNumberCancel.isVisible = true;
            this.view.settings.flxEditPhoneNumberCountryCode.setVisibility(true);
            if (applicationManager.getConfigurationManager().getCountryCodeFlag() == true) {
                this.view.settings.tbxEditPhoneNumberCountryCode.setVisibility(true);
            } else {
                this.view.settings.tbxEditPhoneNumberCountryCode.setVisibility(false);
            }
            this.view.settings.btnEditPhoneNumberSave.toolTip = kony.i18n.getLocalizedString('i18n.ProfileManagement.Save');
            CommonUtilities.setText(this.view.settings.btnEditPhoneNumberSave, kony.i18n.getLocalizedString('i18n.ProfileManagement.Save'), CommonUtilities.getaccessibilityConfig());
        },
        /**
         * Method to enable/disable button based on the edited phone number entered
         */
        checkUpdateEditPhoneForm: function() {
            if (!CommonUtilities.isCSRMode()) {
                var formdata = this.getEditPhoneFormData();
                if (formdata.phoneNumber === "") {
                    this.disableButton(this.view.settings.btnEditPhoneNumberSave);
                } else {
                    this.enableButton(this.view.settings.btnEditPhoneNumberSave);
                }
            }
        },
        /**
         * Method to set phone number according to the services
         * @param {Object} services - Array of accounts
         */
        /* setEditPhoneServicesData: function(services) {
             var scopeObj = this;
             function getServiceToggleListener(account, index) {
                 return function() {
                     var data = scopeObj.view.settings.segEditPhoneNumberOption1.data[index];
                     data.lblCheckBox.skin = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
           data.lblCheckBox.text = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED : ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                     scopeObj.view.settings.segEditPhoneNumberOption1.setDataAt(data, index);
                 }
             }
             var dataMap = {
                 "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
                 "flxCheckbox": "flxCheckbox",
                 "lblCheckBox": "lblCheckBox",
                 "lblAccounts": "lblAccounts"
             };
             var data = services.map(function(account, index) {
                 return {
                     "id": account.id,
                     "lblCheckBox": account.selected ? {"text":ViewConstants.FONT_ICONS.CHECBOX_SELECTED,"skin":ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN} : {"text":ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,"skin":ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN},
                     "lblAccounts": account.name,
                     "flxCheckbox": {
                         "onClick": getServiceToggleListener(account, index)
                     }
                 }
             })
             this.view.settings.segEditPhoneNumberOption1.widgetDataMap = dataMap;
             this.view.settings.segEditPhoneNumberOption1.setData(data);
             this.view.forceLayout();
         },*/
        /**
         * Render The Profile Settings View
         * @param {Object} userProfielViewModel
         * Values : name, dob, maskedSSN
         */
        updateUserProfileSetttingsView: function(data) {
            var userProfileViewModel = {
                name: (data.userlastname === null) ? data.userfirstname : (data.userfirstname === null) ? data.userlastname : data.userfirstname + " " + data.userlastname,
                dob: CommonUtilities.getFrontendDateString(data.dateOfBirth, "YYYY-MM-DD"),
                maskedSSN: (data.ssn)? '***-**-' + CommonUtilities.getLastFourDigit(data.ssn):"",
                userImage: data.userImageURL
            };
            this.showProfile();
            this.collapseAll();
            this.setSelectedSkin("flxProfile");
            this.expandWithoutAnimation(this.view.settings.flxProfileSettingsSubMenu);
            this.view.settings.lblProfileSettingsCollapse.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
            CommonUtilities.setText(this.view.settings.lblNameValue, userProfileViewModel.name, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblDOBValue, userProfileViewModel.dob, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblSocialSecurityValue, userProfileViewModel.maskedSSN, CommonUtilities.getaccessibilityConfig());
            if (applicationManager.getConfigurationManager().isSMEUser === "true") {
                if (!kony.sdk.isNullOrUndefined(data.ContactNumbers[0])) {
                    CommonUtilities.setText(this.view.settings.lblMobileValue, data.ContactNumbers[0].Value, CommonUtilities.getaccessibilityConfig());
                    this.view.settings.flxMobileNumber.setVisibility(true);
                } else {
                    this.view.settings.flxMobileNumber.setVisibility(false);
                }
                if (!kony.sdk.isNullOrUndefined(data.EmailIds[0])) {
                    CommonUtilities.setText(this.view.settings.lblEmailIdValue, data.EmailIds[0].Value, CommonUtilities.getaccessibilityConfig());
                    this.view.settings.flxEmailId.setVisibility(true);
                } else {
                    this.view.settings.flxEmailId.setVisibility(false);
                }
            } else {
                this.view.settings.flxMobileNumber.setVisibility(false);
                this.view.settings.flxEmailId.setVisibility(false);
            }
            this.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
            this.view.settings.flxImageError.setVisibility(false);
            if (applicationManager.getConfigurationManager().getProfileImageAvailabilityFlag() === true && userProfileViewModel.userImage && userProfileViewModel.userImage.trim() != "") {
                this.view.settings.imgProfile.base64 = userProfileViewModel.userImage;
                this.view.settings.btnEditPhoto.isVisible = true;
                this.view.settings.btnDeletephoto.isVisible = true;
                this.view.settings.btnAddPhoto.isVisible = false;
                this.view.settings.flxWhatIsSSN.isVisible = false;
            } else if (applicationManager.getConfigurationManager().getProfileImageAvailabilityFlag() === false) {
                this.view.settings.imgProfile.src = ViewConstants.IMAGES.USER_GREY_IMAGE;
                this.view.settings.btnEditPhoto.isVisible = false;
                this.view.settings.btnDeletephoto.isVisible = false;
                this.view.settings.btnAddPhoto.isVisible = false;
                this.view.settings.flxWhatIsSSN.isVisible = false;
            } else {
                this.view.settings.imgProfile.src = ViewConstants.IMAGES.USER_GREY_IMAGE;
                this.view.settings.btnEditPhoto.isVisible = false;
                this.view.settings.btnDeletephoto.isVisible = false;
                this.view.settings.btnAddPhoto.isVisible = true;
                this.view.settings.flxWhatIsSSN.isVisible = true;
            }
            if (!applicationManager.getConfigurationManager().checkUserPermission("PROFILE_SETTINGS_UPDATE")) {
                this.view.settings.btnEditPhoto.isVisible = false;
                this.view.settings.btnDeletephoto.isVisible = false;
                this.view.settings.btnAddPhoto.isVisible = false;
                this.view.settings.flxWhatIsSSN.isVisible = false;
                this.view.forceLayout();
            }
        },
        showConsentManagementView: function(result) {
            var scopeObj = this;
            this.showConsent();
            this.collapseAll();
            this.expand(this.view.settings.flxConsentManagementSubMenu);
            this.view.settings.lblConsentManagementCollapse.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
            this.view.settings.lblConsentManagementCollapse.toolTip = "Collapse";
            this.view.customheader.customhamburger.activateMenu("Settings", "Consent Management");
            this.view.settings.lblConsentManagementCollapse.accessibilityConfig = {
                "a11yLabel": "Collapse"
            };

            this.view.forceLayout();
        },
        showManageAccountAccessView: function() {
            var scopeObj = this;
            this.showManageAccountAccess();
            this.collapseAll();
            this.expand(this.view.settings.flxManageAccountAccessSubMenu);
            this.view.settings.lblManageAccountAccessCollapse.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
            this.view.settings.lblManageAccountAccessCollapse.toolTip = "Collapse";
            this.view.customheader.customhamburger.activateMenu("Settings", "Manage Account Access");
            this.view.settings.lblManageAccountAccessCollapse.accessibilityConfig = {
                "a11yLabel": "Collapse"
            };
            this.view.forceLayout();
        },

        setDefaultUserPhoto: function() {
            var userImageURLTest = applicationManager.getUserPreferencesManager().getUserImage();
            if (this.view.customheader && userImageURLTest == "") {
              	this.view.customheader.headermenu.imgUserReset.src = "";
                this.view.customheader.headermenu.imgUserReset.src = ViewConstants.IMAGES.USER_DEFAULT_IMAGE;
              	this.view.customheader.CopyimgToolTip0i580d9acc07c42.src = "";
                this.view.customheader.CopyimgToolTip0i580d9acc07c42.src = ViewConstants.IMAGES.USER_DEFAULT_IMAGE;
            }
            if (this.view.settings && userImageURLTest == "") {
              	this.view.settings.imgProfile.src = "";
                this.view.settings.imgProfile.src = ViewConstants.IMAGES.USER_DEFAULT_IMAGE;
            }
        },
        /**
         * Method to show the profile management wrapper
         */
        showProfile: function() {
            this.showViews(["flxProfileWrapper"]);
        },
        /**
         * Method to show the consent management wrapper
         */
        showConsent: function() {
            this.showViews(["flxConsentManagementWrapper"]);
        },
        //show showManageAccountAccess
        showManageAccountAccess: function() {
            this.showViews(["flxManageAccountAccessWrapper"]);
        },
        /*********************Ankit Refactored***************************/
        /**
         * Method to set email got from backend
         * @param {Object} response - response of email JSON got from backend
         */
        setEmailsToLbx: function(response) {
            var data = [];
            var section = this.editContext.sectionIndex;
            var index = this.editContext.rowIndex; //this.view.settings.segAccounts.selectedRowIndex[1];
            var accData = this.view.settings.segAccounts.data[section][1][index];
            var list = [];
            if (accData.email !== null && accData.email !== " " && accData.email !== undefined) {
                list.push("assignedEmail");
                list.push(accData.email);
                data.push(list);
            } else {
                data.push(["email", "-"]);
            }
            for (var i = 0; i < response.length; i++) {
                list = [];
                if (accData.email !== response[i].Value) {
                    (response[i].id)?list.push(response[i].id):list.push("");
                    list.push(response[i].Value);
                    data.push(list);
                }
            }
            this.view.settings.lbxEmailForReceiving.masterData = data;
            if (accData.email === null || accData.email === " " || accData.email === undefined) {
                this.view.settings.lbxEmailForReceiving.selectedKey = "email";
            } else {
                this.view.settings.lbxEmailForReceiving.selectedKey = "assignedEmail";
            }
        },
        /**
         * Method to enable/disable the button of terms and condition
         */
        showTermsAndConditions: function() {
            this.enableButton(this.view.btnSave);
            var currForm = this.view;
            currForm.flxTermsAndConditions.height = currForm.flxHeader.info.frame.height + currForm.flxContainer.info.frame.height + currForm.flxFooter.info.frame.height;
            this.view.flxTermsAndConditions.isVisible = true;
            this.view.lblTermsAndConditions.setFocus(true);
            if (CommonUtilities.isFontIconChecked(this.view.settings.lblEnableEStatementsCheckBox) === true && CommonUtilities.isFontIconChecked(this.view.settings.lblTCContentsCheckbox) === true) {
                if (CommonUtilities.isFontIconChecked(this.view.lblTCContentsCheckbox) === false) {
                    CommonUtilities.toggleFontCheckbox(this.view.lblTCContentsCheckbox);
                }
            } else {
                if (CommonUtilities.isFontIconChecked(this.view.lblTCContentsCheckbox) === true) {
                    CommonUtilities.toggleFontCheckbox(this.view.lblTCContentsCheckbox);
                }
            }
        },
        bindTnCData: function(TnCcontent) {
            if (TnCcontent.alreadySigned) {
                this.view.settings.flxTCCheckBox.setVisibility(false);
            } else {
                this.view.settings.flxTCCheckBox.setVisibility(true);
                if (TnCcontent.contentTypeId === OLBConstants.TERMS_AND_CONDITIONS_URL) {
                    this.view.settings.btnTermsAndConditions.onClick = function() {
                        window.open(TnCcontent.termsAndConditionsContent);
                    }
                } else {
                    this.view.settings.btnTermsAndConditions.onClick = this.showTermsAndConditionPopUp;
                }
                this.view.flxClose.onClick = this.hideTermsAndConditionPopUp;
                this.setTnCDATASection(TnCcontent.termsAndConditionsContent);
            }
        },
        showTermsAndConditionPopUp: function() {
            var height = this.view.flxHeader.info.frame.height + this.view.flxContainer.info.frame.height + this.view.flxFooter.info.frame.height;
            this.view.flxTermsAndConditions.height = height + "dp";
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.forceLayout();
        },
        hideTermsAndConditionPopUp: function() {
            this.view.flxTermsAndConditions.setVisibility(false);
        },
        setTnCDATASection: function(content) {
            this.view.rtxTC.text = content;
            FormControllerUtility.setHtmlToBrowserWidget(this, this.view.brwBodyTnC, content);

            /*     if (document.getElementById("iframe_brwBodyTnC").contentWindow.document.getElementById("viewer")) {
                     document.getElementById("iframe_brwBodyTnC").contentWindow.document.getElementById("viewer").innerHTML = content;
                 } else {
                     if (!document.getElementById("iframe_brwBodyTnC").newOnload) {
                         document.getElementById("iframe_brwBodyTnC").newOnload = document.getElementById("iframe_brwBodyTnC").onload;
                     }
                     document.getElementById("iframe_brwBodyTnC").onload = function() {
                         document.getElementById("iframe_brwBodyTnC").newOnload();
                         document.getElementById("iframe_brwBodyTnC").contentWindow.document.getElementById("viewer").innerHTML = content;
                 }
                }*/
        },
        /**
         * Method to save details of account on click of save button
         */
        onSaveAccountDetails: function() {
            var self = this;
            var sectionIndex = this.editContext.sectionIndex;
            var index = this.editContext.rowIndex; //self.view.settings.segAccounts.selectedRowIndex[1];
            var data = self.view.settings.segAccounts.data[sectionIndex][1][index];
            if (self.view.settings.tbxAccountNickNameValue.text === "" || self.view.settings.tbxAccountNickNameValue.text === null) {
                data.nickName = data.lblAccountHolderValue;
            } else {
                data.nickName = self.view.settings.tbxAccountNickNameValue.text;
            }
            if (data.external === true) {
                data = {
                    Account_id: data.accountIdPk,
                    NickName: data.nickName,
                    FavouriteStatus: self.view.settings.lblFavoriteEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? "true" : "false",
                    external: true,
                };
            } else {
                data = {
                    accountID: self.view.settings.lblAccountNumberValue.text,
                    nickName: data.nickName,
                    favouriteStatus: self.view.settings.lblFavoriteEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? 1 : 0,
                    eStatementEnable: self.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? 1 : 0,
                    email: self.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? self.view.settings.lbxEmailForReceiving.selectedKeyValue[1] : " ",
                };
            }
            FormControllerUtility.showProgressBar(self.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.savePreferredAccountsData(data);
        },
        /**
         * Method to prefill the data while editing prefered account
         * @param {boolean} errorScenario true/false
         */
        onPreferenceAccountEdit: function(context, errorScenario) {
            if (errorScenario === true) {
                this.view.settings.flxErrorEditAccounts.setVisibility(true);
                CommonUtilities.setText(this.view.settings.lblErrorEditAccounts, kony.i18n.getLocalizedString("i18n.ProfileManagement.weAreUnableToProcess"), CommonUtilities.getaccessibilityConfig());
            } else {
                this.editContext = context;
                CommonUtilities.setText(this.view.settings.lblIAccept, kony.i18n.getLocalizedString("i18n.ProfileManagement.IAccept"), CommonUtilities.getaccessibilityConfig());
                this.view.settings.flxErrorEditAccounts.setVisibility(false);
         
            var self = this;
            var section = context.sectionIndex; //this.view.settings.segAccounts.selectedRowIndex[0]
            var index = context.rowIndex; //this.view.settings.segAccounts.selectedRowIndex[1];
            var data = this.view.settings.segAccounts.data[section][1][index];
            if (data.nickName === data.lblAccountHolderValue) {
                this.view.settings.tbxAccountNickNameValue.text = "";
            } else {
                this.view.settings.tbxAccountNickNameValue.text = data.nickName;
            }
            if (data.lblAccountsTypeIcon["isVisible"]) {
                this.view.settings.lblAccountIcon.setVisibility(true);
                this.view.settings.lblAccountIcon.text = data.lblAccountsTypeIcon["text"];
            }
            CommonUtilities.setText(this.view.settings.lblFullNameValue, data.lblAccountHolderValue.text, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblAccountTypeValue, data.lblAccountTypeValue.text, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblAccountNumberValue, data.lblAccountNumberValue.text, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblFavoriteEmailCheckBox, data.lblMarkAsFavouriteAccountCheckBoxIcon.text, CommonUtilities.getaccessibilityConfig());
            this.view.settings.lblFavoriteEmailCheckBox.skin = "skn0273e320pxolbfonticons";
            //           this.view.settings.lblFavoriteEmailCheckBox.onTouchEnd = function() {
            //               self.toggleFontCheckBox(self.view.settings.lblFavoriteEmailCheckBox);
            //           };
            if (data.external === false) {
                this.view.settings.lblTCContentsCheckbox.setVisibility(true);
                this.view.settings.flxPleaseNoteTheFollowingPoints.setVisibility(true);
                this.view.settings.flxEnableEStatementsCheckBox.setVisibility(true);
                this.view.settings.btnTermsAndConditions.setVisibility(true);
                CommonUtilities.setText(this.view.settings.lblEnableEStatementsCheckBox, data.lblEstatementCheckBoxIcon.text, CommonUtilities.getaccessibilityConfig());
                this.view.settings.lblEnableEStatementsCheckBox.skin = "skn0273e320pxolbfonticons";
                CommonUtilities.setText(this.view.lblTCContentsCheckbox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                this.view.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                this.view.settings.btnTermsAndConditions.onClick = this.showTermsAndConditionPopUp;
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getUserEmail();
                if (this.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                    this.view.settings.flxTCCheckBox.setVisibility(true);
                    this.view.settings.flxTCContentsCheckbox.setVisibility(true);
                    CommonUtilities.setText(this.view.settings.lblTCContentsCheckbox, ViewConstants.FONT_ICONS.CHECBOX_SELECTED, CommonUtilities.getaccessibilityConfig());
                    this.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                    this.view.settings.flxPleaseNoteTheFollowingPoints.height = "160dp";
                } else {
                    this.view.settings.flxTCCheckBox.setVisibility(false);
                    this.view.settings.flxTCContentsCheckbox.setVisibility(false);
                    CommonUtilities.setText(this.view.settings.lblTCContentsCheckbox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                    this.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                    this.view.settings.flxPleaseNoteTheFollowingPoints.height = "120dp";
                }
            } else {
                this.view.settings.flxEmailForReceiving.setVisibility(false);
                this.view.settings.flxTCCheckBox.setVisibility(false);
                this.view.settings.flxTCContentsCheckbox.setVisibility(false);
                this.view.settings.lblTCContentsCheckbox.setVisibility(false);
                this.view.settings.flxPleaseNoteTheFollowingPoints.setVisibility(false);
                this.view.settings.flxEnableEStatementsCheckBox.setVisibility(false);
                this.view.settings.btnTermsAndConditions.setVisibility(false);
            }
            if (this.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED && this.view.settings.lblTCContentsCheckbox.text !== ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                this.disableButton(this.view.settings.btnEditAccountsSave);
                this.disableButton(this.view.btnSave);
            } else {
                this.enableButton(this.view.settings.btnEditAccountsSave);
                this.view.settings.flxTCCheckBox.setVisibility(true);
                this.enableButton(this.view.btnSave);
            }
            this.view.settings.flximgEnableEStatementsCheckBox.onClick = function() {
                self.toggleCheckBoxEditAccounts(self.view.settings.lblEnableEStatementsCheckBox);
            };
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnEditAccountsSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnEditAccountsSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.settings.btnEditAccountsSave.focus = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnEditAccountsSave.onClick = this.onSaveAccountDetails;
            }
            this.showViews(["flxEditAccountsWrapper"]);
            if (applicationManager.getConfigurationManager().editNickNameAccountSettings === "true") {
                this.view.settings.flxAccountNickName.setVisibility(true);
            } else {
                this.view.settings.flxAccountNickName.setVisibility(false);
            }
            if (data.email && this.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                var response = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
                this.setEmailsToLbx(response);
                this.view.settings.flxEmailForReceiving.setVisibility(true);
                this.view.settings.lbxEmailForReceiving.setVisibility(true);
            } else {
                this.view.settings.flxEmailForReceiving.setVisibility(false);
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
          }
        },
        /**
         * Method to move a particular account upwards
         */
        onBtnMoveUp: function() {
            var section = this.view.settings.segAccounts.selectedRowIndex[0]
            var index = this.view.settings.segAccounts.selectedRowIndex[1];
            var segData = this.view.settings.segAccounts.data[section][1];
            var updatedAccounts = [{
                accountNumber: segData[index].lblAccountNumberValue.text,
                accountPreference: segData[index - 1].accountPreference.toString(),
            }, {
                accountNumber: segData[index - 1].lblAccountNumberValue.text,
                accountPreference: segData[index].accountPreference.toString(),
            }]
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.setAccountsPreference(updatedAccounts);
        },
        /**
         * Method to move a particular account downwards
         */
        onBtnMoveDown: function() {
            var section = this.view.settings.segAccounts.selectedRowIndex[0]
            var index = this.view.settings.segAccounts.selectedRowIndex[1];
            var segData = this.view.settings.segAccounts.data[section][1];
            var updatedAccounts = [{
                accountNumber: segData[index].lblAccountNumberValue.text,
                accountPreference: segData[index + 1].accountPreference.toString(),
            }, {
                accountNumber: segData[index + 1].lblAccountNumberValue.text,
                accountPreference: segData[index].accountPreference.toString(),
            }]
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.setAccountsPreference(updatedAccounts);
        },
        accountTypeConfig: (function() {
            var accountTypeConfig = {};
            var ApplicationManager = require('ApplicationManager');
            applicationManager = ApplicationManager.getApplicationManager();
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING)] = {
                sideImage: ViewConstants.IMAGES.SIDEBAR_TURQUOISE,
                skin: ViewConstants.SKINS.SAVINGS_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_TURQUOISE
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING)] = {
                sideImage: ViewConstants.IMAGES.SIDEBAR_PURPLE,
                skin: ViewConstants.SKINS.CHECKINGS_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_PURPLE
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD)] = {
                sideImage: ViewConstants.IMAGES.SIDEBAR_YELLOW,
                skin: ViewConstants.SKINS.CREDIT_CARD_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_YELLOW
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT)] = {
                sideImage: ViewConstants.IMAGES.SIDEBAR_BLUE,
                skin: ViewConstants.SKINS.DEPOSIT_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_TURQUOISE
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE)] = {
                sideImage: ViewConstants.IMAGES.SIDEBAR_BROWN,
                skin: ViewConstants.SKINS.MORTGAGE_CARD_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_YELLOW
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN)] = {
                sideImage: ViewConstants.IMAGES.SIDEBAR_BROWN,
                skin: ViewConstants.SKINS.MORTGAGE_CARD_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_YELLOW
            };
            accountTypeConfig['Default'] = {
                sideImage: ViewConstants.IMAGES.SIDEBAR_TURQUOISE,
                skin: ViewConstants.SKINS.SAVINGS_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_TURQUOISE
            }
            return accountTypeConfig;
        })(),
        /**
         * Method to get configurations for accounts
         * @param {string} accountType account type
         * @returns
         */
        getConfigFor: function(accountType) {
            if (this.accountTypeConfig[accountType]) {
                return this.accountTypeConfig[accountType];
            } else {
                return this.accountTypeConfig.Default;
            }
        },
        /**
         * Method to generate accounts viewmodel for account preferences
         * @param {JSON} accountsData Data related to account
         * @returns JSON accounts data
         */
        generateViewModelAccounts: function(accountsData) {
            accountsData = JSON.parse(JSON.stringify(accountsData));
            var getAccountHolder = function(accHolder) {
                try {
                    var nameObj = JSON.parse(accHolder);
                    return nameObj.fullname;
                } catch (exception) {
                    return accHolder;
                }
            }
            if (accountsData.isExternalAccount) {
                return {
                    "accountIdPK": accountsData.accountID,
                    "flxIdentifier": this.getConfigFor(accountsData.accountType).skin,
                    "AccountName": accountsData.AccountName,
                    "AccountHolder": accountsData.accountHolder,
                    "AccountNumber": accountsData.accountID,
                    "AccountType": accountsData.accountType,
                    "imgFavoriteCheckBox": (String(accountsData.favouriteStatus) !== "0") ? ViewConstants.IMAGES.CHECKED_IMAGE : ViewConstants.IMAGES.UNCHECKED_IMAGE,
                    "imgMenu": this.getConfigFor(accountsData.accountType).image,
                    "imgEStatementCheckBox": ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                    "fullName": "",
                    "nickName": accountsData.nickName,
                    "accountPreference": Number.MAX_SAFE_INTEGER,
                    "email": "",
                    "external": true
                }
            } else {
                return {
                    "flxIdentifier": this.getConfigFor(accountsData.accountType).skin,
                    "AccountName": accountsData.accountName,
                    "AccountHolder": getAccountHolder(accountsData.accountHolder),
                    "AccountNumber": accountsData.accountID,
                    "AccountType": accountsData.accountType,
                    "imgFavoriteCheckBox": accountsData.favouriteStatus == "1" ? ViewConstants.IMAGES.CHECKED_IMAGE : ViewConstants.IMAGES.UNCHECKED_IMAGE,
                    "imgEStatementCheckBox": (accountsData.eStatementEnable == "true" || accountsData.eStatementEnable == "1") ? ViewConstants.IMAGES.CHECKED_IMAGE : ViewConstants.IMAGES.UNCHECKED_IMAGE,
                    "imgMenu": this.getConfigFor(accountsData.accountType).image,
                    "fullName": accountsData.firstName + " " + accountsData.lastName,
                    "nickName": accountsData.nickName,
                    "accountPreference": Number(accountsData.accountPreference),
                    "email": accountsData.email,
                    "external": false,
                    "displayName": accountsData.displayName,
                    "isBusinessAccount": (accountsData.isBusinessAccount === "true"),
                    "MembershipName": accountsData.MembershipName,
                    "Membership_id": accountsData.Membership_id,
                }
            }
        },
        /**
         * Method to show accounts preffered list
         * @param {Collection} viewModel Array od accounts jsons
         */
        showPreferredAccountsList: function(viewModel) {
            var scopeObj = this;
            this.accountDataStore = cloneJSON(viewModel);
            this.view.customheader.customhamburger.activateMenu("Settings", "Account Settings");
            this.view.settings.tbxAccountNickNameValue.onKeyUp = function() {
                var regex = new RegExp("^[a-zA-Z0-9 _.-]*$");
                if (regex.test(scopeObj.view.settings.tbxAccountNickNameValue.text)) {
                    scopeObj.enableButton(scopeObj.view.settings.btnEditAccountsSave);
                } else {
                    scopeObj.disableButton(scopeObj.view.settings.btnEditAccountsSave);
                }
                if (scopeObj.view.settings.tbxAccountNickNameValue.text.trim() == "") {
                    scopeObj.disableButton(scopeObj.view.settings.btnEditAccountsSave);
                }
            };
            this.showAccounts();
            this.collapseAll();
            this.expandWithoutAnimation(this.view.settings.flxAccountSettingsSubMenu);
            this.view.settings.lblAccountSettingsCollapse.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
            if (viewModel.errorCase === true) {
                this.view.flxDowntimeWarning.setVisibility(true);
                this.view.settings.segAccounts.setVisibility(false);
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.view.flxDowntimeWarning.setVisibility(false);
                this.setAccountsSegmentData(viewModel.map(this.generateViewModelAccounts));
                this.setSelectedSkin("flxAccountPreferences");
                this.view.settings.segAccounts.setVisibility(true);
            }
        },
        /**
         * Method to map accounts data to segment
         * Array od accounts jsons
         * @param {*} viewModel
         */
        setAccountsSegmentData: function(viewModel) {
            var self = this;
            var isCombinedUser = applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true";
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            var isBusinessUser = applicationManager.getConfigurationManager().getConfigurationValue('isSMEUser') === "true";
            var dataMap = {
                "btnEdit": "btnEdit",
                "flxAccountHolder": "flxAccountHolder",
                "flxContent": "flxContent",
                "flxAccountHolderKey": "flxAccountHolderKey",
                "flxAccountHolderValue": "flxAccountHolderValue",
                "flxAccountListItemWrapper": "flxAccountListItemWrapper",
                "flxAccountName": "flxAccountName",
                "flxAccountNumber": "flxAccountNumber",
                "flxAccountNumberKey": "flxAccountNumberKey",
                "flxAccountNumberValue": "flxAccountNumberValue",
                "flxAccountRow": "flxAccountRow",
                "flxAccountType": "flxAccountType",
                "flxAccountTypeKey": "flxAccountTypeKey",
                "flxAccountTypeValue": "flxAccountTypeValue",
                "flxEStatement": "flxEStatement",
                "flxEStatementCheckBox": "flxEStatementCheckBox",
                "flxFavoriteCheckBox": "flxFavoriteCheckBox",
                "flxLeft": "flxLeft",
                "flxMarkAsFavorite": "flxMarkAsFavorite",
                //"flxMenu": "flxMenu",
                "flxRight": "flxRight",
                "imgEStatementCheckBox": "imgEStatementCheckBox",
                "imgFavoriteCheckBox": "imgFavoriteCheckBox",
                "flxIdentifier": "flxIdentifier",
                "imgMenu": "imgMenu",
                "lblAccountHolderColon": "lblAccountHolderColon",
                "lblAccountHolderKey": "lblAccountHolderKey",
                "lblAccountHolderValue": "lblAccountHolderValue",
                "lblAccountName": "lblAccountName",
                "lblAccountNumberColon": "lblAccountNumberColon",
                "lblAccountNumberKey": "lblAccountNumberKey",
                "lblAccountNumberValue": "lblAccountNumberValue",
                "lblAccountTypeColon": "lblAccountTypeColon",
                "lblAccountTypeKey": "lblAccountTypeKey",
                "lblAccountTypeValue": "lblAccountTypeValue",
                "lblEStatement": "lblEStatement",
                "lblMarkAsFavorite": "lblMarkAsFavorite",
                "lblSepeartorlast": "lblSepeartorlast",
                "lblSeperator": "lblSeperator",
                "lblSeperator2": "lblSeperator2",
                "lblseperator3": "lblseperator3",
                "lblsepeartorfirst": "lblsepeartorfirst",
                "flxMoveUp": "flxMoveUp",
                "flxMoveDown": "flxMoveDown",
                "imgMoveUp": "imgMoveUp",
                "lblMove": "lblMove",
                "imgMoveDown": "imgMoveDown",
                "lblMarkAsFavouriteAccountCheckBoxIcon": "lblMarkAsFavouriteAccountCheckBoxIcon",
                "lblEstatementCheckBoxIcon": "lblEstatementCheckBoxIcon",
                "lblMoveUpIcon": "lblMoveUpIcon",
                "lblMoveDownIcon": "lblMoveDownIcon",
                "btn": "btn",
                "template": "template",
                "lblTransactionHeader": "lblTransactionHeader",
                "lblAccountsTypeIcon": "lblAccountsTypeIcon"
            };
            var mapViewModel = function(data, index, viewModel) {
                var dataObject = {
                    "btn": {
                        text: ""
                    },
                    "accountPreference": data.accountPreference,
                    "accountIdPk": (data.external) ? data.accountIdPK : "",
                    "external": data.external,
                    "fullName": data.fullName,
                    "nickName": data.nickName,
                    "flxIdentifier": {
                        "skin": data.flxIdentifier
                    },
                    "lblTransactionHeader": {
                        "text": data.AccountType,
                        "accessibilityconfig": {
                            "a11yLabel": data.AccountType,
                        },
                    },
                    "lblMarkAsFavouriteAccountCheckBoxIcon": {
                        text: data.imgFavoriteCheckBox === ViewConstants.IMAGES.CHECKED_IMAGE ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                        "accessibilityconfig": {
                            "a11yLabel": data.imgFavoriteCheckBox === ViewConstants.IMAGES.CHECKED_IMAGE ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                        }
                    },
                    "lblEstatementCheckBoxIcon": (data.external) ? {
                        isVisible: false
                    } : {
                        text: data.imgEStatementCheckBox === ViewConstants.IMAGES.CHECKED_IMAGE ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                        "accessibilityconfig": {
                            "a11yLabel": data.imgEStatementCheckBox === ViewConstants.IMAGES.CHECKED_IMAGE ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                        }
                    },
                    "lblMoveUpIcon": index !== 0 ? {
                        text: ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                        "accessibilityconfig": {
                            "a11yLabel": ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                        },
                        skin: ViewConstants.SKINS.FONT_ICON_BLUE_21PX
                    } : {
                        text: ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                        "accessibilityconfig": {
                            "a11yLabel": ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                        },
                        skin: ViewConstants.SKINS.LABEL_MOVE_INACTIVE
                    },
                    "lblMoveDownIcon": {
                        text: ViewConstants.FONT_ICONS.DISABLE_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": ViewConstants.FONT_ICONS.DISABLE_DOWN,
                        },
                        skin: ViewConstants.SKINS.FONT_ICON_BLUE_21PX
                    },
                    "lblAccountName": {
                        "text": data.AccountName,
                        "accessibilityconfig": {
                            "a11yLabel": data.AccountName,
                        },
                    },
                    "lblAccountHolderKey": {
                        "text": kony.i18n.getLocalizedString('i18n.ProfileManagement.AccountHolder'),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString('i18n.ProfileManagement.AccountHolder'),
                        },
                    },
                    "lblAccountHolderColon": {
                        "text": ": ",
                        "accessibilityconfig": {
                            "a11yLabel": ": ",
                        },
                    },
                    "lblAccountHolderValue": {
                        "text": data.AccountHolder,
                        "accessibilityconfig": {
                            "a11yLabel": data.AccountHolder,
                        },
                    },
                    "lblAccountNumberKey": {
                        "text": kony.i18n.getLocalizedString('i18n.ProfileManagement.AccountNumber'),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString('i18n.ProfileManagement.AccountNumber'),
                        },
                    },
                    "lblAccountNumberValue": {
                        "text": data.AccountNumber,
                        "accessibilityconfig": {
                            "a11yLabel": data.AccountNumber,
                        },
                    },
                    "lblAccountTypeKey": {
                        "text": kony.i18n.getLocalizedString('i18n.ProfileManagement.AccountType'),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString('i18n.ProfileManagement.AccountType'),
                        },
                    },
                    "lblAccountNumberColon": {
                        "text": ": ",
                        "accessibilityconfig": {
                            "a11yLabel": ": ",
                        },
                    },
                    "lblAccountTypeValue": {
                        "text": data.AccountType,
                        "accessibilityconfig": {
                            "a11yLabel": data.AccountType,
                        },
                    },
                    "imgEStatementCheckBox": data.imgEStatementCheckBox,
                    "lblAccountTypeColon": {
                        "text": ": ",
                        "accessibilityconfig": {
                            "a11yLabel": ": ",
                        },
                    },
                    "imgFavoriteCheckBox": data.imgFavoriteCheckBox,
                    "lblMarkAsFavorite": {
                        "text": "Mark as Favorite Account",
                        "accessibilityconfig": {
                            "a11yLabel": "Mark as Favorite Account",
                        },
                    },
                    "lblEStatement": (data.external) ? {
                        isVisible: false
                    } : {
                        "text": kony.i18n.getLocalizedString("i18n.ProfileManagement.E-statementEnabled"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.ProfileManagement.E-statementEnabled"),
                        },
                    },
                    "btnEdit": {
                        text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                        },
                        "isVisible": applicationManager.getConfigurationManager().checkUserPermission("ACCOUNT_SETTINGS_EDIT"),
                        onClick: function(eventobject, context) {
                            self.onPreferenceAccountEdit(context, null)
                        }.bind(this)
                    },
                   /* "flxMenu": {
                        "skin": "CopyslFbox",
                        "isVisible": applicationManager.getConfigurationManager().reOrderAccountPreferences === "true" ? true : false
                    },*/
                    "imgMenu": data.imgMenu,
                    "imgMoveUp": (data.external) ? {
                        isVisible: false
                    } : (index !== 0 ? ViewConstants.IMAGES.ACTIVE_UP : ViewConstants.IMAGES.DISABLE_UP),
                    "lblMove": (data.external) ? {
                        isVisible: false
                    } : {
                        "text": "Move",
                        "accessibilityconfig": {
                            "a11yLabel": "Move",
                        },
                    },
                    "imgMoveDown": (data.external) ? {
                        isVisible: false
                    } : ViewConstants.IMAGES.DISABLE_DOWN,
                    "lblSepeartorlast": "lblSepeartorlast",
                    "lblsepeartorfirst": "lblsepeartorfirst",
                    "flxMoveUp": (data.external) ? {
                        isVisible: false
                    } : {
                        onClick: function() {
                            if (index !== 0) {
                                self.onBtnMoveUp();
                            }
                        }
                    },
                    "flxMoveDown": (data.external) ? {
                        isVisible: false
                    } : {
                        onClick: function() {
                            self.onBtnMoveDown();
                        }
                    },
                    "lblSeperator": "lblSeperator",
                    "lblSeperator2": "lblSeperator2",
                    "lblseperator3": "lblseperator3",
                    "template": "flxAccountRow",
                    "email": data.email,
                    "lblAccountsTypeIcon": {
                        //"isVisible" :  (isCombinedUser)? true : false,
                        "isVisible": this.profileAccess === "both" ? true : false,
                        "text": (data.isBusinessAccount) ? "r" : "s"
                    }
                };
                if (CommonUtilities.isCSRMode()) {
                    dataObject.flxMoveUp.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    dataObject.flxMoveDown.onClick = CommonUtilities.disableButtonActionForCSRMode();
                }
                return dataObject;
            };
            var data = self.getDataWithSections(viewModel, mapViewModel.bind(this));
            var len = data.length;
            var i = 0;
            if (len !== 0) {
                for (i = 0; i < data.length; i++) {
                    var rowCount = data[i][1].length;
                    data[i][1][rowCount - 1].imgMoveDown = ViewConstants.IMAGES.DISABLE_DOWN;
                    data[i][1][rowCount - 1].lblMoveDownIcon = {
                        text: ViewConstants.FONT_ICONS.DISABLE_DOWN,
                        skin: ViewConstants.SKINS.LABEL_MOVE_INACTIVE
                    };
                    data[i][1][rowCount - 1].flxMoveDown = function() {};
                    data[i][1][0].imgMoveUp = ViewConstants.IMAGES.DISABLE_UP;
                    data[i][1][0].lblMoveUpIcon = {
                        text: ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                        skin: ViewConstants.SKINS.LABEL_MOVE_INACTIVE
                    };
                    //                      data[i][1][0].lblMarkAsFavouriteAccountCheckBoxIcon = {
                    //                           skin: "skn0273e320pxolbfonticons",
                    //                           text: "D"
                    //                       };
                    //                      data[i][1][0].lblEstatementCheckBoxIcon = {
                    //                           skin: "skn0273e320pxolbfonticons",
                    //                           text: "D"
                    //                       };
                    data[i][1][0].flxMoveUp = function() {};
                }
            }
            this.view.settings.segAccounts.widgetDataMap = dataMap;
            if (isCombinedUser || isBusinessUser)
                this.view.settings.segAccounts.sectionHeaderSkin = "segAccountListItemHeaderPMCombinedAccess";
            else
                this.view.settings.segAccounts.sectionHeaderSkin = "segAccountListItemHeaderPM";
            if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                for (i = 0; i < data.length; i++) {
                    for (var j = 0; j < data[i][1].length; j++)
                        data[i][1][j].template = "flxAccountRowTablet";
                }
            };
            this.view.settings.segAccounts.setData(data);
            this.view.settings.forceLayout();
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to create section header for accounts
         * @param {Collection} accounts List of accounts
         */
        getDataWithSections: function(viewModel, mapViewModel) {
            var finalData = {};
            var scopeObj = this;
            var prioritizeAccountTypes = applicationManager.getTypeManager().getAccountTypesByPriority();
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            var retailUser = applicationManager.getConfigurationManager().getConfigurationValue('isRBUser') === "true";
            var business = kony.i18n.getLocalizedString("i18n.accounts.Business");
            var personal = kony.i18n.getLocalizedString("i18n.accounts.Personal");
            var accountTypes = [];

            viewModel.forEach(function(account, index, viewModel) {
                if (isSingleCustomerProfile) {
                    if (account.external != true) {
                        var accountType = applicationManager.getTypeManager().getAccountType(account.AccountType);
                        if (finalData.hasOwnProperty(accountType)) {
                            finalData[accountType][1].push(mapViewModel(account, index, viewModel));
                        } else {
                            finalData[accountType] = [{
                                    lblTransactionHeader: applicationManager.getTypeManager().getAccountTypeDisplayValue(accountType) != undefined ?
                                        applicationManager.getTypeManager().getAccountTypeDisplayValue(accountType) : accountType + " " + kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                                    template: "flxAccountListItemHeaderPM"
                                },
                                [mapViewModel(account, index, viewModel)]
                            ]
                        }
                    }
                } else {
                    var accountRoleType = personal;
                    var accountTypeIcon = "";
                    if (account.isBusinessAccount === false) {
                        if (scopeObj.primaryCustomerId.id === account.Membership_id && scopeObj.primaryCustomerId.type === 'personal') {
                            accountRoleType = "Personal Accounts";
                            accountTypeIcon = "s";
                        } else {
                            accountRoleType = account.Membership_id;
                            accountTypeIcon = "s";
                        }
                    } else {
                        accountRoleType = account.Membership_id;
                        accountTypeIcon = "r";
                    }
                    if (finalData.hasOwnProperty(accountRoleType) && account.Membership_id === finalData[accountRoleType][0]["membershipId"]) {
                        finalData[accountRoleType][1].push(mapViewModel(account, index, viewModel));
                    } else {
                        finalData[accountRoleType] = [{
                                lblTransactionHeader: accountRoleType === "Personal Accounts" ? accountRoleType : account.MembershipName,
                                template: "flxAccountListItemHeaderPM",
                                membershipId: account.Membership_id,
                            },
                            [mapViewModel(account, index, viewModel)]
                        ];
                        //                 if(accountRoleType!==personal)
                        //                   accountTypes.push(accountRoleType);
                    }
                }
            })
            var data = [];
            if (!isSingleCustomerProfile) {
                data = this.sortAccountData(finalData, accountTypes);
            } else {
                for (var key in prioritizeAccountTypes) {
                    var accountType = prioritizeAccountTypes[key];
                    if (finalData.hasOwnProperty(accountType)) {
                        data.push(finalData[accountType]);
                    }
                }
            }
            return data;
        },

        sortAccountData: function(finalData, accountTypes) {
            var data = [];
            var personal = kony.i18n.getLocalizedString("i18n.accounts.Personal");

            if (finalData[personal][1].length != 0)
                data.push(finalData[personal]);
            accountTypes.forEach(function(type) {
                if (finalData[type][1].length != 0)
                    data.push(finalData[type]);
            });

            for (var i = 0; i < data.length; i++) {
                var accoountTypeOrder = applicationManager.getTypeManager().getAccountTypesByPriority();
                var sortedData = data[i][1];
                sortedData.sort(function(a, b) {
                    return accoountTypeOrder.indexOf(a.lblAccountTypeValue) - accoountTypeOrder.indexOf(b.lblAccountTypeValue);
                });
                data[i][1] = sortedData;
            }

            return data;
        },


        /**
         * Method that gets called on click of save default accounts
         */
        onSaveDefaultAccounts: function() {
            var isRetailUser = applicationManager.getConfigurationManager().getConfigurationValue('isRBUser') === "true";
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            if (scopeObj.view.settings.btnDefaultTransactionAccountEdit.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Save")) {
                //if(!isRetailUser){
                var defaultAccounts = {
                    default_account_billPay: (scopeObj.view.settings.segBillPayAccounts.selectedRowItems.length > 0) ? scopeObj.view.settings.segBillPayAccounts.selectedRowItems[0].accountID : this.defaultBillPayAccounts,
                    default_account_deposit: (scopeObj.view.settings.segCheckDepositAccounts.selectedRowItems.length > 0) ? scopeObj.view.settings.segCheckDepositAccounts.selectedRowItems[0].accountID : this.defaultCheckDepositAccounts
                };
                //             }  
                //             else{
                //                var defaultAccounts = {
                //                   default_account_transfers: scopeObj.view.settings.lbxTransfers.selectedKey,
                //                   default_account_billPay: scopeObj.view.settings.lbxBillPay.selectedKey,
                //                   default_from_account_p2p: scopeObj.view.settings.lbxPayAPreson.selectedKey,
                //                   default_account_deposit: scopeObj.view.settings.lbxCheckDeposit.selectedKey
                //               };
                //             }
                if ((scopeObj.view.settings.flxTransfers.isVisible && defaultAccounts.default_account_transfers === "undefined") ||
                    (scopeObj.view.settings.flxBillPay.isVisible && defaultAccounts.default_account_billPay === "undefined") ||
                    (scopeObj.view.settings.flxPayAPerson.isVisible && defaultAccounts.default_from_account_p2p === "undefined") ||
                    (scopeObj.view.settings.flxCheckDeposit.isVisible && defaultAccounts.default_account_deposit === "undefined")) {
                    scopeObj.view.settings.txtDefaultTransactionAccountWarning.text = kony.i18n.getLocalizedString("i18n.profile.defaultAccountToBlank");
                    scopeObj.view.settings.txtDefaultTransactionAccountWarning.setVisibility(true);
                    scopeObj.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
                    scopeObj.view.settings.forceLayout();
                    FormControllerUtility.hideProgressBar(scopeObj.view);
                } else {
                    scopeObj.view.settings.txtDefaultTransactionAccountWarning.setVisibility(true);
                    scopeObj.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveDefaultAccounts(defaultAccounts);
                    scopeObj.showDefaultScreen();
                    CommonUtilities.setText(scopeObj.view.settings.btnDefaultTransactionAccountEdit, kony.i18n.getLocalizedString("i18n.billPay.Edit"), CommonUtilities.getaccessibilityConfig());
                    scopeObj.view.settings.btnDefaultTransactionAccountEdit.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
                }
            } else {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAccountsList();
                //             if(isRetailUser){
                //               scopeObj.view.settings.lbxTransfers.setVisibility(true);
                //               scopeObj.view.settings.flxTransfersValue.setVisibility(false);
                //               scopeObj.view.settings.lbxBillPay.setVisibility(true);
                //               scopeObj.view.settings.flxBillPayValue.setVisibility(false);
                //               scopeObj.view.settings.lbxPayAPreson.setVisibility(true);
                //               scopeObj.view.settings.flxPayAPersonValue.setVisibility(false);
                //               scopeObj.view.settings.lbxCheckDeposit.setVisibility(true);
                //               scopeObj.view.settings.flxCheckDepositValue.setVisibility(false);
                //               scopeObj.view.settings.flxCheckDepositAccountInfo.setVisibility(false);
                //               scopeObj.view.settings.flxBillPayAccountInfo.setVisibility(false);
                //             }
                //else{
                if (this.billPayAccounts.length !== 0) {
                    scopeObj.view.settings.flxBillPaySelectedValue.setVisibility(true);
                    scopeObj.view.settings.flxBillPayValue.setVisibility(false);
                }
                if (this.checkDepositAccounts.length !== 0) {
                    scopeObj.view.settings.flxCheckDepositSelectedValue.setVisibility(true);
                    scopeObj.view.settings.flxCheckDepositValue.setVisibility(false);
                }
                // }
                scopeObj.view.settings.btnDefaultTransctionAccountsCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                CommonUtilities.setText(scopeObj.view.settings.btnDefaultTransactionAccountEdit, kony.i18n.getLocalizedString("i18n.ProfileManagement.Save"), CommonUtilities.getaccessibilityConfig());
                scopeObj.view.settings.btnDefaultTransactionAccountEdit.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
                scopeObj.view.settings.btnDefaultTransctionAccountsCancel.setVisibility(true);
            }
        },
        /**
         * Method to show UI according to the default account
         * @param {JSON} viewModel list of default accounts for different transactions
         */
        showDefaultUserAccount: function(viewModel) {
            this.showViews(["flxDefaultTransactionAccountWrapper"]);
            this.view.customheader.customhamburger.activateMenu("Settings", "Account Settings");
            if (viewModel.errorCase === true) {
                this.view.settings.flxDefaultTransactionButtons.setVisibility(false);
                this.view.settings.flxDefaultTransctionAccounts.setVisibility(false);
                this.view.settings.flxDefaultTransactionAccountWrapper.setVisibility(true);
                this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
                this.view.settings.txtDefaultTransactionAccountWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
                this.view.settings.flxDefaultAccountsSelected.setVisibility(false);
            } else {
                this.view.settings.flxDefaultTransactionButtons.setVisibility(true);
                this.view.settings.flxDefaultTransctionAccounts.setVisibility(true);
                this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
                this.view.settings.txtDefaultTransactionAccountWarning.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.YouhaveSelectedTheFollowingAccounts");
                this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
                this.showDefaultTransctionAccount(viewModel);
                this.setSelectedSkin("flxSetDefaultAccount");
            }
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show UI according to the default account
         * @param {JSON} viewModel list of default accounts for different transactions
         */
        showDefaultTransctionAccount: function(viewModel) {
            var isCombinedUser = applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true";
            this.showViews(["flxDefaultTransactionAccountWrapper"]);
            var count = 0;
            for (var keys in viewModel) {
                if (viewModel[keys] !== 'None') count++;
            }
            if (count !== 0) {
                this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
                this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
            } else {
                this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
                this.view.settings.flxDefaultAccountsSelected.setVisibility(false);
            }
            if (isCombinedUser) {
                CommonUtilities.setText(this.view.settings.lblBillPayValue, viewModel.defaultBillPayAccount, CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(this.view.settings.lblCheckDepositValue, viewModel.defaultCheckDepositAccount, CommonUtilities.getaccessibilityConfig());
                this.getFontIconsForAccount(viewModel);
            } else {
                CommonUtilities.setText(this.view.settings.lblTransfersValue, viewModel.defaultTransferAccount, CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(this.view.settings.lblBillPayValue, viewModel.defaultBillPayAccount, CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(this.view.settings.lblPayAPersonValue, viewModel.defaultP2PAccount, CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(this.view.settings.lblCheckDepositValue, viewModel.defaultCheckDepositAccount, CommonUtilities.getaccessibilityConfig());
            }
        },

        /**
         * Method to get Font Icons For Account
         * @param {JSON} fromAccount Account information
         * @returns void
         */
        getFontIconsForAccount: function(viewModel) {
            this.view.settings.lblTransfersIcon.setVisibility(false);
            this.view.settings.lblBillPayIcon.setVisibility(false);
            this.view.settings.lblPayAPersonIcon.setVisibility(false);
            this.view.settings.lblCheckDepositIcon.setVisibility(false);
            this.accountDataStore = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.accounts;

            if (kony.sdk.isNullOrUndefined(this.accountDataStore)) return;
            if (kony.sdk.isNullOrUndefined(viewModel)) return;

            var businessAccountList = [];
            var personalAccountList = [];
            var accountData = cloneJSON(this.accountDataStore);
            var len = accountData.length;
            var i;

            for (i = 0; i < len; i++) {
                if (accountData[i]["isBusinessAccount"] === "true") {
                    businessAccountList.push(cloneJSON(accountData[i]["accountName"]));
                    businessAccountList.push(cloneJSON(accountData[i]["nickName"]));
                } else {
                    personalAccountList.push(cloneJSON(accountData[i]["accountName"]));
                    personalAccountList.push(cloneJSON(accountData[i]["nickName"]));
                }
            }

            if (!kony.sdk.isNullOrUndefined(viewModel.defaultTransferAccount)) {
                this.view.settings.lblTransfersIcon.setVisibility(true);
                this.view.settings.lblTransfersIcon.text = this.getAcccountIconByType(businessAccountList, personalAccountList, viewModel.defaultTransferAccount);
            }
            if (!kony.sdk.isNullOrUndefined(viewModel.defaultBillPayAccount)) {
                this.view.settings.lblBillPayIcon.setVisibility(true);
                this.view.settings.lblBillPayIcon.text = this.getAcccountIconByType(businessAccountList, personalAccountList, viewModel.defaultBillPayAccount);
            }
            if (!kony.sdk.isNullOrUndefined(viewModel.defaultP2PAccount)) {
                this.view.settings.lblPayAPersonIcon.setVisibility(true);
                this.view.settings.lblPayAPersonIcon.text = this.getAcccountIconByType(businessAccountList, personalAccountList, viewModel.defaultP2PAccount);
            }
            if (!kony.sdk.isNullOrUndefined(viewModel.defaultCheckDepositAccount)) {
                this.view.settings.lblCheckDepositIcon.setVisibility(true);
                this.view.settings.lblCheckDepositIcon.text = this.getAcccountIconByType(businessAccountList, personalAccountList, viewModel.defaultCheckDepositAccount);
            }
        },

        /**
         * Method to get account Icon By Type
         * @param {JSON} fromAccount Account information
         * @returns void
         */
        getAcccountIconByType: function(businessAccountList, personalAccountList, accountName) {
            if (businessAccountList.includes(accountName)) return "r";
            if (personalAccountList.includes(accountName)) return "s";
            return "";
        },

        /**
         * Method to get account in Format AccountName-XXXX1234
         * @param {JSON} fromAccount Account information
         * @returns Array with account id and display name in format AccountName-XXXX1234
         */
        generateFromAccounts: function(fromAccount) {
            var getAccountDisplayName = function(fromAccount) {
                return fromAccount.accountName + '-XXXX' + fromAccount.accountID.slice(-4);
            };
            return [fromAccount.accountID, getAccountDisplayName(fromAccount)];
        },
        /**
         * Method to show the list of accounts
         * @param {Object} viewModel Model containing list of accounts related to transactions
         */
        showAccountsList: function(viewModel) {
            var isCombinedUser = applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true";
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            var isSMEUser = applicationManager.getConfigurationManager().getConfigurationValue('isSMEUser') === "true";
            var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
            this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
            this.view.settings.flxDefaultTransctionAccounts.setVisibility(true);
            this.view.settings.flxDefaultTransactionButtons.setVisibility(true);
            this.view.settings.txtDefaultTransactionAccountWarning.setVisibility(false);
            /*if(!isCombinedUser&&!isSMEUser){
              this.view.settings.lbxTransfers.setVisibility(true);
              this.view.settings.lbxBillPay.setVisibility(true);
              this.view.settings.lbxPayAPreson.setVisibility(true);
              this.view.settings.lbxCheckDeposit.setVisibility(true);
              this.view.settings.lbxTransfers.masterData = viewModel.TransfersAccounts.map(this.generateFromAccounts);
              this.view.settings.lbxTransfers.selectedKey = viewModel.defaultTransfersAccounts;
              this.view.settings.lbxBillPay.masterData = viewModel.BillPayAccounts.map(this.generateFromAccounts);
              this.view.settings.lbxBillPay.selectedKey = viewModel.defaultBillPayAccounts;
              this.view.settings.lbxPayAPreson.masterData = viewModel.P2PAccounts.map(this.generateFromAccounts);
              this.view.settings.lbxPayAPreson.selectedKey = viewModel.defaultP2PAccounts;
              this.view.settings.lbxCheckDeposit.masterData = viewModel.CheckDepositAccounts.map(this.generateFromAccounts);
              this.view.settings.lbxCheckDeposit.selectedKey = viewModel.defaultCheckDepositAccounts;
            }
            else {*/
            this.view.settings.flxBillPaySelectedValue.setVisibility(true);
            this.view.settings.flxBillPayAccounts.setVisibility(false);
            this.view.settings.flxBillPaySelectedValue.onClick = this.onBillPayClick.bind(this);
            this.billPayAccounts = [];
            if (!(kony.sdk.isNullOrUndefined(viewModel.BillPayAccounts))) {
                this.setBillPayAccountsData(viewModel.BillPayAccounts);
            }
            this.view.settings.flxCheckDepositSelectedValue.setVisibility(true);
            this.view.settings.flxCheckDepositAccounts.setVisibility(false);
            this.view.settings.flxCheckDepositSelectedValue.onClick = this.onCheckDepositClick.bind(this);
            //this.view.settings.lblCheckDepositAccountIcon.isVisible = isCombinedUser?true:false;
            this.view.settings.lblCheckDepositAccountIcon.isVisible = this.profileAccess === "both" ? true : false;
            this.checkDepositAccounts = [];
            if (!(kony.sdk.isNullOrUndefined(viewModel.CheckDepositAccounts))) {
                this.setCheckDepositAccountsData(viewModel.CheckDepositAccounts);

            }
            this.defaultBillPayAccounts = viewModel.defaultBillPayAccounts;
            this.defaultCheckDepositAccounts = viewModel.defaultCheckDepositAccounts;
            //this.view.settings.lblBillPayAccountName.text = this.view.settings.lblBillPayValue.text + '-XXXX' + viewModel.defaultBillPayAccounts.slice(-4);
            //this.view.settings.lblCheckDepositAccountName.text = this.view.settings.lblCheckDepositValue.text + '-XXXX' + viewModel.defaultCheckDepositAccounts.slice(-4);
            (this.defaultBillPayAccounts !== "None" && this.defaultBillPayAccounts !== 'undefined') ? this.view.settings.lblBillPayAccountName.text = this.view.settings.lblBillPayValue.text + '-XXXX' + viewModel.defaultBillPayAccounts.slice(-4) : this.view.settings.lblBillPayAccountName.text = "None";
            if (this.view.settings.lblBillPayValue.text === "" || this.view.settings.lblBillPayValue.text == null) { 
                this.view.settings.lblBillPayValue.text = "None";
            }
            (this.defaultCheckDepositAccounts !== "None" && this.defaultCheckDepositAccounts !== 'undefined') ? this.view.settings.lblCheckDepositAccountName.text = this.view.settings.lblCheckDepositValue.text + '-XXXX' + viewModel.defaultCheckDepositAccounts.slice(-4) : this.view.settings.lblCheckDepositAccountName.text = "None";
            if (this.view.settings.lblCheckDepositValue.text === "" || this.view.settings.lblCheckDepositValue.text == null) { 
                this.view.settings.lblCheckDepositValue.text = "None"; 
            }
            this.view.settings.lblBillPayAccountIcon.text = accountsModule.presentationController.fetchIsBusinessAccount(this.defaultBillPayAccounts) === "true" ? "r" : "s";
            this.view.settings.lblCheckDepositAccountIcon.text = accountsModule.presentationController.fetchIsBusinessAccount(this.defaultCheckDepositAccounts) === "true" ? "r" : "s";
            // }
            this.view.settings.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },

        onBillPayClick: function() {
            this.view.settings.flxBillPayAccounts.setVisibility(!this.view.settings.flxBillPayAccounts.isVisible);
        },
        onCheckDepositClick: function() {
            this.view.settings.flxCheckDepositAccounts.setVisibility(!this.view.settings.flxCheckDepositAccounts.isVisible);
        },

        setBillPayAccountsData: function(accounts) {
            var isCombinedUser = applicationManager.getConfigurationManager().isCombinedUser === "true";
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            var dataMap = {
                "lblDefaultAccountIcon": "lblDefaultAccountIcon",
                "lblDefaultAccountName": "lblDefaultAccountName",
                "accountId": "accountId",
                "flxAccountRoleType": "flxAccountRoleType",
                "lblAccountRoleType": "lblAccountRoleType",
                "lblAccountTypeHeader": "lblAccountTypeHeader",
                "flxDefaultAccountsHeader": "flxDefaultAccountsHeader"
            };
            this.billPayAccounts = accounts;
            this.view.settings.segBillPayAccounts.widgetDataMap = dataMap;
            //this.view.settings.lblBillPayAccountIcon.isVisible = isCombinedUser?true:false;
            if (accounts.length !== 0) {
                var data = this.getDropdownDataWithSections(accounts);
                this.view.settings.segBillPayAccounts.setData(data);
                this.view.settings.segBillPayAccounts.rowTemplate = "flxRowDefaultAccounts";
                this.view.settings.segBillPayAccounts.onRowClick = this.onBillPayAccountSelect.bind(this);
            } else {
                this.view.settings.flxBillPayValue.setVisibility(true);
                //this.view.settings.flxBillPayAccountInfo.setVisibility(false);
                this.view.settings.flxBillPaySelectedValue.setVisibility(false);
            }
            this.view.settings.lblBillPayAccountIcon.isVisible = this.profileAccess === "both" ? true : false;
            this.view.settings.forceLayout();
        },
        setCheckDepositAccountsData: function(accounts) {
            //var isCombinedUser = applicationManager.getConfigurationManager().isCombinedUser==="true";
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            var data;
            var dataMap = {
                "lblDefaultAccountIcon": "lblDefaultAccountIcon",
                "lblDefaultAccountName": "lblDefaultAccountName",
                "accountId": "accountId",
                "flxAccountRoleType": "flxAccountRoleType",
                "lblAccountRoleType": "lblAccountRoleType",
                "lblAccountTypeHeader": "lblAccountTypeHeader",
                "flxDefaultAccountsHeader": "flxDefaultAccountsHeader"
            };
            this.checkDepositAccounts = accounts;
            this.view.settings.segCheckDepositAccounts.widgetDataMap = dataMap;
            if (accounts.length !== 0) {
                data = this.getDropdownDataWithSections(accounts);
                this.view.settings.segCheckDepositAccounts.setData(data);
                this.view.settings.segCheckDepositAccounts.rowTemplate = "flxRowDefaultAccounts";
                this.view.settings.segCheckDepositAccounts.onRowClick = this.onCheckDepositAccountSelect.bind(this);
            } else {

                this.view.settings.flxCheckDepositValue.setVisibility(true);
                this.view.settings.flxCheckDepositSelectedValue.setVisibility(false);
            }

            //this.view.settings.lblCheckDepositAccountIcon.isVisible = isCombinedUser?true:false;
            this.view.settings.lblCheckDepositAccountIcon.isVisible = this.profileAccess === "both" ? true : false;
            this.view.settings.forceLayout();
        },
        onBillPayAccountSelect: function() {
            //var isCombinedUser = applicationManager.getConfigurationManager().isCombinedUser==="true";
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            var account = this.view.settings.segBillPayAccounts.selectedRowItems[0];
            this.view.settings.lblBillPayAccountIcon.text = account.lblDefaultAccountIcon.text;
            this.view.settings.lblBillPayAccountName.text = account.lblDefaultAccountName;
            //this.view.settings.lblBillPayAccountIcon.isVisible = isCombinedUser?true:false;
            this.view.settings.lblBillPayAccountIcon.isVisible = this.profileAccess === "both" ? true : false;
            this.view.settings.flxBillPayAccounts.setVisibility(false);
        },
        onCheckDepositAccountSelect: function() {
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            //var isCombinedUser = applicationManager.getConfigurationManager().isCombinedUser==="true";
            var account = this.view.settings.segCheckDepositAccounts.selectedRowItems[0];
            this.view.settings.lblCheckDepositAccountIcon.text = account.lblDefaultAccountIcon.text;
            this.view.settings.lblCheckDepositAccountName.text = account.lblDefaultAccountName;
            //this.view.settings.lblCheckDepositAccountIcon.isVisible = isCombinedUser?true:false;
            this.view.settings.lblCheckDepositAccountIcon.isVisible = this.profileAccess === "both" ? true : false;
            this.view.settings.flxCheckDepositAccounts.setVisibility(false);
        },

        getUsernameRules: function(data) {
            FormControllerUtility.hideProgressBar(this.view);
            if (data) {
                this.view.settings.rtxRulesUsername.text = data.usernamepolicies.content; //
            } else {
                CommonUtilities.showServerDownScreen();
            }
        },
        responsiveViews: {},
        initializeResponsiveViews: function() {
            this.responsiveViews["flxProfileWrapper"] = this.isViewVisible("flxProfileWrapper");
            this.responsiveViews["flxNameChangeRequestWrapper"] = this.isViewVisible("flxNameChangeRequestWrapper");
            this.responsiveViews["flxPhoneNumbersWrapper"] = this.isViewVisible("flxPhoneNumbersWrapper");
            this.responsiveViews["flxEditPhoneNumbersWrapper"] = this.isViewVisible("flxEditPhoneNumbersWrapper");
            this.responsiveViews["flxChangeLanguageWrapper"] = this.isViewVisible("flxChangeLanguageWrapper");
            this.responsiveViews["flxAddPhoneNumbersWrapper"] = this.isViewVisible("flxAddPhoneNumbersWrapper");
            this.responsiveViews["flxEmailWrapper"] = this.isViewVisible("flxEmailWrapper");
            this.responsiveViews["flxEditEmailWrapper"] = this.isViewVisible("flxEditEmailWrapper");
            this.responsiveViews["flxAddNewEmailWrapper"] = this.isViewVisible("flxAddNewEmailWrapper");
            this.responsiveViews["flxAddressesWrapper"] = this.isViewVisible("flxAddressesWrapper");
            this.responsiveViews["flxAddNewAddressWrapper"] = this.isViewVisible("flxAddNewAddressWrapper");
            this.responsiveViews["flxEditAddressWrapper"] = this.isViewVisible("flxEditAddressWrapper");
            this.responsiveViews["flxAccountsWrapper"] = this.isViewVisible("flxAccountsWrapper");
            this.responsiveViews["flxEditAccountsWrapper"] = this.isViewVisible("flxEditAccountsWrapper");
            this.responsiveViews["flxDefaultTransactionAccountWrapper"] = this.isViewVisible("flxDefaultTransactionAccountWrapper");
            this.responsiveViews["flxUsernameAndPasswordWrapper"] = this.isViewVisible("flxUsernameAndPasswordWrapper");
            this.responsiveViews["flxEditUsernameWrapper"] = this.isViewVisible("flxEditUsernameWrapper");
            this.responsiveViews["flxAcknowledgementWrapper"] = this.isViewVisible("flxAcknowledgementWrapper");
            this.responsiveViews["flxEditPasswordWrapper"] = this.isViewVisible("flxEditPasswordWrapper");
            this.responsiveViews["flxEditPasswordWrapperComp"] = this.isViewVisible("flxEditPasswordWrapperComp");
            this.responsiveViews["flxSecuritySettingVerificationWrapper"] = this.isViewVisible("flxSecuritySettingVerificationWrapper");
            this.responsiveViews["flxUsernameVerificationWrapper"] = this.isViewVisible("flxUsernameVerificationWrapper");
            this.responsiveViews["flxUsernameVerificationWrapper2"] = this.isViewVisible("flxUsernameVerificationWrapper2");
            this.responsiveViews["flxEditSecuritySettingsWrapper"] = this.isViewVisible("flxEditSecuritySettingsWrapper");
            this.responsiveViews["flxAnswerSecurityQuestionsWrapper"] = this.isViewVisible("flxAnswerSecurityQuestionsWrapper");
            this.responsiveViews["flxSecureAccessCodeWrapper"] = this.isViewVisible("flxSecureAccessCodeWrapper");
            this.responsiveViews["flxTransactionalAndPaymentsAlertsWrapper"] = this.isViewVisible("flxTransactionalAndPaymentsAlertsWrapper");
            this.responsiveViews["flxConsentManagementWrapper"] = this.isViewVisible("flxConsentManagementWrapper");
            this.responsiveViews["flxManageAccountAccessWrapper"] = this.isViewVisible("flxManageAccountAccessWrapper");
            this.responsiveViews["flxEBankingAccessWrapper"] = this.isViewVisible("flxEBankingAccessWrapper");
        },
        isViewVisible: function(container) {
            return this.view.settings[container].isVisible;
        },
        onBreakpointChange: function(width) {
            var scope = this;
            orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.customheader.onBreakpointChangeComponent(width);
            //               this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup,width);
            //               this.view.CustomChangeLanguagePopup.onBreakpointChangeComponent(scope.view.CustomChangeLanguagePopup,width);
            this.setupFormOnTouchEnd(width);
            var views = Object.keys(this.responsiveViews);
            views.forEach(function(e) {
                scope.view.settings[e].isVisible = scope.responsiveViews[e];
            });
        },
        setupFormOnTouchEnd: function(width) {
            if (width == 640) {
                this.view.onTouchEnd = function() {}
                this.nullifyPopupOnTouchStart();
            } else {
                if (width == 1024) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else {
                    this.view.onTouchEnd = function() {
                        hidePopups();
                    }
                }
                var userAgent = kony.os.deviceInfo().userAgent;
                if (userAgent.indexOf("iPad") != -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                }
            }
        },
        nullifyPopupOnTouchStart: function() {},
        /**
         * Method to show security questions screen
         * @param {}
         */
        showSecurityQuestionsScreen: function() {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(this.view);
            this.collapseAll();
            this.expand(this.view.settings.flxSecuritySettingsSubMenu);
            this.view.settings.lblSecuritySettingsCollapse.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
            this.view.customheader.customhamburger.activateMenu("Settings", "Security Settings");
            scopeObj.view.settings.flxErrorEditSecuritySettings.setVisibility(false);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
            scopeObj.setSelectedSkin("flxSecurityQuestions");
            scopeObj.view.settings.btnEditSecuritySettingsCancel.onClick = function() {
                scopeObj.view.settings.flxErrorEditSecuritySettings.setVisibility(false);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
                scopeObj.setSelectedSkin("flxSecurityQuestions");
                scopeObj.selectedQuestions = {
                    ques: ["Select a Question", "Select a Question", "Select a Question", "Select a Question", "Select a Question"],
                    key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
                };
                scopeObj.selectedQuestionsTemp = {
                    securityQuestions: [],
                    flagToManipulate: []
                };
            };
            scopeObj.view.settings.tbxEnterOTP.onKeyUp = function() {
                scopeObj.checkOTP();
            };
            scopeObj.showOTPAction();
        },

        prepareApprovalMatrixSubmenus: function(accounts) {
            if (accounts != null && accounts.length > 0) {
                this.addOrRemoveApprovalMatrixSubmenus(accounts.length);
                this.mapDataToSubmenus(accounts);
            } else {
                this.view.settings.flxApprovalMatrix.isVisible = false;
            }
        },

        addOrRemoveApprovalMatrixSubmenus: function(numberOfAccounts) {

            var existingChildrenLength = this.view.settings.flxApprovalMatrixSubMenu.widgets().length - 1;

            if (existingChildrenLength > numberOfAccounts) {
                for (var i = numberOfAccounts + 1; i <= existingChildrenLength; i++) {
                    this.view.settings.flxApprovalMatrixSubMenu.removeAt(i);
                    widgetsMap[4]["subMenu"]["children"].removeAt(i);
                }

            }
            if (existingChildrenLength < numberOfAccounts) {
                for (var i = existingChildrenLength; i < numberOfAccounts; i++) {
                    var id = "Account" + i;
                    var flxSubmenuItem = this.view.settings.FlxAMatrix.clone(id);
                    this.view.settings.flxApprovalMatrixSubMenu.add(flxSubmenuItem);

                    var config = {
                        "configuration": "",
                        "widget": flxSubmenuItem.id
                    }

                    widgetsMap[4]["subMenu"]["children"].push(config);
                }
            }

            this.view.settings.flxApprovalMatrixSubMenu.FlxAMatrix.isVisible = false;
        },

        mapDataToSubmenus: function(accounts) {
            var scopeObj = this;
            var subMenus = scopeObj.view.settings.flxApprovalMatrixSubMenu.widgets();
            for (var i = 1; i < subMenus.length; i++) {
                var submenu = this.view.settings[subMenus[i].id];
                var subMenuItem = submenu.widgets();
                var lblAMatrix = subMenuItem[1].id;
                //format to be modified
                scopeObj.view.settings[lblAMatrix].text = accounts[i - 1]["displayName"];
                scopeObj.view.settings[lblAMatrix].toolTip = accounts[i - 1]["displayName"];
                submenu.onClick = scopeObj.showApprovalMatrixUIForSelectedAccount.bind(scopeObj, submenu.id, accounts[i - 1]["Account_id"], accounts[i - 1]["displayName"]);
            }
        },

        getApprovalMatrixOfFirstAccount: function(companyAccounts) {
            if (this.view.settings.flxApprovalMatrixSubMenu.widgets().length >= 1) {
                if (kony.sdk.isNullOrUndefined(companyAccounts) || companyAccounts.length == 0) {
                    this.view.settings.flxApprovalMatrixSubMenu.widgets()[0].LblAMatrix.text = kony.i18n.getLocalizedString("i18n.konybb.manageUser.noAccountAccess");
                    this.view.settings.flxApprovalMatrixAccountWrapper.isVisible = true;
                    this.view.settings.segApprovalMatrix.isVisible = false;
                    this.view.settings.flxApprovalMatrix.isVisible = true;
                    this.view.settings.flxNoApprovalMatrixData.isVisible = true;
                    this.view.settings.lblNoApprovalMatrixData.isVisible = true;
                    this.view.settings.lblNoApprovalMatrixData.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.noOrganisationAccounts");
                } else {
                    this.view.settings.lblNoApprovalMatrixData.isVisible = false;
                    var account1 = this.view.settings.flxApprovalMatrixSubMenu.widgets()[1];
                    this.view.settings[account1.id].onClick();
                }
            }
        },

        showApprovalMatrixUIForSelectedAccount: function(selectedSubmenuID, accountNumber, accountName) {
            var scopeObj = this;
            scopeObj.selectedSubmenuID = selectedSubmenuID;
            this.collapseAll();
            scopeObj.expand(scopeObj.view.settings.flxApprovalMatrixSubMenu);
            scopeObj.setSelectedSkin(selectedSubmenuID);
            scopeObj.view.customheader.customhamburger.activateMenu("Settings", "Approval Matrix");
            scopeObj.currentSelectAccountInApprovalMatrix = accountName;
            scopeObj.view.settings.TabsHeaderNewCopy.btnTab1.onClick = scopeObj.showPerTrasactionApprovalMatrix.bind(scopeObj, accountNumber);
            scopeObj.view.settings.TabsHeaderNewCopy.btnTab2.onClick = scopeObj.showDailyApprovalMatrix.bind(scopeObj, accountNumber);
            scopeObj.view.settings.TabsHeaderNewCopy.btnTab3.onClick = scopeObj.showWeeklyApprovalMatrix.bind(scopeObj, accountNumber);

            scopeObj.view.settings.TabsHeaderNewCopy.btnTab1.onClick();
        },

        showApprovalMatrixAfterSuccessfulEditing: function(approvalMatrixUpdated) {
            var scopeObj = this;

            if (approvalMatrixUpdated.isSuccessful) {
                this.view.flxEditApprovalMatrixWrapper.setVisibility(false);
                this.view.flxSettingsWrapper.setVisibility(true);
                this.showErrorMessageOnAppprovalMatrixEdit(false, "");

                this.collapseAll();
                scopeObj.expand(scopeObj.view.settings.flxApprovalMatrixSubMenu);
                scopeObj.setSelectedSkin(scopeObj.selectedSubmenuID);
                scopeObj.view.customheader.customhamburger.activateMenu("Settings", "Approval Matrix");

                switch (scopeObj.selectedTab) {
                    case "ONE":
                        scopeObj.view.settings.TabsHeaderNewCopy.btnTab1.onClick();
                        break;
                    case "TWO":
                        scopeObj.view.settings.TabsHeaderNewCopy.btnTab2.onClick();
                        break;
                    case "THREE":
                        scopeObj.view.settings.TabsHeaderNewCopy.btnTab3.onClick();
                }
            } else {
                this.showErrorMessageOnAppprovalMatrixEdit(true, approvalMatrixUpdated.errorMessage);
            }
            FormControllerUtility.hideProgressBar(scopeObj.view);
        },

        showPerTrasactionApprovalMatrix: function(accountNumber) {
            var scopeObj = this;
            scopeObj.selectedTab = "ONE";
            this.view.settings.TabsHeaderNewCopy.focusTab(1);
            scopeObj.fetchApprovalMatrixBasedOnAccountIdAndLimitTypeId(accountNumber, "MAX_TRANSACTION_LIMIT");

        },
        showDailyApprovalMatrix: function(accountNumber) {
            var scopeObj = this;
            scopeObj.selectedTab = "TWO";
            this.view.settings.TabsHeaderNewCopy.focusTab(2);
            scopeObj.fetchApprovalMatrixBasedOnAccountIdAndLimitTypeId(accountNumber, "DAILY_LIMIT");

        },
        showWeeklyApprovalMatrix: function(accountNumber) {
            var scopeObj = this;
            scopeObj.selectedTab = "THREE";
            this.view.settings.TabsHeaderNewCopy.focusTab(3);
            scopeObj.fetchApprovalMatrixBasedOnAccountIdAndLimitTypeId(accountNumber, "WEEKLY_LIMIT");
        },

        showApprvalMatrixWrapper: function(matrixResult) {
            var finalMatrixPerTransaction = this.prepareSegmentDataForApprovalMatrix(matrixResult);
            this.showApprovalMatrix(finalMatrixPerTransaction);
        },

        fetchApprovalMatrixBasedOnAccountIdAndLimitTypeId: function(accountNumber, limitTypeId) {
            FormControllerUtility.showProgressBar(this.view);
            var profilePresentationController = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").
            presentationController;
            profilePresentationController.fetchApprovalMatrixBasedOnAccountIdAndLimitTypeId(accountNumber, limitTypeId);
        },

        showApprovalMatrix: function(segmentData, errorMessage) {
            var widgetDataMap = {
                "lblFeatureAction": "featureActionName",
                "btnEditMatrix": "btnEditMatrix",
                "lblSeparator1": "lblSeparator1",
                "lblSeparator2": "lblSeparator2",
                "lblSeparator3": "lblSeparator3",
                "lblApprovalLimits": "lblApprovalLimits",
                "lblApprovers": "lblApprovers",
                "lblApprovalRule": "lblApprovalRule",
                "lblApprovalLimitsValue": "range",
                "lblApproversValue": "approvers",
                "lblApprovalRuleValue": "rule",
                "lblNoApprovalRulesDefined": "noApprovalRulesDefined"
            };

            this.view.settings.segApprovalMatrix.widgetDataMap = widgetDataMap;
            this.view.settings.segApprovalMatrix.setData(segmentData);

            if (kony.sdk.isNullOrUndefined(segmentData) || segmentData.length <= 0) {
                this.view.settings.segApprovalMatrix.isVisible = false;
                this.view.settings.flxNoApprovalMatrixData.isVisible = true;
                var serviceFailure = !kony.sdk.isNullOrUndefined(errorMessage);
                this.view.settings.flxNoApprovalMatrixData.lblNoApprovalMatrixData.text =
                    serviceFailure == true ? errorMessage : kony.i18n.getLocalizedString("i18n.approvalMatrix.emptyListOfAprovalMatrix");
            } else {
                this.view.settings.segApprovalMatrix.isVisible = true;
                this.view.settings.flxNoApprovalMatrixData.isVisible = false;
                this.view.settings.flxNoApprovalMatrixData.lblNoApprovalMatrixData.text = "";
            }
            this.view.InfoIconPopups.isVisible = false;
            this.showViews(["flxApprovalMatrixAccountWrapper"]);
            this.view.settings.lblApprovalMatrixCollapse.text = "P";
            this.view.settings.lblApprovalMatrixCollapse.toolTip = "Collapse";
            CommonUtilities.setText(this.view.settings.lblApprovalMatrixCollapse, "Collapse", CommonUtilities.getaccessibilityConfig());
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },

        prepareSegmentDataForApprovalMatrix: function(approvalMatrix) {
            var self = this;
            var data = [];
            var canEdit = applicationManager.getConfigurationManager().checkUserPermission("APPROVAL_MATRIX_MANAGE");
            for (var i = 0; i < approvalMatrix.actions.length; i++) {
                var editData = {
                    "accountNumber": approvalMatrix.accountId,
                    "featureAction": {
                        featureActionId: approvalMatrix.actions[i]["actionId"],
                        featureActionName: approvalMatrix.actions[i]["actionName"],
                        featureActionDescription: approvalMatrix.actions[i]["actionDescription"],
                        featureActionLimit: null
                    },
                    "frequency": approvalMatrix.limitTypeId,
                    "limitStatments": approvalMatrix.actions[i]["limits"]
                };



                var sectionData = [];
                var onEditOfFeatureAction = {
                    isVisible: canEdit,
                    text: "Edit",
                    onClick: self.onEditOfFeatureAction.bind(self, editData)
                }
                var sectionHeader = {
                    "featureActionName": approvalMatrix.actions[i]["actionName"],
                    "btnEditMatrix": onEditOfFeatureAction,
                    "lblSeparator1": "A",
                    "lblSeparator2": "A",
                    "lblSeparator3": "A",
                    "lblApprovalLimits": "Approval Limits",
                    "lblApprovers": "Approver/s",
                    "lblApprovalRule": "Approval Rule",
                };
                sectionData.push(sectionHeader, self.prepareApprovalMatrixViewRows(approvalMatrix.actions[i]["limits"]));
                data.push(sectionData);
            }

            return data;
        },

        getProcessedFeatureActionName: function(actionname) {
            if (kony.application.getCurrentBreakpoint() === 1024) {
                if (actionname.length > 50)
                    return actionname.substring(0, 50) + "...";
            }
            return actionname;
        },

        prepareApprovalMatrixViewRows: function(limits) { //noApprovalRulesDefined to be added for first time scenario
            var scopeObj = this;
            var rows = [];

            if (limits.length == 1 && limits[0]["numberOfApprovals"] == 0) {
                rows.push({
                    range: "",
                    approvers: {
                        text: "",
                        toolTip: ""
                    },
                    rule: "",
                    noApprovalRulesDefined: {
                        isVisible: true
                    }
                });

                return rows;
            }

            for (var i = 0; i < limits.length; i++) {
                var approversExtendedList = scopeObj.getApprovers(limits[i]["approvers"]);
                var approversShortendList = approversExtendedList.length > 30 ? approversExtendedList.substring(0, 30) + "..." : approversExtendedList;
                var row = {
                    range: scopeObj.calculateRange(limits[i]["lowerlimit"], limits[i]["upperlimit"]),
                    approvers: {
                        text: approversShortendList,
                        toolTip: approversExtendedList
                    },
                    rule: kony.sdk.isNullOrUndefined(limits[i]["approvalRuleName"]) ? "No Approval Rule set" : limits[i]["approvalRuleName"],
                    noApprovalRulesDefined: {
                        isVisible: false
                    }
                };

                rows.push(row);
            }

            return rows;
        },

        calculateRange: function(lowerLimit, upperLimit) {
            var result = "";
            var formattedLowerLimit = applicationManager.getFormatUtilManager().formatAmount(lowerLimit);
            var formattedUpperLimit = applicationManager.getFormatUtilManager().formatAmount(upperLimit);

            if (lowerLimit != -1 && upperLimit != -1) {
                result = kony.i18n.getLocalizedString("i18n.common.currencySymbol") + formattedLowerLimit + " - " + kony.i18n.getLocalizedString("i18n.common.currencySymbol") + formattedUpperLimit;
            } else if (lowerLimit == -1 && upperLimit == -1) {
                result = "Above " + 1;
            } else if (lowerLimit == -1) {
                result = "Up to " + kony.i18n.getLocalizedString("i18n.common.currencySymbol") + formattedUpperLimit;
            } else if (upperLimit == -1) {
                result = "Above " + kony.i18n.getLocalizedString("i18n.common.currencySymbol") + formattedLowerLimit;
            }

            return result;
        },

        getApprovers: function(approvalsList) {
            var approvers = approvalsList[0]["approverName"];
            for (var i = 1; i < approvalsList.length; i++) {
                approvers = approvers + ", " + approvalsList[i]["approverName"];
            }

            if (approvers === "") {
                approvers += "No Approvers set";
            }
            return approvers;
        },

        getApproversIds: function(approvalsList) {
            var approvers = [];

            for (var i = 0; i < approvalsList.length; i++) {
                approvers.push(approvalsList[i]["approverId"]);
            }
            return approvers;
        },

        onEditOfFeatureAction: function(editData) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            scopeObj.prevApprovalMatrixData = {
                accountId: editData["accountNumber"],
                actionId: editData["featureAction"]["featureActionId"],
                limitTypeId: editData["frequency"],
                limits: scopeObj.getLimitsFromEditData(editData["limitStatments"])
            };
            var profilePresentationController = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").
            presentationController;
            profilePresentationController.getOrganizationLimitForGivenFeatureAction(editData);

        },

        getLimitsFromEditData: function(limitStatments) {
            var limits = [];
            var approversList = [];

            for (var i = 0; i < limitStatments.length; i++) {
                var limitStatment = limitStatments[i];

                for (var j = 0; j < limitStatment["approvers"].length; j++) {
                    approversList.push({
                        "approverId": limitStatment["approvers"][j]["approverId"]
                    });
                }

                var deformattedLower = applicationManager.getFormatUtilManager().deFormatAmount(limitStatment["lowerlimit"]);
                var deformattedUpper = applicationManager.getFormatUtilManager().deFormatAmount(limitStatment["upperlimit"]);
                var limit = {
                    lowerlimit: deformattedLower,
                    upperlimit: deformattedUpper,

                    approvalruleId: limitStatment["approvalRuleId"],

                    approvers: approversList
                }
                limits.push(limit);
            }
            return limits;
        },

        /**
          Entry method of edit approval matrix flow ( after fetching aproval limit )
        **/
        navigateToEditApprovalMatrix: function(EditModel) {
            var scopeObj = this;
            this.view.flxErrorMessage.isVisible = false;
            this.editApprovalMatrixLimits(EditModel.editData);
            this.createApprovalMatrixPopupUI(EditModel.editData);
            FormControllerUtility.hideProgressBar(scopeObj.view);
        },

        /**
              Method used to get list of approval rules
            **/
        getApprovalRules: function() {
            var rules = CommonUtilities.getApprovalRules();

            for (var i = 0; i < rules.length; i++) {
                rules[i].imgRadioButton = "radio_btn_inactive.png";
                rules[i].lblSeperator = "-";
            }

            return rules;
        },

        /**
          Method is used to get list of approvers
        **/
        getListOfApprovers: function(currentState, widget, context) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            var profilePresentationController = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").
            presentationController;
            var requestParam = {
                "accountId": currentState["accountNumber"],
                "actionId": currentState["featureAction"]["featureActionId"]
            };
            profilePresentationController.getAccountActionCustomerApproverList(requestParam, context);
        },

        /**
              Method used to close the popup related to approval matrix
            **/
        closeApprovalMatrixPopup: function() {
            this.view.flxApprovalMatrixPopup.isVisible = false;
            this.view.forceLayout();
        },

        /**
        This method used to create the popup that used while editing approval matix
        **/
        createApprovalMatrixPopupUI: function(currentState) {
            var scope = this;
            CommonUtilities.disableButton(this.view.formActionsNew.btnNext);
            this.view.formActionsNew.btnCancel.onClick = this.closeApprovalMatrixPopup.bind(this);
            this.view.flxCross.onClick = this.closeApprovalMatrixPopup.bind(this);
            this.view.EditActions.btnCancel.onClick = this.closeEditApprovalMatrixFlow.bind(this);
            this.view.EditActions.btnNext.onClick = this.saveApprovalMatrix.bind(this, currentState);
            this.view.segData.onRowClick = this.toggleSaveButtonOfAMPopup.bind(this);
        },

        /**
          Method toggles approval rule and approvers selection popup
        **/
        toggleSaveButtonOfAMPopup: function() {

            if (this.view.segData.selectedRowItems !== null && this.view.segData.selectedRowItems.length > 0) {
                CommonUtilities.enableButton(this.view.formActionsNew.btnNext);
            } else {
                CommonUtilities.disableButton(this.view.formActionsNew.btnNext);
            }
        },

        /**
          Method used to show the popup related to approvers selection on approval matrix edit page
        **/

        showAddRemoveApproversPopup: function(approversResponse) {
            var scopeObj = this;

            if (!kony.sdk.isNullOrUndefined(approversResponse.error)) {
                this.showErrorMessageOnAppprovalMatrixEdit(true, approversResponse.error);
                FormControllerUtility.hideProgressBar(scopeObj.view);
                return;
            }
            if (kony.sdk.isNullOrUndefined(approversResponse["approvers"]) || approversResponse["approvers"].length == 0) {
                this.showErrorMessageOnAppprovalMatrixEdit(true, kony.i18n.getLocalizedString("i18n.common.errorCodes.21000"));
                FormControllerUtility.hideProgressBar(scopeObj.view);
                return;
            }
            var header = kony.i18n.getLocalizedString("i18n.konybb.ApproversHeader");
            var selection = constants.SEGUI_MULTI_SELECT_BEHAVIOR;
            var selectionConfig = {
                imageIdentifier: "imgCheckbox",
                selectedStateImage: "active.png",
                unselectedStateImage: "inactive.png"
            };
            var template = "flxViewApprovers";
            var dataMap = {
                "imgCheckbox": "imgCheckbox",
                "lblName": "fullName",
                "lblRole": "role",
                "lblSeperator": "lblSeperator",
                "lblId": "id"
            }
            var data = approversResponse["approvers"];
            //sorting apporvers based on full name
            data.sort(function(a, b) {
                if (a["fullName"] > b["fullName"]) {
                    return 1;
                } else if (a["fullName"] < b["fullName"]) {
                    return -1;
                }
                return 0;
            });

            var selectedIndices = [];
            var ids = this.view.TabBodyNew.segTemplates.data[approversResponse["context"]["sectionIndex"]][1][approversResponse["context"]["rowIndex"]]["lblApproversValue"]["ids"];
            for (var i = 0; i < data.length; i++) {
                data[i]["lblSeperator"] = "-";
                data[i]["imgCheckbox"] = "imgCheckbox";
                data[i]["fullName"] = data[i]["firstname"] + " " + data[i]["lastname"];
                if (ids.includes(data[i]["id"]))
                    selectedIndices.push(i);
            }

            var selected = [
                [0, selectedIndices],
            ]

            this.view.formActionsNew.btnNext.onClick = scopeObj.setApprovers.bind(scopeObj, approversResponse["context"]);
            this.view.lblHeader.text = header;
            this.view.segData.selectionBehavior = selection;
            this.view.segData.selectionBehaviorConfig = selectionConfig;
            this.view.segData.rowTemplate = template;
            this.view.segData.widgetDataMap = dataMap;

            this.view.segData.setData(data);
            this.view.segData.selectedRowIndices = selected;
            selected[0][1].length > 0 ? CommonUtilities.enableButton(this.view.formActionsNew.btnNext) : CommonUtilities.disableButton(this.view.formActionsNew.btnNext);
            this.view.flxApprovalMatrixPopup.height = this.view.flxHeader.info.frame.height + this.view.flxContainer.info.frame.height + this.view.flxFooter.info.frame.height + "dp";
            this.view.flxApprovalMatrixPopup.isVisible = true;
            this.view.scrollToWidget(this.view.imgCross);
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(scopeObj.view);
        },

        /**
          Method is used to show popup for approval rule selection on approval matrix edit page
        **/
        showChangeRulePopup: function(currentState, widget, context) {
            var scopeObj = this;
            var maximumApprovalsPossible = this.view.TabBodyNew.segTemplates.data[context["sectionIndex"]][1][context["rowIndex"]]["lblApproversValue"]["ids"].length;

            if (maximumApprovalsPossible <= 0) return;

            var header = kony.i18n.getLocalizedString("i18n.konybb.ChangeApprovalRules");
            var selection = constants.SEGUI_SINGLE_SELECT_BEHAVIOR;
            var selectionConfig = {
                imageIdentifier: "imgRadioButton",
                selectedStateImage: "radiobtn_active.png",
                unselectedStateImage: "radio_btn_inactive.png"
            };
            var template = "flxApprovalRule";
            var dataMap = {
                "imgRadioButton": "imgRadioButton",
                "lblRule": "ruleName",
                "lblRuleId": "ruleId",
                "lblNumberOfApprovals": "numberOfApprovals",
                "lblSeperator": "lblSeperator"
            };
            if (!kony.sdk.isNullOrUndefined(this.view.TabBodyNew.segTemplates.data[context["sectionIndex"]][1][context["rowIndex"]]["lblRuleValue"]["ruleId"])) {
                var selectedId = this.view.TabBodyNew.segTemplates.data[context["sectionIndex"]][1][context["rowIndex"]]["lblRuleValue"]["ruleId"];
            }

            var data = this.getApprovalRules();
            data = this.getListOfPossibleRulesBasedOnNumberOfApprovers(data, maximumApprovalsPossible);
            var selectedRowIndex;
            if (!kony.sdk.isNullOrUndefined(selectedId)) {
                selectedRowIndex = this.getSelectedRule(data, selectedId);
            } else {
                selectedRowIndex = [0, 0];
            }

            CommonUtilities.disableButton(this.view.formActionsNew.btnNext);
            this.view.formActionsNew.btnNext.onClick = scopeObj.setApprovalRule.bind(scopeObj, context);
            this.view.lblHeader.text = header;
            this.view.segData.selectionBehavior = selection;
            this.view.segData.selectionBehaviorConfig = selectionConfig;
            this.view.segData.rowTemplate = template;
            this.view.segData.widgetDataMap = dataMap;
            this.view.segData.setData(data);
            this.view.segData.selectedRowIndex = selectedRowIndex;
            this.view.segData.selectedRowIndex.length > 0 ? CommonUtilities.enableButton(this.view.formActionsNew.btnNext) : CommonUtilities.disableButton(this.view.formActionsNew.btnNext);
            this.view.flxApprovalMatrixPopup.height = this.view.flxHeader.info.frame.height + this.view.flxContainer.info.frame.height + this.view.flxFooter.info.frame.height + "dp";
            this.view.flxApprovalMatrixPopup.isVisible = true;
            this.view.scrollToWidget(this.view.imgCross);
            this.AdjustScreen();
        },

        /**
          Method is used to return only those approval rules which are valid for selected number of approvers
        **/
        getListOfPossibleRulesBasedOnNumberOfApprovers: function(rules, numberOfApprovers) {
            if (numberOfApprovers <= 0) {
                return [];
            }
            var finalRules = [];
            for (var i = 0; i < rules.length; i++) {
                if (rules[i]["numberOfApprovals"] <= numberOfApprovers) {
                    finalRules.push(rules[i]);
                }
            }

            return finalRules;
        },

        /**
          Heleper method to get index of selected rule
        **/
        getSelectedRule: function(data, id) {
            var index = -1;
            for (var i = 0; i < data.length; i++) {
                if (data[i]["ruleId"] === id) {
                    index = i;
                }
            }

            return index == -1 ? [0, 0] : [0, index];
        },

        setApprovers: function(editRowData, widget) {
            var approverIds = [];
            var approvers = this.view.segData.selectedRowItems[0]["fullName"];
            approverIds.push(this.view.segData.selectedRowItems[0]["id"]);
            for (var i = 1; i < this.view.segData.selectedRowItems.length; i++) {
                approvers = approvers + ", " + this.view.segData.selectedRowItems[i]["fullName"];
                approverIds.push(this.view.segData.selectedRowItems[i]["id"]);
            }
            var data = this.view.TabBodyNew.segTemplates.data[editRowData["sectionIndex"]][1][editRowData["rowIndex"]];
            data.lblApproversValue = {
                text: approvers,
                ids: approverIds
            }
            var btnChangeRuleCofig = data.btnChangeRule;

            if (approverIds.length <= 0) {
                btnChangeRuleCofig.isVisible = false;
            } else {
                btnChangeRuleCofig.isVisible = true;
            }

            data.btnChangeRule = btnChangeRuleCofig;

            this.view.TabBodyNew.segTemplates.setDataAt(data, editRowData["rowIndex"], editRowData["sectionIndex"]);

            this.view.TabBodyNew.enableOrDisableProceedWidget();
            this.closeApprovalMatrixPopup();
        },

        setApprovalRule: function(editRowData, widget) {
            var data = this.view.TabBodyNew.segTemplates.data[editRowData["sectionIndex"]][1][editRowData["rowIndex"]];
            data.lblRuleValue = {
                text: this.view.segData.selectedRowItems[0]["ruleName"],
                ruleId: this.view.segData.selectedRowItems[0]["ruleId"],
                numberOfApprovals: this.view.segData.selectedRowItems[0]["numberOfApprovals"]
            }
            this.view.TabBodyNew.segTemplates.setDataAt(data, editRowData["rowIndex"], editRowData["sectionIndex"]);

            this.view.TabBodyNew.enableOrDisableProceedWidget();
            this.closeApprovalMatrixPopup();
        },

        /**
          Method returns the datamap for the editing the approval matrix
        **/

        getLimitsDataMap: function() {
            return {
                "btnDelete": "btnDelete",
                "btnAddorRemoveApprovers": "btnAddorRemoveApprovers",
                "btnChangeRule": "btnChangeRule",
                "lstType": "lstType",
                "lblApproversKey": "lblApproversKey",
                "lblRuleKey": "lblRuleKey",
                "flxTopSeperator": "flxTopSeperator",
                "flxBottomSeperator": "flxBottomSeperator",
                "flxSeperator": "flxSeperator",
                "flxUpperLimitContainer": "flxUpperLimitContainer",
                "flxLowerLimit": "flxLowerLimit",
                "imgInfoIcon": "imgInfoIcon",
                "flxInfoIcon": "flxInfoIcon",
                "lblLimitHeader": "lblLimitHeader",
                "lblApproversValue": "lblApproversValue",
                "lblRuleValue": "lblRuleValue",
                "lblType": "lblType",
                "tbxLowerLimit": "tbxLowerLimit",
                "tbxUpperLimit": "tbxUpperLimit",
                "lblCurrSymbol": "lblCurrSymbol",
                "lblUpperCurrSymbol": "lblUpperCurrSymbol",
                "lblAccountFrequency": "lblAccountFrequency",
                "imgIcon": "imgIcon",
                "lblHeadingSeperator": "lblHeadingSeperator"
            };
        },

        /**
              Method used to set the default values for editing approval matrix
            **/

        getDefaultValuesForLimits: function(currentState) {
            var scope = this;
            return {
                btnDelete: {
                    "text": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                    "onClick": function(eventobject, context) {
                        scope.view.TabBodyNew.removeRowAt(context.rowIndex, context.sectionIndex);
                        scope.updateRowHeaders();
                        scope.view.TabBodyNew.enableOrDisableProceedWidget();
                        scope.view.TabBodyNew.setMasterDataOfListBox();
                        scope.AdjustScreen();
                    }.bind(scope)
                },
                btnAddorRemoveApprovers: {
                    "text": kony.i18n.getLocalizedString("i18n.konybb.AddorRemoveApprovers"),
                    "onClick": scope.getListOfApprovers.bind(scope, currentState)
                },
                btnChangeRule: {
                    "text": kony.i18n.getLocalizedString("i18n.konybb.ChangeApprovalRule"),
                    "onClick": scope.showChangeRulePopup.bind(scope, currentState),
                    "isVisible": true
                },
                lblApproversKey: {
                    "text": kony.i18n.getLocalizedString("i18n.konybb.Approvers") + " :"
                },
                lblRuleKey: {
                    "text": kony.i18n.getLocalizedString("i18n.konybb.Rule") + " :"
                },
                lblTopSeperator: {
                    "text": "-"
                },
                lblBottomSeperator: {
                    "text": "-"
                },
                lblSeperator: {
                    "text": "-"
                },
                imgInfoIcon: {
                    "src": "info_grey.png"
                },
                flxInfoIcon: {
                    "onClick": function(eventobject, context) {
                        scope.displayApprovalRuleInfoPopup(kony.i18n.getLocalizedString("i18n.settings.approvalMatrix.changeRuleInfo"));
                    }.bind(scope)
                },
                lblType: {
                    "text": kony.i18n.getLocalizedString("i18n.common.Type") + " :"
                },
                lblCurrSymbol: {
                    "text": kony.i18n.getLocalizedString("i18n.common.currencySymbol")
                },
                lblUpperCurrSymbol: {
                    "text": kony.i18n.getLocalizedString("i18n.common.currencySymbol")
                }
            };
        },

        toggleInfoPopupForRule: function(top, left, popupInfo) {
            this.view.InfoIconPopups.flxCross.onClick = function() {
                this.view.InfoIconPopups.isVisible = false;
            }.bind(this);
            var currForm = kony.application.getCurrentForm();

            if (currForm.InfoIconPopups.isVisible === true) {
                currForm.InfoIconPopups.top = 155;
                currForm.InfoIconPopups.isVisible = false;
                currForm.forceLayout();
            } else {
                currForm.InfoIconPopups.top = top;
                currForm.InfoIconPopups.left = left;
                currForm.InfoIconPopups.RichTextInfo.text = popupInfo;
                currForm.InfoIconPopups.isVisible = true;
                currForm.forceLayout();
            }
        },

        displayApprovalRuleInfoPopup: function(popupInfo) {
            var currForm = kony.application.getCurrentForm();
            var section = currForm.TabBodyNew.segTemplates.selectedRowIndex[0]
            var index = currForm.TabBodyNew.segTemplates.selectedRowIndex[1];
            var segmentData = currForm.TabBodyNew.segTemplates.data[section][1];

            var currTop = 0;
            currForm.InfoIconPopups.top = 155;

            //currTop = currForm.InfoIconPopups.top;
            currTop = parseInt(currTop);
            var indexCount = 0;
            for (var i = 0; i < section; i++) {
                indexCount += (currForm.TabBodyNew.segTemplates.data[i][1].length);
            }
            currTop += ((242 * indexCount) + (50 * (section + 1)) + (((index + 1) * 242) + 200));

            if (currForm.flxErrorMessage.isVisible === true) {
                currTop += currForm.flxErrorMessage.info.frame.height;
            }

            currTop = currTop + 10 + "dp";

            var currLeft = currForm.flxMatrixSegmentWrapper.frame.x + 20;
            currLeft = currLeft + "dp";

            this.toggleInfoPopupForRule(currTop, currLeft, popupInfo);
        },

        displayFeatureActionInfoPopup: function(popupInfo) {
            var currForm = kony.application.getCurrentForm();
            var currTop = currForm.flxContainer.frame.y + currForm.flxMatrixSegmentWrapper.frame.y + currForm.flxFeatureActionHeader.info.frame.height + 5;

            var currLeft = currForm.flxMatrixSegmentWrapper.frame.x + 20;
            currTop = currTop + "dp";
            currLeft = currLeft + "dp";
            this.toggleInfoPopupForRule(currTop, currLeft, popupInfo);
        },

        updateRowHeaders: function() {
            var rows = this.view.TabBodyNew.segTemplates.data;

            for (var i = 0; i < rows[0][1].length; i++) {
                rows[0][1][i]["lblLimitHeader"]["text"] = "Approval Limit " + (i + 1);
            }

            if (rows[0][1].length == 1) {
                rows[0][1][0]["btnDelete"]["isVisible"] = false;
            } else {
                rows[0][1][0]["btnDelete"]["isVisible"] = true;
            }

            this.view.TabBodyNew.segTemplates.setData(rows);
        },

        /**
          Method used to return section data for the editing for approval matrix
        **/

        getSectionData: function(frequency) {
            var scopeObj = this;
            return {
                lblAccountFrequency: {
                    "text": scopeObj.currentSelectAccountInApprovalMatrix + " - " + frequency
                },
                imgIcon: {
                    "src": "info_grey.png"
                },
                lblHeadingSeperator: {
                    "text": "-"
                }
            };
        },

        /**
              Method used to return a blank record in approval matrix editing flow
            **/

        getBlankLimitRecord: function() {
            var scope = this;
            return {
                tbxLowerLimit: {
                    "placeholder": kony.i18n.getLocalizedString("i18n.transfers.amountlabel"),
                    "text": ""
                },
                flxUpperLimitContainer: {
                    "isVisible": true
                },
                tbxUpperLimit: {
                    "placeholder": kony.i18n.getLocalizedString("i18n.transfers.amountlabel"),
                    "text": ""
                },
                lstType: scope.getListData(0, 0),
                lblLimitHeader: {
                    "text": "Approval Limit "
                },
                lblApproversValue: {
                    text: "",
                    ids: []
                },
                lblRuleValue: {
                    text: "",
                    ruleId: "",
                    numberOfApprovals: ""
                }
            };
        },

        setDataFromViewPageOnEditFlow: function(limits) {
            var scopeObj = this;
            var rows = [];

            if ((kony.sdk.isNullOrUndefined(limits) || !Array.isArray(limits) || limits.length == 0) ||
                (limits.length == 1 && limits[0]["numberOfApprovals"] == 0)) {
                var row = {
                    tbxLowerLimit: {
                        "placeholder": kony.i18n.getLocalizedString("i18n.transfers.amountlabel"),
                        "text": ""
                    },
                    flxLowerLimit: {
                        "isVisible": true
                    },
                    tbxUpperLimit: {
                        "placeholder": kony.i18n.getLocalizedString("i18n.transfers.amountlabel"),
                        "text": ""
                    },
                    flxUpperLimitContainer: {
                        "isVisible": true
                    },
                    lstType: scopeObj.getListData(lower, upper),
                    lblLimitHeader: {
                        "text": "Approval Limit " + 1
                    },
                    lblApproversValue: {
                        "text": "",
                        "ids": []
                    },
                    lblRuleValue: {
                        "text": "",
                        "ruleId": "",
                        numberOfApprovals: ""
                    }
                }

                rows.push(row);
            } else {
                for (var i = 0; i < limits.length; i++) {
                    var lower = Number(limits[i]["lowerlimit"]);
                    var upper = Number(limits[i]["upperlimit"]);
                    var formattedLower = applicationManager.getFormatUtilManager().formatAmount(limits[i]["lowerlimit"]);
                    var formattedUpper = applicationManager.getFormatUtilManager().formatAmount(limits[i]["upperlimit"]);
                    var row = {
                        tbxLowerLimit: {
                            "placeholder": kony.i18n.getLocalizedString("i18n.transfers.amountlabel"),
                            "text": lower != -1 ? (formattedLower) : (upper != -1 ? (formattedUpper) : "1")
                        },
                        flxLowerLimit: {
                            "isVisible": true
                        },
                        tbxUpperLimit: {
                            "placeholder": kony.i18n.getLocalizedString("i18n.transfers.amountlabel"),
                            "text": lower != -1 ? (upper != -1 ? (formattedUpper) : "1") : "1"
                        },
                        flxUpperLimitContainer: {
                            "isVisible": lower != -1 ? (upper != -1 ? true : false) : false
                        },
                        lstType: scopeObj.getListData(lower, upper),
                        lblLimitHeader: {
                            "text": "Approval Limit " + (i + 1)
                        },
                        lblApproversValue: {
                            "text": scopeObj.getApprovers(limits[i]["approvers"]),
                            "ids": scopeObj.getApproversIds(limits[i]["approvers"])
                        },
                        lblRuleValue: {
                            "text": limits[i]["approvalRuleName"],
                            "ruleId": limits[i]["approvalRuleId"],
                            numberOfApprovals: limits[i]["numberOfApprovals"]
                        }
                    }

                    rows.push(row);
                }
            }

            return rows;
        },

        getListData: function(lowerLimit, upperLimit) {
            var masterData = [
                ["key1", "Range"],
                ["key2", "UpTo"],
                ["key3", "Above"]
            ];
            var selectedKeyValues;

            if (lowerLimit != -1 && upperLimit != -1) {
                selectedKeyValues = masterData[0][0];
            } else if (lowerLimit == -1 && upperLimit == -1) {
                selectedKeyValues = masterData[2][0];
            } else if (upperLimit != -1) {
                selectedKeyValues = masterData[1][0];
            } else if (lowerLimit != -1) {
                selectedKeyValues = masterData[2][0];
            }
            return {
                "masterData": masterData,
                "selectedKey": selectedKeyValues
            };
        },

        getSelectedKeyValue: function(masterData, selectedKey) {
            var value = null;
            masterData.forEach(function(data) {
                if (data[0] == selectedKey)
                    value = data[1];
            });
            return value;
        },

        /**
              Method used to show the editing flow of approval matrix
            **/

        editApprovalMatrixLimits: function(currentState) {
            var scope = this;
            scope.limitCapForThisFeatureAction = currentState["featureAction"]["featureActionLimit"];
            scope.view.lblMaxLimitValue.text = kony.i18n.getLocalizedString("i18n.common.currencySymbol") + applicationManager.getFormatUtilManager().formatAmount(scope.limitCapForThisFeatureAction);
            this.view.btnAddAnotherRow.onClick = function() {
                scope.view.TabBodyNew.addEmptyRow();
                scope.updateRowHeaders();
                scope.view.TabBodyNew.enableOrDisableProceedWidget();
                scope.view.TabBodyNew.setMasterDataOfListBox();
                scope.AdjustScreen();
            }.bind(this);
            this.view.TabBodyNew.segTemplates.rowTemplate = "flxEditApprovalLimit";
            this.view.TabBodyNew.segTemplates.sectionHeaderTemplate = "flxAccountFrequencyHeader";
            this.view.lblFeatureActionHeader.text = currentState.featureAction["featureActionName"];
            this.view.flxPopupIcon.onClick = scope.displayFeatureActionInfoPopup.bind(scope, currentState.featureAction["featureActionDescription"]);
            this.view.TabBodyNew.setSectionData([this.getSectionData(currentState.frequency)]);
            this.view.TabBodyNew.setRowDataMap([this.getLimitsDataMap()]);
            this.view.TabBodyNew.setDefaultValues([this.getDefaultValuesForLimits(currentState)]);
            this.view.TabBodyNew.addDataForSections([scope.setDataFromViewPageOnEditFlow(currentState["limitStatments"])]);
            this.view.TabBodyNew.setEmptyRowData([this.getBlankLimitRecord()]);
            this.view.TabBodyNew.setProceedWidget(this.view.EditActions.btnNext);

            var rows = this.view.TabBodyNew.segTemplates.data;
            if (rows[0][1].length == 1) {
                rows[0][1][0]["btnDelete"]["isVisible"] = false;
            } else {
                rows[0][1][0]["btnDelete"]["isVisible"] = true;
            }

            this.view.TabBodyNew.enableOrDisableProceedWidget();
            this.view.TabBodyNew.setMasterDataOfListBox();
            this.view.flxEditApprovalMatrixWrapper.setVisibility(true);
            this.view.flxSettingsWrapper.setVisibility(false);
            this.view.InfoIconPopups.setVisibility(false);
            scope.AdjustScreen();
        },

        closeEditApprovalMatrixFlow: function() {
            FormControllerUtility.hideProgressBar(this.view);
            this.view.flxEditApprovalMatrixWrapper.setVisibility(false);
            this.view.flxSettingsWrapper.setVisibility(true);
            this.showErrorMessageOnAppprovalMatrixEdit(false, "");
            this.AdjustScreen();
        },

        saveApprovalMatrix: function(currentState) {
            var scopeObj = this;
            var validityResponse = this.isDataValid()
            if (!validityResponse.valid) {
                this.showErrorMessageOnAppprovalMatrixEdit(true, validityResponse.error);
                return;
            } else {
                FormControllerUtility.showProgressBar(scopeObj.view);
                this.view.flxErrorMessage.isVisible = false;
                this.view.lblErrorSeparator.isVisible = false;
                var newState = {
                    accountId: currentState["accountNumber"],
                    actionId: currentState["featureAction"]["featureActionId"],
                    limitTypeId: currentState["frequency"],
                    limits: scopeObj.getCurrentStateOfEditPage()
                };
                //This is to verify if the user actually made any changes to the Approval Matrix
                if (JSON.stringify(scopeObj.prevApprovalMatrixData) !== JSON.stringify(newState)) {
                    var profilePresentationController = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController;
                    profilePresentationController.saveApprovalMatrixForAccountAndFeatureAction(newState);
                } else {
                    this.closeEditApprovalMatrixFlow();
                }
            }
        },

        /**
         *Part of Approval Matrix Flow
         *Method to show error message on edit page of approval matrix
         **/
        showErrorMessageOnAppprovalMatrixEdit: function(toShow, error) {
            if (toShow) {
                this.view.flxErrorMessage.lblError.text = error;
                this.view.flxErrorMessage.isVisible = true;
                this.view.lblErrorSeparator.isVisible = true;
            } else {
                this.view.flxErrorMessage.lblError.text = error;
                this.view.flxErrorMessage.isVisible = false;
                this.view.lblErrorSeparator.isVisible = false;
            }
            this.AdjustScreen();
        },

        areAllRangesValid: function() {
            var segData = this.view.TabBodyNew.segTemplates.data;

            if (segData === null) return false;
            else {
                var rows = segData[0][1];
                for (var rowIndex = 0; rowIndex < rows.length; rowIndex++) {
                    var rowWidgets = rows[rowIndex];
                    var lowerWidgetData = rows[rowIndex]["tbxLowerLimit"];
                    var lowerLimit = applicationManager.getFormatUtilManager().deFormatAmount(lowerWidgetData.text);
                    var upperWidgetData = rows[rowIndex]["tbxUpperLimit"];
                    var upperLimit = applicationManager.getFormatUtilManager().deFormatAmount(upperWidgetData.text);
                    var limitType = this.getSelectedKeyValue(rows[rowIndex]["lstType"].masterData, rows[rowIndex]["lstType"].selectedKey);

                    if (limitType == "Range") {
                        if (Number(upperLimit) > Number(lowerLimit)) {
                            if (upperWidgetData.skin === "skntxtSSP424242BorderFF0000Op100Radius2px") {
                                upperWidgetData.skin = "sknTbxSSP42424215PxWithoutBorder";
                                this.view.TabBodyNew.updateKeyAt("tbxUpperLimit", upperWidgetData, rowIndex, 0);
                                this.view.TabBodyNew.onSegmentReload();
                            } else {
                                //no need to update and reload the segment
                            }
                        } else {
                            upperWidgetData.skin = "skntxtSSP424242BorderFF0000Op100Radius2px";
                            this.view.TabBodyNew.updateKeyAt("tbxUpperLimit", upperWidgetData, rowIndex, 0);
                            this.view.TabBodyNew.onSegmentReload();
                            return false;
                        }
                    }

                }
            }
            return true;
        },

        /**
         *Part of Approval Matrix Flow
         *Main Method to validate if current data at edit page is correct for update approval matrix service to work
         **/
        isDataValid: function() {

            if (this.view.TabBodyNew.isAllFieldsAreValid()) {

                if (this.areAllRangesValid()) {
                    var data = this.view.TabBodyNew.segTemplates.data[0][1];

                    if (!this.allRulesAndApproversRequirementsMatch(data)) {
                        return {
                            "valid": false,
                            "error": kony.i18n.getLocalizedString("i18n.Settings.ApprovalMatrix.approverValidation")
                        }
                    }

                    var limitRanges = [];

                    for (var i = 0; i < data.length; i++) {
                        var limitRange = [];
                        if (this.getSelectedKeyValue(data[i]["lstType"].masterData, data[i]["lstType"].selectedKey) == "Above") {
                            limitRange.push(data[i]["tbxLowerLimit"].text);
                            limitRange.push(this.limitCapForThisFeatureAction);
                        } else if (this.getSelectedKeyValue(data[i]["lstType"].masterData, data[i]["lstType"].selectedKey) == "UpTo") {
                            limitRange.push("0");
                            limitRange.push(data[i]["tbxLowerLimit"].text);
                        } else {
                            limitRange.push(data[i]["tbxLowerLimit"].text);
                            limitRange.push(data[i]["tbxUpperLimit"].text);
                        }

                        limitRanges.push(limitRange);
                    }

                    limitRanges.sort(function(a, b) {
                        return a[0] - b[0];
                    })

                    var areValidInterval = this.checkForValidIntervals(limitRanges);

                    return {
                        "valid": areValidInterval,
                        "error": kony.i18n.getLocalizedString("i18n.Settings.ApprovalMatrix.intervalvalidation")
                    };
                } else {
                    return {
                        "valid": false,
                        "error": kony.i18n.getLocalizedString("i18n.Settings.ApprovalMatrix.upperlimitValidation")
                    }
                }

            } else {
                return {
                    "valid": false,
                    "error": kony.i18n.getLocalizedString("i18n.Settings.ApprovalMatrix.amountValidation")
                }
            }
        },

        /**
         *Part of Approval Matrix Flow
         *Helper Method to validate if number of selected approvers is greater than or equal to that required by approval rule applied
         **/
        allRulesAndApproversRequirementsMatch: function(approvalMatrixRows) {
            for (var i = 0; i < approvalMatrixRows.length; i++) {
                if (approvalMatrixRows[i]["lblApproversValue"]["ids"].length < approvalMatrixRows[i]["lblRuleValue"]["numberOfApprovals"]) {
                    return false;
                }
            }
            return true;
        },

        /**
         *Part of Approval Matrix Flow
         *Helper Method to validate if ranges defined on edit page are exclusive and exhaustive
         **/
        checkForValidIntervals: function(limitRanges) {
            var first = applicationManager.getFormatUtilManager().deFormatAmount(limitRanges[0][0]);
            var second = applicationManager.getFormatUtilManager().deFormatAmount(limitRanges[0][1]);
            if (limitRanges.length == 1) {
                return (Number(first) == 0) && (Number(second) == this.limitCapForThisFeatureAction);
            }
            var i = 0;
            var flag = false;
            while (i < limitRanges.length - 1) {
                first = applicationManager.getFormatUtilManager().deFormatAmount(limitRanges[i][1]);
                second = applicationManager.getFormatUtilManager().deFormatAmount(limitRanges[i + 1][0]);

                if (first == second) {
                    i = i + 1;
                    flag = true;
                } else {
                    flag = false;
                    return flag;
                }
            }
            return flag && Number(applicationManager.getFormatUtilManager().deFormatAmount(limitRanges[i][1])) == Number(this.limitCapForThisFeatureAction);
        },

        /**
         *Part of Approval Matrix Flow
         *Method is used to construct paylod (using current data on edit page) to update approval matrix
         **/
        getCurrentStateOfEditPage: function() {
            var data = this.view.TabBodyNew.segTemplates.data[0][1];

            var limits = [];

            for (var i = 0; i < data.length; i++) {
                var approversList = [];
                for (var j = 0; j < data[i]["lblApproversValue"]["ids"].length; j++) {
                    approversList.push({
                        "approverId": data[i]["lblApproversValue"]["ids"][j]
                    });
                }
                var deformattedLower = applicationManager.getFormatUtilManager().deFormatAmount(data[i]["tbxLowerLimit"].text);
                var deformattedUpper = applicationManager.getFormatUtilManager().deFormatAmount(data[i]["tbxUpperLimit"].text);
                var limit = {
                    lowerlimit: data[i]["flxUpperLimitContainer"].isVisible ? deformattedLower : (this.getSelectedKeyValue(data[i]["lstType"].masterData, data[i]["lstType"].selectedKey) == "Above" ? deformattedLower : "-1"),

                    upperlimit: data[i]["flxUpperLimitContainer"].isVisible ? deformattedUpper : (this.getSelectedKeyValue(data[i]["lstType"].masterData, data[i]["lstType"].selectedKey) == "UpTo" ? deformattedLower : "-1"),

                    approvalruleId: data[i]["lblRuleValue"]["ruleId"],

                    approvers: approversList
                }
                limits.push(limit);
            }
            return limits;
        },

        isFavourite: function(account) {
            return account.favouriteStatus && account.favouriteStatus === '1';
        },

        isExternal: function(account) {
            if (account.isExternalAccount) {
                if (account.isExternalAccount === "true")
                    return true;
            } else if (account.externalIndicator) {
                if (account.externalIndicator === "true")
                    return true;
            } else
                return false;
        },

        getDropdownDataWithSections: function(accounts) {
            var scopeObj = this;
            var finalData = {};
            var prioritizeAccountTypes = [];
            var business = kony.i18n.getLocalizedString("i18n.accounts.Business");
            var personal = kony.i18n.getLocalizedString("i18n.accounts.Personal");
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            accounts.forEach(function(account) {
                if (isSingleCustomerProfile) {
                    var accountType = applicationManager.getTypeManager().getAccountType(account.accountType);
                    if (finalData.hasOwnProperty(accountType)) {
                        finalData[accountType][1].push(scopeObj.createSegmentData(account));
                    } else {
                        finalData[accountType] = [{
                                flxAccountRoleType: {
                                    "isVisible": false
                                },
                                lblAccountTypeHeader: {
                                    "text": accountType, //=== "Personal Accounts" ? accountType : account.MembershipName,
                                    "left": "10px"
                                },
                                template: "flxDefaultAccountsHeader"
                            },
                            [scopeObj.createSegmentData(account)]
                        ];
                    }
                    this.sectionData = [];
                    var data = [];
                    for (var key in prioritizeAccountTypes) {
                        var accountType = prioritizeAccountTypes[key];
                        if (finalData.hasOwnProperty(accountType)) {
                            data.push(finalData[accountType]);
                            this.sectionData.push(accountType);
                        }
                    }
                    for (i = 0; i < data.length; i++) {
                        var sortedData = data[i][1];
                        if (!this.isFavAccAvailable) this.isFavAccAvailable = sortedData.filter(this.isFavourite).length > 0;
                        if (!this.isExtAccAvailable) this.isExtAccAvailable = sortedData.filter(this.isExternal).length > 0;
                    }
                } else {
                    var accountType = personal;
                    var accountTypeIcon = "";
                    if (account.isBusinessAccount === "false") {
                        if (scopeObj.primaryCustomerId.id === account.Membership_id && scopeObj.primaryCustomerId.type === 'personal') {
                            accountType = "Personal Accounts";
                            accountTypeIcon = "s";
                        } else {
                            accountType = account.Membership_id;
                            accountTypeIcon = "s";
                        }
                    } else {
                        accountType = account.Membership_id;
                        accountTypeIcon = "r";
                    }
                    if (finalData.hasOwnProperty(accountType) && account.Membership_id === finalData[accountType][0]["membershipId"]) {
                        if (finalData[accountType][1][finalData[accountType][1].length - 1].length === 0) {
                            finalData[accountType][1].pop();
                        }
                        finalData[accountType][1].push(scopeObj.createSegmentData(account));
                    } else {
                        prioritizeAccountTypes.push(accountType);
                        finalData[accountType] = [{
                                flxAccountRoleType: {
                                    "isVisible": false
                                },
                                lblAccountRoleType: accountType === personal ? "s" : "r",
                                lblAccountTypeHeader: {
                                    "text": accountType === "Personal Accounts" ? accountType : account.MembershipName,
                                    "left": "20px"
                                },
                                membershipId: account.Membership_id,
                                template: "flxDefaultAccountsHeader"
                            },
                            [scopeObj.createSegmentData(account)]
                        ];
                    }
                }
            });

            return this.sortAccountData(finalData);
        },

        createSegmentData: function(account) {
            var processedAccountName = this.generateFromAccounts(account);
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile;
            var businessUser = applicationManager.getConfigurationManager().isSMEUser === "true"
            var dataObject = {
                "lblDefaultAccountName": processedAccountName[1],
                "accountID": account.Account_id || account.accountID || account.accountNumber,
                "lblDefaultAccountIcon": {
                    isVisible: !isSingleCustomerProfile && this.profileAccess === "both" ? true : false,
                    left: !isSingleCustomerProfile && this.profileAccess === "both" ? "20px" : "10px",
                    text: account.isBusinessAccount === "true" ? "r" : "s",
                }

            };
            return dataObject;
        },

        sortAccountData: function(finalData) {
            var data = [];
            var prioritizeAccountRoleTypes = [];
            var viewType = applicationManager.getConfigurationManager().getConfigurationValue('combinedDashboardView');

            var sections = Object.keys(finalData);
            var index = sections.indexOf(kony.i18n.getLocalizedString("i18n.accounts.personalAccounts"));
            if (index > -1) {
                sections.splice(index, 1);
            }

            prioritizeAccountRoleTypes.push(kony.i18n.getLocalizedString("i18n.accounts.personalAccounts"));
            prioritizeAccountRoleTypes = prioritizeAccountRoleTypes.concat(sections);

            this.sectionData = [];

            for (var i = 0; i < prioritizeAccountRoleTypes.length; i++) {
                var accountType = prioritizeAccountRoleTypes[i];
                if (finalData.hasOwnProperty(accountType)) {
                    data.push(finalData[accountType]);
                    this.sectionData.push(accountType);
                }
            }


            for (var i = 0; i < data.length; i++) {
                var accoountTypeOrder = applicationManager.getTypeManager().getAccountTypesByPriority();
                var sortedData = data[i][1];
                sortedData.sort(function(a, b) {
                    return accoountTypeOrder.indexOf(a.lblAccountType) - accoountTypeOrder.indexOf(b.lblAccountType);
                });
                data[i][1] = sortedData;

            }

            return data;
        },
        navigateToAlertComm: function() {
            var data = this.view.settings.segAlertsCategory.data;
            this.view.settings.flxTransactionalAndPaymentsAlertsWrapper.setVisibility(true);
            this.view.settings.flxAlertCommunications.skin = "sknFlxffffffBottomCursornoBottom";
            this.view.settings.flxAlertCommunications.hoverSkin = "sknhoverf7f7f7";
            this.view.settings.lblAlertComm.skin = "lblSSP42424213px";
            this.view.settings.flxIndicatorAlertComm.setVisibility(true);
            for (var i = 0; i < data.length; i++) {
                data[i].flxIndicatorAlertsCategory.isVisible = false;
                data[i].lblAlertsCategory.skin = "sknLblSSP72727213px";
                data[i].flxAlertCategory.hoverSkin = "sknhoverf7f7f7";
                data[i].flxAlertCategory.skin = "sknFlxffffffBottomCursornoBottom";
            }
            this.view.settings.segAlertsCategory.setData(data);
            this.view.settings.flxAlertsSubMenu.height = (data.length + 1) * 40;
            FormControllerUtility.showProgressBar(this.view);
            this.setAlertCommData();
        },
        setAlertCommData: function(commData) {
            this.showViews(["flxAlertsCommunication"]);
            this.currentSelectedSubMenu = "flxAlertsSubMenu";
            this.view.settings.flxAlertComm1.setVisibility(true);
            this.view.settings.flxAlertComm2.setVisibility(true);
            this.view.settings.flxTransactionalAndPaymentsAlertsWrapper.setVisibility(false);
            this.view.customheader.customhamburger.activateMenu("Settings", "Alert Settings");
            CommonUtilities.setText(this.view.settings.btnEditCommAlerts, kony.i18n.getLocalizedString("i18n.billPay.Edit"), CommonUtilities.getaccessibilityConfig());
            this.showAlertCommView();
            this.view.settings.lblSelectPhoneNumber.text = "";
            this.view.settings.lblSelectEmailNumber.text = "";
            var userPhoneNumbers = applicationManager.getUserPreferencesManager().getEntitlementPhoneNumbers();
            var userEmailIds = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
            if (userPhoneNumbers.length !== 0)
                this.setSegContactData(userPhoneNumbers, this.view.settings.segPhoneComm);
            else
                this.view.settings.flxAlertComm1.setVisibility(false);
            if (userEmailIds.length !== 0)
                this.setSegContactData(userEmailIds, this.view.settings.segEmailComm);
            else
                this.view.settings.flxAlertComm2.setVisibility(false);
            if (userPhoneNumbers.length === 0 && userEmailIds.length === 0) {
                this.view.settings.flxAlertCommBody.setVisibility(false);
                this.view.settings.rtxAlertsCommWarning.text = kony.i18n.getLocalizedString("i18n.alertSettings.NoPrimaryMsg");
            } else {
                this.view.settings.flxAlertCommBody.setVisibility(true);
            }
            FormControllerUtility.hideProgressBar(this.view);
        },
        setSegContactData: function(contactInfo, widgetId) {
            var contactText = "";
            var self = this;
            var primaryPhone = "";
            var primaryEmail = "";
            var contactTypeId = "";
            var primaryContact = {};
            var dataMap = {
                "flxAlertCommunication": "flxAlertCommunication",
                "flxContactsList": "flxContactsList",
                "lblContact": "lblContact",
                "flxPrimary": "flxPrimary",
                "lblPrimary": "lblPrimary",
                "contactData": "contactData"
            };
            var data = contactInfo.map(function(contact) {
                if (contact.Type_id === "COMM_TYPE_PHONE") {
                    contactTypeId = "phone";
                    contactText = contact.Extension + " - (" + contact.phoneCountryCode + ")" + contact.phoneNumber;
                    if (contact.isAlertsRequired === "true") {
                        self.view.settings.lblSelectPhoneNumber.text = contactText;
                        self.view.settings.lblPhoneCommValue.text = contactText;
                        self.view.settings.lblSelectPhoneNumber.info = contact;
                    }
                    if (contact.isPrimary === "true")
                        primaryPhone = contactText;
                    primaryContact = contact;
                } else {
                    contactTypeId = "Email";
                    contactText = contact.Value;
                    if (contact.isAlertsRequired === "true") {
                        self.view.settings.lblSelectEmailNumber.text = contactText;
                        self.view.settings.lblEmailCommValue.text = contactText;
                        self.view.settings.lblEmailCommValue.info = contact;
                    }
                    if (contact.isPrimary === "true")
                        primaryEmail = contactText;
                    primaryContact = contact;
                }
                return {
                    "flxAlertCommunication": "flxAlertCommunication",
                    "flxContactsList": "flxContactsList",
                    "lblContact": contactText,
                    "flxPrimary": {
                        "isVisible": contact.isPrimary === "true" ? true : false
                    },
                    "lblPrimary": kony.i18n.getLocalizedString("i18n.ProfileManagement.Primary"),
                    "contactData": contact
                }
            });
            if (contactTypeId === "phone" && this.view.settings.lblSelectPhoneNumber.text === "") {
                this.view.settings.lblSelectPhoneNumber.text = primaryPhone;
                this.view.settings.lblPhoneCommValue.text = primaryPhone;
                this.view.settings.lblSelectPhoneNumber.info = primaryContact;
            } else if (contactTypeId === "Email" && this.view.settings.lblSelectEmailNumber.text === "") {
                this.view.settings.lblSelectEmailNumber.text = primaryEmail;
                this.view.settings.lblEmailCommValue.text = primaryEmail;
                this.view.settings.lblEmailCommValue.info = primaryContact;
            }
            widgetId.widgetDataMap = dataMap;
            widgetId.setData(data);
            this.view.forceLayout();
        },
        showEditAlertCommunications: function() {
            this.view.settings.lblPhoneComm.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.PhoneNumber");
            this.view.settings.lblEmailComm.text = kony.i18n.getLocalizedString("i18n.LoginMFA.EmailID");
            this.view.settings.lblPhoneComm.skin = "sknLblSSP42424215px";
            this.view.settings.lblEmailComm.skin = "sknLblSSP42424215px";
            this.view.settings.lblPhoneCommValue.setVisibility(false);
            this.view.settings.flxCommPhoneDropDown.setVisibility(true);
            this.view.settings.lblEmailCommValue.setVisibility(false);
            this.view.settings.flxCommEmailDropDown.setVisibility(true);
            this.view.settings.btnEditCommAlerts.setVisibility(false);
            this.view.settings.flxErrorEditAlertComm.setVisibility(false);
            this.view.settings.rtxAlertsCommWarning.text = kony.i18n.getLocalizedString("i18n.alertSettings.alertCommViewMsg");
            this.view.settings.flxButtons.setVisibility(true);
            this.view.forceLayout();
        },
        saveAlertCommunications: function() {
            var phoneInfo = this.view.settings.lblSelectPhoneNumber.info;
            var emailInfo = this.view.settings.lblSelectEmailNumber.info;
            var self = this;
            var data = {
                Extension: phoneInfo.Extension,
                isPrimary: phoneInfo.isPrimary === "true" ? '1' : '0',
                isAlertsRequired: true,
            }
            data = {
                "emailIds": [{
                    'id': emailInfo.id,
                    'Extension': emailInfo.Extension,
                    'isPrimary': emailInfo.isPrimary === "true" ? '1' : '0',
                    'isAlertsRequired': '1',
                    'value': emailInfo.Value
                }],
                "phoneNumbers": [{
                    'id': phoneInfo.id,
                    'Extension': phoneInfo.Extension,
                    'isPrimary': phoneInfo.isPrimary === "true" ? '1' : '0',
                    'isAlertsRequired': '1',
                    'phoneNumber': phoneInfo.phoneNumber,
                    'phoneCountryCode': phoneInfo.phoneCountryCode
                }],
            }
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateAlertCommunication(data);
        },
        showAlertCommView: function() {
            this.view.settings.lblPhoneComm.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.PhoneNumber");
            this.view.settings.lblEmailComm.text = kony.i18n.getLocalizedString("i18n.LoginMFA.EmailID");
            this.view.settings.lblPhoneComm.skin = "sknSSP72727215Px";
            this.view.settings.lblEmailComm.skin = "sknSSP72727215Px";
            this.view.settings.lblPhoneCommValue.setVisibility(true);
            this.view.settings.flxCommPhoneDropDown.setVisibility(false);
            this.view.settings.lblEmailCommValue.setVisibility(true);
            this.view.settings.flxCommEmailDropDown.setVisibility(false);
            this.view.settings.btnEditCommAlerts.setVisibility(true);
            this.view.settings.btnEditCommAlerts.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
            this.view.settings.flxErrorEditAlertComm.setVisibility(false);
            this.view.settings.rtxAlertsCommWarning.text = kony.i18n.getLocalizedString("i18n.alertSettings.alertCommEditMsg");
            this.view.settings.flxButtons.setVisibility(false);
            this.view.forceLayout();
        },

        showEditConsent: function() {
            consentEdit = true;
            var scopeObj = this;
            this.view.settings.btnEditConsent.isVisible = false;
            var flexRoot = this.view.settings.flxConsentManagementContainer.flxAllConsentTypes.widgets();
            for (var i = 0; i < flexRoot.length; i++) {
                var flxItemRow = flexRoot[i].widgets()[5];
                var flxOptions = flxItemRow.widgets();

                if (flexRoot[i].widgets()[2].isVisible === false) {
                    for (var j = 0; j < flxOptions.length; j++) {
                        var itemOption = flxOptions[j].widgets();
                        itemOption[0].skin = "sknlblOLBFonts3343A820pxOlbFontIcons";
                        itemOption[1].skin = "sknLblSSP42424215px";
                    }
                }
            }
            scopeObj.view.settings.flxConsentManagementButtons.setVisibility(true);
            scopeObj.view.settings.flxConsentManagementContainer.flxAllConsentTypes = this.view.settings.flxConsentManagementContainer.flxAllConsentTypes;
            scopeObj.disableButton(this.view.settings.btnConsentManagementSave);
            scopeObj.view.forceLayout();
        },

        setSelectedContactInfo: function(segmentPath, valueLabelPath) {
            var selectedIndex = segmentPath.selectedRowIndex[1];
            var selectedItem = segmentPath.data[selectedIndex];
            valueLabelPath.text = selectedItem.lblContact;
            valueLabelPath.info = selectedItem.contactData;
            this.view.forceLayout();
        },
        /**
         * Method to error while updating the alert communication scenario
         * @param {String} errorMessage- Message to be shown
         */
        showEditAlertCommError: function(errorMessage) {
            this.view.settings.flxErrorEditAlertComm.setVisibility(true);
            CommonUtilities.setText(this.view.settings.lblErrorAlertComm, errorMessage, CommonUtilities.getaccessibilityConfig());
        },
        onCategoryMenuClick: function() {
            var selectedIndex = this.view.settings.segAlertsCategory.selectedRowIndex ? this.view.settings.segAlertsCategory.selectedRowIndex[1] : 0;
            this.selectedCategoryIndex = selectedIndex;
            var data = this.view.settings.segAlertsCategory.data;
            var accountAlertsHeading = data[selectedIndex].lblAlertsCategory.text;
            CommonUtilities.setText(this.view.settings.accountAlerts.lblAlertsHeading, accountAlertsHeading == "Account" ? accountAlertsHeading + " " + kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts") : accountAlertsHeading, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.settings.lblAlertsHeading, accountAlertsHeading == "Account" ? accountAlertsHeading + " " + kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts") : accountAlertsHeading, CommonUtilities.getaccessibilityConfig());
            this.expand(this.view.settings.flxAlertsSubMenu);
            if (this.view.settings.flxAlertCommunications.isVisible && this.view.settings.flxIndicatorAlertComm.isVisible) {
                this.view.settings.flxAlertCommunications.skin = "sknFlxffffffBottomCursornoBottom";
                this.view.settings.flxAlertCommunications.hoverSkin = "sknhoverf7f7f7";
                this.view.settings.lblAlertComm.skin = "sknLblSSP72727213px";
                this.view.settings.flxIndicatorAlertComm.setVisibility(false);
            }
            for (var i = 0; i < data.length; i++) {
                data[i].flxIndicatorAlertsCategory.isVisible = false;
                data[i].lblAlertsCategory.skin = "sknLblSSP72727213px";
                data[i].flxAlertCategory.hoverSkin = "sknhoverf7f7f7";
                data[i].flxAlertCategory.skin = "sknFlxffffffBottomCursornoBottom";
            }
            data[selectedIndex].flxIndicatorAlertsCategory.isVisible = true;
            data[selectedIndex].lblAlertsCategory.skin = "lblSSP42424213px";
            data[selectedIndex].flxAlertCategory.hoverSkin = "sknFlxffffffBottomCursornoBottom";
            this.currentSelectedSubMenu = "flxSegAlertsCategory";
            this.navigateByAlertId(data[selectedIndex].lblReference.text, data[selectedIndex].isAccountLevel);
            this.view.settings.segAlertsCategory.setData(data);
            this.view.forceLayout();
        },
        /**
         *Method is used to call respective functions based on alert preference
         **/
        setAlertsDataToUI: function(alertsModel) {
            var scopeObj = this;
            var alertsViewModel = alertsModel.alertsData;
            var isInitialLoad = alertsViewModel.categorySubscription.isInitialLoad;
            this.showViews(["flxAlertsHeaderContainer"]);
            var isModifyScreen = this.view.settings.btnEditAlerts.isVisible === true ? false : true;
            this.enableFrequency = alertsViewModel.alertConfiguration[0].enableFrequency === "1" ? true : false;
          var isEnabledScreen = alertsViewModel.categorySubscription.isSubscribed == "true"?true:false;
          this.view.settings.lblStatus1.onTouchEnd = function() {
            scopeObj.enableButton(scopeObj.view.settings.btnAlertSave);
            if((isInitialLoad=="true"||isInitialLoad==true)&&scopeObj.view.settings.lblStatus1.text !== OLBConstants.SWITCH_ACTION.ON)
              scopeObj.initialAlertsToggle(alertsModel);
            if (scopeObj.view.settings.lblStatus1.text === OLBConstants.SWITCH_ACTION.ON) {
              scopeObj.view.settings.lblStatus1.text = OLBConstants.SWITCH_ACTION.OFF;
              scopeObj.view.settings.lblStatus1.skin = ViewConstants.SKINS.SWITCH_OFF;
            } else {
              scopeObj.view.settings.lblStatus1.text = OLBConstants.SWITCH_ACTION.ON;
              scopeObj.view.settings.lblStatus1.skin = ViewConstants.SKINS.SWITCH_ON;
            }
            scopeObj.ToggleSwitchAlerts();
          };
           if(isEnabledScreen){
              	this.view.settings.lblStatus1.text = OLBConstants.SWITCH_ACTION.ON;
                this.view.settings.lblStatus1.skin = ViewConstants.SKINS.SWITCH_ON;
              }
              else{
                this.view.settings.lblStatus1.text = OLBConstants.SWITCH_ACTION.OFF;
                this.view.settings.lblStatus1.skin = ViewConstants.SKINS.SWITCH_OFF;
              }
            if (alertsViewModel.alertConfiguration[0].alertPreferenceView === "CATEGORY") {
                this.setCategoryChannelsToUI(alertsViewModel.alertCategory.supportedChannels, alertsViewModel.alertCategory.subscribedChannels, isInitialLoad);
                this.setCategoryFrequencyData(alertsViewModel.alertCategory.subscribedFrequency);
            } else {
                this.view.settings.flxChannelsFrequency.setVisibility(false);
                this.view.settings.flxChannelsFrequencyTab.setVisibility(false);
            }
            this.view.settings.flxEnableSwitch.setEnabled(false);
            this.view.settings.flxChannelsFrequency.setEnabled(false);
            this.view.settings.flxChannelsFrequencyTab.setEnabled(false);
            this.view.settings.segAlertsListing.setEnabled(false);
            this.alertPreference = alertsViewModel.alertConfiguration[0].alertPreferenceView;
            if (alertsViewModel.alertTypes.length === 0) {
                scopeObj.view.settings.flxNoAlertsFound.setVisibility(true);
                scopeObj.view.settings.flxAlertsSegment.setVisibility(false);
            } else if (this.alertPreference === "ALERT")
                this.setAlertLevelAlertsSegData(alertsViewModel.alertTypes, isInitialLoad);
            else if (this.alertPreference === "GROUP")
                this.setGroupLevelAlertsSegData(alertsViewModel.alertTypes, isInitialLoad);
            else if (this.alertPreference === "CATEGORY")
                this.setCategoryLevelAlertsSegData(alertsViewModel.alertTypes);
            this.isServiceCallNeeded = false;
            this.view.settings.btnEditAlerts.onClick = scopeObj.onEditAlertsClick.bind(this, alertsModel.AlertCategoryId);
            this.view.settings.btnAlertCancel.onClick = scopeObj.onClickCancelAlerts.bind(this, alertsModel);
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnAlertSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnAlertSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.settings.btnAlertSave.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnAlertSave.onClick = scopeObj.onAlertSaveClick.bind(this, alertsModel);
            }
            var data = this.view.settings.segAlertsCategory.data;
            if (this.selectedCategoryIndex === undefined || this.selectedCategoryIndex === null) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].lblReference.text === alertsModel.AlertCategoryId) {
                        this.selectedCategoryIndex = i;
                        var accountAlertsHeading = data[i].lblAlertsCategory.text;
                        CommonUtilities.setText(this.view.settings.accountAlerts.lblAlertsHeading, accountAlertsHeading == "Account" ? accountAlertsHeading + " " + kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts") : accountAlertsHeading, CommonUtilities.getaccessibilityConfig());
                        CommonUtilities.setText(this.view.settings.lblAlertsHeading, accountAlertsHeading == "Account" ? accountAlertsHeading + " " + kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts") : accountAlertsHeading, CommonUtilities.getaccessibilityConfig());
                        data[i].flxIndicatorAlertsCategory.isVisible = true;
                        data[i].lblAlertsCategory.skin = "lblSSP42424213px";
                        data[i].flxAlertCategory.hoverSkin = "sknFlxffffffBottomCursornoBottom";
                        this.currentSelectedSubMenu = "flxAlertsSubMenu";
                    } else {
                        data[i].flxIndicatorAlertsCategory.isVisible = false;
                        data[i].lblAlertsCategory.skin = "sknLblSSP72727213px";
                        data[i].flxAlertCategory.hoverSkin = "sknhoverf7f7f7";
                        data[i].flxAlertCategory.skin = "sknFlxffffffBottomCursornoBottom";
                    }
                }
                this.view.settings.segAlertsCategory.setData(data);
            }
            if (data[this.selectedCategoryIndex].isAccountLevel === true) {
                this.onEditAlertsClick(alertsModel.AlertCategoryId);
            }
            this.view.settings.flxAlertsBody.forceLayout();
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         *Method is used to set channels data at category level
         * @param {String,String,Boolean} supported channels string , subscribed channels string , isInitialLoad flag
         **/
        setCategoryChannelsToUI: function(supportedChannels, subscribedChannels, isInitialLoad) {
            var channelsData = supportedChannels.split(",");
            var channelIcons = {
                "CH_SMS": {
                    "text": "v",
                    "normalSkin": "sknLblSMS727272FontIcon",
                    "selectedSkin": "sknLblSMS003e75FontIcon"
                },
                "CH_EMAIL": {
                    "text": "w",
                    "normalSkin": "sknLblEmail727272FontIcon",
                    "selectedSkin": "sknLblEmail003e75FontIcon"
                },
                "CH_PUSH_NOTIFICATION": {
                    "text": "x",
                    "normalSkin": "sknLblPush727272FontIcon",
                    "selectedSkin": "sknLblPush003e75FontIcon"
                },
                "CH_NOTIFICATION_CENTER": {
                    "text": "y",
                    "normalSkin": "sknLblNotification727272FontIcon",
                    "selectedSkin": "sknLblNotification003e75FontIcon"
                }
            };
            var channelsCount = 0;
            if (isInitialLoad === "true" || isInitialLoad === true || subscribedChannels === undefined)
                subscribedChannels = "";
            this.view.settings.flxChannel1.skin = "slFbox";
            this.view.settings.flxChannel2.skin = "slFbox";
            this.view.settings.flxChannel3.skin = "slFbox";
            this.view.settings.flxChannel4.skin = "slFbox";
            this.view.settings.flxChannel1Tab.skin = "slFbox";
            this.view.settings.flxChannel2Tab.skin = "slFbox";
            this.view.settings.flxChannel3Tab.skin = "slFbox";
            this.view.settings.flxChannel4Tab.skin = "slFbox";
            for (var i = 0; i < channelsData.length; i++) {
                this.view.settings["flxChannel" + (i + 1)].setVisibility(true);
                this.view.settings["flxChannel" + (i + 1) + "Tab"].setVisibility(true);
                if (i !== 0) {
                    this.view.settings["flxVerticalSeparator" + (i)].setVisibility(true);
                    this.view.settings["flxVerticalSeparator" + (i) + "Tab"].setVisibility(true);
                }
                this.view.settings["lblChannel" + (i + 1)].text = channelIcons[channelsData[i]].text;
                this.view.settings["lblChannel" + (i + 1) + "Tab"].text = channelIcons[channelsData[i]].text;
                this.view.settings["lblChannel" + (i + 1)].info = {
                    "id": channelsData[i],
                    "isSelected": false,
                    "normalSkin": channelIcons[channelsData[i]].normalSkin,
                    "selectedSkin": channelIcons[channelsData[i]].selectedSkin
                };
                this.view.settings["lblChannel" + (i + 1) + "Tab"].info = {
                    "id": channelsData[i],
                    "isSelected": false,
                    "normalSkin": channelIcons[channelsData[i]].normalSkin,
                    "selectedSkin": channelIcons[channelsData[i]].selectedSkin
                };
                if (subscribedChannels.indexOf(channelsData[i]) >= 0) {
                    this.view.settings["flxChannel" + (i + 1)].onClick();
                    this.view.settings["lblChannel" + (i + 1)].skin = this.view.settings["lblChannel" + (i + 1)].info.selectedSkin;
                    this.view.settings["lblChannel" + (i + 1) + "Tab"].skin = this.view.settings["lblChannel" + (i + 1) + "Tab"].info.selectedSkin;
                    this.view.settings["flxChannel" + (i + 1) + "Tab"].onClick();
                } else {
                    this.view.settings["lblChannel" + (i + 1)].skin = this.view.settings["lblChannel" + (i + 1)].info.normalSkin;
                    this.view.settings["lblChannel" + (i + 1) + "Tab"].skin = this.view.settings["lblChannel" + (i + 1) + "Tab"].info.normalSkin;
                }
            }
            for (var x = i + 1; x <= 4; x++) {
                this.view.settings["flxChannel" + x].setVisibility(false);
                this.view.settings["flxVerticalSeparator" + (x - 1)].setVisibility(false);
                this.view.settings["flxChannel" + x + "Tab"].setVisibility(false);
                this.view.settings["flxVerticalSeparator" + (x - 1) + "Tab"].setVisibility(false);
            }
            this.view.settings.flxChannels.width = (channelsData.length) * 45 + "px";
            this.view.settings.flxChannelsTab.width = (channelsData.length) * 55 + "px";
            this.view.forceLayout();
        },
        /**
         *Method is used to set master data for frequency
         **/
        setFrequencyData: function() {
            var category = [
                ["MONTHLY", "Monthly"],
                ["WEEKLY", "Weekly"],
                ["DAILY", "Daily"]
            ];
            var weekData = [
                ["SELECT", "Select"],
                ["MONDAY", "Monday"],
                ["TUESDAY", "Tuesday"],
                ["WEDNESDAY", "Wednesday"],
                ["THURSDAY", "Thursday"],
                ["FRIDAY", "Friday"],
                ["SATURDAY", "Saturday"],
                ["SUNDAY", "Sunday"]
            ]
            var monthDates = [
                ["SELECT", "Select"]
            ];
            for (var i = 1; i <= 31; i++) {
                monthDates.push([i.toString(), i.toString()]);
            }
            var time = [
                ["SELECT", "Select"]
            ];
            var timeString = "";
            for (var j = 1; j < 13; j++) {
                timeString = j;
                time.push([timeString + ":00AM", timeString + ":00AM"], [timeString + ":30AM", timeString + ":30AM"]);
            }
            for (var j = 1; j < 13; j++) {
                timeString = j;
                time.push([timeString + ":00PM", timeString + ":00PM"], [timeString + ":30PM", timeString + ":30PM"]);
            }
            this.frequencyList = {
                "category": category,
                "weeks": weekData,
                "dates": monthDates,
                "time": time
            };
        },
        /**
         *Method is used to set frequency data at category level
         * @param [{"alertFrequencyId":"","frequencyValue":"","frequencyTime":""}] Frequency object JSON
         **/
        setCategoryFrequencyData: function(subscribedFreq) {
            if (subscribedFreq && this.enableFrequency) {
                if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                    this.view.settings.lstBoxFrequency1Tab.masterData = this.frequencyList.category;
                    this.view.settings.lstBoxFrequency1Tab.selectedKey = subscribedFreq[0].alertFrequencyId;
                    if (subscribedFreq[0].alertFrequencyId === "DAILY") {
                        this.view.settings.flxFrequency2Tab.setVisibility(false);
                    } else {
                        this.view.settings.flxFrequency2Tab.setVisibility(true);
                        this.view.settings.lstBoxFrequency2Tab.masterData = subscribedFreq[0].alertFrequencyId === "WEEKLY" ? this.frequencyList.weeks : this.frequencyList.dates;
                        this.view.settings.lstBoxFrequency2Tab.selectedKey = subscribedFreq[0].frequencyValue;
                    }
                    this.view.settings.lstBoxFrequency3Tab.masterData = this.frequencyList.time;
                    this.view.settings.lstBoxFrequency3Tab.selectedKey = applicationManager.getFormatUtilManager().getTwelveHourTimeString(subscribedFreq[0].frequencyTime);
                    this.view.settings.flxCategoryFrequency.setVisibility(false);
                    this.view.settings.flxCategoryFrequencyTab.setVisibility(true);
                } else {
                    this.view.settings.lstBoxFrequency1.masterData = this.frequencyList.category;
                    this.view.settings.lstBoxFrequency1.selectedKey = subscribedFreq[0].alertFrequencyId;
                    if (subscribedFreq[0].alertFrequencyId === "DAILY") {
                        this.view.settings.flxFrequency2.setVisibility(false);
                    } else {
                        this.view.settings.flxFrequency2.setVisibility(true);
                        this.view.settings.lstBoxFrequency2.masterData = subscribedFreq[0].alertFrequencyId === "WEEKLY" ? this.frequencyList.weeks : this.frequencyList.dates;
                        this.view.settings.lstBoxFrequency2.selectedKey = subscribedFreq[0].frequencyValue;
                    }
                    this.view.settings.lstBoxFrequency3.masterData = this.frequencyList.time;
                    this.view.settings.lstBoxFrequency3.selectedKey = applicationManager.getFormatUtilManager().getTwelveHourTimeString(subscribedFreq[0].frequencyTime);
                    this.view.settings.flxCategoryFrequencyTab.setVisibility(false);
                    this.view.settings.flxCategoryFrequency.setVisibility(true);
                }
            } else {
                this.view.settings.flxCategoryFrequency.setVisibility(false);
                this.view.settings.flxCategoryFrequencyTab.setVisibility(false);
            }
            this.view.forceLayout();
        },
        /**
         *Method is used to set segment data at ALERT level
         **/
        setAlertLevelAlertsSegData: function(alertTypesData, isInitialLoad) {
            var self = this;
            var widgetMap = {
                "flxCategoryAlertsHeader": "flxCategoryAlertsHeader",
                "flxCategoryAlertsContainer": "flxCategoryAlertsContainer",
                "flxGroupCheckBox": "flxGroupCheckBox",
                "lblGroupCheckBoxIcon": "lblGroupCheckBoxIcon",
                "lblGroupName": "lblGroupName",
                "lblGroupAlertsCount": "lblGroupAlertsCount",
                "flxFrequencyFields": "flxFrequencyFields",
                "lblFrequencyTitle": "lblFrequencyTitle",
                "flxFrequency": "flxFrequency",
                "flxFrequencyBox1": "flxFrequencyBox1",
                "lstBoxFrequency1": "lstBoxFrequency1",
                "flxFrequencyBox2": "flxFrequencyBox2",
                "lstBoxFrequency2": "lstBoxFrequency2",
                "flxFrequencyBox3": "flxFrequencyBox3",
                "lstBoxFrequency3": "lstBoxFrequency3",
                "flxChannels": "flxChannels",
                "flxChannel1": "flxChannel1",
                "lblChannel1": "lblChannel1",
                "flxVerticalSeparator1": "flxVerticalSeparator1",
                "flxChannel2": "flxChannel2",
                "lblChannel2": "lblChannel2",
                "flxVerticalSeparator2": "flxVerticalSeparator2",
                "flxChannel3": "flxChannel3",
                "lblChannel3": "lblChannel3",
                "flxVerticalSeparator3": "flxVerticalSeparator3",
                "flxChannel4": "flxChannel4",
                "lblChannel4": "lblChannel4",
                "flxGroupAlertRowDW": "flxGroupAlertRowDW",
                "flxGroupAlertRowTablet": "flxGroupAlertRowTablet",
                "flxAlert": "flxAlert",
                "flxAlertName": "flxAlertName",
                "flxAlertCheckBox": "flxAlertCheckBox",
                "lblAlertCheckBoxIcon": "lblAlertCheckBoxIcon",
                "lblAlertName": "lblAlertName",
                "flxAmountAttribute": "flxAmountAttribute",
                "flxAttributeValues": "flxAttributeValues",
                "flxAlertAttribute1": "flxAlertAttribute1",
                "lblAttributeTitle1": "lblAttributeTitle1",
                "lblFromValue": "lblFromValue",
                "flxAmount1": "flxAmount1",
                "lblCurrencySymbol1": "lblCurrencySymbol1",
                "tbxAmount1": "tbxAmount1",
                "flxAlertAttribute2": "flxAlertAttribute2",
                "lblToValue": "lblToValue",
                "flxAmount2": "flxAmount2",
                "lblCurrencySymbol2": "lblCurrencySymbol2",
                "tbxAmount2": "tbxAmount2",
                "flxAlertAttribute3": "flxAlertAttribute3",
                "lblAttributeTitle3": "lblAttributeTitle3",
                "lblToValue2": "lblToValue2",
                "flxAmount3": "flxAmount3",
                "lblCurrencySymbol3": "lblCurrencySymbol3",
                "tbxAmount3": "tbxAmount3",
                "flxSeperator": "flxSeperator",
                "showToVal": "showToVal",
                "recordData": "recordData",
                "alertsubtype_id": "alertsubtype_id",
                "alerttype_id": "alerttype_id",
                "supportedChannels": "supportedChannels",
                "subscribedChannels": "subscribedChannels"
            };
            var segData = [];
            var rows = [];
            var subscribedAlertsCount = 0;
            for (var i = 0; i < alertTypesData.length; i++) {
                var sectionData = self.mapAlertLevelGroupSection(alertTypesData[i]);
                rows = [];
                subscribedAlertsCount = 0;
                if (alertTypesData[i].alertSubTypes.length > 0) {
                    for (var j = 0; j < alertTypesData[i].alertSubTypes.length; j++) {
                        rows.push(self.mapAlertLevelAlertRow(alertTypesData[i].alertSubTypes[j], alertTypesData[i].isSubscribed, isInitialLoad));
                        if (alertTypesData[i].alertSubTypes[j].isSubscribed === "true") {
                            subscribedAlertsCount++;
                        }
                    }
                    rows[rows.length - 1].flxSeperator.isVisible = false;
                    rows[rows.length - 1].flxFullSeperator.isVisible = true;
                }
                if ((subscribedAlertsCount !== alertTypesData[i].alertSubTypes.length) && alertTypesData[i].isSubscribed === "true")
                    sectionData.lblGroupCheckBoxIcon.text = "z";
                if (alertTypesData[i].isSubscribed === "true") {
                    sectionData.lblGroupAlertsCount.text = "(" + subscribedAlertsCount + "/" + alertTypesData[i].alertSubTypes.length + ")";
                    sectionData.lblGroupAlertsCount.isVisible = true;
                }
                segData.push([sectionData, rows]);
            }
            this.view.settings.segAlertsListing.widgetDataMap = widgetMap;
            this.view.settings.segAlertsListing.setData(segData);
            this.view.forceLayout();
        },
        /**
         *Method is used to set mapping data for Group section at ALERT/CATEGORY level
         **/
        mapAlertLevelGroupSection: function(secData) {
            var self = this;
            return {
                "alerttype_id": secData.alerttype_id,
                "flxGroupCheckBox": {
                    "onClick": self.toggleGroupCheckBox
                },
                "lblGroupCheckBoxIcon": {
                    "text": secData.isSubscribed === "true" ? "C" : "D",
                    "skin": OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN
                },
                "lblGroupName": {
                    "text": secData.alerttypetext_DisplayName
                },
                "lblGroupAlertsCount": {
                    "isVisible": false,
                    "text": "0/" + secData.alertSubTypes.length
                },
                "isEdited": false,
                "template": "flxCategoryAlertsHeader",
            };
        },
        /**
         *Method is used to set mapping data for Alert section at ALERT level
         **/
        mapAlertLevelAlertRow: function(rowData, isGroupSubscribed, isInitialLoad) {
            var self = this;
            var showToVal = false;
            var template = "";
            var isTablet = false;
            var frqText1 = "";
            var frqText2 = "SELECT";
            var frqText3 = "SELECT";
            if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                template = "flxGroupAlertRowTablet";
                isTablet = true;
            } else {
                template = "flxGroupAlertRowDW";
            }
            if (rowData.alertCondition && rowData.alertCondition.id === "IN_BETWEEN")
                showToVal = true;
            if (rowData.subscribedFrequency) {
                frqText1 = rowData.subscribedFrequency[0].alertFrequencyId;
                frqText2 = rowData.subscribedFrequency[0].frequencyValue === "" || rowData.subscribedFrequency[0].frequencyValue === "null" || rowData.subscribedFrequency[0].frequencyValue === "0" || rowData.subscribedFrequency[0].frequencyValue === undefined ? "SELECT" : rowData.subscribedFrequency[0].frequencyValue;
                frqText3 = applicationManager.getFormatUtilManager().getTwelveHourTimeString(rowData.subscribedFrequency[0].frequencyTime);
            }
            var rowJSON = {
                "recordData": rowData,
                "alertsubtype_id": rowData.alertsubtype_id,
                "flxAlertCheckBox": {
                    "onClick": self.toggleAlertCheckBox
                },
                "lblAlertCheckBoxIcon": {
                    "text": isGroupSubscribed === "true" && rowData.isSubscribed === "true" ? "C" : "D",
                    "skin": OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN,
                },
                "lblAlertName": rowData.alertsubtypetext_displayName,
                "flxFrequencyFields": {
                    "isVisible": isGroupSubscribed === "true" && rowData.isSubscribed === "true" && self.enableFrequency && frqText1 !== "" ? true : false
                },
                "flxFrequencyBox1": {
                    "width": isTablet === true ? (frqText2 === "SELECT" ? "49%" : "100%") : "28%"
                },
                "flxFrequencyBox3": {
                    "top": isTablet === true ? (frqText2 === "SELECT" ? "0dp" : "50dp") : "0dp"
                },
                "flxFrequencyBox2": {
                    "isVisible": frqText2 === "SELECT" ? false : true
                },
                "lstBoxFrequency1": {
                    "masterData": self.frequencyList.category,
                    "selectedKey": frqText1 !== "" ? frqText1 : "DAILY",
                    "onSelection": function() {
                        self.onFrequencySelection(arguments[1], 1)
                    },
                    "skin": "sknlbxalto42424215pxBordere3e3e32pxRadius"
                },
                "lstBoxFrequency2": {
                    "masterData": frqText1 === "MONTHLY" ? self.frequencyList.dates : self.frequencyList.weeks,
                    "selectedKey": frqText2,
                    "onSelection": function() {
                        self.onFrequencySelection(arguments[1], 2)
                    },
                    "skin": "sknlbxalto42424215pxBordere3e3e32pxRadius"
                },
                "lstBoxFrequency3": {
                    "masterData": self.frequencyList.time,
                    "selectedKey": frqText3,
                    "onSelection": function() {
                        self.onFrequencySelection(arguments[1], 3)
                    },
                    "skin": "sknlbxalto42424215pxBordere3e3e32pxRadius"
                },
                "flxAmountAttribute": {
                    "isVisible": isGroupSubscribed === "true" && rowData.isSubscribed === "true" && rowData.alertAttribute ? true : false
                },
                "lblAttributeTitle1": rowData.alertsubtypetext_description,
                "lblFromValue": {
                    "text": kony.i18n.getLocalizedString("i18n.alertSettings.FromValue"),
                    "isVisible": showToVal
                },
                "flxAmount1": {
                    "skin": "sknBorder727272bgffffff"
                },
                "tbxAmount1": {
                    "text": rowData.alertsubtype_value1 ? rowData.alertsubtype_value1 : "0",
                    "onTextChange": function() {
						this.enableButton(this.view.settings.btnAlertSave);
					}.bind(this)
                },
                "flxAlertAttribute2": {
                    "isVisible": isTablet === false && showToVal
                },
                "flxAmount2": {
                    "skin": "sknBorder727272bgffffff"
                },
                "tbxAmount2": {
                    "text": rowData.alertsubtype_value2 ? rowData.alertsubtype_value2 : "0",
                     "onTextChange": function() {
						this.enableButton(this.view.settings.btnAlertSave);
					}.bind(this)
                },
                "flxAlertAttribute3": {
                    "isVisible": rowData.isSubscribed === "true" && isTablet && showToVal
                },
                "flxAmount3": {
                    "skin": "sknBorder727272bgffffff"
                },
                "tbxAmount3": {
                    "text": rowData.alertsubtype_value2 ? rowData.alertsubtype_value2 : "0",
                    "onTextChange": function() {
						this.enableButton(this.view.settings.btnAlertSave);
					}.bind(this)
                },
                "flxChannels": {
                    "width": "220px",
                    "isVisible": true
                },
                "flxChannel1": {
                    "skin": "slFbox",
                    "isVisible": true,
                    "onClick": function() {
                        self.channelOnClick(1)
                    }
                },
                "lblChannel1": {
                    "text": "v",
                    "skin": "slFbox",
                    "info": {}
                },
                "flxChannel2": {
                    "skin": "slFbox",
                    "isVisible": true,
                    "onClick": function() {
                        self.channelOnClick(2)
                    }
                },
                "lblChannel2": {
                    "text": "w",
                    "skin": "slFbox",
                    "info": {}
                },
                "flxChannel3": {
                    "skin": "slFbox",
                    "isVisible": true,
                    "onClick": function() {
                        self.channelOnClick(3)
                    }
                },
                "lblChannel3": {
                    "text": "x",
                    "skin": "slFbox",
                    "info": {}
                },
                "flxChannel4": {
                    "skin": "slFbox",
                    "isVisible": true,
                    "onClick": function() {
                        self.channelOnClick(4)
                    }
                },
                "lblChannel4": {
                    "text": "y",
                    "skin": "slFbox",
                    "info": {}
                },
                "flxVerticalSeparator1": {
                    "isVisible": true
                },
                "flxVerticalSeparator2": {
                    "isVisible": true
                },
                "flxVerticalSeparator3": {
                    "isVisible": true
                },
                "flxSeperator": {
                    "isVisible": true
                },
                "flxFullSeperator": {
                    "isVisible": false
                },
                "supportedChannels": rowData.supportedChannels,
                "subscribedChannels": rowData.subscribedChannels ? rowData.subscribedChannels : "",
                "showToVal": showToVal,
                "template": template,
            };
            var finalJSON = [];
            finalJSON = self.setAlertChannelsToUI(rowData.supportedChannels, rowJSON.lblAlertCheckBoxIcon.text === "C" ? rowJSON.subscribedChannels : "", rowJSON);
            return finalJSON;
        },
        /**
         *Method is used to set segment data at GROUP level
         **/
        setGroupLevelAlertsSegData: function(alertTypesData, isInitialLoad) {
            var self = this;
            var widgetMap = {
                "flxGroupAlertsHeaderDW": "flxGroupAlertsHeaderDW",
                "flxGroupAlertsHeaderTablet": "flxGroupAlertsHeaderTablet",
                "flxGroupAlertsContainer": "flxGroupAlertsContainer",
                "flxGroupAlertsContainerTablet": "flxGroupAlertsContainerTablet",
                "flxGroupCheckBox": "flxGroupCheckBox",
                "lblGroupCheckBoxIcon": "lblGroupCheckBoxIcon",
                "lblGroupName": "lblGroupName",
                "flxFrequencyFields": "flxFrequencyFields",
                "lblFrequencyTitle": "lblFrequencyTitle",
                "flxFrequency": "flxFrequency",
                "flxFrequencyBox1": "flxFrequencyBox1",
                "flxFrequencyBox2": "flxFrequencyBox2",
                "flxFrequencyBox3": "flxFrequencyBox3",
                "lstBoxFrequency1": "lstBoxFrequency1",
                "lstBoxFrequency2": "lstBoxFrequency2",
                "lstBoxFrequency3": "lstBoxFrequency3",
                "lblChannelsTitle": "lblChannelsTitle",
                "flxChannels": "flxChannels",
                "flxChannel1": "flxChannel1",
                "lblChannel1": "lblChannel1",
                "flxVerticalSeparator1": "flxVerticalSeparator1",
                "flxChannel2": "flxChannel2",
                "lblChannel2": "lblChannel2",
                "flxVerticalSeparator2": "flxVerticalSeparator2",
                "flxChannel3": "flxChannel3",
                "lblChannel3": "lblChannel3",
                "flxVerticalSeparator3": "flxVerticalSeparator3",
                "flxChannel4": "flxChannel4",
                "lblChannel4": "lblChannel4",
                "flxCategoryAlertRow": "flxCategoryAlertRow",
                "flxAlert": "flxAlert",
                "lblAlertName": "lblAlertName",
                "flxAttributes": "flxAttributes",
                "flxAttributeValues": "flxAttributeValues",
                "flxAlertAttribute1": "flxAlertAttribute1",
                "lblAttributeTitle1": "lblAttributeTitle1",
                "lblFromValue": "lblFromValue",
                "flxAmount1": "flxAmount1",
                "lblCurrencySymbol1": "lblCurrencySymbol1",
                "tbxAmount1": "tbxAmount1",
                "flxAlertAttribute2": "flxAlertAttribute2",
                "lblToValue": "lblToValue",
                "flxAmount2": "flxAmount2",
                "lblCurrencySymbol2": "lblCurrencySymbol2",
                "tbxAmount2": "tbxAmount2",
                "flxAlertAttribute3": "flxAlertAttribute3",
                "lblAttributeTitle3": "lblAttributeTitle3",
                "lblToValue2": "lblToValue2",
                "flxAmount3": "flxAmount3",
                "lblCurrencySymbol3": "lblCurrencySymbol3",
                "tbxAmount3": "tbxAmount3",
                "flxSeperator": "flxSeperator",
                "flxFullSeperator": "flxFullSeperator",
                "showToVal": "showToVal",
                "recordData": "recordData",
                "alertsubtype_id": "alertsubtype_id",
                "alerttype_id": "alerttype_id",
                "supportedChannels": "supportedChannels",
                "subscribedChannels": "subscribedChannels"
            };
            var segData = [];
            var rows = [];
            for (var i = 0; i < alertTypesData.length; i++) {
                var sectionData = self.mapGroupLevelGroupSection(alertTypesData[i], isInitialLoad);
                rows = [];
                if (alertTypesData[i].alertSubTypes.length > 0) {
                    for (var j = 0; j < alertTypesData[i].alertSubTypes.length; j++) {
                        rows.push(self.mapGroupLevelAlertRow(alertTypesData[i].alertSubTypes[j], alertTypesData[i].isSubscribed));
                    }
                    rows[rows.length - 1].flxSeperator.isVisible = false;
                    rows[rows.length - 1].flxFullSeperator.isVisible = true;
                }
                segData.push([sectionData, rows]);
            }
            this.view.settings.segAlertsListing.widgetDataMap = widgetMap;
            this.view.settings.segAlertsListing.setData(segData);
            this.view.forceLayout();
        },
        /**
         *Method is used to set mapping for group section at GROUP level
         **/
        mapGroupLevelGroupSection: function(secData, isInitialLoad) {
            var self = this;
            var isTablet = false;
            var template = "flxGroupAlertsHeaderDW";
            var dropDownSkin = "sknLblOlbFontIconsA0A0A012Px";
            var frqText1 = "";
            var frqText2 = "SELECT";
            var frqText3 = "SELECT";
            if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                template = "flxGroupAlertsHeaderTablet";
                isTablet = true;
            } else {
                template = "flxGroupAlertsHeaderDW";
            }
            if (secData.subscribedFrequency) {
                frqText1 = secData.subscribedFrequency[0].alertFrequencyId;
                frqText2 = secData.subscribedFrequency[0].frequencyValue === "" || secData.subscribedFrequency[0].frequencyValue === "null" || secData.subscribedFrequency[0].frequencyValue === "0" || secData.subscribedFrequency[0].frequencyValue === undefined ? "SELECT" : secData.subscribedFrequency[0].frequencyValue;
                frqText3 = secData.subscribedFrequency[0].frequencyTime ? applicationManager.getFormatUtilManager().getTwelveHourTimeString(secData.subscribedFrequency[0].frequencyTime) : "SELECT";
            }
            if (secData.isSubscribed === "true")
                dropDownSkin = "sknLblFontTypeIcon1a98ff12pxOther";
            var rowJSON = {
                "alerttype_id": secData.alerttype_id,
                "flxGroupCheckBox": {
                    "onClick": self.toggleGroupCheckBox
                },
                "lblGroupCheckBoxIcon": {
                    "text": secData.isSubscribed === "true" ? "C" : "D",
                    "skin": OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN,
                },
                "lblGroupName": {
                    "text": secData.alerttypetext_DisplayName
                },
                "flxFrequencyFields": {
                    "isVisible": self.enableFrequency && frqText1 !== "" ? true : false
                },
                "flxFrequencyBox1": {
                    "width": isTablet === true ? (frqText2 === "SELECT" ? "49%" : "100%") : "28%"
                },
                "flxFrequencyBox3": {
                    "top": isTablet === true ? (frqText2 === "SELECT" ? "0dp" : "50dp") : "0dp"
                },
                "flxFrequencyBox2": {
                    "isVisible": frqText2 === "SELECT" ? false : true
                },
                "lstBoxFrequency1": {
                    "masterData": self.frequencyList.category,
                    "selectedKey": frqText1 === "" ? "DAILY" : frqText1,
                    "onSelection": function() {
                        self.onFrequencySelection(arguments[1], 1)
                    },
                    "skin": "sknlbxalto42424215pxBordere3e3e32pxRadius"
                },
                "lstBoxFrequency2": {
                    "masterData": frqText1 === "MONTHLY" ? self.frequencyList.dates : self.frequencyList.weeks,
                    "selectedKey": frqText2,
                    "onSelection": function() {
                        self.onFrequencySelection(arguments[1], 2)
                    },
                    "skin": "sknlbxalto42424215pxBordere3e3e32pxRadius"
                },
                "lstBoxFrequency3": {
                    "masterData": self.frequencyList.time,
                    "selectedKey": frqText3,
                    "onSelection": function() {
                        self.onFrequencySelection(arguments[1], 3)
                    },
                    "skin": "sknlbxalto42424215pxBordere3e3e32pxRadius"
                },
                "flxChannels": template === "flxGroupAlertsHeaderDW" ? {
                    "width": "180px"
                } : {
                    "width": "220px"
                },
                "flxChannel1": {
                    "skin": "slFbox",
                    "isVisible": true,
                    "onClick": function() {
                        self.channelOnClick(1)
                    }
                },
                "lblChannel1": {
                    "text": "v",
                    "skin": "slFbox",
                    "info": {}
                },
                "flxChannel2": {
                    "skin": "slFbox",
                    "isVisible": true,
                    "onClick": function() {
                        self.channelOnClick(2)
                    }
                },
                "lblChannel2": {
                    "text": "w",
                    "skin": "slFbox",
                    "info": {}
                },
                "flxChannel3": {
                    "skin": "slFbox",
                    "isVisible": true,
                    "onClick": function() {
                        self.channelOnClick(3)
                    }
                },
                "lblChannel3": {
                    "text": "x",
                    "skin": "slFbox",
                    "info": {}
                },
                "flxChannel4": {
                    "skin": "slFbox",
                    "isVisible": true,
                    "onClick": function() {
                        self.channelOnClick(4)
                    }
                },
                "lblChannel4": {
                    "text": "y",
                    "skin": "slFbox",
                    "info": {}
                },
                "flxVerticalSeparator1": {
                    "isVisible": true
                },
                "flxVerticalSeparator2": {
                    "isVisible": true
                },
                "flxVerticalSeparator3": {
                    "isVisible": true
                },
                "supportedChannels": secData.supportedChannels,
                "subscribedChannels": secData.subscribedChannels ? secData.subscribedChannels : "",
                "template": template
            };
            var finalJSON = [];
            finalJSON = self.setAlertChannelsToUI(secData.supportedChannels, rowJSON.lblGroupCheckBoxIcon.text === "C" ? rowJSON.subscribedChannels : "", rowJSON);
            return finalJSON;
        },
        /**
         *Method is used to set mapping for alert section at GROUP/CATEGORY level
         **/
        mapGroupLevelAlertRow: function(rowData, isGroupSubscribed) {
            var isTablet = false;
            var showToVal = false;
            if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                isTablet = true;
            }
            if (rowData.alertCondition && rowData.alertCondition.id === "IN_BETWEEN")
                showToVal = true;
            return {
                "recordData": rowData,
                "alertsubtype_id": rowData.alertsubtype_id,
                "lblAlertName": rowData.alertsubtypetext_displayName,
                "flxAttributes": {
                    "isVisible": isGroupSubscribed === "true" && rowData.alertAttribute ? true : false
                },
                "lblAttributeTitle1": rowData.alertsubtypetext_description,
                "lblFromValue": {
                    "text": kony.i18n.getLocalizedString("i18n.alertSettings.FromValue"),
                    "isVisible": showToVal
                },
               "tbxAmount1": {
                    "text": rowData.alertsubtype_value1 ? rowData.alertsubtype_value1 : "0",
					"onTextChange": function() {
						this.enableButton(this.view.settings.btnAlertSave);
					}.bind(this)
                },
                "flxAmount1": {
                    "skin": "sknBorder727272bgffffff"
                },
                "flxAmount2": {
                    "skin": "sknBorder727272bgffffff"
                },
                "flxAmount3": {
                    "skin": "sknBorder727272bgffffff"
                },
                "flxAlertAttribute2": {
                    "isVisible": isTablet === false && showToVal
                },
                "tbxAmount2": {
                    "text": rowData.alertsubtype_value2 ? rowData.alertsubtype_value2 : "0",
                    "onTextChange": function() {
						this.enableButton(this.view.settings.btnAlertSave);
					}.bind(this)
                },
                "flxAlertAttribute3": {
                    "isVisible": isGroupSubscribed === "true" && isTablet && showToVal ? true : false
                },
                "tbxAmount3": {
                    "text": rowData.alertsubtype_value2 ? rowData.alertsubtype_value2 : "0",
                    "onTextChange": function() {
						this.enableButton(this.view.settings.btnAlertSave);
					}.bind(this)
                },
                "flxSeperator": {
                    "isVisible": true
                },
                "flxFullSeperator": {
                    "isVisible": false
                },
                "showToVal": showToVal,
                "template": "flxCategoryAlertRow"
            }
        },
        /**
         *Method is used to set data for segment at CATEGORY level
         **/
        setCategoryLevelAlertsSegData: function(alertTypesData) {
            var self = this;
            var widgetMap = {
                "flxCategoryAlertsHeader": "flxCategoryAlertsHeader",
                "flxCategoryAlertsContainer": "flxCategoryAlertsContainer",
                "flxGroupCheckBox": "flxGroupCheckBox",
                "lblGroupCheckBoxIcon": "lblGroupCheckBoxIcon",
                "lblGroupName": "lblGroupName",
                "lblGroupAlertsCount": "lblGroupAlertsCount",
                "flxCategoryAlertRow": "flxCategoryAlertRow",
                "flxAlert": "flxAlert",
                "lblAlertName": "lblAlertName",
                "flxAttributes": "flxAttributes",
                "flxAttributeValues": "flxAttributeValues",
                "flxAlertAttribute1": "flxAlertAttribute1",
                "lblAttributeTitle1": "lblAttributeTitle1",
                "lblFromValue": "lblFromValue",
                "flxAmount1": "flxAmount1",
                "lblCurrencySymbol1": "lblCurrencySymbol1",
                "tbxAmount1": "tbxAmount1",
                "flxAlertAttribute2": "flxAlertAttribute2",
                "lblToValue": "lblToValue",
                "flxAmount2": "flxAmount2",
                "lblCurrencySymbol2": "lblCurrencySymbol2",
                "tbxAmount2": "tbxAmount2",
                "flxAlertAttribute3": "flxAlertAttribute3",
                "lblAttributeTitle3": "lblAttributeTitle3",
                "lblToValue2": "lblToValue2",
                "flxAmount3": "flxAmount3",
                "lblCurrencySymbol3": "lblCurrencySymbol3",
                "tbxAmount3": "tbxAmount3",
                "flxSeperator": "flxSeperator",
                "flxFullSeperator": "flxFullSeperator",
                "showToVal": "showToVal",
                "recordData": "recordData",
                "alertsubtype_id": "alertsubtype_id",
                "alerttype_id": "alerttype_id"
            };
            var segData = [];
            var rows = [];
            for (var i = 0; i < alertTypesData.length; i++) {
                var sectionData = self.mapAlertLevelGroupSection(alertTypesData[i]);
                rows = [];
                if (alertTypesData[i].alertSubTypes.length > 0) {
                    for (var j = 0; j < alertTypesData[i].alertSubTypes.length; j++) {
                        rows.push(self.mapGroupLevelAlertRow(alertTypesData[i].alertSubTypes[j], alertTypesData[i].isSubscribed));
                    }
                    rows[rows.length - 1].flxSeperator.isVisible = false;
                    rows[rows.length - 1].flxFullSeperator.isVisible = true;
                }
                segData.push([sectionData, rows]);
            }
            this.view.settings.segAlertsListing.widgetDataMap = widgetMap;
            this.view.settings.segAlertsListing.setData(segData);
            this.view.forceLayout();
        },
        /**
         *Method is used to set channels data for group/alert sections at GROUP/ALERT level respectively
         **/
        setAlertChannelsToUI: function(supportedChannels, subscribedChannels, rowJSON) {
            var channelsData = supportedChannels.split(",");
            var channelIcons = {
                "CH_SMS": {
                    "text": "v",
                    "normalSkin": "sknLblSMS727272FontIcon",
                    "selectedSkin": "sknLblSMS003e75FontIcon"
                },
                "CH_NOTIFICATION_CENTER": {
                    "text": "y",
                    "normalSkin": "sknLblNotification727272FontIcon",
                    "selectedSkin": "sknLblNotification003e75FontIcon"
                },
                "CH_PUSH_NOTIFICATION": {
                    "text": "x",
                    "normalSkin": "sknLblPush727272FontIcon",
                    "selectedSkin": "sknLblPush003e75FontIcon"
                },
                "CH_EMAIL": {
                    "text": "w",
                    "normalSkin": "sknLblEmail727272FontIcon",
                    "selectedSkin": "sknLblEmail003e75FontIcon"
                }
            };
            for (var i = 0; i < channelsData.length; i++) {
                rowJSON["flxChannel" + (i + 1)].skin = "slFbox";
                rowJSON["flxChannel" + (i + 1)].isVisible = true;
                rowJSON["lblChannel" + (i + 1)].text = channelIcons[channelsData[i]].text;
                rowJSON["lblChannel" + (i + 1)].info = {
                    "id": channelsData[i],
                    "isSelected": false,
                    "normalSkin": channelIcons[channelsData[i]].normalSkin,
                    "selectedSkin": channelIcons[channelsData[i]].selectedSkin
                };
                if (subscribedChannels.indexOf(channelsData[i]) >= 0) {
                    rowJSON["lblChannel" + (i + 1)].info.isSelected = true;
                    rowJSON["lblChannel" + (i + 1)].skin = rowJSON["lblChannel" + (i + 1)].info.selectedSkin;
                    rowJSON["flxChannel" + (i + 1)].skin = rowJSON["flxChannel" + i] && rowJSON["flxChannel" + i].skin === "slFbox" ? "bbSknFlxffffffWithShadow1" : "bbSknFlxffffffWithShadow2";
                } else {
                    rowJSON["lblChannel" + (i + 1)].skin = rowJSON["lblChannel" + (i + 1)].info.normalSkin;
                }
            }
            for (var x = i + 1; x <= 4; x++) {
                rowJSON["flxChannel" + x].isVisible = false;
                rowJSON["flxVerticalSeparator" + (x - 1)].isVisible = false;
            }
            if (rowJSON.template === "flxGroupAlertRowDW" || rowJSON.template === "flxGroupAlertRowTablet" ||
                rowJSON.template === "flxGroupAlertsHeaderTablet") {
                rowJSON.flxChannels.width = (channelsData.length) * 55 + "px";
            } else {
                rowJSON.flxChannels.width = (channelsData.length) * 45 + "px";
            }

            return rowJSON;
        },
        /**
         *Method is used to set selected channels skin at both ALERT&GROUP level
         **/
        channelOnClick: function(opt) {
            var secIndex = 0;
            var rowIndex = 0;
            this.enableButton(this.view.settings.btnAlertSave);
            var data = this.view.settings.segAlertsListing.data;
            var selectedRowData = [];
            var isSubscribed = false;
            if (this.alertPreference === "GROUP") {
                secIndex = this.view.settings.segAlertsListing.selectedRowIndex[0];
                selectedRowData = data[secIndex][0];
                isSubscribed = selectedRowData.lblGroupCheckBoxIcon.text === "C" ? true : false;
            } else {
                secIndex = this.view.settings.segAlertsListing.selectedRowIndex[0];
                rowIndex = this.view.settings.segAlertsListing.selectedRowIndex[1];
                selectedRowData = data[secIndex][1][rowIndex];
                isSubscribed = selectedRowData.lblAlertCheckBoxIcon.text === "C" ? true : false;
            }
            if (isSubscribed) { //to enable onclick for only subscribed group/alert
                var info = selectedRowData["lblChannel" + opt].info;
                if (info && info.isSelected === true) {
                    selectedRowData["flxChannel" + opt].skin = "slFbox";
                    selectedRowData["lblChannel" + opt].info.isSelected = false;
                    selectedRowData["lblChannel" + opt].skin = selectedRowData["lblChannel" + opt].info.normalSkin;
                    if (selectedRowData["flxChannel" + (opt + 1)])
                        selectedRowData["flxChannel" + (opt + 1)].skin = selectedRowData["flxChannel" + (opt + 1)].skin === "bbSknFlxffffffWithShadow2" ? "bbSknFlxffffffWithShadow1" : "slFbox";
                } else {
                    selectedRowData["flxChannel" + opt].skin = selectedRowData["flxChannel" + (opt - 1)] && selectedRowData["flxChannel" + (opt - 1)].skin !== "slFbox" ? "bbSknFlxffffffWithShadow2" : "bbSknFlxffffffWithShadow1";
                    selectedRowData["lblChannel" + opt].info.isSelected = true;
                    selectedRowData["lblChannel" + opt].skin = selectedRowData["lblChannel" + opt].info.selectedSkin;
                    if (selectedRowData["flxChannel" + (opt + 1)])
                        selectedRowData["flxChannel" + (opt + 1)].skin = selectedRowData["flxChannel" + (opt + 1)].skin === "bbSknFlxffffffWithShadow1" ? "bbSknFlxffffffWithShadow2" : "slFbox";
                }
                if (this.alertPreference === "GROUP") {
                    data[secIndex][0] = selectedRowData
                    this.view.settings.segAlertsListing.setData(data);
                } else
                    this.view.settings.segAlertsListing.setDataAt(selectedRowData, rowIndex, secIndex);
                this.view.settings.flxAlertsBody.forceLayout();
                this.AdjustScreen();
                this.view.forceLayout();
            }
        },
        /**
         *Method is used to update the alerts data on checkbox click
         **/
        toggleAlertCheckBox: function() {
            this.enableButton(this.view.settings.btnAlertSave);
            var data = this.view.settings.segAlertsListing.data;
            var secIndex = this.view.settings.segAlertsListing.selectedRowIndex[0];
            var selectedSecData = data[secIndex];
            var rowIndex = this.view.settings.segAlertsListing.selectedRowIndex[1];
            var selectedRowData = data[secIndex][1][rowIndex];
            var selectedAlerts = 0;
            if (selectedRowData.lblAlertCheckBoxIcon.text === "D") {
                selectedRowData.lblAlertCheckBoxIcon.text = "C";
                selectedRowData.flxFrequencyFields.isVisible = true;
                selectedRowData.flxAmountAttribute.isVisible = true;
                selectedRowData.flxFrequencyFields.isVisible = selectedRowData.recordData.subscribedFrequency ? true : false;
                selectedRowData.flxAmountAttribute.isVisible = selectedRowData.recordData.alertAttribute ? true : false;
                selectedRowData.flxAlertAttribute3.isVisible = (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) && selectedRowData.showToVal ? true : false;
                selectedRowData = this.setAlertChannelsToUI(selectedRowData.supportedChannels, selectedRowData.subscribedChannels, selectedRowData)
            } else {
                selectedRowData.lblAlertCheckBoxIcon.text = "D";
                selectedRowData.flxFrequencyFields.isVisible = false;
                selectedRowData.flxAmountAttribute.isVisible = false;
                selectedRowData.flxAlertAttribute3.isVisible = false;
                selectedRowData = this.setAlertChannelsToUI(selectedRowData.supportedChannels, "", selectedRowData);
            }
            for (var i = 0; i < selectedSecData[1].length; i++) {
                if (selectedSecData[1][i].lblAlertCheckBoxIcon.text === "C")
                    selectedAlerts++;
            }
            selectedSecData[0].lblGroupCheckBoxIcon.text = selectedAlerts === selectedSecData[1].length ? "C" : selectedAlerts === 0 ? "D" : "z";
            selectedSecData[0].lblGroupAlertsCount.text = "(" + selectedAlerts + "/" + selectedSecData[1].length + ")";
            selectedSecData[0].lblGroupAlertsCount.isVisible = selectedAlerts === 0 ? false : true;
            selectedSecData[1][rowIndex] = selectedRowData;
            data[secIndex] = selectedSecData;
            this.view.settings.segAlertsListing.setData(data);
            this.view.settings.flxAlertsBody.forceLayout();
            this.AdjustScreen();
            this.view.forceLayout();
        },
        /**
         *Method is used to update groups&alerts row data on checkbox click
         **/
        toggleGroupCheckBox: function() {
            this.enableButton(this.view.settings.btnAlertSave);
            var data = this.view.settings.segAlertsListing.data;
            var secIndex = this.view.settings.segAlertsListing.selectedRowIndex[0];
            var selectedSecData = data[secIndex];
            if (this.alertPreference === "GROUP" || this.alertPreference === "CATEGORY") {
                if (selectedSecData[0].lblGroupCheckBoxIcon.text === "D") {
                    selectedSecData[0].lblGroupCheckBoxIcon.text = "C";
                    for (var x = 0; x < selectedSecData[1].length; x++) {
                        selectedSecData[1][x].flxAttributes.isVisible = true;
                        selectedSecData[1][x].flxAlertAttribute3.isVisible = (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) && selectedSecData[1][x].showToVal ? true : false;
                    }
                    if (this.alertPreference === "GROUP")
                        selectedSecData[0] = this.setAlertChannelsToUI(selectedSecData[0].supportedChannels, selectedSecData[0].subscribedChannels, selectedSecData[0]);
                } else {
                    selectedSecData[0].lblGroupCheckBoxIcon.text = "D";
                    for (var x = 0; x < selectedSecData[1].length; x++) {
                        selectedSecData[1][x].flxAttributes.isVisible = false;
                        selectedSecData[1][x].flxAlertAttribute3.isVisible = false;
                    }
                    if (this.alertPreference === "GROUP")
                        selectedSecData[0] = this.setAlertChannelsToUI(selectedSecData[0].supportedChannels, "", selectedSecData[0]);
                }
            } else {
                if (selectedSecData[0].lblGroupCheckBoxIcon.text === "D") {
                    selectedSecData[0].lblGroupAlertsCount.text = "(" + selectedSecData[1].length + "/" + selectedSecData[1].length + ")";
                    selectedSecData[0].lblGroupAlertsCount.isVisible = true;
                    selectedSecData[0].lblGroupCheckBoxIcon.text = "C";
                    for (var y = 0; y < selectedSecData[1].length; y++) {
                        selectedSecData[1][y].lblAlertCheckBoxIcon.text = "C";
                        selectedSecData[1][y].flxFrequencyFields.isVisible = selectedSecData[1][y].recordData.subscribedFrequency ? true : false;
                        selectedSecData[1][y].flxAmountAttribute.isVisible = selectedSecData[1][y].recordData.alertAttribute ? true : false;
                        selectedSecData[1][y].flxAlertAttribute3.isVisible = (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) && selectedSecData[1][y].showToVal ? true : false;
                        selectedSecData[1][y] = this.setAlertChannelsToUI(selectedSecData[1][y].supportedChannels, selectedSecData[1][y].subscribedChannels, selectedSecData[1][y]);
                    }
                } else {
                    selectedSecData[0].lblGroupCheckBoxIcon.text = "D";
                    selectedSecData[0].lblGroupAlertsCount.isVisible = false;
                    for (var y = 0; y < selectedSecData[1].length; y++) {
                        selectedSecData[1][y].lblAlertCheckBoxIcon.text = "D";
                        selectedSecData[1][y].flxFrequencyFields.isVisible = false;
                        selectedSecData[1][y].flxAmountAttribute.isVisible = false;
                        selectedSecData[1][y].flxAlertAttribute3.isVisible = false;
                        selectedSecData[1][y] = this.setAlertChannelsToUI(selectedSecData[1][y].supportedChannels, "", selectedSecData[1][y]);
                    }
                }
            }
            data[secIndex] = selectedSecData;
            this.view.settings.segAlertsListing.setData(data);
            this.view.settings.flxAlertsBody.forceLayout();
            this.AdjustScreen();
            this.view.forceLayout();
        },
            /**
      *Method is used to call enable/disable the segment based on switch selection
    **/
    ToggleSwitchAlerts : function(){
      var scopeObj = this;
      if(this.view.settings.flxAlertsSegment.isVisible){
          if(this.view.settings.lblStatus1.text === OLBConstants.SWITCH_ACTION.ON){
              this.enableAlertsSegment(true);
          }
          else{
              this.enableAlertsSegment(false);
          }
      this.view.forceLayout();
      }
      var isEnabled = this.view.settings.lblStatus1.text === OLBConstants.SWITCH_ACTION.ON ? true : false;
      if(this.alertPreference==="CATEGORY"){
		  this.view.settings.flxChannelsFrequency.setEnabled(isEnabled);
	  }
    },
        /**
         *Method is used to set frequency data based on frequency type selection at CATEGORY level
         **/
        displayBasedOnSelectedFrequency: function() {
            var preference = this.alertPreference;
            this.view.settings.flxFrequency2.setVisibility(true);
            if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                if (this.view.settings.lstBoxFrequency1Tab.selectedKey === "WEEKLY")
                    this.view.settings.lstBoxFrequency2Tab.masterData = this.frequencyList.weeks;
                else if (this.view.settings.lstBoxFrequency1Tab.selectedKey === "MONTHLY")
                    this.view.settings.lstBoxFrequency2Tab.masterData = this.frequencyList.dates;
                else
                    this.view.settings.flxFrequency2Tab.setVisibility(false);
                this.view.settings.lstBoxFrequency2Tab.selectedKey = "SELECT";
                this.view.settings.lstBoxFrequency3Tab.selectedKey = "SELECT";
            } else {
                if (this.view.settings.lstBoxFrequency1.selectedKey === "WEEKLY")
                    this.view.settings.lstBoxFrequency2.masterData = this.frequencyList.weeks;
                else if (this.view.settings.lstBoxFrequency1.selectedKey === "MONTHLY")
                    this.view.settings.lstBoxFrequency2.masterData = this.frequencyList.dates;
                else
                    this.view.settings.flxFrequency2.setVisibility(false);
                this.view.settings.lstBoxFrequency2.selectedKey = "SELECT";
                this.view.settings.lstBoxFrequency3.selectedKey = "SELECT";
            }
        },
        /**
         *Method is used to set frequency data based on frequency type selection at ALERT&GROUP levels
         **/
        onFrequencySelection: function(args, opt) {
            var self = this;
            var secIndex = 0;
            var rowIndex = args.rowIndex;
            var data = this.view.settings.segAlertsListing.data;
            var selectedRowData = [];
            secIndex = args.sectionIndex;
            if (this.alertPreference === "GROUP") {
                selectedRowData = data[secIndex][0];
            } else {
                selectedRowData = data[secIndex][1][rowIndex];
            }
            if (opt === 1) {
                selectedRowData.flxFrequencyBox2.isVisible = true;
                selectedRowData.flxFrequencyBox1.width = (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) ? "100%" : "28%";
                selectedRowData.flxFrequencyBox3.top = (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) ? "50dp" : "0dp";
                if (selectedRowData.lstBoxFrequency1.selectedKey === "WEEKLY")
                    selectedRowData.lstBoxFrequency2 = {
                        "masterData": this.frequencyList.weeks,
                        "selectedKey": "SELECT",
                        "onSelection": function() {
                            self.onFrequencySelection(arguments[1], 2)
                        },
                        "skin": "sknlbxalto42424215pxBordere3e3e32pxRadius"
                    };
                else if (selectedRowData.lstBoxFrequency1.selectedKey === "MONTHLY")
                    selectedRowData.lstBoxFrequency2 = {
                        "masterData": this.frequencyList.dates,
                        "selectedKey": "SELECT",
                        "onSelection": function() {
                            self.onFrequencySelection(arguments[1], 2)
                        },
                        "skin": "sknlbxalto42424215pxBordere3e3e32pxRadius"
                    };
                else {
                    selectedRowData.flxFrequencyBox2.isVisible = false;
                    selectedRowData.flxFrequencyBox1.width = (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) ? "49%" : "28%";
                    selectedRowData.flxFrequencyBox3.top = "0dp";
                }
                selectedRowData.lstBoxFrequency3.selectedKey = "SELECT";
            } else if (opt === 2) {
                selectedRowData.lstBoxFrequency2.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
                selectedRowData.lstBoxFrequency3.selectedKey = "SELECT";
            } else {
                selectedRowData.lstBoxFrequency3.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
            }
            if (this.alertPreference === "GROUP") {
                data[secIndex][0] = selectedRowData;
                this.view.settings.segAlertsListing.setData(data);
            } else {
                data[secIndex][1][rowIndex] = selectedRowData;
                this.view.settings.segAlertsListing.setData(data);
            }
            this.view.forceLayout();
        },
        /**
         *Method is used to set edit page
         **/
        onEditAlertsClick: function() {
            this.view.settings.flxEnableSwitch.setEnabled(true);
            this.view.settings.flxChannelsFrequency.setEnabled(true);
            this.view.settings.flxChannelsFrequencyTab.setEnabled(true);
            this.view.settings.segAlertsListing.setEnabled(this.view.settings.lblStatus1.text === OLBConstants.SWITCH_ACTION.ON?true:false);
            FormControllerUtility.showProgressBar(this.view);
            this.view.settings.btnEditAlerts.setVisibility(false);
            this.view.settings.flxAlertButtons.setVisibility(true);
            this.view.settings.flxAlertsBody.forceLayout();
            this.enableAlertsSegment(this.view.settings.lblStatus1.text === OLBConstants.SWITCH_ACTION.ON?true:false);
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         *Method is used to enable/disable the checkboxes in segment
         * @param boolean (based on toggle switch selected index)
         **/
        enableAlertsSegment: function(isEnable) {
            var checkBoxskin = "sknFontIconCheckBoxSelected";
            this.view.settings.segAlertsListing.setEnabled(true);
            if (isEnable===false) {
                checkBoxskin = "sknC0C0C020pxolbfonticons";
                this.view.settings.segAlertsListing.setEnabled(false);
            }
            var data = this.view.settings.segAlertsListing.data;
            for (var i = 0; i < data.length; i++) {
                data[i][0].lblGroupCheckBoxIcon.skin = checkBoxskin;
                if (this.alertPreference === "ALERT") {
                    for (var j = 0; j < data[i][1].length; j++) {
                        data[i][1][j].lblAlertCheckBoxIcon.skin = checkBoxskin;
                    }
                }
            }
            this.view.settings.segAlertsListing.setData(data);
            if (this.alertPreference === "CATEGORY") {
                this.view.settings.flxChannelsFrequency.setEnabled(isEnable);
            }
            this.view.forceLayout();
        },
        /**
         *Method is used to call updateAlerts by collecting payloads from respective preference based functions
         **/
        onAlertSaveClick: function(alertsViewModel) {
            var alertWidget = this.view.settings;
            var data = this.view.settings.segAlertsCategory.data;
            var requestPayload;
            if (this.isValidData(alertsViewModel)) {
                if (this.alertPreference === "CATEGORY")
                    requestPayload = this.generateCategoryAlertsPayload(alertsViewModel);
                else if (this.alertPreference === "GROUP")
                    requestPayload = this.generateGroupAlertsPayload(alertsViewModel);
                else
                    requestPayload = this.generateAlertLevelAlertsPayload(alertsViewModel);
                if (data[this.selectedCategoryIndex].isAccountLevel === true) {
                    if (alertsViewModel.accountId) {
                        requestPayload.accountId = alertsViewModel.accountId;
                    } else if (alertsViewModel.accountTypeId) {
                        requestPayload.accountTypeId = alertsViewModel.accountTypeId;
                    }
                }
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateAlerts(requestPayload, alertsViewModel);

            }
        },
        /**
         *Method is used to generate update payload at CATEGORY level
         **/
        generateCategoryAlertsPayload: function(alertsModel) {
            var payload = {
                "alertCategoryId": alertsModel.AlertCategoryId,
                "isSubscribed": this.view.settings.lblStatus1.text === OLBConstants.SWITCH_ACTION.ON ? true:false,
                "alertSubscription": {
                    "preferenceLevel": this.alertPreference,
                    "chanls": [],
                    "groups": []
                }
            }
            var alertsViewModel = alertsModel.alertsData;
            var groupsData = [];
            var groupJSON = {};
            var alertJSON = {};
            if (this.view.settings.flxAlertsSegment.isVisible) {
                var data = this.view.settings.segAlertsListing.data;
                for (var i = 0; i < data.length; i++) {
                    groupJSON = {};
                    groupJSON.typeID = data[i][0].alerttype_id;
                    groupJSON.isSub = data[i][0].lblGroupCheckBoxIcon.text !== "D" ? true : false;
                    groupJSON.alerts = [];
                    for (var j = 0; j < data[i][1].length; j++) {
                        alertJSON = {};
                        alertJSON.id = data[i][1][j].alertsubtype_id;
                        alertJSON.value1 = data[i][1][j].tbxAmount1.text;
                        alertJSON.value2 = (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) ? data[i][1][j].tbxAmount3.text : data[i][1][j].tbxAmount2.text;
                        alertJSON.isSub = alertsViewModel.alertTypes[i].alertSubTypes[j].isSubscribed === "true" ? true : false;
                        groupJSON.alerts.push(alertJSON);
                    }
                    groupsData.push(groupJSON);
                }
            }
            payload.alertSubscription.groups = groupsData;
            var channelsData = [];
            var supChanls = alertsViewModel.alertCategory.supportedChannels.split(",");
            for (var x = 1; x <= supChanls.length; x++) {
                if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                    channelsData.push({
                        "id": this.view.settings["lblChannel" + x + "Tab"].info.id,
                        "isSub": this.view.settings["lblChannel" + x + "Tab"].info.isSelected
                    });
                } else {
                    channelsData.push({
                        "id": this.view.settings["lblChannel" + x].info.id,
                        "isSub": this.view.settings["lblChannel" + x].info.isSelected
                    });
                }
            }
            payload.alertSubscription.chanls = channelsData;
            if (((kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) && this.view.settings.flxCategoryFrequencyTab.isVisible)) {
                payload.alertSubscription.freq = {};
                payload.alertSubscription.freq.id = this.view.settings.lstBoxFrequency1Tab.selectedKey;
                if (this.view.settings.lstBoxFrequency1Tab.selectedKey !== "DAILY")
                    payload.alertSubscription.freq.value = this.view.settings.lstBoxFrequency2Tab.selectedKey;
                payload.alertSubscription.freq.time = applicationManager.getFormatUtilManager().getTwentyFourHourTimeString(this.view.settings.lstBoxFrequency3Tab.selectedKey);
            } else if (this.view.settings.flxCategoryFrequency.isVisible) {
                payload.alertSubscription.freq = {};
                payload.alertSubscription.freq.id = this.view.settings.lstBoxFrequency1.selectedKey;
                if (this.view.settings.lstBoxFrequency1.selectedKey !== "DAILY")
                    payload.alertSubscription.freq.value = this.view.settings.lstBoxFrequency2.selectedKey;
                payload.alertSubscription.freq.time = applicationManager.getFormatUtilManager().getTwentyFourHourTimeString(this.view.settings.lstBoxFrequency3.selectedKey);
            }
            return payload;
        },
        /**
         *Method is used to call respective preference level validation functions
         **/
        isValidData: function(alertsModel) {
            var isValid = true;
            var data = this.view.settings.segAlertsListing.data;
            if (this.alertPreference === "CATEGORY") {
                isValid = this.validateCategoryLevel(alertsModel.alertsData);
            } else if (this.alertPreference === "GROUP") {
                isValid = this.validateGroupLevel(alertsModel.alertsData);
            } else {
                isValid = this.validateAlertLevel(alertsModel.alertsData);
            }
            return isValid;
        },
        /**
         *Method is used to validate entered data at CATEGORY level
         **/
        validateCategoryLevel: function(alertsModel) {
            var isValid = true;
            var supportedChans = 0;
            this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.common.errorEditData");
            this.view.settings.flxInvalidAmount.setVisibility(false);
            if (this.view.settings.flxCategoryFrequencyTab.isVisible && kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                this.view.settings.lstBoxFrequency2Tab.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
                this.view.settings.lstBoxFrequency3Tab.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
                if (this.view.settings.flxFrequency2Tab.isVisible && this.view.settings.lstBoxFrequency2Tab.selectedKey === "SELECT") {
                    this.view.settings.lstBoxFrequency2Tab.skin = "sknlbxff0000SSP15px";
                    this.view.settings.flxInvalidAmount.setVisibility(true);
                    isValid = false;
                }
                if (this.view.settings.lstBoxFrequency3Tab.selectedKey === "SELECT") {
                    this.view.settings.flxInvalidAmount.setVisibility(true);
                    this.view.settings.lstBoxFrequency3Tab.skin = "sknlbxff0000SSP15px";
                    isValid = false;
                }
            } else if (this.view.settings.flxCategoryFrequency.isVisible) {
                this.view.settings.lstBoxFrequency2.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
                this.view.settings.lstBoxFrequency3.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
                if (this.view.settings.flxFrequency2.isVisible && this.view.settings.lstBoxFrequency2.selectedKey === "SELECT") {
                    this.view.settings.lstBoxFrequency2.skin = "sknlbxff0000SSP15px";
                    this.view.settings.flxInvalidAmount.setVisibility(true);
                    isValid = false;
                }
                if (this.view.settings.lstBoxFrequency3.selectedKey === "SELECT") {
                    this.view.settings.flxInvalidAmount.setVisibility(true);
                    this.view.settings.lstBoxFrequency3.skin = "sknlbxff0000SSP15px";
                    isValid = false;
                }
            }
            var supChanls = alertsModel.alertCategory.supportedChannels.split(",");
            for (var i = 1; i <= supChanls.length; i++) {
                if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                    if (this.view.settings["lblChannel" + i + "Tab"].info.isSelected === true)
                        supportedChans++;
                } else {
                    if (this.view.settings["lblChannel" + i].info.isSelected === true)
                        supportedChans++;
                }
            }
            if (supportedChans === 0) {
                isValid = false;
                this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.profile.editSecureAccessSettingsError");
                this.view.settings.flxInvalidAmount.setVisibility(true);
                this.view.settings.flxChannels.skin = "sknFlxFF0000Op100Radius3px";
            } else
                this.view.settings.flxChannels.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
            if (this.view.settings.flxAlertsSegment.isVisible) {
                var data = this.view.settings.segAlertsListing.data;
                for (var x = 0; x < data.length; x++) {
                    for (var y = 0; y < data[x][1].length; y++) {
                        data[x][1][y].flxAmount1.skin = "sknBorder727272bgffffff";
                        data[x][1][y].flxAmount2.skin = "sknBorder727272bgffffff";
                        data[x][1][y].flxAmount3.skin = "sknBorder727272bgffffff";
                        if (data[x][1][y].flxAttributes.isVisible) {
                            var amount1 = Number(CommonUtilities.deFormatAmount(data[x][1][y].tbxAmount1.text));
                            if (isNaN(amount1) || amount1 <= 0) {
                                isValid = false;
                                this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.common.errorEditData");
                                this.view.settings.flxInvalidAmount.setVisibility(true);
                                data[x][1][y].flxAmount1.skin = "sknFlxFF0000Op100Radius3px";
                            }
                            if (data[x][1][y].showToVal) {
                                var amount2 = Number(CommonUtilities.deFormatAmount(((kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) ? data[x][1][y].tbxAmount3.text : data[x][1][y].tbxAmount2.text)));
                                if (amount2 <= 0 || isNaN(amount2) || amount2 <= amount1) {
                                    isValid = false;
                                    this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.common.errorEditData");
                                    data[x][1][y].flxAmount2.skin = "sknFlxFF0000Op100Radius3px";
                                    data[x][1][y].flxAmount3.skin = "sknFlxFF0000Op100Radius3px";
                                    this.view.settings.flxInvalidAmount.setVisibility(true);
                                }
                            }
                        }
                    }
                }
                this.view.settings.segAlertsListing.setData(data);
            }
            this.view.settings.flxAlertsBody.forceLayout();
            this.AdjustScreen();
            this.view.forceLayout();
            return isValid;
        },
        /**
         *Method is used to validate entered data at GROUP level
         **/
        validateGroupLevel: function(alertsModel) {
            var isValid = true;
            var supportedChans = 0;
            this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.common.errorEditData");
            this.view.settings.flxInvalidAmount.setVisibility(false);
            if (this.view.settings.flxAlertsSegment.isVisible) {
                var data = this.view.settings.segAlertsListing.data;
                for (var x = 0; x < data.length; x++) {
                    supportedChans = 0;
                    data[x][0].lstBoxFrequency2.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
                    data[x][0].lstBoxFrequency3.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
                    if (data[x][0].lblGroupCheckBoxIcon.text === "C") {
                        if (data[x][0].flxFrequencyFields.isVisible && data[x][0].flxFrequencyBox2.isVisible && data[x][0].lstBoxFrequency2.selectedKey === "SELECT") {
                            data[x][0].lstBoxFrequency2.skin = "sknlbxff0000SSP15px";
                            this.view.settings.flxInvalidAmount.setVisibility(true);
                            isValid = false;
                        }
                        if (data[x][0].flxFrequencyFields.isVisible && data[x][0].lstBoxFrequency3.selectedKey === "SELECT") {
                            this.view.settings.flxInvalidAmount.setVisibility(true);
                            data[x][0].lstBoxFrequency3.skin = "sknlbxff0000SSP15px";
                            isValid = false;
                        }
                        var supChanls = alertsModel.alertTypes[x].supportedChannels.split(",");
                        for (var i = 1; i <= supChanls.length; i++) {
                            if (data[x][0]["lblChannel" + i].info.isSelected === true)
                                supportedChans++;
                        }
                        if (supportedChans === 0) {
                            isValid = false;
                            this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.profile.editSecureAccessSettingsError");
                            this.view.settings.flxInvalidAmount.setVisibility(true);
                            data[x][0].flxChannels.skin = "sknFlxFF0000Op100Radius3px";
                        } else
                            data[x][0].flxChannels.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                        for (var y = 0; y < data[x][1].length; y++) {
                            data[x][1][y].flxAmount1.skin = "sknBorder727272bgffffff";
                            data[x][1][y].flxAmount2.skin = "sknBorder727272bgffffff";
                            data[x][1][y].flxAmount3.skin = "sknBorder727272bgffffff";
                            if (data[x][1][y].flxAttributes.isVisible) {
                                var amount1 = Number(CommonUtilities.deFormatAmount(data[x][1][y].tbxAmount1.text));
                                if (isNaN(amount1) || amount1 <= 0) {
                                    isValid = false;
                                    this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.common.errorEditData");
                                    this.view.settings.flxInvalidAmount.setVisibility(true);
                                    data[x][1][y].flxAmount1.skin = "sknFlxFF0000Op100Radius3px";
                                }
                                if (data[x][1][y].showToVal) {
                                    var amount2 = Number(CommonUtilities.deFormatAmount(((kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) ? data[x][1][y].tbxAmount3.text : data[x][1][y].tbxAmount2.text)));
                                    if (amount2 <= 0 || isNaN(amount2) || amount2 <= amount1) {
                                        isValid = false;
                                        this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.common.errorEditData");
                                        data[x][1][y].flxAmount2.skin = "sknFlxFF0000Op100Radius3px";
                                        data[x][1][y].flxAmount3.skin = "sknFlxFF0000Op100Radius3px";
                                        this.view.settings.flxInvalidAmount.setVisibility(true);
                                    }
                                }
                            }
                        }
                    }
                }
                this.view.settings.segAlertsListing.setData(data);
            }
            this.view.settings.flxAlertsBody.forceLayout();
            this.AdjustScreen();
            this.view.forceLayout();
            return isValid;
        },
        /**
         *Method is used to validate entered data at ALERT level
         **/
        validateAlertLevel: function(alertsModel) {
            var isValid = true;
            var supportedChans = 0;
            this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.common.errorEditData");
            this.view.settings.flxInvalidAmount.setVisibility(false);
            if (this.view.settings.flxAlertsSegment.isVisible) {
                var data = this.view.settings.segAlertsListing.data;
                for (var x = 0; x < data.length; x++) {
                    for (var y = 0; y < data[x][1].length; y++) {
                        supportedChans = 0;
                        if (data[x][1][y].lblAlertCheckBoxIcon.text === "C") {
                            data[x][1][y].lstBoxFrequency2.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
                            data[x][1][y].lstBoxFrequency3.skin = "sknlbxalto42424215pxBordere3e3e32pxRadius";
                            if (data[x][1][y].flxFrequencyFields.isVisible && data[x][1][y].flxFrequencyBox2.isVisible && data[x][1][y].lstBoxFrequency2.selectedKey === "SELECT") {
                                data[x][1][y].lstBoxFrequency2.skin = "sknlbxff0000SSP15px";
                                this.view.settings.flxInvalidAmount.setVisibility(true);
                                isValid = false;
                            }
                            if (data[x][1][y].flxFrequencyFields.isVisible && data[x][1][y].lstBoxFrequency3.selectedKey === "SELECT") {
                                this.view.settings.flxInvalidAmount.setVisibility(true);
                                data[x][1][y].lstBoxFrequency3.skin = "sknlbxff0000SSP15px";
                                isValid = false;
                            }
                            var supChanls = alertsModel.alertTypes[x].alertSubTypes[y].supportedChannels.split(",");
                            for (var i = 1; i <= supChanls.length; i++) {
                                if (data[x][1][y]["lblChannel" + i].info.isSelected === true)
                                    supportedChans++;
                            }
                            if (supportedChans === 0) {
                                isValid = false;
                                this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.profile.editSecureAccessSettingsError");
                                this.view.settings.flxInvalidAmount.setVisibility(true);
                                data[x][1][y].flxChannels.skin = "sknFlxFF0000Op100Radius3px";
                            } else
                                data[x][1][y].flxChannels.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
                            data[x][1][y].flxAmount1.skin = "sknBorder727272bgffffff";
                            data[x][1][y].flxAmount2.skin = "sknBorder727272bgffffff";
                            data[x][1][y].flxAmount3.skin = "sknBorder727272bgffffff";
                            if (data[x][1][y].flxAmountAttribute.isVisible) {
                                var amount1 = Number(CommonUtilities.deFormatAmount(data[x][1][y].tbxAmount1.text));
                                if (isNaN(amount1) || amount1 <= 0) {
                                    isValid = false;
                                    this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.common.errorEditData");
                                    this.view.settings.flxInvalidAmount.setVisibility(true);
                                    data[x][1][y].flxAmount1.skin = "sknFlxFF0000Op100Radius3px";
                                }
                                if (data[x][1][y].showToVal) {
                                    var amount2 = Number(CommonUtilities.deFormatAmount(((kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) ? data[x][1][y].tbxAmount3.text : data[x][1][y].tbxAmount2.text)));
                                    if (amount2 <= 0 || isNaN(amount2) || amount2 <= amount1) {
                                        isValid = false;
                                        this.view.settings.lblInvalidAmount.text = kony.i18n.getLocalizedString("i18n.common.errorEditData");
                                        data[x][1][y].flxAmount2.skin = "sknFlxFF0000Op100Radius3px";
                                        data[x][1][y].flxAmount3.skin = "sknFlxFF0000Op100Radius3px";
                                        this.view.settings.flxInvalidAmount.setVisibility(true);
                                    }
                                }
                            }
                        }
                    }
                }
                this.view.settings.segAlertsListing.setData(data);
            }
            this.view.settings.flxAlertsBody.forceLayout();
            this.AdjustScreen();
            this.view.forceLayout();
            return isValid;
        },
        /**
         *Method is used to generate update payload at GROUP level
         **/
        generateGroupAlertsPayload: function(alertsModel) {
            var payload = {
                "alertCategoryId": alertsModel.AlertCategoryId,
                "isSubscribed": this.view.settings.lblStatus1.text === OLBConstants.SWITCH_ACTION.ON? true:false,
                "alertSubscription": {
                    "preferenceLevel": this.alertPreference,
                    "groups": []
                }
            }
            var alertsViewModel = alertsModel.alertsData;
            var groupsData = [];
            var groupJSON = {};
            var alertJSON = {};
            if (this.view.settings.flxAlertsSegment.isVisible) {
                var data = this.view.settings.segAlertsListing.data;
                for (var i = 0; i < data.length; i++) {
                    groupJSON = {};
                    groupJSON.typeID = data[i][0].alerttype_id;
                    groupJSON.isSub = data[i][0].lblGroupCheckBoxIcon.text !== "D" ? true : false;
                    groupJSON.alerts = [];
                    groupJSON.chanls = [];
                    groupJSON.freq = {};
                    var channelsData = [];
                    var supChanls = alertsViewModel.alertTypes[i].supportedChannels.split(",");
                    for (var x = 1; x <= supChanls.length; x++) {
                        channelsData.push({
                            "id": data[i][0]["lblChannel" + x].info.id,
                            "isSub": data[i][0]["lblChannel" + x].info.isSelected
                        });
                    }
                    groupJSON.chanls = channelsData;
                    if (data[i][0].flxFrequencyFields.isVisible) {
                        groupJSON.freq.id = data[i][0].lstBoxFrequency1.selectedKey;
                        if (data[i][0].lstBoxFrequency1.selectedKey !== "DAILY")
                            groupJSON.freq.value = data[i][0].lstBoxFrequency2.selectedKey;
                        groupJSON.freq.time = applicationManager.getFormatUtilManager().getTwentyFourHourTimeString(data[i][0].lstBoxFrequency3.selectedKey);
                    }
                    for (var j = 0; j < data[i][1].length; j++) {
                        alertJSON = {};
                        alertJSON.id = data[i][1][j].alertsubtype_id;
                        alertJSON.value1 = data[i][1][j].tbxAmount1.text;
                        alertJSON.value2 = (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) ? data[i][1][j].tbxAmount3.text : data[i][1][j].tbxAmount2.text;
                        alertJSON.isSub = alertsViewModel.alertTypes[i].alertSubTypes[j].isSubscribed === "true" ? true : false;
                        groupJSON.alerts.push(alertJSON);
                    }
                    groupsData.push(groupJSON);
                }
            }
            payload.alertSubscription.groups = groupsData;
            return payload;
        },
        /**
         *Method is used to generate update payload at ALERT level
         **/
        generateAlertLevelAlertsPayload: function(alertsModel) {
            var payload = {
                "alertCategoryId": alertsModel.AlertCategoryId,
                "isSubscribed": this.view.settings.lblStatus1.text === OLBConstants.SWITCH_ACTION.ON? true:false,
                "alertSubscription": {
                    "preferenceLevel": this.alertPreference,
                    "groups": []
                }
            }
            var alertsViewModel = alertsModel.alertsData;
            var groupsData = [];
            var groupJSON = {};
            var alertJSON = {};
            if (this.view.settings.flxAlertsSegment.isVisible) {
                var data = this.view.settings.segAlertsListing.data;
                for (var i = 0; i < data.length; i++) {
                    groupJSON = {};
                    groupJSON.typeID = data[i][0].alerttype_id;
                    groupJSON.isSub = data[i][0].lblGroupCheckBoxIcon.text !== "D" ? true : false;
                    groupJSON.alerts = [];
                    for (var j = 0; j < data[i][1].length; j++) {
                        alertJSON = {};
                        alertJSON.id = data[i][1][j].alertsubtype_id;
                        alertJSON.value1 = data[i][1][j].tbxAmount1.text;
                        alertJSON.value2 = (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) ? data[i][1][j].tbxAmount3.text : data[i][1][j].tbxAmount2.text;
                        alertJSON.isSub = data[i][1][j].lblAlertCheckBoxIcon.text === "C" ? true : false;
                        alertJSON.chanls = [];
                        var channelsData = [];
                        var supChanls = alertsViewModel.alertTypes[i].alertSubTypes[j].supportedChannels.split(",");
                        for (var x = 1; x <= supChanls.length; x++) {
                            channelsData.push({
                                "id": data[i][1][j]["lblChannel" + x].info.id,
                                "isSub": data[i][1][j]["lblChannel" + x].info.isSelected
                            });
                        }
                        alertJSON.chanls = channelsData;
                        if (data[i][1][j].flxFrequencyFields.isVisible) {
                            alertJSON.freq = {};
                            alertJSON.freq.id = data[i][1][j].lstBoxFrequency1.selectedKey;
                            if (data[i][1][j].lstBoxFrequency1.selectedKey !== "DAILY")
                                alertJSON.freq.value = data[i][1][j].lstBoxFrequency2.selectedKey;
                            alertJSON.freq.time = applicationManager.getFormatUtilManager().getTwentyFourHourTimeString(data[i][1][j].lstBoxFrequency3.selectedKey);
                        }
                        groupJSON.alerts.push(alertJSON);
                    }
                    groupsData.push(groupJSON);
                }
            }
            payload.alertSubscription.groups = groupsData;
            return payload;
        },
        /**
         *Method is used to enable/disable when isInitialLoad is true at ALERT&GROUP&CATEGORY levels
         **/
        initialAlertsToggle: function(alertsModel) {
            var alertsViewModel = alertsModel.alertsData;
            if (this.alertPreference === "CATEGORY") {
                this.setCategoryChannelsToUI(alertsViewModel.alertCategory.supportedChannels, alertsViewModel.alertCategory.supportedChannels, false);
            }
            for (var i = 0; i < alertsViewModel.alertTypes.length; i++) {
                alertsViewModel.alertTypes[i].isSubscribed = "true";
                if (this.alertPreference === "GROUP")
                    alertsViewModel.alertTypes[i].subscribedChannels = alertsViewModel.alertTypes[i].supportedChannels;
                for (var j = 0; j < alertsViewModel.alertTypes[i].alertSubTypes.length; j++) {
                    alertsViewModel.alertTypes[i].alertSubTypes[j].isSubscribed = "true";
                    if (this.alertPreference === "ALERT")
                        alertsViewModel.alertTypes[i].alertSubTypes[j].subscribedChannels = alertsViewModel.alertTypes[i].alertSubTypes[j].supportedChannels;
                }
            }
            if (alertsViewModel.alertTypes.length === 0) {
                this.view.settings.flxNoAlertsFound.setVisibility(true);
                this.view.settings.flxAlertsSegment.setVisibility(false);
            } else if (this.alertPreference === "ALERT")
                this.setAlertLevelAlertsSegData(alertsViewModel.alertTypes, true);
            else if (this.alertPreference === "GROUP")
                this.setGroupLevelAlertsSegData(alertsViewModel.alertTypes, true);
            else if (this.alertPreference === "CATEGORY")
                this.setCategoryLevelAlertsSegData(alertsViewModel.alertTypes);
            this.view.settings.flxAlertsBody.forceLayout();
            this.AdjustScreen();
            this.view.forceLayout();
        },
        clearCategoryLevelValidation: function() {
            this.view.settings.flxInvalidAmount.setVisibility(false);
            this.view.settings.flxChannels.skin = "sknFlxffffffShadowe3e3e3Rad3Px";
        },
        toggleCheckboxConsent: function(elemItem, elemSubitem) {
            var scopeObj = this;

            var flexRoot = this.view.settings.flxConsentManagementContainer.flxAllConsentTypes.widgets();

            var flxConsentNotificationDetails = flexRoot[elemItem].widgets()[2];
            if (flxConsentNotificationDetails.isVisible !== true && consentEdit == true) {
                var itemItemRowSelected = flexRoot[elemItem].widgets()[5];
                var itemOptions = itemItemRowSelected.widgets();

                var checkboxOption = itemOptions[elemSubitem].widgets();
                if (checkboxOption[0].text === "C") {
                    checkboxOption[0].text = "D";
                } else {
                    checkboxOption[0].text = "C";
                }

                var sameOptionConsent = true;
                for (var i = 0; i < flexRoot.length; i++) {
                    var flxItemRow = flexRoot[i].widgets()[5];
                    var flxOptions = flxItemRow.widgets();
                    for (var j = 0; j < flxOptions.length; j++) {
                        var lblCheckboxOption = flxOptions[j].widgets();

                        if (lblCheckboxOption[0].text !== lblCheckboxOption[0].textInitial) {
                            sameOptionConsent = false;
                            if (typeof consentManagementData[0].consents[i].subTypes !== "undefined" && consentManagementData[0].consents[i].subTypes.length > 0) {
                                if (lblCheckboxOption[0].text === "C") {
                                    consentManagementData[0].consents[i].subTypes[j].consentSubTypeGiven = "YES";
                                } else {
                                    consentManagementData[0].consents[i].subTypes[j].consentSubTypeGiven = "NO";
                                }
                            } else {
                                if (lblCheckboxOption[0].text === "C") {
                                    consentManagementData[0].consents[i].consentGiven = "YES";
                                } else {
                                    consentManagementData[0].consents[i].consentGiven = "NO";
                                }
                            }
                        }
                    }
                }

                if (sameOptionConsent === false)
                    scopeObj.enableButton(this.view.settings.btnConsentManagementSave);
                else
                    scopeObj.disableButton(this.view.settings.btnConsentManagementSave);
            }
        },

        //add function for no data 
        showErrorConsent: function(errorMsg, action) {

            if (action === "get") {
                this.setDataVisibility(false);

            } else {
                consentEdit = true;
                this.view.settings.flxErrorConsent.setVisibility(true);
            }
            this.view.settings.btnEditConsent.setVisibility(false);
            // this.view.settings.lblErrorConsent.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
            this.view.settings.lblErrorConsent.text = errorMsg;
            FormControllerUtility.hideProgressBar(this.view);
            this.showConsentManagementView(this);
        },

        setConsentManagementData: function(data) {
            consentEdit = false;

            this.setDataVisibility(true);
            this.view.settings.flxConsentManagementContainer.flxAllConsentTypes.removeAll();
            this.view.settings.flxConsentManagementButtons.setVisibility(false);

            var scopeObj = this;
            if (!data.consentTypes) {
                scopeObj.view.settings.lblErrorConsent.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.invalidConsent");
                this.setDataVisibility(false);
                //scopeObj.view.settings.lblConsentManagementNotification.skin ="sknLabelSSPFF000015Px"; 
                scopeObj.view.settings.btnEditConsent.setVisibility(false);
            } else {
                consentManagementData = data.consentTypes;
                var resultsDataConsent = data.consentTypes[0].consents;
                var checkConsentBlock = resultsDataConsent.every(o => 'consentBlock' in o);
                scopeObj.view.settings.btnEditConsent.setVisibility((applicationManager.getConfigurationManager().checkUserPermission("CDP_CONSENT_EDIT") && !checkConsentBlock) ? true : false);

                var SAMPLE_CONSENT_TYPE_ITEM = "flxProfileManagementConsent";
                var SAMPLE_CHANNEL_ITEM = "flxProfileManagementConsentOptions";
                var ALL_CONSENT_TYPES = "flxAllConsentTypes";

                for (var item in resultsDataConsent) {
                    var consentTypeItem = this.view.settings[SAMPLE_CONSENT_TYPE_ITEM].clone("flxCloneAllRows" + item);
                    var childWidgets = consentTypeItem.widgets();

                    childWidgets[0].widgets()[0].text = resultsDataConsent[item].consentTypeName;

                    childWidgets[2].setVisibility(resultsDataConsent[item].consentBlock === "YES" ? true : false);

                    if (kony.i18n.getLocalizedString("i18n.ProfileManagement." + resultsDataConsent[item].consentType + "")) {
                        childWidgets[3].widgets()[0].text = kony.i18n.getLocalizedString("i18n.ProfileManagement." + resultsDataConsent[item].consentType + "");
                    } else {
                        childWidgets[3].widgets()[0].text = "Some description for this consent type";
                    }

                    childWidgets[3].accessibilityconfig = {
                        "a11yLabel": "Some description for this consent type"
                    };

                    childWidgets[4].text = kony.i18n.getLocalizedString("i18n.ProfileManagement.blockConsent");

                    var choises = consentTypeItem.widgets()[5];
                    if (resultsDataConsent[item].subTypes !== undefined && resultsDataConsent[item].subTypes.length > 0) {
                        for (var subItem in resultsDataConsent[item].subTypes) {
                            var channelItem = this.view.settings[SAMPLE_CHANNEL_ITEM].clone("flxCloneRow" + subItem + item);
                            var childChannelWidgets = channelItem.widgets();
                            if (kony.i18n.getLocalizedString("i18n.ProfileManagement.consent." + resultsDataConsent[item].subTypes[subItem].consentSubType + "")) {
                                childChannelWidgets[1].text = kony.i18n.getLocalizedString("i18n.ProfileManagement.consent." + resultsDataConsent[item].subTypes[subItem].consentSubType + "");
                            } else {
                                childChannelWidgets[1].text = resultsDataConsent[item].subTypes[subItem].consentSubType;
                            }

                            channelItem.width = (30 + (11 * childChannelWidgets[1].text.length)) + "dp";

                            childChannelWidgets[0].text = resultsDataConsent[item].subTypes[subItem].consentSubTypeGiven === "YES" ? "C" : "D";
                            childChannelWidgets[0].textInitial = resultsDataConsent[item].subTypes[subItem].consentSubTypeGiven === "YES" ? "C" : "D";
                            childChannelWidgets[0].skin = "sknFontIconA0A0A0CheckBoxDisabled";
                            childChannelWidgets[0].accessibilityConfig = {
                                "a11yLabel": resultsDataConsent[item].subTypes[subItem].consentSubTypeGiven === "YES" ? (kony.i18n.getLocalizedString("i18n.Accessibility.checkboxSelected") + " " + resultsDataConsent[item].subTypes[subItem].consentSubType) : (kony.i18n.getLocalizedString("i18n.Accessibility.checkboxUnSelected") + " " + resultsDataConsent[item].subTypes[subItem].consentSubType)
                            };
                            childChannelWidgets[0].onTouchStart = scopeObj.toggleCheckboxConsent.bind(this, item, subItem);
                            choises.add(channelItem);
                            channelItem.isVisible = true;
                        }
                    } else {
                        var channelItemSingle = this.view.settings[SAMPLE_CHANNEL_ITEM].clone("flxCloneRow" + item);
                        channelItemSingle.width = "100%";
                        var childChannelWidgetsSingle = channelItemSingle.widgets();
                        childChannelWidgetsSingle[0].text = resultsDataConsent[item].consentGiven === "YES" ? "C" : "D";
                        childChannelWidgetsSingle[0].textInitial = resultsDataConsent[item].consentGiven === "YES" ? "C" : "D";
                        childChannelWidgetsSingle[0].skin = "sknFontIconA0A0A0CheckBoxDisabled";
                        childChannelWidgetsSingle[0].accessibilityConfig = {
                            "a11yLabel": resultsDataConsent[item].consentGiven === "YES" ? (kony.i18n.getLocalizedString("i18n.Accessibility.checkboxSelected") + " " + resultsDataConsent[item].consentTypeName) : (kony.i18n.getLocalizedString("i18n.Accessibility.checkboxUnSelected") + " " + resultsDataConsent[item].consentTypeName)
                        };
                        childChannelWidgetsSingle[0].onTouchStart = scopeObj.toggleCheckboxConsent.bind(this, item, 0);
                        childChannelWidgetsSingle[1].text = "Allow " + resultsDataConsent[item].consentTypeName;
                        choises.add(channelItemSingle);
                        channelItemSingle.isVisible = true;
                    }
                    this.view.settings[ALL_CONSENT_TYPES].add(consentTypeItem);
                    consentTypeItem.isVisible = true;
                }
            }
            if (applicationManager.getConfigurationManager().checkUserPermission("CDP_CONSENT_VIEW")) {
                scopeObj.showConsentManagementView(this);
            }

        },

        setDataVisibility: function(vissibility) {

            this.view.settings.flxConsentManagementWrapper.setVisibility(vissibility);
            this.view.settings.flxConsentManagementNotification.setVisibility(vissibility);
            this.view.settings.flxErrorConsent.setVisibility(!vissibility);
        },

        onAccountDescrClick: function(item, subItem) {
            var self = this;
            var flxAccountsContainer = self.view.settings.flxMAAContainer.widgets();
            //item, subitem = the clicked row for description
            for (var i = 0; i < flxAccountsContainer.length; i++) {
                var flxBankRow = flxAccountsContainer[i].widgets()[1].widgets()[2];
                var flxAccountsItem = flxBankRow.widgets();
                for (var j = 0; j < flxAccountsItem.length; j++) {
                    //verify if this is the clicked one
                    if (item == i && subItem == j) {
                        var descRow = flxAccountsItem[j].widgets()[1];
                        if (descRow.isVisible) {
                            descRow.isVisible = false;
                            flxAccountsItem[j].widgets()[0].skin = "sknFlxLineUnSelected";
                            flxAccountsItem[j].widgets()[0].widgets()[0].widgets()[0].widgets()[0].src = "listboxuparrow.png";
                        } else {
                  			flxAccountsItem[j].widgets()[0].widgets()[1].isVisible = true;
                            descRow.isVisible = true;
                            flxAccountsItem[j].widgets()[0].skin = "sknFlxLineSelected";
                            flxAccountsItem[j].widgets()[0].widgets()[0].widgets()[0].widgets()[0].src = "listboxdownarrow.png";
                        }
                    } else {
                        var descRow = flxAccountsItem[j].widgets()[1];
                        flxAccountsItem[j].widgets()[0].widgets()[0].widgets()[0].widgets()[0].src = "listboxuparrow.png";
                        descRow.isVisible = false;
                        flxAccountsItem[j].widgets()[0].skin = "sknFlxLineUnSelected";
                    }
                }
            }
          
          
            self.view.forceLayout();
        },
        // is not yet called from anywhere; to be added
        setNoMaaData: function(errorMsg, action) {
            this.view.settings.flxManageAccountAccessWrapper.setVisibility(true);
            this.view.settings.flxMAAErrors.setVisibility(true);
            this.view.settings.lblMAAErrorMessage.text = errorMsg;
            this.view.settings.lblMAAErrorMessage.accessibilityconfig = {
                "a11yLabel": errorMsg
            };
            this.view.settings.flxMAANotification.setVisibility(false);

            if (action === "get") {
                this.view.settings.flxMAAContainer.removeAll();
                this.view.settings.flxMAAContainerContent.removeAll();
                this.view.settings.flxMAAContainerContentDescRow.removeAll();
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.showManageAccountAccessView();
        },
        setExpDate: function(data) {
            var formUt = applicationManager.getFormatUtilManager();
            var configManager = applicationManager.getConfigurationManager();
            var dValid = data.substring(0, 2);
            var mValid = data.substring(3, 6);
            var yValid = data.substring(7, 11);
            var validDate = yValid + "-" + mValid + "-" + dValid;
            var tempDate = formUt.getDateObjectfromString(validDate, "YYYY-MM-DD");
            var dateDiff = formUt.getNumberOfDaysBetweenTwoDates(tempDate, new Date());
            var expiryDate = formUt.getFormatedDateString(new Date(validDate), configManager.frontendDateFormat[configManager.getLocale()]);

            return {
                "daysLeft": dateDiff,
                "expiryDate": expiryDate
            }

        },

        onBtnClick: function(arrangementId, tppName) {
            var self = this;
            this.view.flxRevokedPopup.setVisibility(true);
            this.view.lblRevokedHeadingBank.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.revoked") + tppName + "?";
            this.view.lblRevokedHeadingBank.accessibilityconfig = {
                "a11yLabel": kony.i18n.getLocalizedString("i18n.ProfileManagement.revoked") + tppName + "?"
            };
            this.view.flxCrossRevokedHeading.onClick = function() {
                self.view.flxRevokedPopup.setVisibility(false);
            };
            this.view.btnNoRevoked.onClick = function() {
                self.view.flxRevokedPopup.setVisibility(false);
            };
            this.view.btnYesRevoked.onClick = function() {
                self.view.flxRevokedPopup.setVisibility(false);

                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateRevokeAccountAccess(arrangementId);
                self.view.forceLayout();
            };
        },
        setManageAccountAccessData: function(data) {
            this.view.settings.lblMAANotification.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.FromThirdPartiesNotification");
            this.view.settings.lblMAAHeader.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.FromThirdParties");
            this.view.settings.flxManageAccountAccessWrapper.setVisibility(true);

            this.view.settings.flxMAAContainer.removeAll();
            this.view.settings.flxMAAContainerContent.removeAll();
            this.view.settings.flxMAAContainerContentDescRow.removeAll();

            var self = this;
            var tppExists = false;

            if (data) {

                var allBanksContainer = "flxMAAContainer";
                var allBanksItem = "flxMAAContainerItem";
                var allAccountsItem = "flxMAANameAccount";
                var allDescItem = "flxMAAContainerContentDesc";
                var allAccountsContainer = "flxMAAContainerContent";
                var allDescContainer = "flxMAAContainerContentDescRow";

                var tppIndex = 0;

                for (var item in data) {
                    if (data[item].consentStatus.toUpperCase() !== "REVOKEDBYPSU") {

                        tppExists = true;


                        var bankItem = self.view.settings[allBanksItem].clone("flxBank" + item);
                        var childWidgetsBank = bankItem.widgets();
                        var flexAccounts = childWidgetsBank[1].widgets()[2];

                        if (data[item].tppStatus.toUpperCase() === "BLOCKED") {
                            childWidgetsBank[0].isVisible = true;
                            var blockedMessage = kony.i18n.getLocalizedString("i18n.ProfileManagement.blocked");
                            childWidgetsBank[0].widgets()[1].text = blockedMessage.replace("${bankName}", data[item].thirdpartyprovider);
                            childWidgetsBank[0].widgets()[1].accessibilityconfig = {
                                "a11yLabel": blockedMessage.replace("${bankName}", data[item].thirdpartyprovider)
                            };
                        }
                        if (data[item].consentStatus.toUpperCase() !== "EXPIRED") {
                            childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[1].widgets()[0].text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ExpiresOn") + this.setExpDate(data[item].validUntil).expiryDate;
                            childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[1].widgets()[0].accessibilityconfig = {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.ProfileManagement.ExpiresOn") + this.setExpDate(data[item].validUntil).expiryDate
                            };
                            childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[2].widgets()[0].text = "(" + this.setExpDate(data[item].validUntil).daysLeft + kony.i18n.getLocalizedString("i18n.ProfileManagement.daysLeft") + ")";
                            childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[2].widgets()[0].accessibilityconfig = {
                                "a11yLabel": "(" + this.setExpDate(data[item].validUntil).daysLeft + kony.i18n.getLocalizedString("i18n.ProfileManagement.daysLeft") + ")"
                            };
                            if (this.setExpDate(data[item].validUntil).daysLeft >= 10)
                                childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[2].widgets()[0].skin = "sknLbl04AA1615px";
                            else
                                childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[2].widgets()[0].skin = "sknLblE5690B15px";
                        } else {
                            childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[1].widgets()[0].text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ExpiredOn") + this.setExpDate(data[item].validUntil).expiryDate;
                            childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[1].widgets()[0].accessibilityconfig = {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.ProfileManagement.ExpiredOn") + this.setExpDate(data[item].validUntil).expiryDate
                            };
                            childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[1].widgets()[0].skin = "sknlblff000015px";
                            childWidgetsBank[1].widgets()[0].widgets()[1].widgets()[2].widgets()[0].isVisible = false;
                            childWidgetsBank[1].widgets()[0].widgets()[1].left = "12%";
                        }

                        childWidgetsBank[1].widgets()[0].widgets()[0].widgets()[0].src = (data[item].imagePath != undefined && data[item].imagePath != "") ? data[item].imagePath : "bank_icon.png";
                        childWidgetsBank[1].widgets()[0].widgets()[0].widgets()[1].text = data[item].thirdpartyprovider;
                        childWidgetsBank[1].widgets()[0].widgets()[0].widgets()[1].accessibilityconfig = {
                            "a11yLabel": data[item].thirdpartyprovider
                        };

                        childWidgetsBank[1].widgets()[3].isVisible = (applicationManager.getConfigurationManager().checkUserPermission("PSD2_TPP_CONSENT_REVOKE") ? true : false);
                        
                       this.view.settings.flxMAAContainerContentLine.isVisible = (applicationManager.getConfigurationManager().checkUserPermission("PSD2_TPP_CONSENT_REVOKE") ? true : false);
                      childWidgetsBank[1].widgets()[3].widgets()[0].onClick = self.onBtnClick.bind(null, data[item].arrangementId, data[item].thirdpartyprovider);

                        for (var subItem in data[item].accountIBANs) {
                            var accountItem = self.view.settings[allAccountsItem].clone("flxAccount" + item + subItem);
                            var childWidgetsAccount = accountItem.widgets();
                            childWidgetsAccount[0].widgets()[0].widgets()[0].onClick = self.onAccountDescrClick.bind(this, tppIndex, subItem);
                            childWidgetsAccount[0].widgets()[0].widgets()[1].widgets()[0].text = data[item].accountIBANs[subItem].shortName;
                            childWidgetsAccount[0].widgets()[0].widgets()[1].widgets()[0].accessibilityconfig = {
                                "a11yLabel": data[item].accountIBANs[subItem].shortName
                            };
                            let iban = data[item].accountIBANs[subItem].accountIBAN;
                            if (iban.length > 6){
                              iban = iban.match(/.{1,4}/g).join(' ');
                            }
                            childWidgetsAccount[0].widgets()[0].widgets()[2].widgets()[0].text = iban;
                            childWidgetsAccount[0].widgets()[0].widgets()[1].widgets()[0].accessibilityconfig = {
                                "a11yLabel": data[item].accountIBANs[subItem].accountIBAN
                            };
                            var flexDescr = childWidgetsAccount[1];

                            for (var subItemDesc in data[item].accountIBANs[subItem].consents) {
                                var descItem = self.view.settings[allDescItem].clone("flxAccount" + item + subItem + subItemDesc);
                                var childWidgetsDesc = descItem.widgets();
                                if (data[item].accountIBANs[subItem].consents[subItemDesc].consentType !== "allPsd2") {
                                    childWidgetsDesc[1].widgets()[0].text = kony.i18n.getLocalizedString("i18n.ProfileManagement.consent.short." + data[item].accountIBANs[subItem].consents[subItemDesc].consentType.toUpperCase() + "");
                                    childWidgetsDesc[1].widgets()[0].accessibilityconfig = {
                                        "a11yLabel": kony.i18n.getLocalizedString("i18n.ProfileManagement.consent.short." + data[item].accountIBANs[subItem].consents[subItemDesc].consentType.toUpperCase() + "")
                                    };
                                    childWidgetsDesc[1].widgets()[1].text = kony.i18n.getLocalizedString("i18n.ProfileManagement.consent.long." + data[item].accountIBANs[subItem].consents[subItemDesc].consentType.toUpperCase() + "");
                                    childWidgetsDesc[1].widgets()[1].accessibilityconfig = {
                                        "a11yLabel": kony.i18n.getLocalizedString("i18n.ProfileManagement.consent.long." + data[item].accountIBANs[subItem].consents[subItemDesc].consentType.toUpperCase() + "")
                                    };
                                    descItem.isVisible = true;
                                    flexDescr.add(descItem);
                                }
                            }
                            accountItem.isVisible = true;
                            flexAccounts.add(accountItem);
                        }
                        self.view.settings[allBanksContainer].add(bankItem);


                        bankItem.isVisible = true;
                        tppIndex += 1;
                    }
			

                }

            }

            if (tppExists === false) {
                this.setNoMaaData(kony.i18n.getLocalizedString("i18n.ProfileManagement.consent.psd2.noConsents"), "get");

            } else {
              
                this.view.settings.flxMAANotificationError.setVisibility(false);
                this.view.settings.flxMAANotification.setVisibility(true);
                this.view.settings.flxMAAErrors.setVisibility(false);
            }

            if (applicationManager.getConfigurationManager().checkUserPermission("PSD2_TPP_CONSENT_VIEW")) {
                self.showManageAccountAccessView(this);
            }
        },
		showErrorMessageForApprovalMatrix : function(error){
		  this.view.flxDowntimeWarning.isVisible = true;
		  this.view.rtxDowntimeWarning.text = error.errorMessage;
		  if(error.isContractsEmpty){
			this.view.settings.flxApprovalMatrixDetailsWrapper.isVisible = false;
		  }
		  else{
			this.view.settings.flxApprovalMatrixDetailsWrapper.isVisible = true;
		  }
		  this.view.forceLayout();
		},
    
		showApprovalMatrixContractDetails : function(data){
		  var contracts = data.contractDetails;
		  var contractFilter = data.contractFilter;
		  this.view.flxDowntimeWarning.isVisible = false;
		  this.showViews(["flxApprovalMatrixDetailsWrapper"]);
          this.toggle(this.view.settings.flxApprovalMatrix, this.view.settings.lblApprovalMatrixCollapse);
          this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.ProfileManagement.Settingscapson"),kony.i18n.getLocalizedString("i18n.Settings.ApprovalMatrix.approvalMatrix"));
		  this.view.settings.segContractDetails.widgetDataMap = this.getContractsWidgetDataMap();
		  this.contractsApprovalMatrix = [this.getSegmentDataForContracts(contracts)];
		  this.view.settings.segContractDetails.setData(this.contractsApprovalMatrix);
		  this.setContractFilterData(contractFilter);
		  this.view.settings.tbxSearch.text = "";
		  this.view.settings.lblSelectedFilter.text = "All";
		  this.view.settings.tbxSearch.onKeyUp = this.searchContractDetails;
		  FormControllerUtility.hideProgressBar(this.view);
		  this.view.forceLayout();
		},

		getSegmentDataForContracts : function(contracts){
		  var sectionData = this.getSectionDataForContracts();
		  var rowData = this.getRowDataForContracts(contracts);
		  return([sectionData, rowData]);
		},
		
		getSectionDataForContracts : function(){
		  return({
			"lblCustomerNameHeader": {
			  "text": "Customer Name"
			},
			"lblCustomerIDHeader": {
			  "text": "Customer ID"
			},
			"lblContractHeader": {
			  "text": "Contract"
			},
			"lblActionHeader": {
			  "text": "Action"
			},
			"lblTopSeparator": {
			  "text": "-"
			},
			"lblBottomSeparator": {
			  "text": "-"
			},
			"imgCustomerNameSort": {
			  "src": "sorting.png"
			},
			"imgContractSort": {
			  "src": "sorting.png"
			},
			"flxCustomerName": {
			  "onClick" : this.sortContractsData.bind(this, "imgCustomerNameSort", this.getContractsWidgetDataMap()["lblCustomerName"])
			},
			"flxContract": {
			  "onClick" : this.sortContractsData.bind(this, "imgContractSort", this.getContractsWidgetDataMap()["lblContract"])
			}
		  });
		},
		
		getRowDataForContracts : function(contracts){
		  var scopeObj = this;
		  var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController;
		  if(contracts.length === 0){
			return([this.adjustRowContainers(contracts)]);
		  }
		  contracts.forEach(contract => {
			contract.btnAction = {
			   "text" : profileModule.getActionNameBasedOnPermissions(),
			  onClick : function(eventobject, context){
				var rowData = scopeObj.view.settings.segContractDetails.data[context.sectionIndex][1][context.rowIndex];
				rowData.entryPoint = kony.application.getCurrentForm().id;
				profileModule.isApprovalMatrixDisabled(rowData);
			  }
			};
			contract = Object.assign(contract, this.adjustRowContainers(contracts));
		  });
		  return contracts;
		},
		adjustRowContainers : function(rowData){
		  var isEmpty = (rowData.length === 0);
		  return({
			"flxNoRecords" : {
			  "isVisible" : isEmpty
			},
			"lblNoRecords" : {
			  "text" : "No records are available"
			},
			"flxHeadersContainer" : {
			  "isVisible" : !isEmpty
			},
			"lblRowSeparator" : {
			  "isVisible" : !isEmpty,
			  "text" : "-"
			},
            "flxApprovalMatrixContractRow" : {
              "height" : isEmpty ? "300dp" : "45dp"
            }
		  });
		},
		
		getContractsWidgetDataMap : function(){
		  return({
			"lblCustomerNameHeader":"lblCustomerNameHeader",
			"lblCustomerIDHeader":"lblCustomerIDHeader",
			"lblContractHeader":"lblContractHeader",
			"lblActionHeader":"lblActionHeader",
			"imgCustomerNameSort":"imgCustomerNameSort",
			"imgContractSort":"imgContractSort",
			"lblTopSeparator":"lblTopSeparator",
			"lblBottomSeparator":"lblBottomSeparator",
			"flxCustomerName":"flxCustomerName",
			"flxContract":"flxContract",
			"lblRowSeparator":"lblRowSeparator",
			"lblCustomerName":"coreCustomerName",
			"lblCustomerID":"coreCustomerID",
			"lblContract":"contractName",
			"btnAction":"btnAction",
			"flxNoRecords":"flxNoRecords",
			"lblNoRecords":"lblNoRecords",
			"flxHeadersContainer":"flxHeadersContainer",
            "flxApprovalMatrixContractRow":"flxApprovalMatrixContractRow"
		  });
		},
		sortContractsData : function(imgWidget, sortKey){
		  FormControllerUtility.showProgressBar(this.view);
		  var segmentData = this.view.settings.segContractDetails.data;
		  var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController;
		  profileModule.sortSegmentData(segmentData, 0, imgWidget, sortKey);
		  this.view.settings.segContractDetails.setData(segmentData);
		  FormControllerUtility.hideProgressBar(this.view);
		  this.view.forceLayout();
		},
		
		setContractFilterData : function(filter){
		  this.view.settings.segFilter.widgetDataMap = {"lblName" : "contract"};
		  this.view.settings.segFilter.setData(filter);
		  this.view.settings.segFilter.onRowClick = function(segWidget, sectionIndex, rowIndex){
			var filterValue = filter[rowIndex].contract;
			this.view.settings.lblSelectedFilter.text = filterValue;
			this.filterContractDetails(filterValue);
			this.view.settings.flxFilter.isVisible = false;
			this.view.settings.lblDropDown.text = "O";
			this.view.forceLayout();
		  }.bind(this);
		  this.view.settings.flxIcon.onClick = function(){
			var icon = this.view.settings.lblDropDown.text;
			if(icon === "O"){
			  this.view.settings.lblDropDown.text = "P";
			  this.view.settings.flxFilter.isVisible = true;
			}
			else{
			  this.view.settings.lblDropDown.text = "O";
			  this.view.settings.flxFilter.isVisible = false;
			}
			this.view.forceLayout();
		  }.bind(this);
		},
		
		filterContractDetails : function(filterKey){
		  FormControllerUtility.showProgressBar(this.view);
		  var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController;
		  var filteredData = profileModule.filterSegmentData(this.contractsApprovalMatrix, 0, filterKey);
		  if(filteredData[0][1].length === 0)
			filteredData[0][1] = [this.adjustRowContainers([])];
		  this.view.settings.segContractDetails.setData(filteredData);
		  this.filteredContractsData = filteredData;
		  FormControllerUtility.hideProgressBar(this.view);
		  this.view.forceLayout();
		},
		
		searchContractDetails : function(){
		  FormControllerUtility.showProgressBar(this.view);
		  var seachKey = this.view.settings.tbxSearch.text;
		  var segmentData = this.contractsApprovalMatrix;
		  if(this.view.settings.lblSelectedFilter.text !== "All"){
			segmentData = this.filteredContractsData;
		  }
		  var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController;
		  var filteredData = profileModule.searchSegmentData(segmentData, 0, seachKey);
		  if(filteredData[0][1].length === 0)
			filteredData[0][1] = [this.adjustRowContainers([])];
		  this.view.settings.segContractDetails.setData(filteredData);
		  FormControllerUtility.hideProgressBar(this.view);
		  this.view.forceLayout();
		},
      
        showRegisteredDevices: function(){  
          this.collapseAll();
          this.expand(this.view.settings.flxDeviceManagementSubMenu);
          this.setSelectedSkin("flxRegisterDevices");
          this.view.deviceRegistration.left = this.view.settings.flxRight.info.frame.x + 5 + this.view.settings.info.frame.x + "dp";
          this.view.deviceRegistration.top = "65dp";
          this.view.deviceRegistration.width = this.view.settings.flxRight.info.frame.width - 3 + "dp";
          this.view.deviceRegistration.height = this.view.settings.flxRight.info.frame.height - 5 + "dp";
          this.showViews({"isComponent" : true, "componentName" : "deviceRegistration"});
          this.view.deviceRegistration.fetchDevices();
        }
    }
});