define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnSave **/
    AS_Button_c8ea025c0db24c678d59f914069014d0: function AS_Button_c8ea025c0db24c678d59f914069014d0(eventobject) {
        var self = this;
        this.SaveTermsAndConditions();
    },
    /** onClick defined for btnCancel **/
    AS_Button_j11414a39cff46a1a18e242e3d9058a7: function AS_Button_j11414a39cff46a1a18e242e3d9058a7(eventobject) {
        var self = this;
        this.CloseTermsAndConditions();
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_cff920382e1e477ab3b92c9f4b59d7fc: function AS_FlexContainer_cff920382e1e477ab3b92c9f4b59d7fc(eventobject) {
        var self = this;
        this.CloseTermsAndConditions();
    },
    /** init defined for frmProfileManagement **/
    AS_Form_a2e2f9a8fbd64f00a8033cd9cd1c8fd9: function AS_Form_a2e2f9a8fbd64f00a8033cd9cd1c8fd9(eventobject) {
        var self = this;
        this.initProfileSettingsMenu();
    },
    /** preShow defined for frmProfileManagement **/
    AS_Form_a9b44bbdcde74e61aa1cc479ec66074d: function AS_Form_a9b44bbdcde74e61aa1cc479ec66074d(eventobject) {
        var self = this;
        this.preShowProfileManagement();
    },
    /** postShow defined for frmProfileManagement **/
    AS_Form_b7b4d384166e4c168c5651b714b239fe: function AS_Form_b7b4d384166e4c168c5651b714b239fe(eventobject) {
        var self = this;
        this.postShowProfileManagement();
    },
    /** onTouchEnd defined for frmProfileManagement **/
    AS_Form_d366c36537dd41778b825ab8f10d506f: function AS_Form_d366c36537dd41778b825ab8f10d506f(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onDeviceBack defined for frmProfileManagement **/
    AS_Form_fe83062d2bdb4ffb973260d7c19d2fd8: function AS_Form_fe83062d2bdb4ffb973260d7c19d2fd8(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** updateFormUI defined for deviceRegistration **/
    AS_UWI_a7bacb08b8f94236a94ad6784b65c188: function AS_UWI_a7bacb08b8f94236a94ad6784b65c188(context) {
        var self = this;
        return self.updateFormUI.call(this, context);
    }
});