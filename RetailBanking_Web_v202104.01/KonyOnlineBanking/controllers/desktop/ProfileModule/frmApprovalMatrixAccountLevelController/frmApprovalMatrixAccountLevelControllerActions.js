define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for frmApprovalMatrixAccountLevel **/
    AS_Form_ff74e9e179074e76a50347d55a9d4e4b: function AS_Form_ff74e9e179074e76a50347d55a9d4e4b(eventobject) {
        var self = this;
        this.preShow();
    },
    /** postShow defined for frmApprovalMatrixAccountLevel **/
    AS_Form_ic723dab44b8417badd1000b2378f4ba: function AS_Form_ic723dab44b8417badd1000b2378f4ba(eventobject) {
        var self = this;
        this.postShow();
    },
    /** onDeviceBack defined for frmApprovalMatrixAccountLevel **/
    AS_Form_j4a2a5a4c2df4bf3b80e7fe32105131b: function AS_Form_j4a2a5a4c2df4bf3b80e7fe32105131b(eventobject) {
        var self = this;
        this.onClickOfDeviceBack("Logout");
    }
});