define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEdit **/
    AS_Button_a84c5cd25ad1432189d1e006491e9034: function AS_Button_a84c5cd25ad1432189d1e006491e9034(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnRepeat **/
    AS_Button_fdc36b0e77b34c39a8ea3585360caa65: function AS_Button_fdc36b0e77b34c39a8ea3585360caa65(eventobject, context) {
        var self = this;
        var currForm = kony.application.getCurrentForm();
        _kony.mvc.GetController(currForm.id, true).presenter.onloadPayABill()
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_fbbe8e9045694e08a49bfc6c43e7a4fa: function AS_FlexContainer_fbbe8e9045694e08a49bfc6c43e7a4fa(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    }
});