define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnbacktopayeelist **/
    AS_Button_gfca6ca3cbbb479984fc8086573b5775: function AS_Button_gfca6ca3cbbb479984fc8086573b5775(eventobject) {
        var self = this;
        this.presenter.getExternalAccounts();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_c07b23e24d3848b49fbc4c340c51fac1: function AS_FlexContainer_c07b23e24d3848b49fbc4c340c51fac1(eventobject) {
        var self = this;
        //this.addExternalAccount();
        this.presenter.showDomesticAccounts();
    },
    /** onTouchStart defined for imgViewCVVCode **/
    AS_Image_a4bd5e13fa5141d3926bf37116fbca13: function AS_Image_a4bd5e13fa5141d3926bf37116fbca13(eventobject, x, y) {
        var self = this;
        this.showPassword();
    },
    /** onTouchEnd defined for imgViewCVVCode **/
    AS_Image_f16dbe07074540a983f230053ff19894: function AS_Image_f16dbe07074540a983f230053ff19894(eventobject, x, y) {
        var self = this;
        this.hidePassword();
    },
    /** onEndEditing defined for tbxAnswers1 **/
    AS_TextField_ad4bf1541e204eb2a1e50ded9a715b7d: function AS_TextField_ad4bf1541e204eb2a1e50ded9a715b7d(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers1 **/
    AS_TextField_jff625b76b684c679f1d436aca05f128: function AS_TextField_jff625b76b684c679f1d436aca05f128(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onEndEditing defined for tbxAnswers2 **/
    AS_TextField_e256eb58f2df41d39994d0b2398e9d7e: function AS_TextField_e256eb58f2df41d39994d0b2398e9d7e(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers2 **/
    AS_TextField_fe5ae42e836f4b9ca9a49f267c7094a5: function AS_TextField_fe5ae42e836f4b9ca9a49f267c7094a5(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** init defined for frmTransfersGateway **/
    AS_Form_g8b999a5329045869cc51191e9b0dd76: function AS_Form_g8b999a5329045869cc51191e9b0dd76(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmTransfersGateway **/
    AS_Form_f20e3dfe89634c188e2bac81e363ba03: function AS_Form_f20e3dfe89634c188e2bac81e363ba03(eventobject) {
        var self = this;
        this.initTabsActions();
    },
    /** postShow defined for frmTransfersGateway **/
    AS_Form_da100ee263ca4f7ba32db1e4a057a9bd: function AS_Form_da100ee263ca4f7ba32db1e4a057a9bd(eventobject) {
        var self = this;
        this.postShowtransfers();
    },
    /** onDeviceBack defined for frmTransfersGateway **/
    AS_Form_c19eed724b57466ea509c5803cc6ca2d: function AS_Form_c19eed724b57466ea509c5803cc6ca2d(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmTransfersGateway **/
    AS_Form_i364d65c6f574708903770999eb6e963: function AS_Form_i364d65c6f574708903770999eb6e963(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
});