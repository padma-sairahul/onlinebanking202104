define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxClickAction14 **/
    AS_FlexContainer_a89e46d9351649f89681f1725b77854a: function AS_FlexContainer_a89e46d9351649f89681f1725b77854a(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction18 **/
    AS_FlexContainer_ab68c64ed9324dfebc84dc240d6d217b: function AS_FlexContainer_ab68c64ed9324dfebc84dc240d6d217b(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction17 **/
    AS_FlexContainer_b7c0dc090bca4ba4b62fde8abb95de9a: function AS_FlexContainer_b7c0dc090bca4ba4b62fde8abb95de9a(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction12 **/
    AS_FlexContainer_b97e8d521566424b8d47a6199595ed56: function AS_FlexContainer_b97e8d521566424b8d47a6199595ed56(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction3 **/
    AS_FlexContainer_cf1d995ebc1f453b92ac8e263b534c86: function AS_FlexContainer_cf1d995ebc1f453b92ac8e263b534c86(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction6 **/
    AS_FlexContainer_d935c95abdd14f53a07d9833d4f01747: function AS_FlexContainer_d935c95abdd14f53a07d9833d4f01747(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, eventobject);
    },
    /** onClick defined for flxClickAction16 **/
    AS_FlexContainer_e2185f487f91414db85c7648e74eec7d: function AS_FlexContainer_e2185f487f91414db85c7648e74eec7d(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction2 **/
    AS_FlexContainer_e23aac8fc9f643aca6ab58db88277436: function AS_FlexContainer_e23aac8fc9f643aca6ab58db88277436(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction11 **/
    AS_FlexContainer_ead196aa0dfc47279646011ab1b818fe: function AS_FlexContainer_ead196aa0dfc47279646011ab1b818fe(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction9 **/
    AS_FlexContainer_fa64802c24eb402ba1a2deac34ce7b03: function AS_FlexContainer_fa64802c24eb402ba1a2deac34ce7b03(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction7 **/
    AS_FlexContainer_fed10f5ed39c4090bceb353a3aeb2e1c: function AS_FlexContainer_fed10f5ed39c4090bceb353a3aeb2e1c(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction4 **/
    AS_FlexContainer_g2ff793cebb64642b020e6067357277c: function AS_FlexContainer_g2ff793cebb64642b020e6067357277c(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction1 **/
    AS_FlexContainer_g85a45e4ce2742378312d2de46a43e0c: function AS_FlexContainer_g85a45e4ce2742378312d2de46a43e0c(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction20 **/
    AS_FlexContainer_h12f4414ffee480290e2a07cbf310d8f: function AS_FlexContainer_h12f4414ffee480290e2a07cbf310d8f(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction5 **/
    AS_FlexContainer_h884fc3aaa01441391e8fcb8f153faab: function AS_FlexContainer_h884fc3aaa01441391e8fcb8f153faab(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction10 **/
    AS_FlexContainer_h9efb41ba0234e53a239dd2533597117: function AS_FlexContainer_h9efb41ba0234e53a239dd2533597117(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction15 **/
    AS_FlexContainer_hc0b3f06ca1b44ca81906d6de9d4a85b: function AS_FlexContainer_hc0b3f06ca1b44ca81906d6de9d4a85b(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction8 **/
    AS_FlexContainer_he1f9a91ed2f463082f40c607c7297e7: function AS_FlexContainer_he1f9a91ed2f463082f40c607c7297e7(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxPermissionClickable **/
    AS_FlexContainer_i0528db1d86e4cd29d2604ab57cf2df6: function AS_FlexContainer_i0528db1d86e4cd29d2604ab57cf2df6(eventobject, context) {
        var self = this;
        return self.selectOrUnselectEntireFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction19 **/
    AS_FlexContainer_j50521e4a504479d85a19e0e1a3ed8fc: function AS_FlexContainer_j50521e4a504479d85a19e0e1a3ed8fc(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction13 **/
    AS_FlexContainer_j6a1e0e05edf47bdab50c721cbbb7b98: function AS_FlexContainer_j6a1e0e05edf47bdab50c721cbbb7b98(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    }
});