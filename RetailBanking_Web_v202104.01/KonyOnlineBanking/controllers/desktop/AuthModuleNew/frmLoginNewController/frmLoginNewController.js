define({
    //Type your controller code here
    currentBreakpoint: 1366,
    frmInit: function() {
        var scope = this;
        this.view.postShow = this.frmPostShow;
        this.view.onBreakpointChange = function() {
            scope.onBreakpointChange(kony.application.getCurrentBreakpoint());
        };
        this.view.flxFormContent.doLayout = this.frmResized;
        this.view.flxCheckBox.onClick = this.rememberMeCheckboxToggle;
    },
    frmPostShow: function() {
        this.onBreakpointChange(kony.application.getCurrentBreakpoint());
    },
    rememberMeCheckboxToggle: function() {
        if (this.view.lblRememberMeCheckbox.text === "D") {
            this.view.lblRememberMeCheckbox.skin = "skn0273e320pxolbfonticons";
            this.view.lblRememberMeCheckbox.text = "C";
        } else {
            this.view.lblRememberMeCheckbox.skin = "sknC0C0C020pxolbfonticons"; //sknOlbFontsIconse3e3e3
            this.view.lblRememberMeCheckbox.text = "D";
        }
    },
    onBreakpointChange: function(breakpoint) {
        this.currentBreakpoint = breakpoint;
        if (breakpoint === 1024) {
            this.setTabletView();
        } else if (breakpoint === 640) {
            this.setMobileView();
        } else {
            this.setDesktopView();
        }
        this.view.LoginFooter.onBreakpointChangeComponent(breakpoint);
    },
    setDesktopView: function() {
        this.view.flxLoginContainer.skin = "sknFlxBgFFFFFFNoBdrShadow104c7d";
        this.frmResized();
    },
    setTabletView: function() {
        this.view.flxLoginContainer.skin = "sknFlxBgFFFFFFBdr6pxShadow104c7d";
        this.frmResized();
    },
    setMobileView: function() {
        this.view.flxLoginContainer.skin = "sknFlxBgFFFFFFBdr6pxShadow104c7d";
        this.frmResized();
    },
    frmResized: function() {
        if (this.currentBreakpoint === 640 || this.currentBreakpoint === 1024) {
            this.view.flxMain.minHeight = this.view.flxFormContent.frame.height - 80 + "dp";
            this.view.flxFooter.width = "100%";
        } else {
            if (this.view.flxFormContent.frame.height > 630) {
                this.view.flxLoginContainer.height = this.view.flxFormContent.frame.height + "dp";
            } else {
                this.view.flxLoginContainer.height = "630dp";
            }
            this.view.flxFooter.width = this.view.flxFormContent.frame.width - 500 + "dp";
        }
    }
});