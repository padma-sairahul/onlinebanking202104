define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for frmLocateUs **/
    AS_Form_a53ed9c9cdb645898d1c6a3cbcd07ce6: function AS_Form_a53ed9c9cdb645898d1c6a3cbcd07ce6(eventobject) {
        var self = this;
        this.locateUsPreshow();
    },
    /** onTouchEnd defined for frmLocateUs **/
    AS_Form_c359f271da264268a61a5e6d1e47746a: function AS_Form_c359f271da264268a61a5e6d1e47746a(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onDeviceBack defined for frmLocateUs **/
    AS_Form_c726cc5535004e8286163944bcd6c94d: function AS_Form_c726cc5535004e8286163944bcd6c94d(eventobject) {
        var self = this;
        kony.print("on device back");
    },
    /** postShow defined for frmLocateUs **/
    AS_Form_f61793624dac44e2a92a68d8a966552d: function AS_Form_f61793624dac44e2a92a68d8a966552d(eventobject) {
        var self = this;
        this.postShowLocateUs();
    }
});