define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onDeviceBack defined for frmACHDashboard **/
    AS_Form_a04c0fcf671f4d559eaa3cbb036f5f9e: function AS_Form_a04c0fcf671f4d559eaa3cbb036f5f9e(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** postShow defined for frmACHDashboard **/
    AS_Form_a611f37546134d9bae565a5819191f20: function AS_Form_a611f37546134d9bae565a5819191f20(eventobject) {
        var self = this;
        this.onPostShow();
    },
    /** init defined for frmACHDashboard **/
    AS_Form_b5b5c16384c34a4c886b45b78ac4e29f: function AS_Form_b5b5c16384c34a4c886b45b78ac4e29f(eventobject) {
        var self = this;
        this.onInit();
    },
    /** preShow defined for frmACHDashboard **/
    AS_Form_e6da823cdc264fb7945bb9272152d72c: function AS_Form_e6da823cdc264fb7945bb9272152d72c(eventobject) {
        var self = this;
        return self.onPreShow.call(this);
    },
    /** onTouchEnd defined for frmACHDashboard **/
    AS_Form_i3356ad2edb245dab7586732b2a6916e: function AS_Form_i3356ad2edb245dab7586732b2a6916e(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});