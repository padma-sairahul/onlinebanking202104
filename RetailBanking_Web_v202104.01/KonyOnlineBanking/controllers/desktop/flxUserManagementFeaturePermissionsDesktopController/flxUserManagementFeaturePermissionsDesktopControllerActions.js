define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxClickAction13 **/
    AS_FlexContainer_a429ff2b397c460490b044463bf5e792: function AS_FlexContainer_a429ff2b397c460490b044463bf5e792(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onTouchStart defined for flxTooltip **/
    AS_FlexContainer_a5b5201925f447aeb0c76287b44a38d3: function AS_FlexContainer_a5b5201925f447aeb0c76287b44a38d3(eventobject, x, y, context) {
        var self = this;
        this.onInfoTouchStart(eventobject, context);
    },
    /** onClick defined for flxPermissionClickable **/
    AS_FlexContainer_a85d2a9f3bc048bbb867bc634167e80f: function AS_FlexContainer_a85d2a9f3bc048bbb867bc634167e80f(eventobject, context) {
        var self = this;
        this.selectOrUnselectEntireFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction9 **/
    AS_FlexContainer_b132af6fa2784c989212ef1375f351f3: function AS_FlexContainer_b132af6fa2784c989212ef1375f351f3(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction10 **/
    AS_FlexContainer_c115d004128e4a50827bf1156800da6b: function AS_FlexContainer_c115d004128e4a50827bf1156800da6b(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction17 **/
    AS_FlexContainer_c130fa5d607d4326b5256afe9c6423a6: function AS_FlexContainer_c130fa5d607d4326b5256afe9c6423a6(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction11 **/
    AS_FlexContainer_ccbfa3f33fcb4b77880280c8efb38031: function AS_FlexContainer_ccbfa3f33fcb4b77880280c8efb38031(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction20 **/
    AS_FlexContainer_d097120d4ad64a519c77bd8e00e93e18: function AS_FlexContainer_d097120d4ad64a519c77bd8e00e93e18(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction12 **/
    AS_FlexContainer_d9b6f2a37a4b45139e3c03301be56a13: function AS_FlexContainer_d9b6f2a37a4b45139e3c03301be56a13(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction6 **/
    AS_FlexContainer_dec92d61a9fe4306a9669712863cc8db: function AS_FlexContainer_dec92d61a9fe4306a9669712863cc8db(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction18 **/
    AS_FlexContainer_e0cf991b251f4d94bb05a199b8d004cc: function AS_FlexContainer_e0cf991b251f4d94bb05a199b8d004cc(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction8 **/
    AS_FlexContainer_e279564b50cf47e5b603b7fd84edc800: function AS_FlexContainer_e279564b50cf47e5b603b7fd84edc800(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction1 **/
    AS_FlexContainer_e7229cbbf652490d8423e4bc89e83ede: function AS_FlexContainer_e7229cbbf652490d8423e4bc89e83ede(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction5 **/
    AS_FlexContainer_f000869b60f04214812bd2515a9e869e: function AS_FlexContainer_f000869b60f04214812bd2515a9e869e(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onTouchEnd defined for flxTooltip **/
    AS_FlexContainer_f3fa848976e54d469bfc875a701df72a: function AS_FlexContainer_f3fa848976e54d469bfc875a701df72a(eventobject, x, y, context) {
        var self = this;
        this.onInfoTouchEnd(eventobject, context);
    },
    /** onClick defined for flxClickAction2 **/
    AS_FlexContainer_f57d65b921824d68b26939a7e69ba38c: function AS_FlexContainer_f57d65b921824d68b26939a7e69ba38c(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction19 **/
    AS_FlexContainer_g1603bee1bcf4fee924f82a90fdda159: function AS_FlexContainer_g1603bee1bcf4fee924f82a90fdda159(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction15 **/
    AS_FlexContainer_h2f2950079cd40a1a6a14f2ff8ce473c: function AS_FlexContainer_h2f2950079cd40a1a6a14f2ff8ce473c(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction16 **/
    AS_FlexContainer_h4bda4def3044119abe1bc01881384ad: function AS_FlexContainer_h4bda4def3044119abe1bc01881384ad(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, null, null);
    },
    /** onClick defined for flxClickAction7 **/
    AS_FlexContainer_i2d46855cfa945499b2c5cfd559bd333: function AS_FlexContainer_i2d46855cfa945499b2c5cfd559bd333(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction14 **/
    AS_FlexContainer_iac1414e27f548d0aee83311d068bc56: function AS_FlexContainer_iac1414e27f548d0aee83311d068bc56(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction4 **/
    AS_FlexContainer_iacc1535eb1b4422b39036b71fe28394: function AS_FlexContainer_iacc1535eb1b4422b39036b71fe28394(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction3 **/
    AS_FlexContainer_j1f126c1d30046b0a1f8c1447b39de2c: function AS_FlexContainer_j1f126c1d30046b0a1f8c1447b39de2c(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    }
});