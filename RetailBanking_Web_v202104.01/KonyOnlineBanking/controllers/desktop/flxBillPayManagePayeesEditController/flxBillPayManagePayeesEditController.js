define({
    segmentManagePayeesRowClick: function() {
        var currForm = kony.application.getCurrentForm();
        var index = currForm.tableView.segmentBillpay.selectedRowIndex[1];
        var data = currForm.tableView.segmentBillpay.data;
        data[index].imgDropdown = "arrow_down.png";
        data[index].template = "flxBillPayManagePayees";
        currForm.tableView.segmentBillpay.setDataAt(data[index], index);
        this.AdjustScreen(-320);
    },
    viewEBill: function() {
        var currForm = kony.application.getCurrentForm();
        var height_to_set = 140 + currForm.flxContainer.info.frame.height;
        currForm.flxViewEbill.height = height_to_set + "dp";
        currForm.flxViewEbill.isVisible = true;
        this.AdjustScreen(30);
        currForm.forceLayout();
    },
    showPayABill: function() {
        var currForm = kony.application.getCurrentForm();
        currForm.payABill.isVisible = true;
        currForm.breadcrumb.setBreadcrumbData([{
            text: "BILL PAY"
        }, {
            text: "PAY A BILL"
        }]);
        currForm.btnConfirm.setVisibility(false);
        currForm.tableView.isVisible = false;
        this.AdjustScreen(30);
    },
    viewBillPayActivity: function() {
        var currForm = kony.application.getCurrentForm();
        var height_to_set = 120 + currForm.flxContainer.info.frame.height;
        currForm.flxBillPayManagePayeeActivity.height = height_to_set + "dp";
        currForm.transferActivity.height = height_to_set + "dp";
        currForm.flxBillPayManagePayeeActivity.setVisibility(true);
        this.AdjustScreen(30);
        currForm.forceLayout();
    },
    AdjustScreen: function(data) {
        var currentForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currentForm.customheader.info.frame.height + currentForm.flxContainer.info.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currentForm.flxFooter.info.frame.height;
            if (diff > 0)
                currentForm.flxFooter.top = mainheight + diff + data + "dp";
            else
                currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        } else {
            currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        }
    },
});