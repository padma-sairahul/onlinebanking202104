/**
 * Description of Module representing a New Account Opening form.
 * @module frmNAOController
 */
define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function(CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {
    var orientationHandler = new OrientationHandler();
    var loanData = {};
    return /** @alias module:frmNAOController */ {
        /** updates the present Form based on required function.
         * @param {uiDataMap[]} uiDataMap list of functions
         */
        updateFormUI: function(uiDataMap) {
            if (uiDataMap.showLoadingIndicator) {
                if (uiDataMap.showLoadingIndicator.status === true) {
                    FormControllerUtility.showProgressBar(this.view);
                } else {
                    FormControllerUtility.hideProgressBar(this.view);
                }
            }
            if (uiDataMap.enableDownTimeWarning) {
                this.view.rtxDowntimeWarning.text = uiDataMap.enableDownTimeWarning;
                this.view.flxDownTimeWarning.setVisibility(true);
                this.AdjustScreen();
            }
            if (uiDataMap.enableflxAccounts) {
                this.hideAll();
                this.view.LoanDetails.lblFavoriteEmailCheckBox.text = "D";
                FormControllerUtility.disableButton(this.view.LoanDetails.btnConfirm);
                this.view.flxAccounts.setVisibility(true);
                this.AdjustScreen();
            }
            if (uiDataMap.enableflxLoanDetails) {
                this.hideAll();
                this.view.flxLoanDetails.setVisibility(true);
                this.AdjustScreen();
            }
            if (uiDataMap.enableflxReviewApplication) {
                this.hideAll();
                this.loanData = uiDataMap.enteredData;
                this.setReviewApplicationData(uiDataMap.enteredData);
                this.view.flxReviewApplication.setVisibility(true);
                this.AdjustScreen();
            }
            if (uiDataMap.modifyflxReviewApplication) {
                this.hideAll();
                this.populateLoansData(this.loanData);
                this.view.flxLoanDetails.setVisibility(true);
                this.AdjustScreen();
            }
            if (uiDataMap.enableAcknowledgement) {
                this.hideAll();
                this.populateAcknowledgementData(this.loanData);
                this.view.flxNAOAcknowledgement.setVisibility(true);
                this.AdjustScreen();
            }
        },

        /**
         * used to set the actions to the  NAO On clicks
         */
        setFlowActions: function() {
            var self = this;
            this.view.onBreakpointChange = function() {
                self.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
            this.view.CampaignDescription.btnAccountsDescriptionCancel.onClick = function() {
                self.NavigateToAccountsLandingForm();
            };
            this.view.CampaignDescription.btnAccountsDescriptionProceed.onClick = function() {
                var context = {
                    "enableflxLoanDetails": true
                };
                self.updateFormUI(context);
            };
            this.view.LoanDetails.btnCancel.onClick = function() {
                var context = {
                    "enableflxAccounts": true
                };
                self.updateFormUI(context);
            }
            this.view.LoanDetails.btnloanDetailsCancel.onClick = function() {
                self.NavigateToAccountsLandingForm();
            }
            this.view.LoanDetails.btnConfirm.onClick = function() {
                self.validateLoanAmount();
            };
            this.view.confirmDialog.confirmButtons.btnModify.onClick = function() {
                var context = {
                    "modifyflxReviewApplication": true
                };
                self.updateFormUI(context);
            };
            this.view.confirmDialog.confirmButtons.btnCancel.onClick = function() {
                //this.view.flxSubmitApplications.setVisibility(true);
                self.view.flxDownTimeWarning.setVisibility(false);
                self.view.flxSubmit.setVisibility(true);
            };
            this.view.SubmitForm.btnNo.onClick = function() {
                self.view.flxSubmit.setVisibility(false);
                self.view.forceLayout();
            };
            this.view.SubmitForm.btnYes.onClick = function() {
                self.view.flxSubmit.setVisibility(false);
                self.NavigateToAccountsLandingForm();
            };
            this.view.SubmitForm.flxCross.onClick = function() {
                self.view.flxSubmit.setVisibility(false);
                self.view.forceLayout();
            };
            this.view.confirmDialog.confirmButtons.btnConfirm.onClick = this.callLoanService;
            this.view.LoanDetails.lblFavoriteEmailCheckBox.onTouchStart = function() {
                self.toggleProductSelectionCheckbox();
            };
            this.view.AcknowledgementCM.btnAckGoToAccounts.onClick = function() {
                self.navigateToAccounts();
            };

            this.view.LoanDetails.tbxLoanPayOffAmount.onKeyUp = function() {
                self.populateAmounts();
            };
            this.view.LoanDetails.lbxSelectReasonNew.onSelection = function() {
                self.calInterestRate(self.view.LoanDetails.lbxSelectReasonNew.selectedKey);
                self.populateAmounts();
            };
            this.view.LoanDetails.btnTermsAndConditions.onClick = function() {
                self.view.flxTermsAndConditions.setVisibility(true);
                self.AdjustScreen();
            };
            this.view.flxClose.onClick = function() {
                self.view.flxTermsAndConditions.setVisibility(false);
                self.AdjustScreen();
            };
        },

        callLoanService: function() {
            var CampaignManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('CampaignManagement');
            CampaignManagementModule.presentationController.createLoanAccount(this.loanData);
        },

        /*Populates the data in Loan Details form*/
        populateLoansData: function(loanData) {
            this.view.LoanDetails.lblProductName.text = loanData.productName;
            this.view.LoanDetails.tbxLoanPayOffAmount.text = loanData.loanAmt;
            if (loanData.duration > 12)
                this.view.LoanDetails.lbxSelectReasonNew.selectedKey.text = (loanData.duration / 12) + "Years";
            else
                this.view.LoanDetails.lbxSelectReasonNew.selectedKey.text = loanData.duration + "Months";
            this.view.LoanDetails.lblInterestRate.text = loanData.interestRate;
            this.view.LoanDetails.lblIntrestAmt.text = loanData.interestAmt;
            this.view.LoanDetails.lblTotalRepaymenyAmt.text = loanData.totalRepayAmt;
            this.view.LoanDetails.tbxOptional.text = loanData.reason;
        },

        /*Populates the data in reView form*/
        setReviewApplicationData: function(loanData) {
            var self = this;
            this.view.confirmDialog.keyValueFrom.lblValue.text = loanData.productName;
            this.view.confirmDialog.keyValueTo.lblValue.text = loanData.loanAmt;
            if (loanData.duration > 12) {
                this.view.confirmDialog.keyValueAmount.lblValue.text = loanData.duration / 12 + " " + "Years";
                self.loanData.duration = (loanData.duration / 12) + "Y";
            } else {
                this.view.confirmDialog.keyValueAmount.lblValue.text = loanData.duration + " " + "Months";
                self.loanData.duration = (loanData.duration) + "M";
            }
            this.view.confirmDialog.keyValuePaymentDate.lblValue.text = loanData.interestRate;
            this.view.confirmDialog.keyValueFrequency.lblValue.text = loanData.interestAmt;
            this.view.confirmDialog.keyValueFrequencyType.lblValue.text = loanData.totalRepayAmt;
            this.view.confirmDialog.keyValueNote.lblValue.text = loanData.reason;
        },

        /*Populates the data in Acknowledgement form*/
        populateAcknowledgementData: function(loanData) {
            this.view.AcknowledgementCM.confirmDialog.lblValue.text = loanData.productName;
            this.view.AcknowledgementCM.confirmDialog.lblValueTo.text = loanData.loanAmt;
            this.view.AcknowledgementCM.confirmDialog.RichText0bfcfe81705e240.text = this.view.confirmDialog.keyValueAmount.lblValue.text;
            this.view.AcknowledgementCM.confirmDialog.CopyRichText0j287807d04b44c.text = loanData.interestRate;
            this.view.AcknowledgementCM.confirmDialog.lblValueDescription.text = loanData.interestAmt;
            this.view.AcknowledgementCM.confirmDialog.CopylblValueDescription0e7e996a47e5744.text = loanData.totalRepayAmt;
            this.view.AcknowledgementCM.confirmDialog.CopylblValueDescription0b68a3d2d9c5942.text = loanData.reason;
        },

        enteredData: function() {
            var data = {
                productName: this.view.LoanDetails.lblProductName.text,
                loanAmt: this.view.LoanDetails.tbxLoanPayOffAmount.text,
                duration: this.view.LoanDetails.lbxSelectReasonNew.selectedKey,
                interestRate: this.view.LoanDetails.lblInterestRate.text,
                interestAmt: this.view.LoanDetails.lblIntrestAmt.text,
                totalRepayAmt: this.view.LoanDetails.lblTotalRepaymenyAmt.text,
                reason: this.view.LoanDetails.tbxOptional.text
            };
            return data;
        },


        /**
         * Method to load and return NAO Module.
         * @returns {object} NAO Module object.
         */
        loadNAOModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
        },

        /**
         * Form preShow action handler
         */
        preshow: function() {
            var scopeObj = this;
            this.hideAll();
            this.view.flxAccounts.setVisibility(true);
            applicationManager.getNavigationManager().applyUpdates(this);
            this.view.LoanDetails.lblFavoriteEmailCheckBox.text = "D";
            FormControllerUtility.disableButton(this.view.LoanDetails.btnConfirm);
            this.view.LoanDetails.tbxLoanPayOffAmount.text = applicationManager.getConfigurationManager().getCurrencyCode() + "5,000.00";
            this.view.LoanDetails.lblIntrestAmt.text = applicationManager.getConfigurationManager().getCurrencyCode() + "400.00";
            this.view.LoanDetails.lblTotalRepaymenyAmt.text = applicationManager.getConfigurationManager().getCurrencyCode() + "400.00";
            this.view.LoanDetails.lblAmount.text = applicationManager.getConfigurationManager().getCurrencyCode() + "10,000";
            this.view.LoanDetails.lbxSelectReasonNew.selectedKey = "6";
            this.populateAmounts();
            this.setFlowActions();
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Campaign Management");
            FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxFooter', 'flxHeader', 'flxContainer','flxFormContent']);
            //this.loadNAOModule().presentationController.getCampaignsNAO();
        },

        showReviewApplication: function(products) {
            if (kony.application.getCurrentBreakpoint() === 640) {
                this.view.customheader.lblHeaderMobile.text = "Review Application";
            } else {
                this.view.customheader.lblHeaderMobile.text = "";
            }
            this.hideAll();
            this.view.flxReviewApplication.setVisibility(true);
            this.view.ReviewApplicationNAO.flxColorLine.setFocus(true);
            this.setUserData();
            this.setSelectedAccountSegmentData(products);
            this.view.ReviewApplicationNAO.btnEdit.onClick = this.showSelectedProductsList;
            this.view.ReviewApplicationNAO.btnReviewApplicationSubmit.onClick = this.performCreditCheck.bind(this, this.saveProducts.bind(this, products));
            //this.AdjustScreen();
        },



        /** 
         * get the total form height
         * @returns {string} height
         */
        getTotalFormHeight: function() {
            return this.view.flxHeader.frame.height + this.view.flxContainer.frame.height + this.view.flxFooter.frame.height + "dp";
        },




        /**
         * Toggle Select Product Checkbox of Select Products Segment
         * @param {object}  index of row
         */
        toggleProductSelectionCheckbox: function() {
            if (this.view.LoanDetails.lblFavoriteEmailCheckBox.text === "C") {
                this.view.LoanDetails.lblFavoriteEmailCheckBox.text = "D";
                FormControllerUtility.disableButton(this.view.LoanDetails.btnConfirm);
            } else {
                this.view.LoanDetails.lblFavoriteEmailCheckBox.text = "C";
                FormControllerUtility.enableButton(this.view.LoanDetails.btnConfirm);
            }
        },


        /**
         * shows the acknowledgement scrren
         * @param {object} acknowledgementView acknowledgementViewModel
         */
        showAcknowledgement: function(acknowledgementView) {
            this.showAcknowledgementUI();
            this.setSelectedProductsData(acknowledgementView.selectedProducts)
            this.view.AcknowledgementNAO.btnAckFundAccounts.onClick = function() {
                var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                accountModule.presentationController.showAccountsDashboard();
            }
            if (kony.application.getCurrentBreakpoint() == 640) {
                this.view.customheader.lblHeaderMobile.text = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");
                this.view.customheader.forceLayout();
            } else {
                this.view.customheader.lblHeaderMobile.text = "";
            }
        },


        /**
         * shows the AcknowledgementUI
         */
        showAcknowledgementUI: function() {
            this.hideAll();
            this.view.flxNAOAcknowledgement.setVisibility(true);
            this.view.AcknowledgementNAO.btnAckGoToAccounts.setVisibility(false);
            this.view.AcknowledgementNAO.btnAckFundAccounts.text = kony.i18n.getLocalizedString("i18n.NAO.GoToAccounts");
            this.view.AcknowledgementNAO.btnAckFundAccounts.toolTip = kony.i18n.getLocalizedString("i18n.NAO.GoToAccounts");
            this.AdjustScreen();
        },

        /**
         * hides the all UI
         */
        hideAll: function() {
            this.view.flxDownTimeWarning.setVisibility(false);
            this.view.flxAccounts.setVisibility(false);
            this.view.flxNAOAcknowledgement.setVisibility(false);
            this.view.flxReviewApplication.setVisibility(false);
            this.view.flxLoanDetails.setVisibility(false);
        },
        /**
         * post show of the form
         */
        postshowfrmNAO: function() {
            this.AdjustScreen();
        },

        /**
         * used to navigate the account landing form
         */
        NavigateToAccountsLandingForm: function() {
            var frmName;
            var configurationManager = applicationManager.getConfigurationManager();
            if (configurationManager.isSMEUser === "true")
                frmName = "frmDashboard";
            else if (configurationManager.isCombinedUser === "true")
                frmName = "frmDashboard";
            else
                frmName = "frmDashboard";
            applicationManager.getNavigationManager().navigateTo(frmName);
        },


        /**
         *AdjustScreen- function to adjust the footer
         */
        AdjustScreen: function() {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight =
                this.view.customheader.frame.height +
                this.view.flxContainer.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0) {
                   // this.view.flxFooter.top = mainheight + diff + "dp";
                } else {
                  //  this.view.flxFooter.top = mainheight + "dp";
                }
            } else {
              //  this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
        },


        /**
         * onBreakpointChange : Handles ui changes on .
         * @member of {frmNAOController}
         * @param {integer} width - current browser width
         * @return {} 
         * @throws {}
         */
        onBreakpointChange: function(width) {
            var scope = this;
            this.view.CustomPopupLogout.onBreakpointChangeComponent(scope.view.CustomPopupLogout, width)
            this.AdjustScreen();
        },
        calInterestRate: function(Duration) {
            if (Duration == "6")
                this.view.LoanDetails.lblInterestRate.text = "13.5%";
            if (Duration == "12")
                this.view.LoanDetails.lblInterestRate.text = "12.8%";
            if (Duration == "24")
                this.view.LoanDetails.lblInterestRate.text = "12.3%";
            if (Duration == "36")
                this.view.LoanDetails.lblInterestRate.text = "11.9%";
        },
        navigateToAccounts: function() {
            var AccountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule');
            AccountsModule.presentationController.showAccountsDashboard(null, this.loanData);
            //applicationManager.getNavigationManager().navigateTo("frmAccountsLanding"); 
        },
        /*This function is used to calculate Interest*/
        calculateInterest: function() {
            var principalAmt = this.view.LoanDetails.tbxLoanPayOffAmount.text;
            principalAmt = Number(principalAmt.replace(/[&\/\\#,+()€$~'":*?<>{}]/g, ''));
            var rateOfInterest = this.view.LoanDetails.lblInterestRate.text;
            rateOfInterest = Number(rateOfInterest.replace(/[&\/\\#,+()$€~%'":*?<>{}]/g, ''));
            var duration = Number(this.view.LoanDetails.lbxSelectReasonNew.selectedKey);
            var interest = (principalAmt * (1 + (rateOfInterest / 100) * (duration / 12)) - principalAmt).toFixed(2);
            return this.numberWithCommas(interest);
        },
        /*This function is used to calculate Monthly payment amount*/
        calculateEMI: function() {
            var principalAmt = this.view.LoanDetails.tbxLoanPayOffAmount.text;
            principalAmt = Number(principalAmt.replace(/[&\/\\#$,+()€~%'":*?<>{}]/g, ''));
            var duration = Number(this.view.LoanDetails.lbxSelectReasonNew.selectedKey);
            var interest = Number(this.calculateInterest().replace(/[&\/\$\#,+()€~%'":*?<>{}]/g, ''));
            var emi = ((interest + principalAmt) / duration).toFixed(2);
            return this.numberWithCommas(emi);
        },
        numberWithCommas: function(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        },
        populateAmounts: function() {
            var self = this;
            var currencycode = applicationManager.getConfigurationManager().getCurrencyCode();
            var tbxLoanPayOffAmount = this.view.LoanDetails.tbxLoanPayOffAmount.text;
            if (tbxLoanPayOffAmount.indexOf(currencycode) == -1) {
                this.view.LoanDetails.tbxLoanPayOffAmount.text = currencycode + this.view.LoanDetails.tbxLoanPayOffAmount.text;
            }
            this.loanData = this.enteredData;
            this.view.LoanDetails.lblIntrestAmt.text = applicationManager.getConfigurationManager().getCurrencyCode() + this.calculateInterest();
            this.view.LoanDetails.lblTotalRepaymenyAmt.text = applicationManager.getConfigurationManager().getCurrencyCode() + this.calculateEMI();
            self.enableSkins();
        },
        /*This function is used to validate form*/
        validateLoanAmount: function() {
            var self = this;
            var principalAmt = Number(this.view.LoanDetails.tbxLoanPayOffAmount.text.replace(/[&\/\\#$,+()€~'":*?<>{}]/g, ''));
            var eligibleAmt = Number(this.view.LoanDetails.lblAmount.text.replace(/[&\/\\#,+()€~%'":$*?<>{}]/g, ''));
            if (principalAmt < eligibleAmt) {
                var context = {
                    "enableflxReviewApplication": true,
                    "enteredData": self.enteredData()
                };
                this.view.LoanDetails.tbxLoanPayOffAmount.skin = "skntxt727272";
                this.view.LoanDetails.flxError.setVisibility(false);

                self.updateFormUI(context);

            } else {
                if ((principalAmt > eligibleAmt) && this.view.LoanDetails.lblFavoriteEmailCheckBox.text == "D") {
                    this.view.LoanDetails.lblError.text = "Loan amount must be less than Eligible Amount and enable terms and conditions";
                    this.view.LoanDetails.tbxLoanPayOffAmount.skin = "skntxtSSP424242BorderFF0000Op100Radius2px";
                    this.view.LoanDetails.flxError.setVisibility(true);
                    FormControllerUtility.disableButton(this.view.LoanDetails.btnConfirm);
                    this.AdjustScreen();
                } else if (principalAmt > eligibleAmt) {
                    this.view.LoanDetails.lblError.text = "Loan amount must be less than Eligible Amount";
                    this.view.LoanDetails.tbxLoanPayOffAmount.skin = "skntxtSSP424242BorderFF0000Op100Radius2px";
                    this.view.LoanDetails.lblFavoriteEmailCheckBox.text = "D";
                    this.view.LoanDetails.flxError.setVisibility(true);
                    FormControllerUtility.disableButton(this.view.LoanDetails.btnConfirm);
                    this.AdjustScreen();
                } else if (this.view.LoanDetails.lblFavoriteEmailCheckBox.text == "D") {
                    this.view.LoanDetails.tbxLoanPayOffAmount.skin = "skntxt727272";
                    this.view.LoanDetails.lblError.text = "Enable Terms and Conditions";
                    this.view.LoanDetails.flxError.setVisibility(true);
                    FormControllerUtility.disableButton(this.view.LoanDetails.btnConfirm);
                    this.AdjustScreen();
                }

            }
        },
        /*This function is used to Enable skins*/
        enableSkins: function() {
            var principalAmt = Number(this.view.LoanDetails.tbxLoanPayOffAmount.text.replace(/[&\/\\#,+()€~'":*?<>{}]/g, ''));
            var eligibleAmt = Number(this.view.LoanDetails.lblAmount.text.replace(/[&\/\\#,+()€~%'":*?<>{}]/g, ''));
            if ((principalAmt <= eligibleAmt) && this.view.LoanDetails.lblFavoriteEmailCheckBox.text === "C") {
                this.view.LoanDetails.tbxLoanPayOffAmount.skin = "skntxt727272";
                this.view.LoanDetails.flxError.setVisibility(false);
                FormControllerUtility.enableButton(this.view.LoanDetails.btnConfirm);
                this.AdjustScreen();
            }
        },
    };
});