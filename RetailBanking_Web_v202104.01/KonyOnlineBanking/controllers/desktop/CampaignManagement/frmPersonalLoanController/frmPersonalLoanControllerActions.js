define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for frmPersonalLoan **/
    AS_Form_e5c82ff871ac4d3499c45281198a3290: function AS_Form_e5c82ff871ac4d3499c45281198a3290(eventobject) {
        var self = this;
        this.preshow();
    },
    /** postShow defined for frmPersonalLoan **/
    AS_Form_f3c965b61c834c979d55a3f975b79800: function AS_Form_f3c965b61c834c979d55a3f975b79800(eventobject) {
        var self = this;
        this.postshowfrmNAO();
    },
    /** onSelection defined for lbxSelectReasonNew **/
    AS_ListBox_b9a56b67e0e84838b0b3deeb442d34f3: function AS_ListBox_b9a56b67e0e84838b0b3deeb442d34f3(eventobject) {
        var self = this;
        this.calInterestRate(this.view.LoanDetails.lbxSelectReasonNew.selectedKey);
    }
});