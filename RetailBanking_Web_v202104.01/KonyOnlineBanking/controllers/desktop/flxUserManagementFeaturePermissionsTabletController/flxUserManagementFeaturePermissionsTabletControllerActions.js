define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxClickAction16 **/
    AS_FlexContainer_a5c3acf143814a358efbf96eea005646: function AS_FlexContainer_a5c3acf143814a358efbf96eea005646(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction4 **/
    AS_FlexContainer_af6d4c4374d2407c892268ee39c5fb55: function AS_FlexContainer_af6d4c4374d2407c892268ee39c5fb55(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction7 **/
    AS_FlexContainer_b1e19c8b0762472bb491ffe5d12eb6e1: function AS_FlexContainer_b1e19c8b0762472bb491ffe5d12eb6e1(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction20 **/
    AS_FlexContainer_bfea247d8a004387bfe48f58d235738b: function AS_FlexContainer_bfea247d8a004387bfe48f58d235738b(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction9 **/
    AS_FlexContainer_ce5f6b7d06004369b30e8c959f4c44a0: function AS_FlexContainer_ce5f6b7d06004369b30e8c959f4c44a0(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, null, null);
    },
    /** onTouchEnd defined for flxTooltip **/
    AS_FlexContainer_d4ac2f892037456392fea6fc50cb7859: function AS_FlexContainer_d4ac2f892037456392fea6fc50cb7859(eventobject, x, y, context) {
        var self = this;
        this.onInfoTouchEnd(eventobject, context);
    },
    /** onClick defined for flxClickAction17 **/
    AS_FlexContainer_e5294744f3b1475b86b0617e0280c175: function AS_FlexContainer_e5294744f3b1475b86b0617e0280c175(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction2 **/
    AS_FlexContainer_e790c1e24e7a42edab0b035a100150e6: function AS_FlexContainer_e790c1e24e7a42edab0b035a100150e6(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction5 **/
    AS_FlexContainer_edd2b43430d5492ca4f75c32d6349b02: function AS_FlexContainer_edd2b43430d5492ca4f75c32d6349b02(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction8 **/
    AS_FlexContainer_f7b651eb41f14c308e819eb8e403eede: function AS_FlexContainer_f7b651eb41f14c308e819eb8e403eede(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction14 **/
    AS_FlexContainer_fa8c9987411b4d7e9c611d22060754ef: function AS_FlexContainer_fa8c9987411b4d7e9c611d22060754ef(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction11 **/
    AS_FlexContainer_fbda9ee11ba4492bb5fea413fd434f10: function AS_FlexContainer_fbda9ee11ba4492bb5fea413fd434f10(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction10 **/
    AS_FlexContainer_fe40857578cf49598d15f1a63a33c36e: function AS_FlexContainer_fe40857578cf49598d15f1a63a33c36e(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction19 **/
    AS_FlexContainer_g4a85321c57f422f985f7b8b5a4ebcea: function AS_FlexContainer_g4a85321c57f422f985f7b8b5a4ebcea(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction3 **/
    AS_FlexContainer_g5b8ac5708f4403e880807f0ae71ff73: function AS_FlexContainer_g5b8ac5708f4403e880807f0ae71ff73(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction1 **/
    AS_FlexContainer_ga80ad3345c748699845a6c7b6fcd24f: function AS_FlexContainer_ga80ad3345c748699845a6c7b6fcd24f(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction18 **/
    AS_FlexContainer_gbdaa5fa02b24dd598c9b0770f25bf65: function AS_FlexContainer_gbdaa5fa02b24dd598c9b0770f25bf65(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onTouchStart defined for flxTooltip **/
    AS_FlexContainer_h9e6eddcb3024246b9736dbf5b4b8048: function AS_FlexContainer_h9e6eddcb3024246b9736dbf5b4b8048(eventobject, x, y, context) {
        var self = this;
        this.onInfoTouchStart(eventobject, context);
    },
    /** onClick defined for flxPermissionClickable **/
    AS_FlexContainer_hf7c3cad8f3f4218a5b3cc09ba3c0b26: function AS_FlexContainer_hf7c3cad8f3f4218a5b3cc09ba3c0b26(eventobject, context) {
        var self = this;
        this.selectOrUnselectEntireFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction15 **/
    AS_FlexContainer_ic0d8d3ff3d1453c9e52185e5a41378c: function AS_FlexContainer_ic0d8d3ff3d1453c9e52185e5a41378c(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction6 **/
    AS_FlexContainer_j62ec37938db462b9c10b249e57ecc78: function AS_FlexContainer_j62ec37938db462b9c10b249e57ecc78(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction13 **/
    AS_FlexContainer_j675bcdee92e40d1a5733b8f3cb25550: function AS_FlexContainer_j675bcdee92e40d1a5733b8f3cb25550(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction12 **/
    AS_FlexContainer_jba014f96d074b3cb25256151324a9b3: function AS_FlexContainer_jba014f96d074b3cb25256151324a9b3(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    }
});