define({
    showSelectedRow: function() {
        var index = kony.application.getCurrentForm().tableView.segP2P.selectedrowIndex;
        var sectionIndex = index[0];
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().tableView.segP2P.data;
        data[sectionIndex][1][rowIndex].imgDropdown = "arrow_down.png";
        if (kony.application.getCurrentBreakpoint() == 640) {
            data[sectionIndex][1][rowIndex].template = "flxSentMobile";
        } else {
            data[sectionIndex][1][rowIndex].template = "flxSent";
        }
        kony.application.getCurrentForm().tableView.segP2P.setDataAt(data[sectionIndex][1][rowIndex], rowIndex, sectionIndex);
        this.AdjustScreen(-55);
    },
    showSendMoney: function() {
        kony.application.getCurrentForm().tableView.flxTabs.btnSendRequest.skin = "sknBtnAccountSummarySelected";
        kony.application.getCurrentForm().tableView.flxTabs.btnMyRequests.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnSent.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnRecieved.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnManageRecepient.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxSendMoney.setVisibility(true);
        kony.application.getCurrentForm().tableView.flxSendReminder.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxRequestMoney.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxHorizontalLine2.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxTableHeaders.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxSearch1.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxHorizontalLine3.setVisibility(false);
        kony.application.getCurrentForm().tableView.segP2P.setVisibility(false);
        this.AdjustScreen(30);
    },
    //UI Code
    AdjustScreen: function(data) {
        var currForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currForm.customheader.info.frame.height + currForm.flxContainer.info.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currForm.flxFooter.info.frame.height;
            if (diff > 0)
                currForm.flxFooter.top = mainheight + diff + data + "dp";
            else
                currForm.flxFooter.top = mainheight + data + "dp";
        } else {
            currForm.flxFooter.top = mainheight + data + "dp";
        }
        currForm.forceLayout();
    },
});