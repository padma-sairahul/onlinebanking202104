define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for flxDots **/
    AS_FlexContainer_c5ce53e127214bc98a55c8646e50ebfc: function AS_FlexContainer_c5ce53e127214bc98a55c8646e50ebfc(eventobject, x, y, context) {
        var self = this;
        this.setClickOrigin();
    },
    /** onClick defined for flxDots **/
    AS_FlexContainer_jfcd5b0997904edf8a5dc28f2e7dea3a: function AS_FlexContainer_jfcd5b0997904edf8a5dc28f2e7dea3a(eventobject, context) {
        var self = this;
        this.menuPressed();
    }
});