define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnRepeat **/
    AS_Button_a221ef718b9b4c47bfa795241ae0d628: function AS_Button_a221ef718b9b4c47bfa795241ae0d628(eventobject, context) {
        var self = this;
        this.executeOnParent("repeatTransaction");
    },
    /** onClick defined for btnAttachments **/
    AS_Button_a2d7bb2b21dc47fa993c31bd2b2ee7f4: function AS_Button_a2d7bb2b21dc47fa993c31bd2b2ee7f4(eventobject, context) {
        var self = this;
        this.executeOnParent("repeatTransaction");
    },
    /** onClick defined for btnCancel2 **/
    AS_Button_e9b62ede0d3d4843a1cbf34b0113e7ac: function AS_Button_e9b62ede0d3d4843a1cbf34b0113e7ac(eventobject, context) {
        var self = this;
        this.executeOnParent("repeatTransaction");
    },
    /** onClick defined for btnCancel1 **/
    AS_Button_efb9aa1bb08141baafda15abd7affe9b: function AS_Button_efb9aa1bb08141baafda15abd7affe9b(eventobject, context) {
        var self = this;
        this.executeOnParent("repeatTransaction");
    },
    /** onClick defined for btnAction **/
    AS_Button_h3838e34bed24554abb63debdf3f646f: function AS_Button_h3838e34bed24554abb63debdf3f646f(eventobject, context) {
        var self = this;
        this.executeOnParent("viewTransactionReport");
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a74c7e232d64498dadced7789bac7c12: function AS_FlexContainer_a74c7e232d64498dadced7789bac7c12(eventobject, context) {
        var self = this;
        this.showUnselectedRow();
    }
});