define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAction **/
    AS_Button_f1751cfad76d47dc9d6c0bb08868db14: function AS_Button_f1751cfad76d47dc9d6c0bb08868db14(eventobject, context) {
        var self = this;
        this.MakeTransferAction();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a95fa3d8b80845268f39519d2304a27f: function AS_FlexContainer_a95fa3d8b80845268f39519d2304a27f(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});