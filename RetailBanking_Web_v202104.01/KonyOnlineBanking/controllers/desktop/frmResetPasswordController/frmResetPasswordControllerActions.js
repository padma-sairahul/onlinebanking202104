define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onBeginEditing defined for tbxNewPassword **/
    AS_TextField_edf085ea72834042a6ad008341086e0a: function AS_TextField_edf085ea72834042a6ad008341086e0a(eventobject, changedtext) {
        var self = this;
    },
    /** onKeyUp defined for tbxNewPassword **/
    AS_TextField_a147586e75b04b3b8cec36a8861b45b4: function AS_TextField_a147586e75b04b3b8cec36a8861b45b4(eventobject) {
        var self = this;
    },
    /** onKeyUp defined for tbxMatchPassword **/
    AS_TextField_i5293c1b83184c5c8228f68b41a5b94e: function AS_TextField_i5293c1b83184c5c8228f68b41a5b94e(eventobject) {
        var self = this;
    },
    /** onKeyUp defined for tbxLastName **/
    AS_TextField_c8c113c1feb04593a665de302e563d06: function AS_TextField_c8c113c1feb04593a665de302e563d06(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onKeyUp defined for tbxSSN **/
    AS_TextField_h6de70a95a364601b403b7bd19d979ab: function AS_TextField_h6de70a95a364601b403b7bd19d979ab(eventobject) {
        var self = this;
        this.allFieldsCheck();
        this.ssnCheck();
    },
    /** onSelection defined for lbxYear **/
    AS_ListBox_a5b4a6387a4648f796d1f94a8f057281: function AS_ListBox_a5b4a6387a4648f796d1f94a8f057281(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onSelection defined for lbxMonth **/
    AS_ListBox_f43bcc9dd3d241adb337edb636d7c25c: function AS_ListBox_f43bcc9dd3d241adb337edb636d7c25c(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onSelection defined for lbxDate **/
    AS_ListBox_abdb35e128d94c5a8f408844217d39e8: function AS_ListBox_abdb35e128d94c5a8f408844217d39e8(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onClick defined for btnProceed **/
    AS_Button_aa2fc85679bf4271a045018262c582c1: function AS_Button_aa2fc85679bf4271a045018262c582c1(eventobject) {
        var self = this;
        this.verifyUserDetails();
    },
    /** onClick defined for btnNext **/
    AS_Button_h08129946fca478c8d44c0ab65511028: function AS_Button_h08129946fca478c8d44c0ab65511028(eventobject) {
        var self = this;
        this.requestOTPValue();
    },
    /** onClick defined for btnUseCVV **/
    AS_Button_f3a27dd0c5884e79ba8975b6800e8ea4: function AS_Button_f3a27dd0c5884e79ba8975b6800e8ea4(eventobject) {
        var self = this;
        this.useCVVForReset();
    },
    /** onTouchStart defined for imgViewCVV **/
    AS_Image_e8fd4567261c422d80f7845cc2af7b71: function AS_Image_e8fd4567261c422d80f7845cc2af7b71(eventobject, x, y) {
        var self = this;
        this.showOTP();
    },
    /** onBeginEditing defined for tbxCVV **/
    AS_TextField_h030bc5eb17e47d39cbf07fc304cd8fa: function AS_TextField_h030bc5eb17e47d39cbf07fc304cd8fa(eventobject, changedtext) {
        var self = this;
        if (this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible) {
            this.reEnterOTP();
        }
    },
    /** onKeyUp defined for tbxCVV **/
    AS_TextField_j8ae877c189f49b8a0524a2e0b52f219: function AS_TextField_j8ae877c189f49b8a0524a2e0b52f219(eventobject) {
        var self = this;
        this.otpCheck();
    },
    /** onClick defined for btnNext **/
    AS_Button_a4ac98db97304acd816953d2fd45cd6e: function AS_Button_a4ac98db97304acd816953d2fd45cd6e(eventobject) {
        var self = this;
        this.isOTPCorrect();
    },
    /** onClick defined for btnResendOTP **/
    AS_Button_dcd6d43d9f934985991e420eea27c094: function AS_Button_dcd6d43d9f934985991e420eea27c094(eventobject) {
        var self = this;
        this.resendOTPValue();
    },
    /** onClick defined for btnUseCVV **/
    AS_Button_fc10b48d9d1c467690881dd39a1433f3: function AS_Button_fc10b48d9d1c467690881dd39a1433f3(eventobject) {
        var self = this;
        this.useCVVForReset();
    },
    /** onClick defined for flxViewHideCVV **/
    AS_FlexContainer_fe0be06b9ac64653bf2b2d24a1df08ed: function AS_FlexContainer_fe0be06b9ac64653bf2b2d24a1df08ed(eventobject) {
        var self = this;
        this.showCVV();
    },
    /** onTouchStart defined for imgViewCVV **/
    AS_Image_j2acf91c13184a85b4b76aedbed08508: function AS_Image_j2acf91c13184a85b4b76aedbed08508(eventobject, x, y) {
        var self = this;
        //this.showCVV();/
    },
    /** onBeginEditing defined for tbxCVV **/
    AS_TextField_afab8107f8e446c2b86876e1f394949a: function AS_TextField_afab8107f8e446c2b86876e1f394949a(eventobject, changedtext) {
        var self = this;
        this.reEnterCVV();
    },
    /** onKeyUp defined for tbxCVV **/
    AS_TextField_f20dbb4fddca4e45a538f8bf9654a1ca: function AS_TextField_f20dbb4fddca4e45a538f8bf9654a1ca(eventobject) {
        var self = this;
        this.cvvCheck();
    },
    /** onClick defined for btnNext **/
    AS_Button_ifacc90ce3d342c5b32da91c7a6c6e42: function AS_Button_ifacc90ce3d342c5b32da91c7a6c6e42(eventobject) {
        var self = this;
        this.isCVVCorrect();
    },
    /** onClick defined for btnUseOTP **/
    AS_Button_e1f38c4db7644c9bb10877bb3c1ca82a: function AS_Button_e1f38c4db7644c9bb10877bb3c1ca82a(eventobject) {
        var self = this;
        this.useOTPForReset();
    },
    /** onClick defined for btnCreate **/
    AS_Button_dfd1970db004461ea5290dcc03e6d9f2: function AS_Button_dfd1970db004461ea5290dcc03e6d9f2(eventobject) {
        var self = this;
    },
    /** onSelection defined for lbxQuestion1 **/
    AS_ListBox_d19715af59fa4e5691aab2465284e2bf: function AS_ListBox_d19715af59fa4e5691aab2465284e2bf(eventobject) {
        var self = this;
        this.setQuestionForListBox1();
    },
    /** onBeginEditing defined for tbxAnswer1 **/
    AS_TextField_e4482e7f2f664429a9315e210712e607: function AS_TextField_e4482e7f2f664429a9315e210712e607(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer1();
    },
    /** onEndEditing defined for tbxAnswer1 **/
    AS_TextField_e970ae07757c4e969fb123f8b17d3118: function AS_TextField_e970ae07757c4e969fb123f8b17d3118(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer1 **/
    AS_TextField_ef5e9a24c5504d90b931a2c24fbd16a0: function AS_TextField_ef5e9a24c5504d90b931a2c24fbd16a0(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onSelection defined for lbxQuestion2 **/
    AS_ListBox_j3b62e329c1b4fa0981ced02b8d6c257: function AS_ListBox_j3b62e329c1b4fa0981ced02b8d6c257(eventobject) {
        var self = this;
        this.setQuestionForListBox2();
    },
    /** onBeginEditing defined for tbxAnswer2 **/
    AS_TextField_ccd36a5b84df4221926ce36f4c8ddba6: function AS_TextField_ccd36a5b84df4221926ce36f4c8ddba6(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer2();
    },
    /** onEndEditing defined for tbxAnswer2 **/
    AS_TextField_c55b57730f0b46b0b9a45f857d35b941: function AS_TextField_c55b57730f0b46b0b9a45f857d35b941(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer2 **/
    AS_TextField_efedb0922453453d95970ae6c3aad6d4: function AS_TextField_efedb0922453453d95970ae6c3aad6d4(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onSelection defined for lbxQuestion3 **/
    AS_ListBox_g189f4e38a0a49689bfa3ddc2687e0e7: function AS_ListBox_g189f4e38a0a49689bfa3ddc2687e0e7(eventobject) {
        var self = this;
        this.setQuestionForListBox3();
    },
    /** onBeginEditing defined for tbxAnswer3 **/
    AS_TextField_d1d06aa50cf948de901c477129da4912: function AS_TextField_d1d06aa50cf948de901c477129da4912(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer3();
    },
    /** onEndEditing defined for tbxAnswer3 **/
    AS_TextField_ec190dad0597473385edbc20bc8d9a31: function AS_TextField_ec190dad0597473385edbc20bc8d9a31(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer3 **/
    AS_TextField_cd23a36a257f4fbfad519df3e8f4532a: function AS_TextField_cd23a36a257f4fbfad519df3e8f4532a(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onSelection defined for lbxQuestion4 **/
    AS_ListBox_af31dd4e16594b71afd33203d7a38413: function AS_ListBox_af31dd4e16594b71afd33203d7a38413(eventobject) {
        var self = this;
        this.setQuestionForListBox4();
    },
    /** onBeginEditing defined for tbxAnswer4 **/
    AS_TextField_ffde4fff69214ab18dc619d7c7367256: function AS_TextField_ffde4fff69214ab18dc619d7c7367256(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer4();
    },
    /** onEndEditing defined for tbxAnswer4 **/
    AS_TextField_b3e04719ffc1405ead6a81ae062ea865: function AS_TextField_b3e04719ffc1405ead6a81ae062ea865(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer4 **/
    AS_TextField_bc7f3173e2734cc0aab8572a6e8a76cc: function AS_TextField_bc7f3173e2734cc0aab8572a6e8a76cc(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onSelection defined for lbxQuestion5 **/
    AS_ListBox_e2882689029844fc9c6286c39a7a2cfc: function AS_ListBox_e2882689029844fc9c6286c39a7a2cfc(eventobject) {
        var self = this;
        this.setQuestionForListBox5();
    },
    /** onBeginEditing defined for tbxAnswer5 **/
    AS_TextField_cf41c99d9b1e4f398a4246848d08d195: function AS_TextField_cf41c99d9b1e4f398a4246848d08d195(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer5();
    },
    /** onEndEditing defined for tbxAnswer5 **/
    AS_TextField_b42641c18e0f4b45a35596c73b2aa689: function AS_TextField_b42641c18e0f4b45a35596c73b2aa689(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer5 **/
    AS_TextField_e9943a8cade846c1962f647500c80d54: function AS_TextField_e9943a8cade846c1962f647500c80d54(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.SetSecurityQuestions.tbxAnswer1.text, this.view.SetSecurityQuestions.tbxAnswer2.text, this.view.SetSecurityQuestions.tbxAnswer3.text, this.view.SetSecurityQuestions.tbxAnswer4.text, this.view.SetSecurityQuestions.tbxAnswer5.text);
    },
    /** onClick defined for btnSetSecurityQuestionsCancel **/
    AS_Button_b421538571184a66b388559812a6d86b: function AS_Button_b421538571184a66b388559812a6d86b(eventobject) {
        var self = this;
        this.onCancelSecurityQuestions();
    },
    /** onClick defined for btnSetSecurityQuestionsProceed **/
    AS_Button_c7b59ffb265244959417dfc45f9b7ffb: function AS_Button_c7b59ffb265244959417dfc45f9b7ffb(eventobject) {
        var self = this;
        this.onProceedSQ();
    },
    /** onClick defined for btnProceed **/
    AS_Button_feae1769d354440b8d25b5581061d8c7: function AS_Button_feae1769d354440b8d25b5581061d8c7(eventobject) {
        var self = this;
        this.securityQuesAnsProceed();
    },
    /** onClick defined for btnProceed **/
    AS_Button_dcf8539756e4499993bbc38c7718b40b: function AS_Button_dcf8539756e4499993bbc38c7718b40b(eventobject) {
        var self = this;
        this.updateSecurityQuestions();
    },
    /** onClick defined for btnLoginLater **/
    AS_Button_ad9b6bbadb884deca296b8fb15cf8c27: function AS_Button_ad9b6bbadb884deca296b8fb15cf8c27(eventobject) {
        var self = this;
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.showLoginScreen();
    },
    /** onClick defined for btnProceed **/
    AS_Button_h19a0727701149869f5686130193ab2a: function AS_Button_h19a0727701149869f5686130193ab2a(eventobject) {
        var self = this;
        applicationManager.getNavigationManager().navigateTo("frmAccountsLanding");
    },
    /** onTouchStart defined for lblHowToEnroll **/
    AS_Label_i91cb93887364b769b9fd4dce05b1fbd: function AS_Label_i91cb93887364b769b9fd4dce05b1fbd(eventobject, x, y) {
        var self = this;
        this.returnToLogin();
    },
    /** onClick defined for btnBackToLogin **/
    AS_Button_a8eef43fd77c4b69b0a0d7354e48117b: function AS_Button_a8eef43fd77c4b69b0a0d7354e48117b(eventobject) {
        var self = this;
        this.goToLogin();
    },
    /** onTouchStart defined for AlterneteActionsEnterCVV **/
    AS_UWI_i5c53b650f9c485fbf8220c504b96497: function AS_UWI_i5c53b650f9c485fbf8220c504b96497(eventobject, x, y) {
        var self = this;
        this.showEnterCVVPage();
    },
    /** onTouchStart defined for AlterneteActionsEnterPIN **/
    AS_UWI_dd7bf1f9cbf74d1f9b2916cde1f19c3f: function AS_UWI_dd7bf1f9cbf74d1f9b2916cde1f19c3f(eventobject, x, y) {
        var self = this;
        this.goToResetUsingOTP();
    },
    /** onRowClick defined for segLanguagesList **/
    AS_Segment_g22b93a777f84d9cb00e8ef17c29fbdc: function AS_Segment_g22b93a777f84d9cb00e8ef17c29fbdc(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.selectYourLanguage();
    }
});