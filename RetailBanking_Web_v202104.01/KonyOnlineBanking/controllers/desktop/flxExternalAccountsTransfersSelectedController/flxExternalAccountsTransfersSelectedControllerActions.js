define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnDeletemod **/
    AS_Button_a61805ca8984455683cd2397f588215f: function AS_Button_a61805ca8984455683cd2397f588215f(eventobject, context) {
        var self = this;
    },
    /** onClick defined for btnViewActivity **/
    AS_Button_a8cbd225a8c24429a1ccf087db634970: function AS_Button_a8cbd225a8c24429a1ccf087db634970(eventobject, context) {
        var self = this;
        this.viewactivitypopup();
    },
    /** onClick defined for btnEditmod **/
    AS_Button_fa1c5037917246cba309fa50f1bdbb51: function AS_Button_fa1c5037917246cba309fa50f1bdbb51(eventobject, context) {
        var self = this;
    },
    /** onClick defined for btnAction **/
    AS_Button_g48ebe54643b41a8b1940ce6974cd95a: function AS_Button_g48ebe54643b41a8b1940ce6974cd95a(eventobject, context) {
        var self = this;
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_d5082dadc93e45b0893daeab2d47154e: function AS_FlexContainer_d5082dadc93e45b0893daeab2d47154e(eventobject, context) {
        var self = this;
        //this.pendingRowOnClick();
        this.showUnselectedRow();
    },
    /** onTouchEnd defined for imgDropdown **/
    AS_Image_i0ec1376551b495583118a8117909108: function AS_Image_i0ec1376551b495583118a8117909108(eventobject, x, y, context) {
        var self = this;
    }
});