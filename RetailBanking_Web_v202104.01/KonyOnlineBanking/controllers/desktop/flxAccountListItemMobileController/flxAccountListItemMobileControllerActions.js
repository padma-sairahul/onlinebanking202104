define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxFavourite **/
    AS_FlexContainer_a3b8b9f7c77a40bfaaf9a429dc8afd5a: function AS_FlexContainer_a3b8b9f7c77a40bfaaf9a429dc8afd5a(eventobject, context) {
        var self = this;
        this.imgPressed();
    },
    /** onClick defined for flxMenu **/
    AS_FlexContainer_b653e20415d642ada06783e03de57b6c: function AS_FlexContainer_b653e20415d642ada06783e03de57b6c(eventobject, context) {
        var self = this;
        this.menuPressed();
    },
    /** onClick defined for flxContent **/
    AS_FlexContainer_b6b472ce9c0647e083bb21ae3923fa59: function AS_FlexContainer_b6b472ce9c0647e083bb21ae3923fa59(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onTouchStart defined for flxMenu **/
    AS_FlexContainer_bf23ec82edc0494184ed52983489ca79: function AS_FlexContainer_bf23ec82edc0494184ed52983489ca79(eventobject, x, y, context) {
        var self = this;
        this.setClickOrigin();
    },
    /** onClick defined for flxAccountName **/
    AS_FlexContainer_c98a0c9ab7554f54aa5e42f32c47b2ef: function AS_FlexContainer_c98a0c9ab7554f54aa5e42f32c47b2ef(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxAvailableBalance **/
    AS_FlexContainer_e3b6669d510f4641b389e88555ca2e1f: function AS_FlexContainer_e3b6669d510f4641b389e88555ca2e1f(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxSeparator **/
    AS_FlexContainer_e852f86c32624f6fabbc3c1f67714117: function AS_FlexContainer_e852f86c32624f6fabbc3c1f67714117(eventobject, context) {
        var self = this;
        this.menuPressed();
    }
});