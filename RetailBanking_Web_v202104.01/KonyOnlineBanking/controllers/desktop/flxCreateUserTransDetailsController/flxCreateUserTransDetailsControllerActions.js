define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onKeyUp defined for tbxPerTransDenialAmount **/
    AS_TextField_ab4ecb44a01d4790800399aee79d4a18: function AS_TextField_ab4ecb44a01d4790800399aee79d4a18(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxWeeklyTransDenialAmount **/
    AS_TextField_ad60138272da4c36bf12b09244cd870e: function AS_TextField_ad60138272da4c36bf12b09244cd870e(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onEndEditing defined for tbxDailyTransDenialAmount **/
    AS_TextField_ae8f978cbd9b450494bff40561e74146: function AS_TextField_ae8f978cbd9b450494bff40561e74146(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onKeyUp defined for tbxDailyTransApproveAmount **/
    AS_TextField_b2a66e57f2ab436a893f92d5eb2d6002: function AS_TextField_b2a66e57f2ab436a893f92d5eb2d6002(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onKeyUp defined for tbxPerTransApproveAmount **/
    AS_TextField_b4c645e83fa443b0be3267885db81a7f: function AS_TextField_b4c645e83fa443b0be3267885db81a7f(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxPerTransApproveAmount **/
    AS_TextField_d8da8fc831bc4acda1dc707b0df15569: function AS_TextField_d8da8fc831bc4acda1dc707b0df15569(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onKeyUp defined for tbxWeeklyTransDenialAmount **/
    AS_TextField_e3bfddd3a61744a8ab784d2eb6f6a624: function AS_TextField_e3bfddd3a61744a8ab784d2eb6f6a624(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxDailyTransApproveAmount **/
    AS_TextField_ed74e8d517a3432f9b193e9b69bbdca5: function AS_TextField_ed74e8d517a3432f9b193e9b69bbdca5(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onEndEditing defined for tbxWeeklyTransApproveAmount **/
    AS_TextField_f33cc5db04774efcb22f4123596f8e4b: function AS_TextField_f33cc5db04774efcb22f4123596f8e4b(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onKeyUp defined for tbxWeeklyTransApproveAmount **/
    AS_TextField_f34df8808711463d81dfde3e5e412577: function AS_TextField_f34df8808711463d81dfde3e5e412577(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onKeyUp defined for tbxDailyTransDenialAmount **/
    AS_TextField_ge2cf89592e547e79a7249688dd9687b: function AS_TextField_ge2cf89592e547e79a7249688dd9687b(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxPerTransDenialAmount **/
    AS_TextField_h3d2490effa746129d7c71efe1dd207a: function AS_TextField_h3d2490effa746129d7c71efe1dd207a(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    }
});