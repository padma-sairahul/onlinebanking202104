define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a0cd9beb2428424dbe812b5fee88f25c: function AS_FlexContainer_a0cd9beb2428424dbe812b5fee88f25c(eventobject, context) {
        var self = this;
        this.segmentManagePayeesRowClick();
    },
    /** onClick defined for btnEbill **/
    AS_Button_c8730fbda2b04ec68e9102577de6dff5: function AS_Button_c8730fbda2b04ec68e9102577de6dff5(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnViewActivity **/
    AS_Button_fa8320ab1ac34fe6ab762c9823e43012: function AS_Button_fa8320ab1ac34fe6ab762c9823e43012(eventobject, context) {
        var self = this;
        this.viewBillPayActivity();
    },
    /** onClick defined for btnCancel **/
    AS_Button_j4bd96a491b14516a8856a49a26a87de: function AS_Button_j4bd96a491b14516a8856a49a26a87de(eventobject, context) {
        var self = this;
        kony.print("Cancel clicked");
    },
    /** onClick defined for btnSave **/
    AS_Button_ge4011dd363f4770a24293ff660eebd9: function AS_Button_ge4011dd363f4770a24293ff660eebd9(eventobject, context) {
        var self = this;
        kony.print("Save clicked");
    }
});