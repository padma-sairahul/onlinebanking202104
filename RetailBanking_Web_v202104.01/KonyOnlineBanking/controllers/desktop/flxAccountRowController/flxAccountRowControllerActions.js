define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEdit **/
    AS_Button_jd6ad5b2c8274bd6a0d1f81a44e81027: function AS_Button_jd6ad5b2c8274bd6a0d1f81a44e81027(eventobject, context) {
        var self = this;
        this.showEditAccountsPreferences();
    },
    /** onClick defined for flxMoveUp **/
    AS_FlexContainer_a48619a7516f43e382746a1e0de8628d: function AS_FlexContainer_a48619a7516f43e382746a1e0de8628d(eventobject, context) {
        var self = this;
    },
    /** onClick defined for flxMenu **/
    AS_FlexContainer_bb5a416ba43f478a87a7051e0db62614: function AS_FlexContainer_bb5a416ba43f478a87a7051e0db62614(eventobject, context) {
        var self = this;
        this.menuPressed();
    },
    /** onClick defined for flxContent **/
    AS_FlexContainer_df073d195a67484fa42b1db1ab59f3fa: function AS_FlexContainer_df073d195a67484fa42b1db1ab59f3fa(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxMoveDown **/
    AS_FlexContainer_e90018729f764273bcb1f9baa020ef3e: function AS_FlexContainer_e90018729f764273bcb1f9baa020ef3e(eventobject, context) {
        var self = this;
    },
    /** onClick defined for flxFavoriteCheckBox **/
    AS_FlexContainer_eb098f2af52643b79d6495f7c0a8f3f4: function AS_FlexContainer_eb098f2af52643b79d6495f7c0a8f3f4(eventobject, context) {
        var self = this;
        this.toggleFavouriteCheckBox();
    },
    /** onClick defined for flxAccountName **/
    AS_FlexContainer_f03a7fe36c594723812ff20dc88b60bd: function AS_FlexContainer_f03a7fe36c594723812ff20dc88b60bd(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxEStatementCheckBox **/
    AS_FlexContainer_i0e2d2c700ae4750a6aa0376dd344a70: function AS_FlexContainer_i0e2d2c700ae4750a6aa0376dd344a70(eventobject, context) {
        var self = this;
        this.toggleEstatementCheckBox();
    }
});