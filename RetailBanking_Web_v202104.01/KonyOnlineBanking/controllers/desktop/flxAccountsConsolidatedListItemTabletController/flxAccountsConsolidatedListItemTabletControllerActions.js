define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_d661f4a42c314a70a33c9c6dece07aa7: function AS_FlexContainer_d661f4a42c314a70a33c9c6dece07aa7(eventobject, context) {
        var self = this;
        this.toggleSegRowCheckBox(eventobject, context);
    },
    /** onClick defined for flxRightCheckBox **/
    AS_FlexContainer_eedb8659dd7e43328aa6321a4275b949: function AS_FlexContainer_eedb8659dd7e43328aa6321a4275b949(eventobject, context) {
        var self = this;
        this.toggleSegRowCheckBox(eventobject, context, true);
    }
});