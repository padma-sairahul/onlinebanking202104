define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAction **/
    AS_Button_d6f447a92d554855ae218fca072a7957: function AS_Button_d6f447a92d554855ae218fca072a7957(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_c4bd0a44feca4a90ae0712ec3128b556: function AS_FlexContainer_c4bd0a44feca4a90ae0712ec3128b556(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});