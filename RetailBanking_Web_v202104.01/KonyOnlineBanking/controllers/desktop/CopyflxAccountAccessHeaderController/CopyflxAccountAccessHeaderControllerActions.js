define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxImgCheckView **/
    AS_FlexContainer_da077e67abf5429f9f716d16a161b11a: function AS_FlexContainer_da077e67abf5429f9f716d16a161b11a(eventobject, context) {
        var self = this;
        this.executeOnParent("checkUncheckViewAccess");
    },
    /** onClick defined for flxImgCheckTransaction **/
    AS_FlexContainer_fa4726a95c86414abf7469234555174d: function AS_FlexContainer_fa4726a95c86414abf7469234555174d(eventobject, context) {
        var self = this;
        this.executeOnParent("checkUncheckTransactionAccess");
    }
});