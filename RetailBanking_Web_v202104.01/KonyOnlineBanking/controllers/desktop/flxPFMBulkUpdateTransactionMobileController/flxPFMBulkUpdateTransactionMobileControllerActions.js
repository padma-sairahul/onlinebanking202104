define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a46f40fbfb2648589cb204640ed9671a: function AS_FlexContainer_a46f40fbfb2648589cb204640ed9671a(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for flxCheckbox **/
    AS_FlexContainer_e18400c1771248d0acbb312659753a18: function AS_FlexContainer_e18400c1771248d0acbb312659753a18(eventobject, context) {
        var self = this;
        this.toggleCheckBox();
    }
});