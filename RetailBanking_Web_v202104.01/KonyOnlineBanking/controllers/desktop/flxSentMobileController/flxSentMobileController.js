define({
    showSelectedRow: function() {
        var index = kony.application.getCurrentForm().tableView.segP2P.selectedRowIndex;
        var sectionIndex = index[0];
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().tableView.segP2P.data;
        for (var sections = 0; sections < data.length; sections++) {
            for (var rows = 0; rows < data[sections][1].length; rows++) {
                if (sections == sectionIndex && rowIndex == rows) {
                    data[sections][1][rowIndex].imgDropdown = "chevron_up.png";
                    if (kony.application.getCurrentBreakpoint() == 640) {
                        data[sections][1][rowIndex].template = "flxSentSelectedMobile";
                    } else {
                        data[sections][1][rowIndex].template = "flxSentSelected";
                    }
                } else {
                    data[sections][1][rows].imgDropdown = "arrow_down.png";
                    if (kony.application.getCurrentBreakpoint() == 640) {
                        data[sections][1][rows].template = "flxSentMobile";
                    } else {
                        data[sections][1][rows].template = "flxSent";
                    }
                }
            }
        }
        kony.application.getCurrentForm().tableView.segP2P.setData(data);
        this.AdjustScreen(115);
    },
    showSendMoney: function() {
        kony.application.getCurrentForm().tableView.flxTabs.btnSendRequest.skin = "sknBtnAccountSummarySelected";
        kony.application.getCurrentForm().tableView.flxTabs.btnMyRequests.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnSent.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnRecieved.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnManageRecepient.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxSendMoney.setVisibility(true);
        kony.application.getCurrentForm().tableView.flxSendReminder.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxRequestMoney.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxHorizontalLine2.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxTableHeaders.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxSearch1.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxHorizontalLine3.setVisibility(false);
        kony.application.getCurrentForm().tableView.segP2P.setVisibility(false);
        this.AdjustScreen(30);
    },
    //UI Code
    AdjustScreen: function(data) {
        var currForm = kony.application.getCurrentForm();
        currForm.forceLayout();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currForm.customheader.info.frame.height + currForm.flxContainer.info.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currForm.flxFooter.info.frame.height;
            if (diff > 0)
                currForm.flxFooter.top = mainheight + diff + data + "dp";
            else
                currForm.flxFooter.top = mainheight + data + "dp";
        } else {
            currForm.flxFooter.top = mainheight + data + "dp";
        }
        currForm.forceLayout();
    },
});