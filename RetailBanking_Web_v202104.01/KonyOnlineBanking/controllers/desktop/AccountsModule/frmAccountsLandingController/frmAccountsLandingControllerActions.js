define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxPrimaryActions **/
    AS_FlexContainer_b6338a2887cb4b46b51611005897ecf4: function AS_FlexContainer_b6338a2887cb4b46b51611005897ecf4(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        ntf.navigate();
    },
    /** init defined for frmAccountsLanding **/
    AS_Form_d34f75ed3c4e4069b44c12d6c6550d43: function AS_Form_d34f75ed3c4e4069b44c12d6c6550d43(eventobject) {
        var self = this;
        this.initActions();
    },
    /** postShow defined for frmAccountsLanding **/
    AS_Form_debd6d79386b4b94b2e1f527a6b35c25: function AS_Form_debd6d79386b4b94b2e1f527a6b35c25(eventobject) {
        var self = this;
        this.onLoadChangePointer();
        this.postShow();
        this.setContextualMenuLeft();
    },
    /** onBreakpointChange defined for frmAccountsLanding **/
    AS_Form_e0cecb4fa6fa414da4a7eefd732fd884: function AS_Form_e0cecb4fa6fa414da4a7eefd732fd884(eventobject, breakpoint) {
        var self = this;
        this.onBreakpointChange(breakpoint);
    },
    /** onDeviceBack defined for frmAccountsLanding **/
    AS_Form_e7eedb1b544c4428bdd6df093769eb55: function AS_Form_e7eedb1b544c4428bdd6df093769eb55(eventobject) {
        var self = this;
        kony.print("Back Button is clicked");
    },
    /** onTouchEnd defined for frmAccountsLanding **/
    AS_Form_edae91fa152840d48dfb91459c97a0ef: function AS_Form_edae91fa152840d48dfb91459c97a0ef(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** preShow defined for frmAccountsLanding **/
    AS_Form_f0257ebf68784c548c562b9486d8a886: function AS_Form_f0257ebf68784c548c562b9486d8a886(eventobject) {
        var self = this;
        this.preShowFrmAccountsLanding();
        this.setAccountListData();
    },
    /** onKeyUp defined for LoginUsingSelectedBank.tbxNewUsername **/
    AS_TextField_cf2826b931594d0fa5c4944344d9dc18: function AS_TextField_cf2826b931594d0fa5c4944344d9dc18(eventobject) {
        var self = this;
        this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
    },
    /** onKeyUp defined for LoginUsingSelectedBank.tbxEnterpassword **/
    AS_TextField_f2030e8e14ef414facdb8bf21ac978f9: function AS_TextField_f2030e8e14ef414facdb8bf21ac978f9(eventobject) {
        var self = this;
        this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
    },
    /** onKeyUp defined for SelectBankOrVendor.tbxName **/
    AS_TextField_f21640467d6440dcbb0abd7833eb9542: function AS_TextField_f21640467d6440dcbb0abd7833eb9542(eventobject) {
        var self = this;
        this.onTextChangeOfExternalBankSearch();
    }
});