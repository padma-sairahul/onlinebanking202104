define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onTouchEnd defined for frmContactUsPrivacyTandC **/
    AS_Form_d45ef6cec3f940f5a8116f4e21d2ca69: function AS_Form_d45ef6cec3f940f5a8116f4e21d2ca69(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});