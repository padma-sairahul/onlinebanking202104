define(['FormControllerUtility', 'ViewConstants', 'CommonUtilities', 'CampaignUtility'], function(FormControllerUtility, ViewConstants, CommonUtilities, CampaignUtility) {

    var orientationHandler = new OrientationHandler();
    return {
        /**
         * Method to patch update UI
         * @param {Object} uiModel Data from presentation controller
         */

        updateFormUI: function(uiModel) {
            if (uiModel) {
                if (uiModel.key) {
                    switch (uiModel.key) {
                        case BBConstants.DASHBOARD:
                            this.loadDashboardWidgets();
                            break;
                        case BBConstants.APPROVALS_REQUESTS_COUNT:
                            this.initApprovalRequestWidget();
                            break;
                        case BBConstants.APPROVALS_REQUESTS_COUNT_FAILURE:
                            this.approvalRequestsServiceFailure();
                            break;
                        case BBConstants.UNREAD_MESSAGES_COUNT:
                            this.updateAlertIcon(uiModel.responseData);
                            break;
                        case BBConstants.CASH_POSITION:
                            this.populateCashPositionChart(uiModel.responseData);
                            break;
                        case BBConstants.CASH_POSITION_ERROR:
                            this.showAlertCashPositionChart(uiModel.responseData);
                            break;
                        case BBConstants.LOADING_INDICATOR:
                            FormControllerUtility.showProgressBar(this.view);
                            break;
                        case BBConstants.SHOW_PASSWORD_EXPIRY_WARNING:
                            this.setPasswordLockoutSettingWaring(uiModel.responseData);
                            break;
                        case BBConstants.SHOW_PASSWORD_EXPIRY_WARNING_ERROR:
                            this.setPasswordLockoutSettingWaring(uiModel.responseData);
                            break;
                    }
                }
                //olb related method calls
                if (uiModel.campaignRes) {
                    this.campaignSuccess(uiModel.campaignRes);
                }
                if (uiModel.campaignError) {
                    this.campaignFailure();
                }

                if (uiModel.PFMDisabled) {
                    this.disablePFMWidget();
                }
                if (uiModel.PFMMonthlyWidget) {
                    this.getPFMMonthlyDonutChart(uiModel.PFMMonthlyWidget);
                }
                if (uiModel.outageMessage) {
                    this.setOutageNotification(uiModel.outageMessage.show, uiModel.outageMessage.message);
                }
                if (uiModel.savedExteranlAccountsModel) {
                    this.presentExternalAccountsAddedConfirmation(uiModel.savedExteranlAccountsModel);
                }
                if (uiModel.externalBankLoginContext) {
                    this.showExternalBankLogin(uiModel.externalBankLoginContext);
                }
                if (uiModel.externalBankLogin) {
                    this.onExternalBankLoginSuccess(uiModel.externalBankLogin);
                }
                if (uiModel.externalBankLoginFailure) {
                    this.onExternalBankLoginFailure(uiModel.externalBankFailure);
                }
                if (uiModel.externalBankList) {
                    //activate side menu
                    this.showExternalBankList(uiModel.externalBankList);
                }
                if (uiModel.saveExternalBankCredentialsSuccess) {
                    this.onSuccessSaveExternalBankCredentailsSuccess(uiModel.saveExternalBankCredentialsSuccess);
                }
                if (uiModel.saveExternalBankCredentialsFailure) {
                    this.onSuccessSaveExternalBankCredentailsFailure(uiModel.saveExternalBankCredentialsFailure);
                }
                if (uiModel.externalBankAccountsModel) {
                    this.updateExternalBankAccountList(uiModel.externalBankAccountsModel);
                }
                if (uiModel.showLoadingIndicator) {
                    if (uiModel.showLoadingIndicator.status === true) {
                        FormControllerUtility.showProgressBar(this.view);
                    } else {
                        FormControllerUtility.hideProgressBar(this.view);

                    }
                }
                if (uiModel.serviceError) {
                    this.setServiceError(uiModel.serviceError);
                }
                if (uiModel.welcomeBanner) {
                    this.updateProfileBanner(uiModel.welcomeBanner);
                }
                if (uiModel.accountsSummary) {
                    var accounts = (kony.sdk.isNullOrUndefined(uiModel.accountsSummary) || uiModel.accountsSummary.constructor !== Array) ? [] : uiModel.accountsSummary;
                    this.updateAccountList(accounts);
                    if (accounts !== null && accounts.length !== 0) {
                        /*show cash position flex*/
                        if (this.transactionViewAccessCheck()) {
                            this.view.flxMyCashPosition.left = "0%";
                            this.view.flxMyCashPosition.setVisibility(true);
                        } else {
                            this.view.flxMyCashPosition.setVisibility(false);
                        }
                        this.view.upcomingTransactions.setVisibility(true);
                    } else {
                        /*hide cash position flex*/
                        this.view.flxMyCashPosition.setVisibility(false);
                        this.view.upcomingTransactions.setVisibility(false);
                    }
                }
                if (uiModel.unreadCount) {
                    this.updateAlertIcon(uiModel.unreadCount);
                }
                if (uiModel.UpcomingTransactions) {
                    this.showUpcomingTransactionsWidget(uiModel.UpcomingTransactions);
                }
                if (uiModel.unreadMessages) {
                    this.showMessagesWidget(uiModel.unreadMessages);
                }
                if (uiModel.externalBankAccountsModel) {
                    if (uiModel.externalBankAccountsModel.length > 0) {
                        this.presentExternalAccountsList(uiModel.externalBankAccountsModel);
                    } else {
                        this.onAllExternalAccountsAlreadyAdded();
                    }
                }
                if (uiModel.passwordResetWarning) {
                    this.setPasswordLockoutSettingWaring(uiModel.passwordResetWarning);
                }
            }
        },
        campaignSuccess: function(data) {
            var CampaignManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('CampaignManagement');
            CampaignManagementModule.presentationController.updateCampaignDetails(data);
            var self = this;
            if (data.length === 0) {
                this.view.flxBannerContainerMobile.setVisibility(false);
                this.view.flxBannerContainerDesktop.setVisibility(false);
            } else if (kony.application.getCurrentBreakpoint() >= 1366 && !orientationHandler.isMobile && !orientationHandler.isTablet) {
                this.view.flxBannerContainerDesktop.setVisibility(true);
                this.view.flxBannerContainerMobile.setVisibility(false);
                this.view.imgBannerDesktop.src = data[0].imageURL;
                this.view.imgBannerDesktop.onTouchStart = function() {
                    CampaignUtility.onClickofInAppCampaign(data[0].destinationURL);
                };
            } else {
                var self = this;
                this.view.flxBannerContainerMobile.setVisibility(true);
                this.view.flxBannerContainerDesktop.setVisibility(false);
                this.view.imgBannerMobile.src = data[0].imageURL;
                this.view.imgBannerMobile.onTouchStart = function() {
                    CampaignUtility.onClickofInAppCampaign(data[0].destinationURL);
                };
            }
            this.AdjustScreen();
        },

        /**
        Method to hide banners on fetch campaigns service failure
        */
        campaignFailure: function() {
            this.view.flxBannerContainerDesktop.setVisibility(false);
            this.view.flxBannerContainerMobile.setVisibility(false);
            this.AdjustScreen();
        },

        /**
        Method to check if the user has access to view any transaction.
        */
        transactionViewAccessCheck: function() {
            var transactionViewAccess = applicationManager.getConfigurationManager().getViewTransactionPermissionsList();
            var checkUserPermission = function(permission) {
                return applicationManager.getConfigurationManager().checkUserPermission(permission);
            };
            var isTransactionViewEnabled = transactionViewAccess.some(checkUserPermission);
            if (isTransactionViewEnabled)
                return true;
            else
                return false;
        },
        /*
        Method to fetch password expiry warning
        */
        getPasswordExpirationWarning: function(data) {
            if (data) {
                var navigationObject = {
                    requestData: {
                        "userName": this.username
                    },
                    onSuccess: {
                        form: "BBAccountsModule/frmBBAccountsLanding",
                        module: "BBAccountsModule",
                        context: {
                            key: BBConstants.SHOW_PASSWORD_EXPIRY_WARNING,
                            responseData: null
                        }
                    },
                    onFailure: {
                        form: "BBAccountsModule/frmBBAccountsLanding",
                        module: "BBAccountsModule",
                        context: {
                            key: BBConstants.SHOW_PASSWORD_EXPIRY_WARNING_ERROR,
                            responseData: null
                        }
                    }
                };
                this.presenter.presentationController.fetchPasswordExpirationWarning(navigationObject);
            } else {
                this.view.flxPasswordResetWarning.setVisibility(false);
                this.AdjustScreen();
            }
        },


        updateExternalBankAccountList: function(externalAccounts) {
            if (externalAccounts.length > 0) {
                this.presentExternalAccountsList(externalAccounts);
            } else {
                this.onAllExternalAccountsAlreadyAdded();
            }
        },

        updateAccountWidget: function(accounts) {
            var accountList = (kony.sdk.isNullOrUndefined(accounts) || accounts.constructor !== Array) ? [] : accounts;
            var isAccountAvailable = accountList.length > 0 ? true : false;
            this.updateAccountList(accountList);
            this.updateCashPositionUiBasedOnAccountsAvailability(isAccountAvailable);
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },

        updateCashPositionUiBasedOnAccountsAvailability: function(isAccountAvailable) {
            if (isAccountAvailable) {
                /*show cash position flex*/
                if (this.transactionViewAccessCheck()) {
                    this.view.flxMyCashPosition.left = "0%";
                    this.view.flxMyCashPosition.setVisibility(true);
                } else {
                    this.view.flxMyCashPosition.setVisibility(false);
                }
                this.view.upcomingTransactions.setVisibility(true);
            } else {
                /*hide cash position flex*/
                this.view.flxMyCashPosition.setVisibility(false);
                this.view.upcomingTransactions.setVisibility(false);
            }
            this.AdjustScreen();
        },

        updateLoadingIndicatorBasedOnStatus: function(status) {
            if (status === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else {
                FormControllerUtility.hideProgressBar(this.view);
            }
            this.AdjustScreen();
        },

        /**
         * Method to init frmBBAccountsLanding
         */
        initializeAccountsDashboard: function() {

            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule');
            this.username = applicationManager.getUserPreferencesManager().getUserObj().userName;
            this.initializeReceivablePayableChart();
            var currBreakpoint = kony.application.getCurrentBreakpoint();
            var chartProperties = {
                height: null
            };
            if (currBreakpoint === 640) {
                chartProperties.height = 170;
            } else {
                chartProperties.height = 285;
            }
            this.initializeCashPositionChart(chartProperties);
        },
        /**  Returns height of the page
         * @returns {String} height height of the page
         */
        getPageHeight: function() {
            var height = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height + this.view.flxFooter.info.frame.height + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT;
            return height + ViewConstants.POSITIONAL_VALUES.DP;
        },

        /**
          /**
           * Method to load and return Messages and Alerts Module.
           * @returns {object} Messages and Alerts Module object.
           */
        loadAccountModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        },
        /**
         * Method to set data to the Apprvals/Requests widget
         */


        initApprovalRequestWidget: function() {
            var checkUserPermission = function(permission) {
                return applicationManager.getConfigurationManager().checkUserPermission(permission);
            };
            var approvalPermissions = applicationManager.getConfigurationManager().getApprovalsFeaturePermissionsList();
            var requestsPermissions = applicationManager.getConfigurationManager().getRequestsFeaturePermissionsList();
            var isApproveEnabled = approvalPermissions.some(checkUserPermission);
            var isRequestsEnabled = requestsPermissions.some(checkUserPermission);

            if (!isApproveEnabled && !isRequestsEnabled) {
                this.view.flxApprovalAndRequest.setVisibility(false);
            } else {
                if (isApproveEnabled)
                    this.updateMyApprovalsWidget();
                else
                    this.view.flxMyApprovals.setVisibility(false);

                if (isRequestsEnabled)
                    this.updateMyRequestsWidget();
                else
                    this.view.flxMyRequests.setVisibility(false);
            }
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },


        /**
         * Method to hide approval requests widget on service failure
         */
        approvalRequestsServiceFailure: function() {
            this.view.flxApprovalAndRequest.setVisibility(false);
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },


        updateMyApprovalsWidget: function() {
            var approvalData = [
                [{
                        lblWidgetTitle: kony.i18n.getLocalizedString("i18n.konybb.Common.MyApprovals"),
                        imgApprovals: {
                            src: "bbapproval.png"
                        },
                        btnShowAll: {
                            text: kony.i18n.getLocalizedString("i18n.konybb.Common.ViewallApprovals"),
                            onClick: function() {
                                var navigationObject = {
                                    "requestData": null,
                                    "onSuccess": {
                                        "form": "frmBBMyApprovals",
                                        "module": "ApprovalsReqModule",
                                        "context": {
                                            "key": BBConstants.DASHBOARD_DEFAULT_TAB,
                                            "responseData": null
                                        }
                                    },
                                    "onFailure": {
                                        "form": "AuthModule/frmLogin",
                                        "module": "AuthModule",
                                        "context": {
                                            "key": BBConstants.LOG_OUT,
                                            "responseData": null
                                        }
                                    }
                                };
                                var ApprovalsReqModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
                                ApprovalsReqModule.presentationController.noServiceNavigate(navigationObject);
                            }
                        },
                        template: "flxDashBoardWidgetHeader"
                    },
                    []
                ]
            ];

            if (applicationManager.getConfigurationManager().isApproveTransactionEnabled()) {
                if (applicationManager.getConfigurationManager().transactionsPendingForMyApprovalCount > 0)
                    approvalData[0][1].push({
                        lblName: kony.i18n.getLocalizedString("i18n.common.transactions"),
                        lblOption1Value: applicationManager.getConfigurationManager().transactionsPendingForMyApprovalCount + ""
                    });
            }
            if (applicationManager.getConfigurationManager().isApproveACHEnabled()) {
                if (applicationManager.getConfigurationManager().achTransactionsPendingForMyApprovalCount > 0)
                    approvalData[0][1].push({
                        lblName: kony.i18n.getLocalizedString("i18n.konybb.Common.ACHTransactions"),
                        lblOption1Value: applicationManager.getConfigurationManager().achTransactionsPendingForMyApprovalCount + ""
                    });
                if (applicationManager.getConfigurationManager().achFilesPendingForMyApprovalCount > 0)
                    approvalData[0][1].push({
                        lblName: kony.i18n.getLocalizedString("i18n.konybb.Common.ACHFiles"),
                        lblOption1Value: applicationManager.getConfigurationManager().achFilesPendingForMyApprovalCount + ""
                    });
            }

            this.view.myApprovals.setWidgetData(approvalData);
            if (approvalData[0][1].length > 0) {
                this.view.flxMyApprovals.setVisibility(true);
            } else {
                this.view.flxMyApprovals.setVisibility(false);
            }
            this.view.flxApprovalAndRequest.setVisibility(true);
            this.AdjustScreen();
            this.view.forceLayout();
        },

        updateMyRequestsWidget: function() {
            var AwaitingCount = applicationManager.getConfigurationManager().AwaitingCount;
            var RejectedCount = applicationManager.getConfigurationManager().RejectedCount;
            var ApprovedCount = applicationManager.getConfigurationManager().ApprovedCount;
            var requestData = [
                [{
                        lblWidgetTitle: kony.i18n.getLocalizedString("i18n.konybb.Common.MyRequests"),
                        imgApprovals: {
                            src: "bbrequests.png"
                        },
                        btnShowAll: {
                            text: kony.i18n.getLocalizedString("i18n.konybb.Common.ViewallRequests"),
                            onClick: function() {
                                var navigationObject = {
                                    "requestData": null,
                                    "onSuccess": {
                                        "form": "frmBBMyRequests",
                                        "module": "ApprovalsReqModule",
                                        "context": {
                                            "key": BBConstants.DASHBOARD_DEFAULT_TAB,
                                            "responseData": null
                                        }
                                    },
                                    "onFailure": {
                                        "form": "AuthModule/frmLogin",
                                        "module": "AuthModule",
                                        "context": {
                                            "key": BBConstants.LOG_OUT,
                                            "responseData": null
                                        }
                                    }
                                };
                                var ApprovalsReqModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
                                ApprovalsReqModule.presentationController.noServiceNavigate(navigationObject);
                            }
                        },
                        template: "flxDashBoardWidgetHeader"
                    },
                    []
                ]
            ];

            if (AwaitingCount > 0) {
                requestData[0][1].push({
                    lblName: kony.i18n.getLocalizedString("i18n.konybb.Common.Awaiting"),
                    lblOption1Value: AwaitingCount + ""
                });
            }
            if (RejectedCount > 0) {
                requestData[0][1].push({
                    lblName: kony.i18n.getLocalizedString("i18n.konybb.Common.Rejected"),
                    lblOption1Value: RejectedCount + ""
                });
            }
            if (ApprovedCount > 0) {
                requestData[0][1].push({
                    lblName: kony.i18n.getLocalizedString("i18n.konybb.Common.Approved"),
                    lblOption1Value: ApprovedCount + ""
                });
            }

            this.view.myRequests.setWidgetData(requestData);

            if (requestData[0][1].length > 0) {
                this.view.flxMyRequests.setVisibility(true);
            } else {
                this.view.flxMyRequests.setVisibility(false);
            }
            this.view.flxApprovalAndRequest.setVisibility(true);
            this.view.forceLayout();
            this.AdjustScreen();
        },

        formPreshowUISetting: function() {
            var scopeObj = this;
            scopeObj.view.flxDowntimeWarning.setVisibility(false);
            this.view.accountListMenu.setVisibility(false);

            //resetting MasterData
            this.view.mySpending.lblOverallSpendingAmount.isVisible = false; //Setting Visiblity of PFM false to show no masterData
            this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(true);
            this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(false);

            this.view.flxSecondaryDetails.setVisibility(false);
            this.view.flxReceivablesPayables.setVisibility(false);

            this.view.btnContactUs.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.footer.contactUs"));

            if (CommonUtilities.isCSRMode()) {
                this.view.btnOpenNewAccount.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(15);
                //write a new message
                this.view.myMessages.btnWriteNewMessage.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(15);
            }
            this.view.accountList.lblImgDropdown.toolTip = "";
            this.AdjustScreen();
        },

        initActions: function() {
            var scopeObj = this;
            var currBreakpoint = kony.application.getCurrentBreakpoint();
            this.view.btnContactUs.onClick = function() {
                this.presenter.presentationController.showContactUs();
            }.bind(this);

            this.view.AddExternalAccounts.LoginUsingSelectedBank.imgViewPassword.onTouchStart = this.showPassword;
            this.view.imgCloseDowntimeWarning.onTouchEnd = function() {
                scopeObj.setServiceError(false);
            }.bind(this);
            this.view.imgCloseOutageWarning.onTouchEnd = function() {
                this.view.flxOutageWarning.setVisibility(false);
                this.AdjustScreen();
                FormControllerUtility.hideProgressBar(this.view);
            }.bind(this);
            if (CommonUtilities.isCSRMode()) {
                //new user onboarding
                this.view.btnOpenNewAccount.onClick = CommonUtilities.disableButtonActionForCSRMode();
                //write a new message
                this.view.myMessages.btnWriteNewMessage.onClick = CommonUtilities.disableButtonActionForCSRMode();
            } else {
                //new user onboarding
                this.view.btnOpenNewAccount.onClick = this.loadAccountModule().presentationController.navigateToNewAccountOpening.bind(this.loadAccountModule().presentationController);
                //write a new message
                this.view.myMessages.btnWriteNewMessage.onClick = this.loadAccountModule().presentationController.newMessage.bind(this.loadAccountModule().presentationController);
            }

            this.AdjustScreen();
        },

        showFilterPopup: function() {
            var scopeObj = this;
            if (scopeObj.view.FavouriteAccountTypes.origin) {
                scopeObj.view.FavouriteAccountTypes.origin = false;
                return;
            }
            var Popuptop = scopeObj.view.flxAccountListAndBanner.frame.y + scopeObj.view.flxLeftContainer.frame.y + 30;
            Popuptop = Popuptop.toString();
            scopeObj.view.FavouriteAccountTypes.top = Popuptop + ViewConstants.POSITIONAL_VALUES.DP;
            var currBreakpoint = kony.application.getCurrentBreakpoint();
            if (currBreakpoint === 640 || orientationHandler.isMobile) {
                scopeObj.view.FavouriteAccountTypes.left = "";
                scopeObj.view.FavouriteAccountTypes.right = "10dp";
            } else if (currBreakpoint === 1024 || orientationHandler.isTablet) {
                scopeObj.view.FavouriteAccountTypes.left = "";
                scopeObj.view.FavouriteAccountTypes.right = "24dp";
            } else {
                scopeObj.view.FavouriteAccountTypes.left = "";
                scopeObj.view.FavouriteAccountTypes.right = scopeObj.view.flxMainWrapper.frame.x + scopeObj.view.flxRightContainer.frame.width + 102 + "dp";
            }
            scopeObj.view.accountListMenu.isVisible = false;
            scopeObj.view.accountList.lblImgDropdown.text = "P";

            scopeObj.view.FavouriteAccountTypes.isVisible = true;
            //scopeObj.view.FavouriteAccountTypes.imgToolTip.setFocus(true);

            scopeObj.view.FavouriteAccountTypes.forceLayout();
            scopeObj.view.accountList.forceLayout();
            scopeObj.AdjustScreen();
        },

        setHeader: function() {
            this.view.customheader.headermenu.lblNewNotifications.setVisibility(false);
            this.view.customheader.headermenu.imgNotifications.src = ViewConstants.IMAGES.NOTIFICATION_ICON;
            this.view.customheader.customhamburger.activateMenu("Accounts", "My Accounts");
            this.view.customheader.forceCloseHamburger();
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
            this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
        },

        /*
        	Method to show durations drop down
        */
        showDurationDropDown: function() {
            var visible = this.view.flxDurationList.isVisible ? false : true;
            this.view.flxAccountList.setVisibility(false);
            this.view.imgAccountsDropDown.text = "O";
            this.view.imgAccountsDropdownMobile.text = "O";
            if (visible) {
                this.view.imgYearsDropDownMobile.text = "P";
                this.view.imgYearsDropDown.text = "P";
            } else {
                this.view.imgYearsDropDownMobile.text = "O";
                this.view.imgYearsDropDown.text = "O";
            }
            this.view.flxDurationList.setVisibility(visible);
        },

        /*
        	Method to show accounts drop down
        */
        showAccountsDropDown: function() {
            var visible = this.view.flxAccountList.isVisible ? false : true;
            this.view.flxDurationList.setVisibility(false);
            this.view.imgYearsDropDown.text = "O";
            this.view.imgYearsDropDownMobile.text = "O";
            if (visible) {
                this.view.imgAccountsDropdownMobile.text = "P";
                this.view.imgAccountsDropDown.text = "P";
            } else {
                this.view.imgAccountsDropdownMobile.text = "O";
                this.view.imgAccountsDropDown.text = "O";
            }
            this.view.flxAccountList.setVisibility(visible);

        },

        /*
        	Method to fetch cash position on duration type drop down click
        */
        fetchCashPositionByDurationOnRowClick: function(seguiWidget, sectionNumber, rowNumber, selectedState) {

            var selectedRow = this.view.durationListMenu.segAccountListActions.selectedRowItems[sectionNumber];
            var duration = selectedRow.lblUsers;
            var currBreakpoint = kony.application.getCurrentBreakpoint();

            this.view.imgYearsDropDown.text = "O";
            this.view.imgYearsDropDownMobile.text = "O";

            if (currBreakpoint === 640) {
                this.view.btnSelectYearsMobile.text = duration;
            } else {
                this.view.btnSelectYears.text = duration;
            }

            var inputPayLoad = this.getCashPositionRequestData();
            this.loadCashposition(inputPayLoad);
            /* close the flex duration segment */
            this.view.flxDurationList.setVisibility(false);

        },

        /*
        	Method to fetch cash position on account type drop down click
        */
        fetchCashPositionByAccountTypeOnRowClick: function(seguiWidget, sectionNumber, rowNumber, selectedState) {

            var selectedRow = this.view.selectAccountList.segAccountListActions.selectedRowItems[sectionNumber];
            var accountType = selectedRow.lblUsers;
            var currBreakpoint = kony.application.getCurrentBreakpoint();

            this.view.imgAccountsDropDown.text = "O";
            this.view.imgAccountsDropdownMobile.text = "O";

            if (currBreakpoint === 640) {
                this.view.btnSelectAccountsMobile.text = accountType;
            } else {
                this.view.btnSelectAccounts.text = accountType;
            }

            var inputPayLoad = this.getCashPositionRequestData();
            this.loadCashposition(inputPayLoad);
            /* close the flex accounts segment */
            this.view.flxAccountList.setVisibility(false);

        },

        /*
        	Method to form the request data based on duration and account type selected from the dropdown
        */
        getCashPositionRequestData: function() {
            var inputPayLoad = {
                "Duration": BBConstants.LAST_YEAR,
                "AccountType": BBConstants.ALL
            };

            var duration;
            var accountType;
            if (kony.application.getCurrentBreakpoint() === 640) {
                duration = this.view.btnSelectAccountsMobile.text;
                accountType = this.view.btnSelectYearsMobile.text;
            } else {
                duration = this.view.btnSelectYears.text;
                accountType = this.view.btnSelectAccounts.text;
            }

            if (duration === kony.i18n.getLocalizedString("i18n.konybb.cashPosition.thisYear")) {
                inputPayLoad.Duration = BBConstants.THIS_YEAR;
            } else if (duration === kony.i18n.getLocalizedString("i18n.konybb.cashPosition.lastYear")) {
                inputPayLoad.Duration = BBConstants.LAST_YEAR;
            } else if (duration === kony.i18n.getLocalizedString("i18n.konybb.cashPosition.thisMonth")) {
                inputPayLoad.Duration = BBConstants.MONTH;
            } else if (duration === kony.i18n.getLocalizedString("i18n.konybb.cashPosition.lastBusinessWeek")) {
                inputPayLoad.Duration = BBConstants.WEEK;
            }

            if (accountType === kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")) {
                inputPayLoad.AccountType = BBConstants.ALL;
            } else if (accountType === kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.CheckingAccounts")) {
                inputPayLoad.AccountType = BBConstants.CHECKINGS;
            } else if (accountType === kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.SavingsAccounts")) {
                inputPayLoad.AccountType = BBConstants.SAVINGS;
            }

            return inputPayLoad;
        },

        /**
         * This function will setup UI based on Permissions
         **/
        setPermissionBasedView: function() {
            var configurationManager = applicationManager.getConfigurationManager();
            //Setting OpenNewAccount Visibility based on Permission
            if (applicationManager.getConfigurationManager().checkUserPermission("OPEN_NEW_ACCOUNT")) {
                this.view.btnOpenNewAccount.setVisibility(true);
                this.view.flxSeparator.setVisibility(true);

            } else {
                this.view.btnOpenNewAccount.setVisibility(false);
                this.view.flxSeparator.setVisibility(false);
                this.view.flxPrimaryActions.height = "50dp";
                this.view.flxPrimaryActions.forceLayout();
            }

            //Setting Approvals Visibility based on Permission
            if (configurationManager.isApproveTransactionEnabled() || configurationManager.isApproveACHEnabled()) {
                this.view.flxMyApprovals.setVisibility(true);
            } else {
                this.view.flxMyApprovals.setVisibility(false);
            }

            //Setting Requests Visibility based on Permission
            if (configurationManager.isRequestTransactionEnabled() || configurationManager.isRequestACHEnabled()) {
                this.view.flxMyRequests.setVisibility(true);
            } else {
                this.view.flxMyRequests.setVisibility(false);
            }
            this.AdjustScreen();
        },

        /**
         * This function shows the masked password on click of the eye icon
         */
        showPassword: function() {
            if (this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.secureTextEntry === true) {
                this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.secureTextEntry = false;
            } else {
                this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.secureTextEntry = true;
            }
        },

        /**
         *
         */
        setAccountListData: function() {},
        /**
         *
         */
        onLoadChangePointer: function() {
            this.view.customheader.imgKony.setFocus(true);
        },
        /**
         *
         */
        setContextualMenuLeft: function() {},
        /**
         * Ui team proposed method to handle screen aligment
         */
        AdjustScreen: function() {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.info.frame.height + this.view.flxMain.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;
                } else {
                    this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
                }
            } else {
                this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
            }
            this.view.forceLayout();
            this.initializeResponsiveViews();
        },

        onExternalBankLoginSuccess: function(response) {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            var username = this.ExternalLoginContextData.username;
            var password = this.ExternalLoginContextData.password;
            var sessionToken = response.params.SessionToken;
            var mainUser = applicationManager.getUserPreferencesManager().getCurrentUserName();
            var bankId = this.ExternalLoginContextData.bankId;
            authModule.presentationController.saveExternalBankCredentials(username, password, sessionToken, mainUser, bankId);
        },

        onExternalBankLoginFailure: function(response) {
            this.view.AddExternalAccounts.LoginUsingSelectedBank.lblLoginUsingSelectedBankError.text = kony.i18n.getLocalizedString("i18n.login.failedToLogin");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankError.isVisible = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text = "";
            this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text,
                this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
            CommonUtilities.hideProgressBar(this.view);
        },

        /**
         * Method to set error message if service call fails
         * @param {boolean} status true/false
         */
        setServiceError: function(status) {
            if (status) {
                this.view.flxDowntimeWarning.setVisibility(true);
                this.view.lblDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
                this.view.lblDowntimeWarning.setFocus(true);
            } else {
                this.view.flxDowntimeWarning.setVisibility(false);
            }
            this.AdjustScreen();

            FormControllerUtility.hideProgressBar(this.view);
        },

        /**
         * Method to set warning message for password expiry
         * @param {boolean} status true/false
         */

        setPasswordLockoutSettingWaring: function(status) {
            if (status.show === true && status.message.passwordExpiryWarningRequired === "true") {
                var expiryMsg = kony.i18n.getLocalizedString("18n.credMgmt.passwordExpiryWarning");
                this.view.lblPasswordResetWarning.text = expiryMsg.replace("xxx", status.message.passwordExpiryRemainingDays);
                this.view.flxPasswordResetWarning.setVisibility(true);
                this.view.imgClosePasswordResetWarning.onTouchEnd = function() {
                    this.view.flxPasswordResetWarning.setVisibility(false);
                    this.AdjustScreen();
                    this.view.forceLayout();
                }.bind(this);
            } else {
                this.view.flxPasswordResetWarning.setVisibility(false);
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
            this.view.forceLayout();
        },

        /**
         * Method to show outage message
         * @param {Boolean} isOutage true/false
         * @param {String} outageMessage Message to show
         */
        setOutageNotification: function(isOutage, outageMessage) {
            var scopeObj = this;
            var outageUI = scopeObj.view.flxOutageWarning;
            if (isOutage && !outageUI.isVisible) {
                scopeObj.view.lblOutageWarning.text = outageMessage || "";
                scopeObj.view.imgCloseOutageWarning.onTouchEnd = function() {
                    scopeObj.setOutageNotification(false);
                };
                outageUI.setVisibility(true);
                scopeObj.view.lblOutageWarning.setFocus(true);
                scopeObj.AdjustScreen();
            } else if (!isOutage && outageUI.isVisible) {
                outageUI.setVisibility(false);
                var acctop = scopeObj.view.accountListMenu.frame.y + 6;
                scopeObj.view.accountListMenu.top = acctop + "dp";
                scopeObj.AdjustScreen();
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * Method updates the frmAccountsDashboard with the user's first name, profile image & last logged
         * @param {JSON} profileBannerData Data of user like login time, name etc
         */
        updateProfileBanner: function(profileBannerData) {
            this.view.welcome.lblWelcome.text = kony.i18n.getLocalizedString('i18n.accounts.welcome') + ' ' + profileBannerData.userfirstname + '!';
            this.view.welcome.lblLastLoggedIn.text = kony.i18n.getLocalizedString('i18n.accounts.lastLoggedIn') + ' ' + profileBannerData.lastlogintime;
            var userImageUrl = (kony.sdk.isNullOrUndefined(profileBannerData.userImageURL) || (profileBannerData.userImageURL === "")) ?
                "header_default_userimg_mod.png" : profileBannerData.userImageURL;
            this.view.welcome.imgProfile.src = userImageUrl;
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Returns if a account is favourite or not
         * @param {JSON} account Account whose favourite status needs to be checked
         * @returns {boolean} true/false
         */
        isFavourite: function(account) {
            return account.favouriteStatus && account.favouriteStatus === '1';
        },
        /**
         * filter the accounts based on currentFilter
         * @param {Collection} accounts List of accounts
         * @param {String} currentFilter Fileter like Favourite etc
         * @returns {Collection} filtered account based on current filter
         */
        filterAccounts: function(accounts, currentFilter) {
            if (currentFilter === kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")) {
                return accounts;
            }
            return accounts.filter(this.isFavourite);
        },
        /**
         * Method to set bank accounts for the segment
         * @param {Collection} accounts List of accounts
         * @param {String} currentFilter like Favourite etc
         * @param {Collection} favAccounts List of favourite accounts
         * @returns {JSON} bank segment data
         */
        setBanksAccountSegment: function(accounts, currentFilter, favAccounts) {
            var banks = [];
            var preferredAccounts = {
                "lblUsers": {
                    "toolTip": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") +
                        " " + kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.PreferredAccounts"),
                    "key": kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.PreferredAccounts"),
                    "text": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") +
                        " " + kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.PreferredAccounts")
                },
                "lblSeparator": {
                    "text": "label"
                }
            };

            var allAccounts = {
                "lblUsers": {
                    "toolTip": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") +
                        " " + kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts"),
                    "key": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts"),
                    "text": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") +
                        " " + kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")
                },
                "lblSeparator": {
                    "text": "label"
                }
            };

            if (currentFilter === kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")) {
                if (favAccounts.length > 0) {

                    banks.push(preferredAccounts);
                }
            } else if (currentFilter === kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts")) {
                banks.push(allAccounts);
            }


            return banks;
        },

        /**
         * Method that gets called on selection of row of accounts segment
         * @param {Collection} accounts List of accounts
         * @param {Collection} banks List of banks
         */
        onSegAccountTypeRowClick: function(accounts, banks) {
            var selectedItem = this.view.FavouriteAccountTypes.segAccountTypes.selectedRowItems[0];
            this.view.accountList.btnShowAllAccounts.text = selectedItem.lblUsers.key;
            this.view.accountList.btnShowAllAccounts.toolTip = selectedItem.lblUsers.key;
            this.currentFilter = selectedItem.lblUsers.key;
            var newBanks = [];
            var preferredAccounts = {
                "lblUsers": {
                    "toolTip": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") +
                        " " + kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.PreferredAccounts"),
                    "key": kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.PreferredAccounts"),
                    "text": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") +
                        " " + kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.PreferredAccounts")
                },
                "lblSeparator": {
                    "text": "label"
                }
            };

            var allAccounts = {
                "lblUsers": {
                    "toolTip": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") +
                        " " + kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts"),
                    "key": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts"),
                    "text": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") +
                        " " + kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")
                },
                "lblSeparator": {
                    "text": "label"
                }
            };

            if (selectedItem.lblUsers.key === kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")) {
                this.view.accountList.segAccounts.setData(this.prepareAccountsViewBasedOnAccountType(this.createAccountSegmentsModel(accounts)));
                if (accounts.filter(this.isFavourite).length > 0) {
                    newBanks.push(preferredAccounts);
                }

                this.AdjustScreen();
            } else if (selectedItem.lblUsers.key === kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.PreferredAccounts")) {
                this.view.accountList.segAccounts.setData(this.prepareAccountsViewBasedOnAccountType(this.createAccountSegmentsModel(accounts.filter(this.isFavourite))));
                newBanks.push(allAccounts);
                this.AdjustScreen();
            }

            this.view.FavouriteAccountTypes.isVisible = false;
            this.view.FavouriteAccountTypes.segAccountTypes.setData(newBanks);
            this.view.accountList.forceLayout();
            this.AdjustScreen();
        },

        accountTypeConfig: {
            'Savings': {
                sideImage: ViewConstants.SIDEBAR_TURQUOISE,
                sideSkin: ViewConstants.SKINS.SAVINGS_SIDE_BAR,
                balanceKey: 'availableBalance',
                balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
            },
            'Checking': {
                sideImage: ViewConstants.SIDEBAR_PURPLE,
                sideSkin: ViewConstants.SKINS.CHECKINGS_SIDE_BAR,
                balanceKey: 'availableBalance',
                balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
            },
            'CreditCard': {
                sideImage: ViewConstants.SIDEBAR_YELLOW,
                sideSkin: ViewConstants.SKINS.CREDIT_CARD_SIDE_BAR,
                balanceKey: 'currentBalance',
                balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
            },
            'Deposit': {
                sideImage: ViewConstants.SIDEBAR_BLUE,
                sideSkin: ViewConstants.SKINS.DEPOSIT_SIDE_BAR,
                balanceKey: 'currentBalance',
                balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
            },
            'Mortgage': {
                sideImage: ViewConstants.SIDEBAR_BROWN,
                sideSkin: ViewConstants.SKINS.MORTGAGE_CARD_SIDE_BAR,
                balanceKey: 'outstandingBalance',
                balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.outstandingBalance')
            },
            'Loan': {
                sideImage: ViewConstants.SIDEBAR_BROWN,
                sideSkin: ViewConstants.SKINS.LOAN_SIDE_BAR,
                balanceKey: 'outstandingBalance',
                balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.outstandingBalance')
            },
            'Default': {
                sideImage: ViewConstants.SIDEBAR_TURQUOISE,
                sideSkin: ViewConstants.SKINS.SAVINGS_SIDE_BAR,
                balanceKey: 'availableBalance',
                balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
            }
        },

        /**
         * Method that gets called to show account details
         * @param {JSON} account Account whose details needs to be shown
         */
        onAccountSelection: function(account) {
            this.loadAccountModule().presentationController.showAccountDetails(account);
        },

        /**
         *
         */
        showExternalAccountUpdateAlert: function() {},

        /** NEED TO REMOVE 
         * Method to check if action is valid or not
         * @param {String} actionName Name of action
         * @param {JSON} account List of accounts
         * @returns {Boolean} true/false
         */
        isValidAction: function(actionName, account) {
            var isValid = false;
            var configurationManager = applicationManager.getConfigurationManager();
            var OLBConstants = configurationManager.OLBConstants;
            var principalBalance, currentAmountDue;
            switch (actionName) {
                case OLBConstants.ACTION.PAY_A_BILL:
                    isValid = (configurationManager.getConfigurationValue('isBillPaymentCreateTransactionEnabled') === "true") &&
                        (account.supportBillPay === "1") && (account.accountType === OLBConstants.ACCOUNT_TYPE.CHECKING || account.accountType === OLBConstants.ACCOUNT_TYPE.CREDITCARD);
                    break;
                case OLBConstants.ACTION.MAKE_A_TRANSFER:
                    isValid = configurationManager.isFundsTransferCreateTransactionEnabled() &&
                        (account.accountType === OLBConstants.ACCOUNT_TYPE.CHECKING || account.accountType === OLBConstants.ACCOUNT_TYPE.SAVING);
                    break;
                case OLBConstants.ACTION.TRANSFER_MONEY:
                    isValid = configurationManager.isFundsTransferCreateTransactionEnabled() &&
                        (account.accountType === OLBConstants.ACCOUNT_TYPE.CHECKING || account.accountType === OLBConstants.ACCOUNT_TYPE.SAVING);
                    break;
                case OLBConstants.ACTION.VIEW_STATEMENTS:
                    isValid = (account.accountType === OLBConstants.ACCOUNT_TYPE.CHECKING || account.accountType === OLBConstants.ACCOUNT_TYPE.SAVING ||
                        account.accountType === OLBConstants.ACCOUNT_TYPE.MORTGAGE || account.accountType === OLBConstants.ACCOUNT_TYPE.CREDITCARD ||
                        account.accountType === OLBConstants.ACCOUNT_TYPE.DEPOSIT || account.accountType === OLBConstants.ACCOUNT_TYPE.LOAN);
                    break;
                case OLBConstants.UPDATE_ACCOUNT_SETTINGS:
                    isValid = (account.accountType === OLBConstants.ACCOUNT_TYPE.LOAN || account.accountType === OLBConstants.ACCOUNT_TYPE.DEPOSIT);
                    break;
                case OLBConstants.ACTION.PAY_A_PERSON_OR_SEND_MONEY:
                    isValid = configurationManager.getConfigurationValue('isP2PCreateTransactionEnabled') === "true" &&
                        (account.accountType === OLBConstants.ACCOUNT_TYPE.CHECKING);
                    break;
                case OLBConstants.ACTION.PAY_DUE_AMOUNT:
                    principalBalance = Number(account.principalBalance) ? Number(account.principalBalance) : 0;
                    currentAmountDue = Number(account.currentAmountDue) ? Number(account.currentAmountDue) : 0;
                    isValid = (configurationManager.isFundsTransferCreateTransactionEnabled()) &&
                        (principalBalance > 0 && currentAmountDue > 0) &&
                        (account.accountType === OLBConstants.ACCOUNT_TYPE.LOAN);
                    break;
                case OLBConstants.ACTION.PAYOFF_LOAN:
                    principalBalance = Number(account.principalBalance) ? Number(account.principalBalance) : 0;
                    isValid = configurationManager.getConfigurationValue('payOffLoanPaymentEnabled') === "true" &&
                        (principalBalance > 0) && (account.accountType === OLBConstants.ACCOUNT_TYPE.LOAN);
                    break;
                case OLBConstants.ACTION.STOPCHECKS_PAYMENT:
                    isValid = (configurationManager.getConfigurationValue('isCheckManagementEnabled') === "true") &&
                        (account.accountType === OLBConstants.ACCOUNT_TYPE.CHECKING || account.accountType === OLBConstants.ACCOUNT_TYPE.SAVING);
                    break;
                default:
                    isValid = true;
            }
            return isValid;
        },

        /**
         * Method to get quick actions for accounts
         * @param {Object} dataInput Data inputs like onCancel/accountType etc
         * @returns {Object} quick action for selected account
         */
        getQuickActions: function(dataInput) {
            var self = this;
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            var onCancel = dataInput.onCancel;
            var quickActions = [{
                actionName: OLBConstants.ACTION.SCHEDULED_TRANSACTIONS,
                displayName: dataInput.scheduledDisplayName || kony.i18n.getLocalizedString("i18n.accounts.scheduledTransactions"),
                action: function(account) {
                    if (dataInput.showScheduledTransactionsForm) {
                        dataInput.showScheduledTransactionsForm(account);
                    }
                }
            }, {
                actionName: OLBConstants.ACTION.MAKE_A_TRANSFER, //MAKE A TRANSFER
                displayName: (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") ? kony.i18n.getLocalizedString("i18n.hamburger.transfer") : (dataInput.makeATransferDisplayName || kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer")),
                action: function(account) {
                    //Function call to  open tranfers page with parameter - account obj to be tranferred from.
                    if (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") {
                        applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen({
                            accountFrom: dataInput.accountNumber
                        });
                    } else {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            accountObject: account,
                            onCancelCreateTransfer: onCancel
                        });
                    }
                }
            }, {
                actionName: OLBConstants.ACTION.TRANSFER_MONEY, //MAKE A TRANSFER
                displayName: (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") ? kony.i18n.getLocalizedString("i18n.hamburger.transfer") : (dataInput.tranferMoneyDisplayName || kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer")),
                action: function(account) {
                    //Function call to  open tranfers page with parameter - account obj to be tranferred from.
                    if (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") {
                        applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen({
                            accountFrom: dataInput.accountNumber
                        });
                    } else {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            accountObject: account,
                            onCancelCreateTransfer: onCancel
                        });
                    }
                }
            }, {
                actionName: OLBConstants.ACTION.PAY_MONEY, //Make Payment
                displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.makePayFrom"),
                action: function(account) {
                    if (applicationManager.getConfigurationManager().getDeploymentGeography() == "EUROPE") {
                        applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({
                            context: "MakePayment",
                            accountFrom: dataInput.accountNumber
                        });
                        return;
                    }

                }
            }, {
                actionName: OLBConstants.ACTION.PAY_A_BILL,
                displayName: (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") ? kony.i18n.getLocalizedString("i18n.Pay.PayBill") : (dataInput.payABillDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payABillFrom")),
                action: function(account) {
                    //Function call to open bill pay screen
                    var billPaymentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule");
                    billPaymentModule.presentationController.showBillPaymentScreen({
                        "sender": "Accounts",
                        "context": "PayABillWithContext",
                        "loadBills": true,
                        "data": {
                            "fromAccountNumber": account.accountID,
                            "show": 'PayABill',
                            "onCancel": onCancel
                        }
                    });
                }
            }, {
                actionName: OLBConstants.ACTION.PAY_A_PERSON_OR_SEND_MONEY,
                displayName: dataInput.sendMoneyDisplayName || kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                action: function(account) {
                    var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    var dataItem = account;
                    dataItem.onCancel = onCancel;
                    p2pModule.presentationController.showPayAPerson("sendMoneyTab", dataItem);
                }
            }, {
                actionName: OLBConstants.ACTION.PAY_DUE_AMOUNT,
                displayName: dataInput.payDueAmountDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payDueAmount"),
                action: function(account) {

                    var data = {
                        "accounts": account
                    };
                    var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                    loanModule.presentationController.navigateToLoanDue(data);
                }
            }, {
                actionName: OLBConstants.ACTION.PAYOFF_LOAN,
                displayName: dataInput.payoffLoanDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan"),
                action: function(account) {

                    var data = {
                        "accounts": account
                    };
                    var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                    loanModule.presentationController.navigateToLoanPay(data);
                }
            }, {
                actionName: OLBConstants.ACTION.STOPCHECKS_PAYMENT,
                displayName: dataInput.stopCheckPaymentDisplayName || kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS"),
                action: function(account) {
                    var stopPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
                    stopPaymentsModule.presentationController.showStopPayments({
                        onCancel: onCancel,
                        accountID: account.accountID,
                        "show": OLBConstants.ACTION.SHOW_STOPCHECKS_FORM
                    });
                }
            }, {
                actionName: OLBConstants.ACTION.VIEW_STATEMENTS,
                displayName: dataInput.viewStatementsDisplayName || kony.i18n.getLocalizedString("i18n.ViewStatements.STATEMENTS"),
                action: function(account) {
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showFormatEstatements(account);
                }
            }, {
                actionName: OLBConstants.ACTION.UPDATE_ACCOUNT_SETTINGS,
                displayName: dataInput.updateAccountSettingsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateAccountSettings"),
                action: function() {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.enterProfileSettings("accountSettings");
                }
            }, {
                actionName: OLBConstants.ACTION.REMOVE_ACCOUNT,
                displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.removeAccount"),
                action: function(account) {
                    var accountData = account;
                    self.showDeletePopUp(account);
                }
            }, {
                actionName: OLBConstants.ACTION.ACCOUNT_PREFERENCES,
                displayName: kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountPreferences"),
                action: function() {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.showPreferredAccounts();
                }
            }, {
                actionName: OLBConstants.ACTION.EDIT_ACCOUNT,
                displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.editAccount"),
                action: function(account) {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.showEditExternalAccount(account);

                }
            }, {
                actionName: OLBConstants.ACTION.ACCOUNT_ALERTS,
                displayName: kony.i18n.getLocalizedString("i18n.Alerts.AccountAlertSettings"),
                action: function(account) {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.fetchAlertsCategory("alertSettings2", account.accountID);
                }
            }, {
                actionName: OLBConstants.ACTION.SET_AS_FAVOURITE,
                displayName: kony.i18n.getLocalizedString("i18n.AccountsLanding.setAsFavourite"),
                action: function(account) {
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.changeAccountFavouriteStatus(account);
                }
            }, {
                actionName: OLBConstants.ACTION.REMOVE_AS_FAVOURITE,
                displayName: kony.i18n.getLocalizedString("i18n.AccountsLanding.removefavourite"),
                action: function(account) {
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.changeAccountFavouriteStatus(account);
                }
            }];
            return quickActions;
        },

        /**
         * enableLogoutAction :  Method to reinitialize logout action on popup yes button
         */
        enableLogoutAction: function() {
            try {
                var scopeObj = this;
                this.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
                this.view.CustomPopup.btnNo.text = kony.i18n.getLocalizedString("i18n.common.no");
                this.view.CustomPopup.btnYes.text = kony.i18n.getLocalizedString("i18n.common.yes");
                this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString("i18n.common.no");
                this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString("i18n.common.yes");
                this.view.CustomPopup.btnYes.onClick = function() {
                    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                    context = {
                        action: "Logout"
                    };
                    authModule.presentationController.doLogout(context);
                    scopeObj.view.flxLogout.left = "-100%";
                };
            } catch (error) {}
        },

        /**
         * Method to get action for specific account
         * @param {Collection} Actions List of Actions
         * @param {String} actionName Name of action
         * @param {JSON} account Account for which action is required
         * @returns {Object} matched action for the account from liat of actions
         */
        getAction: function(Actions, actionName, account) {
            var actionItem, matchedAction;
            for (var i = 0; i < Actions.length; i++) {
                actionItem = Actions[i];
                if (actionItem.actionName === actionName) {
                    matchedAction = {
                        actionName: actionItem.actionName,
                        displayName: actionItem.displayName,
                        action: actionItem.action.bind(null, account)
                    };
                    break;
                }
            }
            if (!matchedAction) {
                CommonUtilities.ErrorHandler.onError("Action :" + actionName + " is not found, please validate with Contextial actions list.");
                return false;
            }
            return matchedAction;
        },

        /**
         * Method to get quick action view model
         * @param {JSON} account Account for which quick actions are required
         * @param {Collection} actions List of actions
         * @returns {Object} actions viewModel
         */
        getQuickActionsViewModel: function(account, actions) {
            var scopeObj = this;
            var finalActionsViewModel = [];
            if (account.accountType) {
                if (actions.length) {
                    var validActions = actions.filter(function(action) {
                        return scopeObj.loadAccountModule().presentationController.isValidAction(action, account);
                    });
                    var onCancel = function() {
                        scopeObj.loadAccountModule().presentationController.presentAccountsLanding();
                    };
                    if (this.isFavourite(account) === true) {
                        validActions.push("Remove as Favourite");
                    } else if (this.isFavourite(account) === false) {
                        validActions.push("Set as Favourite");
                    }
                    finalActionsViewModel = validActions.map(function(action) { //get action object.
                        var quickActions = scopeObj.getQuickActions({
                            onCancel: onCancel,
                            tranferMoneyDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.makeTransferFrom"),
                            payABillDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payABillFrom"),
                            sendMoneyDisplayName: kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                            accountAlertsDisplayName: kony.i18n.getLocalizedString("i18n.Alerts.AccountAlertSettings"),
                            accountNumber: account.Account_id || account.accountID
                        });
                        return scopeObj.getAction(quickActions, action, account);
                    });
                }
            }
            return finalActionsViewModel
        },

        /**
         * Method to show actions for accounts on fetching quick action for that specific account
         * @param {JSON} account account whose quick action needs to be fetched
         * @param {Object} actions List of actions
         */
        onFetchQuickActions: function(account, actions) {
            var scopeObj = this;
            var quickActionsViewModel = scopeObj.getQuickActionsViewModel(account, actions)
            var toQuickActionSegmentModel = function(quickAction) {
                return {
                    "lblUsers": {
                        "text": quickAction.displayName,
                        "toolTip": quickAction.displayName,
                        "accessibilityconfig": {
                            "a11yLabel": quickAction.displayName
                        }
                    },
                    "lblSeparator": "lblSeparator",
                    "onRowClick": quickAction.action
                };
            };
            this.view.accountListMenu.segAccountListActions.setData(quickActionsViewModel.map(toQuickActionSegmentModel));
            this.view.accountListMenu.imgToolTip.setFocus(true);
            this.view.accountList.forceLayout();
            if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
                scopeObj.view.accountListMenu.left = "";
                scopeObj.view.accountListMenu.right = "55dp";
            } else if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                scopeObj.view.accountListMenu.left = "";
                scopeObj.view.accountListMenu.right = "75dp";
            } else {
                scopeObj.view.accountListMenu.left = "590dp";
                scopeObj.view.accountListMenu.right = "";
            }
            this.AdjustScreen();
            this.view.accountListMenu.segAccountListActions.setData(quickActionsViewModel.map(toQuickActionSegmentModel));
            this.view.accountListMenu.imgToolTip.setFocus(true);
            this.view.accountList.forceLayout();
            this.AdjustScreen();
        },

        /**
         * Method to open quick actions
         * @param {JSON} account account whose quick action needs to be seen
         */
        openQuickActions: function(account) {
            var scopeObj = this;
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            //Quick actions Configuration.
            var quickActionsConfig;
            if (account.isExternalAccount) {
                quickActionsConfig = OLBConstants.CONFIG.EXTERNAL_ACCOUNT_QUICK_ACTIONS;
                if (quickActionsConfig) {
                    scopeObj.onFetchQuickActions(account, quickActionsConfig);
                }
            } else {
                quickActionsConfig = OLBConstants.CONFIG.ACCOUNTS_QUICK_ACTIONS;
                if (quickActionsConfig[account.accountType]) {
                    scopeObj.onFetchQuickActions(account, quickActionsConfig[account.accountType]);
                }
            }
        },

        getListOfAccountTypesInGivenAccountList: function(accountList) {
            var accountTypes = [];
            accountList.map(function(account) {
                return account.type;
            }).forEach(function(type) {
                if (accountTypes.indexOf(type) < 0) {
                    accountTypes.push(type);
                }
            });

            return accountTypes;
        },

        /**
         * Method to update accounts list
         * @param {Collection} accounts List of accounts
         */
        updateAccountList: function(accounts) {
            var scopeObj = this;
            accounts = (kony.sdk.isNullOrUndefined(accounts) || accounts.constructor !== Array) ? [] : accounts;
            this.view.customheader.customhamburger.activateMenu("ACCOUNTS", "My Accounts");
            this.view.flxActions.isVisible = false;
            this.view.flxWelcomeAndActions.setVisibility(false);
            this.view.accountList.setVisibility(true);
            this.view.AddExternalAccounts.setVisibility(false);
            var favAccounts = accounts.filter(this.isFavourite);
            this.enableOrDisableTogglingBasedOnNumberOfFavoriteAccounts(favAccounts);
            if (!scopeObj.currentFilter) {
                if (favAccounts.length === 0) {
                    scopeObj.currentFilter = kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts");
                } else {
                    scopeObj.currentFilter = kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts");
                }

            } else {
                var filteredAccounts = scopeObj.filterAccounts(accounts, scopeObj.currentFilter);
                if (filteredAccounts.length === 0) {
                    scopeObj.currentFilter = kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts");
                }
            }
            this.view.accountList.btnShowAllAccounts.text = scopeObj.currentFilter;
            this.view.accountList.btnShowAllAccounts.toolTip = scopeObj.currentFilter;
            var banks = this.setBanksAccountSegment(accounts, scopeObj.currentFilter, favAccounts);
            this.view.FavouriteAccountTypes.segAccountTypes.setData(banks);
            this.view.accountList.segAccounts.widdgetDataMap = {
                "btn": "btn",
                "flxAccountListItem": "flxAccountListItem",
                "flxAccountListItemWrapper": "flxAccountListItemWrapper",
                "flxAccountName": "flxAccountName",
                "flxAccountNameWrapper": "flxAccountNameWrapper",
                "flxAvailableBalance": "flxAvailableBalance",
                "flxAvailableBalanceWrapper": "flxAvailableBalanceWrapper",
                "flxBankName": "flxBankName",
                "flxContent": "flxContent",
                //"flxFavourite": "flxFavourite",
                "flxIdentifier": "flxIdentifier",
                "flxMenu": "flxMenu",
                "imgBankName": "imgBankName",
                //"imgFavourite": "imgFavourite",
                "imgIdentifier": "imgIdentifier",
                "imgInfo": "imgInfo",
                "imgMenu": "imgMenu",
                //"imgStarIcon": "imgStarIcon",
                "imgThreeDotIcon": "imgThreeDotIcon",
                "imgWarning": "imgWarning",
                "lblAccountName": "lblAccountName",
                "lblAccountNumber": "lblAccountNumber",
                "lblAvailableBalanceTitle": "lblAvailableBalanceTitle",
                "lblAvailableBalanceValue": "lblAvailableBalanceValue"
            };
            this.view.accountList.segAccounts.setData([]);
            this.view.FavouriteAccountTypes.segAccountTypes.onRowClick = this.onSegAccountTypeRowClick.bind(this, accounts, banks);
            if (scopeObj.currentFilter === kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")) {
                //review here
                this.view.accountList.segAccounts.setData(this.prepareAccountsViewBasedOnAccountType(this.createAccountSegmentsModel(accounts)));

                this.view.accountList.forceLayout();
                this.AdjustScreen();
            } else if (scopeObj.currentFilter === kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts")) {
                this.view.accountList.segAccounts.setData(this.prepareAccountsViewBasedOnAccountType(this.createAccountSegmentsModel(accounts.filter(this.isFavourite))));
                this.view.accountList.forceLayout();
                this.AdjustScreen();
            }
            this.view.accountList.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },

        enableOrDisableTogglingBasedOnNumberOfFavoriteAccounts: function(favoriteAccounts) {
            if (favoriteAccounts.length > 0) {
                this.view.accountList.flxAccountsRightContainer.onTouchStart = this.flxAccountsRightContainerOnTouchStart.bind(this);
                this.view.accountList.flxAccountsRightContainer.onClick = this.showFilterPopup.bind(this);
                this.view.accountList.lblImgDropdown.setVisibility(true);
            } else {
                this.view.accountList.lblImgDropdown.setVisibility(false);
                this.view.accountList.flxAccountsRightContainer.onTouchStart = function() {};
                this.view.accountList.flxAccountsRightContainer.onClick = function() {};
            }
        },

        flxAccountsRightContainerOnTouchStart: function() {
            var scopeObj = this;
            if (scopeObj.view.FavouriteAccountTypes.isVisible) {
                scopeObj.view.FavouriteAccountTypes.origin = true;
                if (kony.application.getCurrentBreakpoint() === 640 || kony.application.getCurrentBreakpoint() === 1024) {
                    scopeObj.view.accountList.lblImgDropdown.text = "O";
                    scopeObj.view.FavouriteAccountTypes.isVisible = false;
                    scopeObj.AdjustScreen();
                }
            }
        },

        /**
         * Method to update alert icon for unread messages
         * @param {Number} unreadCount Unread count of messages
         */
        updateAlertIcon: function(unreadCount) {
            applicationManager.getConfigurationManager().setUnreadMessageCount(unreadCount);
            this.view.customheader.headermenu.updateAlertIcon();

            FormControllerUtility.hideProgressBar(this.view);
        },

        /**
         * Sets data regarding added external accounts in the segment
         * @param {Collection} accounts List of accounts
         */
        setAddedAccountsData: function(accounts) {
            var self = this;
            var widgetDataMap = {
                "lblCheckBox": "lblCheckBox",
                'lblAccountName': "AccountName",
                'lblAccountNumber': "AccountType",
                'lblAvailableBalanceValue': "AvailableBalanceWithCurrency",
                'lblAvailableBalanceTitle': "AvailableBalanceLabel",
                'lblSeperator': "separator",
                'flxcheckbox': "flxcheckbox"
            };
            this.view.AddExternalAccounts.Acknowledgment.segSelectedAccounts.widgetDataMap = widgetDataMap;
            var data = accounts.map(function(account) {
                return {
                    "AccountName": account.AccountName,
                    "AccountType": account.AccountType,
                    "AvailableBalanceWithCurrency": account.AvailableBalanceWithCurrency,
                    "AvailableBalanceLabel": account.AccountType ? self.accountTypeConfig[account.AccountType].balanceTitle : self.accountTypeConfig["Default"].balanceTitle,
                    "separator": account.separator,
                    "lblCheckBox": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                    "AvailableBalance": account.AvailableBalance,
                    "CurrencyCode": account.CurrencyCode,
                    "AccountNumber": account.Number,
                    "BankId": account.bank_id,
                    "TypeId": account.Type_id,
                    "AccountHolder": account.AccountHolder,
                    "UserName": account.userName
                };
            });
            this.view.AddExternalAccounts.Acknowledgment.segSelectedAccounts.setData(data);
        },

        /**
         * Method to navigate to the acknolodgement
         */
        NavigateToAcknowledgment: function() {
            this.HideAll();
            this.NavigateToAddExternalAccounts();
            this.HideAllExternalAcc();
            this.view.AddExternalAccounts.flxAcknowledgment.isVisible = true;
            this.view.AddExternalAccounts.Acknowledgment.btnAddMoreAccounts.text = kony.i18n.getLocalizedString("i18n.AccountsAggregation.AddMoreAccounts");
            this.view.AddExternalAccounts.Acknowledgment.btnAccountSummary.text = kony.i18n.getLocalizedString("i18n.AccountsAggregation.AccountSummary");
            this.view.AddExternalAccounts.Acknowledgment.btnAccountSummary.toolTip = kony.i18n.getLocalizedString("i18n.AccountsAggregation.AccountSummary");
            this.view.AddExternalAccounts.Acknowledgment.btnAddMoreAccounts.toolTip = kony.i18n.getLocalizedString("i18n.AccountsAggregation.AddMoreAccounts");
        },

        /**
         * Show Overdraft Notifaction Message
         * @param {String} isOverdraft Param to check if a transaction is over drafted
         * @param {String} overDraftMessage overDraft message (optional)
         */
        setOverdraftNotification: function(isOverdraft, overDraftMessage) {
            var scopeObj = this;
            var overdraftUI = scopeObj.view.flxOverdraftWarning;
            if (isOverdraft && !overdraftUI.isVisible) {
                scopeObj.view.lblOverdraftWarning.text = overDraftMessage || kony.i18n.getLocalizedString("i18n.AccountsLanding.OverDraftWarning");
                scopeObj.view.imgCloseWarning.onTouchEnd = function() {
                    scopeObj.setOverdraftNotification(false);
                };
                overdraftUI.setVisibility(true);
                scopeObj.AdjustScreen();
            } else if (!isOverdraft && overdraftUI.isVisible) {
                overdraftUI.setVisibility(false);
                var acctop = scopeObj.view.accountListMenu.frame.y + 5;
                scopeObj.view.accountListMenu.top = acctop + ViewConstants.POSITIONAL_VALUES.DP;
                scopeObj.AdjustScreen();
            }
        },

        updateMessagesWidget: function(messages) {
            var self = this;
            this.view.myMessages.lblHeader.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages");
            if (messages.length === 0) {
                this.view.myMessages.flxMyMessagesWrapper.setVisibility(false);
                this.view.myMessages.flxMyMeesagesWarningWrapper.setVisibility(true);
                this.view.myMessages.flxNoMessages.setVisibility(true);
                this.view.myMessages.rtxNoMessage.text = kony.i18n.getLocalizedString("i18n.AccountsLanding.NoMessagesAtTheMoment");
            } else {
                this.view.myMessages.flxMyMessagesWrapper.setVisibility(true);
                this.view.myMessages.flxMyMeesagesWarningWrapper.setVisibility(false);
                messages = messages.map(function(message) {
                    return {
                        "lblDate": CommonUtilities.getDateAndTime(message.requestCreatedDate, applicationManager.getConfigurationManager().getConfigurationValue("frontendDateFormat")),
                        "rtxDescription": message.requestsubject,
                        "lblSeparator": "lblSeparator",
                        "onClick": self.loadAccountModule().presentationController.showSelectedMessage.bind(self.loadAccountModule().presentationController, message)
                    };
                });
                this.view.myMessages.segMessages.setData(messages);
            }
        },

        /**
         * Method to Hide the PFM Widget on DashBoard Based on the configuration flag. "isPFMWidgetEnabled"
         */
        disablePFMWidget: function() {
            this.view.mySpending.setVisibility(false);
            this.view.upcomingTransactions.left = "6" + ViewConstants.POSITIONAL_VALUES.PERCENTAGE;
            this.view.upcomingTransactions.width = "43.44" + ViewConstants.POSITIONAL_VALUES.PERCENTAGE;
            this.view.myMessages.width = "43.44" + ViewConstants.POSITIONAL_VALUES.PERCENTAGE;
        },

        /**
         * Format data for PFM donut chart
         * @param {Object} monthlyData Monthly data
         * @returns {Object} Formatted Donut chart data
         */
        formatPFMDonutChartData: function(monthlyData) {
            var monthlyDonutChartData = {};

            function addRequireDonutChartFields(month) {
                month.label = month.categoryName;
                month.Value = Number(month.cashSpent);
                month.colorCode = ViewConstants.PFM_CATEGORIES_COLORS[month.categoryName];
                return month;
            }
            var pfmData = monthlyData.map(addRequireDonutChartFields);
            if (monthlyData.length !== 0) {
                monthlyDonutChartData.totalCashSpent = CommonUtilities.formatCurrencyWithCommas(monthlyData[0].totalCashSpent);
            }
            monthlyDonutChartData.pfmChartData = pfmData;
            return monthlyDonutChartData;
        },

        /**
         * Method to get PFM mo0nthly data*
         * @param {Object} PFMData PFM data from backend
         */
        getPFMMonthlyDonutChart: function(PFMData) {
            var donutData = this.formatPFMDonutChartData(PFMData);
            this.view.mySpending.flxMySpendingWrapper.isVisible = true;
            var monthlyChartData = donutData.pfmChartData,
                totalCashSpent = donutData.totalCashSpent;
            if (monthlyChartData.length !== 0) {
                this.view.flxSecondaryDetails.mySpending.flxMySpendingWrapper.flxMySpending.setVisibility(true);
                this.view.mySpending.lblOverallSpendingAmount.isVisible = true;
                this.view.flxSecondaryDetails.mySpending.flxMySpendingWrapper.flxMySpending.donutChart1.chartData = monthlyChartData;
                this.view.flxSecondaryDetails.mySpending.flxMySpendingWrapper.lblOverallSpendingAmount.text = totalCashSpent;
                this.view.flxSecondaryDetails.mySpending.flxMySpendingWrapperdataUnavailable.setVisibility(false);
            } else {
                this.view.flxSecondaryDetails.mySpending.flxMySpendingWrapper.setVisibility(false);
                this.view.flxSecondaryDetails.mySpending.flxMySpendingWrapperdataUnavailable.setVisibility(true);
            }
            this.AdjustScreen()
        },

        /**
         * onBreakpointChange : Handles ui changes on .
         * @member of {frmBBAccountsLandingController}
         * @param {integer} width - current browser width
         * @return {}
         * @throws {}
         */
        onBreakpointChange: function(width) {
            orientationHandler.onOrientationChange(this.onBreakpointChange);
            kony.print('on breakpoint change');
            var responsiveFonts = new ResponsiveFonts();
            this.setupFormOnTouchEnd(width);
            this.view.customheader.onBreakpointChangeComponent(width);
            var scope = this;
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            if (width === 640 || orientationHandler.isMobile) {
                this.nullifyPopupOnTouchStart();
                var data = this.view.accountList.segAccounts.data;
                if (data === undefined) return;
                data.map(function(e) {
                    e.template = "flxAccountListItemMobile";
                });
                this.view.accountList.segAccounts.setData(data);
                this.view.FavouriteAccountTypes.zIndex = 10;
                this.view.customheader.lblHeaderMobile.text = kony.i18n.getLocalizedString('i18n.hamburger.myaccounts');
                responsiveFonts.setMobileFonts();
                this.view.onTouchEnd = function() {}
            } else {
                if (width === 1366) {
                    this.view.flxBannerContainerDesktop.height = ((0.286 * kony.os.deviceInfo().screenWidth) / 2.23) + "dp";
                    this.view.flxActions.isVisible = false;
                }
                var data = this.view.accountList.segAccounts.data;
                if (data === undefined) return;
                data.map(function(e) {
                    e.template = "flxAccountListItem";
                });
                this.view.accountList.segAccounts.setData(data);
                this.view.customheader.lblHeaderMobile.text = "";
                responsiveFonts.setDesktopFonts();

            }
            this.AdjustScreen();
            this.loadAccountModule().presentationController.getAccountDashboardCampaignsOnBreakpointChange();
        },


        setupFormOnTouchEnd: function(width) {
            if (width == 640) {
                this.view.onTouchEnd = function() {}
                this.nullifyPopupOnTouchStart();
            } else {
                if (width == 1024) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else {
                    this.view.onTouchEnd = function() {
                        hidePopups();
                    }
                }
                var userAgent = kony.os.deviceInfo().userAgent;
                if (userAgent.indexOf("iPad") != -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                }
            }
        },

        nullifyPopupOnTouchStart: function() {
            this.view.accountList.flxAccountsRightContainer.onTouchStart = null;
        },

        /*
         * Method gets triggered on click of external account segment row
         */
        onClickOfSegmentRow: function() {
            if (this.selectedRowIndices.length > 0) {
                this.isAtLeastOneAccountSelected = true;
            } else {
                this.isAtLeastOneAccountSelected = false;
            }
            this.enableSaveButton();
        },

        prepareAccountsViewBasedOnAccountType: function(accounts) {
            var scope = this;
            var viewModel = [];
            var savingAccounts = [];
            var totalSavingsSum = 0;
            var checkingAccounts = [];
            var totalCheckingSum = 0;
            var creditAccounts = [];
            var totalCreditSum = 0;
            var depositAccounts = [];
            var totalDepositSum = 0;
            var mortgageAccounts = [];
            var totalMortgageSum = 0;
            var loanAccounts = [];
            var totalLoanSum = 0;
            var others = [];
            var totalOthersSum = 0;

            accounts.forEach(function(account) {
                switch (account.accountType) {
                    case 'Savings':
                        savingAccounts.push(account);
                        totalSavingsSum += parseFloat(account.balance);
                        break;
                    case 'Checking':
                        checkingAccounts.push(account);
                        totalCheckingSum += parseFloat(account.balance);
                        break;
                    case 'CreditCard':
                        creditAccounts.push(account);
                        totalCreditSum += parseFloat(account.balance);
                        break;
                    case 'Deposit':
                        depositAccounts.push(account);
                        totalDepositSum += parseFloat(account.balance);
                        break;
                    case 'Mortgage':
                        mortgageAccounts.push(account);
                        totalMortgageSum += parseFloat(account.balance);
                        break;
                    case 'Loan':
                        loanAccounts.push(account);
                        totalLoanSum += parseFloat(account.balance);
                        break;
                    default:
                        others.push(account);
                        totalOthersSum += parseFloat(account.balance);
                        break;
                }
            });

            if (savingAccounts.length > 0) {
                var savingAccountsGroup = [];
                var savingsHeader = {};
                //make header
                savingsHeader.lblAccountItemHeader = kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.SavingsAccounts");
                savingsHeader.lblTotalSumTitle = kony.i18n.getLocalizedString("i18n.CheckImages.Total");
                savingsHeader.lblTotalSumValue = CommonUtilities.formatCurrencyWithCommas(totalSavingsSum.toFixed(2).toString(), false, savingAccounts.currencyCode);

                savingAccountsGroup.push(savingsHeader);
                savingAccountsGroup.push(savingAccounts);
                viewModel.push(savingAccountsGroup);
            }
            if (checkingAccounts.length > 0) {
                var checkingAccountsGroup = [];
                var checkingHeader = {};
                //make header
                checkingHeader.lblAccountItemHeader = kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.CheckingAccounts");
                checkingHeader.lblTotalSumTitle = kony.i18n.getLocalizedString("i18n.CheckImages.Total");
                checkingHeader.lblTotalSumValue = CommonUtilities.formatCurrencyWithCommas(totalCheckingSum.toFixed(2).toString(), false, checkingAccounts.currencyCode);

                checkingAccountsGroup.push(checkingHeader);
                checkingAccountsGroup.push(checkingAccounts);
                viewModel.push(checkingAccountsGroup);
            }
            if (creditAccounts.length > 0) {
                var creditAccountsGroup = [];
                var creditHeader = {};
                //make header
                creditHeader.lblAccountItemHeader = kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.CreditCardAccounts");
                creditHeader.lblTotalSumTitle = kony.i18n.getLocalizedString("i18n.CheckImages.Total");
                creditHeader.lblTotalSumValue = CommonUtilities.formatCurrencyWithCommas(totalCreditSum.toFixed(2).toString(), false, creditAccounts.currencyCode);
                creditAccountsGroup.push(creditHeader);
                creditAccountsGroup.push(creditAccounts);
                viewModel.push(creditAccountsGroup);
            }
            if (depositAccounts.length > 0) {
                var depositAccountsGroup = [];
                var depositHeader = {};
                //make header
                depositHeader.lblAccountItemHeader = kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.DepositAccounts");
                depositHeader.lblTotalSumTitle = kony.i18n.getLocalizedString("i18n.CheckImages.Total");
                depositHeader.lblTotalSumValue = CommonUtilities.formatCurrencyWithCommas(totalDepositSum.toFixed(2).toString(), false, depositAccounts.currencyCode);
                depositAccountsGroup.push(depositHeader);
                depositAccountsGroup.push(depositAccounts);
                viewModel.push(depositAccountsGroup);
            }
            if (mortgageAccounts.length > 0) {
                var mortgageAccountsGroup = [];
                var mortgageHeader = {};
                //make header
                mortgageHeader.lblAccountItemHeader = kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.MortgageAccounts");
                mortgageHeader.lblTotalSumTitle = kony.i18n.getLocalizedString("i18n.CheckImages.Total");
                mortgageHeader.lblTotalSumValue = CommonUtilities.formatCurrencyWithCommas(totalMortgageSum.toFixed(2).toString(), false, mortgageAccounts.currencyCode);
                mortgageAccountsGroup.push(mortgageHeader);
                mortgageAccountsGroup.push(mortgageAccounts);
                viewModel.push(mortgageAccountsGroup);
            }
            if (loanAccounts.length > 0) {
                var loanAccountsGroup = [];
                var loanHeader = {};
                //make header
                loanHeader.lblAccountItemHeader = kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.LoanAccounts");
                loanHeader.lblTotalSumTitle = kony.i18n.getLocalizedString("i18n.CheckImages.Total");
                loanHeader.lblTotalSumValue = CommonUtilities.formatCurrencyWithCommas(totalLoanSum.toFixed(2).toString(), false, loanAccounts.currencyCode);
                loanAccountsGroup.push(loanHeader);
                loanAccountsGroup.push(loanAccounts);
                viewModel.push(loanAccountsGroup);
            }
            if (others.length > 0) {
                var otherAccountsGroup = [];
                var otherHeader = {};
                //make header
                otherHeader.lblAccountItemHeader = kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.OtherAccounts");
                otherHeader.lblTotalSumTitle = kony.i18n.getLocalizedString("i18n.CheckImages.Total");
                otherHeader.lblTotalSumValue = CommonUtilities.formatCurrencyWithCommas(totalOthersSum.toFixed(2).toString(), false, others.currencyCode);
                otherAccountsGroup.push(otherHeader);
                otherAccountsGroup.push(others);
                viewModel.push(otherAccountsGroup);
            }
            kony.print(JSON.stringify(viewModel));
            return viewModel;
        },

        initializeReceivablePayableChart: function() {
            var data = [];
            var options = {
                legend: {
                    position: "none"
                },
                bar: {
                    groupWidth: "25%"
                }
            };
            var barChart = new kony.ui.CustomWidget({
                "id": "barChartForCashFlow",
                "isVisible": true,
                "left": "0" + ViewConstants.POSITIONAL_VALUES.DP,
                "top": "0" + ViewConstants.POSITIONAL_VALUES.DP,
                "width": "100%",
                "height": "100%",
                "zIndex": 1000000
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "BarChart",
                "chartData": data,
                "chartProperties": options,
                "OnClickOfBar": function() {}
            });
            this.view.flxMainChartContainer.flxChartContainer.add(barChart);

        },

        /**
         * Method on Click Of FlxTermsAndconditions
         */
        onClickOfFlxTermsAndconditions: function() {
            this.toggleBetweenTermsAcceptedAndUnaccepted();
            this.enableSaveButton();
        },

        /**
         * Method to get data of external accounts
         * @param {Object} data data
         * @returns {Object} externalAccountsData
         */
        getExternalAccountsData: function(data) {
            var externalAccountsData = data;
            for (var i in externalAccountsData) {
                externalAccountsData[i].separator = ViewConstants.FONT_ICONS.LABEL_IDENTIFIER;
                externalAccountsData[i].isChecked = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
            }
            return externalAccountsData;
        },
        /**
         * Sets data to segment regarding external bank accounts
         * @param {Collection} accounts List of accounts
         */
        setExternalAccountsData: function(accounts) {
            var self = this;
            var widgetDataMap = {
                "lblCheckBox": "lblCheckBox",
                'lblAccountName': "MaskedAccountName",
                'lblAccountNumber': "AccountType",
                'lblAvailableBalanceValue': "AvailableBalanceWithCurrency",
                'lblAvailableBalanceTitle': "AvailableBalanceLabel",
                'lblSeperator': "separator",
                'flxcheckbox': "flxcheckbox"
            };
            this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.widgetDataMap = widgetDataMap;
            var data = accounts.map(function(account, index) {
                return {
                    "MaskedAccountName": formatAccountName(account.AccountName, account.Number),
                    "AccountName": account.AccountName,
                    "AccountType": account.AccountType,
                    "AvailableBalanceWithCurrency": account.AvailableBalanceWithCurrency,
                    "AvailableBalanceLabel": account.AccountType ? self.accountTypeConfig[account.AccountType].balanceTitle : self.accountTypeConfig["Default"].balanceTitle,
                    "separator": account.separator,
                    "lblCheckBox": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                    "flxcheckbox": {
                        "onClick": self.toggleAccountSelectionCheckbox.bind(this, index)
                    },
                    "AvailableBalance": account.AvailableBalance,
                    "CurrencyCode": account.CurrencyCode,
                    "AccountNumber": account.Number,
                    "BankId": account.bank_id,
                    "TypeId": account.Type_id,
                    "AccountHolder": account.AccountHolder,
                    "UserName": account.UserName
                };
            });
            this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.setData(data);
            this.view.forceLayout();
            var selectedIndices = [];
            for (var i = 0; i < data.length; i++) {
                selectedIndices.push(i);
            }
            this.selectedRowIndices = selectedIndices;

            function formatAccountName(accountName, accountNumber) {
                var stringAccNum = String(accountNumber);
                var stringAccName = String(accountName);
                var isLast4Digits = function(index) {
                    return index > (stringAccNum.length - 5);
                };
                var hashedAccountNumber = stringAccNum.split('').map(function(c, i) {
                    return isLast4Digits(i) ? c : 'X';
                }).join('').slice(-5);
                return stringAccName + "-" + hashedAccountNumber;
            }
        },

        /**
         * Method to hide all flex
         */
        HideAll: function() {
            this.view.flxDowntimeWarning.isVisible = false;
            this.view.flxWelcomeAndActions.isVisible = false;
            this.view.flxAccountListAndBanner.isVisible = false;
            this.view.flxSecondaryDetails.isVisible = false;
            this.view.accountListMenu.isVisible = false;
            this.view.AddExternalAccounts.flxSelectBankOrVendor.setVisibility(false);
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginSelectAccountsMain.setVisibility(false);
            this.AdjustScreen();
        },

        /**
         * Method to add external account
         */
        NavigateToAddExternalAccounts: function() {
            this.view.flxAccountListAndBanner.isVisible = true;
            this.view.accountList.isVisible = false;
            this.view.AddExternalAccounts.isVisible = true;
            this.view.flxActions.isVisible = true;
            if (kony.application.getCurrentBreakpoint() === 640) {
                this.view.customheader.lblHeaderMobile.text = kony.i18n.getLocalizedString('i18n.AccountsAggregation.AddExternalBankAccount');
            }
        },

        /**
         * Method to hide all external accounts
         */
        HideAllExternalAcc: function() {
            this.view.AddExternalAccounts.flxAcknowledgment.isVisible = false;
            this.view.AddExternalAccounts.flxLoginUsingSelectedBank.isVisible = false;
            this.view.AddExternalAccounts.flxSelectBankOrVendor.isVisible = false;
            this.AdjustScreen();
        },

        /**
         * Method to Navigate To External Bank Accounts List
         */
        NavigateToExternalBankAccountsList: function() {
            this.HideAll();
            this.NavigateToAddExternalAccounts();
            this.HideAllExternalAcc();
            this.view.AddExternalAccounts.flxLoginUsingSelectedBank.isVisible = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankMain.isVisible = false;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginSelectAccountsMain.isVisible = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.text = kony.i18n.getLocalizedString("i18n.AccountsAggregation.ExternalAccountsList.AddAccounts");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.toolTip = kony.i18n.getLocalizedString("i18n.AccountsAggregation.ExternalAccountsList.AddAccounts");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.forceLayout();
            this.AdjustScreen();
        },


        /**
         * Method to present External Accounts List
         * @param {Array} externalAccountsList External accounts list
         */
        presentExternalAccountsList: function(externalAccountsList) {
            this.isTermsAndConditionsAccepted = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.imgBank.src = this.ExternalLoginContextData.logo;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.lblUsernameValue.text = this.ExternalLoginContextData.username;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.lblBankValue.text = this.ExternalLoginContextData.bankName;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.onClick = this.addExternalAccounts;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxCheckbox.onClick = this.onClickOfFlxTermsAndconditions;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.onClick = this.presenter.showAccountsDashboard;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnTermsAndConditions.toolTip = kony.i18n.getLocalizedString("i18.AccountAggregation.TermsAndConditions");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankHeading.text = kony.i18n.getLocalizedString("i18n.AddExternalAccount.SelectYourAccounts");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.isVisible = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxIAgree.isVisible = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxAddAccountsError.isVisible = false;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.isVisible = true;

            this.toggleBetweenTermsAcceptedAndUnaccepted();
            var accounts = this.getExternalAccountsData(externalAccountsList);
            this.setExternalAccountsData(accounts);
            this.AdjustScreen();
            this.onClickOfSegmentRow();
            this.NavigateToExternalBankAccountsList();
            CommonUtilities.hideProgressBar(this.view);
        },

        /**
          * This function shows appropriate message to user in case all the accounts of a particular external bank is already added

         */
        onAllExternalAccountsAlreadyAdded: function() {
            this.view.AddExternalAccounts.LoginUsingSelectedBank.imgBank.src = this.ExternalLoginContextData.logo;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.lblUsernameValue.text = this.ExternalLoginContextData.username;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.lblBankValue.text = this.ExternalLoginContextData.bankName;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankHeading.text = kony.i18n.getLocalizedString("i18n.AddExternalAccount.SelectYourAccounts");

            this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.isVisible = false;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxIAgree.isVisible = false;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxAddAccountsError.isVisible = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.isVisible = false;

            this.AdjustScreen();
            this.NavigateToExternalBankAccountsList();
            CommonUtilities.hideProgressBar(this.view);
        },

        responsiveViews: {},

        initializeResponsiveViews: function() {
            this.responsiveViews["accountList"] = this.isViewVisible("accountList");
            this.responsiveViews["AddExternalAccounts"] = this.isViewVisible("AddExternalAccounts");
        },

        isViewVisible: function(container) {
            if (this.view[container].isVisible) {
                return true;
            } else {
                return false;
            }
        },

        /**
         * Method to toggle account checkbox
         * @param {Number} index index of selectd row
         */
        toggleAccountSelectionCheckbox: function(index) {
            var data = this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.data[index];
            if (data.lblCheckBox === ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                data.lblCheckBox = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                var indexOfCurrentRow = this.selectedRowIndices.indexOf(index);
                if (indexOfCurrentRow > -1) {
                    this.selectedRowIndices.splice(indexOfCurrentRow, 1);
                }
            } else {
                data.lblCheckBox = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                this.selectedRowIndices.push(index);
            }
            this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.setDataAt(data, index);
            this.onClickOfSegmentRow();
        },

        /*
         * Navigates to show confirmation for added external accounts
         * @param {Array} addedExternalAccounts External account list
         */
        presentExternalAccountsAddedConfirmation: function(addedExternalAccounts) {
            var selectedAccounts = [];
            for (var i = 0; i < this.selectedRowIndices.length; i++) {
                selectedAccounts.push(this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.data[this.selectedRowIndices[i]]);
            }
            this.setAddedAccountsData(selectedAccounts);
            this.NavigateToAcknowledgment();
            this.AdjustScreen();
            this.view.AddExternalAccounts.Acknowledgment.lblSuccessmsg.text = kony.i18n.getLocalizedString("i18n.AcountsAggregation.ExternalAccountAddition.ComfirmationMessage") +
                " " + this.ExternalLoginContextData.bankName + " " + kony.i18n.getLocalizedString("i18n.CheckImages.Bank");
            this.view.AddExternalAccounts.Acknowledgment.btnAccountSummary.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.showAccountsDashboard();
            }
            this.view.AddExternalAccounts.Acknowledgment.btnAddMoreAccounts.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.showExternalBankList();
            }
            CommonUtilities.hideProgressBar(this.view);
        },

        /**
         * Method to enable Or Disable External Login
         * @param {String} username username
         * @param {String} password password
         * @returns {Boolean} true/false
         */
        enableOrDisableExternalLogin: function(username, password) {
            if (username && String(username).trim() !== "" && password && String(password).trim() !== "") {
                this.enableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
                return true;
            } else {
                this.disableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
                return false;
            }
        },

        /**
         * Method that logins to external bank
         */
        onClickOfExternalBankLogin: function() {
            if (!(this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text,
                    this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text))) {
                return;
            }
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankError.isVisible = false;
            CommonUtilities.showProgressBar(this.view);
            this.ExternalLoginContextData.username = this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text;
            this.ExternalLoginContextData.password = this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text;
            var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
            accountModule.presentationController.authenticateUserInExternalBank(this.ExternalLoginContextData.username,
                this.ExternalLoginContextData.password,
                this.ExternalLoginContextData.identityProvider);
        },

        /**
         * Method to navigate to login screen
         */
        NavigateToLogin: function() {
            var bankHeader = kony.i18n.getLocalizedString("i18n.AccountsAggregation.LoginUsingSelectedBank");
            this.HideAll();
            this.NavigateToAddExternalAccounts();
            this.HideAllExternalAcc();
            this.view.AddExternalAccounts.flxLoginUsingSelectedBank.isVisible = true;
            this.view.AddExternalAccounts.setVisibility(true);
            this.view.AddExternalAccounts.flxSelectBankOrVendor.setVisibility(false);
            this.view.AddExternalAccounts.flxLoginUsingSelectedBank.setVisibility(true);
            this.view.AddExternalAccounts.LoginUsingSelectedBank.setVisibility(true);
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankError.isVisible = false;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBank.setVisibility(true);
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankMain.setVisibility(true);
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankHeading.text = bankHeader;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.text = kony.i18n.getLocalizedString("i18n.common.login");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.toolTip = this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.text;
            this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text,
                this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
        },

        /**
         * Method to show External Bank Login
         * @param {Object} data External bank data
         */
        showExternalBankLogin: function(data) {
            this["ExternalLoginContextData"] = data;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text = "";
            this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text = "";
            this.view.flxActions.isVisible = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.secureTextEntry = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.imgflxLoginUsingSelectedBank.src = data.logo;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankMessage.text = kony.i18n.getLocalizedString("i18n.common.Welcometo") + data.bankName;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.toolTip = kony.i18n.getLocalizedString("i18n.common.login");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.isVisible = true;
            this.NavigateToLogin();
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.onClick = this.onClickOfExternalBankLogin.bind(this);
            this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.showAccountsDashboard();
            };
            this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.onDone = this.onClickOfExternalBankLogin.bind(this);
            this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.onDone = this.onClickOfExternalBankLogin.bind(this);
            this.AdjustScreen();
        },

        /**
         * Method to disable button
         * @param {String} button
         */
        disableButton: function(button) {
            button.setEnabled(false);
            button.skin = ViewConstants.SKINS.LOCATE_BTNSHARESEND;
            button.hoverSkin = ViewConstants.SKINS.LOCATE_BTNSHARESEND;
            button.focusSkin = ViewConstants.SKINS.LOCATE_BTNSHARESEND;
        },

        /**
         * Method to enable button
         * @param {String} button
         */
        enableButton: function(button) {
            button.setEnabled(true);
            button.skin = ViewConstants.SKINS.PFM_BTN_ENABLE;
            button.hoverSkin = ViewConstants.SKINS.PFM_BTN_ENABLE_HOVER;
            button.focusSkin = ViewConstants.SKINS.PFM_BTN_ENABLE_FOCUS;
        },

        /**
         * Method on click of externam bank account
         */
        onSelectionOfExternalBank: function() {
            this.view.AddExternalAccounts.SelectBankOrVendor.tbxName.text = this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.selectedRowItems[0].BankName;
            this.view.AddExternalAccounts.SelectBankOrVendor.flxBankList.isVisible = false;
            this.enableButton(this.view.AddExternalAccounts.SelectBankOrVendor.btnProceed);
            this.AdjustScreen();
        },

        /**
         * Method that gets triggered on click of Proceed
         */
        onClickOfProceedOnExternalBankList: function() {
            var selectedItem = this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.selectedRowItems[0];
            this.showExternalBankLogin({
                "identityProvider": selectedItem.IdentityProvider,
                "logo": selectedItem.logo,
                "isOauth2": selectedItem.Oauth2,
                "bankName": selectedItem.BankName,
                "bankId": selectedItem.id
            });
        },

        /**
         * Method on text change of external Bank Search
         */
        onTextChangeOfExternalBankSearch: function() {
            var searchText = this.view.AddExternalAccounts.SelectBankOrVendor.tbxName.text;
            if (searchText && String(searchText).trim() !== "") {
                searchText = String(searchText).trim().toLowerCase();
                var tempArr = [];
                for (var i in this.externalBankSearchList) {
                    if (String(this.externalBankSearchList[i].BankName).trim().toLowerCase().search(searchText) >= 0) {
                        tempArr.push(this.externalBankSearchList[i]);
                    }
                }
                if (tempArr.length > 0) {
                    this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.setData(tempArr);
                    this.view.AddExternalAccounts.SelectBankOrVendor.flxBankList.isVisible = true;
                } else {
                    this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.setData([]);
                    this.view.AddExternalAccounts.SelectBankOrVendor.flxBankList.isVisible = false;
                }
            } else {
                this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.setData([]);
                this.view.AddExternalAccounts.SelectBankOrVendor.flxBankList.isVisible = false;
            }
            this.disableButton(this.view.AddExternalAccounts.SelectBankOrVendor.btnProceed);
            this.AdjustScreen();
        },

        /**
         * Method on click Of Reset ExternalBankList
         *
         */
        onClickOfResetOnExternalBankList: function() {
            this.view.AddExternalAccounts.SelectBankOrVendor.tbxName.text = "";
            this.onTextChangeOfExternalBankSearch();
        },

        /**
         * Method to show External Accounts
         * @param {Object} accounts data
         */
        showExternalBankList: function(accounts) {
            var data = accounts.map(function(x) {
                return {
                    text: x.BankName,
                    toolTip: x.BankName
                };
            });
            this.view.btnUpdateAccntPreferences.onClick = this.navigateToAccountPreferences;
            this.view.btnUpdateAccntPreferences.toolTip = kony.i18n.getLocalizedString("i18n.AccountsAggregation.UpdateAccountPreferences");
            this.setOverdraftNotification(false);
            this.setOutageNotification(false);
            this.setServiceError(false);
            for (var i in data) {
                delete data[i].Scheme;
                delete data[i].Address;
                delete data[i].Description;
            }
            this.view.flxActions.isVisible = true;
            this.view.AddExternalAccounts.setVisibility(true);
            this.view.AddExternalAccounts.SelectBankOrVendor.setVisibility(true);
            this.view.AddExternalAccounts.flxSelectBankOrVendor.setVisibility(true);
            if (kony.application.getCurrentBreakpoint() === 640) {
                this.view.customheader.lblHeaderMobile.text = kony.i18n.getLocalizedString('i18n.AccountsAggregation.AddExternalBankAccount');
            }
            this.view.accountList.setVisibility(false);
            this.view.flxWelcomeAndActions.setVisibility(false);
            this.view.AddExternalAccounts.flxLoginUsingSelectedBank.setVisibility(false);
            this.view.AddExternalAccounts.flxAcknowledgment.setVisibility(false);
            this.view.flxSecondaryDetails.setVisibility(false);
            this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.widgetDataMap = {
                "lblUsers": "BankName",
                "imgBank": "logo"
            };
            this.externalBankSearchList = accounts; //setting external bank for reference
            this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.setData([]);
            this.view.AddExternalAccounts.SelectBankOrVendor.btnReset.toolTip = kony.i18n.getLocalizedString("i18n.billpay.reset");
            this.view.AddExternalAccounts.SelectBankOrVendor.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
            this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.onRowClick = this.onSelectionOfExternalBank.bind(this);
            this.view.AddExternalAccounts.SelectBankOrVendor.btnProceed.onClick = this.onClickOfProceedOnExternalBankList.bind(this);
            this.view.AddExternalAccounts.SelectBankOrVendor.btnReset.onClick = this.onClickOfResetOnExternalBankList.bind(this);
            this.view.AddExternalAccounts.SelectBankOrVendor.flxBankList.isVisible = false;
            this.view.AddExternalAccounts.SelectBankOrVendor.tbxName.text = "";
            this.disableButton(this.view.AddExternalAccounts.SelectBankOrVendor.btnProceed);
            CommonUtilities.hideProgressBar(this.view);
            this.AdjustScreen();
        },

        /**
         * Method on success Save ExternalBankCredentails
         * @param {JSON} response response
         */
        onSuccessSaveExternalBankCredentailsSuccess: function(response) {
            var self = this;
            var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
            accountsModule.presentationController.fetchExternalBankAccounts({
                mainUser: applicationManager.getUserPreferencesManager().getCurrentUserName(),
                userName: self.ExternalLoginContextData.username,
                bankId: self.ExternalLoginContextData.bankId
            });
        },

        onSuccessSaveExternalBankCredentailsFailure: function(response) {
            var self = this;
            CommonUtilities.hideProgressBar(this.view);
            this.view.AddExternalAccounts.LoginUsingSelectedBank.lblLoginUsingSelectedBankError.text = kony.i18n.getLocalizedString("i18n.login.failedToLogin");
            this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankError.isVisible = true;
            this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text = "";
            this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text,
                this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
            this.view.AddExternalAccounts.forceLayout();
        },

        /**
         * Adds selected external accounts
         */
        addExternalAccounts: function() {
            CommonUtilities.showProgressBar(this.view);
            var selectedAccounts = [];
            for (var i = 0; i < this.selectedRowIndices.length; i++) {
                selectedAccounts.push(this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.data[this.selectedRowIndices[i]]);
            }
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule').presentationController.addExternalBankAccounts(selectedAccounts);
        },

        /**
         * Toggles between terms accepted or not accepted
         */
        toggleBetweenTermsAcceptedAndUnaccepted: function() {
            var isAccepted = this.isTermsAndConditionsAccepted;
            if (!isAccepted) {
                this.view.AddExternalAccounts.LoginUsingSelectedBank.lblCheckBox.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                this.isTermsAndConditionsAccepted = true;
            } else {
                this.view.AddExternalAccounts.LoginUsingSelectedBank.lblCheckBox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                this.isTermsAndConditionsAccepted = false;
            }
            this.view.forceLayout();
        },

        /**
         * Method to enable Save button
         */
        enableSaveButton: function() {
            var termsAccepted = this.isTermsAndConditionsAccepted;
            var isAtleastOneAccountSelected = this.isAtLeastOneAccountSelected;
            if (termsAccepted === true && isAtleastOneAccountSelected === true) {
                this.enableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
            } else {
                this.disableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
            }
        },

        /**
         * initlizeDonutChart: Method is used to initlize the DonutChart for custom widget
         */
        initlizeDonutChart: function() {
            var data = [];
            var options = {
                height: 310,
                width: this.view.mySpending.flxMySpending.frame.width,
                position: 'top',
                chartArea: {
                    right: 150,
                    left: 20
                },
                title: '',
                pieHole: 0.6,
                pieSliceText: 'none',
                toolTip: {
                    text: 'percentage'
                },
                colors: ["#FEDB64", "#E87C5E", "#6753EC", "#E8A75E", "#3645A7", "#04B6DF", "#8ED174", "#D6B9EA", "#B160DC", "#23A8B1"]
            };
            var donutChart = new kony.ui.CustomWidget({
                "id": "donutChart1",
                "isVisible": true,
                "left": "1" + ViewConstants.POSITIONAL_VALUES.DP,
                "top": "0" + ViewConstants.POSITIONAL_VALUES.DP,
                "width": "500" + ViewConstants.POSITIONAL_VALUES.DP,
                "height": "390" + ViewConstants.POSITIONAL_VALUES.DP,
                "zIndex": 1000000
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "DonutChart",
                "chartData": data,
                "chartProperties": options,
                "OnClickOfPie": function() {}
            });
            this.view.mySpending.flxMySpendingWrapper.onClick = function() {
                var pfmModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PersonalFinanceManagementModule");
                pfmModule.presentationController.initPFMForm();
            };
            this.view.flxSecondaryDetails.mySpending.flxMySpendingWrapper.flxMySpending.add(donutChart);
        },

        initializeCashPositionChart: function(chartProperties) {
            var data = [];
            var options = {
                legend: {
                    position: "none"
                },
                seriesType: 'bars',
                series: {
                    2: {
                        type: 'line'
                    }
                },
                colors: ['#1A98FF', '#8F99AC', '#8F99AC'],
                curveType: 'function',
                lineDashStyle: [4, 4],
                vAxis: {
                    format: 'short',
                    minValue: 0,
                    maxValue: 90000,
                    viewWindowMode: 'maximized'
                },
                height: chartProperties.height
            };

            var comboChart = new kony.ui.CustomWidget({
                "id": "comboChartForCashPosition",
                "isVisible": true,
                "left": "0" + ViewConstants.POSITIONAL_VALUES.DP,
                "top": "0" + ViewConstants.POSITIONAL_VALUES.DP,
                "width": "100%",
                "height": "100%",
                "zIndex": 1000000
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "ComboChart",
                "chartData": data,
                "chartProperties": options,
                "OnClickOfBar": function() {}
            });
            this.view.flxMainChartCon.flxCashPostionChart.add(comboChart);

        },

        showAlertCashPositionChart: function(data) {
            /* if the service call fails hide the chart flex*/
            this.view.flxMainChartCon.setVisibility(false);
            /*and show the error flex and the message*/
            var visible = this.view.flxErrorMessage.isVisible ? false : true;
            this.view.flxErrorMessage.setVisibility(visible);

            if (!kony.sdk.isNullOrUndefined(data.errorMessage))
                this.view.lblCashPositErrorMionessage.text = data.errorMessage;
            else
                this.view.flxMyCashPosition.setVisibility(false);

            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },

        populateCashPositionChart: function(data) {
            /*if the service call succeeds hide the error flex regardless*/
            this.view.flxErrorMessage.setVisibility(false);

            //Initializing cash position chart widgets
            if (kony.application.getCurrentBreakpoint() === 640) {
                this.view.flxSelectYearsMobile.setVisibility(true);
                this.view.flxSelectAccountsMobile.setVisibility(true);
                this.view.flxSelectYears.setVisibility(false);
                this.view.flxSelectAccounts.setVisibility(false);
                this.view.flxYearsDropDownMobile.onClick = this.showDurationDropDown;
                this.view.flxSelectAccountsMobileDropDown.onClick = this.showAccountsDropDown;
            } else {
                this.view.flxSelectYearsMobile.setVisibility(false);
                this.view.flxSelectAccountsMobile.setVisibility(false);
                this.view.flxSelectYears.setVisibility(true);
                this.view.flxSelectAccounts.setVisibility(true);
                this.view.flxSelectYears.onClick = this.showDurationDropDown;
                this.view.flxSelectAccounts.onClick = this.showAccountsDropDown;
            }
            this.view.durationListMenu.segAccountListActions.onRowClick = this.fetchCashPositionByDurationOnRowClick;
            this.view.selectAccountList.segAccountListActions.onRowClick = this.fetchCashPositionByAccountTypeOnRowClick;

            this.setCashPositionFilterData();

            /* and show the chart flex regardless*/
            this.view.flxMainChartCon.setVisibility(true);
            /* feed the data to the bar chart*/
            this.view.flxMainChartCon.flxCashPostionChart.comboChartForCashPosition.chartData = data;

            FormControllerUtility.hideProgressBar(this.view);

        },

        /*
        	Method to set duration and account filter data for cash postion chart
        */
        setCashPositionFilterData: function() {
            var durationData = [{
                    lblUsers: kony.i18n.getLocalizedString("i18n.konybb.cashPosition.lastBusinessWeek")
                },
                {
                    lblUsers: kony.i18n.getLocalizedString("i18n.konybb.cashPosition.thisMonth")
                },
                {
                    lblUsers: kony.i18n.getLocalizedString("i18n.konybb.cashPosition.thisYear")
                },
                {
                    lblUsers: kony.i18n.getLocalizedString("i18n.konybb.cashPosition.lastYear")
                }
            ];

            this.view.durationListMenu.segAccountListActions.setData(durationData);

            var accountData = [{
                    lblUsers: kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")
                },
                {
                    lblUsers: kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.SavingsAccounts")
                },
                {
                    lblUsers: kony.i18n.getLocalizedString("i18n.konybb.accountsLanding.CheckingAccounts")
                }
            ];

            this.view.selectAccountList.segAccountListActions.setData(accountData);
        },

        populateReceivablePayableChart: function() {

            var data = [{
                "errmsg": null,
                "monthId": "2",
                "monthName": "",
                "totalCashFlow": "4,90,000",
                "label": "",
                "Value": 490000,
                "colorCode": "red",
                "annotationText": "",
                "tooltipText": "$4,90,000",
                "barColor": '#6BC700'
            }, {
                "errmsg": null,
                "monthId": "1",
                "monthName": "",
                "totalCashFlow": "350.00",
                "label": "",
                "Value": 930000,
                "colorCode": "green",
                "annotationText": "",
                "tooltipText": "$9,30,000",
                "barColor": '#F8A13E'
            }];

            this.view.flxMainChartContainer.flxChartContainer.barChartForCashFlow.chartData = data;
        },

        ///////////////////////////////////refactoring code////////////////////////////////
        /**
         * Method that called on preshow of frmBBAccountsLanding
         */
        preShowFrmAccountsLanding: function() {
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['customheader', 'flxMain', 'flxHeader', 'flxFooter','flxFormContent']);
            applicationManager.getNavigationManager().applyUpdates(this);
            var scopeObj = this;
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Accounts");
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
            this.setPermissionBasedView();
            scopeObj.setHeader();

            this.formPreshowUISetting();
            this.initActions();

            this.loadDashboardWidgets();
        },

        showOutageNotification: function(outageMessageView) {
            this.setOutageNotification(outageMessageView.isVisible, outageMessageView.message);

            FormControllerUtility.hideProgressBar(this.view);
        },

        checkForOverdraft: function(transactions) {
            transactions = (kony.sdk.isNullOrUndefined(transactions) || transactions.constructor !== Array) ? [] : transactions;
            var isOverdraft = false;
            for (var i = 0; i < transactions.length; i++) {
                if (String(transactions[i]["isOverdraft"]).toLowerCase() === "true") {
                    isOverdraft = true;
                    break;
                }
            }
            this.setOverdraftNotification(isOverdraft);
        },

        showUpcomingTransactionsWidget: function(transactions) {
            var self = this;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.upcomingTransactions.lblHeader, kony.i18n.getLocalizedString("i18n.Accounts.upComingTransactions"), accessibilityConfig);
            this.view.upcomingTransactions.lblHeader.skin = ViewConstants.SKINS.LABEL_HEADER_BOLD;
            var formattedTransactions = [];
            this.setOverdraftNotification(false);
            formattedTransactions = transactions.map(function(transaction) {
                if (String(transaction.isOverdraft).toLowerCase() === "true") {
                    self.setOverdraftNotification(true);
                }
                var leftValue = "30dp";
                if (kony.application.getCurrentBreakpoint() === 640) {
                    leftValue = "15dp";
                }
                var date = {
                    text: CommonUtilities.getFrontendDateString(transaction.scheduledDate),
                    left: leftValue,
                    "accessibilityconfig": {
                        "a11yLabel": CommonUtilities.getFrontendDateString(transaction.scheduledDate),
                    }
                };
                var to = {
                    text: kony.i18n.getLocalizedString("i18n.transfers.lblTo"),
                    left: leftValue,
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.lblTo")
                    }
                };
                var toValue = {
                    text: transaction.payPersonName || transaction.payeeNickName || transaction.payeeName || transaction.toAccountName,
                    left: parseInt(leftValue) + 24 + "dp",
                    "accessibilityconfig": {
                        "a11yLabel": transaction.payPersonName || transaction.payeeNickName || transaction.payeeName || transaction.toAccountName,
                    }
                };
                var toNavForm = {
                    text: (applicationManager.getTypeManager().getTransactionTypeDisplayValue(transaction.transactionType) != null ?
                        applicationManager.getTypeManager().getTransactionTypeDisplayValue(transaction.transactionType) :
                        transaction.transactionType),
                    left: leftValue,
                    "accessibilityconfig": {
                        "a11yLabel": (applicationManager.getTypeManager().getTransactionTypeDisplayValue(transaction.transactionType) != null ?
                            applicationManager.getTypeManager().getTransactionTypeDisplayValue(transaction.transactionType) :
                            transaction.transactionType)
                    }
                };
                return {
                    "lblDate": date,
                    "lblTo": to,
                    "lblToValue": toValue,
                    "lblFormToNavigate": toNavForm,
                    "lblAmount": {
                        "text": CommonUtilities.formatCurrencyWithCommas(transaction.amount, false, transaction.currencyCode),
                        "accessibilityconfig": {
                            "a11yLabel": CommonUtilities.formatCurrencyWithCommas(transaction.amount, false, transaction.currencyCode)
                        }
                    },
                    "lblSeparator": "lblSeparator"
                }
            });
            if (formattedTransactions.length === 0) {
                this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(false);
                this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(true);
                this.view.upcomingTransactions.flxNoTransactions.setVisibility(true);
                this.view.upcomingTransactions.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString("i18n.AccountsLanding.NoTransactionsScheduled");
            } else {
                this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(true);
                this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(false);
                this.view.upcomingTransactions.segMessages.setData(formattedTransactions);
            }
            this.view.upcomingTransactions.forceLayout();
            this.AdjustScreen()
        },

        /**
         * Method to show messages
         * @param {Array} messages, messages view modeld
         */
        showMessagesWidget: function(messages) {
            var self = this;
            this.updateMessagesWidget(messages);
            this.view.myMessages.forceLayout();
            this.AdjustScreen();

            FormControllerUtility.hideProgressBar(this.view);
        },

        /**
         * Method to create accounts segment view model
         * @param {Collection} accounts List of accounts
         * @returns {JSON} account viewModel
         */
        createAccountSegmentsModel: function(accounts) {
            var scopeObject = this;
            var getConfigFor = function(accountType) {
                if (scopeObject.accountTypeConfig[accountType]) {
                    return scopeObject.accountTypeConfig[accountType];
                } else {
                    return scopeObject.accountTypeConfig.Default;
                }
            };
            return accounts.map(function(account) {
                var dataObject = {
                    "template": kony.application.getCurrentBreakpoint() === 640 ? "flxAccountListItemMobile" : "flxAccountListItem",
                    "lblAccountName": account.nickName || account.accountName,
                    "lblAccountNumber": kony.i18n.getLocalizedString("i18n.common.accountNumber") + "-X" + CommonUtilities.getLastFourDigit(account.accountID),
                    "lblAvailableBalanceValue": CommonUtilities.formatCurrencyWithCommas(account[getConfigFor(account.accountType).balanceKey], false, account.currencyCode),
                    "lblAvailableBalanceTitle": getConfigFor(account.accountType).balanceTitle + String(account.isExternalAccount === true ? ": " + account.availableBalanceUpdatedAt : ""),
                    "onAccountClick": account.isExternalAccount === true ? scopeObject.showExternalAccountUpdateAlert : scopeObject.onAccountSelection.bind(scopeObject, account),
                    "onQuickActions": scopeObject.openQuickActions.bind(scopeObject, account),
                    "flxMenu": {
                        // "skin": self.getSkinForAccount(account.accountType)
                    },
                    "imgThreeDotIcon": {
                        "text": ViewConstants.FONT_ICONS.THREE_DOTS_ACCOUNTS,
                        "skin": ViewConstants.SKINS.THREE_DOTS_IMAGE,
                        "isVisible": true,
                        "accessibilityconfig": {
                            "a11yLabel": "Contextual Menu"
                        }
                    },
                    "btn": " ",
                    "flxIdentifier": {
                        "skin": getConfigFor(account.accountType).sideSkin,
                        "isVisible": false
                    },
                    "imgBankName": account.bankLogo,
                    "userName": account.userName,
                    "bankId": account.bankId,
                    "accountName": account.accountName,
                    "isError": account.isError,
                    "group": account.originalAmount,
                    "accountType": account.accountType,
                    "balance": account[getConfigFor(account.accountType).balanceKey]
                };
                if (CommonUtilities.isCSRMode()) {
                    var starActive = {
                        "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_ACTIVE,
                        "skin": ViewConstants.SKINS.ACCOUNTS_CSR_FAVOURITE_SELECTED,
                        "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.optionDisabled"),
                        "isVisible": true,
                        "accessibilityconfig": {
                            "a11yLabel": "Enabled Favourite Account"
                        }
                    };
                    var starInActive = {
                        "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_INACTIVE,
                        "skin": ViewConstants.SKINS.ACCOUNTS_CSR_FAVOURITE_UNSELECTED,
                        "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.optionDisabled"),
                        "isVisible": true,
                        "accessibilityconfig": {
                            "a11yLabel": "Disabled Favourite Account"
                        }
                    }
                    dataObject.imgStarIcon = scopeObject.isFavourite(account) ? starActive : starInActive;
                    dataObject.toggleFavourite = CommonUtilities.disableButtonActionForCSRMode;
                } else {
                    dataObject.imgStarIcon = scopeObject.isFavourite(account) ? {
                        "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_ACTIVE,
                        "skin": ViewConstants.SKINS.ACCOUNTS_FAVOURITE_SELECTED,
                        "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.removePreferred"),
                        "isVisible": true
                    } : {
                        "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_INACTIVE,
                        "skin": ViewConstants.SKINS.ACCOUNTS_FAVOURITE_UNSELECTED,
                        "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.setAsFavourite"),
                        "isVisible": true
                    };
                    dataObject.toggleFavourite = scopeObject.loadAccountModule().presentationController.changeAccountFavouriteStatus.bind(scopeObject.loadAccountModule().presentationController, account);
                }
                return dataObject;
            });
        },
        /**
         * showDeletePopUp :  Method to display pop up for deleting external account.
         */
        showDeletePopUp: function(account) {
            try {
                var scopeObject = this;
                this.view.flxLogout.left = "0%";
                scopeObject.view.flxLogout.height = scopeObject.getPageHeight();
                scopeObject.view.flxLogout.setFocus(true);
                this.view.CustomPopup.setFocus(true);
                this.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.removeAccount");
                this.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.AccountsAggregation.AreYouSure") +
                    " " + account.bankName + " " +
                    kony.i18n.getLocalizedString("i18n.AccountsAggregation.PermanentlyFromList");
                if (CommonUtilities.isCSRMode()) {
                    this.view.CustomPopup.btnYes.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    this.view.CustomPopup.btnYes.skin = CommonUtilities.disableButtonSkinForCSRMode();
                    this.view.CustomPopup.btnYes.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
                    this.view.CustomPopup.btnYes.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    this.view.CustomPopup.btnYes.onClick = function() {
                        scopeObject.view.flxLogout.left = "-100%";
                        var accountInfo = {
                            mainUser: applicationManager.getUserPreferencesManager().getCurrentUserName(),
                            userName: account.userName,
                            bankId: account.bankId,
                            accountName: account.accountName,
                            loopCount: "1"
                        };
                        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.deleteExternalAccount(accountInfo);
                    };
                }
                this.view.CustomPopup.btnNo.onClick = function() {
                    scopeObject.view.flxLogout.left = "-100%";
                };
                this.view.CustomPopup.flxCross.onClick = function() {
                    scopeObject.view.flxLogout.left = "-100%";
                };
            } catch (error) {}
        },

        /**
         Method to check if user has permission to view approval and requests count
         */
        approvalsAndRequestsEntitilementCheck: function() {
            var checkUserPermission = function(permission) {
                return applicationManager.getConfigurationManager().checkUserPermission(permission);
            };
            var approvalPermissions = applicationManager.getConfigurationManager().getApprovalsFeaturePermissionsList();
            var requestsPermissions = applicationManager.getConfigurationManager().getRequestsFeaturePermissionsList();
            var isApproveEnabled = approvalPermissions.some(checkUserPermission);
            var isRequestsEnabled = requestsPermissions.some(checkUserPermission);

            if (isApproveEnabled || isRequestsEnabled)
                return true;
            else
                return false;
        },

        /**
         * Method to load all the dashboard widgets
         *@param {object} navObject
         */
        loadDashboardWidgets: function(data) {
            this.getPasswordExpirationWarning(data);
            if (this.transactionViewAccessCheck()) {
                var inputParams = {
                    "Duration": BBConstants.LAST_YEAR,
                    "AccountType": BBConstants.ALL
                };
                this.loadCashposition(inputParams);
            }


            if (this.approvalsAndRequestsEntitilementCheck())
                this.loadApprovalsAndRequestCount();
        },

        /**
         * Method to trigger loadNumberOfUnReadMessages
         *@param {object} navObject
         */
        loadNumberOfUnReadMessages: function() {
            var navigationObject = {
                requestData: null,
                onSuccess: {
                    form: "BBAccountsModule/frmBBAccountsLanding",
                    module: "BBAccountsModule",
                    context: {
                        key: BBConstants.UNREAD_MESSAGES_COUNT,
                        responseData: null
                    }
                },
                onFailure: {
                    form: "AuthModule/frmLogin",
                    module: "AuthModule",
                    context: {
                        key: BBConstants.LOG_OUT,
                        responseData: null
                    }
                }
            };

            this.presenter.presentationController.fetchUnreadMessagesOrNotificationsCount(navigationObject);
        },
        /**
         * Method to trigger loadCashposition
         *@param {object} navObject
         */
        loadCashposition: function(inputParams) {
            var navigationObject = {
                requestData: inputParams,
                onSuccess: {
                    form: "BBAccountsModule/frmBBAccountsLanding",
                    module: "BBAccountsModule",
                    context: {
                        key: BBConstants.CASH_POSITION,
                        responseData: null
                    }
                },
                onFailure: {
                    form: "BBAccountsModule/frmBBAccountsLanding",
                    module: "BBAccountsModule",
                    context: {
                        key: BBConstants.CASH_POSITION_ERROR,
                        responseData: null
                    }
                }
            };

            this.presenter.presentationController.getCashPosition(navigationObject);
        },

        /**
         * Method to trigger loadApprovalsAndRequestCount
         *@param {object} navObject
         */
        loadApprovalsAndRequestCount: function() {
            var navigationObject = {
                requestData: null,
                onSuccess: {
                    form: "BBAccountsModule/frmBBAccountsLanding",
                    module: "BBAccountsModule",
                    context: {
                        key: BBConstants.APPROVALS_REQUESTS_COUNT,
                        responseData: null
                    }
                },
                onFailure: {
                    form: "BBAccountsModule/frmBBAccountsLanding",
                    module: "BBAccountsModule",
                    context: {
                        key: BBConstants.APPROVALS_REQUESTS_COUNT_FAILURE,
                        responseData: null
                    }
                }
            };

            this.presenter.presentationController.fetchCountsOfApprovalAndRequest(navigationObject);
        },

        navigateToAccountPreferences: function() {
            var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
            profileModule.presentationController.showPreferredAccounts();
        }
    };
});