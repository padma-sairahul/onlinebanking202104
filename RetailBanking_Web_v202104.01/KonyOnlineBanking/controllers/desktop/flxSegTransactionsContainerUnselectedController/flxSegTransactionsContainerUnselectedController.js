define({
    showSelectedRow: function() {
        var index = kony.application.getCurrentForm().CategorizedMonthlySpending.segTransactions.selectedIndex;
        var sectionIndex = index[0];
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().CategorizedMonthlySpending.segTransactions.data;
        var collapseAll = function(segments) {
            segments.forEach(function(segment, i) {
                if (segment.template == "flxSegTransactionsContainer") {
                    segment.template = "flxSegTransactionsContainerUnselected";
                    segment.imgDropdown = "arrow_down.png";
                    kony.application.getCurrentForm().CategorizedMonthlySpending.segTransactions.setDataAt(segment, i, sectionIndex);
                }
            });
        };
        if (data[sectionIndex][1]) {
            if (data[sectionIndex][1][rowIndex].template == "flxSegTransactionsContainerUnselected") {
                collapseAll(data[sectionIndex][1]);
                data[sectionIndex][1][rowIndex].imgDropdown = "chevron_up.png";
                data[sectionIndex][1][rowIndex].template = "flxSegTransactionsContainer";
            } else {
                data[sectionIndex][1][rowIndex].imgDropdown = "arrow_down.png";
                data[sectionIndex][1][rowIndex].template = "flxSegTransactionsContainerUnselected";
            }
            kony.application.getCurrentForm().CategorizedMonthlySpending.segTransactions.setDataAt(data[sectionIndex][1][rowIndex], rowIndex, sectionIndex);
        } else {
            if (data[rowIndex].template == "flxSegTransactionsContainerUnselected") {
                collapseAll(data);
                data[rowIndex].imgDropdown = "chevron_up.png";
                data[rowIndex].template = "flxSegTransactionsContainer";
            } else {
                data[rowIndex].imgDropdown = "arrow_down.png";
                data[rowIndex].template = "flxSegTransactionsContainerUnselected";
            }
            kony.application.getCurrentForm().CategorizedMonthlySpending.segTransactions.setDataAt(data[rowIndex], rowIndex, sectionIndex);
        }
    }
});