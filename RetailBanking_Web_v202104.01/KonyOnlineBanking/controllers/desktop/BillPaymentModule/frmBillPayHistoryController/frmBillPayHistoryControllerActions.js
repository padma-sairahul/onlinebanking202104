define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnNo **/
    AS_Button_aa62e2678c3840dfae7b683d7861e194: function AS_Button_aa62e2678c3840dfae7b683d7861e194(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** onClick defined for btnYes **/
    AS_Button_d7d02dbfef8947c6b91c723e58d73396: function AS_Button_d7d02dbfef8947c6b91c723e58d73396(eventobject) {
        var self = this;
        var obj1 = {
            "tabname": "payees"
        };
        var navObj = new kony.mvc.Navigation("frmBillPay");
        navObj.navigate(obj1);
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_e2113baea2fd4827b10c1fa32067e4d5: function AS_FlexContainer_e2113baea2fd4827b10c1fa32067e4d5(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** init defined for frmBillPayHistory **/
    AS_Form_d30f3614dc224fc5bedc49e4aa8443b8: function AS_Form_d30f3614dc224fc5bedc49e4aa8443b8(eventobject) {
        var self = this;
        this.init();
    }
});