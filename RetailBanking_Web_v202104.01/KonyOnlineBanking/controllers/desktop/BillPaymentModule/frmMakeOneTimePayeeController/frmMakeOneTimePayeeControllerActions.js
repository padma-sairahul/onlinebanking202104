define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmMakeOneTimePayee **/
    AS_Form_c8a52cd77977409fa30868f5d72d53a2: function AS_Form_c8a52cd77977409fa30868f5d72d53a2(eventobject) {
        var self = this;
        this.init();
    },
    /** onRowClick defined for segPayeesName **/
    AS_Segment_a9eb260788ca44409d5aa3882669978b: function AS_Segment_a9eb260788ca44409d5aa3882669978b(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.selectPayeeName();
    }
});