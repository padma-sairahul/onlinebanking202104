define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnModify **/
    AS_Button_e3d96e1b543c4f2b9b85e295e1589174: function AS_Button_e3d96e1b543c4f2b9b85e295e1589174(eventobject) {
        var self = this;
        this.navigateToAddPayee();
    },
    /** onClick defined for btnConfirm **/
    AS_Button_g04abad530bf45049c83a2ca495d7c48: function AS_Button_g04abad530bf45049c83a2ca495d7c48(eventobject) {
        var self = this;
        this.goToAddPayeeAcknowledgement();
    },
    /** onClick defined for btnCancel **/
    AS_Button_jef766b0b2734a7fb34012da17707eba: function AS_Button_jef766b0b2734a7fb34012da17707eba(eventobject) {
        var self = this;
        this.showCancelPopup();
    },
    /** init defined for frmOneTimePaymentConfirm **/
    AS_Form_ae99f04d04ee40fe872a1019bf2a7c42: function AS_Form_ae99f04d04ee40fe872a1019bf2a7c42(eventobject) {
        var self = this;
        this.init();
    }
});