/**
 * Description of Module representing a Confirm form.
 * @module frmAddPayeeInformationController
 */
define(['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    var orientationHandler = new OrientationHandler();
    return {
        profileAccess: "",
        init: function() {
            var scopeObj = this;
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule").presentationController;
            this.initActions();
        },
        preShow: function() {
            this.profileAccess = applicationManager.getUserPreferencesManager().profileAccess;
            this.view.customheadernew.activateMenu("Bill Pay", "Add Payee");
            this.view.flxDowntimeWarning.setVisibility(false);
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader', 'flxFooter']);
        },
        postShow: function() {
            this.view.btnEnterPayeeInfo.setFocus(true);
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.info.frame.height - this.view.flxFooter.info.frame.height + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            applicationManager.executeAuthorizationFramework(this);
        },
        initActions: function() {
            this.view.btnSearchPayee.onClick = this.showFlxSearchPayee.bind(this);
            this.view.btnResetPayeeInfo.onClick = this.resetPayeeInformation.bind(this);
            this.view.flxClick.onClick = this.rememberThis.bind(this);
            this.view.btnReset.onClick = this.goToAddBillerDetails.bind(this);
            this.view.flxBillPayActivities.onClick = function() {
                this.presenter.showBillPaymentScreen({
                    context: "History",
                    loadBills: true
                })
            }.bind(this);
            if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                this.view.tbxEnterAccountNmber.placeholder = kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberPlaceholder");
            } else {
                this.view.tbxEnterAccountNmber.placeholder = kony.i18n.getLocalizedString("i18n.AddPayee.EnterAccountNumberasit");
            }
            this.restrictSpecialCharacters();
        },
        onBreakpointChange: function(form, width) {
            var scopeObj = this;
            FormControllerUtility.setupFormOnTouchEnd(width);
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            this.view.CustomPopupLogout.onBreakpointChangeComponent(scopeObj.view.CustomPopupLogout, width);
        },
        /** @alias module:frmAddPayeeInformationController */
        /** updates the present Form based on required function.
         * @param {list} viewModel used to load a view
         */
        updateFormUI: function(viewModel) {
            if (viewModel.isLoading === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewModel.isLoading === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewModel.firstLoad) {
                this.initiateAddPayee(viewModel.firstLoad);
            }
            if (viewModel.registeredPayeeList) {
                this.updateRegisteredPayees(viewModel.registeredPayeeList);
            }
            if (viewModel.isInvalidPayee) {
                this.enterCorrectBillerName();
            }
            if (viewModel.serverError) {
                this.view.rtxDowntimeWarning.text = viewModel.serverError;
                this.view.flxDowntimeWarning.setVisibility(true);
                FormControllerUtility.hideProgressBar(this.view);
                this.view.flxFormContent.forceLayout();
            } else {
                this.view.flxDowntimeWarning.setVisibility(false);
            }
        },

        /**
         * higlightBoxes
         */
        higlightBoxes: function() {
            for (var i = 0; i < arguments.length; i++) {
                arguments[i].skin = 'sknTbxSSPffffff15PxBorderFF0000opa50';
            }
        },
        /**
         * normalizeBoxes
         */
        normalizeBoxes: function() {
            for (var i = 0; i < arguments.length; i++) {
                arguments[i].skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            }
        },
        /**
         * noneditable boxes
         */
        nonEditable: function() {
            for (var i = 0; i < arguments.length; i++) {
                arguments[i].skin = ViewConstants.SKINS.NON_EDITABLETEXTBOX;
                arguments[i].focusSkin = ViewConstants.SKINS.NON_EDITABLETEXTBOX;
                arguments[i].hoverSkin = ViewConstants.SKINS.NON_EDITABLETEXTBOX;
            }
        },
        /**
         * noneditable boxes
         */
        editable: function() {
            for (var i = 0; i < arguments.length; i++) {
                arguments[i].focusSkin = ViewConstants.SKINS.EDITABLE_TEXTBOX;
                arguments[i].hoverSkin = ViewConstants.SKINS.EDITABLE_TEXTBOX;
            }
        },
        /**
         * used to show the add payee screen
         */
        initiateAddPayee: function() {
            this.initializePayeeInformation();
            this.registerWidgetActions();
            this.accountNumberAvailable = true;
        },
        /**
         * used to initilize the payee information
         */
        initializePayeeInformation: function() {
            this.view.flxNoExactMatch.isVisible = false;
            this.view.lblErrorInfo.isVisible = false;
            this.view.tbxEnterName.text = "";
            this.view.tbxEnterAddress.text = "";
            this.view.tbxEnterAddressLine2.text = "";
            this.view.tbxCity.text = "";
            this.view.tbxEnterZipCode.text = "";
            CommonUtilities.setText(this.view.lblRememberMeIcon, ViewConstants.FONT_ICONS.CHECBOX_SELECTED, CommonUtilities.getaccessibilityConfig());
            this.view.lblRememberMeIcon.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
            this.view.tbxAdditionalNote.text = "";
            this.rememberThis();
            this.view.tbxEnterAccountNmber.text = "";
            this.view.tbxConfirmAccNumber.text = "";
            this.normalizeBoxes(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
            FormControllerUtility.disableButton(this.view.btnReset);
        },
        /**
         * navigate to frmAddPayee
         */
        showFlxSearchPayee: function() {
            this.presenter.showBillPaymentScreen({
                context: "AddPayee"
            });
        },
        /**
         * remember functionality
         */
        rememberThis: function() {
            if (this.view.lblRememberMeIcon.text === ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
                CommonUtilities.setText(this.view.lblRememberMeIcon, ViewConstants.FONT_ICONS.CHECBOX_SELECTED, CommonUtilities.getaccessibilityConfig());
                this.view.lblRememberMeIcon.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                this.view.tbxAdditionalNote.isVisible = true;
                this.nonEditable(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
                this.view.tbxEnterAccountNmber.setEnabled(false);
                this.view.tbxConfirmAccNumber.setEnabled(false);
                this.accountNumberAvailable = false;
                this.checkIfAllManualFieldsAreFilled();
            } else {
                CommonUtilities.setText(this.view.lblRememberMeIcon, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, CommonUtilities.getaccessibilityConfig());
                this.view.lblRememberMeIcon.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                this.view.tbxAdditionalNote.isVisible = false;
                this.view.tbxAdditionalNote.text = "";
                this.view.tbxEnterAccountNmber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
                this.editable(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
                this.view.tbxEnterAccountNmber.setEnabled(true);
                this.view.tbxConfirmAccNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
                this.view.tbxConfirmAccNumber.setEnabled(true);
                this.accountNumberAvailable = true;
                this.checkIfAllManualFieldsAreFilled();
            }
            this.view.forceLayout();
        },
        /**
         * validates the all manually added payee manidatory information
         */
        checkIfAllManualFieldsAreFilled: function() {
            if (this.view.tbxAdditionalNote.isVisible === true) {
                if (this.view.tbxEnterName.text.trim() && this.view.tbxEnterAddress.text.trim() && this.view.tbxCity.text.trim() && this.view.tbxEnterZipCode.text.trim()) {
                    FormControllerUtility.enableButton(this.view.btnReset);
                } else {
                    FormControllerUtility.disableButton(this.view.btnReset);
                }
            } else {
                if (this.view.tbxEnterName.text.trim() && this.view.tbxEnterAddress.text.trim() && this.view.tbxCity.text.trim() && this.view.tbxEnterZipCode.text.trim() &&
                    this.view.tbxEnterAccountNmber.text.trim() && this.view.tbxConfirmAccNumber.text.trim()) {
                    FormControllerUtility.enableButton(this.view.btnReset);
                } else {
                    FormControllerUtility.disableButton(this.view.btnReset);
                }
            }
        },
        /**
         * used to register the widget actions
         */
        registerWidgetActions: function() {
            var scopeObj = this;
            FormControllerUtility.disableButton(this.view.btnReset);
            [this.view.tbxEnterName, this.view.tbxEnterAddress, this.view.tbxEnterAddressLine2, this.view.tbxCity,
                this.view.tbxEnterZipCode, this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber, this.view.tbxAdditionalNote
            ].forEach(function(element) {
                element.onKeyUp = scopeObj.checkIfAllManualFieldsAreFilled;
            });
        },
        /**
         * used to set the payee widget Map
         * @param {list} registeredPayees list of payees
         */
        updateRegisteredPayees: function(registeredPayees) {
            this.view.segRegisteredPayees.widgetDataMap = {
                "flxRegistered": "flxRegistered",
                "lblCustomer": "lblCustomer",
                "lblAmount": "lblAmount",
                "lblDate": "lblDate",
                "btnViewDetails": "btnViewDetails",
                "btnPayBills": "btnPayBills",
                "lblHorizontalLine": "lblHorizontalLine",
                "lblIcon": "lblIcon",
                "flxIcon": "flxIcon"
            };
            if (registeredPayees.length === 0) {
                this.view.flxRegisteredPayees.setVisibility(false);
            } else {
                this.view.flxRegisteredPayees.setVisibility(true);
                this.view.segRegisteredPayees.setData(this.createRegisteredPayeesSegmentModel(registeredPayees));
            }
            this.view.forceLayout();
        },
        /**
         * used to show the  list of all payees
         * @param {list} payees list of payees
         * @returns {object} payees object
         */
        createRegisteredPayeesSegmentModel: function(payees) {
            return payees.map(function(payee) {
                return {
                    "lblCustomer": payee.payeeName,
                    "lblHorizontalLine": "A",
                    "lblDate": CommonUtilities.getFrontendDateString(payee.lastPaidDate),
                    "lblAmount": CommonUtilities.formatCurrencyWithCommas(payee.lastPaidAmount),
                    "btnViewDetails": {
                        text: kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
                        isVisible: applicationManager.getConfigurationManager().checkUserPermission("BILL_PAY_VIEW_PAYEES"),
                        onClick: payee.onViewDetailsClick
                    },
                    "btnPayBills": {
                        text: kony.i18n.getLocalizedString("i18n.Pay.PayBills"),
                        isVisible: applicationManager.getConfigurationManager().checkUserPermission("BILL_PAY_CREATE"),
                        onClick: payee.onPayBillsClick
                    },
                    "lblIcon": {
                        //isVisible : applicationManager.getConfigurationManager().isCombinedUser === "true" ? true : false,
                        isVisible: this.profileAccess === "both" ? true : false,
                        text: payee.isBusinessUser === "1" ? "r" : "s"
                    },
                    "flxIcon": {
                        isVisible: false, //this.profileAccess === "both" ? true : false,
                        //isVisible : applicationManager.getConfigurationManager().isCombinedUser === "true" ? true : false
                    }
                };
            }).reduce(function(p, e) {
                return p.concat(e);
            }, []);
        },
        /**
         * reset the payee information
         */
        resetPayeeInformation: function() {
            this.view.flxNoExactMatch.isVisible = false;
            this.view.lblErrorInfo.isVisible = false;
            this.view.tbxEnterName.text = "";
            this.view.tbxEnterAddress.text = "";
            this.view.tbxEnterAddressLine2.text = "";
            this.view.tbxCity.text = "";
            this.view.tbxEnterZipCode.text = "";
            this.view.tbxAdditionalNote.text = "";
            if (this.accountNumberAvailable) {
                this.view.tbxEnterAccountNmber.text = "";
                this.view.tbxConfirmAccNumber.text = "";
                this.normalizeBoxes(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
            }
            FormControllerUtility.disableButton(this.view.btnReset);
        },
        /**
         * used to show the biller details screen
         */
        goToAddBillerDetails: function() {
            if (this.handleErrorInManualAddition()) {
                var contextData = this.editManuallyAddedPayeeDetails();
                let previousForm = kony.application.getPreviousForm().id;
                if (previousForm === "frmPayeeDetails" || previousForm === "frmVerifyPayee") {
                    contextData.modify = true;
                } else {
                    contextData.modify = false;
                }
                this.presenter.showUpdateBillerPage("frmAddPayeeInformation", contextData);
            }
        },
        /**
         * used to edit the manullay added payee details
         * @returns {object} manuallyAddedPayee
         */
        editManuallyAddedPayeeDetails: function() {
            var manuallyAddedPayee = {};
            manuallyAddedPayee.isManualUpdate = true;
            manuallyAddedPayee.billerName = this.view.tbxEnterName.text;
            if (this.accountNumberAvailable) {
                manuallyAddedPayee.noAccountNumber = false;
                manuallyAddedPayee.accountNumber = this.view.tbxConfirmAccNumber.text;
            } else {
                manuallyAddedPayee.noAccountNumber = true;
                manuallyAddedPayee.note = this.view.tbxAdditionalNote.text;
            }
            manuallyAddedPayee.addressLine1 = this.view.tbxEnterAddress.text;
            manuallyAddedPayee.addressLine2 = this.view.tbxEnterAddressLine2.text;
            manuallyAddedPayee.cityName = this.view.tbxCity.text;
            //manuallyAddedPayee.state = this.view.lbxState.selectedKeyValue[1];
            manuallyAddedPayee.zipCode = this.view.tbxEnterZipCode.text;
            return manuallyAddedPayee;
        },
        /**
         * used to handle the error schenario from manually added payee
         * @returns {boolean} returns the status
         */
        handleErrorInManualAddition: function() {
            var response = this.validateManuallyAddedPayeeDetails();
            if (response === 'VALIDATION_SUCCESS') {
                this.view.lblErrorInfo.isVisible = false;
                this.normalizeBoxes(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
                return true;
            } else if (response === 'ACCOUNT_NUMBER_MISMATCH') {
                CommonUtilities.setText(this.view.lblErrorInfo, kony.i18n.getLocalizedString("i18n.addPayee.AccountNumberMismatch"), CommonUtilities.getaccessibilityConfig());
                this.view.lblErrorInfo.isVisible = true;
                FormControllerUtility.disableButton(this.view.btnReset);
                this.higlightBoxes(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
                this.view.forceLayout();
                return false;
            }
        },
        /**
         * used to validate the manually added payee details
         * @returns {string} validation message
         */
        validateManuallyAddedPayeeDetails: function() {
            var accountNumber = this.view.tbxEnterAccountNmber.text;
            var duplicateAccountNumber = this.view.tbxConfirmAccNumber.text;
            if (this.accountNumberAvailable) {
                if (accountNumber === duplicateAccountNumber) {
                    return 'VALIDATION_SUCCESS';
                } else {
                    return 'ACCOUNT_NUMBER_MISMATCH';
                }
            } else {
                return 'VALIDATION_SUCCESS';
            }
        },
        /**
         * validate  the biller Name
         */
        enterCorrectBillerName: function() {
            this.view.rtxMisMatch.text = kony.i18n.getLocalizedString("i18n.addPayee.SelectBiller");
            this.view.lblNotMatching.isVisible = false;
            this.view.flxMisMatch.isVisible = true;
            FormControllerUtility.disableButton(this.view.btnReset);
            this.view.forceLayout();
        },
        /**
         * used to show the permission based UI
         */
        showHistoryOption: function() {
            this.view.flxBillPayActivities.setVisibility(true);
        },

        /**
         * used to hide the permission based UI
         */
        hideHistoryOption: function() {
            this.view.flxBillPayActivities.setVisibility(false);
        },

        restrictSpecialCharacters: function () {

            var specialCharactersSet = "~#^|$%&*!@()_-+=}{][/|?,.><`':;\"\\";
            var alphabetsSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var numericSet = "0123456789";

            this.view.tbxEnterName.restrictCharactersSet = specialCharactersSet.replace("!@#&*_'-.,", '');
            this.view.tbxEnterAddress.restrictCharactersSet = specialCharactersSet.replace("!@#&*_'-.,", '');
            this.view.tbxEnterAddressLine2.restrictCharactersSet = specialCharactersSet.replace("!@#&*_'-.,", '');
            this.view.tbxCity.restrictCharactersSet = numericSet + specialCharactersSet;
            this.view.tbxEnterZipCode.restrictCharactersSet = alphabetsSet + specialCharactersSet;
            this.view.tbxEnterAccountNmber.restrictCharactersSet = specialCharactersSet;
            this.view.tbxConfirmAccNumber.restrictCharactersSet = specialCharactersSet;

        },
    };
});