define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnSend **/
    AS_Button_b9f14703785c419a85ea58c6db247e5e: function AS_Button_b9f14703785c419a85ea58c6db247e5e(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for btnRequest **/
    AS_Button_ea4cbdc4491248859c837b9c1acf0baa: function AS_Button_ea4cbdc4491248859c837b9c1acf0baa(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_b64c9fb184b843579dd945a42f76eae0: function AS_FlexContainer_b64c9fb184b843579dd945a42f76eae0(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});