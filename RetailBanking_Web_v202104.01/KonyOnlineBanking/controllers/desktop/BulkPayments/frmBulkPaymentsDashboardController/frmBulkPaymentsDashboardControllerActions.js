define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnYes **/
    AS_Button_e6227a89fd5e4389bab3617b7c421105: function AS_Button_e6227a89fd5e4389bab3617b7c421105(eventobject) {
        var self = this;
        this.btnYesTakeSurvey();
    },
    /** onClick defined for btnNo **/
    AS_Button_i806e35651e04b1cbdbad15322d6e745: function AS_Button_i806e35651e04b1cbdbad15322d6e745(eventobject) {
        var self = this;
        this.btnNoTakeSurvey();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_gf509ea55f7347ae81f70361bae22054: function AS_FlexContainer_gf509ea55f7347ae81f70361bae22054(eventobject) {
        var self = this;
        this.onFeedbackCrossClick();
    },
    /** postShow defined for frmBulkPaymentsDashboard **/
    AS_Form_c6caedd2f16c41898fac709fcb86790e: function AS_Form_c6caedd2f16c41898fac709fcb86790e(eventobject) {
        var self = this;
        this.onPostShow();
    },
    /** init defined for frmBulkPaymentsDashboard **/
    AS_Form_cd46a142022b49b7806ae44e9938c720: function AS_Form_cd46a142022b49b7806ae44e9938c720(eventobject) {
        var self = this;
        this.onInit();
    },
    /** preShow defined for frmBulkPaymentsDashboard **/
    AS_Form_dc483c4888974b7399d675773163e055: function AS_Form_dc483c4888974b7399d675773163e055(eventobject) {
        var self = this;
        return this.onPreShow();
    },
    /** onTouchEnd defined for frmBulkPaymentsDashboard **/
    AS_Form_h3b597790b1d433aaf598000f1ce5617: function AS_Form_h3b597790b1d433aaf598000f1ce5617(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onDeviceBack defined for frmBulkPaymentsDashboard **/
    AS_Form_he4b41b8533547eeaa9fad84d541b03c: function AS_Form_he4b41b8533547eeaa9fad84d541b03c(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    }
});