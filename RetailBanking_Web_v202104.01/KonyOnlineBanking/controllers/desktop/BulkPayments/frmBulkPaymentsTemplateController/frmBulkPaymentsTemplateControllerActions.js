define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnYes **/
    AS_Button_e6227a89fd5e4389bab3617b7c421105: function AS_Button_e6227a89fd5e4389bab3617b7c421105(eventobject) {
        var self = this;
        this.btnYesTakeSurvey();
    },
    /** onClick defined for btnNo **/
    AS_Button_i806e35651e04b1cbdbad15322d6e745: function AS_Button_i806e35651e04b1cbdbad15322d6e745(eventobject) {
        var self = this;
        this.btnNoTakeSurvey();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_gf509ea55f7347ae81f70361bae22054: function AS_FlexContainer_gf509ea55f7347ae81f70361bae22054(eventobject) {
        var self = this;
        this.onFeedbackCrossClick();
    },
    /** postShow defined for frmBulkPaymentsTemplate **/
    AS_Form_d155753eabd645e88f7492b3dd7a94a1: function AS_Form_d155753eabd645e88f7492b3dd7a94a1(eventobject) {
        var self = this;
        this.onPostShow();
    },
    /** preShow defined for frmBulkPaymentsTemplate **/
    AS_Form_e0c28f49141f4cdbabcb40167881cba2: function AS_Form_e0c28f49141f4cdbabcb40167881cba2(eventobject) {
        var self = this;
        return this.onPreShow();
    },
    /** onDeviceBack defined for frmBulkPaymentsTemplate **/
    AS_Form_f61fabdce070462cb7cd8dcec31ea298: function AS_Form_f61fabdce070462cb7cd8dcec31ea298(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** init defined for frmBulkPaymentsTemplate **/
    AS_Form_gec762c980bd4e4ca24f1b609044ab5c: function AS_Form_gec762c980bd4e4ca24f1b609044ab5c(eventobject) {
        var self = this;
        this.onInit();
    },
    /** onTouchEnd defined for frmBulkPaymentsTemplate **/
    AS_Form_j550cf37c5924c338b095f3244d7b730: function AS_Form_j550cf37c5924c338b095f3244d7b730(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});