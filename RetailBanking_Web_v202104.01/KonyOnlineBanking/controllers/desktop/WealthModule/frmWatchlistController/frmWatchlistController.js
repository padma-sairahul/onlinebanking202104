define(['FormControllerUtility', 'ViewConstants', 'CommonUtilities', 'OLBConstants', 'CampaignUtility'], function(FormControllerUtility, ViewConstants, CommonUtilities, OLBConstants, CampaignUtility) {
  var orientationHandler = new OrientationHandler();
  var responsiveUtils = new ResponsiveUtils();
  return {

    //     updateFormUI: function(uiData) {
    //       if (uiData) {
    //         if (uiData.watchListDetails) {
    //          this.bindWatchlistInstruments(uiData.watchListDetails);
    //           }     
    //       }
    //     },

    init: function(){
      this.view.preShow = this.preShow;
      this.view.postShow = this.postShow;
      this.view.onBreakpointChange = this.onBreakpointChange;
      this.view.WatchlistExtended.requestStart = function() {
        FormControllerUtility.showProgressBar(this.view);
      };
      this.view.WatchlistExtended.requestEnd = function() {
        FormControllerUtility.hideProgressBar(this.view);
      };
    },

    preShow: function(){
      var navFrom = kony.application.getPreviousForm().id;
      this.view.flxBack.isVisible = navFrom === "frmPortfolioOverview" ? true : false;
      this.view.flxBack.onTouchEnd = this.goBack;
      this.view.customheader.activateMenu("Accounts", "My Accounts");
      this.view.customheader.flxAccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
    },

    postShow: function() {
      this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.frame.height - this.view.flxFooter.frame.height + "dp";
    },

    //     onSearchBtnClick: function(dataList) {
    //       var scopeObj = this;
    //       var data1 = scopeObj.getSearchData(dataList);
    //       this.bindWatchlistInstruments(data1);
    //       scopeObj.view.forceLayout();
    //     },
    //     getSearchData: function(dataList1) {
    //       var scopeObj = this;
    //       var searchQuery = scopeObj.view.WatchlistExtended.txtSear.text.trim();
    //       if (searchQuery !== "") {
    //         var data2 = dataList1;
    //         var searchresults = [];
    //         if (!kony.sdk.isNullOrUndefined(searchQuery) && searchQuery !== "") {
    //           var j = 0;
    //           for (var i = 0; i < data2.length; i++) {
    //             var rowdata = null;
    //             if ((data2[i].lblInstruName && data2[i].lblInstruName.toUpperCase().indexOf(searchQuery.toUpperCase()) !== -1) ||
    //                 (data2[i].lblISIN && data2[i].lblISIN.toUpperCase().indexOf(searchQuery.toUpperCase()) !== -1))
    //             {
    //               rowdata = data2[i];
    //             }
    //             if (kony.sdk.isNullOrUndefined(rowdata)) {
    //               data2[i].isNoRecords = true;
    //               data2[i].lblNoResultsFound = {
    //                 "text": kony.i18n.getLocalizedString("i18n.FastTransfers.NoResultsFound")
    //               };
    //               var noRecordsData = data2[i];
    //               if (data2[i].isNoRecords === false) {
    //                 searchresults[j].push(noRecordsData);
    //                 j++;
    //               }
    //             } else {
    //               searchresults[j] = rowdata;
    //               j++;
    //             }
    //           }
    //         }
    //         return searchresults;
    //       } else {
    //         return dataList1;
    //       }
    //     },
    onBreakpointChange: function(form, width){
      responsiveUtils.onOrientationChange(this.onBreakpointChange);
      this.view.customheader.onBreakpointChangeComponent(width);
      this.view.customfooter.onBreakpointChangeComponent(width);
    },     

    goBack: function() {
      var previousForm = kony.application.getPreviousForm().id;
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo(previousForm);
    }
  };

});