define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for frmWatchlist **/
    AS_Form_a24a39079c6b4e6d8c6e1086960191cb: function AS_Form_a24a39079c6b4e6d8c6e1086960191cb(eventobject) {
        var self = this;
        return self.preShow.call(this);
    },
    /** onBreakpointChange defined for frmWatchlist **/
    AS_Form_ad5ef411261f4fedb9f01c1cee73409d: function AS_Form_ad5ef411261f4fedb9f01c1cee73409d(eventobject, breakpoint) {
        var self = this;
        return self.onBreakpointChange.call(this, null, null);
    },
    /** init defined for frmWatchlist **/
    AS_Form_b3286701ed884fa8b3fac54fd9c71634: function AS_Form_b3286701ed884fa8b3fac54fd9c71634(eventobject) {
        var self = this;
        this.init();
    },
    /** postShow defined for frmWatchlist **/
    AS_Form_ed7d63e332ad4cc4a53e1bd6903fe4e3: function AS_Form_ed7d63e332ad4cc4a53e1bd6903fe4e3(eventobject) {
        var self = this;
        this.postShow();
    }
});