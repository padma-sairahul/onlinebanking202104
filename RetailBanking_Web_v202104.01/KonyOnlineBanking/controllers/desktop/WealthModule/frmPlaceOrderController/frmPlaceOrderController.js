define(['FormControllerUtility', 'ViewConstants', 'CommonUtilities', 'OLBConstants', 'CampaignUtility'], 
       function(FormControllerUtility, ViewConstants, CommonUtilities, OLBConstants, CampaignUtility) {
  var orientationHandler = new OrientationHandler();
  var responsiveUtils = new ResponsiveUtils();
  return {
    instrumentDetails: {},
    instrumentId: "",
    ricCode: "",
    favIds: "",
    favRics: "",
    instrumentDetailsQuantity: "0",
    modifyPlaceOrder: "",
    instrIsin: "",
    instrName: "",
    accountsListObj : {},
    currentInstrumentData: {},
    portfolioId: "",
    orderScreen: "",
    orderResponse: {},
    orderRequest: {},
    operation: "",
    selectedOrderType: "Limit",
    orderParams: {},
    navFromForm: "",
    portfolioDetailsData: {},
    portfolioDetails: {},
    orderFlow: "",
    getInstrumentCurrentPosition: "",
    getInstrumentPricingData: "",
    getInstrumentDetailValues: "",
    orderType: {
      'Market': 'Market', //"i18n.transfers.frequency.once",
      'Limit': 'Limit',
      'Stop Loss': 'Stop Loss',
      'Stop Limit': 'Stop Limit'
    },
    orderValidity: {
      //for backend data
      'GTD': 'Day Order', //"i18n.transfers.frequency.once",
      'GTC': 'Good Till Cancelled'
    },
    orderTypePrice: 0,
    init: function() {
    },
    updateFormUI: function(uiData) {
      if (uiData) {
        if (uiData.ProductDetails) {
          if (uiData.ProductDetails.instrumentDetails) {
            this.instrumentDetails = uiData.ProductDetails.instrumentDetails;       
          } else {
            if(uiData.ProductDetails.instrumentMinimal.length > 0)
              this.instrumentDetails = uiData.ProductDetails.instrumentMinimal[0];       
            else
              this.view.InstrumentDetailsPlaceOrder.setVisibility(false);
          }
          var currForm = kony.application.getCurrentForm();   
          this.getInstrumentDetails(this.instrumentDetails);
          this.setFavouriteStatus();
          if(uiData.ProductDetails.pricingDetails){
            this.getPricingDetails(uiData.ProductDetails.pricingDetails);
            this.view.flxRight.setVisibility(true);
            this.view.flxRight.setVisibility(true);
          }
          else{
            this.view.flxRight.setVisibility(false);
            this.view.flxRight.setVisibility(false);
          }
          if (this.modifyPlaceOrder !== "") {
            this.operation = this.modifyPlaceOrder.orderMode;
          }
          if(scope_WealthPresentationController.reload == "Reload" && scope_WealthPresentationController.instrumentAction !== "Modify")
		  this.operation = scope_WealthPresentationController.instrumentAction;
          this.setOperation();
          currForm.forceLayout();
          //avoiding resetting the quantity in case of reload
          if(scope_WealthPresentationController.reload !== "Reload"){
            this.view.lblApproximatelyVal.text = this.setApproximatelyValue("0");

            if (this.modifyPlaceOrder !== "") {
              this.populateModifyForm(this.modifyPlaceOrder);
              this.view.btnProceedPlaceOrder.text = "Modify Order";
              this.operation === "Buy" ? CommonUtilities.disableButton(this.view.InstrumentDetailsPlaceOrder.btnSell) : CommonUtilities.disableButton(this.view.InstrumentDetailsPlaceOrder.btnBuy);
            } else {
              this.view.btnProceedPlaceOrder.text = "Place Order";
              this.view.flxModifyOrder.isVisible = false;
            }
          }
          this.instrIsin = this.instrumentDetails.ISINCode;
          this.instrName = this.instrumentDetails.instrumentName;
          //          this.ricCode = uiData.ProductDetails.RICCode;
          //this.view.lblCurrentPositionAmount.text = this.setCurrentPosition();
          this.view.lblCurrentPositionAmount.text = (applicationManager.getFormatUtilManager().getCurrencySymbol(scope_WealthPresentationController.tradeCurrency)) + scope_WealthPresentationController.currentPosition + " (" + scope_WealthPresentationController.quantity +")";
          this.setUiChanges();
        }
        if (uiData.CreateOrderResponse) {
          this.orderResponse = uiData.CreateOrderResponse;
          if(orderRequest.validate_only == ""){
            this.setAknowledgementData(orderRequest);
            return;
          }
          this.orderViewSummary();
          this.view.flxOrderWarning.setVisibility(false);
          this.view.flxAccountDetails.setVisibility(false);
          this.view.flxOrderDetails.setVisibility(false);
          this.view.flxPlaceOrderButtons.setVisibility(false);
          this.view.InstrumentDetailsPlaceOrder.flxButtons.setVisibility(false);
          this.view.lblOrderDetails.setVisibility(true);
          this.view.flxSeparatorLine1.setVisibility(true);
          this.view.OrderDetailsConfirmation.skin = "slFbox";
          this.view.flxOrderDetailsConfirmation.setVisibility(true);
          this.setUiChanges();
          this.view.OrderDetailsConfirmation.Buttons.btnCancel.onClick = this.openCancelOrderPopup;
        }
        if (uiData.ModifyOrderResponse) {
          this.orderResponse = uiData.ModifyOrderResponse;
          if(orderRequest.validate_only == ""){
            this.setAknowledgementData(orderRequest);
            return;
          }
          this.orderViewSummary();
          if (this.orderFlow === "Acknowledge") {
            this.view.flxPlaceOrderContainer.setVisibility(false);
            this.view.lblPlaceOrder.text = kony.i18n.getLocalizedString("i18n.wealth.orderAcknowledgement");
            this.view.flxAcknowledgement.setVisibility(true);
          }
          if (this.orderFlow === "Confirm") {
            this.view.flxAcknowledgement.setVisibility(false);
            this.view.flxAccountDetails.setVisibility(false);
            this.view.flxOrderDetails.setVisibility(false);
            this.view.flxPlaceOrderButtons.setVisibility(false);
            this.view.InstrumentDetailsPlaceOrder.flxButtons.setVisibility(false);
            this.view.lblOrderDetails.setVisibility(true);
            this.view.flxSeparatorLine1.setVisibility(true);
            this.view.OrderDetailsConfirmation.skin = "slFbox";
            this.view.flxOrderDetailsConfirmation.setVisibility(true);
            this.setUiChanges();
            this.view.OrderDetailsConfirmation.Buttons.btnCancel.onClick = this.openCancelOrderPopup;
          }
        }
        if(uiData.CreateOrderResponseError){
          this.view.flxOrderWarning.setVisibility(true);
          if(uiData.CreateOrderResponseError.serverErrorRes.errormessage){
            this.view.lblOrderWarning.text = uiData.CreateOrderResponseError.serverErrorRes.errormessage;
          }
          else{
            this.view.lblOrderWarning.text = uiData.CreateOrderResponseError.errormessage;
          }
          CommonUtilities.disableButton(this.view.btnProceedPlaceOrder);
          this.view.flxOrderDetailsConfirmation.setVisibility(false);
          this.view.flxFormContent.setContentOffset({
            "y": "0dp"
          });
        }
        if(uiData.holdingsList){
          //  this.instrumentDetailsQuantity = uiData.holdingsList.portfolioHoldings[0].quantity;
        }
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        this.view.flxLoading.setVisibility(false);
      }
    },
    setOperation: function(){
      switch(this.operation) {
        case "Sell":
          this.view.InstrumentDetailsPlaceOrder.flxButtons.setVisibility(true);
          this.view.InstrumentDetailsPlaceOrder.btnBuy.skin = 'sknBtnSSP0273e3Border0273e3';
          this.view.InstrumentDetailsPlaceOrder.btnSell.skin = 'sknBtnSSPBg0273e3Border0273e3'; 
          this.view.flxAll.setVisibility(true)
          break;
        case "Buy":
          this.view.InstrumentDetailsPlaceOrder.flxButtons.setVisibility(true);
          this.view.InstrumentDetailsPlaceOrder.btnBuy.skin = 'sknBtnSSPBg0273e3Border0273e3';
          this.view.InstrumentDetailsPlaceOrder.btnSell.skin = 'sknBtnSSP0273e3Border0273e3';
          this.view.flxAll.setVisibility(false);
          this.view.imgActive.src = "inactive.png";
          this.view.tbxQuantity.setEnabled(true);
          this.view.tbxQuantity.skin = "sknTbxSSPffffff15PxBorder727272opa20";
          break;
        case "Modify":
          this.view.InstrumentDetailsPlaceOrder.flxButtons.setVisibility(false);
          break;
        default:
      }
      this.checkPermissions();
    },	
    changeOperation: function(widgetInfo) {
      let key = "";
      if(widgetInfo.id === undefined ||  widgetInfo.id === ""){
        key = "btn" + widgetInfo;
      }
      else{
        key = widgetInfo.id; 
      }
      this.view.tbxQuantity.text = "0";
      this.view.lblApproximatelyVal.text = this.setApproximatelyValue("0");
      switch(key) {
        case "btnBuy":
          this.operation = "Buy";
          this.setOperation();
          break;
        case "btnSell":
          this.operation = "Sell";
          this.setOperation();
          break;
        default:
      }
      scope_WealthPresentationController.instrumentAction = this.operation;
      this.checkPermissions();
    },
    populateModifyForm: function(params){
      var formUtilityMan = applicationManager.getFormatUtilManager();
      var orderRef = this.modifyPlaceOrder.orderReference;
      var instrOperation = this.modifyPlaceOrder.orderMode;
      this.operation = instrOperation;
      var orderType = this.modifyPlaceOrder.orderType;
      var limitPrice = this.modifyPlaceOrder.limitPrice === "" ? "0" : this.modifyPlaceOrder.limitPrice;
      var stopPrice = this.modifyPlaceOrder.stopPrice === "" ? "0" : this.modifyPlaceOrder.stopPrice;
      var stopLimitPrice = this.modifyPlaceOrder.stopLimitPrice === "" ? "0" : this.modifyPlaceOrder.stopLimitPrice;
      var limitPriceStopLimit = this.modifyPlaceOrder.limitPriceStopLimit === "" ? "0" : this.modifyPlaceOrder.limitPriceStopLimit;
      var stopLimitPriceRow2 = this.modifyPlaceOrder.stopLimitPriceRow2 === "" ? "0" : this.modifyPlaceOrder.stopLimitPriceRow2;
      var orderFees = this.modifyPlaceOrder.fees;
      var orderModeType = this.modifyPlaceOrder.orderModeType;
      var orderQuant = this.modifyPlaceOrder.quantity;
      var orderStatus = this.modifyPlaceOrder.status;
      var orderTradeDate = this.modifyPlaceOrder.tradeDate;
      var orderValidity = this.modifyPlaceOrder.validity;
      var orderValueDate = this.modifyPlaceOrder.valueDate;
      var currency = this.modifyPlaceOrder.instrumentCurrency;
      this.selectedOrderType = this.modifyPlaceOrder.orderModeType;
      if(limitPrice === "" || limitPrice === "0") 
      {
        limitPrice = this.modifyPlaceOrder.price;
      }
      // set flxModifyOrder to visible
      this.view.flxModifyOrder.isVisible = true;
      // change page title
      this.view.lblPlaceOrder.text = "Modify Order";
      // assign values from order
      this.view.tbxQuantity.text = orderQuant;
      
      this.view.lbxValidity.selectedKey = orderValidity === "Day Order"?this.view.lbxValidity.masterData[0][0]:this.view.lbxValidity.masterData[1][0];
      this.view.lbxOrderType.selectedKey = orderModeType;    
      if (orderModeType === "Stop Limit") {
        this.view.tbxLimitPriceStopLimit.text = formUtilityMan.formatAmount(limitPrice);
        this.view.tbxStopLimitPrice.text = formUtilityMan.formatAmount(stopPrice);
        this.view.flxRow3.setVisibility(true);
        this.view.flxLimitPrice.setVisibility(false);
        this.view.flxStopLimitPriceRow2.setVisibility(false);
      } else if (orderModeType === "Stop Loss") {
        this.view.tbxStopLimitPriceRow2.text = formUtilityMan.formatAmount(stopPrice);
        this.view.flxRow3.setVisibility(false);
        this.view.flxLimitPrice.setVisibility(false);
        this.view.flxStopLimitPriceRow2.setVisibility(true);
      } else if (orderModeType === "Limit") {
        this.view.tbxLimitPrice.text = formUtilityMan.formatAmount(limitPrice);
        this.view.flxRow3.setVisibility(false);
        this.view.flxLimitPrice.setVisibility(true);
        this.view.flxStopLimitPriceRow2.setVisibility(false);
      }
      else{
        this.view.flxRow3.setVisibility(false);
        this.view.flxLimitPrice.setVisibility(false);
        this.view.flxStopLimitPriceRow2.setVisibility(false);
      }
      //this.view.lbxValidity.selectedKey = orderValidity; // to be enabled after Transact integration
      this.view.lblOrderRef.text = "Order Ref  " + orderRef + "  from  " + orderTradeDate;
      this.setApproximatelyValue(orderQuant);
      this.view.lblApproximatelyVal.text = this.setApproximatelyValue(orderQuant);
      this.checkValidityPaceOrderForm();
    },
    preShow: function() {
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      this.view.flxLoading.setVisibility(true);
      this.instrumentId = "";
      let scopeObj = this;
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            }
      this.getInstrumentCurrentPosition = applicationManager.getModulesPresentationController("WealthModule").getInstrumentCurrentPosition();
      this.getInstrumentPricingData = applicationManager.getModulesPresentationController("WealthModule").getInstrumentPricingData();
      this.getInstrumentDetailValues = applicationManager.getModulesPresentationController("WealthModule").getInstrumentDetails();
      if(scope_WealthPresentationController.isJointAccount == true){
        this.view.btnProceedPlaceOrder.setVisibility(false);
      }
      else{
        this.view.btnProceedPlaceOrder.setVisibility(true);
          }
      this.checkPermissions();
      this.view.InstrumentDetailsPlaceOrder.imgRefresh.onTouchEnd = this.reloadProductDetails.bind();
      this.view.accountActionsMenu.setVisibility(false);
      this.view.flx3Dot.skin = "slFbox";
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      this.portfolioId = wealthModule.getPortfolioId();
      if(Object.keys(this.getInstrumentPricingData).length > 0 || Object.keys(this.getInstrumentDetailValues).length > 0 || Object.keys(this.getInstrumentCurrentPosition).length > 0){
        this.view.flxRight.setVisibility(true);
        this.instrumentDetails = this.getInstrumentDetailValues;
          var currForm = kony.application.getCurrentForm();   
          this.getInstrumentDetails(this.instrumentDetails);
 //         this.setFavouriteStatus();
          if(Object.keys(this.getInstrumentPricingData).length > 0){
            this.getPricingDetails(this.getInstrumentPricingData);
            this.view.flxRight.setVisibility(true);
          }
          else{
            this.view.flxRight.setVisibility(false);
          }
          if (this.modifyPlaceOrder !== "") {
            this.operation = this.modifyPlaceOrder.orderMode;
          }
          this.setOperation();
          currForm.forceLayout();
          this.view.lblApproximatelyVal.text = this.setApproximatelyValue("0");
          if (this.modifyPlaceOrder !== "") {
             this.populateModifyForm(this.modifyPlaceOrder);
             this.view.btnProceedPlaceOrder.text = "Modify Order";
            this.operation === "Buy" ? CommonUtilities.disableButton(this.view.InstrumentDetailsPlaceOrder.btnSell) : CommonUtilities.disableButton(this.view.InstrumentDetailsPlaceOrder.btnBuy);
          } else {
             this.view.btnProceedPlaceOrder.text = "Place Order";
            this.view.flxModifyOrder.isVisible = false;
            CommonUtilities.enableButton(this.view.InstrumentDetailsPlaceOrder.btnSell);
            CommonUtilities.enableButton(this.view.InstrumentDetailsPlaceOrder.btnBuy);
            this.setOperation();
          }
          this.instrIsin = this.instrumentDetails.ISINCode;
          this.instrName = this.instrumentDetails.instrumentName;
//          this.ricCode = uiData.ProductDetails.RICCode;
          this.view.lblCurrentPositionAmount.text = this.setCurrentPosition();
          //this.view.lblCurrentPositionAmount.text = scope_WealthPresentationController.currentPosition + " (" + scope_WealthPresentationController.quantity +")";
          this.setUiChanges();
        this.view.flxLoading.setVisibility(false);
      } else
        this.view.flxRight.setVisibility(false);
      this.getProductDetails();
      this.accountsListObj = wealthModule.getAccountsListObj();
      var navManager = applicationManager.getNavigationManager();		
      var dataCustom = navManager.getCustomInfo('frmPlaceOrder');
      this.currentInstrumentData = dataCustom;
      var forUtility = applicationManager.getFormatUtilManager();
      var currentCurrency;
      if(dataCustom.holdingsData !== undefined && dataCustom.holdingsData!== ""){
        var currencyCode = dataCustom.holdingsData.instrumentCurrency || dataCustom.holdingsData.secCCy;
        currentCurrency = forUtility.getCurrencySymbol(currencyCode);
      }
      else
        if(this.navFromForm == "frmWatchlist")
          currentCurrency = forUtility.getCurrencySymbol(scope_WealthPresentationController.tradeCurrency);
      else
        currentCurrency = forUtility.getCurrencySymbol(dataCustom.secCCy);
      this.view.lblStopLimitPriceCurrency.text = currentCurrency;
      this.view.lblStopLimitOrderCurrency.text = currentCurrency;
      this.view.lblLimitPriceCurrency.text = currentCurrency;
      this.view.lblLimitPriceCurrencyRow2.text = currentCurrency;
      this.view.OrderDetailsConfirmation.Buttons.btnConfirm.onClick = this.goToAcknowledgement;
      this.view.btnProceedPlaceOrder.onClick = this.goToOrderConfirmation;
      this.view.flxPlaceOrderContainer.setVisibility(true);
      this.view.flxAcknowledgement.setVisibility(false);
      this.view.lblPlaceOrder.text = kony.i18n.getLocalizedString("i18n.wealth.placeOrder");      
      this.view.flxLoadingIndicatorFrom.setVisibility(true);
      this.getCashBalance();
      this.view.imgActive.onTouchEnd = this.onClickSelectAll;
      this.view.btnCancelPlaceOrder.onClick = this.goBack;
      this.view.OrderDetailsConfirmation.Buttons.btnModify.onClick = this.modifyOrderView;
      this.view.flx3Dot.onClick = this.toggleAccountActionsMenu;
      this.view.flxImgInfoIcon.onClick = this.onInfoValidityClick;
      this.view.flxOrderImgInfoIcon.onClick = this.onInfoOrderTypeClick;
      this.view.lbxOrderType.masterData = this.getOrderTypes();
      //this.view.lbxOrderType.selectedKey = this.selectedOrderType;
      this.view.lbxOrderType.selectedKey = this.view.lbxOrderType.masterData[1][1];
      this.view.lbxOrderType.onSelection = this.onOrderTypeSelected;
      this.onOrderTypeSelected(this.view.lbxOrderType);
      this.view.lbxValidity.masterData = this.getOrderValidity();
      this.view.lbxValidity.selectedKey = this.view.lbxValidity.masterData[0][0];
      this.view.lbxOrderAccountsList.masterData = this.getOrderAccountsList(this.accountsListObj);
      var navFrom = applicationManager.getNavigationManager().getPreviousForm();
      this.view.lbxOrderAccountsList.selectedKey = this.portfolioId;  
      this.view.lbxOrderAccountsList.onSelection = this.onOrderAccountSelected;
      scope_WealthPresentationController.instrumentAction === "Sell" ? this.view.flxAll.setVisibility(true) : this.view.flxAll.setVisibility(false);
      this.view.flxAccountDetails.setVisibility(true);
      this.view.flxOrderDetails.setVisibility(true);
      this.view.flxPlaceOrderButtons.setVisibility(true);
      this.view.InstrumentDetailsPlaceOrder.flxButtons.setVisibility(true);
      this.view.lblOrderDetails.setVisibility(false);
      this.view.flxSeparatorLine1.setVisibility(false);
      this.view.flxOrderDetailsConfirmation.setVisibility(false);
      //this.view.CancelOrder.btnYes.onClick = this.goBack;
      this.view.CancelOrder.btnYes.onClick = this.cancelTheOrder;
      this.view.btnCancelTransfer.onClick = this.goBack;
      this.view.btnViewOrder.onClick = this.viewOrders;
      this.operation = dataCustom.operation;
      this.changeOperation(dataCustom.operation);
      this.view.InstrumentDetailsPlaceOrder.btnBuy.onClick = this.changeOperation.bind(this);
      this.view.InstrumentDetailsPlaceOrder.btnSell.onClick = this.changeOperation.bind(this);
      this.view.flxDialogCancelOrder.setVisibility(false);
      this.view.imgActive.src = "inactive.png";
      this.view.tbxQuantity.setEnabled(true);
      this.view.tbxQuantity.skin = "sknTbxSSPffffff15PxBorder727272opa20";
      this.view.tbxQuantity.onTextChange = this.onQuantityChange;
      this.view.flxOrderWarning.setVisibility(false);
      this.restrictSpecialCharacters();
      FormControllerUtility.wrapAmountField(this.view.tbxLimitPrice).onKeyUp(this.checkValidityPaceOrderForm);
      FormControllerUtility.wrapAmountField(this.view.tbxStopLimitPriceRow2).onKeyUp(this.checkValidityPaceOrderForm);
      FormControllerUtility.wrapAmountField(this.view.tbxLimitPriceStopLimit).onKeyUp(this.checkValidityPaceOrderForm);
      FormControllerUtility.wrapAmountField(this.view.tbxStopLimitPrice).onKeyUp(this.checkValidityPaceOrderForm);
      this.view.lbxValidity.onSelection = this.checkValidityPaceOrderForm;
      this.view.tbxLimitPrice.onTextChange = this.onQuantityChange;
      this.view.tbxStopLimitPrice.onTextChange = this.onQuantityChange;
      this.view.tbxStopLimitPrice.onTextChange = this.onQuantityChange;
      this.view.tbxStopLimitPriceRow2.onTextChange = this.onQuantityChange;
      this.portfolioDetailsData = applicationManager.getNavigationManager().getCustomInfo('frmPlaceOrder');
//      this.portfolioDetails.RICCode = this.portfolioDetailsData.RICCode;
      this.instrIsin = this.portfolioDetailsData.ISIN;
      this.orderFlow = "";
      if(this.view.InstrumentDetailsPlaceOrder.imgStar.isVisible){        
        this.getFavouriteStatus();       
        this.view.InstrumentDetailsPlaceOrder.imgStar.onTouchEnd = this.updateFavList.bind();
      }
      applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    postShow: function() {
      this.setUiChanges();
    },
    onInfoOrderTypeClick: function(){
      this.hideAllPopups();
      this.view.imgOrderInfoIcon.src = "bluealert_2.png";
      this.view.flxInfoIconOrderType.setVisibility(true);
      this.view.InfoIconOrderType.flxCross.onClick = this.closeOrderTypePopup;
    },
    onInfoValidityClick: function(){
      this.hideAllPopups();
      this.view.imgInfoIcon.src = "bluealert_2.png";
      this.view.flxInfoIconValidity.setVisibility(true);
      this.view.InfoIconValidity.flxCross.onClick = this.closeValidityPopup;
    },
    closeOrderTypePopup: function(){
      this.view.flxInfoIconOrderType.setVisibility(false);
      this.view.imgOrderInfoIcon.src = "info_grey.png";
    },
    closeValidityPopup: function(){
      this.view.flxInfoIconValidity.setVisibility(false);
      this.view.imgInfoIcon.src = "info_grey.png";
    },
    toggleAccountActionsMenu: function() {
      this.closeOrderTypePopup();
      this.closeValidityPopup();
      if (this.view.accountActionsMenu.isVisible === false) {
        this.view.accountActionsMenu.setVisibility(true);
        this.view.flx3Dot.skin = "sknFlxOlb3DotRadiusf7f7f7";
      } else {
        this.view.accountActionsMenu.setVisibility(false);
        this.view.flx3Dot.skin = "slFbox";
      }
    },
    getProductDetails: function(){
      this.navFromForm = applicationManager.getNavigationManager().getPreviousForm();
      var navManager = applicationManager.getNavigationManager();
      var dataCustom = navManager.getCustomInfo('frmPlaceOrder');
      if (dataCustom.holdingsData && dataCustom.holdingsData.operation === "Modify") {
        this.modifyPlaceOrder = dataCustom.holdingsData;
        this.getInstrumentPricingData = {};
        this.getInstrumentDetailValues = {};
        this.getInstrumentCurrentPosition = {};
      } else {
        this.modifyPlaceOrder = "";
      }
      this.operation = dataCustom.operation;
      scope_WealthPresentationController.instrumentAction = this.operation;
      if(dataCustom.holdingsData !== undefined){
        scope_WealthPresentationController.tradeCurrency = dataCustom.holdingsData.secCCy;
        scope_WealthPresentationController.currentPosition = dataCustom.holdingsData.marketValPOS;
        scope_WealthPresentationController.quantity = dataCustom.holdingsData.quantity;
        scope_WealthPresentationController.instrumentId = dataCustom.holdingsData.instrumentId;
		scope_WealthPresentationController.holdingsType = dataCustom.holdingsData.ISIN;
      }
      if(this.navFromForm == "frmWatchlist")
       scope_WealthPresentationController.tradeCurrency = scope_WealthPresentationController.selectedInstrDetails.lblCurrency;
      if(Object.keys(this.getInstrumentPricingData).length > 0 || Object.keys(this.getInstrumentDetailValues).length > 0 || Object.keys(this.getInstrumentCurrentPosition).length > 0){
      }
      else{
      var data = {
        "searchByInstrumentName": (dataCustom.searchByInstrumentName !== undefined)?dataCustom.searchByInstrumentName: dataCustom.description,
        "portfolioId": this.portfolioId,
        "sortBy": "",
        "navPage": "Holdings"
      };
      var navFrom = applicationManager.getNavigationManager().getPreviousForm();
      this.navFromForm = navFrom;
      data.instrumentId = scope_WealthPresentationController.instrumentId;
	  data.ISIN = scope_WealthPresentationController.holdingsType;
      var navData = this.navFromForm === "frmWatchlist" ? dataCustom : data;
      applicationManager.getModulesPresentationController("WealthModule").getHoldingsCurrentPosition(navData);
      //applicationManager.getModulesPresentationController("WealthModule").getHoldings(navData);
      }
      },
    getCashBalance: function() {
      this.view.lblAmount.setVisibility(false);
      this.view.flxLoadingIndicatorFrom.setVisibility(true);
      var params = {
        "portfolioId":  this.portfolioId
      };
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.getCashBalanceDetails(params);
    },
    setCashBalance: function(data) {
      this.view.lblAmount.text = scope_WealthPresentationController.cashBalance;
      this.view.lblAmount.setVisibility(true);
      this.view.flxLoadingIndicatorFrom.setVisibility(false);
    },
    onBreakpointChange: function(form, width){
      responsiveUtils.onOrientationChange(this.onBreakpointChange, function() {
        if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
          if((this.orderFlow === "Acknowledge") && (orderRequest !== "")){
            this.setAknowledgementData(orderRequest);
          }
        }
      }.bind(this));
      this.setUiChanges();
      if((this.orderFlow === "Acknowledge") && (orderRequest !== "")){
        this.setAknowledgementData(orderRequest);
      }
    },
    getPricingDetails: function(prices) {
      //this.view.PricingDataPlaceOrder.lblCurrency.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      this.view.PricingDataPlaceOrder.lblCode.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.bidRate, prices.referenceCurrency);
      this.view.PricingDataPlaceOrder.lblBidValue.text = ((prices.bidVolume) ? prices.bidVolume : "").toString();
      //this.view.PricingDataPlaceOrder.lblCur.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      //this.view.PricingDataPlaceOrder.lblCurT.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      this.view.PricingDataPlaceOrder.lblVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.askRate, prices.referenceCurrency);
      this.view.PricingDataPlaceOrder.lblValT.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.askRate, prices.referenceCurrency);
      this.view.PricingDataPlaceOrder.lblAskVal.text = ((prices.askVolume) ? prices.askVolume : "").toString();
      this.view.PricingDataPlaceOrder.lblAskValT.text = ((prices.askVolume) ? prices.askVolume : "").toString();
      this.view.PricingDataPlaceOrder.lblVolVal.text = ((prices.volume) ? prices.volume : "").toString();
      //this.view.PricingDataPlaceOrder.lblCurr.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      this.view.PricingDataPlaceOrder.lblOpenVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.openRate, prices.referenceCurrency);
      //this.view.PricingDataPlaceOrder.lblCurre.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      this.view.PricingDataPlaceOrder.lblCloseVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.closeRate, prices.referenceCurrency);
      //this.view.PricingDataPlaceOrder.lblCurVal.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      //this.view.PricingDataPlaceOrder.lblCurValT.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      this.view.PricingDataPlaceOrder.lblHighVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.high52W, prices.referenceCurrency);
      this.view.PricingDataPlaceOrder.lblHighValT.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.high52W, prices.referenceCurrency);
      //this.view.PricingDataPlaceOrder.lblLowCur.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      this.view.PricingDataPlaceOrder.lblLowVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.low52W, prices.referenceCurrency);
      //this.view.PricingDataPlaceOrder.lblLatCur.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      //this.view.PricingDataPlaceOrder.lblLatCurT.text = ((prices.referenceCurrency) ? prices.referenceCurrency : "");
      this.view.PricingDataPlaceOrder.lblLatestVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.latestRate, prices.referenceCurrency);
      this.view.PricingDataPlaceOrder.lblLatestValT.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.latestRate, prices.referenceCurrency);
    },
    getInstrumentDetails: function(instrument) {
      this.instrumentId = instrument.instrumentId;
       if(!kony.sdk.isNullOrUndefined(this.instrumentId)){
        scope_WealthPresentationController.instrumentId = this.instrumentId;
      }
      if((this.instrumentId == undefined || this.instrumentId == "") && (!kony.sdk.isNullOrUndefined(this.getInstrumentCurrentPosition.holdingsId))){
        this.instrumentId = this.getInstrumentCurrentPosition.holdingsId;
        scope_WealthPresentationController.instrumentId = this.instrumentId;
      }
      else
        this.instrumentId = scope_WealthPresentationController.instrumentId;	
      this.ricCode = instrument.RICCode;
      var instrumentDetailsDate = {
        "timeReceived": ((instrument.timeReceived) ? instrument.timeReceived : "00:00:00"),
        "dateReceived": ((instrument.lastPriceDate || instrument.dateReceived) ? instrument.lastPriceDate || instrument.dateReceived : "")
      };
      // instrIsin = instrument.ISINCode;
      this.view.InstrumentDetailsPlaceOrder.lblCorp.text = ((instrument.instrumentName)?instrument.instrumentName:"");
      this.view.InstrumentDetailsPlaceOrder.lblCode.text = ((instrument.ISINCode)?instrument.ISINCode:"") + ((instrument.stockExchange)?" | " + instrument.stockExchange:"");
      //this.view.InstrumentDetailsPlaceOrder.lblCurrency.text = applicationManager.getFormatUtilManager().getCurrencySymbol(((instrument.referenceCurrency)?instrument.referenceCurrency:""));
      this.view.InstrumentDetailsPlaceOrder.lblCurrency.text = applicationManager.getFormatUtilManager().getCurrencySymbol(scope_WealthPresentationController.tradeCurrency);
      if (instrument.lastPrice === "0.0") {
        this.view.InstrumentDetailsPlaceOrder.lblAmount.text = applicationManager.getFormatUtilManager().formatAmount(instrument.closeRate);
      } else {
        this.view.InstrumentDetailsPlaceOrder.lblAmount.text = applicationManager.getFormatUtilManager().formatAmount(instrument.lastPrice);
      }
      this.view.InstrumentDetailsPlaceOrder.lblProfit.text = ((instrument.netChange)?instrument.netChange:"") + ((instrument.percentageChange)?" (" + instrument.percentageChange + "%)":"");
      if(Object.keys(this.getInstrumentDetailValues).length > 0)
        this.view.InstrumentDetailsPlaceOrder.lblTime.text = this.getInstrumentDetailValues.asOfdateTime;
      else
        this.view.InstrumentDetailsPlaceOrder.lblTime.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf")+ " " +this.setInstrumentDate(instrumentDetailsDate);
      if (parseFloat(instrument.percentageChange) > 0) {
        this.view.InstrumentDetailsPlaceOrder.lblProfit.skin = "sknIW2F8523";
      }
      else {
        this.view.InstrumentDetailsPlaceOrder.lblProfit.skin = "sknlblff000015px";
      }
    },
    setInstrumentDate:function(instrumentDate){
  
      let month = instrumentDate.dateReceived.substring(3,6);
      let day = instrumentDate.dateReceived.substring(0,2);
      let year = instrumentDate.dateReceived.substring(7,12);
      var dateFormat = "";
      let hour = instrumentDate.timeReceived.substring(0,2);
      let min = instrumentDate.timeReceived.substring(3,5);
      
      let firstPart = applicationManager.getFormatUtilManager().getTwelveHourTimeString(hour+': '+min);          
      let trdPart = month + ' ' + day;

      return firstPart + ' UTC ' + trdPart;

    },

    
    goToOrderConfirmation: function(){
      // to remove hardcoded params
      this.setPlaceOrderFormData();
      var order;
      var params;
      var limitPrice;
      var price;
      if(this.modifyPlaceOrder.orderReference !== undefined){
        if(this.view.lbxOrderType.selectedKey == "Stop Limit"){
          order = "STOP.LIMIT";
          limitPrice = applicationManager.getFormatUtilManager().deFormatAmount(this.view.tbxLimitPriceStopLimit.text);
          price = applicationManager.getFormatUtilManager().deFormatAmount(this.view.tbxStopLimitPrice.text);
        }
        else if(this.view.lbxOrderType.selectedKey == "Stop Loss"){
          order = "STOP";
          limitPrice = "";
          price = applicationManager.getFormatUtilManager().deFormatAmount(this.view.tbxStopLimitPriceRow2.text);
        }
        else if(this.view.lbxOrderType.selectedKey == "Limit"){
          order = "LIMIT";
          limitPrice = applicationManager.getFormatUtilManager().deFormatAmount(this.view.tbxLimitPrice.text);
          price = "";
        }
        else{
          order = "MARKET";
          limitPrice = "";
          price = "";
        }
        params = {
          "quantity": this.view.tbxQuantity.text,  
          "orderType": order,
          "limitPrice": limitPrice,
          "price": price,
          "orderId":this.modifyPlaceOrder.orderReference,
          "validate_only":true,
          "validity": this.view.lbxValidity.selectedKeyValue[0]
        };
        this.modifyMarketOrder(params);
      }
      else{
      var userManager = applicationManager.getUserPreferencesManager();
          var custId = userManager.getBackendIdentifier();
			if (custId === "") {
				if(userManager.primaryCustomerId.id === "" || userManager.primaryCustomerId.id === undefined) {
					custId = userManager.accessibleCustomerIds[0].id;
				} else {
					custId = userManager.primaryCustomerId.id;
				}
			}
      if(scope_WealthPresentationController.instrumentAction == "Sell"){
          order = scope_WealthPresentationController.instrumentAction.substr(0,3).toUpperCase();
      }
      else{
          order = scope_WealthPresentationController.instrumentAction.toUpperCase();
      }
      params = {    
        "portfolioId": this.portfolioId,
       // "ISINCode": this.instrIsin,
        "instrumentId": scope_WealthPresentationController.instrumentId,
        "customerId": custId,
        "stockExchange": scope_WealthPresentationController.holdingsType,
        "tradeCurrency": scope_WealthPresentationController.tradeCurrency,
        "description": this.instrName,
        "limitPrice": orderParams.limitPrice,
        "marketPrice": orderParams.marketPrice,
        "netAmount": orderParams.netAmount,
        "order": order,
        "orderType": orderParams.orderType.toUpperCase(),
        "quantity": orderParams.quantity,
        "stopLoss": 0,
        "stopPrice": orderParams.stopLimitPrice,
        "validity": orderParams.validity,
        "price":orderParams.stopLimitPrice,
        "dealStatus":"TRANSMITTED",
        "validate_only": true
      };
      if(orderParams.orderType == "Stop Loss"){
        params.price = orderParams.stopLimitPriceRow2;
      }
      if(orderParams.orderType == "Stop Limit"){
        params.limitPrice = orderParams.limitPriceStopLimit;
      }
      this.createMarketOrder(params);
      }
      orderRequest = params;
      this.orderFlow = "Confirm";
 },
    setUiChanges: function() {
      var currBreakpoint = kony.application.getCurrentBreakpoint();
      if (currBreakpoint === 1024 && this.view.flxOrderDetailsConfirmation.isVisible) {
        this.view.flxPlaceOrderContainer.height = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "984dp" : "940dp";
        this.view.flxRight.top = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "744dp" : "706dp";
      } else if (currBreakpoint === 1024) { 
        if (this.view.flxModifyOrder.isVisible) {
          this.view.flxPlaceOrderContainer.height = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "1110dp" : "1026dp";
          this.view.flxRight.top = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "870dp" : "794dp";
          this.view.flxInfoIconOrderType.bottom = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "630dp" : "546dp";
          this.view.flxInfoIconValidity.bottom = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "470dp" : "470dp";
        } else {
          this.view.flxPlaceOrderContainer.height = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "1070dp" : "986dp";
          this.view.flxRight.top = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "830dp" : "754dp";
          this.view.flxInfoIconOrderType.bottom = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "630dp" : "546dp";
          this.view.flxInfoIconValidity.bottom = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "470dp" : "470dp";
        }
      } else if (currBreakpoint !== -1) {               
        if (this.view.flxOrderDetailsConfirmation.isVisible) {
          this.view.flxPlaceOrderContainer.height = this.view.lbxOrderType.selectedKey === "Stop Limit" ? "740dp" : "700dp";
        } else {
          if (this.view.lbxOrderType.selectedKey === "Stop Limit") {
            this.view.flxPlaceOrderContainer.height = this.view.flxModifyOrder.isVisible ? "840dp" : "800dp";
            this.view.flxInfoIconOrderType.bottom = "358dp";
            this.view.flxInfoIconValidity.bottom = "196dp";
          } else {
            this.view.flxPlaceOrderContainer.height = this.view.flxModifyOrder.isVisible ? "800dp" : "760dp";
            this.view.flxInfoIconOrderType.bottom = "316dp";
            this.view.flxInfoIconValidity.bottom = "236dp";
          }
        }
      }
    },
    openCancelOrderPopup: function(){
      this.view.CancelOrder.centerY = "50%";
      this.view.flxDialogCancelOrder.setVisibility(true);
      this.view.CancelOrder.btnNo.onClick = this.onCancelInPopup;
      this.view.CancelOrder.flxCross.onClick = this.onCancelInPopup;
    },
    onCancelInPopup: function(){
      this.view.flxDialogCancelOrder.setVisibility(false);
    },
    goToAcknowledgement: function(){
      var params = orderRequest;
      params.validate_only = "";
      if(this.modifyPlaceOrder.orderReference !== undefined){
      this.modifyMarketOrder(params);
      }
      else{
      this.createMarketOrder(params);
      }
      orderRequest = params;
      // orderRequest = params;
      this.setAknowledgementData(params);
      this.orderFlow = "Acknowledge";
      //      this.view.flxPlaceOrderContainer.setVisibility(false);
      //      this.view.lblPlaceOrder.text = kony.i18n.getLocalizedString("i18n.wealth.orderAcknowledgement");
      //      this.view.flxAcknowledgement.setVisibility(true);
    },
    setAknowledgementData: function(params) {
      var formUtilityMan = applicationManager.getFormatUtilManager();
      this.view.acknowledgmentMyRequests.lblBalanceValue.text = scope_WealthPresentationController.cashBalance;
      if (this.orderResponse && this.orderResponse.status === "success") {
        this.view.flxPlaceOrderContainer.setVisibility(false);
        this.view.lblPlaceOrder.text = kony.i18n.getLocalizedString("i18n.wealth.orderAcknowledgement");
        this.view.flxAcknowledgement.setVisibility(true);
        this.view.AcknowledgementDetails.lblAccountValue.text = this.orderResponse.id;
        this.view.AcknowledgementDetails.lblChequeBooksValue.text = this.view.OrderDetailsConfirmation.lblOrderValue.text;
        this.view.AcknowledgementDetails.lblFeeValue.text = params.quantity;
        this.view.AcknowledgementDetails.lblDeliveryTypeValue.text = this.view.OrderDetailsConfirmation.lblOrderTypeValue.text;

        this.view.AcknowledgementDetails.lblNotesValue.text = this.view.lbxValidity.selectedKeyValue[1];
        this.view.AcknowledgementDetails.lblExtra1Value.text = this.view.OrderDetailsConfirmation.lblTotalOrderAmountValue.text;
        this.view.AcknowledgementDetails.lblExtra2Value.text = formUtilityMan.formatAmountandAppendCurrencySymbol(this.orderResponse.fees, this.currentInstrumentData.currency);
        if(this.view.AcknowledgementDetails.lblDeliveryTypeValue.text === "Stop Limit"){
          this.view.AcknowledgementDetails.lblAddressValue.text = CommonUtilities.formatCurrencyWithCommas(this.view.tbxStopLimitPrice.text, false, this.view.InstrumentDetailsPlaceOrder.lblCurrency.text);
          this.view.AcknowledgementDetails.lblExtra3Value.text = CommonUtilities.formatCurrencyWithCommas(this.view.tbxLimitPriceStopLimit.text, false, this.view.InstrumentDetailsPlaceOrder.lblCurrency.text);
        } else if (this.view.AcknowledgementDetails.lblDeliveryTypeValue.text === "Stop Loss"){					
          this.view.AcknowledgementDetails.lblAddressValue.text = CommonUtilities.formatCurrencyWithCommas(this.view.tbxStopLimitPriceRow2.text, false, this.view.InstrumentDetailsPlaceOrder.lblCurrency.text);
        } else if (this.view.AcknowledgementDetails.lblDeliveryTypeValue.text === "Limit"){
          this.view.AcknowledgementDetails.lblAddressValue.text = CommonUtilities.formatCurrencyWithCommas(this.view.tbxLimitPrice.text, false, this.view.InstrumentDetailsPlaceOrder.lblCurrency.text);
        } else {
          this.view.AcknowledgementDetails.lblAddressValue.text = formUtilityMan.formatAmountandAppendCurrencySymbol(params.marketPrice, this.currentInstrumentData.currency);
        }
      }
    },
    /** sets the order type with i18 value
         */
    getOrderTypes: function() {
      var list = [];
      for (var key in this.orderType) {
        if (this.orderType.hasOwnProperty(key)) {
          //list.push([key, kony.i18n.getLocalizedString(this.orderType[key])]);
          list.push([key, this.orderType[key]]);
        }
      }
      return list;
    },
    /** sets the order type with i18 value
         */
    getOrderValidity: function() {
      var list = [];
      for (var key in this.orderValidity) {
        if (this.orderValidity.hasOwnProperty(key)) {
          //list.push([key, kony.i18n.getLocalizedString(this.orderValidity[key])]);
          list.push([key, this.orderValidity[key]]);
        }
      }
      return list;
    },
    getOrderAccountsList: function(accountList) {      
      var data=accountList;
      var currForm = kony.application.getCurrentForm();
      var segdata = [];
      for (var list in data){			    
        segdata.push([ data[list].portfolioId, CommonUtilities.truncateStringWithGivenLength(data[list].accountName + "....", 26) + CommonUtilities.getLastFourDigit(data[list].accountNumber)]);
      }
      return segdata;
    },
    onOrderAccountSelected: function(data) {
      this.portfolioId = data.selectedKey;
      this.getProductDetails();
      this.getCashBalance();
    },
    onOrderTypeSelected: function(data) {
      var quantityText = this.view.tbxQuantity.text || "0";
      if (data.selectedKey === "Stop Limit") {
        this.setApproximatelyValue(quantityText);
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue(quantityText);
        this.view.tbxLimitPriceStopLimit.text = "";
        this.view.tbxStopLimitPrice.text = "";
        this.selectedOrderType = "Stop Limit";
        this.view.flxRow3.setVisibility(true);
        this.view.flxLimitPrice.setVisibility(false);
        this.view.flxStopLimitPriceRow2.setVisibility(false);
        this.view.AcknowledgementDetails.flxExtra3.setVisibility(true);
        this.view.AcknowledgementDetails.lblExtra3.text = "Limit Price:";
        this.view.AcknowledgementDetails.lblAddress.text = "Stop Price:";
      } else if (data.selectedKey === "Stop Loss") {
        this.setApproximatelyValue(quantityText);
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue(quantityText);
        this.view.tbxStopLimitPriceRow2.text = "";
        this.selectedOrderType = "Stop Loss";
        this.view.flxRow3.setVisibility(false);
        this.view.flxLimitPrice.setVisibility(false);
        this.view.flxStopLimitPriceRow2.setVisibility(true);
        this.view.AcknowledgementDetails.flxExtra3.setVisibility(false);
        this.view.AcknowledgementDetails.lblAddress.text = "Stop Price:";
      } else if (data.selectedKey === "Limit") {
        this.setApproximatelyValue(quantityText);
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue(quantityText);
        this.view.tbxLimitPrice.text = "";
        this.selectedOrderType = "Limit";
        this.view.flxRow3.setVisibility(false);
        this.view.flxLimitPrice.setVisibility(true);
        this.view.flxStopLimitPriceRow2.setVisibility(false);
        this.view.AcknowledgementDetails.flxExtra3.setVisibility(false);
        this.view.AcknowledgementDetails.lblAddress.text = "Limit Price:";
      } else {
        this.setApproximatelyValue(quantityText);
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue(quantityText);
        this.selectedOrderType = "Market";
        this.view.tbxLimitPriceStopLimit.text = "";
        this.view.tbxStopLimitPrice.text = "";
        this.view.tbxStopLimitPriceRow2.text = "";
        this.view.tbxLimitPrice.text = "";
        this.view.flxRow3.setVisibility(false);
        this.view.flxLimitPrice.setVisibility(false);
        this.view.flxStopLimitPriceRow2.setVisibility(false);
        this.view.AcknowledgementDetails.flxExtra3.setVisibility(false);
        this.view.AcknowledgementDetails.lblAddress.text = "Market Price:";
      }
      this.checkValidityPaceOrderForm();
      this.setUiChanges();
      this.onQuantityChange();
    },
        onModifySelected: function(data) {
          var quantityText = this.view.tbxQuantity.text || "0";
      if (data.orderType === "Stop Limit") {
        this.view.tbxQuantity.text = data.quantity;
        this.view.tbxLimitPriceStopLimit.text = data.limitPriceStopLimit;
        this.view.tbxStopLimitPrice.text = data.stopLimitPrice;
        this.view.lbxOrderType.selectedKey = "Stop Limit";		
        this.setApproximatelyValue(quantityText);
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue(quantityText);
        this.view.flxRow3.setVisibility(true);
        this.view.flxLimitPrice.setVisibility(false);
        this.view.flxStopLimitPriceRow2.setVisibility(false);
      } else if (data.orderType === "Stop Loss") {
        this.view.tbxQuantity.text = data.quantity;
        this.view.tbxStopLimitPriceRow2.text = data.stopLimitPriceRow2;
        this.view.lbxOrderType.selectedKey = "Stop Loss";
        this.setApproximatelyValue(quantityText);
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue(quantityText);
        this.view.flxRow3.setVisibility(false);
        this.view.flxLimitPrice.setVisibility(false);
        this.view.flxStopLimitPriceRow2.setVisibility(true);
      } else if (data.orderType === "Limit") {
        this.view.tbxQuantity.text = data.quantity;
        this.view.tbxLimitPrice.text = data.limitPrice;
        this.view.lbxOrderType.selectedKey = "Limit";
        this.setApproximatelyValue(quantityText);
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue(quantityText);
        this.view.flxRow3.setVisibility(false);
        this.view.flxLimitPrice.setVisibility(true);
        this.view.flxStopLimitPriceRow2.setVisibility(false);
      } else {
        this.view.tbxQuantity.text = data.quantity;
        this.view.lbxOrderType.selectedKey = "Market";
        this.setApproximatelyValue(quantityText);
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue(quantityText);
        this.view.flxRow3.setVisibility(false);
        this.view.flxLimitPrice.setVisibility(false);
        this.view.flxStopLimitPriceRow2.setVisibility(false);
      }
      this.setUiChanges();
    },
    modifyOrderView: function() {
      this.view.flxAccountDetails.setVisibility(true);
      this.view.flxOrderDetails.setVisibility(true);
      this.view.flxPlaceOrderButtons.setVisibility(true);
      this.view.InstrumentDetailsPlaceOrder.flxButtons.setVisibility(true);
      this.view.lblOrderDetails.setVisibility(false);
      this.view.flxSeparatorLine1.setVisibility(false);
      this.view.flxOrderDetailsConfirmation.setVisibility(false);
      this.onOrderTypeSelected(this.view.lbxOrderType);
      this.onModifySelected(this.modifyPlaceOrder);
      this.orderFlow = "";
    },
    orderViewSummary:function() {
      var currForm = kony.application.getCurrentForm();
      this.modifyPlaceOrder = this.setPlaceOrderFormData();
      this.view.OrderDetailsConfirmation.lblOrderValue.text = this.operation;
      this.view.OrderDetailsConfirmation.lblQuantityValue.text = this.view.tbxQuantity.text;
      this.view.OrderDetailsConfirmation.lblOrderTypeValue.text = this.view.lbxOrderType.selectedKeyValue[0];
      switch (this.view.lbxOrderType.selectedKeyValue[0]) {
        case "Market":
          this.view.OrderDetailsConfirmation.flxLimitPrice.setVisibility(false);
          this.view.OrderDetailsConfirmation.flxStopPrice.setVisibility(false);
          break;
        case "Limit":
          this.view.OrderDetailsConfirmation.flxLimitPrice.setVisibility(true);
          this.view.OrderDetailsConfirmation.flxStopPrice.setVisibility(false);
          this.view.OrderDetailsConfirmation.lblLimitPriceValue.text = CommonUtilities.formatCurrencyWithCommas(this.view.tbxLimitPrice.text, false, this.view.lblLimitPriceCurrency.text);
          break;
        case "Stop Loss":
          this.view.OrderDetailsConfirmation.flxLimitPrice.setVisibility(false);
          this.view.OrderDetailsConfirmation.flxStopPrice.setVisibility(true);
          this.view.OrderDetailsConfirmation.lblStopPriceValue.text = CommonUtilities.formatCurrencyWithCommas(this.view.tbxStopLimitPriceRow2.text, false,  this.view.lblLimitPriceCurrency.text);
          break;
        case "Stop Limit":
          this.view.OrderDetailsConfirmation.flxLimitPrice.setVisibility(true);
          this.view.OrderDetailsConfirmation.flxStopPrice.setVisibility(true);
          this.view.OrderDetailsConfirmation.lblLimitPriceValue.text = CommonUtilities.formatCurrencyWithCommas(this.view.tbxLimitPriceStopLimit.text, false, this.view.lblLimitPriceCurrency.text);
          this.view.OrderDetailsConfirmation.lblStopPriceValue.text = CommonUtilities.formatCurrencyWithCommas(this.view.tbxStopLimitPrice.text, false, this.view.lblLimitPriceCurrency.text);
          break;
        default:
      }
      this.view.OrderDetailsConfirmation.lblValidityValue.text = this.view.lbxValidity.selectedKeyValue[1];
      var totalAmount = Number(this.view.tbxQuantity.text) * Number(this.view.InstrumentDetailsPlaceOrder.lblAmount.text);
      this.view.OrderDetailsConfirmation.lblTotalOrderAmountValue.text = this.view.lblApproximatelyVal.text;
      this.view.OrderDetailsConfirmation.lblEstimatedFeesValue.text = CommonUtilities.formatCurrencyWithCommas(Number(this.orderResponse.fees), false, this.view.lblLimitPriceCurrency.text);
      currForm.forceLayout();
    },
    restrictSpecialCharacters: function() {
      var scopeObj = this;
      var specialCharactersSet = "~#^|$%&*!@()_-+=}{][/|?,.><`':;\"\\";
      var alphabetsSet = "abcdefghijklmnopqrstuvwxyz";
      scopeObj.view.tbxQuantity.restrictCharactersSet = specialCharactersSet.replace(',.', '') + alphabetsSet + alphabetsSet.toUpperCase();
      scopeObj.view.tbxLimitPrice.restrictCharactersSet = specialCharactersSet.replace(',.', '') + alphabetsSet + alphabetsSet.toUpperCase();
      scopeObj.view.tbxStopLimitPriceRow2.restrictCharactersSet = specialCharactersSet.replace(',.', '') + alphabetsSet + alphabetsSet.toUpperCase();
      scopeObj.view.tbxLimitPriceStopLimit.restrictCharactersSet = specialCharactersSet.replace(',.', '') + alphabetsSet + alphabetsSet.toUpperCase();
      scopeObj.view.tbxStopLimitPrice.restrictCharactersSet = specialCharactersSet.replace(',.', '') + alphabetsSet + alphabetsSet.toUpperCase();
    },
    checkValidityPaceOrderForm: function() {
      var formData = this.setPlaceOrderFormData();
      if (Number(formData.quantity) !== 0 && this.view.flxOrderWarning.isVisible !== true) {
        if (this.selectedOrderType === "Market") {
          if (formData.quantity && formData.orderType && formData.validity) {
            CommonUtilities.enableButton(this.view.btnProceedPlaceOrder);
            return;
          }
          CommonUtilities.disableButton(this.view.btnProceedPlaceOrder);
        } else if (this.selectedOrderType === "Limit") {
          if (formData.quantity && formData.orderType && formData.limitPrice && formData.validity) {
            CommonUtilities.enableButton(this.view.btnProceedPlaceOrder);
            return;
          }
          CommonUtilities.disableButton(this.view.btnProceedPlaceOrder);
        } else if (this.selectedOrderType === "Stop Loss") {
          if (formData.quantity && formData.orderType && formData.stopLimitPriceRow2 && formData.validity) {
            CommonUtilities.enableButton(this.view.btnProceedPlaceOrder);
            return;
          }
          CommonUtilities.disableButton(this.view.btnProceedPlaceOrder);
        } else {
          if (formData.quantity && formData.orderType && formData.stopLimitPrice && formData.limitPriceStopLimit && formData.validity) {
            CommonUtilities.enableButton(this.view.btnProceedPlaceOrder);
            return;
          }
          CommonUtilities.disableButton(this.view.btnProceedPlaceOrder);
        }
      } else {
        CommonUtilities.disableButton(this.view.btnProceedPlaceOrder); 
      }
    },
    setPlaceOrderFormData: function() {
      var viewModel = {};
      var formUtilityMan = applicationManager.getFormatUtilManager();
      (this.view.tbxQuantity.text !== "") ? (viewModel.quantity = this.removeCurrencyWithCommas(this.view.tbxQuantity.text)) : (viewModel.quantity = "");
      (this.view.tbxLimitPrice.text !== "") ? (viewModel.limitPrice = this.removeCurrencyWithCommas(this.view.tbxLimitPrice.text)) : (viewModel.limitPrice = "");
      (this.view.tbxStopLimitPriceRow2.text !== "") ? (viewModel.stopLimitPriceRow2 = this.removeCurrencyWithCommas(this.view.tbxStopLimitPriceRow2.text)) : (viewModel.stopLimitPriceRow2 = "");
      (this.view.tbxLimitPriceStopLimit.text !== "") ? (viewModel.limitPriceStopLimit = this.removeCurrencyWithCommas(this.view.tbxLimitPriceStopLimit.text)) : (viewModel.limitPriceStopLimit = "");
      (this.view.tbxStopLimitPrice && this.view.tbxStopLimitPrice.text !== "") ? (viewModel.stopLimitPrice = this.removeCurrencyWithCommas(this.view.tbxStopLimitPrice.text)) : (viewModel.stopLimitPrice = "");
      (this.view.lbxOrderType.selectedKey !== "") ? (viewModel.orderType = this.view.lbxOrderType.selectedKey) : (viewModel.orderType = "");
      (this.view.lbxValidity.selectedKey !== "") ? (viewModel.validity = this.view.lbxValidity.selectedKeyValue[0]) : (viewModel.validity = "");
      (this.view.lblApproximatelyVal.text !== "") ? (viewModel.netAmount = this.removeCurrencyWithCommas(this.view.lblApproximatelyVal.text)) : (viewModel.netAmount = "");
      (this.view.lbxOrderType.selectedKey !== "") ? viewModel.marketPrice = formUtilityMan.deFormatAmount(this.view.InstrumentDetailsPlaceOrder.lblAmount.text) : (viewModel.marketPrice = "");
      viewModel.orderReference = this.modifyPlaceOrder.orderReference;
      viewModel.orderMode = this.modifyPlaceOrder.orderMode;
      viewModel.orderModeType = viewModel.orderType;
      viewModel.tradeDate = this.modifyPlaceOrder.tradeDate;
      orderParams = viewModel; 
      return viewModel;
    },
    /**Removes commans from currency
         * @param  {number} amount amount entered
         * @returns {number} amount
         */
    removeCurrencyWithCommas: function(amount) {
      if (amount === undefined || amount === null) {
        return;
      }
      return applicationManager.getFormatUtilManager().deFormatAmount(amount);
    },
    hideAllPopups: function() {
      this.closeOrderTypePopup();
      this.closeValidityPopup();
      this.view.accountActionsMenu.setVisibility(false);
      this.view.flx3Dot.skin = "slFbox";
    },
    onQuantityChange: function() {
      var formatQuantity = this.view.tbxQuantity.text;
      this.view.tbxQuantity.text = formatQuantity.replace(/^0+/, '');  
      this.view.lblApproximatelyVal.text = this.setApproximatelyValue(this.view.tbxQuantity.text);
      this.setApproximatelyValue(this.view.tbxQuantity.text);
      var portfolioCurrentPosition = this.currentInstrumentData;
      if (this.currentInstrumentData.holdingsData !== undefined && this.currentInstrumentData.holdingsData !== "") {
        if(this.operation === "Sell" && this.view.lblPlaceOrder.text == "Modify Order")
          this.instrumentDetailsQuantity = this.view.lblCurrentPositionAmount.text.split("(")[1].replace(")", "");
        else
          this.instrumentDetailsQuantity = this.currentInstrumentData.holdingsData.quantity;
      }
      else
        this.instrumentDetailsQuantity = this.currentInstrumentData.quantity;
      scope_WealthPresentationController.quantity = this.instrumentDetailsQuantity;
      portfolioCurrentPosition = portfolioCurrentPosition.marketPrice ? this.currentInstrumentData : scope_WealthPresentationController.selectedInstrDetails;
      var approximatelyValue = Number(this.view.tbxQuantity.text) * this.orderTypePrice;
      var forUtility = applicationManager.getFormatUtilManager();
      var totalValCurrency = applicationManager.getFormatUtilManager().getCurrencySymbol(scope_WealthPresentationController.portfolioTotalValueCurr);
      var cashBalance = Number(forUtility.deFormatAmount(scope_WealthPresentationController.portfolioTotalValue, [totalValCurrency]));
      if (this.operation === "Sell" && Number(this.view.tbxQuantity.text) > Number(this.instrumentDetailsQuantity)) {
        this.view.tbxQuantity.skin = "skntbxSSPFF000015pxnoborder";
        this.view.tbxQuantity.hoverSkin = "skntbxSSPFF000015pxnoborder";
        this.view.tbxQuantity.focusSkin = "skntbxSSPFF000015pxnoborder";
        this.view.flxOrderWarning.setVisibility(true);
        this.view.lblOrderWarning.text = "Your instrument's total count is insufficient to sell these quantities";
      } 
//       else if (this.operation === "Buy" && approximatelyValue > (cashBalance)) {
//         this.view.tbxQuantity.skin = "skntbxSSPFF000015pxnoborder";
//         this.view.tbxQuantity.hoverSkin = "skntbxSSPFF000015pxnoborder";
//         this.view.tbxQuantity.focusSkin = "skntbxSSPFF000015pxnoborder";
//         this.view.flxOrderWarning.setVisibility(true);
//         this.view.lblOrderWarning.text = "Balance is insufficient to buy these quantities";
//       }
      else {
        var quantitySkin = this.view.imgActive.src === "active.png" ? "skndisabled" : "sknTbxSSPffffff15PxBorder727272opa20";
        this.view.tbxQuantity.skin = quantitySkin;
        this.view.tbxQuantity.hoverSkin = "sknTbxBorder0036751px";
        this.view.tbxQuantity.focusSkin = "sknTbxBorder0036751px";
        this.view.flxOrderWarning.setVisibility(false);
      }
      this.view.forceLayout();
      this.checkValidityPaceOrderForm();
    },
    setApproximatelyValue: function(quantity) {
      var portfolioCurrentPosition = this.currentInstrumentData;
      portfolioCurrentPosition = portfolioCurrentPosition.marketPrice ? this.currentInstrumentData : scope_WealthPresentationController.selectedInstrDetails;
      var formUtilityMan = applicationManager.getFormatUtilManager();
      var currentPositionAmount;
      if (this.view.lbxOrderType.selectedKey === "Limit") {
        this.orderTypePrice = Number(formUtilityMan.deFormatAmount(this.view.tbxLimitPrice.text));
        this.view.lblApproximatelyEnd.text = "at limit price";
      } else if (this.view.lbxOrderType.selectedKey === "Stop Limit") {
        this.orderTypePrice = Number(formUtilityMan.deFormatAmount(this.view.tbxStopLimitPrice.text));
        this.view.lblApproximatelyEnd.text = "at stop limit price";
      } else if (this.view.lbxOrderType.selectedKey === "Stop Loss") {
        this.orderTypePrice = Number(formUtilityMan.deFormatAmount(this.view.tbxStopLimitPriceRow2.text));
        this.view.lblApproximatelyEnd.text = "at stop loss price";
      } else {
        this.orderTypePrice = this.instrumentDetails.lastPrice;
        this.view.lblApproximatelyEnd.text = "at market price";
      }
      if (quantity === "" || quantity === "0") {
        currentPositionAmount = 0 * this.orderTypePrice;
        this.view.tbxQuantity.text = quantity;
      } else if (quantity === "1") {
        currentPositionAmount = 1 * this.orderTypePrice;
        this.view.tbxQuantity.text = quantity;
      } else {
        currentPositionAmount = (Number(quantity) || Number(portfolioCurrentPosition.quantity)) * this.orderTypePrice;
      }
      var forUtility = applicationManager.getFormatUtilManager();
      var currentCurrency;
      if (this.currentInstrumentData.holdingsData !== undefined && this.currentInstrumentData.holdingsData !== "") {
        var currencyCode = this.currentInstrumentData.holdingsData.instrumentCurrency || this.currentInstrumentData.holdingsData.secCCy;
        currentCurrency = forUtility.getCurrencySymbol(currencyCode);
      } else
        if (this.navFromForm == "frmWatchlist") currentCurrency = forUtility.getCurrencySymbol(scope_WealthPresentationController.tradeCurrency);
      else currentCurrency = forUtility.getCurrencySymbol(this.currentInstrumentData.secCCy);
      return forUtility.formatAmountandAppendCurrencySymbol(String(currentPositionAmount || 0), currentCurrency);  
    },
    setCurrentPosition: function() {
            let currentPositionQuantity = this.getInstrumentCurrentPosition.quantity || "0";
            let currentPositionCurrency = this.getInstrumentCurrentPosition.secCCy;
            let tempPrice = this.getInstrumentCurrentPosition.marketValPOS || "0.00";
            var forUtility = applicationManager.getFormatUtilManager();
            let currentPositionPrice = forUtility.deFormatAmount(tempPrice);           
            return forUtility.formatAmountandAppendCurrencySymbol(String(currentPositionPrice), currentPositionCurrency) + " (" + currentPositionQuantity + ")";
     },
    onClickSelectAll: function() {
      if (this.view.imgActive.src === "inactive.png") {
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue(this.instrumentDetailsQuantity);
        this.view.tbxQuantity.text = scope_WealthPresentationController.quantity;
        this.onQuantityChange(scope_WealthPresentationController.quantity);
        this.view.imgActive.src = "active.png";
        this.view.tbxQuantity.skin = "skndisabled";
        this.view.tbxQuantity.setEnabled(false);
      } else {
        this.view.imgActive.src = "inactive.png";
        this.view.tbxQuantity.skin = "sknTbxSSPffffff15PxBorder727272opa20";
        this.view.lblApproximatelyVal.text = this.setApproximatelyValue("0");
        this.view.tbxQuantity.setEnabled(true);
      }
    },
    cancelTheOrder: function(){
      // to remove hardcoded Portofolio ID
      var portId = this.portfolioId;  
      var params = {
        "portfolioId": portId,
        "orderId": this.orderResponse.id
      };
      this.cancelOrder(params);
      this.clearOrderDetails();
      this.goBack();
    },
    clearOrderDetails: function(){
      this.orderResponse = "";
      this.orderRequest = "";
      /* if(uiData.CreateOrderResponse) {
          delete uiData.CreateOrderResponse;
      }
      if(uiData.CancelOrderResponse) {
          delete uiData.CancelOrderResponse;
      }*/
    },
    viewOrders: function() {
      scope_WealthPresentationController.viewOrdersTab = true;
      this.goToPortfolioDetails(this.portfolioId);
    },
    goToPortfolioDetails: function(portfolioId) {
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.setPortfolioId(portfolioId);
      scope_WealthPresentationController.isFirst = true;
      if (applicationManager.getConfigurationManager().checkUserFeature("WEALTH_PORTFOLIO_DETAILS")) {
        applicationManager.getNavigationManager().navigateTo("frmPortfolioOverview");
      }
    },
    reloadProductDetails: function() {
      scope_WealthPresentationController.reload = "Reload";
      let data = {
        "portfolioId": applicationManager.getModulesPresentationController("WealthModule").getPortfolioId(),
        "navPage": "Holdings",
        "searchByInstrumentName":this.instrName,
        "sortBy": ""
      };

      applicationManager.getModulesPresentationController("WealthModule").getHoldingsCurrentPosition(data);
    },
    goBack: function() {
           scope_WealthPresentationController.instrumentAction = "";
           scope_WealthPresentationController.reload = "";
            var previousForm = kony.application.getPreviousForm().id;
            var navMan = applicationManager.getNavigationManager();
            if (previousForm === "frmPortfolioOverview") {
                navMan.navigateTo("frmPortfolioOverview");
            } 
            else if (previousForm === "frmWatchlist") {
                navMan.navigateTo("frmWatchlist");
            } else {
               navMan.navigateTo("frmInstrumentDetails");
           }
    },
    createMarketOrder: function(params) {
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.createMarketOrder(params);
    },
    modifyMarketOrder: function(params) {
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.modifyMarketOrder(params);
    },
    cancelOrder: function(params){
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.cancelOrder(params);
    },
    checkPermissions: function(){
      var configManager =  applicationManager.getConfigurationManager();
      var checkUserPermission = function (permission) {
        return applicationManager.getConfigurationManager().checkUserPermission(permission);
      };
      let instrumCreatePermission = configManager.addToWatchlistPermissions().some(checkUserPermission);
      let orderEditPermission = configManager.mgmtOrderEditPermissions().some(checkUserPermission);
      let orderCancelPermission = configManager.mgmtOrderCancelPermissions().some(checkUserPermission);
      let orderVerifyPermission = configManager.mgmtOrderVerifyPermissions().some(checkUserPermission);
      let orderViewAckPermission = configManager.mgmtOrderViewAckPermissions().some(checkUserPermission);
      let orderSellPermission = configManager.sellOrderPermissions().some(checkUserPermission);
      let orderBuyPermission = configManager.buyOrderPermissions().some(checkUserPermission);
      let orderLinkPermission = configManager.mgmtOrderViewOrderLinkPermissions().some(checkUserPermission);
      this.view.InstrumentDetailsPlaceOrder.btnSell.setVisibility(orderSellPermission);
      this.view.InstrumentDetailsPlaceOrder.btnBuy.setVisibility(orderBuyPermission);
      if (orderSellPermission && orderBuyPermission) {
        if(this.orderFlow !== "Confirm"){
          this.view.InstrumentDetailsPlaceOrder.flxButtons.isVisible = true;
        }
        else {
          this.view.InstrumentDetailsPlaceOrder.flxButtons.isVisible = false;
        }
      } else {
        this.view.InstrumentDetailsPlaceOrder.flxButtons.isVisible = false;
      }

      this.view.InstrumentDetailsPlaceOrder.imgStar.setVisibility(instrumCreatePermission);
      this.view.OrderDetailsConfirmation.Buttons.btnModify.setVisibility(orderEditPermission);
      this.view.OrderDetailsConfirmation.Buttons.btnCancel.setVisibility(orderCancelPermission);
      this.view.btnCancelPlaceOrder.setVisibility(orderCancelPermission); 
      this.view.btnProceedPlaceOrder.setVisibility(orderVerifyPermission); 
      this.view.OrderDetailsConfirmation.Buttons.btnConfirm.setVisibility(orderViewAckPermission); 
      this.view.InstrumentDetailsPlaceOrder.btnSell.setVisibility(orderSellPermission);
      this.view.InstrumentDetailsPlaceOrder.btnBuy.setVisibility(orderBuyPermission);
      this.view.btnViewOrder.setVisibility(orderLinkPermission);


      //       this.view.InstrumentDetailsPlaceOrder.imgStar.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("WEALTH_WATCHLIST_INSTRUMENT_CREATE"));
      //       this.view.OrderDetailsConfirmation.Buttons.btnModify.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("WEALTH_ORDER_MGMT_ORDER_EDIT"));
      //       this.view.OrderDetailsConfirmation.Buttons.btnCancel.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("WEALTH_ORDER_MGMT_ORDER_CANCEL"));
      //       this.view.btnCancelPlaceOrder.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("WEALTH_ORDER_MGMT_ORDER_CANCEL")); 
      //       this.view.btnProceedPlaceOrder.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("WEALTH_ORDER_MGMT_ORDER_VERIFICATION_VIEW")); 
      //       this.view.OrderDetailsConfirmation.Buttons.btnConfirm.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("WEALTH_ORDER_MGMT_ORDER_ACKNOWLEDGEMENT_VIEW")); 
      //       this.view.InstrumentDetailsPlaceOrder.btnSell.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("WEALTH_ORDER_MGMT_SELL_ORDER_CREATE"));
      //       this.view.InstrumentDetailsPlaceOrder.btnBuy.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("WEALTH_ORDER_MGMT_BUY_ORDER_CREATE"));
      //       this.view.btnViewOrder.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("WEALTH_ORDER_MGMT_ORDER_LINK_VIEW"));
    },
    setStarValue: function(favoriteList){      
      if(favoriteList && favoriteList.find(element=>element.RIC===this.portfolioDetails.RICCode)){        
        this.view.InstrumentDetailsPlaceOrder.imgStar.src = "activestar.png";  
      }   
    },
    updateFavList: function(oper){
      var params = {};
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");

      if(oper === 'get'){

        params = {
          "sortBy": "volume",
          "pageOffset": 0,
          "pageSize": 3
        };
        wealthModule.getFavoriteInstruments(params);

      }else{

        if(this.view.InstrumentDetailsPlaceOrder.imgStar.src==="activestar.png"){
          this.view.InstrumentDetailsPlaceOrder.imgStar.src = "unfilled_star.png";
          oper = 'Remove';
        }else{
          this.view.InstrumentDetailsPlaceOrder.imgStar.src="activestar.png";
          oper = 'Add';
        }
        params = {
          "RICCode": this.ricCode || this.portfolioDetails.RICCode,
          "instrumentId": this.instrumentId,
          "operation": oper
        };

        wealthModule.updateFavouriteInstruments(params);
      }

    },
    
    getFavouriteStatus: function() {
      var params = {};
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.getUserFavouriteInstruments(params);
    },

    setFavourite: function(obj) {
      this.favIds = obj.favInstrumentIds !== undefined ? obj.favInstrumentIds : "";
      this.favRics = obj.favInstrumentCodes!== undefined ? obj.favInstrumentCodes : "";
      if (this.instrumentId !== "") {
        this.setFavouriteStatus();
      }
    },
    
    setFavouriteStatus() {
      if (this.instrumentId !== "" && this.instrumentId !== undefined && this.favIds.search(this.instrumentId) > -1) {
        this.view.InstrumentDetailsPlaceOrder.imgStar.src="activestar.png";
      } else if (this.ricCode !== "" && this.ricCode !== undefined && this.favRics.search(this.ricCode) > -1) {
        this.view.InstrumentDetailsPlaceOrder.imgStar.src="activestar.png";
      } else {
        this.view.InstrumentDetailsPlaceOrder.imgStar.src = "unfilled_star.png";
      }
    }
    
  };
});