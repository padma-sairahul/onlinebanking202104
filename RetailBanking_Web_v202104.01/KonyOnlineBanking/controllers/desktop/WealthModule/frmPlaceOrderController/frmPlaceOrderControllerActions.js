define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmPlaceOrder **/
    AS_Form_c27c7fc2618e4c9fa367a4df2fec3ef4: function AS_Form_c27c7fc2618e4c9fa367a4df2fec3ef4(eventobject) {
        var self = this;
        return self.init.call(this);
    },
    /** postShow defined for frmPlaceOrder **/
    AS_Form_c9e7e2fb3b794f7781b43647f38af7b5: function AS_Form_c9e7e2fb3b794f7781b43647f38af7b5(eventobject) {
        var self = this;
        return self.postShow.call(this);
    },
    /** preShow defined for frmPlaceOrder **/
    AS_Form_g0b61245ff2941c898b9ef6e419d1593: function AS_Form_g0b61245ff2941c898b9ef6e419d1593(eventobject) {
        var self = this;
        return self.preShow.call(this);
    }
});