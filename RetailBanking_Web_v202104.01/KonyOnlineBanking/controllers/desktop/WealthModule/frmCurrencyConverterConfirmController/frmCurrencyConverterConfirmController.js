define(['CommonUtilities','ViewConstants'], function(CommonUtilities, ViewConstants) {
  var responsiveUtils = new ResponsiveUtils();

  //Type your controller code here 
  return {
    updateFormUI: function(uiData) {
      if (uiData) {
        if(uiData.currencyConfirmData){
          this.setData(uiData.currencyConfirmData);
        }
      }
    },
    init: function(){
      this.view.onBreakpointChange = this.onBreakpointChange;
    },

    preShow: function(){
      this.view.btnConvert.onClick = this.convertNow;
    },

    postShow: function(){
      applicationManager.getNavigationManager().applyUpdates(this);
      this.setActiveHeaderHamburger();
    },
    setSuccess: function(response){
      this.view.lblCCEstimatesFTValue.text = response.fees;
      this.view.forceLayout();
    },
    setData: function(data){
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      let formatValue = applicationManager.getFormatUtilManager();
      this.view.lblCCFromValue.text = data.buyCurrency;
      this.view.lblCCFromAmountValue.text = data.buyAmount;
      this.view.lblCCToValue.text = data.sellCurrency;
      this.view.lblCCToAmountValue.text = data.sellAmount;
      if(data.convertTime){
        this.view.lblCCConvertLaterValue.text = data.convertTime;
        this.view.lblCCConvertLater.setVisibility(true);
        this.view.lblCCConvertLaterValue.setVisibility(true);
        this.view.lblCCConvertNow.setVisibility(false);
        this.view.lblCCConvertNowValue.setVisibility(false);
      } else {
        this.view.lblCCConvertLaterValue.text = "";
        this.view.lblCCConvertLater.setVisibility(false);
        this.view.lblCCConvertLaterValue.setVisibility(false);
        this.view.lblCCConvertNow.setVisibility(true);
        this.view.lblCCConvertNowValue.setVisibility(true);
      }
       let params = {
        buyCurrency: this.view.lblCCFromValue.text,
        sellCurrency: this.view.lblCCToValue.text,
        buyAmount: formatValue.deFormatAmount(this.view.lblCCFromAmountValue.text),
        sellAmount: formatValue.deFormatAmount(this.view.lblCCToAmountValue.text)
      };
      if(this.view.lblCCConvertLaterValue.text){
        params.convertTime = this.view.lblCCConvertLaterValue.text;
      }
      if(params.sellAmount){
        var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
        wealthModule.createOrder(params);
        applicationManager.getPresentationUtility().showLoadingScreen();
      }
    },
    convertNow: function(){
      let formatValue = applicationManager.getFormatUtilManager();
      let params = {
        buyCurrency: this.view.lblCCFromValue.text,
        sellCurrency: this.view.lblCCToValue.text,
        buyAmount: formatValue.deFormatAmount(this.view.lblCCFromAmountValue.text),
        sellAmount: formatValue.deFormatAmount(this.view.lblCCToAmountValue.text)
      };
      if(this.view.lblCCConvertLaterValue.text){
        params.convertTime = this.view.lblCCConvertLaterValue.text;
      }
      if(params.sellAmount){
        var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
        wealthModule.createOrder(params);
        var navManager =  applicationManager.getNavigationManager();
        applicationManager.getPresentationUtility().showLoadingScreen();
        navManager.navigateTo("frmCurrencyConverterAcknowledge");
        navManager.updateForm({
          currencyConfirmData: params
        }, 'frmCurrencyConverterAcknowledge');
      }
    },
    setActiveHeaderHamburger: function() {
      this.view.customheadernew.activateMenu("Accounts", "My Accounts");
      this.view.customheadernew.flxContextualMenu.setVisibility(false);
      this.view.customheadernew.flxAccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
    },
    onBreakpointChange: function(form, width) {
      //       responsiveUtils.onOrientationChange(this.onBreakpointChange, function() {

      //       }.bind(this));
      this.view.customheadernew.onBreakpointChangeComponent(width);
      this.view.customfooternew.onBreakpointChangeComponent(width);
      //       this.view.customfooter.onBreakpointChangeComponent(width);
    },
    setError: function(obj){
    var test = obj; 
    }
  };
});