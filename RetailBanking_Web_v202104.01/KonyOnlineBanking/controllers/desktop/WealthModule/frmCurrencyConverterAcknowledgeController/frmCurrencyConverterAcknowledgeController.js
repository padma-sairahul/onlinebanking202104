define(['CommonUtilities','ViewConstants'], function(CommonUtilities, ViewConstants) {
  var responsiveUtils = new ResponsiveUtils();

  //Type your controller code here 
  return {
    params:[],
    updateFormUI: function(uiData) {
      if (uiData) {
        if(uiData.currencyConfirmData){
          this.params = uiData.currencyConfirmData;
        }
      }
    },
    init: function(){
      this.view.onBreakpointChange = this.onBreakpointChange;
    },

    preShow: function(){

    },

    postShow: function(){
      applicationManager.getNavigationManager().applyUpdates(this);
      this.setActiveHeaderHamburger();
    },
    setSuccess: function(response){
      let formatValue = applicationManager.getFormatUtilManager();
      this.view.lblCCFromValue.text = this.params.buyCurrency;
      this.view.lblCCFromAmountValue.text = formatValue.formatAmount(this.params.buyAmount);
      this.view.lblCCToValue.text = this.params.sellCurrency;
      this.view.lblCCEstimatesFTValue.text = response.fees;
      this.view.lblCCToAmountValue.text = formatValue.formatAmount(this.params.sellAmount);
      if(this.params.convertTime){
        this.view.lblCCConvertLaterValue.text = this.params.convertTime;
        this.view.lblCCConvertLater.setVisibility(true);
        this.view.lblCCConvertLaterValue.setVisibility(true);
        this.view.lblCCConvertNow.setVisibility(false);
        this.view.lblCCConvertNowValue.setVisibility(false);
      } else {
        this.view.lblCCConvertLaterValue.text = "";
        this.view.lblCCConvertLater.setVisibility(false);
        this.view.lblCCConvertLaterValue.setVisibility(false);
        this.view.lblCCConvertNow.setVisibility(true);
        this.view.lblCCConvertNowValue.setVisibility(true);
      }
      this.view.forceLayout();
      applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    setError: function(response){

    },
    setActiveHeaderHamburger: function() {
      this.view.customheadernew.activateMenu("Accounts", "My Accounts");
      this.view.customheadernew.flxContextualMenu.setVisibility(false);
      this.view.customheadernew.flxAccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
    },
    onBreakpointChange: function(form, width) {
      //       responsiveUtils.onOrientationChange(this.onBreakpointChange, function() {

      //       }.bind(this));
      this.view.customheadernew.onBreakpointChangeComponent(width);
      this.view.customfooternew.onBreakpointChangeComponent(width);
      //       this.view.customfooter.onBreakpointChangeComponent(width);
    },
  };
});