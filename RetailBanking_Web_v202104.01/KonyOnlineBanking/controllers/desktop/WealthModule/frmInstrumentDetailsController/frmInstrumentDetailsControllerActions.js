define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmInstrumentDetails **/
    AS_Form_fe4ff4120eac44a89d3e23405b37c28e: function AS_Form_fe4ff4120eac44a89d3e23405b37c28e(eventobject) {
        var self = this;
        this.postshow()
    },
    /** preShow defined for frmInstrumentDetails **/
    AS_Form_g3cb9a78a4cd45709c0fb918358ff035: function AS_Form_g3cb9a78a4cd45709c0fb918358ff035(eventobject) {
        var self = this;
        this.preshow()
    },
    /** init defined for frmInstrumentDetails **/
    AS_Form_j97d996147054b1aa62c677d26f9f692: function AS_Form_j97d996147054b1aa62c677d26f9f692(eventobject) {
        var self = this;
        this.init();
    },
    /** onFilterChanged defined for investmentLineChart **/
    AS_UWI_b67c437eb65743e087b39ea57a272307: function AS_UWI_b67c437eb65743e087b39ea57a272307(filter) {
        var self = this;
        return self.onFilterChanged.call(this, filter);
    }
});