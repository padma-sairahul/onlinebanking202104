define(['CommonUtilities','ViewConstants'], function(CommonUtilities, ViewConstants) {
  var responsiveUtils = new ResponsiveUtils();

  return {
    chartDefaultValue: "",

    productDetails:{},
    instrumentMinimal:{},
    portfolioDetails:{},
    ricCode:'',
   
    updateFormUI: function(uiData) {
      var currForm = kony.application.getCurrentForm();
      if (uiData) {
        if(uiData.currentPosition){
          this.listCurrentPosition(uiData.currentPosition);
        }
        if(uiData.instrumentError){
          this.showErrorProductDetails(uiData.instrumentError);
        }
        if(uiData.historicalData){          
          currForm.forceLayout();
          this.lineChartData(uiData.historicalData);
        }
      }
    },

    init: function(){
      this.view.onBreakpointChange = this.onBreakpointChange;
      this.view.Documents.segValue.onRowClick = function() {
        var scopeObj = this;
        var fileUrl = scopeObj.selectedRowItems[0]["lblFactsheetURL"];
        kony.application.openURL(fileUrl);
      };   
      this.chartDefaultValue = this.view.investmentLineChart.currentFilter;    
      this.view.marketNewsCardComp.marketNewsPostShow = this.postShowWealthComp;
    },

    preshow: function(){
      
      let tempPortfolio = applicationManager.getNavigationManager().getCustomInfo('frmInstrumentDetails').portfolioDetails;
      this.portfolioDetails = tempPortfolio?tempPortfolio:{};
      this.productDetails = applicationManager.getNavigationManager().getCustomInfo('frmInstrumentDetails').productDetails; 
      
      if(this.productDetails.instrumentMinimal[0])        
        this.ricCode = this.portfolioDetails.RICCode? this.portfolioDetails.RICCode : this.productDetails.instrumentMinimal[0].RICCode; 
      else        
        this.ricCode = this.portfolioDetails.RICCode? this.portfolioDetails.RICCode : '';
      
      this.view.flxBack.onTouchEnd=this.goBack;
      this.view.InstrumentDetails.btnSell.onClick = this.onSell;
      this.view.InstrumentDetails.btnBuy.onClick = this.onBuy;
      this.view.marketNewsTablet.lblViewAll.onTouchEnd = this.ViewAllStockNews.bind();     

   //  this.onFilterChanged(this.chartDefaultValue);

      this.view.investmentLineChart.currentFilter = this.chartDefaultValue;
      this.view.InstrumentDetails.setVisibility(true);

      if(this.view.InstrumentDetails.imgStar.isVisible){
        this.updateFavList('get');
        this.view.InstrumentDetails.imgStar.onTouchEnd = this.updateFavList.bind();
      }
      this.view.InstrumentDetails.imgRefresh.onTouchEnd = this.reloadProductDetails.bind();

      var stockNewsParam = {
        "RICCode": this.ricCode,
        "pageSize": 4,
        "pageOffset": 0
      };
      this.view.marketNewsCardComp.getCriteria(stockNewsParam, this.view.flxStocks.isVisible, this.view.flxStocks.isVisible);
    },

    postshow: function() {
      this.checkPermissions();
      applicationManager.getNavigationManager().applyUpdates(this);
      this.setActiveHeaderHamburger();
      this.getProductDetails(this.productDetails);
    },

    listCurrentPosition: function(currentPosition) {

      if (Object.entries(currentPosition).length === 0 || (Object.entries(currentPosition).length === 1 && currentPosition.operation)){
        let refCurrency = this.productDetails.instrumentMinimal[0].instrumentCurrencyId.toUpperCase();
                
        this.view.CurrentPosition.lblCode.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(0, refCurrency);
        this.view.CurrentPosition.lblPLValue.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(0, refCurrency);
        this.view.CurrentPosition.lblQuantityValue.text = "0";
        this.view.CurrentPosition.lblWeightValue.text = applicationManager.getFormatUtilManager().formatAmount("0.00") + "%";
        this.view.CurrentPosition.lblPriceValue.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(0, refCurrency);
      }
      else {
        var price = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(currentPosition.costPrice, currentPosition.secCCy.toUpperCase());
        var marketValue = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(currentPosition.marketValPOS.replace(/,/g,''), currentPosition.secCCy.toUpperCase());
        var unrealizedPL = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(currentPosition.unrealizedProfitLossSecCcy, currentPosition.secCCy.toUpperCase());
        this.view.CurrentPosition.lblCode.text = marketValue;
        this.view.CurrentPosition.lblQuantityValue.text = currentPosition.quantity;
        this.view.CurrentPosition.lblWeightValue.text = applicationManager.getFormatUtilManager().formatAmount(currentPosition.weightPercentage) + "%";
        this.view.CurrentPosition.lblPriceValue.text = price;
        if (parseFloat(currentPosition.unrealizedProfitLossSecCcy) > 0) {
          this.view.CurrentPosition.lblPLValue.skin = "sknIW2F8523";
        }
        else if (parseFloat(currentPosition.unrealizedProfitLossSecCcy) < 0) {
          this.view.CurrentPosition.lblPLValue.skin = "sknlblff000015px";
        }
        else {
          this.view.CurrentPosition.lblPLValue.skin = "bbSknLbl424242SSP17Px";
        }
        this.view.CurrentPosition.lblPLValue.text = unrealizedPL;
      }
    },

    showErrorProductDetails: function() {
      this.view.flxErrorInfo.setVisibility(true);
    },

    getProductDetails: function(response) {

      if (response.instrumentDetails) {
        
        this.getInstrumentDetails(response.instrumentDetails, this.productDetails.instrumentMinimal[0], response.stockNews);        
      } else {
        if(this.productDetails.instrumentMinimal.length>0)
        this.getInstrumentTransactDetails(this.productDetails.instrumentMinimal[0], response.stockNews);       
        else
          this.view.InstrumentDetails.setVisibility(false);
      }
      
      if (this.view.InstrumentDetails.flxButtons.isVisible) {
        this.view.InstrumentDetails.height = "280Dp";             
      }
      else {
        this.view.InstrumentDetails.height = "230Dp";            
      }   

      // Documents
      if (response.documentDetails)
        if (this.view.flxDocuments.isVisible && response.documentDetails.length > 0) {
          this.getDocuments(response.documentDetails); 
        } else {
          this.view.flxDocuments.setVisibility(false);
        }
      else
        this.view.flxDocuments.setVisibility(false);

      // Pricing Data
      if (this.view.flxPriceData.isVisible && response.pricingDetails) 
        this.getPricingDetails(response.pricingDetails);
      else
        this.view.flxPriceData.setVisibility(false);
      
      // Stock News
      if (this.view.flxStocks.isVisible && response.stockNews && response.stockNews.length > 0) {
        this.getStockNews(response.stockNews);
      }
      else {
        this.view.flxStocks.setVisibility(false);
      }

      // Current Position
      if (this.view.CurrentPosition.isVisible) 
      this.listCurrentPosition(this.portfolioDetails);  
      
      applicationManager.getModulesPresentationController("WealthModule").setInstrumentCurrentPosition(this.portfolioDetails?this.portfolioDetails:{});
      applicationManager.getModulesPresentationController("WealthModule").setInstrumentPricingData(response.pricingDetails?response.pricingDetails:{});
    },

    getDocuments: function(documents) {
      var segDocumentsData = [];
      var segDocumentsDataOdd = [];
      var segDocumentsDataEven = [];
      var file_mime_type = {
        "application/x-msword": "doc",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document": "docx",
        "application/pdf": "pdf",
        "application/x-pdf": "pdf",
        "application/rtf": "rtf",
        "text/rtf": "rtf",
        "text/plain": "txt",
      };

      for (var i = 0; i < documents.length; i++) {
        if ((i % 2) === 1) {
          segDocumentsDataOdd.push({
            "lblFactsheet": documents[i].type + "." + file_mime_type[documents[i].mediaType],
            "lblFactsheetURL": documents[i].link
          });
        } 
        else 
        {
          segDocumentsDataEven.push({
            "lblFactsheet": documents[i].type + "." + file_mime_type[documents[i].mediaType],
            "lblFactsheetURL": documents[i].link
          });
        }
      }
      this.view.DocumentsTablet.segDocsLeft.widgetDataMap = {
        "lblFactsheet": "lblFactsheet",
        "lblFactsheetURL": "lblFactsheetURL"
      };
      this.view.DocumentsTablet.segDocsRight.widgetDataMap = {
        "lblFactsheet": "lblFactsheet",
        "lblFactsheetURL": "lblFactsheetURL"
      };
      this.view.DocumentsTablet.segDocsLeft.setData(segDocumentsDataOdd);
      this.view.DocumentsTablet.segDocsRight.setData(segDocumentsDataEven);

      documents.forEach(function (documentItem) {
        segDocumentsData.push({
          "lblFactsheet": documentItem.type + "." + file_mime_type[documentItem.mediaType],
          "lblFactsheetURL": documentItem.link
        });
      });

      this.view.Documents.segValue.widgetDataMap = {
        "lblFactsheet": "lblFactsheet",
        "lblFactsheetURL": "lblFactsheetURL"
      };
      this.view.Documents.segValue.setData(segDocumentsData);
    },

    getInstrumentDetails: function(instrument, instrumentTransact) {
      var tepmTimeZone = "";
      let instrumentParams = {};
           var instrumentDetailsDate = {

        "timeReceived": ((instrument.timeReceived) ? instrument.timeReceived : "00:00:00"),
        "dateReceived": ((instrument.dateReceived) ? instrument.dateReceived : "")
      };

      let netchange = applicationManager.getFormatUtilManager().formatAmount(instrument.netchange);
      let percentageChange = applicationManager.getFormatUtilManager().formatAmount(instrument.percentageChange);

      this.view.InstrumentDetails.lblCorp.text = ((instrumentTransact.instrumentName)?instrumentTransact.instrumentName:"");
      this.view.InstrumentDetails.lblCode.text = ((instrumentTransact.ISINCode)?instrumentTransact.ISINCode:"") + ((instrumentTransact.stockExchange)?" | " + instrumentTransact.stockExchange:"");
      
      if (instrument.lastPrice === "0.0") {
        this.view.InstrumentDetails.lblAmount.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(instrument.closeRate, instrument.referenceCurrency.toUpperCase());
      }
      else {
        this.view.InstrumentDetails.lblAmount.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(instrument.lastPrice, instrument.referenceCurrency.toUpperCase());
      }
      
      this.view.InstrumentDetails.lblProfit.text = ((netchange)?netchange:"") + ((percentageChange)?" (" + percentageChange + "%)":"");
      this.view.InstrumentDetails.lblTime.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf")+ " " + this.setInstrumentDate(instrumentDetailsDate);
      if (parseFloat(instrument.percentageChange) > 0) 
        this.view.InstrumentDetails.lblProfit.skin = "sknIW2F8523";
      else if (parseFloat(instrument.percentageChange) < 0)
        this.view.InstrumentDetails.lblProfit.skin = "sknlblff000015px";
      else
        this.view.InstrumentDetails.lblProfit.skin = "bbSknLbl424242SSP17Px";

      instrumentParams.instrumentName = instrumentTransact.instrumentName;
      instrumentParams.ISINCode = instrumentTransact.ISINCode;
      instrumentParams.stockExchange = instrumentTransact.stockExchange;
      instrumentParams.lastPrice = (instrument.lastPrice === "0.0")?instrument.closeRate : instrument.lastPrice;
      instrumentParams.netChange = instrument.netchange;
      instrumentParams.percentageChange = instrument.percentageChange;
      instrumentParams.referenceCurrency = instrument.referenceCurrency.toUpperCase();
      instrumentParams.asOfdateTime = kony.i18n.getLocalizedString("i18n.accounts.AsOf")+ " " + this.setInstrumentDate(instrumentDetailsDate);
      
      applicationManager.getModulesPresentationController("WealthModule").setInstrumentDetails(instrumentParams);
      
    },
    

    getInstrumentTransactDetails: function(instrument) {
      let instrumentParams = {};
           var instrumentDetailsDate = {

        "timeReceived": ((instrument.timeReceived) ? instrument.timeReceived : "00:00:00"),
        "dateReceived": ((instrument.lastPriceDate) ? instrument.lastPriceDate : "")
      };

      this.view.InstrumentDetails.lblCorp.text = ((instrument.instrumentName)?instrument.instrumentName:"");
      this.view.InstrumentDetails.lblCode.text = ((instrument.ISINCode)?instrument.ISINCode:"") + ((instrument.stockExchange)?" | " + instrument.stockExchange:"");
      this.view.InstrumentDetails.lblAmount.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(instrument.lastPrice, instrument.priceCurrency.toUpperCase());

      this.view.InstrumentDetails.lblTime.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf")+ " " + this.setInstrumentDate(instrumentDetailsDate);
      this.view.InstrumentDetails.lblProfit.text = "";
      
      
      instrumentParams.instrumentName = instrument.instrumentName;
      instrumentParams.ISINCode = instrument.ISINCode;
      instrumentParams.stockExchange = instrument.stockExchange;
      instrumentParams.lastPrice = instrument.lastPrice?instrument.lastPrice : 0 ;
      instrumentParams.netChange = 0;
      instrumentParams.percentageChange = 0;
      instrumentParams.priceCurrency = instrument.priceCurrency.toUpperCase(); 

      instrumentParams.asOfdateTime = kony.i18n.getLocalizedString("i18n.accounts.AsOf")+ " " + this.setInstrumentDate(instrumentDetailsDate);
      
      applicationManager.getModulesPresentationController("WealthModule").setInstrumentDetails(instrumentParams);
      
      
    },
    
    getPricingDetails: function(prices) {
      this.view.PricingData.lblCode.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.bidRate, prices.referenceCurrency.toUpperCase());
      this.view.PricingData.lblBidValue.text = ((prices.bidVolume)?prices.bidVolume:"").toString();
      this.view.PricingData.lblVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.askRate, prices.referenceCurrency.toUpperCase());
      this.view.PricingData.lblAskVal.text = ((prices.askVolume)?prices.askVolume:"").toString();
      this.view.PricingData.lblVolVal.text = ((prices.volume)?prices.volume:"").toString();
      this.view.PricingData.lblOpenVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.openRate, prices.referenceCurrency.toUpperCase());
      this.view.PricingData.lblCloseVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.closeRate, prices.referenceCurrency.toUpperCase());
      this.view.PricingData.lblHighVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.high52W, prices.referenceCurrency.toUpperCase());
      this.view.PricingData.lblLowVal.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.low52W, prices.referenceCurrency.toUpperCase());
      this.view.PricingData.lblLatestVal.text =  applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(prices.latestRate, prices.referenceCurrency.toUpperCase());
    },

    getStockNews: function(news) {
      if (kony.application.getCurrentBreakpoint() > 1024 && this.view.flxStocks.isVisible === true) {
            var stockNewsParam = {
                "RICCode": this.ricCode,
                "pageSize": 4,
                "pageOffset": 0
            };
            this.view.marketNewsCardComp.getCriteria(stockNewsParam, this.view.flxStocks.isVisible, this.view.flxStocks.isVisible);
       }
       else {
            var segStockNewsDataOdd = [];
            var segStockNewsDataEven = [];
            if (news) {
              news = news.slice(0,4);
              for (var i = 0; i < news.length; i++) {
                var dateToString = news[i]["RT"].replace(/\D/g,'');
                if ((i % 2) === 1) {
                  segStockNewsDataOdd.push({
                    "lblName": news[i]["PR"],
                    "lblContent": news[i]["HT"],
                    "lblTime": CommonUtilities.getTimeDiferenceOfDate(dateToString)
                  });
                } 
                else 
                {
                  segStockNewsDataEven.push({
                    "lblName": news[i]["PR"],
                    "lblContent": news[i]["HT"],
                    "lblTime": CommonUtilities.getTimeDiferenceOfDate(dateToString.substring(0,14))
                  });
                }
              }
              this.view.marketNewsTablet.segNewsLeft.widgetDataMap = {
                "lblName": "lblName",
                "lblContent": "lblContent",
                "lblTime": "lblTime"
              };
              this.view.marketNewsTablet.segNewsRight.widgetDataMap = {
                "lblName": "lblName",
                "lblContent": "lblContent",
                "lblTime": "lblTime"
              };
              this.view.marketNewsTablet.segNewsLeft.setData(segStockNewsDataOdd);
              this.view.marketNewsTablet.segNewsRight.setData(segStockNewsDataEven);
            }
       }
    },


    setInstrumentDate:function(instrumentDate){
  
      let month = instrumentDate.dateReceived.substring(3,6);
      let day = instrumentDate.dateReceived.substring(0,2);
      let year = instrumentDate.dateReceived.substring(7,12);
      var dateFormat = "";
      let hour = instrumentDate.timeReceived.substring(0,2);
      let min = instrumentDate.timeReceived.substring(3,5);
      
      let firstPart = applicationManager.getFormatUtilManager().getTwelveHourTimeString(hour+': '+min);          
      let trdPart = month + ' ' + day;

      return firstPart + ' UTC ' + trdPart;

    },

    reloadProductDetails: function() {

      let paramsProdDetails = {
        "ISINCode": this.portfolioDetails.ISIN ? this.portfolioDetails.ISIN : this.productDetails.instrumentMinimal[0].ISINCode,
        "RICCode": this.ricCode,
        "instrumentId": this.productDetails.instrumentMinimal[0].instrumentId
      };
      scope_WealthPresentationController.instrumentAction = "Reload";
      applicationManager.getModulesPresentationController("WealthModule").getProductDetailsById(paramsProdDetails);
    },

    onBreakpointChange: function(form, width){
      responsiveUtils.onOrientationChange(this.onBreakpointChange);
      this.view.customheadernew.onBreakpointChangeComponent(width);
      this.view.customfooternew.onBreakpointChangeComponent(width);
      this.checkPermissions();
      this.getProductDetails(this.productDetails);
    },

    goBack: function(){
      applicationManager.getModulesPresentationController("WealthModule").setInstrumentCurrentPosition({});
      applicationManager.getModulesPresentationController("WealthModule").setInstrumentPricingData({});
      applicationManager.getModulesPresentationController("WealthModule").setInstrumentDetails({});
            
      applicationManager.getNavigationManager().setCustomInfo('frmInstrumentDetails',{});
            
      let ntf = new kony.mvc.Navigation("frmPortfolioOverview");
      ntf.navigate();
    },

    onSell: function(){
      var navManager = applicationManager.getNavigationManager();		
      var dataCustom = navManager.getCustomInfo('frmPlaceOrder');
      if(dataCustom === undefined){
        dataCustom={};
      }
      dataCustom = this.portfolioDetails;
      dataCustom.operation = "Sell";
      navManager.setCustomInfo('frmPlaceOrder', dataCustom);
      navManager.navigateTo("frmPlaceOrder");
    },

    onBuy: function(){
      var navManager = applicationManager.getNavigationManager();		
      var dataCustom = navManager.getCustomInfo('frmPlaceOrder');
      if(dataCustom === undefined){
        dataCustom={};
      }
      dataCustom = this.portfolioDetails;
      dataCustom.operation = "Buy";
      navManager.setCustomInfo('frmPlaceOrder', dataCustom);
      navManager.navigateTo("frmPlaceOrder");
    },
    ViewAllStockNews: function(){
      applicationManager.getModulesPresentationController("WealthModule").fetchNewsDetails();
    },
    chartService: function(filter) {
      var params = {
                
        "RICCode": this.ricCode,
        "dateOrPeriod": filter
      };
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.getHistoricalDataInstrument(params);
    },

    lineChartData: function(responseObj) {
      var graphData = responseObj.response.historicalData;
      if (graphData) {
        this.view.investmentLineChart.setVisibility(true);
        this.view.investmentLineChart.setChartData(graphData, null);
        this.view.flxInvestment.height = "926Dp";
        this.view.flxInvestmentSummary.height = "926Dp";
        this.view.flxInstrumentLineChart.height = "350Dp";
      } 
      else {
        this.view.investmentLineChart.setVisibility(false);
        this.view.flxInvestment.height = "550Dp";
        this.view.flxInvestmentSummary.height = "550Dp";
        this.view.flxInstrumentLineChart.height = "0Dp";
      }
    },

    chartFilters: {
      ONE_DAY: '1D',
      ONE_MONTH: '1M',
      ONE_YEAR: '1Y',
      YTD: 'YTD',
    },

    onFilterChanged: function(filter) {
      var filterMap = "";
      if (filter === this.chartFilters.ONE_DAY) {
        filterMap = "1D";
        this.chartService(filterMap);
      } else if (filter === this.chartFilters.ONE_MONTH) {
        filterMap = "1M";
        this.chartService(filterMap);
      } else if (filter === this.chartFilters.ONE_YEAR) {
        filterMap = "1Y";
        this.chartService(filterMap);
      } else {
        filterMap = "YTD";
        this.chartService(filterMap);
      }
    },
    /**
         *setActiveHeaderHamburger - Method to highlight active header and hamburger
         */
    setActiveHeaderHamburger: function() {
      this.view.customheadernew.activateMenu("Accounts", "My Accounts");
      this.view.customheadernew.flxContextualMenu.setVisibility(false);
      this.view.customheadernew.flxAccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
    },

    setFavourite: function(serviceResponse){    
      
      var favorite = false;
     
      if (serviceResponse.favInstrumentCodes){ 
        if(serviceResponse.favInstrumentCodes.split(':').find(element=>element===this.ricCode)){     
          favorite = true;
        }
      }

      if (serviceResponse.favInstrumentIds){ 
        if(serviceResponse.favInstrumentIds.split(':').find(element=>element===this.productDetails.instrumentMinimal[0].instrumentId)){     
          favorite = true;
        }
      }   

      if(favorite)  
        this.view.InstrumentDetails.imgStar.src = "activestar.png";  
      else
        this.view.InstrumentDetails.imgStar.src = "feedback_icon_grey.png";

    },

    updateFavList: function(operation){
      var params = {};
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");

      if (operation === "get") {

        wealthModule.getUserFavouriteInstruments(params);

      } else {

        if (this.view.InstrumentDetails.imgStar.src === "activestar.png"){
          this.view.InstrumentDetails.imgStar.src = "feedback_icon_grey.png";
          operation = 'Remove';
        } else {
          this.view.InstrumentDetails.imgStar.src="activestar.png";
          operation = 'Add';
        }

        params = {
          "RICCode": this.ricCode,
          "instrumentId":this.productDetails.instrumentMinimal[0].instrumentId,
          "operation": operation
        };
        wealthModule.updateFavouriteInstruments(params);
      }
    },

    checkPermissions: function(){

      var checkUserPermission = function (permission) {
        return applicationManager.getConfigurationManager().checkUserPermission(permission);
      }; 

      this.view.InstrumentDetails.flxButtons.setVisibility(applicationManager.getConfigurationManager().getInstrumentDetailsPermissions().some(checkUserPermission));
      this.view.CurrentPosition.setVisibility(applicationManager.getConfigurationManager().getInstrumentCurrentPositionPermissions().some(checkUserPermission));
      this.view.flxPriceData.setVisibility(applicationManager.getConfigurationManager().getInstrumentPricingDataPermissions().some(checkUserPermission));
      this.view.flxDocuments.setVisibility(applicationManager.getConfigurationManager().getInstrumentNewsPermissions().some(checkUserPermission));
      this.view.flxStocks.setVisibility(applicationManager.getConfigurationManager().getInstrumentDocumentsPermissions().some(checkUserPermission));
      this.view.InstrumentDetails.imgStar.setVisibility(applicationManager.getConfigurationManager().addToWatchlistPermissions().some(checkUserPermission));
      this.view.InstrumentDetails.btnSell.setVisibility(applicationManager.getConfigurationManager().sellOrderPermissions().some(checkUserPermission));
      this.view.InstrumentDetails.btnBuy.setVisibility(applicationManager.getConfigurationManager().buyOrderPermissions().some(checkUserPermission));

    },
    
    postShowWealthComp: function() {
      
    },
    
    
    refreshInstrumentDetails:function(response){
      
      scope_WealthPresentationController.instrumentAction = "";

      let instrument = response.instrumentDetails?response.instrumentDetails:response.instrumentMinimal[0];
    
     
      var instrumentDetailsDate = {};
    
      if(!response.instrumentDetails){
        this.view.InstrumentDetails.lblProfit.text = "";
        instrumentDetailsDate = {

          "timeReceived": ((instrument.timeReceived) ? instrument.timeReceived : "00:00:00"),
          "dateReceived": ((instrument.lastPriceDate) ? instrument.lastPriceDate : "")
        };

        this.view.InstrumentDetails.lblAmount.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(instrument.lastPrice, instrument.priceCurrency.toUpperCase());
        this.view.InstrumentDetails.lblTime.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf")+ " " + this.setInstrumentDate(instrumentDetailsDate);
   
      }else{
   
        instrumentDetailsDate = {

          "timeReceived": ((instrument.timeReceived) ? instrument.timeReceived : "00:00:00"),
          "dateReceived": ((instrument.dateReceived) ? instrument.dateReceived : "")
        };
        let lastPrice = (instrument.lastPrice === "0.0" || instrument.lastPrice === "0" )?instrument.closeRate:instrument.lastPrice;
        this.view.InstrumentDetails.lblAmount.text = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(lastPrice, instrument.referenceCurrency.toUpperCase());
        this.view.InstrumentDetails.lblTime.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf")+ " " + this.setInstrumentDate(instrumentDetailsDate);

        let netchange = applicationManager.getFormatUtilManager().formatAmount(instrument.netchange);
        let percentageChange = applicationManager.getFormatUtilManager().formatAmount(instrument.percentageChange);

        this.view.InstrumentDetails.lblProfit.text = ((netchange)?netchange:"") + ((percentageChange)?" (" + percentageChange + "%)":"");

        if (parseFloat(instrument.percentageChange) > 0) 
          this.view.InstrumentDetails.lblProfit.skin = "sknIW2F8523";
        else if (parseFloat(instrument.percentageChange) < 0)
          this.view.InstrumentDetails.lblProfit.skin = "sknlblff000015px";
        else
          this.view.InstrumentDetails.lblProfit.skin = "bbSknLbl424242SSP17Px";
      }
    }

  };
});