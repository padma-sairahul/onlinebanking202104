define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxPrimaryActions **/
    AS_FlexContainer_h81071a3f5a84195a8b05aa86b8d0a4e: function AS_FlexContainer_h81071a3f5a84195a8b05aa86b8d0a4e(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        ntf.navigate();
    },
    /** preShow defined for CopyfrmAccountsLanding **/
    AS_Form_a90f7f5d2f7d4d2a86095450b2a3b117: function AS_Form_a90f7f5d2f7d4d2a86095450b2a3b117(eventobject) {
        var self = this;
        this.preShowFrmAccountsLanding();
        this.setAccountListData();
    },
    /** onDeviceBack defined for CopyfrmAccountsLanding **/
    AS_Form_acc8d0327bfa4de5b89fa425aee3682c: function AS_Form_acc8d0327bfa4de5b89fa425aee3682c(eventobject) {
        var self = this;
        kony.print("Back Button is clicked");
    },
    /** onBreakpointChange defined for CopyfrmAccountsLanding **/
    AS_Form_b0119960810842129bd987911259675e: function AS_Form_b0119960810842129bd987911259675e(eventobject, breakpoint) {
        var self = this;
        this.onBreakpointChange(breakpoint);
    },
    /** postShow defined for CopyfrmAccountsLanding **/
    AS_Form_bcc29ebd7cf9499190aea85e4cff895a: function AS_Form_bcc29ebd7cf9499190aea85e4cff895a(eventobject) {
        var self = this;
        this.onLoadChangePointer();
        this.postShow();
        this.setContextualMenuLeft();
    },
    /** init defined for CopyfrmAccountsLanding **/
    AS_Form_cd4a800d4a604858ace0382480d20d9b: function AS_Form_cd4a800d4a604858ace0382480d20d9b(eventobject) {
        var self = this;
        this.initActions();
    },
    /** onTouchEnd defined for CopyfrmAccountsLanding **/
    AS_Form_gfca3df6897f408d83a9b7299fb1547f: function AS_Form_gfca3df6897f408d83a9b7299fb1547f(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onKeyUp defined for SelectBankOrVendor.tbxName **/
    AS_TextField_a42cbc8e8d3e4e98af769fcee233684d: function AS_TextField_a42cbc8e8d3e4e98af769fcee233684d(eventobject) {
        var self = this;
        this.onTextChangeOfExternalBankSearch();
    },
    /** onKeyUp defined for LoginUsingSelectedBank.tbxNewUsername **/
    AS_TextField_c5ff87198d6f410f95ec354e7d6ca1ee: function AS_TextField_c5ff87198d6f410f95ec354e7d6ca1ee(eventobject) {
        var self = this;
        this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
    },
    /** onKeyUp defined for LoginUsingSelectedBank.tbxEnterpassword **/
    AS_TextField_h83dd05882ef4dabbe473402cb170cec: function AS_TextField_h83dd05882ef4dabbe473402cb170cec(eventobject) {
        var self = this;
        this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
    },
    /** onFilterChanged defined for investmentLineChart **/
    AS_UWI_jf43b5e6a2b047fc87b07516c975b772: function AS_UWI_jf43b5e6a2b047fc87b07516c975b772(filter) {
        var self = this;
        return self.onFilterChanged.call(this, filter);
    }
});