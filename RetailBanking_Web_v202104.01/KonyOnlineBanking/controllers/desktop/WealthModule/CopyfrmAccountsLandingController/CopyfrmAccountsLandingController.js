define(['FormControllerUtility', 'ViewConstants', 'CommonUtilities', 'OLBConstants', 'CampaignUtility'], function(FormControllerUtility, ViewConstants, CommonUtilities, OLBConstants, CampaignUtility) {
  var orientationHandler = new OrientationHandler();
  return {
    /**
         * Method to patch update UI
         * @param {Object} uiData Data from presentation controller
         */
    accountsLength: "",
    time: 10,
    chartDefaultValue: "",
    updateFormUI: function(uiData) {
      if (uiData) {
        if (uiData.showLoadingIndicator) {
          if (uiData.showLoadingIndicator.status === true) {
            FormControllerUtility.showProgressBar(this.view)
          } else {
            FormControllerUtility.hideProgressBar(this.view)
          }
        }
        if (uiData.serviceError) {
          this.setServiceError(uiData.serviceError)
        }
        if (uiData.campaignRes) {
          this.campaignSuccess(uiData.campaignRes);
        }
        if (uiData.campaignError) {
          this.view.flxBannerContainerDesktop.setVisibility(false);
          this.view.flxBannerContainerMobile.setVisibility(false);
        }
        if (uiData.welcomeBanner) {
          this.updateProfileBanner(uiData.welcomeBanner)
        }
        if (uiData.accountsSummary) {
          this.updateAccountList(uiData.accountsSummary)
        }
        if (uiData.unreadCount) {
          this.updateAlertIcon(uiData.unreadCount);
        }
        if (uiData.UpcomingTransactions) {
          this.showUpcomingTransactionsWidget(uiData.UpcomingTransactions)
        }
        if (uiData.unreadMessages) {
          this.showMessagesWidget(uiData.unreadMessages)
        }
        if (uiData.PFMDisabled) {
          this.disablePFMWidget()
        }
        if (uiData.PFMMonthlyWidget) {
          this.getPFMMonthlyDonutChart(uiData.PFMMonthlyWidget)
        }
        if (uiData.outageMessage) {
          this.setOutageNotification(uiData.outageMessage.show, uiData.outageMessage.message);
        }
        if (uiData.passwordResetWarning) {
          this.setPasswordResetNotification(uiData.passwordResetWarning.show, uiData.passwordResetWarning.message);
        }
        if (uiData.savedExteranlAccountsModel) {
          this.presentExternalAccountsAddedConfirmation(uiData.savedExteranlAccountsModel);
        }
        if (uiData.externalBankLoginContext) {
          this.showExternalBankLogin(uiData.externalBankLoginContext);
        }
        if (uiData.externalBankLogin) {
          this.onExternalBankLoginSuccess(uiData.externalBankLogin);
        }
        if (uiData.externalBankLoginFailure) {
          this.onExternalBankLoginFailure(uiData.externalBankFailure);
        }
        if (uiData.saveExternalBankCredentialsSuccess) {
          this.onSuccessSaveExternalBankCredentailsSuccess(uiData.saveExternalBankCredentialsSuccess)
        }
        if (uiData.saveExternalBankCredentialsFailure) {
          this.onSuccessSaveExternalBankCredentailsFailure(uiData.saveExternalBankCredentialsFailure)
        }
        if (uiData.externalBankAccountsModel) {
          if (uiData.externalBankAccountsModel.length > 0) {
            this.presentExternalAccountsList(uiData.externalBankAccountsModel);
          } else {
            this.onAllExternalAccountsAlreadyAdded();
          }
        }
        if (uiData.TnCresponse) {
          this.bindTnC(uiData.TnCresponse);
        }
        if (uiData.errorMsg) {
          this.showError(uiData.errorMsg);
        }
        if (uiData.campaign) {
          CampaignUtility.showCampaign(uiData.campaign, this.view, "flxMain");
        }
        if(uiData.NewsList){
         // this.loadMarketNews(uiData.NewsList);
        }
        if(uiData.IndexList){
          //this.loadMarketIndex(uiData.IndexList);
        }
        if(uiData.ActivityList){
          // this.listActivity(uiData.ActivityList);
        }
        if(uiData.InvestmentAccountsData){
          var currForm = kony.application.getCurrentForm();
          currForm.forceLayout();
          this.loadInvestmentAccounts(uiData.InvestmentAccountsData);
        }
        if(uiData.InvestmentAccountsChart){
          var currForm = kony.application.getCurrentForm();
          currForm.forceLayout();
          this.lineChartData(uiData.InvestmentAccountsChart);
        }
        if(uiData.TotalAssets){
          this.listTotalAssets(uiData.TotalAssets);
        }
      }

    },



    selectInvestmentSummaryPeriod : function(widgetObject)
    {
      flexData = widgetObject.text;
      if(flexData == "1M")
      {
        this.view.investmentSummaryCard.flxSelector1.setVisibility(true);
        this.view.investmentSummaryCard.flxSelector2.setVisibility(false);
        this.view.investmentSummaryCard.flxSelector3.setVisibility(false);
        this.view.investmentSummaryCard.flxSelector4.setVisibility(false);
      }
      else if (flexData== "1Y")
      {
        this.view.investmentSummaryCard.flxSelector2.setVisibility(true);
        this.view.investmentSummaryCard.flxSelector1.setVisibility(false);
        this.view.investmentSummaryCard.flxSelector3.setVisibility(false);
        this.view.investmentSummaryCard.flxSelector4.setVisibility(false);            
      }
      else if (flexData== "5Y")
      {
        this.view.investmentSummaryCard.flxSelector3.setVisibility(true);
        this.view.investmentSummaryCard.flxSelector1.setVisibility(false);
        this.view.investmentSummaryCard.flxSelector2.setVisibility(false);
        this.view.investmentSummaryCard.flxSelector4.setVisibility(false);                 
      }
      else if (flexData == "YTD")
      {
        this.view.investmentSummaryCard.flxSelector4.setVisibility(true); 
        this.view.investmentSummaryCard.flxSelector1.setVisibility(false);
        this.view.investmentSummaryCard.flxSelector2.setVisibility(false);
        this.view.investmentSummaryCard.flxSelector3.setVisibility(false);              
      }
    },
    chartService: function(filter) {
      var customerId = userManager.getBackendIdentifier();
      var params = {
        "customerId": customerId,
        "graphDuration": filter
      };
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.getDashboardGraphDetails(params);
    },
    loadInvestmentAccounts: function(respData){
      var investmentAccData = [];
      var dataFromResponse = respData.response.PortfolioList.portfolioList;
 //     alert(JSON.stringify(dataFromResponse));
	  var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
        wealthModule.setAccountsListObj(dataFromResponse);
      for (var list in dataFromResponse) {
        var storeData;
        var forUtility = applicationManager.getFormatUtilManager();
        var maskAccountName = CommonUtilities.truncateStringWithGivenLength(dataFromResponse[list].accountName + "....", 26) + CommonUtilities.getLastFourDigit(dataFromResponse[list].accountNumber);
        var profitLossAmount = forUtility.formatAmountandAppendCurrencySymbol(dataFromResponse[list].unRealizedPLAmount, dataFromResponse[list].referenceCurrency);
        var accountBal = forUtility.formatAmountandAppendCurrencySymbol(dataFromResponse[list].marketValue, dataFromResponse[list].referenceCurrency);
        if (dataFromResponse[list].unRealizedPL === "L") {
          storeData = {
            accountName: maskAccountName,
            portfolioId: dataFromResponse[list].portfolioId,
            profitLossAmt: {
              "skin": "sknlblee0055IW13px",
              "text": "-" + profitLossAmount + " (" + dataFromResponse[list].unRealizedPLPercentage + "%" + ")"
            },
            accountBalance: accountBal,
            imageAccountType: {
              "src": "personalaccount.png",
              isVisible: true
            },
            accountType: dataFromResponse[list].investmentType,
            jointAccount: {
              "text": dataFromResponse[list].isJointAccount === "true" ? "Joint" : "",
              "isVisible": dataFromResponse[list].isJointAccount === "true" ? true : false
            },
            flx: {
              "onClick": function(event, context) {
                this.onInvestmentAccountSelect(event, context);
              }.bind(this)
            }
          }
        } else {
          storeData = {
            accountName: maskAccountName,
            portfolioId: dataFromResponse[list].portfolioId,
            profitLossAmt: {
              "skin": "sknLbl2F8523IWSemiBold",
              "text": "+" + profitLossAmount + " (+" + dataFromResponse[list].unRealizedPLPercentage + "%" + ")"
            },
            accountBalance: accountBal,
            imageAccountType: {
              "src": "personalaccount.png",
              isVisible: true
            },
            accountType: dataFromResponse[list].investmentType,
            jointAccount: {
              "text": dataFromResponse[list].isJointAccount === "true" ? "Joint" : "",
              "isVisible": dataFromResponse[list].isJointAccount === "true" ? true : false
            },
            flx: {
              "onClick": function(event, context) {
                this.onInvestmentAccountSelect(event, context);
              }.bind(this)
            }
          }
        }
        investmentAccData.push(storeData);
      }
      this.view.segInvestmentAccounts.widgetDataMap = {
        lblAccountName: "accountName",
        lblProfitBalance: "profitLossAmt",
        lblAccountBalance: "accountBalance",
        imgBankLogo: "imageAccountType",
        //flxBankIcon: "imageBank",
        lblInvestmentLogo: "accountType",
        lblJointAccountLogo: "jointAccount",
        flxInvestmentRow: "flx"
      };
      this.view.segInvestmentAccounts.data = investmentAccData;
    },
    lineChartData: function(responseObj) {
      var val = responseObj.response;
      var forUtility = applicationManager.getFormatUtilManager();
      var totalVal = forUtility.formatAmountandAppendCurrencySymbol(val.marketValue, val.referenceCurrency);
      var unrealizedPL = forUtility.formatAmountandAppendCurrencySymbol(val.unRealizedPLAmount, val.referenceCurrency);
      var todaysPL = forUtility.formatAmountandAppendCurrencySymbol(val.todayPLAmount, val.referenceCurrency);
      this.view.lblValueMarketValue.text = totalVal;
      var flxUnrealisedPL = this.view.flxUnrealisedPL;
      var lbllUnrealisedPL = this.view.lbllUnrealisedPL;
      var lblUnrealisedPLValue = this.view.lblUnrealisedPLValue;
      if (val.unRealizedPLAmount >= 0) {
        lblUnrealisedPLValue.skin = "IWLabelGreenText15Px";
        lblUnrealisedPLValue.text = "+" + unrealizedPL + " (+" + val.unRealizedPLPercentage + "%)";
      } else {
        lblUnrealisedPLValue.skin = "sknIblEE0005SSPsb45px";
        lblUnrealisedPLValue.text = "-" + unrealizedPL + " (-" + val.unRealizedPLPercentage + "%)";
      }
      if (val.todayPLAmount >= 0) {
        this.view.lblTodayPLValue.skin = "IWLabelGreenText15Px";
        this.view.lblTodayPLValue.text = "+" + todaysPL + " (+" + val.todayPLPercentage + "%)";
      } else {
        this.view.lblTodayPLValue.skin = "sknIblEE0005SSPsb45px";
        this.view.lblTodayPLValue.text = "-" + todaysPL + " (-" + val.todayPLPercentage + "%)";
      }
      var graphData = val.graphDuration;
      this.view.investmentLineChart.setChartData(graphData, null);
    },
    chartFilters: {
      ONE_MONTH: '1M',
      ONE_YEAR: '1Y',
      FIVE_YEARS: '5Y',
      YTD: 'YTD',
    },
    onFilterChanged: function(filter) {
      var filterMap = "";
      if (filter === this.chartFilters.ONE_MONTH) {
        filterMap = "OneM";
        this.chartService(filterMap);
      } else if (filter === this.chartFilters.ONE_YEAR) {
        filterMap = "OneY";
        this.chartService(filterMap);
      } else if (filter === this.chartFilters.FIVE_YEARS) {
        filterMap = "FiveY";
        this.chartService(filterMap);
      } else {
        filterMap = "YTD";
        this.chartService(filterMap);
      }

    },
    onInvestmentAccountSelect: function(event, context) {
      var navManager = applicationManager.getNavigationManager();
      var rowIndexValue = context.rowIndex;
      var Id = context.widgetInfo.data[rowIndexValue].portfolioId;
      if(scope_WealthPresentationController.jointAccountDetails.portfolioList[rowIndexValue].isJointAccount === "true") {
        scope_WealthPresentationController.isJointAccount = true;
      } else {
        scope_WealthPresentationController.isJointAccount = false;
      }
      this.goToPortfolioDetails(Id);
    },
    goToPortfolioDetails: function(portfolioId) {
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.setPortfolioId(portfolioId);
      scope_WealthPresentationController.isFirst = true;
     if(applicationManager.getConfigurationManager().checkUserFeature("PORTFOLIO")){
     applicationManager.getNavigationManager().navigateTo("frmPortfolioOverview");
      }
    },
    listInvestmentSummary: function(accounts){
      var totalSummary = [];
      var temp = JSON.stringify(accounts);
      var val=JSON.parse(temp);
      //  var summary = accounts.portfolioDetails;
      for(var i=0;i<val.length;i++){
        //   for(var i in summary){
        var c = accounts[i];
        //    var c = summary[i];
        var name = c.portfolio_id;
        var bal = c.PortfolioMV;
        var profit = c.PortfolioPL;
        //   var cur = c.RefCurrency;
        var cur = "USD";
        var forUtility = applicationManager.getFormatUtilManager();
        var currency = forUtility.getCurrencySymbol(cur);
        var data = {
          name : "Investment Account...." +name,
          bal : currency + bal,
          profit : currency + profit,
          jointAccount: {
            "text": dataFromResponse[list].isJointAccount === "true" ? "Joint" : "",
            "isVisible": dataFromResponse[list].isJointAccount === "true" ? true : false
          },
          logo: "Investment",
          imgLogo: {src: "bank_icon_citi.png"}
        };
        totalSummary.push(data);
      }
      this.view.investmentSummaryCard.segInvestmentAccountsList.widgetDataMap = {
        lblAccountName: "name",
        lblAccountBalance: "bal",
        lblProfitBalance: "profit",
        lblJointAccountLogo: "jointLogo",
        lblInvestmentLogo: "logo",
        imgBankLogo: "imgLogo"
      };
      this.view.investmentSummaryCard.segInvestmentAccountsList.setData(totalSummary);
      var currForm = kony.application.getCurrentForm();
      currForm.forceLayout();
    },
    listTotalAssets: function(consolidatedSummary){
      var data1 = [];
      var data2 = [];
      var data3 = [];
      var data4 = [];
      var legendData = [];
      var l=consolidatedSummary.TotalAssets;
      for (var i = 0; i <= 4; i++) {
        data1[i] = l[i].MVPortfolio + "%";
        data2[i] = l[i].MVPortfolio;
        data3[i] = l[i].AssetCode;
        var strMVAssetClass = l[i].MVAssetClass;
        data4[i] = strMVAssetClass;
        var s= CommonUtilities.formatCurrencyWithCommas(data4[i],false,"USD")
        var skinName= "sknIWLegendColor"+i;
        var legendMap = {
          flxColor: {
            skin: skinName
          },
          flxAssetClass:  data3[i],
          flxMarketValue: s,
        };
        legendData.push(legendMap);
      }
      this.view.totalAssetsCard.segLegend.widgetDataMap = {
        flxColor: "flxColor",
        flxAssetClass: "flxAssetClass",
        flxMarketValue: "flxMarketValue"
      };
      this.view.totalAssetsCard.segLegend.setData(legendData);
      //         	var data1 = ['50%','20%','16%','10%','4%'];
      //         	var data2 = [50,20,16,10,4];
      //         	var data3 = ["Cash","Stocks","Bond","Mutual Fund","Term Deposits"];
      // 			var data4 = ["$30,574.50","$12,129.20","$9,783.20","$6,773.10","$2,225.47"];
      // var dynamicChart = this.view.totalAssetsCard.totalAssetsChart.evaluateJavaScript("AddDonutChart(" + JSON.stringify(data1) + "," + JSON.stringify(data2) + "," + JSON.stringify(data3) + "," + JSON.stringify(data4) + ");");
    },
    /*   listActivity: function(ActivityList) {
var TotalActivity= [];
			var a=ActivityList.recentActivity;
			for(var l in a){
            var c = a[l];

				if(c.operationNature == "Buy"){
					Sent= "You purchased " + c.NoOfShares +" shares of " + c.instrumentName;
				}
				else{
				Sent= "You sold " + c.NoOfShares +" shares of " + c.instrumentName;
				}
             var time=c.operationDate;
      var b=time.substring(11,13);
      var today = new Date();
      var timeToday = today.getHours();
     var dd = String(today.getDate()).padStart(2, '0');
     var ddd=time.substring(8,10);
      if (dd == ddd) {
        var s = timeToday - b;
	var t = s + " hrs ago";
                } else {
					var sd=today;
				var ds= new Date(time);
const utc1 = Date.UTC(ds.getFullYear(), ds.getMonth(), ds.getDate());
  const utc2 = Date.UTC(sd.getFullYear(), sd.getMonth(), sd.getDate());
  var diff = Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));
                    var t = diff + " days ago";
                }
				if(l==0 || l==1 || l==2){
               var storeData = {
                    Sent: Sent,
                    Time: t,
					imgCircle: {src: "circle_blue_filled.png"},
				imgDummy: {isVisible: true},
                 imgDummy1: {isVisible: false}
                            };
                 TotalActivity.push(storeData);
				}
				else if(l==3){
                var storeData = {
                    Sent: Sent,
                    Time: t,
					imgCircle: {src: "circle_blue_filled.png"},
                   imgDummy1: {isVisible: false}
                            };
                    TotalActivity.push(storeData);
				}                             


			}
	this.view.recentActivity.segActivity.widgetDataMap = {
                lblContinue: "Sent",
                lblTime: "Time",
				imgCircle: "imgCircle",
				imgDummy: "imgDummy"

            }
            this.view.recentActivity.segActivity.setData(TotalActivity);
		},*/
    
    listIndex: function(IndexList) {
      var indexobj11={};
      var indexobj2={};
      var indexobj3={};
      var a= IndexList.Names;
      var b=a[0].Name;
      for(var l in b){
        var c=b[l].Fields;

        var d=c.Field;
        for (var no in d){
          if(d[no].Name == "CF_NAME")
          {
            indexobj1= {name: d[no].Utf8String};
            if(l==0)
            {			 Object.assign(indexobj11, indexobj1);
            }
            else if(l==1){
              Object.assign(indexobj2, indexobj1);
            }
            else{			 Object.assign(indexobj3, indexobj1);
                }

          }
          else if(d[no].Name == "CF_CLOSE")
          {
            indexobj1= {closeRate: d[no].Double};
            if(l==0)
            {			 Object.assign(indexobj11, indexobj1);
            }
            else if(l==1){
              Object.assign(indexobj2, indexobj1);
            }
            else{			 Object.assign(indexobj3, indexobj1);
                }
          }
          else if(d[no].Name == "PCTCHNG")
          {
            indexobj1= {percentChange: d[no].Double};
            if(l==0)
            {			 Object.assign(indexobj11, indexobj1);
            }
            else if(l==1){
              Object.assign(indexobj2, indexobj1);
            }
            else{			 Object.assign(indexobj3, indexobj1);
                }
          }
          else if(d[no].Name == "CF_NETCHNG")
          {
            indexobj1= {netChange: d[no].Double};
            if(l==0)
            {			 Object.assign(indexobj11, indexobj1);
            }
            else if(l==1){
              Object.assign(indexobj2, indexobj1);
            }
            else{			 Object.assign(indexobj3, indexobj1);
                }
          }
          else if(d[no].Name == "CF_CURRENCY")
          {
            var ac=d[no].Utf8String;
            if (ac=="USD")
            {ac="$";
            }
            indexobj1= {currency: ac };
            if(l==0)
            {			 Object.assign(indexobj11, indexobj1);
            }
            else if(l==1){
              Object.assign(indexobj2, indexobj1);
            }
            else{			 Object.assign(indexobj3, indexobj1);
                }
          }}
      }
      if(indexobj11.percentChange<0){
      //  this.view.marketIndex.lblChange.skin="sknlblee0055IW13px";
      }
      if(indexobj2.percentChange<0){
       // this.view.marketIndex.CopylblChange0aaa00601b36246.skin="sknlblee0055IW13px";
      }
      if(indexobj3.percentChange<0){
      //  this.view.marketIndex.CopylblChange0fb3be785321d42.skin="sknlblee0055IW13px";
      }
      var navMan = applicationManager.getNavigationManager();
      var data = navMan.getCustomInfo('frmTopNews');
      if (data == undefined) {
        data = {};
      }
      data.indexobj1 = indexobj11;
      data.indexobj2 = indexobj2;
      data.indexobj3 = indexobj3;
      navMan.setCustomInfo('frmTopNews', data);

    //  this.view.marketIndex.lblTitle.text= indexobj11.name;
    //  this.view.marketIndex.lblChange.text= indexobj11.currency +indexobj11.netChange.toLocaleString() +" ("+indexobj11.percentChange.toFixed(2) +"%)";
    //  this.view.marketIndex.lblValue.text= indexobj11.currency +indexobj11.closeRate.toLocaleString();
   //   this.view.marketIndex.CopylblTitle0ef672967b4ee42.text= indexobj2.name;
   //   this.view.marketIndex.CopylblChange0aaa00601b36246.text= indexobj2.currency +indexobj2.netChange.toLocaleString() +" ("+indexobj2.percentChange.toFixed(2) +"%)";
   //   this.view.marketIndex.CopylblValue0c1edac75bf5448.text= indexobj2.currency +indexobj2.closeRate.toLocaleString();
   //   this.view.marketIndex.CopylblTitle0b7443f112ccb43.text= indexobj3.name;
   //   this.view.marketIndex.CopylblChange0fb3be785321d42.text= indexobj3.currency +indexobj3.netChange.toLocaleString() +" ("+indexobj3.percentChange.toFixed(2) +"%)";
   //   this.view.marketIndex.CopylblValue0idfe1df65b1042.text= indexobj3.currency +indexobj3.closeRate.toLocaleString();
    },
    
    
    /*
    listNewHighligths:function(HeadNews){
      var TotalNews=[];
      var temp= HeadNews["HLL"];
      for (var TitleNo in temp){
        var c= temp[TitleNo];
        if(c.Title!=undefined){
          var g= c.Description;
          var time=c.Time;
          var b=time.substring(11,13);
          var today = new Date();
          var timeToday = today.getHours();
          var dd = String(today.getDate()).padStart(2, '0');
          var ddd=time.substring(8,10);
          if(dd==ddd){
            var s=timeToday-b;
          }
          else{
            var s= 24-b+timeToday;
          }
          var t=s+"h ago";
          var storeData = {
            Title: c.Title,
            Id: c.Id,
            Description: g,
            Time: t,
            Publisher: c.Publisher
          };



          if(TitleNo==0){

          }
          else{        
            TotalNews.push(storeData);}
        }}
      this.view.marketNews.segNews.widgetDataMap = { 
        lblContent: "Title",
        lblId: "Id",
        lblName: "Publisher",
        lblTime: "Time",
        lblDes: "Description"
      }
      this.view.marketNews.segNews.setData(TotalNews);
      var currForm = kony.application.getCurrentForm();
      currForm.forceLayout();
    }, 
	*/
    /**
 * Method for refreshing the market index list
 */     
//    loadMarketIndex: function(respData) {
      
//      this.view.flxMarketNewsContainer.marketIndex.setDataMarket(respData.response);
      
//       var segData = [];
//       var newsDetails = [];
//       newsDetails = respData.response;
//       var result = [];
//       var inresult = [];
//       var innerdata = {};
//       for (var i in newsDetails) {
//         var subArray = [];
//         subArray = newsDetails[i].Fields.Field;
//         innerdata = {};
//         for (var j in subArray) {
//           var dt = subArray[j].DataType;
//           var value = subArray[j][dt];
//           var keyA = subArray[j].Name;
//           var data = {};
//           innerdata[keyA] = value;
//         }
//         inresult.push(innerdata);
//       }
//       for (var list in inresult) {
//         var storeData;
//         var change = inresult[list].CF_NETCHNG;
//         var percent = parseFloat(inresult[list].PCTCHNG).toFixed(2);
//         var forUtility = applicationManager.getFormatUtilManager();
//         var balance = forUtility.formatAmountandAppendCurrencySymbol(inresult[list].CF_LAST, inresult[list].CF_CURRENCY);
//         if (inresult[list].CF_NETCHNG < 0) {
//           storeData = {
//             marketName: inresult[list].CF_NAME,
//             amount: balance,
//             profitLoss: {
//               "skin": "sknlblee0055IW13px",
//               "text": change + " (" + percent + "%" + ")"
//             },
//           }
//         } else {
//           storeData = {
//             marketName: inresult[list].CF_NAME,
//             amount: balance,
//             profitLoss: {
//               "skin": "sknLbl2F8523IWSemiBold",
//               "text": "+" + change + " (" + "+" + percent + "%" + ")"
//             },

//           }
//         }
//         segData.push(storeData);
//       }
//       this.view.marketIndex.lblTitle.text= segData[0].marketName;
//       this.view.marketIndex.lblChange.text= segData[0].profitLoss.text;
//       this.view.marketIndex.lblChange.skin = segData[0].profitLoss.skin;
//       this.view.marketIndex.lblValue.text= segData[0].amount;
//       this.view.marketIndex.CopylblTitle0ef672967b4ee42.text= segData[1].marketName;
//       this.view.marketIndex.CopylblChange0aaa00601b36246.text= segData[1].profitLoss.text;
//       this.view.marketIndex.CopylblChange0aaa00601b36246.skin= segData[1].profitLoss.skin;
//       this.view.marketIndex.CopylblValue0c1edac75bf5448.text= segData[1].amount;
//       this.view.marketIndex.CopylblTitle0b7443f112ccb43.text= segData[2].marketName;
//       this.view.marketIndex.CopylblChange0fb3be785321d42.text= segData[2].profitLoss.text;
//       this.view.marketIndex.CopylblChange0fb3be785321d42.skin= segData[2].profitLoss.skin;
//       this.view.marketIndex.CopylblValue0idfe1df65b1042.text= segData[2].amount;
//       var currForm = kony.application.getCurrentForm();
//       currForm.forceLayout();
//    },     
    /**
         * Method for refreshing the market news
         */
    /*
    loadMarketNews: function(data1){
      var data = data1.GetSummaryByTopic_Response_1.StoryMLResponse.STORYML;
      var news;
      news = data.HL;
      var results=[];
      for(var num in news){
        if (num == 4) {
          break;
        };
        var storeData;
        var data=news[num];
        var time;
        time = data.LT.split('T');
        var today = new Date();
        var curDate = today.getFullYear() + "-" +("0" + (today.getMonth()+1)).slice(-2) + "-" + ("0" + today.getDate()).slice(-2);
        var curHrs = new Date().getHours();
        var RT;
        if (time[0] !== curDate) {
          var date= time[0].split('-');
          RT = date[1]+'/'+date[2]+'/'+date[0];
        } 
        else {
          var hours = time[1].split(':')[0];
          if(curHrs >= hours){
            RT = curHrs - hours + ' ' + 'Hours ago';
          }
          else{
            RT = hours - curHrs + ' ' + 'Hours ago';
          }
        }
        if(RT == "0 Hours ago"){
          RT="Few minutes ago";
        }
        else if(RT == "1 Hours ago"){
          RT="1 Hour ago";
        }
        storeData={
          Headline:data.HT,
          Time: RT,
          Provider: data.PR,
          ID: data.ID,
          detailedNews: data.TE
        }
        results.push(storeData);
      }
      this.view.marketNews.segNews.widgetDataMap={

        lblId: "ID",
        lblName: "Provider",
        lblTime: "Time",
        lblContent: "Headline"
      };
      this.view.marketNews.segNews.setData(results); 
      var currForm = kony.application.getCurrentForm();
      currForm.forceLayout();
    },    
    */     
    /**
         * Method for refreshing the campaigns using timer
         */
    startTimertoUpdateAds: function() {
      kony.timer.schedule("RefreshCampaign", this.timerFunction, 1, true);
    },
    timerFunction: function() {
      this.time = this.time - 1;
      if (this.time === 0) {
        this.loadAccountModule().presentationController.getAccountDashboardCampaignsOnBreakpointChange();
        kony.timer.cancel("RefreshCampaign");
      }
    },
    /**
         * Method which is called if the response is succesful.
         * @params {object} -  contains campaign data
         */
    campaignSuccess: function(data) {
      var CampaignManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('CampaignManagement');
      CampaignManagementModule.presentationController.updateCampaignDetails(data);
      var self = this;
      if (data.length === 0) {
        this.view.flxBannerContainerMobile.setVisibility(false);
        this.view.flxBannerContainerDesktop.setVisibility(false);
      } else if (kony.application.getCurrentBreakpoint() >= 1366 && !orientationHandler.isMobile && !orientationHandler.isTablet) {
        this.view.flxBannerContainerDesktop.setVisibility(true);
        this.view.flxBannerContainerMobile.setVisibility(false);
        this.view.imgBannerDesktop.src = data[0].imageURL;
        this.view.imgBannerDesktop.onTouchStart = function() {
          CampaignUtility.onClickofInAppCampaign(data[0].destinationURL);
        };
      } else {
        var self = this;
        this.view.flxBannerContainerMobile.setVisibility(true);
        this.view.flxBannerContainerDesktop.setVisibility(false);
        this.view.imgBannerMobile.src = data[0].imageURL;
        this.view.imgBannerMobile.onTouchStart = function() {
          CampaignUtility.onClickofInAppCampaign(data[0].destinationURL);
        };
      }
      this.AdjustScreen();
    },

    /**
         * Method to init frmAccountsLanding
         */
    initActions: function() {

      FormControllerUtility.setRequestUrlConfig(this.view.brwBodyTnC);
      var config = applicationManager.getConfigurationManager();
      this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule');
      this.loadAccountModule().presentationController.loadWealthComponents();
      this.chartDefaultValue = this.view.investmentLineChart.currentFilter;
    },
    /**  Returns height of the page
         * @returns {String} height height of the page
         */
    getPageHeight: function() {
      var height = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height + this.view.flxFooter.info.frame.height + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT;
      return height + ViewConstants.POSITIONAL_VALUES.DP;
    },
    /**
         * Method to load and return Messages and Alerts Module.
         * @returns {object} Messages and Alerts Module object.
         */
    loadAccountModule: function() {
      return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    },
    /**
         * initlizeDonutChart: Method is used to initlize the DonutChart for custom widget
         */
    initlizeDonutChart: function() {
      FormControllerUtility.updateWidgetsHeightInInfo(this, ['mySpending.flxMySpending']);

      var data = [];
      var options = {
        height: 310,
        width: this.view.mySpending.flxMySpending.info.frame.width,
        position: 'top',
        chartArea: {
          right: 150,
          left: 20
        },
        title: '',
        pieHole: 0.6,
        pieSliceText: 'none',
        toolTip: {
          text: 'percentage'
        },
        colors: ["#FEDB64", "#E87C5E", "#6753EC", "#E8A75E", "#3645A7", "#04B6DF", "#8ED174", "#D6B9EA", "#B160DC", "#23A8B1"]
      };
      var donutChart = new kony.ui.CustomWidget({
        "id": "donutChart1",
        "isVisible": true,
        "left": "1" + ViewConstants.POSITIONAL_VALUES.DP,
        "top": "0" + ViewConstants.POSITIONAL_VALUES.DP,
        "width": "500" + ViewConstants.POSITIONAL_VALUES.DP,
        "height": "390" + ViewConstants.POSITIONAL_VALUES.DP,
        "zIndex": 1000000
      }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
      }, {
        "widgetName": "DonutChart",
        "chartData": data,
        "chartProperties": options,
        "OnClickOfPie": function() {}
      });
      this.view.flxSecondaryDetails.mySpending.flxMySpendingWrapper.flxMySpending.add(donutChart);
    },
    /**
         * Method that called on preshow of frmAccountsLanding
         */
    preShowFrmAccountsLanding: function() {
      this.checkPermission();
      let filterValues = Object.keys(this.chartFilters).map(key => this.chartFilters[key]);
      this.view.investmentLineChart.setChartFilters(filterValues);
      this.view.investmentLineChart.currentFilter = this.chartDefaultValue;
      var scopeObj = this;
      applicationManager.getLoggerManager().setCustomMetrics(this, false, "Accounts");
      this.view.onBreakpointChange = function() {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      }
      var navManager = applicationManager.getNavigationManager();
      //if ((navManager.getPreviousForm() === "frmLogin") || (navManager.getPreviousForm() === "frmMFAPreLogin"))
      //  this.startTimertoUpdateAds();
      this.view.accountListMenu.setVisibility(false);
      this.view.FavouriteAccountTypes.setVisibility(false);
      this.view.customheader.headermenu.lblNewNotifications.setVisibility(false);
      this.view.customheader.headermenu.imgNotifications.src = ViewConstants.IMAGES.NOTIFICATION_ICON;
      this.view.customheader.customhamburger.activateMenu("Accounts", "My Accounts");
      scopeObj.view.flxDowntimeWarning.setVisibility(false);
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
      this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
      //resetting MasterData
      this.view.mySpending.lblOverallSpendingAmount.isVisible = false; //Setting Visiblity of PFM false to show no masterData
      this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(true);
      this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(false);
      this.view.flxSecondaryDetails.setVisibility(true);

      FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader',
                                                             'flxMain',
                                                             'flxFooter',
                                                             'mySpending.flxMySpending',
                                                             'flxAccountListAndBanner',
                                                             'flxLeftContainer',
                                                             'flxMainWrapper',
                                                             'customheader',
                                                             'accountListMenu'
                                                            ]);

      var showFilterPopup = function() {
        if (scopeObj.view.FavouriteAccountTypes.origin) {
          scopeObj.view.FavouriteAccountTypes.origin = false;
          return;
        }
        if (scopeObj.view.FavouriteAccountTypes.isVisible == false) {
          var Popuptop = scopeObj.view.flxAccountListAndBanner.info.frame.y + scopeObj.view.flxLeftContainer.info.frame.y + 30;
          Popuptop = Popuptop.toString();
          scopeObj.view.FavouriteAccountTypes.top = Popuptop + ViewConstants.POSITIONAL_VALUES.DP;
          var currBreakpoint = kony.application.getCurrentBreakpoint();
          if (currBreakpoint === 640 || orientationHandler.isMobile) {
            scopeObj.view.FavouriteAccountTypes.left = "";
            scopeObj.view.FavouriteAccountTypes.right = "10dp";
          } else if (currBreakpoint === 1024 || orientationHandler.isTablet) {
            scopeObj.view.FavouriteAccountTypes.left = "";
            scopeObj.view.FavouriteAccountTypes.right = "24dp";
          } else {
            scopeObj.view.FavouriteAccountTypes.right = "";
            //ARB-9990 : Temporary solution to fix for 1366 windows.
            var userAgent = kony.os.deviceInfo().userAgent;
            if (userAgent.indexOf("Macintosh") != -1 && kony.application.getCurrentBreakpoint() == 1366)
              scopeObj.view.FavouriteAccountTypes.left = scopeObj.view.flxMainWrapper.info.frame.x + (scopeObj.view.flxLeftContainer.info.frame.x + scopeObj.view.flxLeftContainer.info.frame.width - 340) + "dp";
            else if (userAgent.indexOf("Macintosh") == -1 && kony.application.getCurrentBreakpoint() == 1366)
              scopeObj.view.FavouriteAccountTypes.left = scopeObj.view.flxMainWrapper.info.frame.x + (scopeObj.view.flxLeftContainer.info.frame.x + scopeObj.view.flxLeftContainer.info.frame.width - 356) + "dp";
            else
              scopeObj.view.FavouriteAccountTypes.left = scopeObj.view.flxMainWrapper.info.frame.x + (scopeObj.view.flxLeftContainer.info.frame.x + scopeObj.view.flxLeftContainer.info.frame.width - 340) + "dp";
          }
          scopeObj.view.accountListMenu.isVisible = false;
          var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
          scopeObj.view.accountList.lblImgDropdown.text = "P";
          scopeObj.view.FavouriteAccountTypes.isVisible = true;
          scopeObj.view.FavouriteAccountTypes.imgToolTip.setFocus(true);
        } else {
          var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
          scopeObj.view.accountList.lblImgDropdown.text = "O";
          scopeObj.view.FavouriteAccountTypes.isVisible = false;
        }
        scopeObj.view.FavouriteAccountTypes.forceLayout();
        scopeObj.view.accountList.forceLayout();
        scopeObj.AdjustScreen();
      };
      this.view.accountList.flxAccountsRightContainer.onTouchStart = function() {
        if (scopeObj.view.FavouriteAccountTypes.isVisible) {
          scopeObj.view.FavouriteAccountTypes.origin = true;
          if (kony.application.getCurrentBreakpoint() == 640 || kony.application.getCurrentBreakpoint() == 1024) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            scopeObj.view.accountList.lblImgDropdown.text = "O";
            scopeObj.view.FavouriteAccountTypes.isVisible = false;
            scopeObj.AdjustScreen();
          }
        }
      }
      this.view.accountList.flxAccountsRightContainer.onClick = showFilterPopup;
      this.view.btnContactUs.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.footer.contactUs"));
      this.view.btnContactUs.onClick = function() {
        FormControllerUtility.showProgressBar(this.view);
        this.loadAccountModule().presentationController.showContactUs();
      }.bind(this);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.imgViewPassword.onTouchStart = this.showPassword;
      this.view.lblImgCloseDowntimeWarning.onTouchEnd = function() {
        function timerFunc() {
          scopeObj.setServiceError(false);
          kony.timer.cancel("mytimerdowntime");
        }
        kony.timer.schedule("mytimerdowntime", timerFunc, 0.1, false);
      }.bind(this);
      if (CommonUtilities.isCSRMode()) {
        //new user onboarding
        this.view.btnOpenNewAccount.onClick = CommonUtilities.disableButtonActionForCSRMode();
        this.view.btnOpenNewAccount.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(15);
        //write a new message
        this.view.myMessages.btnWriteNewMessage.onClick = CommonUtilities.disableButtonActionForCSRMode();
        this.view.myMessages.btnWriteNewMessage.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(15);
        this.view.myMessages.btnWriteNewMessage.hoverSkin = CommonUtilities.disableSegmentButtonSkinForCSRMode(15);
        this.view.myMessages.btnWriteNewMessage.focusSkin = CommonUtilities.disableSegmentButtonSkinForCSRMode(15);
      } else {
        //new user onboarding
        this.view.btnOpenNewAccount.onClick = this.loadAccountModule().presentationController.navigateToNewAccountOpening.bind(this.loadAccountModule().presentationController);
        //write a new message
        this.view.myMessages.btnWriteNewMessage.onClick = this.loadAccountModule().presentationController.newMessage.bind(this.loadAccountModule().presentationController);
      }
      if (!applicationManager.getConfigurationManager().checkUserPermission("OPEN_NEW_ACCOUNT")) {
        // hiding Open New Account btn for non retail banking user
        this.view.btnOpenNewAccount.setVisibility(false);
        this.view.flxSeparator.setVisibility(false);
        this.view.flxPrimaryActions.height = "50dp";
        this.view.flxPrimaryActions.forceLayout();
      }
      applicationManager.executeAuthorizationFramework(this);
      applicationManager.getNavigationManager().applyUpdates(this);
      CampaignUtility.fetchPopupCampaigns();
     // this.view.marketIndex.lblView.onTouchEnd=this.ViewAllMarketNews;
      this.view.investmentSummaryCard.segInvestmentAccountsList.onRowClick=this.onRowclickInvestmentAccount;

      this.view.totalAssetsCard.totalAssetsChart.onSuccess=this.chartLoad;
      this.view.totalAssetsCard.totalAssetsChart.onPageFinished=this.chartLoad;
    },
    getPortfolioList: function() {
      var userManager = applicationManager.getUserPreferencesManager();
      var customerId = userManager.getBackendIdentifier();
      var params = {
        "customerId": customerId
      };
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.getPortfolioList(params);
    },
    getAssetsList: function() {
      var userManager = applicationManager.getUserPreferencesManager();
      var customerId = userManager.getBackendIdentifier();
      var params = {
        "customerId": customerId
      };
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.getAssetsList(params);
    },     
    loadAssetsDetails: function(resData) {
      this.setGraphData(resData);
    },
    chartLoad: function(){
      var data1 = ['20%','50%','16%','10%','4%'];
      var data2 = [20,50,16,10,4];
      var data3 = ["Cash","Stocks","Bond","Mutual Fund","Term Deposits"];
      var data4 = ["$30,574.50","$12,129.20","$9,783.20","$6,773.10","$2,225.47"];
      var dynamicChart = this.view.totalAssetsCard.totalAssetsChart.evaluateJavaScript("AddDonutChart(" + JSON.stringify(data1) + "," + JSON.stringify(data2) + "," + JSON.stringify(data3) + "," + JSON.stringify(data4) + ");");
    },
    onRowclickInvestmentAccount:function(){
      var navManager = applicationManager.getNavigationManager();

      navManager.navigateTo("frmPortfolioOverview");       
    },
    
    /*ViewAllMarketNews:function(){
      
      let wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.fetchNewsDetails();
      
    },*/
    postShow: function() {
      this.view.customheader.forceCloseHamburger();
    },
    /**
         * This function gets executed if Stop Check Request Permission is absent for the user.
         */
    removeStopCheckAction: function() {
      var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
      var actionsObject = OLBConstants.CONFIG.ACCOUNTS_QUICK_ACTIONS;
      this.modifyObject(actionsObject);
      actionsObject = OLBConstants.CONFIG.ACCOUNTS_SECONDARY_ACTIONS;
      this.modifyObject(actionsObject);
    },

    modifyObject: function(actionsObject) {
      for (var account in actionsObject) {
        if (actionsObject.hasOwnProperty(account)) {
          actionsObject[account] = actionsObject[account].filter(function(action) {
            return (action !== OLBConstants.ACTION.STOPCHECKS_PAYMENT);
          });
        }
      }
    },

    removeViewChequeAction: function() {

    },
    removeChequeRequestAction: function() {

    },

    /**
         * This function gets executed if Stop Cheque Request Permission is present for the user.
         */
    addStopCheckAction: function() {

    },
    /**
         * This function gets executed if Create Cheque Book Request Permission is present for the user.
         */
    addCheckBookRequestAction: function() {

    },
    /**
         * This function gets executed if View Cheques Permission is present for the user.
         */
    viewMyChequesAction: function() {

    },

    /**
         * This function shows the masked password on click of the eye icon
         */
    showPassword: function() {
      if (this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.secureTextEntry === true) {
        this.view.AddExternalAccounts.LoginUsingSelectedBank.imgViewPassword.text = "h";
        this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.secureTextEntry = false;
      } else {
        this.view.AddExternalAccounts.LoginUsingSelectedBank.imgViewPassword.text = "i";
        this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.secureTextEntry = true;
      }
    },
    /**
         *
         */
    setAccountListData: function() {},
    /**
         *
         */
    onLoadChangePointer: function() {
      this.view.customheader.imgKony.setFocus(true);
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.myMessages.btnWriteNewMessage, kony.i18n.getLocalizedString("i18n.AccountsLanding.Compose"), accessibilityConfig);
    },
    /**
         *
         */
    setContextualMenuLeft: function() {
      this.AdjustScreen();
    },
    /**
         * Ui team proposed method to handle screen aligment
         */
    AdjustScreen: function() {


      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.info.frame.height + this.view.flxMain.info.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.info.frame.height;
        if (diff > 0) {
          this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;
        } else {
          this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
        }
      } else {
        this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
      }
      this.view.forceLayout();
      CampaignUtility.onBreakpointChange(this.view.campaignpopup, "flxMain");
      this.initializeResponsiveViews();
    },
    /**
         * Method to set error message if service call fails
         * @param {boolean} status true/false
         */
    setServiceError: function(status) {
      if (status) {
        this.view.flxDowntimeWarning.setVisibility(true);
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(this.view.lblDowntimeWarning, kony.i18n.getLocalizedString("i18n.common.OoopsServerError"), accessibilityConfig);
        this.view.lblDowntimeWarning.setFocus(true);
      } else {
        this.view.flxDowntimeWarning.setVisibility(false);
      }
      this.AdjustScreen();
    },
    showError: function(errorMsg) {
      if (errorMsg) {
        this.view.flxDowntimeWarning.setVisibility(true);
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(this.view.lblDowntimeWarning, errorMsg, accessibilityConfig);
        this.view.lblDowntimeWarning.setFocus(true);
      } else {
        this.view.flxDowntimeWarning.setVisibility(false);
      }
      this.AdjustScreen();
    },
    onExternalBankLoginSuccess: function(response) {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      var username = this.ExternalLoginContextData.username;
      var password = this.ExternalLoginContextData.password;
      var sessionToken = response.params.SessionToken;
      var mainUser = applicationManager.getUserPreferencesManager().getCurrentUserName();
      var bankId = this.ExternalLoginContextData.bankId;
      authModule.presentationController.saveExternalBankCredentials(username, password, sessionToken, mainUser, bankId);
    },
    onExternalBankLoginFailure: function(response) {
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.lblLoginUsingSelectedBankError, kony.i18n.getLocalizedString("i18n.login.failedToLogin"), accessibilityConfig);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankError.isVisible = true;
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword, "", accessibilityConfig);
      this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
      CommonUtilities.hideProgressBar(this.view);
      this.AdjustScreen();
    },
    /**
         * Method to show outage message
         * @param {Boolean} isOutage true/false
         * @param {String} outageMessage Message to show
         */
    setOutageNotification: function(isOutage, outageMessage) {
      var scopeObj = this;
      var outageUI = scopeObj.view.flxOutageWarning;
      var displayMessage = "";
      if (outageMessage && outageMessage.length > 0) {
        outageMessage.forEach(function(message) {
          displayMessage = displayMessage + message + "\n";
        })
      }
      if (isOutage && !outageUI.isVisible) {
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(scopeObj.view.lblOutageWarning, displayMessage, accessibilityConfig);
        scopeObj.view.lblImgCloseOutageWarning.onTouchEnd = function() {
          scopeObj.setOutageNotification(false);
        };
        outageUI.setVisibility(true);
        scopeObj.view.lblOutageWarning.setFocus(true);
        isOutage = false;
        scopeObj.AdjustScreen();
      } else if (!isOutage && outageUI.isVisible) {
        function timerFunc() {
          outageUI.setVisibility(false);
          var acctop = scopeObj.view.accountListMenu.info.frame.y + 6;
          scopeObj.view.accountListMenu.top = acctop + "dp";
          scopeObj.view.forceLayout();
          kony.timer.cancel("mytimerOuttage");
        }
        kony.timer.schedule("mytimerOuttage", timerFunc, 0.1, false);
      }
    },
    setPasswordResetNotification: function(isWarningRequired, warningMessage) {
      var scopeObj = this;
      if (isWarningRequired && warningMessage.passwordExpiryWarningRequired === "true") {
        scopeObj.view.flxPasswordResetWarning.setVisibility(true);
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(scopeObj.view.lblPasswordResetWarning, kony.i18n.getLocalizedString("i18n.accounts.passwordreset") + " " + warningMessage.passwordExpiryRemainingDays + " " + kony.i18n.getLocalizedString("i18n.accounts.days") + " " + kony.i18n.getLocalizedString("i18n.accounts.resetPasswordWarning"), accessibilityConfig);
        scopeObj.view.lblImgClosePasswordResetWarning.onTouchEnd = function() {
          function timerFunc() {
            scopeObj.view.flxPasswordResetWarning.setVisibility(false);
            scopeObj.AdjustScreen();
            kony.timer.cancel("mytimerPassword");
          }
          kony.timer.schedule("mytimerPassword", timerFunc, 0.1, false);
        };
      } else {
        scopeObj.view.flxPasswordResetWarning.setVisibility(false);
        var acctop = scopeObj.view.accountListMenu.info.frame.y + 6;
        scopeObj.view.accountListMenu.top = acctop + "dp";
        scopeObj.AdjustScreen();
      }
      scopeObj.AdjustScreen();
      scopeObj.view.forceLayout();
    },
    /**
         * Method updates the frmAccountsDashboard with the user's first name, profile image & last logged
         * @param {JSON} profileBannerData Data of user like login time, name etc
         */
    updateProfileBanner: function(profileBannerData) {
      //  this.view.accountList.segAccounts.setData([]);
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.welcome.lblWelcome, kony.i18n.getLocalizedString('i18n.accounts.welcome') + ' ' + profileBannerData.userfirstname + '!', accessibilityConfig);
      CommonUtilities.setText(this.view.welcome.lblLastLoggedIn, kony.i18n.getLocalizedString('i18n.accounts.lastLoggedIn') + ' ' + profileBannerData.lastlogintime, accessibilityConfig);
      if (applicationManager.getConfigurationManager().getProfileImageAvailabilityFlag() === true && profileBannerData.userImageURL && profileBannerData.userImageURL.trim() != "")
        this.view.welcome.imgProfile.base64 = profileBannerData.userImageURL;
      else
        this.view.welcome.imgProfile.src = ViewConstants.IMAGES.USER_GREY_IMAGE;

    },
    /**
         * Returns if a account is favourite or not
         * @param {JSON} account Account whose favourite status needs to be checked
         * @returns {boolean} true/false
         */
    isFavourite: function(account) {
      return account.favouriteStatus && account.favouriteStatus === '1';
    },
    /**
         * filter the accounts based on currentFilter
         * @param {Collection} accounts List of accounts
         * @param {String} currentFilter Fileter like Favourite etc
         * @returns {Collection} filtered account based on current filter
         */
    filterAccounts: function(accounts, currentFilter) {
      if (currentFilter === kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")) {
        return accounts;
      }
      if (currentFilter === kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts")) {
        return ((accounts).filter(this.isFavourite));
      } else {
        var filteredAccounts = [];
        for (var i = 0; i < accounts.length; i++) {
          if (currentFilter === accounts[i].bankName) {
            filteredAccounts.push(accounts[i]);
          }
        }
        return filteredAccounts;
      }
    },
    /**
         * Method to set bank accounts for the segment
         * @param {Collection} accounts List of accounts
         * @param {String} currentFilter like Favourite etc
         * @param {Collection} favAccounts List of favourite accounts
         * @returns {JSON} bank segment data
         */
    setBanksAccountSegment: function(accounts, currentFilter, favAccounts) {
      var bankJS = {},
          numberOfFavAccounts = 0,
          self = this;
      var banks = [{
        "lblUsers": {
          "toolTip": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") + " " + kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts"),
          "key": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts"),
          "text": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") + " " + kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts"),
          "accessibilityconfig": {
            "a11yLabel": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") + " " + kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")
          }
        },
        "lblSeparator": {
          "text": "label"
        }
      }];
      banks.push({
        "lblUsers": {
          "toolTip": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") + " " + kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts"),
          "key": kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts"),
          "text": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") + " " + kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts"),
          "accessibilityconfig": {
            "a11yLabel": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") + " " + kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts")
          }
        },
        "lblSeparator": {
          "text": "label"
        }
      });
      if (!kony.sdk.isNullOrUndefined(accounts) && accounts.length > 0) {
        accounts.forEach(function(record) {
          var bankName = record.bankName || record.BankName;
          if (!bankJS[bankName]) {
            banks.push({
              "lblUsers": {
                "toolTip": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") + " " + bankName + " " + kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                "key": (bankName),
                "text": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") + " " + bankName + " " + kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                "accessibilityconfig": {
                  "a11yLabel": kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.show") + " " + bankName + " " + kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                }
              },
              "lblSeparator": {
                "text": "label"
              }
            });
            bankJS[bankName] = true;
          }
          if (self.isFavourite(record)) {
            numberOfFavAccounts += 1;
          }
        });
      }
      var filteredBanks = [];
      for (var k = 0; k < banks.length; k++) {
        if ((banks[k].lblUsers.key === kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts")) && !(banks[k].lblUsers.key === currentFilter)) {
          if (favAccounts.length !== 0) {
            filteredBanks.push(banks[k]);
          }
        } else if (!(banks[k].lblUsers.key === currentFilter) && numberOfFavAccounts !== accounts.length) {
          filteredBanks.push(banks[k]);
        }
      }
      var dataMap = {
        "flxAccountTypes": "flxAccountTypes",
        "lblSeparator": "lblSeparator",
        "lblUsers": "lblUsers"
      };
      this.view.FavouriteAccountTypes.segAccountTypes.widgetDataMap = dataMap;
      this.view.FavouriteAccountTypes.segAccountTypes.setData(filteredBanks);
      this.view.FavouriteAccountTypes.forceLayout();
      this.AdjustScreen();
      return banks;
    },
    /**
         * Method to filter segment data based on banks
         * @param {Collection} banks List of banks
         * @param {Json} selectedItem selected item like key etc
         * @param {Collection} tempFavAccounts List of temporary favourite accounts
         * @returns {Collection} List of banks
         */
    filterSegmentData: function(banks, selectedItem, tempFavAccounts) {
      var newBanks = [];
      for (var k = 0; k < banks.length; k++) {
        if (((banks[k].lblUsers.key).toLowerCase().trim() === (kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts")).toLowerCase().trim()) && ((selectedItem.lblUsers.key).toLowerCase().trim() !== (banks[k].lblUsers.key).toLowerCase().trim())) {
          if (tempFavAccounts.length !== 0) {
            newBanks.push(banks[k]);
          }
        } else if ((selectedItem.lblUsers.key).toLowerCase().trim() !== (banks[k].lblUsers.key).toLowerCase().trim()) {
          newBanks.push(banks[k]);
        }
      }
      return newBanks;
    },
    /**
         * Method that gets called on selection of row of accounts segment
         * @param {Collection} accounts List of accounts
         * @param {Collection} banks List of banks
         */
    onSegAccountTypeRowClick: function(accounts, banks) {
      var self = this;
      var selectedItem = this.view.FavouriteAccountTypes.segAccountTypes.selectedRowItems[0];
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.accountList.btnShowAllAccounts, selectedItem.lblUsers.key, accessibilityConfig);
      this.view.accountList.btnShowAllAccounts.toolTip = selectedItem.lblUsers.key;
      this.currentFilter = selectedItem.lblUsers.key;
      if (selectedItem.lblUsers.key === kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")) {
        var data = self.getDataWithSections(accounts);
        this.view.accountList.segAccounts.setData(data);
        this.AdjustScreen();
      } else if (selectedItem.lblUsers.key === kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts")) {
        var data = self.getDataWithSections(accounts.filter(this.isFavourite));
        this.view.accountList.segAccounts.setData(data);
        this.AdjustScreen();
      } else {
        var filteredAccounts = [],
            bankName;
        for (var i = 0; i < (accounts).length; i++) {
          bankName = (accounts)[i].bankName || (accounts)[i].BankName
          if (selectedItem.lblUsers.key === bankName) {
            filteredAccounts.push((accounts)[i]);
          }
        }
        var data = self.getDataWithSections(filteredAccounts);
        this.view.accountList.segAccounts.setData(data);
        this.AdjustScreen()
      }
      this.view.FavouriteAccountTypes.isVisible = false;
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      this.view.accountList.lblImgDropdown.text = "O";
      var tempFavAccounts = (accounts).filter(this.isFavourite);
      var dataMap = {
        "flxAccountTypes": "flxAccountTypes",
        "lblSeparator": "lblSeparator",
        "lblUsers": "lblUsers"
      };
      this.view.FavouriteAccountTypes.segAccountTypes.widgetDataMap = dataMap;
      var newBanks = this.filterSegmentData(banks, selectedItem, tempFavAccounts);
      this.view.FavouriteAccountTypes.segAccountTypes.setData(newBanks);
      this.view.accountList.forceLayout();
      this.AdjustScreen()
    },
    accountTypeConfig: (function() {
      var accountTypeConfig = {};
      var ApplicationManager = require('ApplicationManager');
      applicationManager = ApplicationManager.getApplicationManager();
      accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING)] = {
        sideImage: ViewConstants.SIDEBAR_TURQUOISE,
        sideSkin: ViewConstants.SKINS.SAVINGS_SIDE_BAR,
        balanceKey: 'availableBalance',
        balanceTitle: 'i18n.accounts.availableBalance'
      },
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING)] = {
        sideImage: ViewConstants.SIDEBAR_PURPLE,
        sideSkin: ViewConstants.SKINS.CHECKINGS_SIDE_BAR,
        balanceKey: 'availableBalance',
        balanceTitle: 'i18n.accounts.availableBalance'
      },
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD)] = {
        sideImage: ViewConstants.SIDEBAR_YELLOW,
        sideSkin: ViewConstants.SKINS.CREDIT_CARD_SIDE_BAR,
        balanceKey: 'currentBalance',
        balanceTitle: 'i18n.accounts.currentBalance'
      },
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT)] = {
        sideImage: ViewConstants.SIDEBAR_BLUE,
        sideSkin: ViewConstants.SKINS.DEPOSIT_SIDE_BAR,
        balanceKey: 'currentBalance',
        balanceTitle: 'i18n.accounts.currentBalance'
      },
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE)] = {
        sideImage: ViewConstants.SIDEBAR_BROWN,
        sideSkin: ViewConstants.SKINS.MORTGAGE_CARD_SIDE_BAR,
        balanceKey: 'outstandingBalance',
        balanceTitle: 'i18n.accounts.outstandingBalance'
      },
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN)] = {
        sideImage: ViewConstants.SIDEBAR_BROWN,
        sideSkin: ViewConstants.SKINS.LOAN_SIDE_BAR,
        balanceKey: 'outstandingBalance',
        balanceTitle: 'i18n.accounts.outstandingBalance'
      },
        accountTypeConfig['Default'] = {
        sideImage: ViewConstants.SIDEBAR_TURQUOISE,
        sideSkin: ViewConstants.SKINS.SAVINGS_SIDE_BAR,
        balanceKey: 'availableBalance',
        balanceTitle: 'i18n.accounts.availableBalance'
      },
        accountTypeConfig['null'] = {
        sideImage: ViewConstants.SIDEBAR_TURQUOISE,
        sideSkin: ViewConstants.SKINS.SAVINGS_SIDE_BAR,
        balanceKey: 'availableBalance',
        balanceTitle: 'i18n.accounts.availableBalance'
      }
      return accountTypeConfig;
    })(),
    /**
         * Method to get skins for account
         * @param {String} type Account type of account
         * @returns {String} Skin
         */
    getSkinForAccount: function(type) {
      switch (type) {
        case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING):
          return ViewConstants.SKINS.ACCOUNTS_SAVINGS_ROW;
        case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING):
          return ViewConstants.SKINS.ACCOUNTS_CHECKING_ROW;
        case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD):
          return ViewConstants.SKINS.ACCOUNTS_CREDITCARD_ROW;
        case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT):
          return ViewConstants.SKINS.ACCOUNTS_DEPOSIT_ROW;
        case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE):
          return ViewConstants.SKINS.ACCOUNTS_MORTGAGE_ROW;
        case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN):
          return ViewConstants.SKINS.ACCOUNTS_LOAN_ROW;
        default:
          return ViewConstants.SKINS.ACCOUNTS_UNCONFIGURED_ACCOUNT_ROW;
      }
    },
    /**
         * Method that gets called to show account details
         * @param {JSON} account Account whose details needs to be shown
         */
    onAccountSelection: function(account) {
      FormControllerUtility.showProgressBar(this.view);
      this.loadAccountModule().presentationController.showAccountDetails(account);
    },
    /**
         * Method to toggle account checkbox
         * @param {Number} index index of selectd row
         */
    toggleAccountSelectionCheckbox: function(index) {
      var data = this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.data[index];
      if (data.lblCheckBox === ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
        data.lblCheckBox = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
        var indexOfCurrentRow = this.selectedRowIndices.indexOf(index);
        if (indexOfCurrentRow > -1) {
          this.selectedRowIndices.splice(indexOfCurrentRow, 1);
        }
      } else {
        data.lblCheckBox = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
        this.selectedRowIndices.push(index);
      }
      this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.setDataAt(data, index);
      this.onClickOfSegmentRow();
    },
    /**
         *
         */
    showExternalAccountUpdateAlert: function() {},

    /**
         * Method to get quick actions for accounts
         * @param {Object} dataInput Data inputs like onCancel/accountType etc
         * @returns {Object} quick action for selected account
         */
    getQuickActions: function(dataInput) {
      var self = this;
      var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
      var onCancel = dataInput.onCancel;
      var quickActions = [{
        actionName: OLBConstants.ACTION.SCHEDULED_TRANSACTIONS,
        displayName: dataInput.scheduledDisplayName || kony.i18n.getLocalizedString("i18n.accounts.scheduledTransactions"),
        action: function(account) {
          if (dataInput.showScheduledTransactionsForm) {
            dataInput.showScheduledTransactionsForm(account);
          }
        }
      }, {
        actionName: OLBConstants.ACTION.MAKE_A_TRANSFER, //MAKE A TRANSFER
        displayName: (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") ? kony.i18n.getLocalizedString("i18n.hamburger.transfer") : (dataInput.makeATransferDisplayName || kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer")),
        action: function(account) {
          //Function call to  open tranfers page with parameter - account obj to be tranferred from.
          if (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") {
            applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen({
              accountFrom: dataInput.accountNumber
            });
          } else {
            applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
              accountObject: account,
              onCancelCreateTransfer: onCancel
            });
          }
        }
      },
                          {
                            actionName: OLBConstants.ACTION.MANAGE_CARDS, //Manage Card
                            displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.ManageCards"),
                            action: function(account) {
                              var data = {
                                "accounts": account
                              };
                              kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToCardsFromAccountDashboard(account);
                            }
                          }, {
                            actionName: OLBConstants.ACTION.TRANSFER_MONEY, //MAKE A TRANSFER
                            displayName: (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") ? kony.i18n.getLocalizedString("i18n.hamburger.transfer") : (dataInput.tranferMoneyDisplayName || kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer")),
                            action: function(account) {
                              if (applicationManager.getConfigurationManager().getDeploymentGeography() == "EUROPE") {
                                applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({
                                  context: "MakePaymentOwnAccounts",
                                  accountFrom: dataInput.accountNumber
                                });
                                return;
                              }
                              //Function call to  open tranfers page with parameter - account obj to be tranferred from.
                              if (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") {
                                applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen({
                                  accountFrom: dataInput.accountNumber
                                });
                              } else {
                                applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                                  accountObject: account,
                                  onCancelCreateTransfer: onCancel
                                });
                              }
                            }
                          }, {
                            actionName: OLBConstants.ACTION.PAY_MONEY, //Make Payment
                            displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.makePayFrom"),
                            action: function(account) {
                              if (applicationManager.getConfigurationManager().getDeploymentGeography() == "EUROPE") {
                                applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({
                                  context: "MakePayment",
                                  accountFrom: dataInput.accountNumber
                                });
                                return;
                              }

                            }
                          },
                          {
                            actionName: OLBConstants.ACTION.PAY_A_BILL,
                            displayName: (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true") ? kony.i18n.getLocalizedString("i18n.Pay.PayBill") : (dataInput.payABillDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payABillFrom")),
                            action: function(account) {
                              //Function call to open bill pay screen
                              var billPaymentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule");
                              billPaymentModule.presentationController.showBillPaymentScreen({
                                "sender": "Accounts",
                                "context": "PayABillWithContext",
                                "loadBills": true,
                                "data": {
                                  "fromAccountNumber": account.accountID,
                                  "show": 'PayABill',
                                  "onCancel": onCancel
                                }
                              });
                            }
                          }, {
                            actionName: OLBConstants.ACTION.PAY_A_PERSON_OR_SEND_MONEY,
                            displayName: dataInput.sendMoneyDisplayName || kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                            action: function(account) {
                              var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                              var dataItem = account;
                              dataItem.onCancel = onCancel;
                              p2pModule.presentationController.showPayAPerson("sendMoneyTab", dataItem);
                            }
                          }, {
                            actionName: OLBConstants.ACTION.PAY_DUE_AMOUNT,
                            displayName: dataInput.payDueAmountDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payDueAmount"),
                            action: function(account) {
                              var data = {
                                "accounts": account
                              };
                              var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                              loanModule.presentationController.navigateToLoanDue(data);
                            }
                          }, {
                            actionName: OLBConstants.ACTION.PAYOFF_LOAN,
                            displayName: dataInput.payoffLoanDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan"),
                            action: function(account) {
                              var data = {
                                "accounts": account
                              };
                              var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                              loanModule.presentationController.navigateToLoanPay(data);
                            }
                          }, {
                            actionName: OLBConstants.ACTION.STOPCHECKS_PAYMENT,
                            displayName: dataInput.stopCheckPaymentDisplayName || kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS"),
                            action: function(account) {
                              var stopPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
                              stopPaymentsModule.presentationController.showStopPayments({
                                onCancel: onCancel,
                                accountID: account.accountID,
                                "show": OLBConstants.ACTION.SHOW_STOPCHECKS_FORM
                              });
                            }
                          }, {
                            actionName: OLBConstants.ACTION.VIEW_STATEMENTS,
                            displayName: dataInput.viewStatementsDisplayName || kony.i18n.getLocalizedString("i18n.ViewStatements.STATEMENTS"),
                            action: function(account) {
                              var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                              accountsModule.presentationController.showFormatEstatements(account);
                            }
                          }, {
                            actionName: OLBConstants.ACTION.UPDATE_ACCOUNT_SETTINGS,
                            displayName: dataInput.updateAccountSettingsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateAccountSettings"),
                            action: function() {
                              var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                              profileModule.presentationController.enterProfileSettings("accountSettings");
                            }
                          }, {
                            actionName: OLBConstants.ACTION.REMOVE_ACCOUNT,
                            displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.removeAccount") + "/ s",
                            action: function(account) {
                              //                     var externalAccountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ExternalAccountsModule");
                              //                     externalAccountsModule.presentationController.deleteConnection(account.accountID);
                              self.showDeletePopUp(account);
                              self.view.accountListMenu.setVisibility(false);
                            }
                          }, {
                            actionName: OLBConstants.ACTION.REFRESH_ACCOUNT,
                            displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.refreshAccount"),
                            action: function(account) {
                              var externalAccountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ExternalAccountsModule");
                              externalAccountsModule.presentationController.refreshConsent(account.accountID);
                              self.view.accountListMenu.setVisibility(false);
                            }
                          }, {
                            actionName: OLBConstants.ACTION.ACCOUNT_PREFERENCES,
                            displayName: kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountPreferences"),
                            action: function() {
                              var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                              profileModule.presentationController.initializeUserProfileClass();
                              profileModule.presentationController.showPreferredAccounts();
                            }
                          }, {
                            actionName: OLBConstants.ACTION.EDIT_ACCOUNT,
                            displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.editAccount"),
                            action: function(account) {
                              var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                              profileModule.presentationController.initializeUserProfileClass();
                              profileModule.presentationController.showEditExternalAccount(account);
                            }
                          }, {
                            actionName: OLBConstants.ACTION.ACCOUNT_ALERTS,
                            displayName: kony.i18n.getLocalizedString("i18n.Alerts.AccountAlertSettings"),
                            action: function(account) {
                              var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                              profileModule.presentationController.initializeUserProfileClass();
                              profileModule.presentationController.fetchAlertsCategory("alertSettings2", account.accountID);
                            }
                          }
                         ];
      return quickActions;
    },
    /**
         * showDeletePopUp :  Method to display pop up for deleting external account.
         */
    showDeletePopUp: function(account) {
      try {
        var scopeObject = this;
        this.view.flxLogout.left = "0%";
        scopeObject.view.flxLogout.height = scopeObject.getPageHeight();
        scopeObject.view.flxLogout.setFocus(true);
        this.view.CustomPopup.setFocus(true);
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(this.view.CustomPopup.lblHeading, kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.removeAccount") + "/ s", accessibilityConfig);
        CommonUtilities.setText(this.view.CustomPopup.lblPopupMessage, kony.i18n.getLocalizedString("i18n.AccountsAggregation.AreYouSure"), accessibilityConfig);
        if (CommonUtilities.isCSRMode()) {
          this.view.CustomPopup.btnYes.onClick = CommonUtilities.disableButtonActionForCSRMode();
          this.view.CustomPopup.btnYes.skin = CommonUtilities.disableButtonSkinForCSRMode();
          this.view.CustomPopup.btnYes.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
          this.view.CustomPopup.btnYes.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
        } else {
          this.view.CustomPopup.btnYes.onClick = function() {
            scopeObject.view.flxLogout.left = "-100%";
            var externalAccountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ExternalAccountsModule");
            externalAccountsModule.presentationController.deleteConnection(account.accountID);
          };
        }
        this.view.CustomPopup.btnNo.onClick = function() {
          scopeObject.view.flxLogout.left = "-100%";
        };
        this.view.CustomPopup.flxCross.onClick = function() {
          scopeObject.view.flxLogout.left = "-100%";
        };
      } catch (error) {}
    },
    /**
         * enableLogoutAction :  Method to reinitialize logout action on popup yes button
         */
    enableLogoutAction: function() {
      try {
        var scopeObj = this;
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(this.view.CustomPopup.lblHeading, kony.i18n.getLocalizedString("i18n.common.LogoutMsg"), accessibilityConfig);
        CommonUtilities.setText(this.view.CustomPopup.btnNo, kony.i18n.getLocalizedString("i18n.common.no"), accessibilityConfig);
        CommonUtilities.setText(widgetID, text, accessibilityConfig);
        CommonUtilities.setText(this.view.CustomPopup.btnYes, kony.i18n.getLocalizedString("i18n.common.yes"), accessibilityConfig);
        this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString("i18n.common.no");
        this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString("i18n.common.yes");
        this.view.CustomPopup.btnYes.onClick = function() {
          var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
          context = {
            action: "Logout"
          };
          authModule.presentationController.doLogout(context);
          scopeObj.view.flxLogout.left = "-100%";
        };
      } catch (error) {}
    },
    /**
         * Method to get action for specific account
         * @param {Collection} Actions List of Actions
         * @param {String} actionName Name of action
         * @param {JSON} account Account for which action is required
         * @returns {Object} matched action for the account from liat of actions
         */
    getAction: function(Actions, actionName, account) {
      var actionItem, matchedAction;
      for (var i = 0; i < Actions.length; i++) {
        actionItem = Actions[i];
        if (actionItem.actionName === actionName) {
          matchedAction = {
            actionName: actionItem.actionName,
            displayName: actionItem.displayName,
            action: actionItem.action.bind(null, account)
          };
          break;
        }
      }
      if (!matchedAction) {
        CommonUtilities.ErrorHandler.onError("Action :" + actionName + " is not found, please validate with Contextial actions list.");
        return false;
      }
      return matchedAction;
    },
    /**
         * Method to get quick action view model
         * @param {JSON} account Account for which quick actions are required
         * @param {Collection} actions List of actions
         * @returns {Object} actions viewModel
         */
    getQuickActionsViewModel: function(account, actions) {
      var scopeObj = this;
      var finalActionsViewModel = [];
      if (account.accountType) {
        if (actions.length) {
          var validActions = actions.filter(function(action) {
            return scopeObj.loadAccountModule().presentationController.isValidAction(action, account);
          });
          var onCancel = function() {
            scopeObj.loadAccountModule().presentationController.presentAccountsLanding();
          };
          finalActionsViewModel = validActions.map(function(action) { //get action object.
            var quickActions = scopeObj.getQuickActions({
              onCancel: onCancel,
              tranferMoneyDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.makeTransferFrom"),
              payABillDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payABillFrom"),
              sendMoneyDisplayName: kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
              accountAlertsDisplayName: kony.i18n.getLocalizedString("i18n.Alerts.AccountAlertSettings"),
              accountNumber: account.Account_id || account.accountID
            });
            return scopeObj.getAction(quickActions, action, account);
          });
        }
      }
      return finalActionsViewModel
    },
    /**
         * Method to show actions for accounts on fetching quick action for that specific account
         * @param {JSON} account account whose quick action needs to be fetched
         * @param {Object} actions List of actions
         */
    onFetchQuickActions: function(account, actions) {
      var scopeObj = this;
      var quickActionsViewModel = scopeObj.getQuickActionsViewModel(account, actions)
      var toQuickActionSegmentModel = function(quickAction) {
        return {
          "lblUsers": {
            "text": quickAction.displayName,
            "toolTip": quickAction.displayName,
            "accessibilityconfig": {
              "a11yLabel": quickAction.displayName
            }
          },
          "lblSeparator": "lblSeparator",
          "onRowClick": quickAction.action
        };
      };
      this.view.accountListMenu.segAccountListActions.setData(quickActionsViewModel.map(toQuickActionSegmentModel));
      this.view.accountListMenu.imgToolTip.setFocus(true);
      this.view.accountList.forceLayout();
      if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
        scopeObj.view.accountListMenu.left = "";
        scopeObj.view.accountListMenu.right = "55dp";
      } else if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
        scopeObj.view.accountListMenu.left = "";
        scopeObj.view.accountListMenu.right = "75dp";
      } else {
        scopeObj.view.accountListMenu.left = "590dp";
        scopeObj.view.accountListMenu.right = "";
      }
      this.AdjustScreen();
    },
    /**
         * Method to open quick actions
         * @param {JSON} account account whose quick action needs to be seen
         */
    openQuickActions: function(account) {
      var scopeObj = this;
      var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
      //Quick actions Configuration.
      var quickActionsConfig;
      if (account.isExternalAccount) {
        quickActionsConfig = OLBConstants.CONFIG.EXTERNAL_ACCOUNT_QUICK_ACTIONS;
        if (quickActionsConfig) {
          scopeObj.onFetchQuickActions(account, quickActionsConfig);
        }
      } else {
        quickActionsConfig = OLBConstants.CONFIG.ACCOUNTS_QUICK_ACTIONS;
        if (quickActionsConfig[account.accountType]) {
          scopeObj.onFetchQuickActions(account, quickActionsConfig[account.accountType]);
        }
      }
    },
    /**
         * Method to create accounts segment view model
         * @param {Collection} accounts List of accounts
         * @returns {JSON} account viewModel
         */
    createAllAccountSegmentsModel: function(account) {
      var scopeObject = this;
      var dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
      var count = 0;
      if (account.expiresAt && account.expiresAt !== undefined) {
        var targetDate = CommonUtilities.getDateAndTime(account.expiresAt);
        var expireDate = (targetDate.split(","))[0];
        var today = kony.os.date(dateFormat);
        var todayDateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(today, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase())
        var targetDateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(expireDate, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase())
        var difference = targetDateObj - todayDateObj;
        count = Math.ceil(difference / (1000 * 60 * 60 * 24));
      }
      var updatedAccountID;
      var accountID = account.accountID;
      var externalaccountID = accountID.substring(accountID.length, accountID.indexOf('-'));
      if (account.externalIndicator && account.externalIndicator === "true") {
        updatedAccountID = externalaccountID;
      } else {
        updatedAccountID = account.accountID
      }
      var getConfigFor = function(accountType) {
        if (scopeObject.accountTypeConfig[accountType]) {
          return scopeObject.accountTypeConfig[accountType];
        } else {
          return scopeObject.accountTypeConfig.Default;
        }
      };
      var dataObject = {
        "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxAccountListItemMobile" : "flxAccountListItem",
        "lblAccountName": {
          "text": account.nickName || account.accountName,
          "accessibilityconfig": {
            "a11yLabel": account.nickName || account.accountName
          }
        },
        "lblAccountNumber": {
          "text": kony.i18n.getLocalizedString("i18n.common.accountNumber") + " " + CommonUtilities.accountNumberMask(updatedAccountID),
          "accessibilityconfig": {
            "a11yLabel": kony.i18n.getLocalizedString("i18n.common.accountNumber") + " " + CommonUtilities.accountNumberMask(updatedAccountID)
          }
        },
        "lblAvailableBalanceValue": {
          "text": CommonUtilities.formatCurrencyWithCommas(account[getConfigFor(account.accountType).balanceKey], false, account.currencyCode),
          "accessibilityconfig": {
            "a11yLabel": CommonUtilities.formatCurrencyWithCommas(account[getConfigFor(account.accountType).balanceKey], false, account.currencyCode)
          }
        },
        "lblAvailableBalanceTitle": {
          "text": kony.i18n.getLocalizedString(getConfigFor(account.accountType).balanceTitle) + String(account.isExternalAccount === true ? ": " + account.availableBalanceUpdatedAt : ""),
          "accessibilityconfig": {
            "a11yLabel": kony.i18n.getLocalizedString(getConfigFor(account.accountType).balanceTitle) + String(account.isExternalAccount === true ? ": " + account.availableBalanceUpdatedAt : "")
          }
        },
        "onAccountClick": account.isExternalAccount === true ? scopeObject.showExternalAccountUpdateAlert : scopeObject.onAccountSelection.bind(scopeObject, account),
        "onQuickActions": scopeObject.openQuickActions.bind(scopeObject, account),
        "flxMenu": {
          // "skin": self.getSkinForAccount(account.accountType)
        },
        "imgThreeDotIcon": {
          "text": ViewConstants.FONT_ICONS.THREE_DOTS_ACCOUNTS,
          "skin": ViewConstants.SKINS.THREE_DOTS_IMAGE,
          "isVisible": true,
          "accessibilityconfig": {
            "a11yLabel": "Contextual Menu"
          }
        },
        "btn": " ",
        "flxIdentifier": {
          "skin": getConfigFor(account.accountType).sideSkin,
          "isVisible": false
        },
        "userName": account.userName,
        "bankId": account.bankId,
        "accountName": account.accountName,
        "isError": account.isError,
        "lblTransactionHeader": {
          "text": account.accountType,
          "accessibilityconfig": {
            "a11yLabel": account.accountType,
          }
        }
      };
      if (CommonUtilities.isCSRMode()) {
        dataObject.imgStarIcon = scopeObject.isFavourite(account) ? {
          "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_ACTIVE,
          "skin": ViewConstants.SKINS.ACCOUNTS_CSR_FAVOURITE_SELECTED,
          "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.optionDisabled"),
          "isVisible": true,
          "accessibilityconfig": {
            "a11yLabel": "Enabled Favourite Account"
          }
        } : {
          "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_INACTIVE,
          "skin": ViewConstants.SKINS.ACCOUNTS_CSR_FAVOURITE_UNSELECTED,
          "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.optionDisabled"),
          "isVisible": true,
          "accessibilityconfig": {
            "a11yLabel": "Disabled Favourite Account"
          }
        };
        dataObject.toggleFavourite = CommonUtilities.disableButtonActionForCSRMode;
      } else {
        dataObject.imgStarIcon = scopeObject.isFavourite(account) ? {
          "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_ACTIVE,
          "skin": ViewConstants.SKINS.ACCOUNTS_FAVOURITE_SELECTED,
          "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.removefavourite"),
          "isVisible": true,
          "accessibilityconfig": {
            "a11yLabel": "Active Favourite Account"
          }
        } : {
          "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_INACTIVE,
          "skin": ViewConstants.SKINS.ACCOUNTS_FAVOURITE_UNSELECTED,
          "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.setAsFavourite"),
          "isVisible": true,
          "accessibilityconfig": {
            "a11yLabel": "Inactive Favourite Account"
          }
        };
        dataObject.toggleFavourite = scopeObject.loadAccountModule().presentationController.changeAccountFavouriteStatus.bind(scopeObject.loadAccountModule().presentationController, account);
      }
      var breakpoint = kony.application.getCurrentBreakpoint();
      var topValueOfFavIcon = "12dp";
      var bottomValueOfBalance = "15dp";
      if (breakpoint === 640) {
        topValueOfFavIcon = "17dp";
        bottomValueOfBalance = "21dp";
      }
      dataObject.flxFavourite = {
        "top": topValueOfFavIcon
      };
      if (account.externalIndicator && account.externalIndicator === "true") {
        dataObject.imgBankName = account.logoURL;
        dataObject.flxBankName = {
          "isVisible": true
        };
        dataObject.lblAvailableBalanceTitle = {
          "text": kony.i18n.getLocalizedString("i18n.accounts.AsOf") + " " + CommonUtilities.getDateAndTime(account.processingTime),
          "accessibilityconfig": {
            "a11yLabel": CommonUtilities.getDateAndTime(account.processingTime)
          }
        };
        if (count <= 0) {
          dataObject.imgInfo = {
            "src": "alert_1.png",
            "isVisible": true
          };
        } else if (count <= account.connectionAlertDays) {
          dataObject.imgInfo = {
            "src": "Info_2x.png",
            "isVisible": true
          };
        }
      } else {
        dataObject.lblAvailableBalanceTitle = {
          "bottom": bottomValueOfBalance,
          "text": kony.i18n.getLocalizedString(getConfigFor(account.accountType).balanceTitle),
          "accessibilityconfig": {
            "a11yLabel": kony.i18n.getLocalizedString(getConfigFor(account.accountType).balanceTitle) + String(account.isExternalAccount === true ? ": " + account.availableBalanceUpdatedAt : "")
          }
        };
        dataObject.flxBankName = {
          "isVisible": false
        };
      }

      return dataObject;
    },
    /**
         * Method to create favourite accounts segment view model
         * @param {Collection} accounts List of accounts
         * @returns {JSON} account viewModel
         */
    createAccountSegmentsModel: function(accounts) {
      var scopeObject = this;
      var getConfigFor = function(accountType) {
        if (scopeObject.accountTypeConfig[accountType]) {
          return scopeObject.accountTypeConfig[accountType];
        } else {
          return scopeObject.accountTypeConfig.Default;
        }
      };
      var accountTypes = [];
      accounts.map(function(account) {
        return account.type;
      }).forEach(function(type) {
        if (accountTypes.indexOf(type) < 0) {
          accountTypes.push(type);
        }
      });
      return accounts.map(function(account) {
        var dataObject = {
          "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxAccountListItemMobile" : "flxAccountListItem",
          "lblAccountName": account.nickName || account.accountName,
          "lblAccountNumber": kony.i18n.getLocalizedString("i18n.common.accountNumber") + " " + CommonUtilities.accountNumberMask(account.accountID),
          "lblAvailableBalanceValue": CommonUtilities.formatCurrencyWithCommas(account[getConfigFor(account.accountType).balanceKey], false, account.currencyCode),
          "lblAvailableBalanceTitle": kony.i18n.getLocalizedString(getConfigFor(account.accountType).balanceTitle) + String(account.isExternalAccount === true ? ": " + account.availableBalanceUpdatedAt : ""),
          "onAccountClick": account.isExternalAccount === true ? scopeObject.showExternalAccountUpdateAlert : scopeObject.onAccountSelection.bind(scopeObject, account),
          "onQuickActions": scopeObject.openQuickActions.bind(scopeObject, account),
          "flxMenu": {
            // "skin": self.getSkinForAccount(account.accountType)
          },
          "imgThreeDotIcon": {
            "text": ViewConstants.FONT_ICONS.THREE_DOTS_ACCOUNTS,
            "skin": ViewConstants.SKINS.THREE_DOTS_IMAGE,
            "isVisible": true,
            "accessibilityconfig": {
              "a11yLabel": "Contextual Menu"
            }
          },
          "btn": " ",
          "flxIdentifier": {
            "skin": getConfigFor(account.accountType).sideSkin,
            "isVisible": false
          },
          "imgBankName": account.bankLogo,
          "userName": account.userName,
          "bankId": account.bankId,
          "accountName": account.accountName,
          "isError": account.isError
        };
        if (CommonUtilities.isCSRMode()) {
          dataObject.imgStarIcon = scopeObject.isFavourite(account) ? {
            "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_ACTIVE,
            "skin": ViewConstants.SKINS.ACCOUNTS_CSR_FAVOURITE_SELECTED,
            "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.optionDisabled"),
            "isVisible": true,
            "accessibilityconfig": {
              "a11yLabel": "Enabled Favourite Account"
            }
          } : {
            "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_INACTIVE,
            "skin": ViewConstants.SKINS.ACCOUNTS_CSR_FAVOURITE_UNSELECTED,
            "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.optionDisabled"),
            "isVisible": true,
            "accessibilityconfig": {
              "a11yLabel": "Disabled Favourite Account"
            }
          };
          dataObject.toggleFavourite = CommonUtilities.disableButtonActionForCSRMode;
        } else {
          dataObject.imgStarIcon = scopeObject.isFavourite(account) ? {
            "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_ACTIVE,
            "skin": ViewConstants.SKINS.ACCOUNTS_FAVOURITE_SELECTED,
            "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.removefavourite"),
            "isVisible": true,
            "accessibilityconfig": {
              "a11yLabel": "Active Favourite Account"
            }
          } : {
            "text": ViewConstants.FONT_ICONS.FAVOURITE_STAR_INACTIVE,
            "skin": ViewConstants.SKINS.ACCOUNTS_FAVOURITE_UNSELECTED,
            "toolTip": kony.i18n.getLocalizedString("i18n.AccountsLanding.setAsFavourite"),
            "isVisible": true,
            "accessibilityconfig": {
              "a11yLabel": "Inactive Favourite Account"
            }
          };
          dataObject.toggleFavourite = scopeObject.loadAccountModule().presentationController.changeAccountFavouriteStatus.bind(scopeObject.loadAccountModule().presentationController, account);
        }
        return dataObject;
      }).reduce(function(p, e) {
        return p.concat(e);
      }, []);
    },
    /**
         * Method to create section header for accounts
         * @param {Collection} accounts List of accounts
         */
    getDataWithSections: function(accounts) {
      var scopeObj = this;
      var finalData = {};
      var prioritizeAccountTypes = applicationManager.getTypeManager().getAccountTypesByPriority();
      accounts.forEach(function(account) {
        var accountType = applicationManager.getTypeManager().getAccountType(account.accountType);
        if (finalData.hasOwnProperty(accountType)) {
          finalData[accountType][1].push(scopeObj.createAllAccountSegmentsModel(account));
        } else {
          finalData[accountType] = [{
            lblTransactionHeader: {
              "text": applicationManager.getTypeManager().getAccountTypeDisplayValue(accountType) != undefined ?
              applicationManager.getTypeManager().getAccountTypeDisplayValue(accountType) : accountType + " " + kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
              "accessibilityconfig": {
                "a11yLabel": applicationManager.getTypeManager().getAccountTypeDisplayValue(accountType) != undefined ?
                applicationManager.getTypeManager().getAccountTypeDisplayValue(accountType) : accountType + " " + kony.i18n.getLocalizedString("i18n.topmenu.accounts")
              }
            },
            template: "flxAccountListItemHeader"
          },
                                    [scopeObj.createAllAccountSegmentsModel(account)]
                                   ];
        }
      });
      var data = [];
      for (var key in prioritizeAccountTypes) {
        var accountType = prioritizeAccountTypes[key];
        if (finalData.hasOwnProperty(accountType)) {
          data.push(finalData[accountType]);
        }
      }
      return data;
    },
    /**
         * Method to update accounts list
         * @param {Collection} accounts List of accounts
         */
    updateAccountList: function(accounts) {

      var scopeObj = this;
      accountsLength = accounts.length;
      this.view.flxLoading.height = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height + this.view.flxFooter.info.frame.height + "dp";
      this.view.customheader.customhamburger.activateMenu("ACCOUNTS", "My Accounts");
      this.view.flxActions.isVisible = false;
      this.view.accountList.lblAccountsHeader.skin = ViewConstants.SKINS.LABEL_HEADER_BOLD;
      this.view.flxWelcomeAndActions.setVisibility(false);
      this.view.accountList.setVisibility(true);
      if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(this.view.customheader.lblHeaderMobile, kony.i18n.getLocalizedString('i18n.hamburger.myaccounts'), accessibilityConfig);
      }
      this.view.AddExternalAccounts.setVisibility(false);
      var favAccounts = accounts.filter(this.isFavourite);
      if (!scopeObj.currentFilter) {
        if (favAccounts.length === 0) {
          scopeObj.currentFilter = kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts");
        } else {
          scopeObj.currentFilter = kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts");
        }
      } else {
        var filteredAccounts = scopeObj.filterAccounts(accounts, scopeObj.currentFilter);
        if (filteredAccounts.length === 0) {
          scopeObj.currentFilter = kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts");
        }
      }
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.accountList.btnShowAllAccounts, scopeObj.currentFilter, accessibilityConfig);
      this.view.accountList.btnShowAllAccounts.toolTip = scopeObj.currentFilter;
      var banks = this.setBanksAccountSegment(accounts, scopeObj.currentFilter, favAccounts);
      this.view.accountList.segAccounts.widgetDataMap = {
        "btn": "btn",
        "flxAccountListItem": "flxAccountListItem",
        "flxAccountListItemWrapper": "flxAccountListItemWrapper",
        "flxAccountName": "flxAccountName",
        "flxAccountNameWrapper": "flxAccountNameWrapper",
        "flxAvailableBalance": "flxAvailableBalance",
        "flxAvailableBalanceWrapper": "flxAvailableBalanceWrapper",
        "flxBankName": "flxBankName",
        "flxContent": "flxContent",
        "flxFavourite": "flxFavourite",
        "flxIdentifier": "flxIdentifier",
        "flxMenu": "flxMenu",
        "imgBankName": "imgBankName",
        "imgFavourite": "imgFavourite",
        "imgIdentifier": "imgIdentifier",
        "imgInfo": "imgInfo",
        "imgMenu": "imgMenu",
        "imgStarIcon": "imgStarIcon",
        "imgThreeDotIcon": "imgThreeDotIcon",
        "imgWarning": "imgWarning",
        "lblAccountName": "lblAccountName",
        "lblAccountNumber": "lblAccountNumber",
        "lblAvailableBalanceTitle": "lblAvailableBalanceTitle",
        "lblAvailableBalanceValue": "lblAvailableBalanceValue",
        "lblTransactionHeader": "lblTransactionHeader"
      };
      this.view.accountList.segAccounts.sectionHeaderSkin = "segAccountListItemHeader";
      this.view.FavouriteAccountTypes.segAccountTypes.onRowClick = this.onSegAccountTypeRowClick.bind(this, accounts, banks);
      if (scopeObj.currentFilter === kony.i18n.getLocalizedString("i18n.AccountsAggregation.DashboardFilter.allAccounts")) {
        var data = scopeObj.getDataWithSections(accounts);
        this.view.accountList.segAccounts.setData(data);
        this.AdjustScreen();
      } else if (scopeObj.currentFilter === kony.i18n.getLocalizedString("i18n.Accounts.FavouriteAccounts")) {
        var data = scopeObj.getDataWithSections(accounts.filter(this.isFavourite));
        this.view.accountList.segAccounts.setData(data);
        this.view.accountList.forceLayout();
        this.AdjustScreen();
      } else {
        var filtereAccounts = [];
        for (var i = 0; i < (accounts).length; i++) {
          if (this.currentFilter === (accounts)[i].bankName) {
            filtereAccounts.push((accounts)[i]);
          }
        }
        var data = scopeObj.getDataWithSections(filtereAccounts);
        this.view.accountList.segAccounts.setData(data);
        this.view.accountList.forceLayout();
        this.AdjustScreen();
      }
      this.view.accountList.forceLayout();
      FormControllerUtility.hideProgressBar(this.view);
      this.AdjustScreen();
    },
    /**
         * Method to update alert icon for unread messages
         * @param {Number} unreadCount Unread count of messages
         */
    updateAlertIcon: function(unreadCount) {
      applicationManager.getConfigurationManager().setUnreadMessageCount(unreadCount);
      this.view.customheader.headermenu.updateAlertIcon();
    },
    /**
         * Show Overdraft Notifaction Message
         * @param {String} isOverdraft Param to check if a transaction is over drafted
         * @param {String} overDraftMessage overDraft message (optional)
         */
    setOverdraftNotification: function(isOverdraft, overDraftMessage) {
      var scopeObj = this;
      var overdraftUI = scopeObj.view.flxOverdraftWarning;
      if (isOverdraft && !overdraftUI.isVisible) {
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(scopeObj.view.lblOverdraftWarning, overDraftMessage || kony.i18n.getLocalizedString("i18n.AccountsLanding.OverDraftWarning"), accessibilityConfig);
        scopeObj.view.lblImgCloseWarning.onTouchEnd = function() {
          scopeObj.setOverdraftNotification(false);
        };
        overdraftUI.setVisibility(true);
        scopeObj.view.lblOverdraftWarning.setFocus(true);
        scopeObj.AdjustScreen();
      } else if (!isOverdraft && overdraftUI.isVisible) {
        function timerFunc() {
          overdraftUI.setVisibility(false);
          var acctop = scopeObj.view.accountListMenu.info.frame.y + 5;
          scopeObj.view.accountListMenu.top = acctop + ViewConstants.POSITIONAL_VALUES.DP;
          scopeObj.AdjustScreen();
          kony.timer.cancel("mytimerOverdraft");
        }
        kony.timer.schedule("mytimerOverdraft", timerFunc, 0.1, false);
      }
    },
    /**
         * Method to update upcomming transactions
         * @param {Collection} transactions List of transactions
         */
    showUpcomingTransactionsWidget: function(transactions) {
      var self = this;
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.upcomingTransactions.lblHeader, kony.i18n.getLocalizedString("i18n.Accounts.upComingTransactions"), accessibilityConfig);
      this.view.upcomingTransactions.lblHeader.skin = ViewConstants.SKINS.LABEL_HEADER_BOLD;
      var formattedTransactions = [];
      this.setOverdraftNotification(false);
      formattedTransactions = transactions.map(function(transaction) {
        if (String(transaction.isOverdraft).toLowerCase() === "true") {
          self.setOverdraftNotification(true);
        }
        var leftValue = "30dp";
        if (kony.application.getCurrentBreakpoint() === 640) {
          leftValue = "15dp";
        }
        var date = {
          text: CommonUtilities.getFrontendDateString(transaction.scheduledDate),
          left: leftValue,
          "accessibilityconfig": {
            "a11yLabel": CommonUtilities.getFrontendDateString(transaction.scheduledDate),
          }
        };
        var to = {
          text: kony.i18n.getLocalizedString("i18n.transfers.lblTo"),
          left: leftValue,
          "accessibilityconfig": {
            "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.lblTo")
          }
        };
        var toValue = {
          text: transaction.payPersonName || transaction.payeeNickName || transaction.payeeName || transaction.toAccountName,
          left: parseInt(leftValue) + 24 + "dp",
          "accessibilityconfig": {
            "a11yLabel": transaction.payPersonName || transaction.payeeNickName || transaction.payeeName || transaction.toAccountName,
          }
        };
        var toNavForm = {
          text: (applicationManager.getTypeManager().getTransactionTypeDisplayValue(transaction.transactionType) != null ?
                 applicationManager.getTypeManager().getTransactionTypeDisplayValue(transaction.transactionType) :
                 transaction.transactionType),
          left: leftValue,
          "accessibilityconfig": {
            "a11yLabel": (applicationManager.getTypeManager().getTransactionTypeDisplayValue(transaction.transactionType) != null ?
                          applicationManager.getTypeManager().getTransactionTypeDisplayValue(transaction.transactionType) :
                          transaction.transactionType)
          }
        };
        return {
          "lblDate": date,
          "lblTo": to,
          "lblToValue": toValue,
          "lblFormToNavigate": toNavForm,
          "lblAmount": {
            "text": CommonUtilities.formatCurrencyWithCommas(transaction.amount, false, transaction.currencyCode),
            "accessibilityconfig": {
              "a11yLabel": CommonUtilities.formatCurrencyWithCommas(transaction.amount, false, transaction.currencyCode)
            }
          },
          "lblSeparator": "lblSeparator"
        }
      });
      if (formattedTransactions.length === 0) {
        this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(false);
        this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(true);
        this.view.upcomingTransactions.flxNoTransactions.setVisibility(true);
        this.view.upcomingTransactions.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString("i18n.AccountsLanding.NoTransactionsScheduled");
      } else {
        this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(true);
        this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(false);
        this.view.upcomingTransactions.segMessages.setData(formattedTransactions);
      }
      this.view.upcomingTransactions.forceLayout();
      this.AdjustScreen()
    },
    /**
         * Method to show messages
         * @param {Array} messages, messages view modeld
         */
    showMessagesWidget: function(messages) {
      var self = this;
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.myMessages.lblHeader, kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages"), accessibilityConfig);
      this.view.myMessages.lblHeader.skin = ViewConstants.SKINS.LABEL_HEADER_BOLD;
      var formattedMessages = [];
      formattedMessages = messages.map(function(message) {
        var leftValue = "30dp";
        if (kony.application.getCurrentBreakpoint() === 640) {
          leftValue = "15dp";
        }
        var date = {
          text: CommonUtilities.getDateAndTime(message.recentMsgDate),
          left: leftValue,
          "accessibilityconfig": {
            "a11yLabel": CommonUtilities.getDateAndTime(message.recentMsgDate),
          }
        };
        var description = {
          text: message.requestsubject,
          left: leftValue
        };
        return {
          "lblDate": date,
          "rtxDescription": description,
          "lblSeparator": "lblSeparator",
          "onClick": self.loadAccountModule().presentationController.showSelectedMessage.bind(self.loadAccountModule().presentationController, message)
        };
      });
      if (formattedMessages.length === 0) {
        this.view.myMessages.flxMyMessagesWrapper.setVisibility(false);
        this.view.myMessages.flxMyMeesagesWarningWrapper.setVisibility(true);
        this.view.myMessages.flxNoMessages.setVisibility(true);
        this.view.myMessages.rtxNoMessage.text = kony.i18n.getLocalizedString("i18n.AccountsLanding.NoMessagesAtTheMoment");
      } else {
        this.view.myMessages.flxMyMessagesWrapper.setVisibility(true);
        this.view.myMessages.flxMyMeesagesWarningWrapper.setVisibility(false);
        this.view.myMessages.segMessages.setData(formattedMessages);
      }
      this.view.myMessages.forceLayout();
      this.AdjustScreen()
    },
    /**
         * Method to Hide the PFM Widget on DashBoard Based on the configuration flag. "isPFMWidgetEnabled"
         */
    disablePFMWidget: function() {
      this.view.mySpending.setVisibility(false);
      this.view.upcomingTransactions.left = "6" + ViewConstants.POSITIONAL_VALUES.PERCENTAGE;
      this.view.upcomingTransactions.width = "43.44" + ViewConstants.POSITIONAL_VALUES.PERCENTAGE;
      this.view.myMessages.width = "43.44" + ViewConstants.POSITIONAL_VALUES.PERCENTAGE;
    },
    /**
         * Format data for PFM donut chart
         * @param {Object} monthlyData Monthly data
         * @returns {Object} Formatted Donut chart data
         */
    formatPFMDonutChartData: function(monthlyData) {
      var monthlyDonutChartData = {};

      function addRequireDonutChartFields(month) {
        month.label = month.categoryName;
        month.Value = Number(month.cashSpent);
        month.colorCode = ViewConstants.PFM_CATEGORIES_COLORS[month.categoryName];
        return month;
      }
      var pfmData = monthlyData.map(addRequireDonutChartFields);
      if (monthlyData.length !== 0) {
        monthlyDonutChartData.totalCashSpent = CommonUtilities.formatCurrencyWithCommas(monthlyData[0].totalCashSpent);
      }
      monthlyDonutChartData.pfmChartData = pfmData;
      return monthlyDonutChartData;
    },
    /**
         * Method to get PFM mo0nthly data*
         * @param {Object} PFMData PFM data from backend
         */
    getPFMMonthlyDonutChart: function(PFMData) {


    },
    /**
         * Sets data regarding added external accounts in the segment
         * @param {Collection} accounts List of accounts
         */
    setAddedAccountsData: function(accounts) {
      var self = this;
      var widgetDataMap = {
        "lblCheckBox": "lblCheckBox",
        'lblAccountName': "AccountName",
        'lblAccountNumber': "AccountType",
        'lblAvailableBalanceValue': "AvailableBalanceWithCurrency",
        'lblAvailableBalanceTitle': "AvailableBalanceLabel",
        'lblSeperator': "separator",
        'flxcheckbox': "flxcheckbox"
      };
      this.view.AddExternalAccounts.Acknowledgment.segSelectedAccounts.widgetDataMap = widgetDataMap;
      var data = accounts.map(function(account) {
        return {
          "AccountName": account.AccountName,
          "AccountType": account.AccountType,
          "AvailableBalanceWithCurrency": account.AvailableBalanceWithCurrency,
          "AvailableBalanceLabel": account.AccountType ? kony.i18n.getLocalizedString(self.accountTypeConfig[account.AccountType.text].balanceTitle) : kony.i18n.getLocalizedString(self.accountTypeConfig["Default"].balanceTitle),
          "separator": account.separator,
          "AvailableBalance": account.AvailableBalance,
          "CurrencyCode": account.CurrencyCode,
          "AccountNumber": account.Number,
          "BankId": account.bank_id,
          "TypeId": account.Type_id,
          "AccountHolder": account.AccountHolder,
          "UserName": account.userName
        };
      });
      this.view.AddExternalAccounts.Acknowledgment.segSelectedAccounts.setData(data);
    },
    /**
         * Method to navigate to the acknolodgement
         */
    NavigateToAcknowledgment: function() {
      this.HideAll();
      this.NavigateToAddExternalAccounts();
      this.HideAllExternalAcc();
      this.view.AddExternalAccounts.flxAcknowledgment.isVisible = true;
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.AddExternalAccounts.Acknowledgment.btnAddMoreAccounts, kony.i18n.getLocalizedString("i18n.AccountsAggregation.AddMoreAccounts"), accessibilityConfig);
      CommonUtilities.setText(this.view.AddExternalAccounts.Acknowledgment.btnAccountSummary, kony.i18n.getLocalizedString("i18n.AccountsAggregation.AccountSummary"), accessibilityConfig);
      this.view.AddExternalAccounts.Acknowledgment.btnAccountSummary.toolTip = kony.i18n.getLocalizedString("i18n.AccountsAggregation.AccountSummary");
      this.view.AddExternalAccounts.Acknowledgment.btnAddMoreAccounts.toolTip = kony.i18n.getLocalizedString("i18n.AccountsAggregation.AddMoreAccounts");
    },
    /**
         * Navigates to show confirmation for added external accounts
         * @param {Array} addedExternalAccounts External account list
         */
    presentExternalAccountsAddedConfirmation: function(addedExternalAccounts) {
      var selectedAccounts = [];
      for (var i = 0; i < this.selectedRowIndices.length; i++) {
        selectedAccounts.push(this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.data[this.selectedRowIndices[i]]);
      }
      this.setAddedAccountsData(selectedAccounts);
      this.NavigateToAcknowledgment();
      this.AdjustScreen();
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.AddExternalAccounts.Acknowledgment.lblSuccessmsg, kony.i18n.getLocalizedString("i18n.AcountsAggregation.ExternalAccountAddition.ComfirmationMessage") + " " + this.ExternalLoginContextData.bankName, accessibilityConfig);
      this.view.AddExternalAccounts.Acknowledgment.btnAccountSummary.onClick = function() {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.showAccountsDashboard();
      }
      this.view.AddExternalAccounts.Acknowledgment.btnAddMoreAccounts.onClick = function() {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.showExternalBankList();
      }
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
         * Method to enable Or Disable External Login
         * @param {String} username username
         * @param {String} password password
         * @returns {Boolean} true/false
         */
    enableOrDisableExternalLogin: function(username, password) {
      if (username && String(username).trim() !== "" && password && String(password).trim() !== "") {
        this.enableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
        return true;
      } else {
        this.disableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
        return false;
      }
    },
    /**
         * Method that logins to external bank
         */
    onClickOfExternalBankLogin: function() {
      if (!(this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text))) {
        return;
      }
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankError.isVisible = false;
      FormControllerUtility.showProgressBar(this.view);
      this.ExternalLoginContextData.username = this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text;
      this.ExternalLoginContextData.password = this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text;
      var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
      accountModule.presentationController.authenticateUserInExternalBank(this.ExternalLoginContextData.username, this.ExternalLoginContextData.password, this.ExternalLoginContextData.identityProvider);
    },
    /**
         * Method to navigate to login screen
         */
    NavigateToLogin: function() {
      this.HideAll();
      this.NavigateToAddExternalAccounts();
      this.HideAllExternalAcc();
      this.view.AddExternalAccounts.flxLoginUsingSelectedBank.isVisible = true;
      this.view.AddExternalAccounts.setVisibility(true);
      this.view.AddExternalAccounts.flxSelectBankOrVendor.setVisibility(false);
      this.view.AddExternalAccounts.flxLoginUsingSelectedBank.setVisibility(true);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.setVisibility(true);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankError.isVisible = false;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBank.setVisibility(true);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankMain.setVisibility(true);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankHeading.text = kony.i18n.getLocalizedString("i18n.AccountsAggregation.LoginUsingSelectedBank");
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin, kony.i18n.getLocalizedString("i18n.common.login"), accessibilityConfig);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.toolTip = this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.text;
      this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
    },
    /**
         * Method to show External Bank Login
         * @param {Object} data External bank data
         */
    showExternalBankLogin: function(data) {
      this["ExternalLoginContextData"] = data;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text = "";
      this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text = "";
      this.view.flxActions.isVisible = true;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.secureTextEntry = true;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.imgflxLoginUsingSelectedBank.src = data.logo;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankMessage.text = "Welcome to " + data.bankName;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.toolTip = kony.i18n.getLocalizedString("i18n.common.login");
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
      //             this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.left = "37.07%";
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.isVisible = true;
      this.NavigateToLogin();
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.onClick = this.onClickOfExternalBankLogin.bind(this);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.onClick = function() {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.showAccountsDashboard();
      };
      this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.onDone = this.onClickOfExternalBankLogin.bind(this);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.onDone = this.onClickOfExternalBankLogin.bind(this);
      if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
        this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankMessage.skin = "sknSSPLight42424220Pxs";
      }
      this.AdjustScreen();
    },
    /**
         * Method to disable button
         * @param {String} button
         */
    disableButton: function(button) {
      button.setEnabled(false);
      button.skin = ViewConstants.SKINS.LOCATE_BTNSHARESEND;
      button.hoverSkin = ViewConstants.SKINS.LOCATE_BTNSHARESEND;
      button.focusSkin = ViewConstants.SKINS.LOCATE_BTNSHARESEND;
    },
    /**
         * Method to enable button
         * @param {String} button
         */
    enableButton: function(button) {
      button.setEnabled(true);
      button.skin = ViewConstants.SKINS.PFM_BTN_ENABLE;
      button.hoverSkin = ViewConstants.SKINS.PFM_BTN_ENABLE_HOVER;
      button.focusSkin = ViewConstants.SKINS.PFM_BTN_ENABLE_FOCUS;
    },
    /**
         * Method to Navigate Account Preferences
         *
         */
    navigateToAccountPreferences: function() {
      var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
      profileModule.presentationController.showPreferredAccounts();
    },
    /**
         * Method on click of externam bank account
         */
    onSelectionOfExternalBank: function() {
      this.view.AddExternalAccounts.SelectBankOrVendor.tbxName.text = this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.selectedRowItems[0].BankName;
      this.view.AddExternalAccounts.SelectBankOrVendor.flxBankList.isVisible = false;
      this.enableButton(this.view.AddExternalAccounts.SelectBankOrVendor.btnProceed);
      this.AdjustScreen();
    },
    /**
         * Method that gets triggered on click of Proceed
         */
    onClickOfProceedOnExternalBankList: function() {
      var selectedItem = this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.selectedRowItems[0];
      this.showExternalBankLogin({
        "identityProvider": selectedItem.IdentityProvider,
        "logo": selectedItem.logo,
        "isOauth2": selectedItem.Oauth2,
        "bankName": selectedItem.BankName,
        "bankId": selectedItem.id
      });
    },
    /**
         * Method on text change of external Bank Search
         */
    onTextChangeOfExternalBankSearch: function() {
      var searchText = this.view.AddExternalAccounts.SelectBankOrVendor.tbxName.text;
      if (searchText && String(searchText).trim() !== "") {
        searchText = String(searchText).trim().toLowerCase();
        var tempArr = [];
        for (var i in this.externalBankSearchList) {
          if (String(this.externalBankSearchList[i].BankName).trim().toLowerCase().search(searchText) >= 0) {
            tempArr.push(this.externalBankSearchList[i]);
          }
        }
        if (tempArr.length > 0) {
          this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.setData(tempArr);
          this.view.AddExternalAccounts.SelectBankOrVendor.flxBankList.isVisible = true;
        } else {
          this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.setData([]);
          this.view.AddExternalAccounts.SelectBankOrVendor.flxBankList.isVisible = false;
        }
      } else {
        this.view.AddExternalAccounts.SelectBankOrVendor.segBanksList.setData([]);
        this.view.AddExternalAccounts.SelectBankOrVendor.flxBankList.isVisible = false;
      }
      this.disableButton(this.view.AddExternalAccounts.SelectBankOrVendor.btnProceed);
      this.AdjustScreen();
    },
    /**
         * Method on click Of Reset ExternalBankList
         *
         */
    onClickOfResetOnExternalBankList: function() {
      this.view.AddExternalAccounts.SelectBankOrVendor.tbxName.text = "";
      this.onTextChangeOfExternalBankSearch();
    },
    /**
         * Method on success Save ExternalBankCredentails
         * @param {JSON} response response
         */
    onSuccessSaveExternalBankCredentailsSuccess: function(response) {
      var self = this;
      var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
      accountsModule.presentationController.fetchExternalBankAccounts({
        mainUser: applicationManager.getUserPreferencesManager().getCurrentUserName(),
        userName: self.ExternalLoginContextData.username,
        bankId: self.ExternalLoginContextData.bankId
      });
    },
    onSuccessSaveExternalBankCredentailsFailure: function(response) {
      var self = this;
      CommonUtilities.hideProgressBar(this.view);
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.lblLoginUsingSelectedBankError, kony.i18n.getLocalizedString("i18n.login.failedToLogin"), accessibilityConfig);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankError.isVisible = true;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text = "";
      this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
      this.AdjustScreen();
    },
    /**
         * Adds selected external accounts
         */
    addExternalAccounts: function() {
      FormControllerUtility.showProgressBar(this.view);
      var selectedAccounts = [];
      for (var i = 0; i < this.selectedRowIndices.length; i++) {
        selectedAccounts.push(this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.data[this.selectedRowIndices[i]]);
      }
      kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule').presentationController.addExternalBankAccounts(selectedAccounts);
    },
    /**
         * Toggles between terms accepted or not accepted
         */
    toggleBetweenTermsAcceptedAndUnaccepted: function() {
      var isAccepted = this.isTermsAndConditionsAccepted;
      if (!isAccepted) {
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.lblCheckBox, ViewConstants.FONT_ICONS.CHECBOX_SELECTED, accessibilityConfig);
        this.isTermsAndConditionsAccepted = true;
      } else {
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.lblCheckBox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED, accessibilityConfig);
        this.isTermsAndConditionsAccepted = false;
      }
      this.view.forceLayout();
    },
    /**
         * Method to enable Save button
         */
    enableSaveButton: function() {
      var termsAccepted = this.isTermsAndConditionsAccepted;
      var isAtleastOneAccountSelected = this.isAtLeastOneAccountSelected;
      if (termsAccepted === true && isAtleastOneAccountSelected === true && !CommonUtilities.isCSRMode()) {
        this.enableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
      } else {
        this.disableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
      }
    },
    /**
         * Method gets triggered on click of external account segment row
         */
    onClickOfSegmentRow: function() {
      if (this.selectedRowIndices.length > 0) {
        this.isAtLeastOneAccountSelected = true;
      } else {
        this.isAtLeastOneAccountSelected = false;
      }
      this.enableSaveButton();
    },
    /**
         * Method on Click Of FlxTermsAndconditions
         */
    onClickOfFlxTermsAndconditions: function() {
      this.toggleBetweenTermsAcceptedAndUnaccepted();
      this.enableSaveButton();
    },
    /**
         * Method to get data of external accounts
         * @param {Object} data data
         * @returns {Object} externalAccountsData
         */
    getExternalAccountsData: function(data) {
      var externalAccountsData = data;
      for (var i in externalAccountsData) {
        externalAccountsData[i].separator = ViewConstants.FONT_ICONS.LABEL_IDENTIFIER;
        externalAccountsData[i].isChecked = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
      }
      return externalAccountsData;
    },
    /**
         * Sets data to segment regarding external bank accounts
         * @param {Collection} accounts List of accounts
         */
    setExternalAccountsData: function(accounts) {
      var self = this;
      var widgetDataMap = {
        "lblCheckBox": "lblCheckBox",
        'lblAccountName': "MaskedAccountName",
        'lblAccountNumber': "AccountType",
        'lblAvailableBalanceValue': "AvailableBalanceWithCurrency",
        'lblAvailableBalanceTitle': "AvailableBalanceLabel",
        'lblSeperator': "separator",
        'flxcheckbox': "flxcheckbox"
      };
      this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.widgetDataMap = widgetDataMap;
      var data = accounts.map(function(account, index) {
        return {
          "MaskedAccountName": {
            "text": formatAccountName(account.AccountName, account.Number),
            "accessibilityconfig": {
              "a11yLabel": formatAccountName(account.AccountName, account.Number),
            }
          },
          "AccountName": {
            "text": account.AccountName,
            "accessibilityconfig": {
              "a11yLabel": account.AccountName
            }
          },
          "AccountType": {
            "text": account.AccountType,
            "accessibilityconfig": {
              "a11yLabel": account.AccountType,
            }
          },
          "AvailableBalanceWithCurrency": {
            "text": account.AvailableBalanceWithCurrency,
            "accessibilityconfig": {
              "a11yLabel": account.AvailableBalanceWithCurrency,
            }
          },
          "AvailableBalanceLabel": {
            "text": account.AccountType ? kony.i18n.getLocalizedString(self.accountTypeConfig[account.AccountType].balanceTitle) : kony.i18n.getLocalizedString(self.accountTypeConfig["Default"].balanceTitle),
            "accessibilityconfig": {
              "a11yLabel": account.AccountType ? kony.i18n.getLocalizedString(self.accountTypeConfig[account.AccountType].balanceTitle) : kony.i18n.getLocalizedString(self.accountTypeConfig["Default"].balanceTitle),
            }
          },
          "separator": account.separator,
          "lblCheckBox": ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
          "flxcheckbox": {
            "onClick": self.toggleAccountSelectionCheckbox.bind(this, index)
          },
          "AvailableBalance": account.AvailableBalance,
          "CurrencyCode": account.CurrencyCode,
          "AccountNumber": account.Number,
          "BankId": account.bank_id,
          "TypeId": account.Type_id,
          "AccountHolder": account.AccountHolder,
          "UserName": account.UserName
        };
      });
      this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.setData(data);
      this.view.forceLayout();
      var selectedIndices = [];
      for (var i = 0; i < data.length; i++) {
        selectedIndices.push(i);
      }
      this.selectedRowIndices = selectedIndices;

      function formatAccountName(accountName, accountNumber) {
        var stringAccNum = String(accountNumber);
        var stringAccName = String(accountName);
        var isLast4Digits = function(index) {
          return index > (stringAccNum.length - 5);
        };
        var hashedAccountNumber = stringAccNum.split('').map(function(c, i) {
          return isLast4Digits(i) ? c : 'X';
        }).join('').slice(-5);
        return stringAccName + "-" + hashedAccountNumber;
      }
    },
    /**
         * Method to hide all flex
         */
    HideAll: function() {
      this.view.flxDowntimeWarning.isVisible = false;
      this.view.flxWelcomeAndActions.isVisible = false;
      this.view.flxAccountListAndBanner.isVisible = false;
      this.view.flxSecondaryDetails.isVisible = false;
      this.view.accountListMenu.isVisible = false;
      this.view.AddExternalAccounts.flxSelectBankOrVendor.setVisibility(false);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginSelectAccountsMain.setVisibility(false);
    },
    /**
         * Method to add external account
         */
    NavigateToAddExternalAccounts: function() {
      this.view.flxAccountListAndBanner.isVisible = true;
      this.view.accountList.isVisible = false;
      this.view.AddExternalAccounts.isVisible = true;
      this.view.flxActions.isVisible = true;
      if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(this.view.customheader.lblHeaderMobile, kony.i18n.getLocalizedString('i18n.AccountsAggregation.AddExternalBankAccount'), accessibilityConfig);
      }
    },
    /**
         * Method to hide all external accounts
         */
    HideAllExternalAcc: function() {
      this.view.AddExternalAccounts.flxAcknowledgment.isVisible = false;
      this.view.AddExternalAccounts.flxLoginUsingSelectedBank.isVisible = false;
      this.view.AddExternalAccounts.flxSelectBankOrVendor.isVisible = false;
    },
    /**
         * Method to Navigate To External Bank Accounts List
         */
    NavigateToExternalBankAccountsList: function() {
      this.HideAll();
      this.NavigateToAddExternalAccounts();
      this.HideAllExternalAcc();
      this.view.AddExternalAccounts.flxLoginUsingSelectedBank.isVisible = true;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankMain.isVisible = false;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginSelectAccountsMain.isVisible = true;
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin, kony.i18n.getLocalizedString("i18n.AccountsAggregation.ExternalAccountsList.AddAccounts"), accessibilityConfig);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.toolTip = kony.i18n.getLocalizedString("i18n.AccountsAggregation.ExternalAccountsList.AddAccounts");
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), accessibilityConfig);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
      this.view.forceLayout();
      this.AdjustScreen();
    },
    /**
         * Method to present External Accounts List
         * @param {Array} externalAccountsList External accounts list
         */
    presentExternalAccountsList: function(externalAccountsList) {
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      this.isTermsAndConditionsAccepted = true;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.imgBank.src = this.ExternalLoginContextData.logo;
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.lblUsernameValue, this.ExternalLoginContextData.username, accessibilityConfig);
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.lblBankValue, this.ExternalLoginContextData.bankName, accessibilityConfig);
      if (CommonUtilities.isCSRMode()) {
        this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.onClick = CommonUtilities.disableButtonActionForCSRMode();
        this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.skin = CommonUtilities.disableButtonSkinForCSRMode();
        this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
        this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
      } else {
        this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.onClick = this.addExternalAccounts;
      }
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxCheckbox.onClick = this.onClickOfFlxTermsAndconditions;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnTermsAndConditions.toolTip = kony.i18n.getLocalizedString("i18n.common.TnC");
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankHeading.text = kony.i18n.getLocalizedString("i18n.AddExternalAccount.SelectYourAccounts");
      this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.isVisible = true;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxIAgree.isVisible = true;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxAddAccountsError.isVisible = false;
      //             this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.left = "37.07%";
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.isVisible = true;
      this.toggleBetweenTermsAcceptedAndUnaccepted();
      var accounts = this.getExternalAccountsData(externalAccountsList);
      this.setExternalAccountsData(accounts);
      this.AdjustScreen();
      this.onClickOfSegmentRow();
      this.NavigateToExternalBankAccountsList();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
         * This function shows appropriate message to user in case all the accounts of a particular external bank is already added
         */
    onAllExternalAccountsAlreadyAdded: function() {
      var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
      this.view.AddExternalAccounts.LoginUsingSelectedBank.imgBank.src = this.ExternalLoginContextData.logo;
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.lblUsernameValue, this.ExternalLoginContextData.username, accessibilityConfig);
      CommonUtilities.setText(this.view.AddExternalAccounts.LoginUsingSelectedBank.lblBankValue, this.ExternalLoginContextData.bankName, accessibilityConfig);
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxLoginUsingSelectedBankHeading.text = kony.i18n.getLocalizedString("i18n.AddExternalAccount.SelectYourAccounts");
      this.view.AddExternalAccounts.LoginUsingSelectedBank.segSelectedAccounts.isVisible = false;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxIAgree.isVisible = false;
      this.view.AddExternalAccounts.LoginUsingSelectedBank.flxAddAccountsError.isVisible = true;
      //             this.view.AddExternalAccounts.LoginUsingSelectedBank.btnCancel.left = "68.12%";
      this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin.isVisible = false;
      this.AdjustScreen();
      this.NavigateToExternalBankAccountsList();
      CommonUtilities.hideProgressBar(this.view);
    },
    responsiveViews: {},
    initializeResponsiveViews: function() {
      this.responsiveViews["accountList"] = this.isViewVisible("accountList");
      this.responsiveViews["AddExternalAccounts"] = this.isViewVisible("AddExternalAccounts");
      this.responsiveViews["flxAcknowledgment"] = this.isViewVisible("flxAcknowledgment");
      this.responsiveViews["flxLoginUsingSelectedBank"] = this.isViewVisible("flxLoginUsingSelectedBank");
      this.responsiveViews["flxSelectBankOrVendor"] = this.isViewVisible("flxSelectBankOrVendor");
    },
    isViewVisible: function(container) {
      if (this.view[container] == undefined) {
        return this.view.AddExternalAccounts[container].isVisible;
      } else {
        return this.view[container].isVisible
      }
    },
    /**
         * onBreakpointChange : Handles ui changes on .
         * @member of {frmAccountsLandingController}
         * @param {integer} width - current browser width
         * @return {}
         * @throws {}
         */
    onBreakpointChange: function(width) {
      kony.print('on breakpoint change');
      var config = applicationManager.getConfigurationManager();
      this.loadAccountModule().presentationController.getAccountDashboardCampaignsOnBreakpointChange();
      orientationHandler.onOrientationChange(this.onBreakpointChange, function() {
        this.view.customheader.onBreakpointChangeComponent(width);
        if (orientationHandler.isMobile) {
          var width;
          if (kony.application.getCurrentBreakpoint() > 640) {
            width = applicationManager.getDeviceUtilManager().getDeviceInfo().screenHeight - 20;
          } else {
            width = applicationManager.getDeviceUtilManager().getDeviceInfo().screenWidth - 20;
          }
          var height = width * 0.44;
          this.view.flxBannerContainerMobile.height = height + "dp";
          this.view.imgBannerMobile.height = height - 20 + "dp";
          this.AdjustScreen();
        }
      }.bind(this));
      this.view.customheader.onBreakpointChangeComponent(width);
      this.view.customfooter.onBreakpointChangeComponent(width);
      this.setupFormOnTouchEnd(width);
      var scope = this;
      var views;
      var data;
      var responsiveFonts = new ResponsiveFonts();
      this.layoutWarningFlexes();
      if (width === 640 || orientationHandler.isMobile) {
        this.nullifyPopupOnTouchStart();
        views = Object.keys(this.responsiveViews);
        views.forEach(function(e) {
          if (scope.view[e] == undefined) {
            scope.view.AddExternalAccounts[e].isVisible = scope.responsiveViews[e];
          } else {
            scope.view[e].isVisible = scope.responsiveViews[e];
          }
        });
        this.view.mySpending.lblOverallSpendingMySpending.text = kony.i18n.getLocalizedString("i18n.PFM.OverallSpending");
        this.view.imgBannerMobile.src = config.getConfigurationValue("DESKTOP_BANNER_IMAGE");
        this.view.FavouriteAccountTypes.zIndex = 10;
        if (this.view.AddExternalAccounts.isVisible === true) {
          var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
          CommonUtilities.setText(this.view.customheader.lblHeaderMobile, kony.i18n.getLocalizedString('i18n.AccountsAggregation.AddExternalBankAccount'), accessibilityConfig);
        } else {
          var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
          CommonUtilities.setText(this.view.customheader.lblHeaderMobile, kony.i18n.getLocalizedString('i18n.hamburger.myaccounts'), accessibilityConfig);
        }
        this.view.customheader.imgKony.isVisible = false;
        responsiveFonts.setMobileFonts();
        this.view.AddExternalAccounts.SelectBankOrVendor.btnReset.skin = "sknBtnffffffBorder0273e31pxRadius2px";
        data = this.view.accountList.segAccounts.data;
        if (data == undefined) {
          return;
        }
        data.map(function(e) {
          e.template = "flxAccountListItemMobile";
        });
        this.view.accountList.segAccounts.setData(data);
      } else {
        views = Object.keys(this.responsiveViews);
        views.forEach(function(e) {
          if (scope.view[e] == undefined) {
            scope.view.AddExternalAccounts[e].isVisible = scope.responsiveViews[e];
          } else {
            scope.view[e].isVisible = scope.responsiveViews[e];
          }
        });
        if (width == 1366) {
          this.view.flxBannerContainerDesktop.height = ((0.286 * kony.os.deviceInfo().screenWidth) / 2.23) + "dp";
        }
        var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
        CommonUtilities.setText(this.view.customheader.lblHeaderMobile, "", accessibilityConfig);
        responsiveFonts.setDesktopFonts();
        this.view.mySpending.lblOverallSpendingMySpending.text = kony.i18n.getLocalizedString("i18n.Accounts.Spending");
        data = this.view.accountList.segAccounts.data;
        if (data == undefined) {
          return;
        }
        data.map(function(e) {
          e.template = "flxAccountListItem";
        });
        this.view.accountList.segAccounts.setData(data);
      }
      this.AdjustScreen();
    },
    setupFormOnTouchEnd: function(width) {
      if (width == 640) {
        this.view.onTouchEnd = function() {}
        this.nullifyPopupOnTouchStart();
      } else {
        if (width == 1024) {
          this.view.onTouchEnd = function() {}
          this.nullifyPopupOnTouchStart();
        } else {
          this.view.onTouchEnd = function() {
            hidePopups();
          }
        }
        var userAgent = kony.os.deviceInfo().userAgent;
        if (userAgent.indexOf("iPad") != -1) {
          this.view.onTouchEnd = function() {}
          this.nullifyPopupOnTouchStart();
        } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
          this.view.onTouchEnd = function() {}
          this.nullifyPopupOnTouchStart();
        }
      }
    },
    nullifyPopupOnTouchStart: function() {
      this.view.accountList.flxAccountsRightContainer.onTouchStart = null;
    },
    layoutWarningFlexes: function() {
      this.view.imgDowntimeWarning.centerY = "";
      this.view.imgDowntimeWarning.top = "10dp";
      this.view.lblDowntimeWarning.centerY = "";
      //this.view.lblDowntimeWarning.height = "preferred";
      this.view.lblImgCloseDowntimeWarning.centerY = "";
      this.view.lblImgCloseDowntimeWarning.top = "10dp";
      this.view.lblImgCloseDowntimeWarning.left = "";
      this.view.lblImgCloseDowntimeWarning.right = "10dp";
      this.view.lblImgCloseDowntimeWarning.height = "20dp";
      this.view.lblImgCloseDowntimeWarning.width = "20dp";
      this.view.DeviceRegisrationWarning.centerY = "";
      this.view.DeviceRegisrationWarning.top = "10dp";
      this.view.lblDeviceRegistrationWarning.centerY = "";
      //this.view.lblDeviceRegistrationWarning.height = "preferred";
      this.view.lblImgDeviceRegistrationClose.centerY = "";
      this.view.lblImgDeviceRegistrationClose.top = "10dp";
      this.view.lblImgDeviceRegistrationClose.left = "";
      this.view.lblImgDeviceRegistrationClose.right = "10dp";
      this.view.lblImgDeviceRegistrationClose.height = "20dp";
      this.view.lblImgDeviceRegistrationClose.width = "20dp";
      this.view.ingPasswordResetWarning.centerY = "";
      this.view.ingPasswordResetWarning.top = "10dp";
      this.view.lblPasswordResetWarning.centerY = "";
      //this.view.lblPasswordResetWarning.height = "preferred";
      this.view.lblImgClosePasswordResetWarning.centerY = "";
      this.view.lblImgClosePasswordResetWarning.top = "10dp";
      this.view.lblImgClosePasswordResetWarning.left = "";
      this.view.lblImgClosePasswordResetWarning.right = "10dp";
      this.view.lblImgClosePasswordResetWarning.height = "20dp";
      this.view.lblImgClosePasswordResetWarning.width = "20dp";
      this.view.imgOverDraft.centerY = "";
      this.view.imgOverDraft.top = "10dp";
      this.view.lblOverdraftWarning.centerY = "";
      //this.view.lblOverdraftWarning.height = "preferred";
      this.view.lblImgCloseWarning.centerY = "";
      this.view.lblImgCloseWarning.top = "10dp";
      this.view.lblImgCloseWarning.left = "";
      this.view.lblImgCloseWarning.right = "10dp";
      this.view.lblImgCloseWarning.height = "20dp";
      this.view.lblImgCloseWarning.width = "20dp";
      this.view.imgInfoIconWarning.centerY = "";
      this.view.imgInfoIconWarning.top = "10dp";
      this.view.lblOutageWarning.centerY = "";
      //this.view.lblOutageWarning.height = "preferred";
      this.view.lblImgCloseOutageWarning.centerY = "";
      this.view.lblImgCloseOutageWarning.top = "10dp";
      this.view.lblImgCloseOutageWarning.left = "";
      this.view.lblImgCloseOutageWarning.right = "10dp";
      this.view.lblImgCloseOutageWarning.height = "20dp";
      this.view.lblImgCloseOutageWarning.width = "20dp";
    },
    bindTnC: function(TnCcontent) {
      var checkboxFlex = this.view.AddExternalAccounts.LoginUsingSelectedBank.flxIAgree;
      var checkboxIcon = this.view.AddExternalAccounts.LoginUsingSelectedBank.flxCheckbox;
      var btnTnC = this.view.AddExternalAccounts.LoginUsingSelectedBank.btnTermsAndConditions;
      var confirmButton = this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin;
      if (TnCcontent.alreadySigned) {
        checkboxFlex.setVisibility(false);
      } else {
        CommonUtilities.disableButton(confirmButton);
        checkboxIcon.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
        checkboxIcon.onClick = this.toggleTnC.bind(this, checkboxIcon, confirmButton);
        checkboxFlex.setVisibility(true);
        if (TnCcontent.contentTypeId === OLBConstants.TERMS_AND_CONDITIONS_URL) {
          btnTnC.onClick = function() {
            window.open(TnCcontent.termsAndConditionsContent);
          }
        } else {
          btnTnC.onClick = this.showTermsAndConditionPopUp;
          this.setTnCDATASection(TnCcontent.termsAndConditionsContent, TnCcontent.termsAndConditionsContent);
          FormControllerUtility.setHtmlToBrowserWidget(this, this.view.brwBodyTnC, TnCcontent.termsAndConditionsContent);
        }
        this.view.flxClose.onClick = this.hideTermsAndConditionPopUp;
      }
      this.view.forceLayout();
    },
    showTermsAndConditionPopUp: function() {
      var height = this.view.flxFooter.info.frame.y + this.view.flxFooter.info.frame.height;
      this.view.flxTermsAndConditionsPopUp.height = height + "dp";
      this.view.flxTermsAndConditionsPopUp.setVisibility(true);
      this.view.forceLayout();
    },
    hideTermsAndConditionPopUp: function() {
      this.view.flxTermsAndConditionsPopUp.setVisibility(false);
    },
    setTnCDATASection: function(content) {
      this.view.rtxTC.text = content;
    },
    toggleTnC: function(widget, confirmButton) {
      CommonUtilities.toggleFontCheckbox(widget, confirmButton);
      if (widget.text === OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED)
        CommonUtilities.disableButton(confirmButton);
      else
        CommonUtilities.enableButton(confirmButton);
    },




    /**
     * Component setGraphData
     * Create Dounut chart and bind data
     */
    setGraphData: function(resData) {
      var donutData=resData.response.AssetList;
      this.view.totalAssets.createDonutChart(donutData);
    },

    /**
     * Component formatDonutChartData
     * Process the data for the chart
     * @return: {Array} - chart data
     */
    formatDonutChartData: function() {
      var scope = this;
      var donutChartData = {};
      var installmentData = [];
      for(var i = 1; i < 7; i++) {
        var fieldData = eval("scope._installmentDetailField" + i);
        if(fieldData && typeof(fieldData) == "string") {
          fieldData = JSON.parse(fieldData);
        }
        if(fieldData) {
          fieldData.color = "#"+fieldData.color;
          installmentData.push(fieldData)
        }
      }
      function addRequireDonutChartFields(installment) {
        installment.label = scope.parseJsonAndGetValue(installment.legend);
        installment.Value = scope.parseJsonAndGetValue(installment.data);
        installment.colorCode = scope.parseJsonAndGetValue(installment.color);
        return installment;
      }
      var donutChartData = installmentData.map(addRequireDonutChartFields);
      return donutChartData;
    },
    checkPermission: function() {
      let self = this;
      
      // WATCHLIST CHECK AND PASS PERMISSION TO COMPONENT
      let watchListAddInstrumentPermission = applicationManager.getConfigurationManager().checkUserPermission("WATCH_LIST_INSTRUMENT_VIEW");
      self.view.flxWatchlistContainer.isVisible = watchListAddInstrumentPermission;
      var watchlistParams = {
            "sortBy":"volume",
		    "pageSize": "5",
		    "pageOffset": "0"
      };
      self.view.WatchlistDashCard.getCriteria(watchlistParams, watchListAddInstrumentPermission);
      
      //RECENT ACTIVITY CHECK AND PASS PERMISSION TO Component
      let recentActivityPermission = applicationManager.getConfigurationManager().checkUserPermission("WEALTH_INVESTMENT_DETAILS_RECENT_ACTIVITY_VIEW");
      self.view.flxRecentActivity.isVisible = recentActivityPermission;
      var customerId = userManager.getBackendIdentifier();
      var recentActivParams = {
          "customerId": customerId 
       };
       self.view.recentActivityComp.getCustomerId(recentActivParams, recentActivityPermission);
      
      // TOP NEWS AND RECENT MARKETS PERMISSIONS CHECK AND PASS TO COMPONENT
       // get market index business component
      var marketIndexParam = {};
      let marketIndexPermission = applicationManager.getConfigurationManager().checkUserPermission("WEALTH_MARKET_AND_NEWS_MARKET_VIEW");
      let marketTopNewsPermission = applicationManager.getConfigurationManager().checkUserPermission("WEALTH_MARKET_AND_NEWS_TOP_NEWS_VIEW");
      let marketNewsViewDetails = applicationManager.getConfigurationManager().checkUserPermission("WEALTH_MARKET_AND_NEWS_TOP_NEWS_DETAILS_VIEW");    
      self.view.marketIndexDashComp.isVisible = marketIndexPermission;
      self.view.marketIndexDashComp.getCriteria(marketIndexParam, marketIndexPermission, marketNewsViewDetails);
      
      // get Market News Component
      var marketNewsParam = {
          "Topic": "OLUSTOPNEWS",
          "pageSize": 4,
           "pageOffset": 0
      };
      self.view.marketNewsCardComp.isVisible = marketTopNewsPermission;
      self.view.marketNewsCardComp.getCriteria(marketNewsParam, marketTopNewsPermission, marketNewsViewDetails);
      if(marketIndexPermission === false && marketTopNewsPermission === false) {
        self.view.flxMarketNewsContainer.isVisible = false;
      }
      
      // CHECK ASSETS PERMISSIONS
      
      let assetsPermission = applicationManager.getConfigurationManager().checkUserPermission("WEALTH_INVESTMENT_DETAILS_TOTAL_ASSETS_VIEW");
      this.view.flxTotalAssetsContainer.isVisible = assetsPermission;
      if(assetsPermission === true){
         this.getAssetsList();
      }
      // CHECK PORTOFOLIO PERMISSIONS
      let portofolioPermission = applicationManager.getConfigurationManager().checkUserPermission("WEALTH_INVESTMENT_DETAILS_INVESTMENT_SUMMARY_VIEW");
      this.view.flxInvestmentSummaryContainer.isVisible = portofolioPermission;
      if(portofolioPermission === true){
        this.onFilterChanged(this.chartDefaultValue);
        this.getPortfolioList();
      }
      
      
    }
  };
});