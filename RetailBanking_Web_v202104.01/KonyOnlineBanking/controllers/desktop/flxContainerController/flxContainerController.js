define(['FormControllerUtility'], function(FormControllerUtility) {
    return {
        segmentHistoryRowClick: function() {
            var currForm = kony.application.getCurrentForm();
            var index = currForm.tableView.segmentBillpay.selectedRowIndex[1];
            var data = currForm.tableView.segmentBillpay.data;
            kony.print("index:" + index);
            data[index].imgDropdown = "arrow_down.png";
            if (kony.application.getCurrentBreakpoint() == 640) {
                data[index].template = "flxBillPayHistoryMobile";
            } else {
                data[index].template = "flxMain";
            }
            currForm.tableView.segmentBillpay.setDataAt(data[index], index);
            this.AdjustScreen(-95);
        },
        showPayABill: function() {
            var currForm = kony.application.getCurrentForm();
            currForm.payABill.isVisible = true;
            currForm.breadcrumb.setBreadcrumbData([{
                text: "BILL PAY"
            }, {
                text: "PAY A BILL"
            }]);
            currForm.btnConfirm.setVisibility(false);
            currForm.tableView.isVisible = false;
            this.AdjustScreen(30);
        },
        AdjustScreen: function(data) {
            var currentForm = kony.application.getCurrentForm();
            FormControllerUtility.updateWidgetsHeightInInfo(currentForm, ['customheader',
                'flxContainer',
                'flxFooter'
            ]);
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = currentForm.customheader.info.frame.height + currentForm.flxContainer.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - currentForm.flxFooter.info.frame.height;
                if (diff > 0)
                    currentForm.flxFooter.top = mainheight + diff + data + "dp";
                else
                    currentForm.flxFooter.top = mainheight + data + "dp";
                currentForm.forceLayout();
            } else {
                currentForm.flxFooter.top = mainheight + data + "dp";
                currentForm.forceLayout();
            }
        },

    };
});