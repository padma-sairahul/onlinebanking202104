define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        phoneInput: true,
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.recipientDetails) {
                this.setFormData(viewPropertiesMap.recipientDetails);
            }
            if (viewPropertiesMap.showSendMoneyToNewRecipientView) {
                this.showSendMoneyToNewRecipientView();
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Update button actions for send money to new recipient flow
         */
        showSendMoneyToNewRecipientView: function() {
            let scopeObj = this;
            this.view.btnSendMoney.text = kony.i18n.getLocalizedString("i18n.Pay.SendMoney");
            this.view.btnSendMoney.onClick = function() {
                var payPerson = {};
                payPerson["context"] = "sendMoneyToNewRecipient";
                scopeObj.pickRecipientValuesForSendMoney(payPerson);
            };
        },

        /**
         * This method is used to pick the recipient values to pass to the send money screen in pay a person.
         * @param {Object} payPersonJSON - contains the context "sendMoneyToNewRecipient".
         */
        pickRecipientValuesForSendMoney: function(payPersonJSON) {
            var validationUtilityManager = applicationManager.getValidationUtilManager();
            var payeeName = this.view.tbxRecipientName.text.trim();
            var nickName = this.view.tbxNickname.text.trim();
            payPersonJSON["name"] = commonUtilities.changedataCase(payeeName);
            payPersonJSON["nickName"] = commonUtilities.changedataCase(nickName);
            if (validationUtilityManager.isValidEmail(this.view.tbxEmail.text.trim()) && this.view.tbxEmail.text.trim() !== "") {
                payPersonJSON["email"] = this.view.tbxEmail.text.trim();
            }
            if (validationUtilityManager.isValidEmail(this.view.tbxAlternateEmail.text.trim()) && this.view.tbxAlternateEmail.text.trim() !== "") {
                payPersonJSON["secondaryEmail"] = this.view.tbxAlternateEmail.text.trim();
            }
            if (validationUtilityManager.isValidPhoneNumber(this.view.tbxPhoneNumber.text.trim()) && this.view.tbxPhoneNumber.text.trim() !== "") {
                payPersonJSON["phone"] = this.view.tbxPhoneNumber.text.trim();
            }
            if (validationUtilityManager.isValidPhoneNumber(this.view.tbxAlternatePhone.text.trim()) && this.view.tbxAlternatePhone.text.trim() !== "") {
                payPersonJSON["secondaryPhoneNumber"] = this.view.tbxAlternatePhone.text.trim();
            }
            if (!validationUtilityManager.isValidEmail(payPersonJSON["email"]) && validationUtilityManager.isValidEmail(payPersonJSON["secondaryEmail"])) {
                payPersonJSON["email"] = payPersonJSON["secondaryEmail"];
                payPersonJSON["secondaryEmail"] = "";
            }
            if (!validationUtilityManager.isValidPhoneNumber(payPersonJSON["phone"]) && validationUtilityManager.isValidPhoneNumber(payPersonJSON["secondaryPhoneNumber"])) {
                payPersonJSON["phone"] = payPersonJSON["secondaryPhoneNumber"];
                payPersonJSON["secondaryPhoneNumber"] = "";
            }
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('PayAPersonModuleNew').presentationController.onSendMoney(payPersonJSON);
        },

        /**
         * Contorls secondary action dropdown
         */
        showSecondaryActions: function() {
            this.view.imgDropdown.src = (this.view.imgDropdown.src === ViewConstants.IMAGES.ARROW_DOWN) ? ViewConstants.IMAGES.CHEVRON_UP : ViewConstants.IMAGES.ARROW_DOWN;
            this.view.secondaryActions.setVisibility(!this.view.secondaryActions.isVisible);
        },

        /**
         * Set up navigation on clicks for
         */
        secondaryActionsNavigation: function() {
            let index = this.view.secondaryActions.segAccountTypes.selectedRowIndex[1]
            if (index === 1)
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.loadP2PSettingsScreen();
            else
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showDeactivateP2P();
        },

        /**
         * To enable/disable add recipient button based on inputs
         */
        validateInput: function() {
            var validationUtilityManager = applicationManager.getValidationUtilManager();
            var name = this.view.tbxRecipientName.text.trim();
            if (name) {
                if (this.phoneInput) {
                    var phone1 = this.view.tbxPhoneNumber.text.trim();
                    var phone2 = this.view.tbxAlternatePhone.text.trim();
                    if (phone1 !== "" && validationUtilityManager.isValidPhoneNumber(phone1) && phone1.length == 10 && phone2 === "") {
                        FormControllerUtility.enableButton(this.view.btnSendMoney);
                    } else if (phone1 !== "" && validationUtilityManager.isValidPhoneNumber(phone1) && phone1.length == 10 && phone2 !== "" && validationUtilityManager.isValidPhoneNumber(phone2) && phone2.length == 10) {
                        FormControllerUtility.enableButton(this.view.btnSendMoney);
                    } else {
                        FormControllerUtility.disableButton(this.view.btnSendMoney);
                    }
                } else {
                    var email1 = this.view.tbxEmail.text.trim();
                    var email2 = this.view.tbxAlternateEmail.text.trim();
                    if (email1 !== "" && validationUtilityManager.isValidEmail(email1) && email2 === "") {
                        FormControllerUtility.enableButton(this.view.btnSendMoney);
                    } else if (email1 !== "" && validationUtilityManager.isValidEmail(email1) && email2 !== "" && validationUtilityManager.isValidEmail(email2)) {
                        FormControllerUtility.enableButton(this.view.btnSendMoney);
                    } else {
                        FormControllerUtility.disableButton(this.view.btnSendMoney);
                    }
                }
            } else {
                FormControllerUtility.disableButton(this.view.btnSendMoney);
            }
        },

        /**
         * Display the appropriate input flex
         * @param {String} type Denotes type of input "phone"/"email"
         */
        showInput: function(type) {
            let phone = true
            let scopeObj = this
            if (type !== "phone")
                phone = false
            return function() {
                scopeObj.phoneInput = phone;
                scopeObj.view.flxAddPhoneNumber.setVisibility(phone);
                scopeObj.view.imgRadioPhone.src = phone ? ViewConstants.IMAGES.RADIO_BUTTON_ACTIVE : ViewConstants.IMAGES.RADIO_BTN_INACTIVE;
                scopeObj.view.flxAddEmail.setVisibility(!phone);
                scopeObj.view.imgRadioEmail.src = !phone ? ViewConstants.IMAGES.RADIO_BUTTON_ACTIVE : ViewConstants.IMAGES.RADIO_BTN_INACTIVE;
                scopeObj.view.tbxPhoneNumber.text = "";
                scopeObj.view.tbxAlternatePhone.text = "";
                scopeObj.view.tbxEmail.text = "";
                scopeObj.view.tbxAlternateEmail.text = "";
                commonUtilities.disableButton(scopeObj.view.btnSendMoney);
            }
        },

        /**
         * Collects form data 
         */
        getFormData: function() {
            let recipient = {}
            recipient["phone"] = this.view.tbxPhoneNumber.text;
            recipient["secondaryPhoneNumber"] = this.view.tbxAlternatePhone.text;
            recipient["email"] = this.view.tbxEmail.text;
            recipient["secondaryEmail"] = this.view.tbxAlternateEmail.text;
            recipient["name"] = commonUtilities.changedataCase(this.view.tbxRecipientName.text.trim());
            recipient["nickName"] = commonUtilities.changedataCase(this.view.tbxNickname.text.trim());
            recipient["primaryContactForSending"] = this.phoneInput ? recipient["phone"] : recipient["email"]
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showAddRecipientDetailsView(recipient);
        },

        /**
         * Sets data to recipient details
         * @param {JSON} recipient - recipient data
         */
        setFormData: function(recipient) {
            this.view.tbxRecipientName.text = recipient["name"];
            this.view.tbxNickname.text = recipient["nickName"];
            let phone = recipient["phone"].trim() ? true : false;
            this.phoneInput = phone;
            this.view.flxAddPhoneNumber.setVisibility(phone);
            this.view.flxAddEmail.setVisibility(!phone);
            this.view.imgRadioPhone.src = phone ? ViewConstants.IMAGES.RADIO_BUTTON_ACTIVE : ViewConstants.IMAGES.RADIO_BTN_INACTIVE;
            this.view.imgRadioEmail.src = !phone ? ViewConstants.IMAGES.RADIO_BUTTON_ACTIVE : ViewConstants.IMAGES.RADIO_BTN_INACTIVE;
            if (phone) {
                this.view.tbxPhoneNumber.text = recipient["phone"];
                this.view.tbxAlternatePhone.text = recipient["secondaryPhoneNumber"];
            } else {
                this.view.tbxEmail.text = recipient["email"];
                this.view.tbxAlternateEmail.text = recipient["secondaryEmail"];
            }
        },
        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };
            this.view.secondaryActions.segAccountTypes.onRowClick = this.secondaryActionsNavigation;

            this.view.flxWhatElse.onClick = this.showSecondaryActions;
            this.view.flxRadioBtnPhone.onClick = this.showInput("phone");
            this.view.flxRadioBtnEmail.onClick = this.showInput("email");
            this.view.tbxRecipientName.onKeyUp = this.validateInput;
            this.view.tbxPhoneNumber.onKeyUp = this.validateInput;
            this.view.tbxAlternatePhone.onKeyUp = this.validateInput;
            this.view.tbxEmail.onKeyUp = this.validateInput;
            this.view.tbxAlternateEmail.onKeyUp = this.validateInput;
            this.view.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showPayAPerson();
            }
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
            commonUtilities.disableButton(this.view.btnSendMoney);
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            this.resetUI();
        },

        /**
         * resets form UI
         */
        resetUI: function() {
            this.view.tbxRecipientName.text = "";
            this.view.tbxNickname.text = "";
            this.view.tbxPhoneNumber.text = "";
            this.view.tbxAlternatePhone.text = "";
            this.view.tbxEmail.text = "";
            this.view.tbxAlternateEmail.text = "";
            this.view.imgRadioPhone.src = ViewConstants.IMAGES.RADIO_BUTTON_ACTIVE;
            this.view.imgRadioEmail.src = ViewConstants.IMAGES.RADIO_BTN_INACTIVE;
            this.view.flxAddPhoneNumber.setVisibility(true);
            this.view.flxAddEmail.setVisibility(false);
            this.view.btnSendMoney.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ADD");
            this.view.btnSendMoney.onClick = this.getFormData;
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        }
    }
});