define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Toggles checkbox and enables/disables button
         */
        toggleTnC: function() {
            if (this.view.imgIAgree.src === ViewConstants.IMAGES.UNCHECKED_IMAGE) {
                this.view.imgIAgree.src = ViewConstants.IMAGES.CHECKED_IMAGE;
                commonUtilities.enableButton(this.view.btnCreateNewAccount);
            } else {
                this.view.imgIAgree.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
                commonUtilities.disableButton(this.view.btnCreateNewAccount);
            }
        },

        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };
            this.view.flxIAgree.onClick = this.toggleTnC;
            this.view.btnNoThanks.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule').presentationController.showAccountsDashboard();
            };
            this.view.btnCreateNewAccount.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule").presentationController.showNewAccountOpening();
            };
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            this.resetUI()
        },

        /**
         * Resets the UI
         */
        resetUI: function() {
            commonUtilities.disableButton(this.view.btnCreateNewAccount);
            this.view.imgIAgree.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
        },
        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        }
    }
});