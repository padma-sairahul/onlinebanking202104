define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    var sendMoneyPaymentJSON = null;
    var TnCcontentTransfer = null;
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.sendMoneyPaymentJSON) {
                this.sendMoneyPaymentJSON = viewPropertiesMap.sendMoneyPaymentJSON;
                this.TnCcontentTransfer = viewPropertiesMap.TnCcontentTransfer;
                this.setReviewData();
            }
            if (viewPropertiesMap.TnCcontentTransfer) {
                this.view.rtxTnC.text = viewPropertiesMap.TnCcontentTransfer.termsAndConditionsContent;
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Set data for review screen
         */
        setReviewData: function() {
            this.view.lblFromValue.text = this.sendMoneyPaymentJSON["From"];
            this.view.lblToValue.text = this.sendMoneyPaymentJSON["To"];
            this.view.lblAmountValue.text = this.sendMoneyPaymentJSON["Amount"];
            this.view.lblSendOnValue.text = this.sendMoneyPaymentJSON["Date"];
            this.view.lblNoteValue.text = this.sendMoneyPaymentJSON["Note"];
            this.view.lblFrequencyValue.text = this.sendMoneyPaymentJSON["frequencyType"];
            if (this.sendMoneyPaymentJSON["frequencyType"] !== "Once") {
                this.view.flxRecurrence.setVisibility(true);
                this.view.lblRecurrenceValue.text = this.sendMoneyPaymentJSON["numberOfRecurrences"];
            } else {
                this.view.flxRecurrence.setVisibility(false);
            }
            if (applicationManager.getConfigurationManager().serviceFeeFlag === "true") {
                this.view.flxContainer1.setVisibility(true);
                let fee = applicationManager.getConfigurationManager().p2pServiceFee;
                this.view.lblFeesValue.text = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency(fee),
                    this.view.lblTotalValue.text = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency((Number(totalPaymentMoney) + Number(fee)))
            } else {
                this.view.flxContainer1.setVisibility(false);
            }
            if (this.TnCcontentTransfer) {
                if (this.TnCcontentTransfer.alreadySigned) {
                    this.view.flxTnCAcceptance.setVisibility(false);
                    commonUtilities.enableButton(this.view.btnConfirm);
                } else {
                    this.view.flxTnCAcceptance.setVisibility(true);
                    commonUtilities.disableButton(this.view.btnConfirm);
                    this.view.imgCheckbox.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                    if (this.TnCcontentTransfer.contentTypeId === OLBConstants.TERMS_AND_CONDITIONS_URL) {
                        this.view.lblTnC.onTouchStart = function() {
                            window.open(TnCcontentTransfer.termsAndConditionsContent);
                        }
                    }
                    this.view.flxCheckbox.onClick = function() {
                        if (this.view.imgCheckbox.src === ViewConstants.IMAGES.UNCHECKED_IMAGE) {
                            this.view.imgCheckbox.src = ViewConstants.IMAGES.CHECKED_IMAGE;
                            commonUtilities.enableButton(this.view.btnConfirm);
                        } else {
                            this.view.imgCheckbox.src = ViewConstants.IMAGES.UNCHECKED_IMAGE
                            commonUtilities.disableButton(this.view.btnConfirm);
                        }
                    }.bind(this);
                }
            }
        },

        /**
         * Contorls secondary action dropdown
         */
        showSecondaryActions: function() {
            this.view.imgDropdown.src = (this.view.imgDropdown.src === ViewConstants.IMAGES.ARROW_DOWN) ? ViewConstants.IMAGES.CHEVRON_UP : ViewConstants.IMAGES.ARROW_DOWN;
            this.view.secondaryActions.setVisibility(!this.view.secondaryActions.isVisible);
        },

        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };

            this.view.btnConfirm.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.checkMFAP2PSendMoney(this.sendMoneyPaymentJSON);
            }.bind(this);
            this.view.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showPayAPerson();
            }
            this.view.btnModify.onClick = function() {
                applicationManager.getNavigationManager().navigateTo('frmP2PSendMoney', false, {
                    'reset': false
                });
            }
            this.view.lblTnC.onTouchStart = function() {
                scopeObj.view.flxDialogs.setVisibility(true);
                scopeObj.view.flxTnCPopup.setVisibility(true);
            }
            this.view.flxCloseTnCPopup.onClick = function() {
                scopeObj.view.flxDialogs.setVisibility(false);
                scopeObj.view.flxTnCPopup.setVisibility(false);
            }
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
            this.resetUI();
        },

        /**
         * 
         */
        resetUI: function() {
            commonUtilities.disableButton(this.view.btnConfirm);
            this.view.imgCheckbox.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        }
    }
});