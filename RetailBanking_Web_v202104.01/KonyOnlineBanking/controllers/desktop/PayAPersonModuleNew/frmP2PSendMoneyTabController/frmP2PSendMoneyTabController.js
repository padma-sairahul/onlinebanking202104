define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.SendMoneyTab) {
                this.sentOrRequestSortMap = [{
                    name: 'nickName',
                    imageFlx: this.view.imgSortRecipentName,
                    clickContainer: this.view.flxSortRecipient
                }];
                FormControllerUtility.setSortingHandlers(this.sentOrRequestSortMap, this.onSentOrRequestSortClickHandler, this);
                FormControllerUtility.updateSortFlex(this.sentOrRequestSortMap, viewPropertiesMap.pagination);
                this.setSendRequestSegmentData(viewPropertiesMap.SendMoneyTab, viewPropertiesMap.pagination);
            }
            if (viewPropertiesMap.noMoreRecords) {
                this.noRecords();
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Show no further records view
         */
        noRecords() {
            this.view.imgNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
            this.view.flxNext.setEnabled(false);
        },

        /**
         * This method is used as an onclick handler for send money tab.
         */
        onSentOrRequestSortClickHandler: function(event, data) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyTabView(data);
        },

        /**
         * setSendRequestSegmentData - Method that binds the data to sent segment.
         * @param {array}  data - list of recipients.
         * @param {Boolean} paginationStatus - flag to determine if we need to show the pagination or not.
         */
        setSendRequestSegmentData: function(data, paginationValues) {
            this.view.flxNext.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.fetchNextRecipientsList.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController, "SendMoneyTab");
            this.view.flxPrevious.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.fetchPreviousRecipientsList.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController, "SendMoneyTab");
            var dataMap = {
                "btnRequest": "btnRequest",
                "btnSend": "btnSend",
                "flxColumn1": "flxColumn1",
                "flxColumn2": "flxColumn2",
                "flxColumn3": "flxColumn3",
                "flxDetails": "flxDetails",
                "flxDropdown": "flxDropdown",
                "flxIdentifier": "flxIdentifier",
                "btnIdentifier": "btnIdentifier",
                "lblIdentifier": "lblIdentifier",
                "flxName": "flxName",
                "flxPrimaryContact": "flxPrimaryContact",
                "flxRequestAction": "flxRequestAction",
                "flxRow": "flxRow",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxSendAction": "flxSendAction",
                "flxSendRequestP2P": "flxSendRequestP2P",
                "flxSendRequestSelectedP2P": "flxSendRequestSelectedP2P",
                "flxSeperator": "flxSeperator",
                "imgDropdown": "imgDropdown",
                "lblFullName": "lblFullName",
                "lblFullName2": "lblFullName2",
                "lblName": "lblName",
                "lblPrimaryContact": "lblPrimaryContact",
                "lblRegisteredEmail1": "lblRegisteredEmail1",
                "lblRegisteredEmail2": "lblRegisteredEmail2",
                "lblRegisteredEmails": "lblRegisteredEmails",
                "lblRegisteredPhone": "lblRegisteredPhone",
                "lblRegisteredPhone1": "lblRegisteredPhone1",
                "lblRegisteredPhone2": "lblRegisteredPhone2",
                "lblSeparator": "lblSeparator",
                "lblSeparatorSelected": "lblSeparatorSelected",
                "imgRowSelected": "imgRowSelected",
                "lblEmpty": "lblEmpty"
            };
            data = data.map(function(dataItem) {
                return {
                    "btnRequest": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Request"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.PayAPerson.Request"),
                    },
                    "btnSend": {
                        "text": kony.i18n.getLocalizedString("i18n.common.send"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.common.send"),
                        "onClick": function() {
                            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.onSendMoney(dataItem);
                        }
                    },
                    "imgDropdown": {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Transaction details"
                        }
                    },
                    "lblFullName": kony.i18n.getLocalizedString("i18n.ProfileManagement.FullName"),
                    "lblFullName2": dataItem.name,
                    "lblName": dataItem.nickName ? dataItem.nickName : dataItem.name,
                    "lblPrimaryContact": dataItem.primaryContactForSending,
                    "lblRegisteredEmail1": dataItem.email ? dataItem.email : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblRegisteredEmail2": dataItem.secondaryEmail ? dataItem.secondaryEmail : " ",
                    "lblRegisteredEmails": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredEmail"),
                    "lblRegisteredPhone": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredPhone"),
                    "lblRegisteredPhone1": dataItem.phone ? dataItem.phone : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblRegisteredPhone2": dataItem.secondaryPhoneNumber2 ? dataItem.secondaryPhoneNumber2 : " ",
                    "template": (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) ? "flxSendRequestMobileP2P" : "flxSendRequestP2P",
                    "lblSeparator": ".",
                    "lblSeparatorSelected": "lblSeparatorSelected",
                    "imgRowSelected": ViewConstants.IMAGES.ARRAOW_UP,
                    "btnIdentifier": {
                        text: "u"
                    },
                    "flxIdentifier": {
                        "width": "0.63%"
                    },
                    "lblEmpty": " "
                };
            });
            if (data.length !== 0) {
                this.view.segRecipients.widgetDataMap = dataMap;
                this.view.segRecipients.setData(data);
                this.updatePaginationValue(paginationValues, kony.i18n.getLocalizedString("i18n.transfers.external_accounts"));
            } else {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showPayAPerson("NoRecipients");
            }
        },

        /**
         * This method is used to update the pagination widget values.
         * @param{Object} values - it contains the set of values like offset, limit.
         * @param{String} paginationText - String representing paginated attribute
         */
        updatePaginationValue: function(values, paginationText) {
            this.view.lblPagination.text = (values.offset + 1) + " - " + (values.offset + values.limit) + " " + paginationText;
            if (values.offset >= values.paginationRowLimit) {
                this.view.imgPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
                this.view.flxPrevious.setEnabled(true);
            } else {
                this.view.imgPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
                this.view.flxPrevious.setEnabled(false);
            }
            if (values.limit < values.paginationRowLimit) {
                this.view.imgNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
                this.view.flxNext.setEnabled(false);
            } else {
                this.view.imgNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
                this.view.flxNext.setEnabled(true);
            }
        },

        /**
         * Contorls secondary action dropdown
         */
        showSecondaryActions: function() {
            this.view.imgDropdown.src = (this.view.imgDropdown.src === ViewConstants.IMAGES.ARROW_DOWN) ? ViewConstants.IMAGES.CHEVRON_UP : ViewConstants.IMAGES.ARROW_DOWN;
            this.view.secondaryActions.setVisibility(!this.view.secondaryActions.isVisible);
        },

        /**
         * Set up navigation on clicks for
         */
        secondaryActionsNavigation: function() {
            let index = this.view.secondaryActions.segAccountTypes.selectedRowIndex[1]
            if (index === 1)
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.loadP2PSettingsScreen();
            else
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showDeactivateP2P();
        },

        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };

            this.view.flxWhatElse.onClick = this.showSecondaryActions;
            this.view.secondaryActions.segAccountTypes.onRowClick = this.secondaryActionsNavigation;

            this.view.btnSentTab.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSentTransactionsView();
            };
            this.view.btnManageRecipientsTab.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showManageRecipientsView();
            };
            this.view.btnAddRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showAddRecipientView();
            }
            this.view.btnSendMoneyNewRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyToNewRecipientView();
            }
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            this.view.flxWarning.setVisibility(false);
            applicationManager.getNavigationManager().applyUpdates(this);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            this.changeSwitchSegTempalte(width);
        },

        /**
         * to set expanded data in the segment
         */
        expandRow: function() {
            var rowIndex = this.view.segRecipients.selectedRowIndex[1];
            var data = this.view.segRecipients.data;
            for (let i = 0; i < data.length; i++) {
                if (i === rowIndex) {
                    data[i].imgDropdown = "chevron_up.png";
                    if (kony.application.getCurrentBreakpoint() === 640)
                        data[i].template = "flxSendRequestSelectedMobileP2P"
                    else
                        data[i].template = "flxSendRequestSelectedP2P";
                } else {
                    data[i].imgDropdown = "arrow_down.png";
                    if (kony.application.getCurrentBreakpoint() === 640)
                        data[i].template = "flxSendRequestMobileP2P";
                    else
                        data[i].template = "flxSendRequestP2P";
                }
            }
            this.view.segRecipients.setData(data);
        },

        /**
         * to collpase expanded row
         */
        collapseRow: function() {
            var rowIndex = this.view.segRecipients.selectedRowIndex[1];
            var data = this.view.segRecipients.data;
            if (kony.application.getCurrentBreakpoint() === 640) {
                data[rowIndex].template = "flxSendRequestMobileP2P";
            } else {
                data[rowIndex].template = "flxSendRequestP2P";
            }
            data[rowIndex].imgDropdown = "arrow_down.png";
            this.view.segRecipients.setDataAt(data[rowIndex], rowIndex);
        },

        /**
         * Function to change mobile and desktop tempalte of segement
         */
        changeSwitchSegTempalte: function(width) {
            let data = this.view.segRecipients.data;
            let template = (width === 640 || responsiveUtils.isMobile) ? "flxSendRequestMobileP2P" : "flxSendRequestP2P";
            if (data) {
                data.map(function(elem) {
                    elem.imgDropDown = "arrow_down.png";
                    return elem.template = template;
                });
                this.view.segRecipients.setData(data);
            }

        }
    }

});