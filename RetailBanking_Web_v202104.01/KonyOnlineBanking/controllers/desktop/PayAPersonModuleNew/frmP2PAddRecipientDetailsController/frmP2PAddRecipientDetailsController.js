define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.editRecipientView) {
                this.setEditOptions(viewPropertiesMap.recipientDetails);
            }
            if (viewPropertiesMap.recipientDetails) {
                this.setDetails(viewPropertiesMap.recipientDetails);
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Sets button actions for edit recipient flow
         */
        setEditOptions: function(recipient) {
            this.view.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showManageRecipientsView();
            };
            this.view.btnModify.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showEditRecipientModifyDetails();
            };
            this.view.btnConfirm.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.editRecipient(recipient);
            };
        },

        /**
         * Sets details of the recipient
         * @param {JSON} recipient - data of the recipient
         */
        setDetails: function(recipient) {
            this.view.lblPrimaryContactValue.text = recipient["primaryContactForSending"];
            this.view.lblPayeeNameValue.text = recipient["name"];
            this.view.lblNickNameValue.text = recipient["nickName"];
            let phoneData = recipient["phone"] ? true : false;
            this.view.flxEmail.setVisibility(!phoneData);
            this.view.flxAlternateEmail.setVisibility(!phoneData);
            this.view.flxPhone.setVisibility(phoneData);
            this.view.flxAlternatePhone.setVisibility(phoneData);
            if (phoneData) {
                this.view.lblPhoneValue.text = recipient["phone"];
                this.view.lblAlternatePhoneValue.text = recipient["secondaryPhoneNumber"].trim() ? recipient["secondaryPhoneNumber"] : kony.i18n.getLocalizedString("i18n.common.none");
            } else {
                this.view.lblEmailValue.text = recipient["email"];
                this.view.lblAlternateEmailValue.text = recipient["secondaryEmail"].trim() ? recipient["secondaryEmail"] : kony.i18n.getLocalizedString("i18n.common.none");
            }
        },

        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            this.resetUI();
        },

        resetUI: function() {
            this.view.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyTabView();
            };
            this.view.btnModify.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showAddRecipientModifyDetails();
            };
            this.view.btnConfirm.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.createP2PPayee();
            };
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        }
    }
});