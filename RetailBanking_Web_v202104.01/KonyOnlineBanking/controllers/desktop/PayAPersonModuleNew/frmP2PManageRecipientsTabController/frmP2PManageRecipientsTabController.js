define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.searchPayAPerson) {
                this.setManageRecipientSegmentData(viewPropertiesMap.searchPayAPerson.payAPersonData, {}, true);
            }
            if (viewPropertiesMap.ManageRecipientsTab) {
                if (applicationManager.getConfigurationManager().canSearchP2PPersons === "true") {
                    this.view.flxSearchWrapper.setVisibility(true);
                    this.view.flxSearchIcon.onClick = this.onSearchBtnClick.bind(this);
                } else {
                    this.view.flxSearchWrapper.setVisibility(false);
                }
                this.setManageRecipientSegmentData(viewPropertiesMap.ManageRecipientsTab, viewPropertiesMap.pagination);
                this.manageRecipientSortMap = [{
                    name: 'nickName',
                    imageFlx: this.view.imgSortRecipientName,
                    clickContainer: this.view.flxSortRecipient
                }];
                FormControllerUtility.setSortingHandlers(this.manageRecipientSortMap, this.onMangeRecipientSortClickHandler, this);
                FormControllerUtility.updateSortFlex(this.manageRecipientSortMap, viewPropertiesMap.pagination);
            }
            if (viewPropertiesMap.noMoreRecords) {
                this.noRecords();
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Show no further records view
         */
        noRecords() {
            this.view.imgNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
            this.view.flxNext.setEnabled(false);
        },

        /**
         * This method is used to check if the search string is valid or not and then to call the service for search pay a person.
         */
        onSearchBtnClick: function() {
            var scopeObj = this;
            var searchKeyword = this.view.tbxSearch.text.trim();
            if (searchKeyword.length >= 0 && scopeObj.prevSearchText !== searchKeyword) {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.searchPayAPerson({
                    'searchKeyword': searchKeyword
                });
                scopeObj.searchView = true;
                scopeObj.prevSearchText = '';
            }
        },

        /**
         * setManageRecipientSegmentData - Sets the data for Manage Recipient segment.
         * @param {array} managepayeesData- A list of recipients.
         */
        setManageRecipientSegmentData: function(managePayeesData, paginationValues, isSearch) {
            let self = this;
            this.view.flxNext.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.fetchNextRecipientsList.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController, "ManageRecipientsTab");
            this.view.flxPrevious.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.fetchPreviousRecipientsList.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController, "ManageRecipientsTab");
            if (!isSearch)
                this.view.flxPaginationControl.setVisibility(true);
            else
                this.view.flxPaginationControl.setVisibility(false);
            managePayeesData = managePayeesData.map(function(payeeRecord, index) {
                var btnDelete = {
                    "onClick": commonUtilities.isCSRMode() ? commonUtilities.disableButtonActionForCSRMode() : function() {
                        self.deleteP2PRecipient(index);
                    },
                    "text": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                };
                var btnEdit = {
                    "onClick": function() {
                        self.editP2PRecipient(payeeRecord);
                    },
                    "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                };
                var btnSendMoney = {
                    "onClick": function() {
                        var data = self.view.segRecipients.data;
                        var selectedData = data[index];
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.onSendMoney(selectedData);
                    },
                    "text": kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                };
                var btnViewActivity = {
                    "onClick": function() {
                        var data = self.view.segRecipients.data;
                        var selectedData = data[index];
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.getRecipientActivity(selectedData["PayPersonId"], selectedData);
                    },
                    "text": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                };
                var height = 85;
                if(payeeRecord.email){height = height+20;}
                if(payeeRecord.secondaryEmail){height = height+20;}
                if(payeeRecord.phone){height = height+20;}
                if(payeeRecord.secondaryPhoneNumber){height = height+20;}
                var flxDetails = {
                  "height": height+"dp"
                };
                var dataObject = {
                    "btnDelete": btnDelete,
                    "btnEdit": btnEdit,
                    "btnSendMoney": btnSendMoney,
                    "btnViewActivity": btnViewActivity,
                    "flxBottomSeperator": "flxBottomSeperator",
                    "flxColumn1": "flxColumn1",
                    "flxColumn2": "flxColumn2",
                    "flxColumn3": "flxColumn3",
                    "flxDeleteAction": "flxDeleteAction",
                    "flxDetails": flxDetails,
                    "flxDropdown": "flxDropdown",
                    "flxEditAction": "flxEditAction",
                    "btnIdentifier": "btnIdentifier",
                    "lblIdentifier": "lblIdentifier",
                    "ManageRecipient": "ManageRecipient",
                    "ManageRecipientSelected": "ManageRecipientSelected",
                    "flxName": "flxName",
                    "flxPrimaryContact": "flxPrimaryContacy",
                    "flxRow": "flxRow",
                    "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                    "flxSeperator": "flxSeperator",
                    "imgDropdown": {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Transaction details"
                        }
                    },
                    "lblName": payeeRecord.nickName ? payeeRecord.nickName : payeeRecord.name,
                    "lblPrimaryContact": payeeRecord.primaryContactForSending,
                    "lblRegisteredEmail1": payeeRecord.email ? payeeRecord.email : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblRegisteredEmail2": payeeRecord.secondaryEmail ? payeeRecord.secondaryEmail : "",
                    "lblRegisteredEmails": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredEmail"),
                    "lblRegisteredPhone": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredPhone"),
                    "lblRegisteredPhone1": payeeRecord.phone ? payeeRecord.phone : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblRegisteredPhone2": payeeRecord.secondaryPhoneNumber ? payeeRecord.secondaryPhoneNumber : "",
                    "recipientID": payeeRecord.PayPersonId,
                    "recipientNickName": payeeRecord.nickName,
                    "lblSeparator": "lblSeparator",
                    "lblSeparatorSelected": "lblSeparatorSelected",
                    "imgRowSelected": ViewConstants.IMAGES.ARRAOW_UP,
                    "template": (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) ? "flxManageRecipientMobileP2P" : "flxManageRecipientP2P",
                    "email": payeeRecord.email,
                    "secondaryEmail": payeeRecord.secondaryEmail,
                    "secondaryPhone": payeeRecord.secondaryPhoneNumber,
                    "name": payeeRecord.name,
                    "nickName": payeeRecord.nickName,
                    "phone": payeeRecord.phone,
                    "PayPersonId": payeeRecord.PayPersonId,
                    "flxIdentifier": {
                        "width": "0.63%"
                    },
                    "lblEmpty": " "
                };
                if (commonUtilities.isCSRMode()) {
                    dataObject.btnDelete.skin = commonUtilities.disableSegmentButtonSkinForCSRMode(13);
                }
                return dataObject;
            });
            var dataMap = {
                "btnDelete": "btnDelete",
                "btnEdit": "btnEdit",
                "btnRequestMoney": "btnRequestMoney",
                "btnSendMoney": "btnSendMoney",
                "btnViewActivity": "btnViewActivity",
                "flxBottomSeperator": "flxBottomSeperator",
                "flxColumn1": "flxColumn1",
                "flxColumn2": "flxColumn2",
                "flxColumn3": "flxColumn3",
                "flxDeleteAction": "flxDeleteAction",
                "flxDetails": "flxDetails",
                "flxDropdown": "flxDropdown",
                "flxEditAction": "flxEditAction",
                "flxIdentifier": "flxIdentifier",
                "lblIdentifier": "lblIdentifier",
                "ManageRecipient": "ManageRecipient",
                "ManageRecipientSelected": "ManageRecipientSelected",
                "flxName": "flxName",
                "flxPrimaryContact": "flxPrimaryContact",
                "flxRow": "flxRow",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxSeperator": "flxSeperator",
                "imgDropdown": "imgDropdown",
                "lblName": "lblName",
                "lblPrimaryContact": "lblPrimaryContact",
                "lblRegisteredEmail1": "lblRegisteredEmail1",
                "lblRegisteredEmail2": "lblRegisteredEmail2",
                "lblRegisteredEmails": "lblRegisteredEmails",
                "lblRegisteredPhone": "lblRegisteredPhone",
                "lblRegisteredPhone1": "lblRegisteredPhone1",
                "lblRegisteredPhone2": "lblRegisteredPhone2",
                "lblSeparator": "lblSeparator",
                "lblSeparatorSelected": "lblSeparatorSelected",
                "imgRowSelected": "imgRowSelected",
                "lblEmpty": "lblEmpty"
            };
            this.view.segRecipients.widgetDataMap = dataMap;
            if (managePayeesData.length > 0) {
                this.view.segRecipients.setData(managePayeesData);
                if (!isSearch)
                    this.updatePaginationValue(paginationValues, kony.i18n.getLocalizedString("i18n.transfers.external_accounts"));
            } else {
                if (!isSearch)
                    this.showNoRecipients();
                else {
                    this.showNoSearchRecipients();
                }
            }
            this.view.flxNoTransactions.setVisibility(false);
            this.view.segRecipients.setVisibility(true);
        },

        /**
         * Shows no recipients
         */
        showNoRecipients: function() {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showAddRecipientView();
        },

        /**
         * Shows no recipients on search
         */
        showNoSearchRecipients: function() {
            this.view.flxNoTransactions.setVisibility(true);
            this.view.segRecipients.setVisibility(false);
        },

        /**
         * This method is used to delete a recipient from manage recipients tab in pay a person.
         */
        deleteP2PRecipient: function(index) {
            let scopeObj = this;
            let data = this.view.segRecipients.data;
            let payeeID = data[index].recipientID;
            this.view.flxDialogs.setVisibility(true);
            this.view.flxDeleteRecipient.setVisibility(true);
            this.view.DeleteRecipientPopup.btnYes.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.deleteRecipient(payeeID);
                scopeObj.view.flxDialogs.setVisibility(false);
            };
        },

        /**
         * This method is used to edit the recipient in pay a person.
         */
        editP2PRecipient: function(payeeData) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.editP2PRecipient(payeeData);
        },

        /**
         * This method is used as an onclick handler for manage recipients tab.
         */
        onMangeRecipientSortClickHandler: function(event, data) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showManageRecipientsView(data);
        },

        /**
         * This method is used to update the pagination widget values.
         * @param{Object} values - it contains the set of values like offset, limit.
         * @param{String} paginationText - String representing paginated attribute
         */
        updatePaginationValue: function(values, paginationText) {
            this.view.lblPagination.text = (values.offset + 1) + " - " + (values.offset + values.limit) + " " + paginationText;
            if (values.offset >= values.paginationRowLimit) {
                this.view.imgPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
                this.view.flxPrevious.setEnabled(true);
            } else {
                this.view.imgPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
                this.view.flxPrevious.setEnabled(false);
            }
            if (values.limit < values.paginationRowLimit) {
                this.view.imgNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
                this.view.flxNext.setEnabled(false);
            } else {
                this.view.imgNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
                this.view.flxNext.setEnabled(true);
            }
        },

        /**
         * Contorls secondary action dropdown
         */
        showSecondaryActions: function() {
            this.view.imgDropdown.src = (this.view.imgDropdown.src === ViewConstants.IMAGES.ARROW_DOWN) ? ViewConstants.IMAGES.CHEVRON_UP : ViewConstants.IMAGES.ARROW_DOWN;
            this.view.secondaryActions.setVisibility(!this.view.secondaryActions.isVisible);
        },

        /**
         * Close delete recipiet popup
         */
        closePopup: function() {
            this.view.flxDialogs.setVisibility(false);
            this.view.flxDeleteRecipient.setVisibility(false);
        },

        /**
         * Init lifecycle function
         */
        init: function() {
            let scopeObj = this;
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };
            this.view.flxWhatElse.onClick = this.showSecondaryActions;
            this.view.btnSendMoneyTab.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showPayAPerson();
            }
            this.view.btnSentTab.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showPayAPerson("SentTransactionsTab");
            };

            this.view.DeleteRecipientPopup.flxCross.onClick = this.closePopup;
            this.view.DeleteRecipientPopup.btnNo.onClick = this.closePopup;
            this.view.btnAddRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showAddRecipientView();
            }
            this.view.btnSendMoneyNewRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyToNewRecipientView();
            }
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            this.changeSwitchSegTempalte(width);
        },

        /**
         * to set expanded data in the segment
         */
        expandRow: function() {
            var rowIndex = this.view.segRecipients.selectedRowIndex[1];
            var data = this.view.segRecipients.data;
            for (let i = 0; i < data.length; i++) {
                if (i === rowIndex) {
                    data[i].imgDropdown = "chevron_up.png";
                    if (kony.application.getCurrentBreakpoint() === 640)
                        data[i].template = "flxManageRecipientSelectedMobileP2P"
                    else
                        data[i].template = "flxManageRecipientSelectedP2P";
                } else {
                    data[i].imgDropdown = "arrow_down.png";
                    if (kony.application.getCurrentBreakpoint() === 640)
                        data[i].template = "flxManageRecipientMobileP2P";
                    else
                        data[i].template = "flxManageRecipientP2P";
                }
            }
            this.view.segRecipients.setData(data);
        },

        /**
         * to collpase expanded row
         */
        collapseRow: function() {
            var rowIndex = this.view.segRecipients.selectedRowIndex[1];
            var data = this.view.segRecipients.data;
            if (kony.application.getCurrentBreakpoint() === 640) {
                data[rowIndex].template = "flxManageRecipientMobileP2P";
            } else {
                data[rowIndex].template = "flxManageRecipientP2P";
            }
            data[rowIndex].imgDropdown = "arrow_down.png";
            this.view.segRecipients.setDataAt(data[rowIndex], rowIndex);
        },

        /**
         * Function to change mobile and desktop tempalte of segement
         */
        changeSwitchSegTempalte: function(width) {
            let data = this.view.segRecipients.data;
            let template = (width === 640 || responsiveUtils.isMobile) ? "flxManageRecipientMobileP2P" : "flxManageRecipientP2P";
            if (data) {
                data.map(function(elem) {
                    //           elem.imgDropDown = "arrow_down.png";
                    return elem.template = template;
                });
                this.view.segRecipients.setData(data);
            }

        }
    }

});