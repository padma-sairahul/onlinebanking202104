define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAddRecipient **/
    AS_Button_efef58950b734e4b97d8a32e2ede4f40: function AS_Button_efef58950b734e4b97d8a32e2ede4f40(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** onClick defined for btnSendMoneyNewRecipient **/
    AS_Button_h3d0d31d860e4dfb9ea1eb783c3ae2a2: function AS_Button_h3d0d31d860e4dfb9ea1eb783c3ae2a2(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** init defined for frmP2PManageRecipientsTab **/
    AS_Form_a6153474be22433c9a0ce861597e7776: function AS_Form_a6153474be22433c9a0ce861597e7776(eventobject) {
        var self = this;
        return self.init.call(this);
    }
});