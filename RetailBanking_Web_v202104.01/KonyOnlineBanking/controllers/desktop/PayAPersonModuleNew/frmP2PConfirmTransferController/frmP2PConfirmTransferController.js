define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.showRequestSendMoneyAck) {
                this.showAcknowledgementSendMoney(viewPropertiesMap.requestObj, viewPropertiesMap.status, viewPropertiesMap.accountName, viewPropertiesMap.accountBalance);
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * This method is used to show the acknowledgement send money screen.
         * @param {Object} requestObj - this contains p2p recipient info of name, nickname, phone, email and transaction object info like amount, frequncy and recurrenc.
         * @param {Object} status - this contains the info like transactionId.
         * @param {String} accountName - contains the from account name.
         * @param {Numebr} accountBalance - contains the from account balance.
         */
        showAcknowledgementSendMoney: function(requestObj, status, accountName, accountBalance) {
            this.view.lblAckMessage.text = kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyMessage") + " " + requestObj.To;
            this.view.ImgAcknowledged.src = ViewConstants.IMAGES.SUCCESS_GREEN;
            this.view.lblReferenceNumValue.text = status.referenceId ? status.referenceId : status.transactionId;
            this.view.lblAccountName.text = accountName;
            let currencySymbol = applicationManager.getFormatUtilManager().getCurrencySymbol(requestObj.transactionCurrency);
            this.view.lblAmountRemaining.text = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency(accountBalance, false, currencySymbol);
            var details = {};
            details["i18n.PayPerson.recipientName"] = requestObj.To;
            details["i18n.PayPerson.using"] = requestObj.p2pContact;
            details["i18n.StopCheckPayments.Amount"] = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency(requestObj.amount, false, currencySymbol);
            if (requestObj.frequencyType === "Once") {
                details["i18n.StopCheckPayments.Date"] = requestObj.Date;
            } else {
                message.message = kony.i18n.getLocalizedString("i18n.common.scheduled.acknowledgement") + " " + requestObj.To;
                if (requestObj.hasHowLong === "ON_SPECIFIC_DATE") {
                    details["i18n.transfers.start_date"] = requestObj.frequencyStartDate;
                    details["i18n.transfers.end_date"] = requestObj.frequencyEndDate;
                }
                if (requestObj.hasHowLong === "NO_OF_RECURRENCES") {
                    details["i18n.StopCheckPayments.Date"] = requestObj.Date;
                    details["i18n.transfers.lblNumberOfRecurrencescolon"] = requestObj.numberOfRecurrences;
                }
            }
            details["i18n.PayAPerson.Note"] = requestObj.transactionsNotes;
            details["i18n.PayPerson.defaultAccountForSendingMoney"] = requestObj.From;
            details["i18n.PayPerson.frequency"] = requestObj.frequencyType;
            this.view.keyValueList.setData(details);
            if (requestObj.context === "sendMoneyToNewRecipient") {
                requestObj.payPersonObject.primaryContactForSending = requestObj.p2pContact;
                requestObj.payPersonObject.transactionId = status.referenceId;
                if (requestObj.personId === undefined || requestObj.personId === null || requestObj.personId === "") {
                    this.view.btnSaveRecipient.onClick = function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.createP2PPayee(requestObj.payPersonObject);
                    };
                    this.view.btnSaveRecipient.setVisibility(true);
                }
            } else {
                this.view.btnSaveRecipient.setVisibility(false);
                this.view.btnSaveRecipient.onClick = function() {};
            }
        },

        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };
            this.view.btnViewSent.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSentTransactionsView();
            };
            this.view.btnTransferMore.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showPayAPerson();
            };
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        }
    }
});