define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.showPayAPersonActivity) {
                this.showPayAPersonViewActivity(viewPropertiesMap.showPayAPersonActivity, viewPropertiesMap.personData);
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Sets the data for View Activity Segment.
         * @param {object} selectedData - contains data like
         */
        showPayAPersonViewActivity: function(selectedData, personData) {
            this.view.lblName.text = personData["lblName"] ? personData["lblName"] : "";
            this.view.lblEmail.text = personData["email"] ? personData["email"] : "";
            this.view.lblPhoneno.text = personData["phone"] ? personData["phone"] : "";
            var dataMap = {
                "lblFrom": "lblFrom",
                "lblDate": "lblDate",
                "lblAmount": "lblAmount",
                "lblStatus": "lblStatus",
                "lblRunningStatus": "lblRunningStatus",
                "lblAmountHeader": "lblAmountHeader",
                "lblFromHeader": "lblFromHeader",
                "amountTransferredTillNow": "amountTransferredTillNow"
            };
            var data = selectedData.map(function(payee) {
                return {
                    "lblFrom": payee.fromAccountName,
                    "lblFromHeader": "From:",
                    "lblAmountHeader": "Running Balance: ",
                    "lblDate": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.getFormattedDateString(payee.transactionDate),
                    "lblAmount": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency(payee.amount),
                    "lblStatus": payee.statusDescription,
                    "lblRunningStatus": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency(payee.fromAccountBalance),
                    "amountTransferredTillNow": payee.amountTransferedTillNow,
                    "template": "flxSortP2PActivity"
                }
            });
            this.view.lblAmount.text = data[0] ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(data[0]["amountTransferredTillNow"]) : kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(0);
            this.view.segActivityData.widgetDataMap = dataMap;
            if (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) {
                this.view.imgSortFromAccount.setVisibility(false);
                for (i = 0; i < data.length; i++) {
                    data[i].template = "flxP2PActivityMobile";
                }
            } else {
                this.view.imgSortFromAccount.setVisibility(true);
            }
            this.view.segActivityData.setData(data);
        },

        /**
         * Contorls secondary action dropdown
         */
        showSecondaryActions: function() {
            this.view.imgDropdown.src = (this.view.imgDropdown.src === ViewConstants.IMAGES.ARROW_DOWN) ? ViewConstants.IMAGES.CHEVRON_UP : ViewConstants.IMAGES.ARROW_DOWN;
            this.view.secondaryActions.setVisibility(!this.view.secondaryActions.isVisible);
        },

        /**
         * Set up navigation on clicks for
         */
        secondaryActionsNavigation: function() {
            let index = this.view.secondaryActions.segAccountTypes.selectedRowIndex[1]
            if (index === 1)
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.loadP2PSettingsScreen();
            else
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showDeactivateP2P();
        },


        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };
            this.view.secondaryActions.segAccountTypes.onRowClick = this.secondaryActionsNavigation;
            this.view.btnAddRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showPayAPerson("AddRecipient");
            }
            this.view.btnSendMoneyNewRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showPayAPerson("sendMoneyToNewRecipient");
            }

            this.view.flxWhatElse.onClick = this.showSecondaryActions;
            this.view.btnBackToManageRecipients.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showManageRecipientsView();
            }
            this.view.btnAddRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showAddRecipientView();
            }
            this.view.btnSendMoneyNewRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyToNewRecipientView();
            }
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        }
    }
});