define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        resetForm: true,

        /**
         * Form onNavigate event
         * @param {Object} context 
         */
        onNavigate: function(context, isBack) {
            if (context && context.reset === false)
                this.resetForm = false;
        },

        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.paymentAccounts) {
                if (viewPropertiesMap.sendPaymentAccounts) {
                    this.setSendPaymentAccounts(viewPropertiesMap.sendPaymentAccounts, viewPropertiesMap.accountCurrency);
                }
            }
            if (viewPropertiesMap.sendMoneyData) {
                this.onSendMoneyBtnClick(viewPropertiesMap.sendMoneyData);
            }
            if (viewPropertiesMap.TnCcontentTransfer) {
                this.sendMoneyToPerson(viewPropertiesMap.PayeeObj, viewPropertiesMap.TnCcontentTransfer);
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * This method is used to show the confirmation screen for pay a person send money.
         * @param {Object} payeeObj - This contains the pay a person info like name, nickname, email, phone,primary contact for sending and transaction object fields like from account number and amount.
         */
        sendMoneyToPerson: function(PayeeObj, TnCcontentTransfer) {
            var self = this;
            var result = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.validatePayAPersonAmount(parseFloat(this.view.tbxAmount.text));
            var resultDate = this.validateBillPayDate();
            if (!result.isAmountValid) {
                this.view.lblErrorAmount.text = result.errMsg;
                this.view.lblErrorAmount.setVisibility(true);
                return;
            } else if (this.view.lbxFrequency.selectedKey !== 'Once' && this.view.lbxRecurrences.selectedKey !== 'NO_OF_RECURRENCES' && !resultDate.isDateValid) {
                var message = kony.i18n.getLocalizedString("i18n.transfers.errors.invalidDeliverByDate");
                return;
            }
            var totalPaymentMoney = this.view.tbxAmount.text;
            totalPaymentMoney = self.deformatAmount(totalPaymentMoney.toString());
            if (applicationManager.getConfigurationManager().serviceFeeFlag === "true") {
                var fee = applicationManager.getConfigurationManager().p2pServiceFee;
                extraData = {
                    "Fees": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency(fee),
                    "Total": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency((Number(totalPaymentMoney) + Number(fee)))
                };
                totalPaymentMoney = Number(Number(totalPaymentMoney) + Number(fee));
            }
            var validateDateRange = function() {
                var dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
                var targetDate = self.view.clndrSendOn.formattedDate;
                var today = kony.os.date(dateFormat);
                var todayDateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(today, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase())
                var targetDateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(targetDate, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase())
                if (todayDateObj < targetDateObj) {
                    return "1";
                }
                return "0";
            };
            var sendMoneyPaymentJSON = {
                "From": self.view.lbxFromAccount.selectedKeyValue[1],
                "To": self.view.lblPayee.text,
                "Amount": self.view.tbxAmount.text,
                "Date": self.view.clndrSendOn.formattedDate,
                "Note": self.view.tbxNote.text,
                "fromAccountNumber": self.view.lbxFromAccount.selectedKeyValue[0],
                "frequencyType": self.view.lbxFrequency.selectedKeyValue[0],
                "p2pContact": self.view.lbxSendVia.selectedKeyValue[1],
                "personId": PayeeObj.PayPersonId ? PayeeObj.PayPersonId : PayeeObj.personId,
                "amount": totalPaymentMoney,
                "scheduledDate": self.view.clndrStartDate.formattedDate,
                "transactionsNotes": self.view.tbxNote.text,
                "numberOfRecurrences": self.view.tbxNoOfRecurrences.text,
                "frequencyStartDate": self.view.clndrStartDate.formattedDate,
                "frequencyEndDate": self.view.clndrEndDate.formattedDate,
                "hasHowLong": self.view.lbxRecurrences.selectedKey,
                "context": PayeeObj.context ? PayeeObj.context : "",
                "isScheduled": validateDateRange(),
                "email": PayeeObj.email,
                "secondaryEmail": PayeeObj.secondaryEmail,
                "phone": PayeeObj.phone,
                "secondaryPhoneNumber": PayeeObj.secondaryPhone,
                "payPersonPhone": PayeeObj.payPersonPhone,
                "payPersonEmail": PayeeObj.payPersonEmail,
                "payPersonName": PayeeObj.payPersonName,
                "name": PayeeObj.name,
                "nickName": PayeeObj.nickName,
                "transactionCurrency": self.returnCurrencyCode(self.view.lblCurrency.text)
            };
            if (PayeeObj.statusDescription !== null && PayeeObj.statusDescription !== "Successful") {
                sendMoneyPaymentJSON.transactionId = PayeeObj.transactionId;
            }
            if (PayeeObj.context !== "") {
                sendMoneyPaymentJSON.payPersonObject = self.sendPayeeObj;
            }

            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showP2PTransferReview(sendMoneyPaymentJSON, TnCcontentTransfer);

        },

        returnCurrencyCode: function(currencySymbol) {
            return applicationManager.getFormatUtilManager().getCurrencySymbolCode(currencySymbol);
        },

        /**
         * used to check Date validations for OneTime Payment.
         */
        validateBillPayDate: function() {
            var resultDate = {
                isDateValid: false
            };
            var sendOnDate = this.getDateObj(this.view.clndrStartDate.dateComponents);
            var deliverByDate = this.getDateObj(this.view.clndrStartDate.dateComponents);
            if (sendOnDate.getTime() < deliverByDate.getTime()) {
                resultDate.isDateValid = true;
            }
            return resultDate;
        },
        getDateObj: function(dateComponents) {
            var date = new Date();
            date.setDate(dateComponents[0]);
            date.setMonth(parseInt(dateComponents[1]) - 1);
            date.setFullYear(dateComponents[2]);
            date.setHours(0, 0, 0, 0)
            return date;
        },

        /**
         * used to get the amount
         * @param {number} amount amount
         * @returns {number} amount
         */
        deformatAmount: function(amount) {
            return applicationManager.getFormatUtilManager().deFormatAmount(amount);
        },

        /**
         * Validates and enables/disables send money button
         */
        checkValidityP2PTransferForm: function() {
            var scopeObj = this;
            var re = new RegExp("^([0-9])+(\.[0-9]{1,2})?$");
            var disableCreateP2PButton = function() {
                FormControllerUtility.disableButton(scopeObj.view.btnSendMoney);
            };
            var result = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.validatePayAPersonAmount(parseFloat(scopeObj.view.tbxAmount.text));
            if (!result.isAmountValid) {
                this.view.tbxAmount.text = result.errMsg;
                this.view.tbxAmount.setVisibility(true);
            }
            if (this.view.tbxAmount.text === null || this.view.tbxAmount.text === "" || isNaN(scopeObj.deformatAmount(this.view.tbxAmount.text)) || !re.test(scopeObj.deformatAmount(this.view.tbxAmount.text)) || (parseFloat(scopeObj.deformatAmount(this.view.tbxAmount.text)) <= 0)) {
                disableCreateP2PButton();
                return;
            }
            if (this.view.lbxFrequency.selectedKey !== "Once" && this.view.lbxRecurrences.selectedKey === "NO_OF_RECURRENCES" && this.view.tbxNoOfRecurrences.text === "") {
                disableCreateP2PButton();
                return;
            }
            if (isNaN(this.view.tbxNoOfRecurrences.text) || parseInt(this.view.tbxNoOfRecurrences.text) <= 0) {
                disableCreateP2PButton();
                return;
            }
            FormControllerUtility.enableButton(this.view.btnSendMoney);
        },

        /**
         * This method is used to show the send money in the pay a person screen.
         * @param {Object} dataItem - contains the data for pay a person like name, email, phone, primary contact for sending and transaction object fields like amount, from account number, frequency, recurrence.
         */
        onSendMoneyBtnClick: function(dataItem) {
            this.view.lblPayee.text = dataItem.payPersonName ? dataItem.payPersonName : dataItem.name;
            var emailMasterDrop = [];
            var sendRecep = this.checkRequriedValues(dataItem);
            for (var i = 0; i < sendRecep.length; i++) {
                if (sendRecep[i] !== null) {
                    var data = [];
                    data.push(sendRecep[i]);
                    data.push(sendRecep[i])
                    if (sendRecep[i] !== undefined && sendRecep[i] !== "") {
                        emailMasterDrop.push(data);
                    }
                }
            }
            this.view.lbxSendVia.masterData = emailMasterDrop;
            if (dataItem.p2pContact) {
                this.view.lbxSendVia.selectedKey = dataItem.p2pContact ? dataItem.p2pContact : this.view.lbxSendVia.masterData[0][0];
            }
            if (dataItem.lblPrimaryContact !== "" || dataItem.lblPrimaryContact !== null || dataItem.lblPrimaryContact !== undefined) {
                this.view.lbxSendVia.selectedKey = dataItem.lblPrimaryContact ? dataItem.lblPrimaryContact : this.view.lbxSendVia.masterData[0][0];
            }
            if (dataItem.amount) {
                this.view.tbxAmount.text = dataItem.amount;
                commonUtilities.enableButton(this.view.btnSendMoney);
            }
            this.view.btnSendMoney.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.getTnCP2PTransfer(dataItem);
            };
        },

        checkRequriedValues: function(dataItem) {
            return [dataItem.email, dataItem.secondaryEmail2, dataItem.phone, dataItem.secondaryPhone, dataItem.secondaryPhoneNumber2, dataItem.payPersonPhone, dataItem.secondaryPhoneNumber, dataItem.secondoryPhoneNumber, dataItem.payPersonEmail, dataItem.secondaryEmail];
        },

        /**
         * This method is used to set the masterdata for from accounts in send money screen in pay a person.
         * @param {Object} accounts - contains the list of accounts.
         */
        setSendPaymentAccounts: function(accounts, accountsWithCurrency) {
            var scopeObj = this;
            this.view.lbxFromAccount.masterData = accounts;
            this.view.lbxFromAccount.selectedKey = this.view.lbxFromAccount.masterData[0][0];
            this.setFromAccountCurrency(accountsWithCurrency);
            this.view.lbxFromAccount.onSelection = function() {
                scopeObj.setFromAccountCurrency(accountsWithCurrency);
            };
        },

        setFromAccountCurrency: function(accountsWithCurrency) {
            var fromAccountCurrencyCode = this.getFromAccount(accountsWithCurrency, this.view.lbxFromAccount.selectedKey);
            this.view.lblCurrency.text = applicationManager.getFormatUtilManager().getCurrencySymbol(fromAccountCurrencyCode);
        },

        getFromAccount: function(data, value) {
            if (!value)
                return data[0][1];
            data = data.filter(function(item) {
                return item[0] === value
            });
            return data[0][1];
        },

        /**
         * Contorls secondary action dropdown
         */
        showSecondaryActions: function() {
            this.view.imgDropdown.src = (this.view.imgDropdown.src === ViewConstants.IMAGES.ARROW_DOWN) ? ViewConstants.IMAGES.CHEVRON_UP : ViewConstants.IMAGES.ARROW_DOWN;
            this.view.secondaryActions.setVisibility(!this.view.secondaryActions.isVisible);
        },

        /**
         * Set up navigation on clicks for
         */
        secondaryActionsNavigation: function() {
            let index = this.view.secondaryActions.segAccountTypes.selectedRowIndex[1]
            if (index === 1)
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.loadP2PSettingsScreen();
            else
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showDeactivateP2P();
        },

        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };

            this.view.flxWhatElse.onClick = this.showSecondaryActions;
            this.view.secondaryActions.segAccountTypes.onRowClick = this.secondaryActionsNavigation;
            this.view.btnSentTab.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSentTransactionsView();
            };
            this.view.btnManageRecipientsTab.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showManageRecipientsView();
            };
            this.view.btnSendMoneyTab.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyTabView();
            };
            this.view.lbxFrequency.onSelection = this.onFrequencyChanged;
            this.view.lbxRecurrences.onSelection = this.onHowLongChange;
            this.view.tbxAmount.onKeyDown = commonUtilities.validateAmountFieldKeyPress.bind(this, this.view.tbxAmount);
            this.view.tbxAmount.onKeyUp = this.checkValidityP2PTransferForm;
            this.view.tbxNoOfRecurrences.onKeyUp = this.checkValidityP2PTransferForm;
            this.view.tbxAmount.onBeginEditing = commonUtilities.removeDelimitersForAmount.bind(this, this.view.tbxAmount);
            this.view.tbxAmount.onEndEditing = commonUtilities.validateAndFormatAmount.bind(this, this.view.tbxAmount);
            this.view.btnAddRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showAddRecipientView();
            }
            this.view.btnSendMoneyNewRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyToNewRecipientView();
            }
            this.view.btnCancel.onClick = function() {
                new kony.mvc.Navigation(kony.application.getPreviousForm().id).navigate();
            }
        },

        /**
         * This method is used as an onclick for the frequency change in the send money for pay a person.
         */
        onHowLongChange: function() {
            let value = this.view.lbxRecurrences.selectedKey;
            if (value === "ON_SPECIFIC_DATE") {
                this.view.flxContainer2.setVisibility(false);
                this.view.flxContainer3.setVisibility(true);
                this.view.lblForHowLong.setVisibility(true);
                this.view.lbxRecurrences.setVisibility(true);
            } else if (value === "NO_OF_RECURRENCES") {
                this.view.flxContainer2.setVisibility(true);
                this.view.flxContainer3.setVisibility(false);
                this.view.lblNoOfRecurrences.setVisibility(true);
                this.view.tbxNoOfRecurrences.setVisibility(true);
                this.view.lblForHowLong.setVisibility(true);
                this.view.lbxRecurrences.setVisibility(true);
            }
            this.checkValidityP2PTransferForm();
        },

        /**
         * This method is used as an onclick for the frequency change.
         */
        onFrequencyChanged: function() {
            let frequencyValue = this.view.lbxFrequency.selectedKey;
            let howLongValue = this.view.lbxRecurrences.selectedKey;
            if (frequencyValue !== "Once" && howLongValue !== 'NO_OF_RECURRENCES') {
                this.view.flxContainer2.setVisibility(false);
                this.view.flxContainer3.setVisibility(true);
                this.view.lblForHowLong.setVisibility(true);
                this.view.lbxRecurrences.setVisibility(true);
            } else if (frequencyValue !== "Once" && howLongValue === 'NO_OF_RECURRENCES') {
                this.view.flxContainer2.setVisibility(true);
                this.view.flxContainer3.setVisibility(false);
                this.view.lblNoOfRecurrences.setVisibility(true);
                this.view.tbxNoOfRecurrences.setVisibility(true);
                this.view.lblForHowLong.setVisibility(true);
                this.view.lbxRecurrences.setVisibility(true);
            } else {
                this.view.lblForHowLong.setVisibility(false);
                this.view.lbxRecurrences.setVisibility(false);
                this.view.flxContainer3.setVisibility(false);
                this.view.flxContainer2.setVisibility(true);
                this.view.lblNoOfRecurrences.setVisibility(false);
                this.view.tbxNoOfRecurrences.setVisibility(false);
            }
            this.checkValidityP2PTransferForm();
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            if (this.resetForm)
                this.resetUI();
        },

        /**
         * Resets the UI
         */
        resetUI: function() {
            this.view.lbxFrequency.masterData = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.listboxFrequencies();
            this.view.lbxFrequency.selectedKey = this.view.lbxFrequency.masterData[0][0];
            this.view.lbxRecurrences.masterData = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.listboxForHowLong();
            this.view.lbxRecurrences.selectedKey = this.view.lbxRecurrences.masterData[0][0];
            var dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
            this.view.clndrSendOn.dateFormat = dateFormat;
            var date = new Date();
            this.view.clndrSendOn.dateComponents = [date.getDate(), date.getMonth() + 1, date.getFullYear()];
            this.view.tbxAmount.text = "";
            this.view.tbxNoOfRecurrences.text = "";
            this.view.tbxNote.text = "";
            commonUtilities.disableButton(this.view.btnSendMoney);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        }
    }
});