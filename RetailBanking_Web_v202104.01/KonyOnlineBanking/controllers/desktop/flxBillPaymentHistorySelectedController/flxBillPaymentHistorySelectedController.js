define({
    showUnselectedRow: function() {
        var rowIndex = kony.application.getCurrentForm().segmentBillpay.selectedRowIndex[1];
        var data = kony.application.getCurrentForm().segmentBillpay.data;
        var pre_val;
        var required_values = [];
        var array_close = ["O", false, "sknFlxIdentifier", "sknffffff15pxolbfonticons", "50dp", "sknflxffffffnoborder"];
        var array_open = ["P", true, "sknflx4176a4", "sknLbl4a90e215px", "145dp", "sknFlxf7f7f7"];
        if (previous_index_history === rowIndex) {
            data[rowIndex].lblDropdown == "P" ? required_values = array_close : required_values = array_open;
            this.toggle(rowIndex, required_values);
        } else {
            if (previous_index_history >= 0) {
                pre_val = previous_index_history;
                this.toggle(pre_val, array_close);
            }
            pre_val = rowIndex;
            this.toggle(rowIndex, array_open);
        }
        previous_index_history = rowIndex;
    },
    toggle: function(index, array) {
        var data = kony.application.getCurrentForm().segmentBillpay.data;
        data[index].lblDropdown = array[0];
        data[index].flxIdentifier.isVisible = array[1];
        data[index].flxIdentifier.skin = array[2];
        data[index].lblIdentifier.skin = array[3];
        data[index].flxBillPaymentHistorySelected.height = array[4];
        data[index].flxBillPaymentHistorySelected.skin = array[5];
        kony.application.getCurrentForm().segmentBillpay.setDataAt(data[index], index);
    },

});