define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onEndEditing defined for tbxPerTransDenialAmount **/
    AS_TextField_a309afab36d340afba7905e3c2689a54: function AS_TextField_a309afab36d340afba7905e3c2689a54(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onEndEditing defined for tbxDailyTransDenialAmount **/
    AS_TextField_a3be96ac45ee4d82b28c6f8e1f791d00: function AS_TextField_a3be96ac45ee4d82b28c6f8e1f791d00(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onKeyUp defined for tbxWeeklyTransApproveAmount **/
    AS_TextField_b1a28627da1647fba11e05f91c353aaa: function AS_TextField_b1a28627da1647fba11e05f91c353aaa(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxWeeklyTransDenialAmount **/
    AS_TextField_b9ac2d3433864a8581db2b8f3c72d780: function AS_TextField_b9ac2d3433864a8581db2b8f3c72d780(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onTextChange defined for tbxDailyTransApproveAmount **/
    AS_TextField_c8be12af7ae34eea81239042cab3a286: function AS_TextField_c8be12af7ae34eea81239042cab3a286(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onKeyUp defined for tbxPerTransApproveAmount **/
    AS_TextField_e36f13966ab54713936fffaca3045a91: function AS_TextField_e36f13966ab54713936fffaca3045a91(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxPerTransApproveAmount **/
    AS_TextField_e6d19b89e3bd468f952075f9ab154aa1: function AS_TextField_e6d19b89e3bd468f952075f9ab154aa1(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onKeyUp defined for tbxDailyTransDenialAmount **/
    AS_TextField_f6d311e77ca24a54bfbed81024f74d8e: function AS_TextField_f6d311e77ca24a54bfbed81024f74d8e(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxDailyTransApproveAmount **/
    AS_TextField_h3805f148e684885889823bcdde21634: function AS_TextField_h3805f148e684885889823bcdde21634(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onKeyUp defined for tbxDailyTransApproveAmount **/
    AS_TextField_ha3c9e1b8921430384f6a74515a22b90: function AS_TextField_ha3c9e1b8921430384f6a74515a22b90(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxWeeklyTransApproveAmount **/
    AS_TextField_hf834ef75cc947cd8f8c8658335a71af: function AS_TextField_hf834ef75cc947cd8f8c8658335a71af(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxWeeklyTransDenialAmount **/
    AS_TextField_ib730a456f4e44e7a1782b81d86d6d56: function AS_TextField_ib730a456f4e44e7a1782b81d86d6d56(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxWeeklyTransApproveAmount **/
    AS_TextField_j4fd307a8a5a471f9e5ba25769514fd0: function AS_TextField_j4fd307a8a5a471f9e5ba25769514fd0(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onTextChange defined for tbxPerTransApproveAmount **/
    AS_TextField_j9bc295d80f045439b6c563d4ecca266: function AS_TextField_j9bc295d80f045439b6c563d4ecca266(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onKeyUp defined for tbxWeeklyTransDenialAmount **/
    AS_TextField_jcde2ceb0c694116a1a9aefeaf0f9058: function AS_TextField_jcde2ceb0c694116a1a9aefeaf0f9058(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    }
});