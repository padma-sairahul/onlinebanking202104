define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEbill **/
    AS_Button_a646dfa7713f408b81a43c2d3380ecac: function AS_Button_a646dfa7713f408b81a43c2d3380ecac(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnActivateEbill **/
    AS_Button_e86312ab98eb47448e6529778636f6fa: function AS_Button_e86312ab98eb47448e6529778636f6fa(eventobject, context) {
        var self = this;
        this.executeOnParent("activateEBill");
    },
    /** onClick defined for btnViewEBill **/
    AS_Button_g9b1648fcec946958edfabcb3909f957: function AS_Button_g9b1648fcec946958edfabcb3909f957(eventobject, context) {
        var self = this;
        //test
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_f9ca08f022b6465fad2765cae606432f: function AS_FlexContainer_f9ca08f022b6465fad2765cae606432f(eventobject, context) {
        var self = this;
        this.segmentAllPayeesRowClick();
    }
});