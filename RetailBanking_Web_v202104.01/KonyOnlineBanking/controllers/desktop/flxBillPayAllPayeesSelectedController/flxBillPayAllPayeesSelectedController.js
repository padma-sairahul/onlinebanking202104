define(['FormControllerUtility'], function(FormControllerUtility) {
    return {
        segmentAllPayeesRowClick: function() {
            var currForm = kony.application.getCurrentForm();
            var index = currForm.tableView.segmentBillpay.selectedRowIndex[1];
            var data = currForm.tableView.segmentBillpay.data;
            data[index].imgDropdown = "arrow_down.png";
            data[index].imgErrorSelected = "error_yellow.png";
            data[index].lblError = "Verify payment amount and available account balance";
            if (kony.application.getCurrentBreakpoint() == 640) {
                data[index].template = "flxBillPayAllPayeesMobile";
            } else {
                data[index].template = "flxBillPayAllPayees";
            }
            currForm.tableView.segmentBillpay.setDataAt(data[index], index);
            this.AdjustScreen(-321);
            /*currForm.flxCustomListBoxContainerPayFrom.isVisible=false;
            currForm.flxCustomListBoxContainerCategory.isVisible=false;*/
        },
        viewEBill: function() {
            var currForm = kony.application.getCurrentForm();
            FormControllerUtility.updateWidgetsHeightInInfo(currForm, ['flxContainer']);
            var height_to_set = 140 + currForm.flxContainer.info.frame.height;
            currForm.flxViewEbill.height = height_to_set + "dp";
            currForm.flxViewEbill.isVisible = true;
            this.AdjustScreen(30);
            currForm.forceLayout();
        },
        //UI Code
        AdjustScreen: function(data) {
            var currForm = kony.application.getCurrentForm();
            if (data !== undefined) {} else {
                data = 0;
            }
            FormControllerUtility.updateWidgetsHeightInInfo(currForm, ['flxFooter']);
            var footerTop = currForm.flxFooter.info.frame.y;
            footerTop += data;
            currForm.flxFooter.top = footerTop + "dp";
            currForm.forceLayout();
            return;
        }
    };
});