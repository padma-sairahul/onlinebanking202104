define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnUseCVV **/
    AS_Button_b014ab53d3c6421cb0d39f83ae5e147f: function AS_Button_b014ab53d3c6421cb0d39f83ae5e147f(eventobject) {
        var self = this;
        this.useCVVForReset();
    },
    /** onClick defined for btnOnlineAccessEnroll **/
    AS_Button_b1c783f693db4c88b35b960d93ccc013: function AS_Button_b1c783f693db4c88b35b960d93ccc013(eventobject) {
        var self = this;
        //this.verifyUser();
        var context = {
            "action": "NavigateToEnroll"
        };
        var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollModule.presentationController.showEnrollPage(context);
    },
    /** onClick defined for btnUseCVV **/
    AS_Button_bdb7d79246ab43e38d477d71090512cd: function AS_Button_bdb7d79246ab43e38d477d71090512cd(eventobject) {
        var self = this;
        this.useCVVForReset();
    },
    /** onClick defined for btnProceed **/
    AS_Button_c3f3b614ef974d01822ba57f93ce1714: function AS_Button_c3f3b614ef974d01822ba57f93ce1714(eventobject) {
        var self = this;
        this.letsGetStarted();
    },
    /** onClick defined for btnUseOTP **/
    AS_Button_da99080919b041c4ac365b2690a28094: function AS_Button_da99080919b041c4ac365b2690a28094(eventobject) {
        var self = this;
        this.useOTPForReset();
    },
    /** onClick defined for btnLogin **/
    AS_Button_de4e981b9fec4fa78dae2465790df83f: function AS_Button_de4e981b9fec4fa78dae2465790df83f(eventobject) {
        var self = this;
        this.onLoginClick();
    },
    /** onClick defined for btnProceed **/
    AS_Button_df12693ac5b44c3b9864b1091128b959: function AS_Button_df12693ac5b44c3b9864b1091128b959(eventobject) {
        var self = this;
        this.verifyUserDetails();
    },
    /** onClick defined for btnEnroll **/
    AS_Button_e5560a494d924458bc9240c27758a826: function AS_Button_e5560a494d924458bc9240c27758a826(eventobject) {
        var self = this;
        this.loginLater(this.view.flxEnrollOrServerError);
    },
    /** onClick defined for btnNext **/
    AS_Button_e674540514d44d46a690c2ba43e3d1bf: function AS_Button_e674540514d44d46a690c2ba43e3d1bf(eventobject) {
        var self = this;
        this.isOTPCorrect();
    },
    /** onClick defined for btnProceed **/
    AS_Button_ea17702b24d74d10b184d18ad2c0577a: function AS_Button_ea17702b24d74d10b184d18ad2c0577a(eventobject) {
        var self = this;
        this.loginLater(this.view.flxResetSuccessful);
    },
    /** onClick defined for btnForgotPassword **/
    AS_Button_ead9d66606d2401ea0d5e7bb9943bf25: function AS_Button_ead9d66606d2401ea0d5e7bb9943bf25(eventobject) {
        var self = this;
        this.verifyUser();
    },
    /** onClick defined for btnNext **/
    AS_Button_ec982f6c3e4d43dda32d288ea7cb89ea: function AS_Button_ec982f6c3e4d43dda32d288ea7cb89ea(eventobject) {
        var self = this;
        this.isCVVCorrect();
    },
    /** onClick defined for btnYes **/
    AS_Button_f30ad6790c514d8b9e3a3b6642ab8841: function AS_Button_f30ad6790c514d8b9e3a3b6642ab8841(eventobject) {
        var self = this;
        this.btnYesTakeSurvey();
    },
    /** onClick defined for btnNext **/
    AS_Button_g8926370565949d99200dc018568c10c: function AS_Button_g8926370565949d99200dc018568c10c(eventobject) {
        var self = this;
        this.showResetConfirmationPage();
    },
    /** onClick defined for btnBackToLogin **/
    AS_Button_hb4e02e809db4e1bae0ba4b9556b99ea: function AS_Button_hb4e02e809db4e1bae0ba4b9556b99ea(eventobject) {
        var self = this;
        this.loginLater(this.view.flxEnrollOrServerError);
    },
    /** onClick defined for btnLoginLater **/
    AS_Button_ie4688df9bed49909277fa74cc9a8cce: function AS_Button_ie4688df9bed49909277fa74cc9a8cce(eventobject) {
        var self = this;
        this.loginLater(this.view.flxResetSuccessful);
    },
    /** onClick defined for btnNo **/
    AS_Button_ja24bde46bf44018b58917cff76e85aa: function AS_Button_ja24bde46bf44018b58917cff76e85aa(eventobject) {
        var self = this;
        this.btnNoTakeSurvey();
    },
    /** onClick defined for btnOpenNewAccount **/
    AS_Button_jd7ba24236124efb8537a66ac4a97242: function AS_Button_jd7ba24236124efb8537a66ac4a97242(eventobject) {
        var self = this;
        this.navigateToNewUserOnBoarding();
    },
    /** onClick defined for btnNext **/
    AS_Button_je1a71e9e3a441829c006de52cf56f16: function AS_Button_je1a71e9e3a441829c006de52cf56f16(eventobject) {
        var self = this;
        this.requestOTPValue();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_f40184f7a43448b0ab98aa871fa5f37b: function AS_FlexContainer_f40184f7a43448b0ab98aa871fa5f37b(eventobject) {
        var self = this;
        this.onFeedbackCrossClick();
    },
    /** preShow defined for frmLogin **/
    AS_Form_a00d69b6a6fe4e8f982b0de8159a5f3b: function AS_Form_a00d69b6a6fe4e8f982b0de8159a5f3b(eventobject) {
        var self = this;
        this.onPreShow();
    },
    /** onDeviceBack defined for frmLogin **/
    AS_Form_c876da882d124a0aa022ca5dc35ec6d6: function AS_Form_c876da882d124a0aa022ca5dc35ec6d6(eventobject) {
        var self = this;
        kony.print("User pressed back button");
    },
    /** onBreakpointChange defined for frmLogin **/
    AS_Form_c9b1074774c146c49c05e5e3e32857c9: function AS_Form_c9b1074774c146c49c05e5e3e32857c9(eventobject, breakpoint) {
        var self = this;
        return self.onBreakpointChange.call(this, breakpoint);
    },
    /** postShow defined for frmLogin **/
    AS_Form_df5e80bd9a794da2b4e45afaa435f2e4: function AS_Form_df5e80bd9a794da2b4e45afaa435f2e4(eventobject) {
        var self = this;
        this.onPostShow();
        applicationManager.getTypeManager().initialiseAccountTypeManager();
        applicationManager.getTypeManager().initialiseTransactionTypeManager();
        applicationManager.getTypeManager().initialisePfmTypeManager();
    },
    /** init defined for frmLogin **/
    AS_Form_g3cb493f4556464f917fe3924464890b: function AS_Form_g3cb493f4556464f917fe3924464890b(eventobject) {
        var self = this;
        this.frmLoginInit();
    },
    /** onTouchEnd defined for frmLogin **/
    AS_Form_he4ff28aa4df4130a957effb7bd85ce0: function AS_Form_he4ff28aa4df4130a957effb7bd85ce0(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onTouchStart defined for imgViewCVV **/
    AS_Image_a77ff3d5f598477d96ba8a7a74a917ea: function AS_Image_a77ff3d5f598477d96ba8a7a74a917ea(eventobject, x, y) {
        var self = this;
        this.showOTP();
    },
    /** onTouchStart defined for lblHowToEnroll **/
    AS_Label_hbeccaed576f4fe2b9a931795bd748aa: function AS_Label_hbeccaed576f4fe2b9a931795bd748aa(eventobject, x, y) {
        var self = this;
        this.returnToLogin();
    },
    /** onTouchStart defined for lstbxCards **/
    AS_ListBox_c790c4d06bea4977bc373f4d4d1d535c: function AS_ListBox_c790c4d06bea4977bc373f4d4d1d535c(eventobject, x, y) {
        var self = this;
        // this.presenter.showCVVCards(this);
    },
    /** onSelection defined for lbxMonth **/
    AS_ListBox_f02eb01ff527451f94db938c8addb1e6: function AS_ListBox_f02eb01ff527451f94db938c8addb1e6(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onSelection defined for lbxDate **/
    AS_ListBox_fbceb21f4ddd4de1a9a20791e2251754: function AS_ListBox_fbceb21f4ddd4de1a9a20791e2251754(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onSelection defined for lbxYear **/
    AS_ListBox_gcfe9c71f0534d908d98a6f965b3fd53: function AS_ListBox_gcfe9c71f0534d908d98a6f965b3fd53(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onRowClick defined for segLanguagesList **/
    AS_Segment_a7f920ba00b74a61829eea1648992834: function AS_Segment_a7f920ba00b74a61829eea1648992834(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.selectYourLanguage();
    },
    /** onRowClick defined for segUsers **/
    AS_Segment_e71ec59a0219453f9933531a29981d7c: function AS_Segment_e71ec59a0219453f9933531a29981d7c(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.selectUserName();
    },
    /** onKeyUp defined for tbxLastName **/
    AS_TextField_ab492a75ee104b79bcef227862bd1dd1: function AS_TextField_ab492a75ee104b79bcef227862bd1dd1(eventobject, changedtext) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onEndEditing defined for tbxUserName **/
    AS_TextField_ba5381fb58bc4e69a7e5366199a2aecb: function AS_TextField_ba5381fb58bc4e69a7e5366199a2aecb(eventobject, changedtext) {
        var self = this;
        this.hideUserNames();
        this.setNormalSkin(this.view.main.flxUserName);
        this.view.main.lblUsernameCapsLocIndicator.setVisibility(false);
    },
    /** onDone defined for tbxPassword **/
    AS_TextField_bcdc10ae8b1a403c82fa05d79cf6b6df: function AS_TextField_bcdc10ae8b1a403c82fa05d79cf6b6df(eventobject, changedtext) {
        var self = this;
        this.toEnableOrDisableLogin();
    },
    /** onBeginEditing defined for tbxCVV **/
    AS_TextField_c2d9de5f0b544570a165739090cb744c: function AS_TextField_c2d9de5f0b544570a165739090cb744c(eventobject, changedtext) {
        var self = this;
        this.reEnterCVV();
    },
    /** onKeyUp defined for tbxUserName **/
    AS_TextField_cfa0d44f49e44ed09ae7f7c35af168f4: function AS_TextField_cfa0d44f49e44ed09ae7f7c35af168f4(eventobject) {
        var self = this;
        this.checkifUserNameContainsMaskCharacter();
        this.capsLockIndicatorForUserName();
        /*var orientationHandler = new OrientationHandler();
        if (kony.application.getCurrentBreakpoint() > 1024 && orientationHandler.isDesktop) {
          if (event.getModifierState("CapsLock")) {
            this.view.main.lblUsernameCapsLocIndicator.setVisibility(true);
          } else {
            this.view.main.lblUsernameCapsLocIndicator.setVisibility(false);
          }
          this.view.forceLayout();
        }*/
    },
    /** onKeyUp defined for tbxPassword **/
    AS_TextField_d95ee59d841c471cb206a8bf70db7295: function AS_TextField_d95ee59d841c471cb206a8bf70db7295(eventobject) {
        var self = this;
        this.enableLogin(this.view.main.tbxUserName.text.trim(), this.view.main.tbxPassword.text);
        this.capsLockIndicatorForPassword();
    },
    /** onKeyUp defined for tbxCVV **/
    AS_TextField_e1039fff3fb44a11822e0ea10aa5484e: function AS_TextField_e1039fff3fb44a11822e0ea10aa5484e(eventobject) {
        var self = this;
        this.cvvCheck();
    },
    /** onBeginEditing defined for tbxPassword **/
    AS_TextField_e48f6a4124bf4de286d0c4a912983d38: function AS_TextField_e48f6a4124bf4de286d0c4a912983d38(eventobject, changedtext) {
        var self = this;
        if (this.view.main.flxPassword.skin == "sknBorderFF0101") {
            this.credentialsMissingUIChangesAnti();
        }
    },
    /** onBeginEditing defined for tbxUserName **/
    AS_TextField_ed806356b3314d86b37761258f426fca: function AS_TextField_ed806356b3314d86b37761258f426fca(eventobject, changedtext) {
        var self = this;
        showSuggestion = true;
        this.showUserNamesBasedOnlength()
        this.setFocusSkin(this.view.main.flxUserName);
    },
    /** onKeyUp defined for tbxSSN **/
    AS_TextField_fecb63274ea6496298bc7eb0399a4e65: function AS_TextField_fecb63274ea6496298bc7eb0399a4e65(eventobject, changedtext) {
        var self = this;
        this.allFieldsCheck();
        this.ssnCheck();
    },
    /** onKeyUp defined for tbxCVV **/
    AS_TextField_g6943b93f48a40b4b3253ac5cc4bacc3: function AS_TextField_g6943b93f48a40b4b3253ac5cc4bacc3(eventobject) {
        var self = this;
        this.otpCheck();
    },
    /** onBeginEditing defined for tbxCVV **/
    AS_TextField_j0019f86f656471d85690a0d9c52a0fe: function AS_TextField_j0019f86f656471d85690a0d9c52a0fe(eventobject, changedtext) {
        var self = this;
        this.reTypeOTP();
    },
    /** onTouchStart defined for ResetMyPassword **/
    AS_UWI_c13e044fb512427b8ea7503d5078016c: function AS_UWI_c13e044fb512427b8ea7503d5078016c(eventobject, x, y) {
        var self = this;
        this.goToResetUsingOTP();
    },
    /** onTouchStart defined for SignInAs **/
    AS_UWI_cad98eb1a8304f689ee3b40cfa08351f: function AS_UWI_cad98eb1a8304f689ee3b40cfa08351f(eventobject, x, y) {
        var self = this;
        this.showEnterCVVPage();
    },
    /** onTouchStart defined for AlterneteActionsEnterPIN **/
    AS_UWI_d63f35e0cf744eaab601100605282f5d: function AS_UWI_d63f35e0cf744eaab601100605282f5d(eventobject, x, y) {
        var self = this;
        this.goToResetUsingOTP();
    },
    /** onTouchStart defined for AlterneteActionsEnterCVV **/
    AS_UWI_ed53609c73864a7187559ff06aed44c6: function AS_UWI_ed53609c73864a7187559ff06aed44c6(eventobject, x, y) {
        var self = this;
        this.showEnterCVVPage();
    },
    /** onTouchStart defined for AlterneteActionsSignIn **/
    AS_UWI_febaf38267ab420ea10a7175c9a77888: function AS_UWI_febaf38267ab420ea10a7175c9a77888(eventobject, x, y) {
        var self = this;
        this.loginWithVerifiedUserName();
    },
    /** onTouchStart defined for AlterneteActionsResetPassword **/
    AS_UWI_g49f33dc26c54fb5af2ced556a943d58: function AS_UWI_g49f33dc26c54fb5af2ced556a943d58(eventobject, x, y) {
        var self = this;
        this.goToPasswordResetOptionsPage();
    },
    /** onSuccess defined for loginComponent **/
    AS_UWI_ga36df554a514922a7a9057a860088d9: function AS_UWI_ga36df554a514922a7a9057a860088d9(response) {
        var self = this;
        return self.postLoginSuccess.call(this, response);
    },
    /** errorMFANavigation defined for OTPComponent **/
    AS_UWI_gfd6b1123e3345f8a02c9b99db5e6495: function AS_UWI_gfd6b1123e3345f8a02c9b99db5e6495(response) {
        var self = this;
        return self.MFANavigation.call(this, response);
    }
});