define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for frmLoginLanguage **/
    AS_Form_a778a02a21204ce9b531ebf73c46144e: function AS_Form_a778a02a21204ce9b531ebf73c46144e(eventobject) {
        var self = this;
        //this.onPreShow();
    },
    /** onTouchEnd defined for frmLoginLanguage **/
    AS_Form_i79b7965a6bc45f7aae63f59dd688b08: function AS_Form_i79b7965a6bc45f7aae63f59dd688b08(eventobject, x, y) {
        var self = this;
        //hidePopups();
    },
    /** postShow defined for frmLoginLanguage **/
    AS_Form_ib2c7270983345e9bd9ee39a89b71007: function AS_Form_ib2c7270983345e9bd9ee39a89b71007(eventobject) {
        var self = this;
        var config = applicationManager.getConfigurationManager();
        kony.i18n.setCurrentLocaleAsync(config.configurations.getItem("LOCALE"), function() {
            var previousForm = kony.application.getPreviousForm();
            var currentForm = kony.application.getCurrentForm();
            var OLBLogoutStatus = kony.store.getItem('OLBLogoutStatus');
            //var isUserLoggedin = kony.store.getItem('UserLoginStatus');
            if (previousForm && previousForm.id == "frmProfileManagement") applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
            else if (OLBLogoutStatus && OLBLogoutStatus.isUserLoggedoutSuccessfully || (currentForm && currentForm.id == "frmLogout")) {
                if (OLBLogoutStatus) {
                    // kony.store.setItem('UserLoginStatus',false);
                    OLBLogoutStatus.isUserLoggedoutSuccessfully = false;
                    applicationManager.getStorageManager().setStoredItem('OLBLogoutStatus', OLBLogoutStatus);
                }
                applicationManager.getNavigationManager().navigateTo("frmLogout");
            } else {
                applicationManager.getNavigationManager().navigateTo("frmLogin");
            }
        }, function() {});
    },
    /** onDeviceBack defined for frmLoginLanguage **/
    AS_Form_jce8c301abad400688ca23e71a3aab00: function AS_Form_jce8c301abad400688ca23e71a3aab00(eventobject) {
        var self = this;
        kony.print("User pressed back button");
    }
});