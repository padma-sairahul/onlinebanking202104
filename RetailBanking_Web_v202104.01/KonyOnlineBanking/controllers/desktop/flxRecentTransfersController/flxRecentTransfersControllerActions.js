define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAction **/
    AS_Button_j1c1b04b6be54a3498963baecf89c656: function AS_Button_j1c1b04b6be54a3498963baecf89c656(eventobject, context) {
        var self = this;
        this.executeOnParent("viewTransactionReport");
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_h1ff818820394c29bac0dbc1d66e7dce: function AS_FlexContainer_h1ff818820394c29bac0dbc1d66e7dce(eventobject, context) {
        var self = this;
        //this.pendingRowOnClick();
        this.showSelectedRow();
    }
});