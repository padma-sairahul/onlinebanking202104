define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnDownloadStatements **/
    AS_Button_b19da7cbe7904fb199bf6c5eeeb8c580: function AS_Button_b19da7cbe7904fb199bf6c5eeeb8c580(eventobject) {
        var self = this;
        this.showdownloadpopup();
    },
    /** onClick defined for btnCancel **/
    AS_Button_i8d6b75c37914ddcb6232819aa9f546b: function AS_Button_i8d6b75c37914ddcb6232819aa9f546b(eventobject) {
        var self = this;
        this.view.flxDownladstatementspopup.setVisibility(false);
    },
    /** onClick defined for btnOkay **/
    AS_Button_id7ea32b170e48718da0e97dc3781051: function AS_Button_id7ea32b170e48718da0e97dc3781051(eventobject) {
        var self = this;
        this.navigateToAccountDetails();
    },
    /** onClick defined for btnDownload **/
    AS_Button_je8b67c3fd794f55842fdd4c4dbe7935: function AS_Button_je8b67c3fd794f55842fdd4c4dbe7935(eventobject) {
        var self = this;
        this.view.flxDownladstatementspopup.setVisibility(false);
        //this.view.flxGenerateStatementsPopup.setVisibility(true);
        this.generateCombinedStatement();
    },
    /** postShow defined for frmConsolidatedStatements **/
    AS_Form_accf4eda64bd4f20bc046b4aa90e2167: function AS_Form_accf4eda64bd4f20bc046b4aa90e2167(eventobject) {
        var self = this;
        this.postShowConsolidatedStatements();
    },
    /** init defined for frmConsolidatedStatements **/
    AS_Form_acfdbbf030ba478789ed69e88c9c5647: function AS_Form_acfdbbf030ba478789ed69e88c9c5647(eventobject) {
        var self = this;
        this.init();
    },
    /** onDeviceBack defined for frmConsolidatedStatements **/
    AS_Form_aee0f71f77a448518033de9d76dce140: function AS_Form_aee0f71f77a448518033de9d76dce140(eventobject) {
        var self = this;
        kony.print("on device back");
    },
    /** preShow defined for frmConsolidatedStatements **/
    AS_Form_d84b2cfd8de24751a9b5502c3eb3dc32: function AS_Form_d84b2cfd8de24751a9b5502c3eb3dc32(eventobject) {
        var self = this;
        this.preShowConsolidatedStatements();
    },
    /** onTouchEnd defined for frmConsolidatedStatements **/
    AS_Form_f678db6110074ef19e33dc7d5cef0e50: function AS_Form_f678db6110074ef19e33dc7d5cef0e50(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onTouchStart defined for imgNotificationClose **/
    AS_Image_e5307a279867438fad05b43b325a8087: function AS_Image_e5307a279867438fad05b43b325a8087(eventobject, x, y) {
        var self = this;
        this.view.flxGenerateStatementsPopup.setVisibility(false);
    },
    /** onTouchStart defined for imgClose **/
    AS_Image_g4313f2274164a2c99ab2e10b38c997b: function AS_Image_g4313f2274164a2c99ab2e10b38c997b(eventobject, x, y) {
        var self = this;
        this.view.flxDownladstatementspopup.setVisibility(false);
    },
    /** onRowClick defined for segFileFilters **/
    AS_Segment_i7c6e45efa254151bd096cede0565f42: function AS_Segment_i7c6e45efa254151bd096cede0565f42(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.fileFiltersCallback.call(this, rowNumber);
    }
});