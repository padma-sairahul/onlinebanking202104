define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxAttachment **/
    AS_FlexContainer_i68bc99d23094733a385eadfa77fcf43: function AS_FlexContainer_i68bc99d23094733a385eadfa77fcf43(eventobject, context) {
        var self = this;
        return self.fileClicked.call(this);
    },
    /** onTouchEnd defined for imgRemoveAttachment **/
    AS_Image_e196bc99f47c463fa06191b61a5be30d: function AS_Image_e196bc99f47c463fa06191b61a5be30d(eventobject, x, y, context) {
        var self = this;
        return self.removeClicked.call(this);
    }
});