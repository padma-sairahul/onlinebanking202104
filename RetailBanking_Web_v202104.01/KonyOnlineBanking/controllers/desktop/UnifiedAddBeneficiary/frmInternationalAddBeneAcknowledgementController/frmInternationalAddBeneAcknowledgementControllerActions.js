define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onDeviceBack defined for frmInternationalAddBeneAcknowledgement **/
    AS_Form_c7dc536604704b738edf4d04b313fced: function AS_Form_c7dc536604704b738edf4d04b313fced(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** onBreakpointChange defined for frmInternationalAddBeneAcknowledgement **/
    AS_Form_f92bc32d0c5d46c098f83dc672c186a3: function AS_Form_f92bc32d0c5d46c098f83dc672c186a3(eventobject, breakpoint) {
        var self = this;
        return self.onBreakPointChange.call(this);
    }
});