define(['FormControllerUtility'],function(FormControllerUtility){
  return {
    /**
	* @api : onNavigate
	* called when the application gets navigated to the respective form
	* @return : NA
	*/
    onNavigate: function(context) {
      var scope = this;
      this.context = context;
      this.view.unifiedAddBeneficiary.setContext(scope, this.context);
      this.view.SwiftLookup.setContext(scope);
      this.view.imgCloseIcon.onTouchStart = this.closeErrorFlex;
    },

    /**
	* @api : onBreakPointChange
	* Reponsible to retain the UI of the form
	* @return : NA
	*/
    onBreakPointChange: function(form,width) {
      var scope = this;
      this.view.flxContent.enableScrolling = true;
      this.view.flxContent.minHeight = kony.os.deviceInfo().screenHeight - 
      parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
      this.view.customheadernew.onBreakpointChangeComponent(width);
    },

    /**
	* @api : preshow
	* Reponsible to retain the data for custom properties for multiple entries into the component
	* @return : NA
	*/
    preshow: function(){
      var scope = this;
      this.view.flxContent.enableScrolling = true;
      scope.view.SwiftLookup.removeSwiftLookup = function(){ 
        scope.removeSwiftLookup();
      };
      this.view.flxAddSameBankAccount.onTouchStart = this.sameBankTransfer;
      this.view.flxAddDomesticAccount.onTouchStart = this.domesticTransfer;
      this.view.flxAddInternationalAccount.onTouchStart = this.internationalTransfer;
      this.view.flxAddPersonToPerson.onTouchStart = this.p2pTransfer;
    },

    /**
	* @api : postShow
	* event called after ui rendered on the screen, is a component life cycle event.
	* @return : NA
	*/
    postShow: function(){
      var scope = this;
      this.view.flxContent.enableScrolling = true;
      this.view.flxContent.minHeight = kony.os.deviceInfo().screenHeight - 
      parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
      if(this.context.transferFail !== "" && (!kony.sdk.isNullOrUndefined(this.context.transferFail))){
        this.view.flxError.setVisibility(true);
        this.view.lblError1.text = this.context.errorMessage;
        this.view.lblError2.text = this.context.transferFail;
      }
    },
    
    /**
	* @api : sameBankTransfer
	* navigate to same bank add beneficiary form
	* @return : NA
	*/
    sameBankTransfer: function() {
      var selectedTrasferType = { "transferType" : "Same Bank Transfer",
                                 "clickedButton" : "AddNewAccount",
                                 "flowType" : "add",
                                 "payeeType" : "New Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedAddBeneficiary/frmSameBankAddBeneficiary", false, selectedTrasferType);
    },

    /**
	* @api : domesticTransfer
	* navigate to domestic add beneficiary form
	* @return : NA
	*/
    domesticTransfer: function() {
      var selectedTrasferType = { "transferType" : "Domestic Transfer",
                                 "clickedButton" : "AddNewAccount",
                                 "flowType" : "add",
                                 "payeeType" : "New Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedAddBeneficiary/frmDomesticAddBeneficiary", false, selectedTrasferType);
    },

    /**
	* @api : internationalTransfer
	* navigate to international add beneficiary form
	* @return : NA
	*/
    internationalTransfer: function() {
      var selectedTrasferType = { "transferType" : "International Transfer",
                                 "clickedButton" : "AddNewAccount",
                                 "flowType" : "add",
                                 "payeeType" : "New Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedAddBeneficiary/frmInternationalAddBeneficiary", false, selectedTrasferType);
    },

    /**
	* @api : p2pTransfer
	* navigate to pay a person add beneficiary form
	* @return : NA
	*/
    p2pTransfer: function() {
      var selectedTrasferType = { "transferType" : "Pay a Person",
                                 "clickedButton" : "AddNewAccount",
                                 "flowType" : "add",
                                 "payeeType" : "New Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedAddBeneficiary/frmPayaPersonAddBeneficiary", false, selectedTrasferType);
    },

    /**
     * @api : closeErrorFlex
     * closes the error flex
     * @return : NA
     */
    closeErrorFlex: function() {
      this.view.flxError.setVisibility(false);
    },

    /**
     * @api : showLookupPopup
     * sets the visibility of lookup as true									   
     * @return : NA
     */
    showLookupPopup: function(context) {
      var scope = this;
      this.view.flxContent.minHeight = "190%";
      this.view.flxContent.enableScrolling = false;
      this.view.flxDialogs.setVisibility(true);
      this.view.flxSwiftLookup.isVisible = true;
      this.view.SwiftLookup.initializePopup(context);
    },

    /**
     * @api : removeSwiftLookup
     * sets the visibility of lookup as false									   
     * @return : NA
     */
    removeSwiftLookup: function() {    
      this.view.flxContent.minHeight = kony.os.deviceInfo().screenHeight - 
      parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
      this.view.flxContent.enableScrolling = true;
      this.view.flxSwiftLookup.isVisible = false;
      this.view.flxDialogs.setVisibility(false);
    },

    /**
     * @api : getLookupData
     * gets the data from lookup popup									   
     * @return : NA
     */
    getLookupData: function(context) {
      this.view.flxContent.minHeight = kony.os.deviceInfo().screenHeight - 
      parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
      this.view.flxContent.enableScrolling = true;
      this.view.flxSwiftLookup.isVisible = false;
      this.view.flxDialogs.setVisibility(false);
      this.view.unifiedAddBeneficiary.setLookupData(context);
    },

    /**
     * @api : swiftLookupError
     * sets error if swift lookup service gets failed										   
     * @return : NA
     */
    swiftLookupError: function(context) {
      if(context !== ""){
        this.removeSwiftLookup();
        this.view.flxError.isVisible = true;
        this.view.lblError1.text = "Error";
        this.view.lblError2.text = context; }
      else{
        this.view.flxError.isVisible = false;
      }
    },

    /**
     * continueAddBeneficiary
     * @api : continueAddBeneficiary
     * Method to perform continue navigation action
     * @return : NA
     */
    continueAddBeneficiary: function(params) {
      var scope = this;
      var formName;
      if(params.contractListData.hasOwnProperty("isSingleProfile")) {
        params.isSingleCustomer = params.contractListData["isSingleProfile"].toString(); 
        if(params.contractListData["isSingleProfile"] === false) {
          formName = "frmLinkPayee";
        } else {
          formName = "UnifiedAddBeneficiary/frmInternationalAddBeneficiaryConfirm";
        }}
      var navMan = applicationManager.getNavigationManager(params);
      navMan.navigateTo(formName, false, params);
    },

    /**
     * @api : confirmCancel
     * navigates to landing screen							   
     * @return : NA
     */
    confirmCancel: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },
    
     updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };

});