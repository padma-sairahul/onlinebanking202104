define(['FormControllerUtility'],function(FormControllerUtility){
  return {
    contextData: {},
    transferType: "",
    confirmFrm: "",
    inputFrm:"",
    /**
	* @api : onNavigate
	* called when the application gets navigated to the respective form
	* @return : NA
	*/
    onNavigate: function(params) {
      var scope = this;
      scope.contextData = params;
      if(params.contractListData.hasOwnProperty("transferType")) {
        scope.transferType = params.contractListData["transferType"];
      }
      if(params.contractListData.hasOwnProperty("profileAccess")) {
        this.view.contractList.setContext(params.contractListData["profileAccess"]);
      }
      if(params.contractListData.hasOwnProperty("contractList")) {
        if(params.flowType === "EDIT"){
          this.view.contractList.initialiseComponent(params.contractListData["contractList"], params.data);
        } else {
          this.view.contractList.initialiseComponent(params.contractListData["contractList"]);
        }
      }
      this.view.contractList.setParentScope(this);
      this.decideFormName();
    },

    /**
	* @api : onBreakPointChange
	* Reponsible to retain the UI of the form
	* @return : NA
	*/
    onBreakPointChange: function(form,width) {
      var scope = this;
      this.view.customheadernew.onBreakpointChangeComponent(width);
    },

    /**
	* @api : decideFormName
	* decides the form to which navigation should be done
	* @return : NA
	*/
    decideFormName: function() {
      var scope = this;
      if(scope.transferType === "Domestic") {
        scope.confirmFrm = "UnifiedAddBeneficiary/frmDomesticAddBeneficiaryConfirm";
        scope.inputFrm = "UnifiedAddBeneficiary/frmDomesticAddBeneficiary";
      } else if(scope.transferType === "International") {
        scope.confirmFrm = "UnifiedAddBeneficiary/frmInternationalAddBeneficiaryConfirm";
        scope.inputFrm = "UnifiedAddBeneficiary/frmInternationalAddBeneficiary";
      } else if(scope.transferType === "SameBank") {
        scope.confirmFrm = "UnifiedAddBeneficiary/frmSameBankAddBeneficiaryConfirm";
        scope.inputFrm = "UnifiedAddBeneficiary/frmSameBankAddBeneficiary";
      } else {
        scope.confirmFrm = "UnifiedAddBeneficiary/frmPayaPersonAddBeneficiaryConfirm";
        scope.inputFrm = "UnifiedAddBeneficiary/frmPayaPersonAddBeneficiary";
      }
    },

    /**
	* @api : continueLinkPayee
	* navigates to acknowledgement screen
	* @return : NA
	*/
    continueLinkPayee: function(data) {
      var scope = this;
      scope.contextData["data"] = data;
      var nav=new kony.mvc.Navigation(scope.confirmFrm);
      nav.navigate(scope.contextData);
    },

    /**
	* @api : backLinkPayee
	* navigates to input screen on click of back button
	* @return : NA
	*/
    backLinkPayee: function() {    
      var scope = this;
      scope.contextData["flowType"] = "modify";
      var navMan = applicationManager.getNavigationManager(scope.contextData);
      navMan.navigateTo(scope.inputFrm, false, scope.contextData);
    },

    /**
	* @api : cancelLinkPayee
	* navigates to landing screen on click of cancel button
	* @return : NA
	*/
    cancelLinkPayee: function() {
      var scope = this;
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },

    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };
});