define(['FormControllerUtility'],function(FormControllerUtility) {
  return {
    /**
	* @api : onNavigate
	* called when the application gets navigated to the respective form
	* @return : NA
	*/
    onNavigate: function(context) {
      var scope = this;
      this.context = context;
      this.view.unifiedAddBeneficiary.setContext(scope, this.context);
      this.view.imgCloseIcon.onTouchStart = this.closeErrorFlex;
    },

    /**
	* @api : onBreakPointChange
	* Reponsible to retain the UI of the form
	* @return : NA
	*/
    onBreakPointChange: function(form,width) {
      var scope = this;
      this.view.customheadernew.onBreakpointChangeComponent(width);
    },

    /**
	* @api : preShow
	* called when the form gets loaded
	* @return : NA
	*/
    preShow: function() {
      var scope = this;
      this.view.flxAddSameBankAccount.onTouchStart = this.sameBankTransfer;
      this.view.flxAddDomesticAccount.onTouchStart = this.domesticTransfer;
      this.view.flxAddInternationalAccount.onTouchStart = this.internationalTransfer;
      this.view.flxAddPersonToPerson.onTouchStart = this.p2pTransfer;
    },

    /**
	* @api : postShow
	* event called after ui rendered on the screen, is a component life cycle event.
	* @return : NA
    */
    postShow: function() {
      var scope = this;
      if(this.context.transferFail !== "" && (!kony.sdk.isNullOrUndefined(this.context.transferFail))){
        this.view.flxError.setVisibility(true);
        this.view.lblError1.text = this.context.errorMessage;
        this.view.lblError2.text = this.context.transferFail;
      }
    },

    /**
	* @api : sameBankTransfer
	* navigate to same bank add beneficiary form
	* @return : NA
	*/
    sameBankTransfer: function() {
      var selectedTrasferType = { "transferType" : "Same Bank Transfer",
                                 "clickedButton" : "AddNewAccount",
                                 "flowType" : "add",
                                 "payeeType" : "New Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedAddBeneficiary/frmSameBankAddBeneficiary", false, selectedTrasferType);
    },

    /**
	* @api : domesticTransfer
	* navigate to domestic add beneficiary form
	* @return : NA
	*/
    domesticTransfer: function() {
      var selectedTrasferType = { "transferType" : "Domestic Transfer",
                                 "clickedButton" : "AddNewAccount",
                                 "flowType" : "add",
                                 "payeeType" : "New Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedAddBeneficiary/frmDomesticAddBeneficiary", false, selectedTrasferType);
    },

    /**
	* @api : internationalTransfer
	* navigate to international add beneficiary form
	* @return : NA
	*/
    internationalTransfer: function() {
      var selectedTrasferType = { "transferType" : "International Transfer",
                                 "clickedButton" : "AddNewAccount",
                                 "flowType" : "add",
                                 "payeeType" : "New Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedAddBeneficiary/frmInternationalAddBeneficiary", false, selectedTrasferType);
    },

    /**
	* @api : p2pTransfer
	* navigate to pay a person add beneficiary form
	* @return : NA
	*/
    p2pTransfer: function() {
      var selectedTrasferType = { "transferType" : "Pay a Person",
                                 "clickedButton" : "AddNewAccount",
                                 "flowType" : "add",
                                 "payeeType" : "New Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedAddBeneficiary/frmPayaPersonAddBeneficiary", false, selectedTrasferType);
    },

    /**
     * continueAddBeneficiary
     * @api : continueAddBeneficiary
     * Method to perform continue navigation action
     * @return : NA
     */
    continueAddBeneficiary: function(params) {
      var scope = this;
      var formName;
      if(params.contractListData.hasOwnProperty("isSingleProfile")) {
        params.isSingleCustomer = params.contractListData["isSingleProfile"].toString(); 
        if(params.contractListData["isSingleProfile"] === false) {
          formName = "frmLinkPayee";
        } else {
          formName = "UnifiedAddBeneficiary/frmSameBankAddBeneficiaryConfirm";
        }}
      var navMan = applicationManager.getNavigationManager(params);
      navMan.navigateTo(formName, false, params);
    },

    /**
     * @api : confirmCancel
     * navigates to landing screen										   
     * @return : NA
     */
    confirmCancel: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },

    /**
     * @api : closeErrorFlex
     * closes the error flex
     * @return : NA
     */
    closeErrorFlex: function() {
      this.view.flxError.setVisibility(false);
    },

    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };

});