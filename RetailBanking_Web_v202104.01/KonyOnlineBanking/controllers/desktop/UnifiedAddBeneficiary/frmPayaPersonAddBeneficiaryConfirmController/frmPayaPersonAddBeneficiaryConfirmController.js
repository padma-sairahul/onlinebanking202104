define(['FormControllerUtility'],function(FormControllerUtility){ 
  return {
    /**
	* @api : onNavigate
	* called when the application gets navigated to the respective form
	* @return : NA
	*/
    onNavigate: function(data) {
      var scope = this;
      this.setContextForAddBeneficiaryService(data);    
      this.view.confirmTransfer.setContext(data,scope);
    },

    /**
	* @api : onBreakPointChange
	* Reponsible to retain the UI of the form
	* @return : NA
	*/
    onBreakPointChange: function(form,width) {
      var scope = this;
      this.view.customheadernew.onBreakpointChangeComponent(width);
    },

    /**
	* @api : preShow
	* Reponsible to retain the data for custom properties for multiple entries into the component
	* @return : NA
	*/
    preShow: function() {
      var scope = this;
      this.view.confirmTransfer.cancelReviewYes = function() {
        var navMan = applicationManager.getNavigationManager();
        navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
      };
    },

    /**
	* @api : setContextForAddBeneficiaryService
	* sets the corresponding context in criteria
	* @return : NA
	*/
    setContextForAddBeneficiaryService: function(context) {
      context.isVerified = "true";
      context.isSameBankAccount = "false";
      context.isInternationalAccount = "false";
      if((!kony.sdk.isNullOrUndefined(context.emailID)) && context.emailID !== "") {
        context.primaryContact = context.emailID;
      } else if((!kony.sdk.isNullOrUndefined(context.phoneNumber)) && context.phoneNumber !== "") {
        context.primaryContact = context.phoneNumber;
        if(!kony.sdk.isNullOrUndefined(context.countryCode))
          context.phoneNumberData = context.countryCode+" "+context.displayPhoneNumber;
        else
          context.phoneNumberData = context.displayPhoneNumber;
      } else if((!kony.sdk.isNullOrUndefined(context.nationalID)) && context.nationalID !== "") {
        context.primaryContact = context.nationalID;
        context.phoneNumber = context.nationalID;
      } 
    },

    /**
	* @api : navToack
	* navigates to acknowledgement screen
	* @return : NA
	*/
    navToack: function(context) {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedAddBeneficiary/frmPayaPersonAddBeneAcknowledgement");
    },

    /**
	* @api : modifyTransfer
	* navigates to acknowledgement screen
	* @return : NA
	*/
    modifyTransfer: function(context) {
      context.flowType = "modify";
      context.transferFail = "";
      var navMan = applicationManager.getNavigationManager(context);
      navMan.navigateTo("UnifiedAddBeneficiary/frmPayaPersonAddBeneficiary", false, context);
    },

    /**
	* @api : confirmTransferSuccess
	* navigates to acknowledgement screen when service gets success
	* @return : NA
	*/ 
    confirmTransferSuccess: function(params) { 
      var navMan = applicationManager.getNavigationManager(params);
      navMan.navigateTo("UnifiedAddBeneficiary/frmPayaPersonAddBeneAcknowledgement", false, params);
    },

    /**
	* @api : confirmTransferError
	* navigates to input screen when service fails
	* @return : NA
	*/
    confirmTransferError: function(params) {
      params.flowType = "modify";
      params.errorMessage = "Failed to Add Beneficiary";
      var navMan = applicationManager.getNavigationManager(params);
      navMan.navigateTo("UnifiedAddBeneficiary/frmPayaPersonAddBeneficiary", false, params);
    },

    /**
	* @api : onError
	* gets handled on error scenario
	* @return : NA
	*/
    onError: function(errObj) {
      alert(JSON.stringify(errObj));
    },

    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };

});