define(['FormControllerUtility'],function(FormControllerUtility){
  return {

    /**
	* @api : onNavigate
	* called when the application gets navigated to the respective form
	* @return : NA
	*/
    onNavigate:function(context) {  
      var scope = this;
      this.view.TransferAcknowledgement.setContext(context,scope);          
    },

    /**
	* @api : onBreakPointChange
	* Reponsible to retain the UI of the form
	* @return : NA
	*/
    onBreakPointChange: function(form,width) {
      var scope = this;
      this.view.customheadernew.onBreakpointChangeComponent(width);
    },

    /**
	* @api : transferActivities
	* navigates to transfers activities screen
	* @return : NA
	*/
    transferActivities: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("frmPastPaymentsNew");
    },

    /**
	* @api : newTransfer
	* navigates to landing screen
	* @return : NA
	*/
    newTransfer: function(){
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },


    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };
});