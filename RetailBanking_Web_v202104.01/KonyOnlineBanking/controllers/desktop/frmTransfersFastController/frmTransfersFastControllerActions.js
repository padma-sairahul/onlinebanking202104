define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxResetUserImg **/
    AS_FlexContainer_e1ebda128af147b2840ab4db7c871c23: function AS_FlexContainer_e1ebda128af147b2840ab4db7c871c23(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onClick defined for flxUserId **/
    AS_FlexContainer_a9e83d8fa47e4310b61f080c0a2b1085: function AS_FlexContainer_a9e83d8fa47e4310b61f080c0a2b1085(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_e2cc83a4375640b5aa42c341843ff084: function AS_FlexContainer_e2cc83a4375640b5aa42c341843ff084(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_e58999a491f2460eab1080865e39f154: function AS_FlexContainer_e58999a491f2460eab1080865e39f154(eventobject) {
        var self = this;
        //this.closeHamburgerMenu();
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_b131d2a2e2b84f41a09688931e96d1d4: function AS_Button_b131d2a2e2b84f41a09688931e96d1d4(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_gcce3990a8c14d0b916a80d5dc7cc363: function AS_Button_gcce3990a8c14d0b916a80d5dc7cc363(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnbacktopayeelist **/
    AS_Button_g5aa9d1286ea4eac9c3b994139caf739: function AS_Button_g5aa9d1286ea4eac9c3b994139caf739(eventobject) {
        var self = this;
        this.presenter.getExternalAccounts();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_c78508ff256348fda5861d9adb10bc42: function AS_FlexContainer_c78508ff256348fda5861d9adb10bc42(eventobject) {
        var self = this;
        //this.addExternalAccount();
        this.presenter.showDomesticAccounts();
    },
    /** onTouchStart defined for imgViewCVVCode **/
    AS_Image_a60a713ca0eb491f8076bfa9858edcf3: function AS_Image_a60a713ca0eb491f8076bfa9858edcf3(eventobject, x, y) {
        var self = this;
        this.showPassword();
    },
    /** onTouchEnd defined for imgViewCVVCode **/
    AS_Image_g3c95526b44c4a3dae1f3ae1df938b8c: function AS_Image_g3c95526b44c4a3dae1f3ae1df938b8c(eventobject, x, y) {
        var self = this;
        this.hidePassword();
    },
    /** onEndEditing defined for tbxAnswers1 **/
    AS_TextField_hdbf6e370e8e485b8c1aba3fbd9c4474: function AS_TextField_hdbf6e370e8e485b8c1aba3fbd9c4474(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers1 **/
    AS_TextField_a817809fdaba46ba8bbd1924b26e1015: function AS_TextField_a817809fdaba46ba8bbd1924b26e1015(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onEndEditing defined for tbxAnswers2 **/
    AS_TextField_b75eae7786734a7e8f731ffcf98f02f8: function AS_TextField_b75eae7786734a7e8f731ffcf98f02f8(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers2 **/
    AS_TextField_d323198d686546de9a48a0607a8b5353: function AS_TextField_d323198d686546de9a48a0607a8b5353(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_h300af62888247e0bfea92d286279a5e: function AS_Button_h300af62888247e0bfea92d286279a5e(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_dd61e5dcca774ce29168911ee5e386bc: function AS_Button_dd61e5dcca774ce29168911ee5e386bc(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_ec433f48bf88459eb232e8bd4250375a: function AS_Button_ec433f48bf88459eb232e8bd4250375a(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_g3cb53f89d5f42298ff7f828f3774156: function AS_Button_g3cb53f89d5f42298ff7f828f3774156(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_bc2eb3c855b5464b8a178377c970046b: function AS_Button_bc2eb3c855b5464b8a178377c970046b(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for btnClose **/
    AS_Button_ac1160b75c0c44bc80aecad0ff48716c: function AS_Button_ac1160b75c0c44bc80aecad0ff48716c(eventobject) {
        var self = this;
        this.closeViewReport();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_c71e50083d9f4e1998f811c10f71169f: function AS_Button_c71e50083d9f4e1998f811c10f71169f(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_fa0b8a980bcc4ffba6486dbf86887594: function AS_Button_fa0b8a980bcc4ffba6486dbf86887594(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_ga3fe23a32674376ad8e53f50fd89559: function AS_Button_ga3fe23a32674376ad8e53f50fd89559(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_gf7246e86b7e408fad5a9da225cce464: function AS_Button_gf7246e86b7e408fad5a9da225cce464(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_gcd25930a3de4b60baee084efbbe1ff7: function AS_Button_gcd25930a3de4b60baee084efbbe1ff7(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** init defined for frmTransfersFast **/
    AS_Form_b0db7f4c05894344a3f299b3c3e1a306: function AS_Form_b0db7f4c05894344a3f299b3c3e1a306(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmTransfersFast **/
    AS_Form_e829839ba7cd4cb6afba733340d523ba: function AS_Form_e829839ba7cd4cb6afba733340d523ba(eventobject) {
        var self = this;
        this.initTabsActions();
    },
    /** postShow defined for frmTransfersFast **/
    AS_Form_a25f2474f27e4ca6985b43e7a4cd86c9: function AS_Form_a25f2474f27e4ca6985b43e7a4cd86c9(eventobject) {
        var self = this;
        this.postShowtransfers();
    },
    /** onDeviceBack defined for frmTransfersFast **/
    AS_Form_j597862f49e242769879929b611742a8: function AS_Form_j597862f49e242769879929b611742a8(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmTransfersFast **/
    AS_Form_hdf39ffadb034e499666ee6579bef9dc: function AS_Form_hdf39ffadb034e499666ee6579bef9dc(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_b6d9131aa18e4fc69efb21e4262e072c: function AS_FlexContainer_b6d9131aa18e4fc69efb21e4262e072c(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for flxToDropdown **/
    AS_FlexContainer_b4dc20431b594ce080000b6acd00fd34: function AS_FlexContainer_b4dc20431b594ce080000b6acd00fd34(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for flxFrequencyDropdown **/
    AS_FlexContainer_b2bf4932c0104ead809f80a320d82c66: function AS_FlexContainer_b2bf4932c0104ead809f80a320d82c66(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});