define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_fd07a89d061248b0b623cdcbf281f3ab: function AS_FlexContainer_fd07a89d061248b0b623cdcbf281f3ab(eventobject, context) {
        var self = this;
        this.toggleCheckBox(eventobject, context);
    },
    /** onTouchStart defined for lblDropDown **/
    AS_Label_b19c2d2fdac147db9d8d84ac5d072072: function AS_Label_b19c2d2fdac147db9d8d84ac5d072072(eventobject, x, y, context) {
        var self = this;
        this.toggleSegment(eventobject, context)
    }
});