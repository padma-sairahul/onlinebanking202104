define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmContactUsPrivacyTandC **/
    AS_Form_bfee161e52bc4ec29b7d4c2bada7e891: function AS_Form_bfee161e52bc4ec29b7d4c2bada7e891(eventobject) {
        var self = this;
        this.init();
    },
    /** onTouchEnd defined for frmContactUsPrivacyTandC **/
    AS_Form_d45ef6cec3f940f5a8116f4e21d2ca69: function AS_Form_d45ef6cec3f940f5a8116f4e21d2ca69(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** postShow defined for frmContactUsPrivacyTandC **/
    AS_Form_dd0a31c1c5b146dcae49951586c4151b: function AS_Form_dd0a31c1c5b146dcae49951586c4151b(eventobject) {
        var self = this;
        this.postShow();
    },
    /** onDeviceBack defined for frmContactUsPrivacyTandC **/
    AS_Form_g3ed63b605504d3099f924681f621216: function AS_Form_g3ed63b605504d3099f924681f621216(eventobject) {
        var self = this;
        kony.print("on device back");
    },
    /** preShow defined for frmContactUsPrivacyTandC **/
    AS_Form_h25a0a7476d94d6386b82106f20b3af7: function AS_Form_h25a0a7476d94d6386b82106f20b3af7(eventobject) {
        var self = this;
        this.preshow();
    }
});