define({
    showSelectedRow: function() {
        var currentForm = kony.application.getCurrentForm();
        var index = currentForm.scheduledTransactions.segTransactions.selectedIndex;
        var sectionIndex = index[0];
        var rowIndex = index[1];
        var data = currentForm.scheduledTransactions.segTransactions.data;
        var ClosedTemplate = 'flxScheduledTransactionsMobile';
        var OpenedTemplate = 'flxScheduledTransactionsSelectedMobile';
        if (data[sectionIndex][1]) {
            if (data[sectionIndex][1][rowIndex].template == ClosedTemplate) {
                data[sectionIndex][1][rowIndex].imgDropdown = "P";
                data[sectionIndex][1][rowIndex].template = OpenedTemplate;
            } else {
                data[sectionIndex][1][rowIndex].imgDropdown = "O";
                data[sectionIndex][1][rowIndex].template = ClosedTemplate;
            }
            currentForm.scheduledTransactions.segTransactions.setDataAt(data[sectionIndex][1][rowIndex], rowIndex, sectionIndex);
        } else {
            if (data[rowIndex].template == ClosedTemplate) {
                data[rowIndex].imgDropdown = "P";
                data[rowIndex].template = OpenedTemplate;
            } else {
                data[rowIndex].imgDropdown = "O";
                data[rowIndex].template = ClosedTemplate;
            }
            currentForm.scheduledTransactions.segTransactions.setDataAt(data[rowIndex], rowIndex, sectionIndex);
        }
        this.AdjustScreen(-95);
    },
    showEditRule: function() {
        kony.print("edit rule pressed");
        kony.application.getCurrentForm().flxEditRule.isVisible = true;
        this.AdjustScreen(30);
        kony.application.getCurrentForm().forceLayout();
    },
    rememberCategory: function() {
        var index = kony.application.getCurrentForm().transactions.segTransactions.selectedIndex;
        var sectionIndex = index[0];
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().transactions.segTransactions.data;
        if (data[sectionIndex][1][rowIndex].imgRememberCategory == "unchecked_box.png") {
            data[sectionIndex][1][rowIndex].imgRememberCategory = "checked_box.png";
        } else {
            data[sectionIndex][1][rowIndex].imgRememberCategory = "unchecked_box.png";
        }
        kony.application.getCurrentForm().transactions.segTransactions.setDataAt(data[sectionIndex][1][rowIndex], rowIndex, sectionIndex);
        this.AdjustScreen(30);
    },
    AdjustScreen: function(data) {
        var currentForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currentForm.customheader.frame.height + currentForm.flxMainWrapper.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currentForm.flxFooter.frame.height;
            if (diff > 0)
                currentForm.flxFooter.top = mainheight + diff + "dp";
            else
                currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        } else {
            currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        }
    },
});