define({
    showSelectedRow: function() {
        var index = kony.application.getCurrentForm().tableView.segP2P.selectedRowIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().tableView.segP2P.data;
        //data[rowIndex].template = "flxSendRequestSelected";
        for (i = 0; i < data.length; i++) {
            if (i == rowIndex) {
                kony.print("index:" + index);
                data[i].imgDropdown = "chevron_up.png";
                if (kony.application.getCurrentBreakpoint() == 640) {
                    data[i].template = "flxSendRequestSelectedMobile";
                } else {
                    data[i].template = "flxSendRequestSelected";
                }
            } else {
                data[i].imgDropdown = "arrow_down.png";
                if (kony.application.getCurrentBreakpoint() == 640) {
                    data[i].template = "flxSendRequestMobile";
                } else {
                    data[i].template = "flxSendRequest";
                }
            }
        }
        kony.application.getCurrentForm().tableView.segP2P.setData(data);
        this.AdjustScreen(130);
        //kony.application.getCurrentForm().tableView.segP2P.setDataAt(data[rowIndex], rowIndex);
    },
    showSendMoney: function() {
        kony.application.getCurrentForm().tableView.flxTabs.btnSendRequest.skin = "sknBtnAccountSummarySelected";
        kony.application.getCurrentForm().tableView.flxTabs.btnMyRequests.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnSent.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnRecieved.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnManageRecepient.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxSendMoney.setVisibility(true);
        kony.application.getCurrentForm().tableView.flxSendReminder.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxRequestMoney.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxHorizontalLine2.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxTableHeaders.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxSearch1.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxHorizontalLine3.setVisibility(false);
        kony.application.getCurrentForm().tableView.segP2P.setVisibility(false);
        this.AdjustScreen(30);
    },
    showRequestMoney: function() {
        kony.application.getCurrentForm().tableView.flxTabs.btnSendRequest.skin = "sknBtnAccountSummarySelected";
        kony.application.getCurrentForm().tableView.flxTabs.btnMyRequests.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnSent.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnRecieved.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxTabs.btnManageRecepient.skin = "sknBtnAccountSummaryUnselected";
        kony.application.getCurrentForm().tableView.flxSendMoney.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxSendReminder.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxRequestMoney.setVisibility(true);
        kony.application.getCurrentForm().tableView.flxHorizontalLine2.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxTableHeaders.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxSearch1.setVisibility(false);
        kony.application.getCurrentForm().tableView.flxHorizontalLine3.setVisibility(false);
        kony.application.getCurrentForm().tableView.segP2P.setVisibility(false);
        this.AdjustScreen(30);
    },
    //UI Code
    AdjustScreen: function(data) {
        var currForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currForm.customheader.info.frame.height + currForm.flxContainer.info.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currForm.flxFooter.info.frame.height;
            if (diff > 0)
                currForm.flxFooter.top = mainheight + diff + data + "dp";
            else
                currForm.flxFooter.top = mainheight + data + "dp";
        } else {
            currForm.flxFooter.top = mainheight + data + "dp";
        }
        currForm.forceLayout();
    },
});