define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnSend **/
    AS_Button_f27e7f3d43d04eeeba1ec4d0459893fa: function AS_Button_f27e7f3d43d04eeeba1ec4d0459893fa(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a503f91b130746a49738a0ad07b066f9: function AS_FlexContainer_a503f91b130746a49738a0ad07b066f9(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});