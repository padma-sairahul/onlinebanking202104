define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnViewActivity **/
    AS_Button_a25fd7ef870d4fca8e8e9e650b3bed29: function AS_Button_a25fd7ef870d4fca8e8e9e650b3bed29(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnDelete **/
    AS_Button_c033a8d6812d4eeb8474bdcb33467534: function AS_Button_c033a8d6812d4eeb8474bdcb33467534(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnRequestMoney **/
    AS_Button_d62adf39de124c918c043563fb7d9922: function AS_Button_d62adf39de124c918c043563fb7d9922(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for btnSendMoney **/
    AS_Button_ed3f3528a11b4eea8dc078ef9f418d62: function AS_Button_ed3f3528a11b4eea8dc078ef9f418d62(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for btnEdit **/
    AS_Button_j31e51592b614912a062bbf4f5994bc2: function AS_Button_j31e51592b614912a062bbf4f5994bc2(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_e1cda2cec1144ce9981daa50335b68f6: function AS_FlexContainer_e1cda2cec1144ce9981daa50335b68f6(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});