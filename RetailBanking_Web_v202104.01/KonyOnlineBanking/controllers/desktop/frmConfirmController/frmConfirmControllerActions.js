define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmConfirm **/
    AS_Form_d9da2e78e1e64e52b805184c42506ae6: function AS_Form_d9da2e78e1e64e52b805184c42506ae6(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmConfirm **/
    AS_Form_fd3555d200f14f3c9ea0b9e8912dd1c4: function AS_Form_fd3555d200f14f3c9ea0b9e8912dd1c4(eventobject) {
        var self = this;
        this.initActions();
    },
    /** postShow defined for frmConfirm **/
    AS_Form_ib195729afc04fa883199f812184cb7f: function AS_Form_ib195729afc04fa883199f812184cb7f(eventobject) {
        var self = this;
        this.postShowFrmConfirm();
        if (this.presenter && this.presenter.loadHamburgerTransfers) {
            this.presenter.loadHamburgerTransfers('frmConfirm');
        }
    },
    /** onDeviceBack defined for frmConfirm **/
    AS_Form_gb1de90d674c44f2837fb78df79f6a99: function AS_Form_gb1de90d674c44f2837fb78df79f6a99(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmConfirm **/
    AS_Form_f046446679b34d54a2835b8a6d68f630: function AS_Form_f046446679b34d54a2835b8a6d68f630(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});