define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnModify **/
    AS_Button_c8501a56b0ee410e9342f332af04abe6: function AS_Button_c8501a56b0ee410e9342f332af04abe6(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a38e0e1ce52448669051a44fce8f5403: function AS_FlexContainer_a38e0e1ce52448669051a44fce8f5403(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});