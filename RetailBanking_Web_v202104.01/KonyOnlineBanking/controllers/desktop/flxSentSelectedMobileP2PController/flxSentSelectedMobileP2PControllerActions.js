define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnModify **/
    AS_Button_be82b0b552c648f7bbb9ebcd491b103c: function AS_Button_be82b0b552c648f7bbb9ebcd491b103c(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_j05502343fdd4b7a8303a00c6bb6394b: function AS_FlexContainer_j05502343fdd4b7a8303a00c6bb6394b(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});