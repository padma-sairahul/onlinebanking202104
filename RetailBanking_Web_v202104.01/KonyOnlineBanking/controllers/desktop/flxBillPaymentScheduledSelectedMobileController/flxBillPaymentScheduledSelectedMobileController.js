define({
    showUnselectedRow: function() {
        var rowIndex = kony.application.getCurrentForm().segmentBillpay.selectedRowIndex[1];
        var data = kony.application.getCurrentForm().segmentBillpay.data;
        var pre_val;
        var required_values = [];
        var array_close = ["O", false, "sknffffff15pxolbfonticons", "80dp", "sknflxffffffnoborder"];
        if (applicationManager.getConfigurationManager().isCombinedUser === "true" && (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile)) {
            var array_open = ["P", true, "sknLbl4a90e215px", "300dp", "sknFlxf7f7f7"];
        } else {
            var array_open = ["P", true, "sknLbl4a90e215px", "365dp", "sknFlxf7f7f7"];
        }
        if (previous_index_schedule === rowIndex) {
            data[rowIndex].lblDropdown == "P" ? required_values = array_close : required_values = array_open;
            this.toggle(rowIndex, required_values);
        } else {
            if (previous_index_schedule >= 0) {
                pre_val = previous_index_schedule;
                this.toggle(pre_val, array_close);
            }
            pre_val = rowIndex;
            this.toggle(rowIndex, array_open);
        }
        previous_index_schedule = rowIndex;
    },
    toggle: function(index, array) {
        var data = kony.application.getCurrentForm().segmentBillpay.data;
        data[index].lblDropdown = array[0];
        data[index].flxIdentifier.isVisible = array[1];
        //data[index].flxIdentifier.skin = array[2];
        data[index].lblIdentifier.skin = array[2];
        data[index].flxBillPaymentScheduledSelectedMobile.height = array[3];
        data[index].flxBillPaymentScheduledSelectedMobile.skin = array[4];
        data[index].flxEdit.isVisible = array[1];
        kony.application.getCurrentForm().segmentBillpay.setDataAt(data[index], index);
    },
});