define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEdit **/
    AS_Button_a12d349bb94f408299a71096f6577fb2: function AS_Button_a12d349bb94f408299a71096f6577fb2(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnEbill **/
    AS_Button_ac5b94874e504d349e2999bc3e725476: function AS_Button_ac5b94874e504d349e2999bc3e725476(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnCancel **/
    AS_Button_h26a47546410489cb5c64dd3b4fdf361: function AS_Button_h26a47546410489cb5c64dd3b4fdf361(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a7569f6b78c942ecb81f375944f31605: function AS_FlexContainer_a7569f6b78c942ecb81f375944f31605(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    }
});