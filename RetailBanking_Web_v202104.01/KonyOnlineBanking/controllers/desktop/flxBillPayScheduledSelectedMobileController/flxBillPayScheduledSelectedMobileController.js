define({
    segmentHistoryRowClick: function() {
        var currForm = kony.application.getCurrentForm();
        var formController = _kony.mvc.GetController(currForm.id, true);
        formController.onScheduledSegmentRowClick();
        this.AdjustScreen(-70);
    },
    showPayABill: function() {
        var currForm = kony.application.getCurrentForm();
        currForm.payABill.isVisible = true;
        currForm.breadcrumb.setBreadcrumbData([{
            text: "BILL PAY"
        }, {
            text: "PAY A BILL"
        }]);
        currForm.btnConfirm.setVisibility(false);
        currForm.tableView.isVisible = false;
        this.AdjustScreen(30);
    },
    AdjustScreen: function(data) {
        var currentForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currentForm.customheader.info.frame.height + currentForm.flxContainer.info.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currentForm.flxFooter.info.frame.height;
            if (diff > 0)
                currentForm.flxFooter.top = mainheight + diff + data + "dp";
            else
                currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        } else {
            currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        }
    },
});