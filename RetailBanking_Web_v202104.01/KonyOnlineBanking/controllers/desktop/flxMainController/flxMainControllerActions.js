define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnRepeat **/
    AS_Button_dc060cb63d754e2c9c5c95fd8bac079f: function AS_Button_dc060cb63d754e2c9c5c95fd8bac079f(eventobject, context) {
        var self = this;
        var currForm = kony.application.getCurrentForm();
        _kony.mvc.GetController(currForm.id, true).presenter.onloadPayABill()
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_bf07ef8c9537476b8ea4589636860db0: function AS_FlexContainer_bf07ef8c9537476b8ea4589636860db0(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    }
});