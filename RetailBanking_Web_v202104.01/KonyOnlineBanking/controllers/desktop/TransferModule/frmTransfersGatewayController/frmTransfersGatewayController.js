define(['commonUtilities', 'FormControllerUtility', 'OLBConstants', 'ViewConstants'], function(commonUtilities, FormControllerUtility, OLBConstants, ViewConstants) {
    var orientationHandler = new OrientationHandler();
    var offset = OLBConstants.DEFAULT_OFFSET;
    var KonyBankAccountsTransfer = "KonyBankAccountsTransfer";
    var OtherKonyAccountsTransfer = "OtherKonyAccountsTransfer";
    var OtherBankAccountsTransfer = "OtherBankAccountsTransfer";
    var DomesticWireTransfer = "DomesticWireTransfer";
    var InternationalAccountsTransfer = "InternationalAccountsTransfer";
    var InternationalWireTransfer = "InternationalWireTransfer";
    var CONFIG_WIRE_TRANSFER = 'WIRE_TRANSFER';
    this.isExternalAccount = false;
    this.isInternalAccount = false;


    return {
        initialLoadingDone: false,

        /** Manages the upcomming flow
         * @param  {object} viewModel object consisting data based on which new flow has to drive
         */
        updateFormUI: function(viewModel) {
            if (viewModel.serverError) {
                this.showServerError(viewModel.serverError);
            } else {
                if (viewModel.isLoading === true) {
                    FormControllerUtility.showProgressBar(this.view);
                } else if (viewModel.isLoading === false) {
                    FormControllerUtility.hideProgressBar(this.view);
                }
                if (viewModel.resetAndShowProgressBar) {
                    this.hideAll();
                }
                if (viewModel.gateway) {
                    this.updateGateWay(viewModel.gateway);
                }
                if (viewModel.pendingAccounts) {
                    this.showPendingAccounts(viewModel.pendingAccounts);
                }

            }
            this.AdjustScreen();
            this.updateHamburgerMenu();
            this.view.forceLayout();
        },

        /**Preshow for Transfers
         */
        preShowTransfers: function() {
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Transfers");
            this.view.flxVerifyAccount.setVisibility(false);
            this.view.customheader.headermenu.btnLogout.isVisible = true;
            this.view.lblVerifyButton.hoverSkin = "skncursor";
            commonUtilities.setText(this.view.TermsAndConditions1.lblInsTwo, kony.i18n.getLocalizedString("i18n.BillPay.TermsAndConditionsPoint2"), commonUtilities.getaccessibilityConfig());
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['customheader', 'flxMain', 'flxFooter', 'flxHeader']);
        },

        /**Show main transfer window - contains tabs etc
         */
        showMainTransferWindow: function() {
            this.view.flxTrasfersWindow.setVisibility(true);
            this.view.flxNoAccounts.setVisibility(false);
        },

        showAddIntraBankRecipientOption: function() {
            this.view.flxAddKonyAccount.setVisibility(true);
        },
        hideAddIntraBankRecipientOption: function() {
            this.view.flxAddKonyAccount.setVisibility(false);
        },
        showAddInterBankRecipientOption: function() {
            this.view.flxAddNonKonyAccount.setVisibility(true);
        },
        hideAddInterBankRecipientOption: function() {
            this.view.flxAddNonKonyAccount.setVisibility(false);

        },
        showAddInternationalBankRecipientOption: function() {
            this.view.flxAddInternationalAccount.setVisibility(true);
        },
        hideAddInternationalBankRecipientOption: function() {
            this.view.flxAddInternationalAccount.setVisibility(false);
        },

        /**Called when post show is called
         */
        postShowtransfers: function() {
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.showPendingAccountsCount("frmTransfersGateway");
            this.view.customheader.forceCloseHamburger();
            var scope = this;
            this.view.onBreakpointChange = function() {
                scope.onBreakpointChange(kony.application.getCurrentBreakpoint());
            }
            this.view.lblAddKonyAccount.toolTip = kony.i18n.getLocalizedString("i18n.transfers.add_kony_accountMod");
            this.view.lblAddNonKonyAccount.toolTip = kony.i18n.getLocalizedString("i18n.transfers.add_non_kony_accountMod");
            this.view.lblAddInternationalAccount.toolTip = kony.i18n.getLocalizedString("i18n.transfers.add_international_accountMod");
            applicationManager.executeAuthorizationFramework(this);
            if (this.areAllHidden(["flxAddKonyAccount", "flxAddNonKonyAccount", "flxAddInternationalAccount"])) {
                this.view.flxAddAccountWindow.setVisibility(false);
            }
            this.AdjustScreen();
        },
        areAllHidden: function(widgetIds) {
            return widgetIds.some(function(widgetId) {
                return !this.view[widgetId].isVisible
            })
        },
        /**Form Preshow actions to be done
         */
        initActions: function() {
            this.view.customheader.forceCloseHamburger();
            applicationManager.getNavigationManager().applyUpdates(this);

        },
        /**Adjusts screen - Places footer on correct place
         * @param  {number} data Offset needs to be added
         */
        AdjustScreen: function(data) {
            this.view.forceLayout();
            if (data === undefined) {
                data = 0;
            }
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.info.frame.height + this.view.flxMain.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT + data + ViewConstants.POSITIONAL_VALUES.DP;
                } else {
                    this.view.flxFooter.top = mainheight + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT + data + ViewConstants.POSITIONAL_VALUES.DP;
                }
                this.view.forceLayout();
            } else {
                this.view.flxFooter.top = mainheight + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT + data + ViewConstants.POSITIONAL_VALUES.DP;
                this.view.forceLayout();
            }
            this.initializeResponsiveViews();
        },
        /**Initial Actions mapping - called on preshow
         */
        initTabsActions: function() {
            var scopeObj = this;
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
            this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
            this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
            this.view.flxTransferViewReport.isVisible = false;
            this.fixBreadcrumb();
            this.view.flxVerifyAccount.setVisibility(false);
            this.view.btnExternalAccounts.onClick = function() {
                //this.view.flxMainContainerTable.flxSegment.skin="slFbox";
                var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transfersModule.presentationController.showExternalAccounts();
                //this.view.transfermain.Search.txtSearch.setFocus(true);
            }.bind(this);
            this.view.btnExternalAccounts.toolTip = kony.i18n.getLocalizedString("i18n.transfers.external_accounts");
            this.view.btnRecent.onClick = function() {
                var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transfersModule.presentationController.showRecentUserTransactions();
                //this.view.transfermain.Search.txtSearch.setFocus(true);
            }.bind(this);
            this.view.btnScheduled.onClick = function() {
                this.getUserScheduledTransactions();
                //this.view.transfermain.Search.txtSearch.setFocus(true);
            }.bind(this);
            //Setting Actions for Add Kony Accounts
            this.view.flxAddKonyAccount.onClick = function() {
                var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transfersModule.presentationController.showSameBankAccounts();
            }.bind(this);
            this.view.flxAddNonKonyAccount.onClick = function() {
                var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transfersModule.presentationController.showDomesticAccounts();
            }.bind(this);
            this.view.flxAddInternationalAccount.onClick = function() {
                var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transfersModule.presentationController.showInternationalAccounts();
            }.bind(this);
            this.view.flxSperator2.top = "57dp";
            this.view.flxSeperator3.top = "57dp";
        },
        /**Resets Bread crumb state
         */
        fixBreadcrumb: function() {
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer")
            }]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
        },
        /**Hides all the main flexes and  Resets UI
         */
        hideAll: function() {

        },
        /**  Returns height of the page
         * @returns {String} height height of the page
         */
        getPageHeight: function() {
            var height = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height + this.view.flxFooter.info.frame.height + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT;
            return height + ViewConstants.POSITIONAL_VALUES.DP;
        },
        /**Hamburger Menu COnfiguration
         * @param  {object} sideMenuModel Side Menu viewModel
         */
        updateHamburgerMenu: function(sideMenuModel) {
            this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");

        },
        /**Show Server error in the UI
         * @param  {object} viewModel ViewModel containing server error message
         */
        showServerError: function(viewModel) {
            var scopeObj = this;
            scopeObj.view.flxMakeTransferError.setVisibility(true);
            scopeObj.view.rtxMakeTransferError.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
        },


        //UI Code
        /**
         * onBreakpointChange : Handles ui changes on .
         * @member of {frmTransfersController}
         * @param {integer} width - current browser width
         * @return {}
         * @throws {}
         */
        onBreakpointChange: function(width) {
            /*orientationHandler.onOrientationChange(this.onBreakpointChange, function() {}, function() {
                      this.view.transfermain.maketransfer.calSendOn.dismiss();
                  }.bind(this));*/
            var scope = this;
            this.view.customheader.onBreakpointChangeComponent(width);
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.CustomPopupCancel.onBreakpointChangeComponent(scope.view.CustomPopupCancel, width);
            this.view.deletePopup.onBreakpointChangeComponent(scope.view.deletePopup, width);
            this.setupFormOnTouchEnd(width);
            this.view.flxFooter.isVisible = true;
            //this.view.transfermain.flxHeader.isVisible = false;
            // this.view.transfermain.maketransfer.flxOption1.isVisible = false;
            // this.view.transfermain.maketransfer.flxOption2.isVisible = false;
            // this.view.transfermain.maketransfer.flxOption3.isVisible = false;
            // this.view.transfermain.maketransfer.flxOption4.isVisible = false;
            // this.view.transfermain.maketransfer.tbxNoOfRecurrences.isVisible = false;
            // this.view.transfermain.maketransfer.flxCalEndingOn.isVisible = false;
            var views;
            if (width === 640 || orientationHandler.isMobile) {
                var scopeObj = this;
                views = Object.keys(this.responsiveViews);
                views.forEach(function(e) {
                    scope.view[e].isVisible = scope.responsiveViews[e];
                });
                /*this.view.transferActivitymod.flxtransferActivityWrapper.skin = "slFbox";
                this.view.transferActivitymod.flxRight.skin = "sknFlxffffffborderr0pxe3e3e3";
                this.view.transferActivitymod.flxHeader.skin = "sknFlxffffffborderr0pxe3e3e3";
                this.view.transfermain.flxTabsChecking.skin = "slFbox";*/
                commonUtilities.setText(this.view.customheader.lblHeaderMobile, "Transfers", commonUtilities.getaccessibilityConfig());
                //this.view.transfermain.Search.skin = "slFbox";
                this.view.flxTransferType.skin = "sknFlxffffff";
                this.view.flxToMyKonyBank.skin = "sknFlxffffffShadowdddcdc";
                this.view.flxToOtherKonyBank.skin = "sknFlxffffffShadowdddcdc";
                this.view.flxToOtherBank.skin = "sknFlxffffffShadowdddcdc";
                this.view.flxToInternational.skin = "sknFlxffffffShadowdddcdc";
                this.view.flxWireTransfer.skin = "sknFlxffffffShadowdddcdc";
                this.view.flxArrow1.isVisible = true;
                this.view.flxArrow2.isVisible = true;
                this.view.flxArrow3.isVisible = true;
                this.view.flxArrow4.isVisible = true;
                this.view.flxArrow5.isVisible = true;
                this.view.flxToMyKonyBankInfo.skin = "slFbox";
                this.view.flxToOtherKonyBankInfo.skin = "slFbox";
                this.view.flxToOtherBankInfo.skin = "slFbox";
                this.view.flxToInternationalInfo.skin = "slFbox";
                this.view.flxWireTransferInfo.skin = "slFbox";
                this.view.flxMainContainerTable.skin = "slFbox";
                this.view.forceLayout();
            } else {
                views = Object.keys(this.responsiveViews);
                views.forEach(function(e) {
                    scope.view[e].isVisible = scope.responsiveViews[e];
                });
                commonUtilities.setText(this.view.customheader.lblHeaderMobile, "", commonUtilities.getaccessibilityConfig());
                /*this.view.transferActivitymod.flxHeader.skin = "slFbox";
                this.view.transfermain.maketransfer.flxTransferForm.skin = "sknFFFFFFnoBor";
                this.view.transfermain.Search.skin = "sknFlxffffff";
                this.view.transfermain.flxTabsChecking.skin = "slFbox";
                this.view.transfermain.flxMainContainerTable.skin = "sknFlxffffffShadowdddcdc"*/
                this.view.flxToMyKonyBank.skin = "sknBorderE3E3E3";
                this.view.flxToOtherKonyBank.skin = "sknBorderE3E3E3";
                this.view.flxToOtherBank.skin = "sknBorderE3E3E3";
                this.view.flxToInternational.skin = "sknBorderE3E3E3";
                this.view.flxWireTransfer.skin = "sknBorderE3E3E3";
                //this.view.transferActivitymod.flxRight.skin = "slFbox";
                this.view.flxToMyKonyBankInfo.skin = "sknflxffffffBottomBorder";
                this.view.flxToOtherKonyBankInfo.skin = "sknflxffffffBottomBorder";
                this.view.flxToOtherBankInfo.skin = "sknflxffffffBottomBorder";
                this.view.flxToInternationalInfo.skin = "sknflxffffffBottomBorder";
                this.view.flxWireTransferInfo.skin = "sknflxffffffBottomBorder";
                this.view.flxToMyKonyBank.hoverSkin = "sknFlxffffffShadowdddcdc";
                this.view.flxToOtherKonyBank.hoverSkin = "sknFlxffffffShadowdddcdc";
                this.view.flxToOtherBank.hoverSkin = "sknFlxffffffShadowdddcdc";
                this.view.flxToInternational.hoverSkin = "sknFlxffffffShadowdddcdc";
                this.view.flxWireTransfer.hoverSkin = "sknFlxffffffShadowdddcdc";
                this.view.flxArrow1.isVisible = false;
                this.view.flxArrow2.isVisible = false;
                this.view.flxArrow3.isVisible = false;
                this.view.flxArrow4.isVisible = false;
                this.view.flxArrow5.isVisible = false;
                this.view.forceLayout();
            }
            if (width === 1388) {
                this.view.lblTransfers.skin = "sknSSP42424220Px";
            }
            this.AdjustScreen();
        },
        setupFormOnTouchEnd: function(width) {
            if (width == 640) {
                this.view.onTouchEnd = function() {}
                this.nullifyPopupOnTouchStart();
            } else {
                if (width == 1024) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else {
                    this.view.onTouchEnd = function() {
                        hidePopups();
                    }
                }
                var userAgent = kony.os.deviceInfo().userAgent;
                if (userAgent.indexOf("iPad") != -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                }
            }
        },
        nullifyPopupOnTouchStart: function() {},
        /**showPendingAccounts  - Shows wether is there any pending account for verification
         * @param  {object} pendingAccounts Array of pending Accounts
         */
        showPendingAccounts: function(pendingAccounts) {
            var self = this;
            if (pendingAccounts.error === true) {
                this.view.flxVerifyAccount.setVisibility(false);
            } else {
                var count = 0;
                for (var i in pendingAccounts) {
                    if (pendingAccounts[i].isVerified !== "true") {
                        count += 1;
                        break;
                    }
                }
                if (count === 0) {
                    this.view.flxVerifyAccount.setVisibility(false);
                } else {
                    commonUtilities.setText(this.view.lblVerifyWarning, kony.i18n.getLocalizedString("i18n.transfers.verifyAccountCount"), commonUtilities.getaccessibilityConfig());
                    this.view.lblVerifyWarning.toolTip = kony.i18n.getLocalizedString("i18n.transfers.verifyAccountCount");
                    this.view.lblVerifyButton.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
                    commonUtilities.setText(this.view.lblVerifyButton, kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify"), commonUtilities.getaccessibilityConfig());
                    this.view.lblVerifyButton.onTouchEnd = function() {
                        var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                        transfersModule.presentationController.showExternalAccounts();
                    }
                    this.view.flxVerifyAccount.setVisibility(true);
                }
            }
        },
        /**Entry Point Method of Scheduled Tab
         */
        getUserScheduledTransactions: function() {
            this.first = 0;
            this.last = 10;
            var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transfersModule.presentationController.getScheduledUserTransactions();
            //this.view.transfermain.flxMainContainerTable.flxSegment.skin="slFbox";
        },
        /**Needs to be called when gateway need to be shown initially
         * @param  {object} gateway View Model containing from account number for preselection
         */
        updateGateWay: function(gateway) {
            this.showTransfersGateway();
            this.configureTransferGateway(gateway.overrideFromAccount);
        },
        /**Shows Transfer Gateway View
         */
        showTransfersGateway: function() {
            this.hideAll();
            this.view.flxTrasfersWindow.setVisibility(true);
            //this.view.flxNoAccounts.setVisibility(false);
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer")
            }]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
            //this.view.transfermain.btnMakeTransfer.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED_MODIFIED;
            this.view.flxTrasfersWindow.setVisibility(true);
            this.view.flxAddAccountWindow.setVisibility(true);
            this.view.flxChangeTransferType.setVisibility(false);
            this.view.forceLayout();
        },
        /**Configure Transfer Gateway using a config
         * @param  {array} overrideFromAccount AccountID for preselection
         */
        configureTransferGateway: function(overrideFromAccount) {
            var TransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            var transferGatewayConfig = [{
                    key: OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS,
                    info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
                    title: "i18n.transfers.toMyKonyBankAccounts",
                    displayName: KonyBankAccountsTransfer,
                    image: ViewConstants.IMAGES.KONYBANK_ACC
                },
                {
                    key: OLBConstants.TRANSFER_TYPES.OTHER_INTERNAL_MEMBER,
                    info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
                    title: "i18n.transfers.toOtherKonyBankAccounts",
                    displayName: OtherKonyAccountsTransfer,
                    image: ViewConstants.IMAGES.OTHERKONYBANK_ACC
                },
                {
                    key: OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT,
                    info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
                    title: "i18n.transfers.toOtherBankAccounts",
                    displayName: OtherBankAccountsTransfer,
                    image: ViewConstants.IMAGES.OTHERBANK_ACC
                },
                {
                    key: OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT,
                    info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
                    title: "i18n.transfers.toInternationalAccounts",
                    displayName: InternationalAccountsTransfer,
                    image: ViewConstants.IMAGES.INTERNATIONALBANK_ACC,
                    external: true
                },
                {
                    key: OLBConstants.TRANSFER_TYPES.WIRE_TRANSFER,
                    info: ["i18n.transfers.365days", "i18n.transfers.transferLimitWireTransfer"],
                    title: "i18n.transfers.wireTransfer",
                    displayName: [DomesticWireTransfer, InternationalWireTransfer],
                    image: ViewConstants.IMAGES.WIRE_TRANSFER_ACC
                }
            ]
            var self = this;
            var transferToViews = this.view.flxTransfersGateway.widgets();
            //var transferToViews = this.view.transfermain.flxTransfersGateway.widgets();
            function createCallback(config, visible) {
                if (config.key === CONFIG_WIRE_TRANSFER) {
                    return function() {
                        var WireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        WireTransferModule.presentationController.showWireTransfer();
                    }
                }
                return function() {
                    TransferModule.presentationController.loadAccountsByTransferType(config.key, null, overrideFromAccount);
                }
            }
            var visibleCounter = 0;
            for (var index = 0; index < transferGatewayConfig.length; index++) {
                var transferToView = transferToViews[visibleCounter + 1];
                var gatewayConfig = transferGatewayConfig[index];
                if (TransferModule.presentationController.isTransferTypeEnabled(gatewayConfig.key)) {
                    transferToView.setVisibility(true);
                    var infoWidgets = transferToView.widgets()[0].widgets();
                    infoWidgets[0].src = gatewayConfig.image;
                    infoWidgets[1].text = kony.i18n.getLocalizedString(gatewayConfig.title);
                    infoWidgets[1].accessibilityconfig = {
                        "a11yLabel": kony.i18n.getLocalizedString(gatewayConfig.title)
                    };
                    infoWidgets[2].text = kony.i18n.getLocalizedString(this.getDisplayDescription(gatewayConfig.displayName, gatewayConfig.key));
                    infoWidgets[2].accessibilityconfig = {
                        "a11yLabel": kony.i18n.getLocalizedString(this.getDisplayDescription(gatewayConfig.displayName, gatewayConfig.key))
                    };
                    var buttonProceed = transferToView.widgets()[1].widgets()[0];
                    var buttonProceed1 = transferToView.widgets()[2];
                    buttonProceed.onClick = createCallback(gatewayConfig);
                    buttonProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
                    buttonProceed1.onClick = createCallback(gatewayConfig);
                    transferToView.onClick = createCallback(gatewayConfig);
                    visibleCounter++;
                }
            }
            for (var i = visibleCounter + 1; i < transferToViews.length; i++) {
                var view = transferToViews[i];
                view.setVisibility(false);
            }

        },
        responsiveViews: {},
        initializeResponsiveViews: function() {
            this.responsiveViews["flxTransfersGateway"] = this.isViewVisible("flxTransfersGateway");
        },
        isViewVisible: function(container) {
            return this.view[container].isVisible;

        },
        /**getDisplayDescription : This function gets the display description to be displayed
         * @param  {String} DisplayName description to be displayed
         * @param  {String} gatewayConfigkey for which description is to be displayed
         */
        getDisplayDescription: function(displayName, gatewayConfigkey) {
            var description = "";
            var i18nKey = "";
            var servicesForUser = applicationManager.getConfigurationManager().getServicesListForUser();
            if (servicesForUser) {
                servicesForUser.forEach(function(dataItem) {
                    if (gatewayConfigkey === OLBConstants.TRANSFER_TYPES.WIRE_TRANSFER) {
                        if (dataItem.displayName === displayName[0] || dataItem.displayName === displayName[1]) {
                            description = !description ? dataItem.serviceDesc : description + " & " + dataItem.serviceDesc;
                        }
                    } else if (dataItem.displayName === displayName) {
                        description = dataItem.serviceDesc;
                    }
                });
                if (gatewayConfigkey === OLBConstants.TRANSFER_TYPES.WIRE_TRANSFER) {
                    i18nKey = "i18n.transfers.internationalAndDomesticWireTransferDescription";
                } else {
                    switch (displayName) {
                        case "KonyBankAccountsTransfer":
                            i18nKey = "i18n.transfers.toMyKonyBankAccountsDescription";
                            break;
                        case "OtherKonyAccountsTransfer":
                            i18nKey = "i18n.transfers.toOtherKonyBankAccountsDescription";
                            break;
                        case "OtherBankAccountsTransfer":
                            i18nKey = "i18n.transfers.toOtherBankAccountsDescription";
                            break;
                        case "InternationalAccountsTransfer":
                            i18nKey = "i18n.transfers.toInternationalAccountsDescription";
                            break;
                        default:
                            break;
                    }
                }
                var bundle = {};
                bundle[i18nKey] = description;
                kony.i18n.updateResourceBundle(bundle, "en");
            }
            return i18nKey;
        },


    }
});