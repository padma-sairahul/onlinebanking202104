define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnCancelKA **/
    AS_Button_ab4bde1d6283404f93f34a6576b18779: function AS_Button_ab4bde1d6283404f93f34a6576b18779(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for btnIntCancelKA **/
    AS_Button_fec0a2a2fa8540f79b4414f8ced07e14: function AS_Button_fec0a2a2fa8540f79b4414f8ced07e14(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for btnAddAccountKA **/
    AS_Button_he6cf577812b4291a5f7c29119deb4f8: function AS_Button_he6cf577812b4291a5f7c29119deb4f8(eventobject) {
        var self = this;
        this.addInternalAccount();
    },
    /** onClick defined for flxMainContainer **/
    AS_FlexContainer_ff3ad820daf64a91b4f1825347cfa174: function AS_FlexContainer_ff3ad820daf64a91b4f1825347cfa174(eventobject) {
        var self = this;
        //test
    },
    /** preShow defined for frmAddInternalAccount **/
    AS_Form_dad23327add24b55b3d7e1c346d61713: function AS_Form_dad23327add24b55b3d7e1c346d61713(eventobject) {
        var self = this;
        this.preshowFrmAddAccount();
    },
    /** postShow defined for frmAddInternalAccount **/
    AS_Form_db7d2c00709448119ad5e593e1219416: function AS_Form_db7d2c00709448119ad5e593e1219416(eventobject) {
        var self = this;
        this.postShowAddInternalAccount();
    },
    /** onDeviceBack defined for frmAddInternalAccount **/
    AS_Form_i0bb08defb9f4ff2a983b4e42e5a7093: function AS_Form_i0bb08defb9f4ff2a983b4e42e5a7093(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmAddInternalAccount **/
    AS_Form_j8f3e23d143d43cd9dc6bb231366f5d1: function AS_Form_j8f3e23d143d43cd9dc6bb231366f5d1(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onKeyUp defined for tbxAccountNickNameKA **/
    AS_TextField_fced5a9ed6fe4c73bb7cc5ac88cc1b8f: function AS_TextField_fced5a9ed6fe4c73bb7cc5ac88cc1b8f(eventobject) {
        var self = this;
        this.validateInternalError();
    },
    /** onKeyUp defined for tbxAccountNumberKA **/
    AS_TextField_fdf2472550634709a5f626bd266b2f42: function AS_TextField_fdf2472550634709a5f626bd266b2f42(eventobject) {
        var self = this;
        this.validateInternalError();
    },
    /** onKeyUp defined for tbxBeneficiaryNameKA **/
    AS_TextField_g0a9591ca3ec40c98998386e235ff800: function AS_TextField_g0a9591ca3ec40c98998386e235ff800(eventobject) {
        var self = this;
        this.validateInternalError();
    },
    /** onKeyUp defined for tbxAccountNumberAgainKA **/
    AS_TextField_g96e908cf8ed40ccb3f3ebc79df4c800: function AS_TextField_g96e908cf8ed40ccb3f3ebc79df4c800(eventobject) {
        var self = this;
        this.validateInternalError();
    }
});