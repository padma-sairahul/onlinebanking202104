define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnbacktopayeelist **/
    AS_Button_ca3b63cfaebb49329d8e4d7f2ab2a689: function AS_Button_ca3b63cfaebb49329d8e4d7f2ab2a689(eventobject) {
        var self = this;
        this.presenter.getExternalAccounts();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_f67a44d570bf4722b5071962ff199656: function AS_FlexContainer_f67a44d570bf4722b5071962ff199656(eventobject) {
        var self = this;
        //this.addExternalAccount();
        this.presenter.showDomesticAccounts();
    },
    /** preShow defined for frmTransfers **/
    AS_Form_b61534baeb4a46729f7a7c0bb24ffab8: function AS_Form_b61534baeb4a46729f7a7c0bb24ffab8(eventobject) {
        var self = this;
        this.initTabsActions();
    },
    /** onDeviceBack defined for frmTransfers **/
    AS_Form_e1bde3d3274840838ac1e5cb73816fa3: function AS_Form_e1bde3d3274840838ac1e5cb73816fa3(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** postShow defined for frmTransfers **/
    AS_Form_f61a533a94744161a486552a0668316d: function AS_Form_f61a533a94744161a486552a0668316d(eventobject) {
        var self = this;
        this.postShowtransfers();
    },
    /** init defined for frmTransfers **/
    AS_Form_j65feee81194476c97341e7afd896acc: function AS_Form_j65feee81194476c97341e7afd896acc(eventobject) {
        var self = this;
        this.initActions();
    },
    /** onTouchEnd defined for frmTransfers **/
    AS_Form_j82b124f81af40f0b31ef0ec43e42ebe: function AS_Form_j82b124f81af40f0b31ef0ec43e42ebe(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});