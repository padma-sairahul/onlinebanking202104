define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnNo **/
    AS_Button_a65d6139ac0e4d2088ce5adf0e4b7693: function AS_Button_a65d6139ac0e4d2088ce5adf0e4b7693(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** onClick defined for confirmButtons.btnCancel **/
    AS_Button_d366ebe6ea4343b585ce38de07a2574f: function AS_Button_d366ebe6ea4343b585ce38de07a2574f(eventobject) {
        var self = this;
        this.showCancelPopup();
    },
    /** onClick defined for confirmButtons.btnModify **/
    AS_Button_dbde2a0598bf421fb52da8a663db7e5c: function AS_Button_dbde2a0598bf421fb52da8a663db7e5c(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.modifyAccountInfo();
    },
    /** onClick defined for btnYes **/
    AS_Button_e2b952ec4b624bcab2244dbe0e685044: function AS_Button_e2b952ec4b624bcab2244dbe0e685044(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for confirmButtons.btnConfirm **/
    AS_Button_ea16de84649b4a0d880eeab903accae7: function AS_Button_ea16de84649b4a0d880eeab903accae7(eventobject) {
        var self = this;
        this.addAccount();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_j960ad1e5302489aa68eb1b4fb943c9e: function AS_FlexContainer_j960ad1e5302489aa68eb1b4fb943c9e(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** onDeviceBack defined for frmConfirmAccount **/
    AS_Form_b719e37e59a2412fbfafb4b56d17a5d9: function AS_Form_b719e37e59a2412fbfafb4b56d17a5d9(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmConfirmAccount **/
    AS_Form_fe632bf8494144e78431c3d7f0067409: function AS_Form_fe632bf8494144e78431c3d7f0067409(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** postShow defined for frmConfirmAccount **/
    AS_Form_hb11d89886164d6a8a5aa0a3a4b02ffe: function AS_Form_hb11d89886164d6a8a5aa0a3a4b02ffe(eventobject) {
        var self = this;
        this.postShowfrmConfirmAccount();
    },
    /** preShow defined for frmConfirmAccount **/
    AS_Form_j55a0e01009f45c2bacdc0a24f84d3bd: function AS_Form_j55a0e01009f45c2bacdc0a24f84d3bd(eventobject) {
        var self = this;
        this.preShowfrmConfirmAccount();
    }
});