define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for frmBulkTransferFiles **/
    AS_Form_d23569ec1fcb4633863478a08c6c7edc: function AS_Form_d23569ec1fcb4633863478a08c6c7edc(eventobject) {
        var self = this;
        self.preShow();
    },
    /** postShow defined for frmBulkTransferFiles **/
    AS_Form_ff629bd8a25549278138d00a9f1d3371: function AS_Form_ff629bd8a25549278138d00a9f1d3371(eventobject) {
        var self = this;
        self.postShow();
    },
    /** init defined for frmBulkTransferFiles **/
    AS_Form_h95fa7369986444eb70362f5ec79a0f4: function AS_Form_h95fa7369986444eb70362f5ec79a0f4(eventobject) {
        var self = this;
        this.init();
    }
});