define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxCheckbox **/
    AS_FlexContainer_d1d5c15f5192465d8a30c8bf92c11a99: function AS_FlexContainer_d1d5c15f5192465d8a30c8bf92c11a99(eventobject, context) {
        var self = this;
        this.toggleCheckBox();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_j818ebb666d34b95959696b7d50c1e0e: function AS_FlexContainer_j818ebb666d34b95959696b7d50c1e0e(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});