define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_i37dc9df41904f2e93f998803330696c: function AS_FlexContainer_i37dc9df41904f2e93f998803330696c(eventobject, context) {
        var self = this;
        this.segmentManagePayeesRowClick();
    },
    /** onClick defined for btnEbill **/
    AS_Button_h1725c213a244e6f85d95dfa3e34932c: function AS_Button_h1725c213a244e6f85d95dfa3e34932c(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnActions **/
    AS_Button_jf1f8a7f5e2844bbac4fe1f74c50af6d: function AS_Button_jf1f8a7f5e2844bbac4fe1f74c50af6d(eventobject, context) {
        var self = this;
        //Just adding comment to register action, will set in form controller
    }
});