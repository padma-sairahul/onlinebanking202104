define(['FormControllerUtility', 'CommonUtilities', 'CSRAssistUI', 'ViewConstants', 'OLBConstants'], function(FormControllerUtility, CommonUtilities, CSRAssistUI, ViewConstants, OLBConstants) {
    var responsiveUtils = new ResponsiveUtils();
    return /** @alias module: frmMessgaeDetailsMobileController */ {
        frmPreshow: function() {
            //  var scopeObj = this;
            this.setActions();
            this.hideReplyView();
            this.dismissAllPopups();
        },
        init: function() {
            this.view.preShow = this.frmPreshow;
            this.view.postShow = this.frmPostShow;
            this.view.onDeviceBack = function() {};
            this.view.onTouchEnd = this.hidePop();
            this.view.onBreakpointChange = this.onBreakpointChange;
        },
        frmPostShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.frame.height - this.view.flxFooter.frame.height + "dp";
            this.showView();
        },
        loadsMessagesNewModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MessagesModuleNew");
        },
        updateFormUI: function(context) {
            if (context.showProgressBar) {
                FormControllerUtility.showProgressBar(this.view);
            }
            if (context.hideProgressBar) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (context.showRequestsView) {
                this.showRequestsView(context.showRequestsView);
            }
            if (context.updateAlertDetailView) {
                this.updateAlertDetails(context)
            }
            if (context.updateMessageDetailView) {
                this.updateMessageDetails(context)
            }
            if (context.updateDeletedMessageDetailView) {
                this.updateDeletedMessageDetails(context)
            }
        },
        setActions: function() {
            var scopeObj = this;
            this.view.btnBackToAlerts.onClick = this.navToAlerts;
            this.view.btnDismiss.onClick = this.showDismissPopup;
            this.view.flxInfo.onClick = this.showHideInfo;
            this.view.AllForms.flxCross.onClick = this.hidePop;
            this.view.btnBackToMessages.onClick = this.navToMessages;
            this.view.flxDelete.onClick = this.showDeletePopup;
            this.view.btnBackToDeletedMessages.onClick = this.navToDeletedMessages;
            this.view.btnDeleteForever.onClick = this.deleteForEverMessage;
            this.view.btnReply.onClick = this.showReplyView;
            this.view.flxReplyImageAttachment.onClick = this.replyBrowseFiles.bind(this);
        },
        dismissAllPopups: function() {
            this.view.flxDialogs.isVisible = false;
            this.view.FlxDismiss.isVisible = false;
            this.view.flxLogout.isVisible = false;
            this.view.flxDeletePopup.isVisible = false;
            this.view.flxDeleteForEverPopup.isVisible = false;
        },
        showDismissPopup: function() {
            var scope = this;
            this.dismissAllPopups();
            this.view.flxDialogs.isVisible = true;
            this.view.FlxDismiss.isVisible = true;
            this.view.CustomPopup1.flxCross.onClick = function() {
                scope.view.FlxDismiss.isVisible = false;
                scope.view.flxDialogs.isVisible = false;
            };
            this.view.CustomPopup1.btnNo.onClick = function() {
                scope.view.FlxDismiss.isVisible = false;
                scope.view.flxDialogs.isVisible = false;
            };
        },
        showDeletePopup: function() {
            var scope = this;
            this.dismissAllPopups();
            this.view.flxDialogs.isVisible = true;
            this.view.flxDeletePopup.isVisible = true;
            this.view.CustomPopup2.flxCross.onClick = function() {
                scope.view.flxDeletePopup.isVisible = false;
                scope.view.flxDialogs.isVisible = false;
            };
            this.view.CustomPopup2.btnNo.onClick = function() {
                scope.view.flxDeletePopup.isVisible = false;
                scope.view.flxDialogs.isVisible = false;
            };
        },
        deleteForEverMessage: function() {
            var scope = this;
            this.dismissAllPopups();
            this.view.flxDialogs.isVisible = true;
            this.view.flxDeleteForEverPopup.isVisible = true;
            this.view.CustomPopup4.flxCross.onClick = function() {
                scope.view.flxDeleteForEverPopup.isVisible = false;
                scope.view.flxDialogs.isVisible = false;
                rest
            };
            this.view.CustomPopup4.btnNo.onClick = function() {
                scope.view.flxDeleteForEverPopup.isVisible = false;
                scope.view.flxDialogs.isVisible = false;
            };
            if (CommonUtilities.isCSRMode()) {
                FormControllerUtility.disableButtonActionForCSRMode();
                scope.view.btnDeleteForever.skin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                scope.showHardDeletePopup();
            }
        },
        showView: function() {
            var previousForm = kony.application.getPreviousForm().id;
            if (previousForm === "frmAlerts") {
                this.view.flxAlertsMobile.isVisible = true;
                this.view.flxMyMessageDetails.isVisible = false;
                this.view.flxDeletedMessageDetails.isVisible = false;
            }
            if (previousForm === "frmMyMessages") {
                this.view.flxAlertsMobile.isVisible = false;
                this.view.flxMyMessageDetails.isVisible = true;
                this.view.flxDeletedMessageDetails.isVisible = false;
            }
            if (previousForm === "frmDeletedMessage") {
                this.view.flxAlertsMobile.isVisible = false;
                this.view.flxMyMessageDetails.isVisible = false;
                this.view.flxDeletedMessageDetails.isVisible = true;
            }
        },
        updateAlertDetails: function(data) {
            var scopeObj = this;
            this.view.lblHeadingNotification.text = data.updateAlertDetailView.response.notificationSubject;
            this.view.rtxNotification.text = data.updateAlertDetailView.response.notificationText;
            this.view.lblDateAndTime.text = CommonUtilities.getDateAndTime(data.updateAlertDetailView.response.receivedDate);
            this.view.CustomPopup1.btnYes.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                scopeObj.closeDismissPopup();
                scopeObj.loadsMessagesNewModule().presentationController.dismissNotification(data.updateAlertDetailView.response.userNotificationId);
            };
            /**
             *Code to show interactive notifications based on configurations
             */
            if (applicationManager.getConfigurationManager().isInteractiveNotificationEnabled === "true") {
                if (data.notificationActionLink && data.notificationActionLink !== "") {
                    this.view.lblOfferLink.text = data.notificationActionLink;
                    this.view.lblOfferLink.isVisible = true;
                    var handCursor = document.querySelectorAll(("." + OLBConstants.SKINS.INTERACTIVE_LINK));
                    for (var i = 0; i < handCursor.length; i++) {
                        handCursor[i].style.cursor = "pointer";
                    }
                    var notificationActionLink = this.view.lblOfferLink.text;
                    if (notificationActionLink) {
                        notificationActionLink = notificationActionLink.trim().toUpperCase();
                    }
                    switch (notificationActionLink) {
                        case "PAY BILL":
                            this.view.lblOfferLink.onTouchEnd = function() {
                                scopeObj.loadsMessagesNewModule().presentationController.navigateToBillPay();
                            };
                            break;
                        case "SEND MONEY":
                            this.view.lblOfferLink.onTouchEnd = function() {
                                scopeObj.loadsMessagesNewModule().presentationController.navigateToSendMoney();
                            };
                            break;
                        case "VIEW MY ACCOUNT":
                            this.view.lblOfferLink.onTouchEnd = function() {
                                scopeObj.loadsMessagesNewModule().presentationController.navigateToViewMyAccount();
                            };
                            break;
                        case "TRANSFER MONEY":
                            this.view.lblOfferLink.onTouchEnd = function() {
                                scopeObj.loadsMessagesNewModule().presentationController.navigateToTransferPage();
                            };
                            break;
                        default:
                            this.view.lblOfferLink.onTouchEnd = function() {
                                window.open(data.notificationActionLink);
                            };
                            break;
                    }
                } else {
                    this.view.lblOfferLink.isVisible = false;
                }
            } else {
                this.view.lblOfferLink.isVisible = false;
            }
            data.imageURL = data.imageURL ? data.imageURL.trim() : "";
            if (data.imageURL !== "") {
                this.view.imgBanner.src = data.imageURL;
                this.view.imgBanner.isVisible = true;
            } else {
                this.view.imgBanner.isVisible = false;
            }

        },
        updateMessageDetails: function(context) {
            var self = this;
            var selectedRow = context.updateMessageDetailView.messages
            if (context) {
                this.view.segMessages.setData([]);
                var widgetDataMap = this.processMessagesDataMap();
                var message = this.processMessagesData(context.updateMessageDetailView.messages);
                this.view.segMessages.widgetDataMap = widgetDataMap;
                this.view.segMessages.setData(message);
                this.view.AllForms.setVisibility(false);
                if (CommonUtilities.isCSRMode()) {
                    this.view.CustomPopup2.btnYes.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                    this.view.CustomPopup2.btnYes.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                } else {
                    this.view.CustomPopup2.btnYes.onClick = function() {
                        self.dismissAllPopups();
                        FormControllerUtility.showProgressBar(self.view);
                        self.loadsMessagesNewModule().presentationController.softDeleteRequest(selectedRow[0].CustomerRequest_id);
                        self.loadsMessagesNewModule().presentationController.showMyMessagesPage();
                    };
                }
                this.view.forceLayout();
            }
        },
        /**
         * processMessagesDataMap :This function is used for binding the each individual messages Data to the Segment fields
         * @returns {object} data
         */
        processMessagesDataMap: function() {
            return {
                "flxDummy": "flxDummy",
                "flxMessage": "flxMessage",
                "flxNameDate": "flxNameDate",
                "imgToolTip": "imgToolTip",
                "imgUser": "imgUser",
                "lblDate": "lblDate",
                "lblDummy": "lblDummy",
                "rtxMessage": "rtxMessage",
                "flxDocAttachment1": "flxDocAttachment1",
                "flxAttachmentName1": "flxAttachmentName1",
                "imgPDF1": "imgPDF1",
                "lblDocName1": "lblDocName1",
                "lblSize1": "lblSize1",
                "flxVerticalMiniSeparator1": "flxVerticalMiniSeparator1",
                "imgRemoveAttachment1": "imgRemoveAttachment1",
                "flxRemoveAttachment1": "flxRemoveAttachment1",
                "flxDownloadAttachment1": "flxDownloadAttachment1",
                "imgDownloadAttachment1": "imgDownloadAttachment1",
                "flxDocAttachment2": "flxDocAttachment2",
                "imgPDF2": "imgPDF2",
                "lblDocName2": "lblDocName2",
                "lblSize2": "lblSize2",
                "imgRemoveAttachment2": "imgRemoveAttachment2",
                "flxRemoveAttachment2": "flxRemoveAttachment2",
                "flxDownloadAttachment2": "flxDownloadAttachment2",
                "imgDownloadAttachment2": "imgDownloadAttachment2",
                "flxDocAttachment3": "flxDocAttachment3",
                "imgPDF3": "imgPDF3",
                "lblDocName3": "lblDocName3",
                "lblSize3": "lblSize3",
                "flxRemoveAttachment3": "flxRemoveAttachment3",
                "imgRemoveAttachment3": "imgRemoveAttachment3",
                "flxDownloadAttachment3": "flxDownloadAttachment3",
                "imgDownloadAttachment3": "imgDownloadAttachment3",
                "flxDocAttachment4": "flxDocAttachment4",
                "imgPDF4": "imgPDF4",
                "lblDocName4": "lblDocName4",
                "lblSize4": "lblSize4",
                "flxRemoveAttachment4": "flxRemoveAttachment4",
                "imgRemoveAttachment4": "imgRemoveAttachment4",
                "flxDownloadAttachment4": "flxDownloadAttachment4",
                "imgDownloadAttachment4": "imgDownloadAttachment4",
                "flxDocAttachment5": "flxDocAttachment5",
                "imgPDF5": "imgPDF5",
                "lblDocName5": "lblDocName5",
                "lblSize5": "lblSize5",
                "flxRemoveAttachment5": "flxRemoveAttachment5",
                "imgRemoveAttachment5": "imgRemoveAttachment5",
                "flxDownloadAttachment5": "flxDownloadAttachment5",
                "imgDownloadAttachment5": "imgDownloadAttachment5",
                "lblUser": "lblUser",
                "lblRowSeperator": "lblRowSeperator",
            };
        },
        /**
         * processMessagesData :This function is used for setting the messages data to the segment
         * @param {data}  data consists of the array of the messages
         * @returns {object} data data
         */
        processMessagesData: function(data) {
            var self = this;
            var username = self.loadsMessagesNewModule().presentationController.getCurrentUserName();
            data = data.map(function(dataItem) {
                var totalAttachments = parseInt(dataItem.totalAttachments);
                var hasFirstAttachment = self.hasFirstAttachment(totalAttachments);
                var hasSecondAttachment = self.hasSecondAttachment(totalAttachments);
                var hasThirdAttachment = self.hasThirdAttachment(totalAttachments);
                var hasFourthAttachment = self.hasFourthAttachment(totalAttachments);
                var hasFifthAttachment = self.hasFifthAttachment(totalAttachments);
                return {
                    "requestId": dataItem.CustomerRequest_id,
                    "template": dataItem.createdby != username ? "flxMessagesLeft" : "flxMessagesRight",
                    "rtxMessage": dataItem.MessageDescription,
                    "lblDate": CommonUtilities.getDateAndTime(dataItem.lastmodifiedts),
                    "flxDocAttachment1": {
                        "isVisible": hasFirstAttachment ? true : false
                    },
                    "lblDocName1": hasFirstAttachment ? dataItem.attachments[0].Name : "",
                    "lblSize1": hasFirstAttachment ? '(' + dataItem.attachments[0].Size.trim() + "KB" + ')' : "",
                    "imgPDF1": hasFirstAttachment ? self.getImageByType(dataItem.attachments[0].type) : "pdf_image.png",
                    "flxRemoveAttachment1": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment1": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[0].media_Id;
                            var fileName = dataItem.attachments[0].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment1": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                    },
                    "flxDocAttachment2": {
                        "isVisible": hasSecondAttachment ? true : false
                    },
                    "lblDocName2": hasSecondAttachment ? dataItem.attachments[1].Name : "",
                    "lblSize2": hasSecondAttachment ? '(' + dataItem.attachments[1].Size.trim() + "KB" + ')' : "",
                    "imgPDF2": hasSecondAttachment ? self.getImageByType(dataItem.attachments[1].type) : "pdf_image.png",
                    "flxRemoveAttachment2": {
                        "isVisible": false
                    },
                    "lblUser": dataItem.createdby,
                    "lblRowSeperator": {
                        "isVisible": true
                    },
                    "flxDownloadAttachment2": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[1].media_Id;
                            var fileName = dataItem.attachments[1].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment2": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "flxDocAttachment3": {
                        "isVisible": hasThirdAttachment ? true : false
                    },
                    "lblDocName3": hasThirdAttachment ? dataItem.attachments[2].Name : "",
                    "lblSize3": hasThirdAttachment ? '(' + dataItem.attachments[2].Size.trim() + "KB" + ')' : "",
                    "imgPDF3": hasThirdAttachment ? self.getImageByType(dataItem.attachments[2].type) : "pdf_image.png",
                    "flxRemoveAttachment3": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment3": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[2].media_Id;
                            var fileName = dataItem.attachments[2].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment3": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "flxDocAttachment4": {
                        "isVisible": hasFourthAttachment ? true : false
                    },
                    "lblDocName4": hasFourthAttachment ? dataItem.attachments[3].Name : "",
                    "lblSize4": hasFourthAttachment ? '(' + dataItem.attachments[3].Size.trim() + "KB" + ')' : "",
                    "imgPDF4": hasFourthAttachment ? self.getImageByType(dataItem.attachments[3].type) : "pdf_image.png",
                    "flxRemoveAttachment4": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment4": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[3].media_Id;
                            var fileName = dataItem.attachments[3].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment4": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "flxDocAttachment5": {
                        "isVisible": hasFifthAttachment ? true : false
                    },
                    "lblDocName5": hasFifthAttachment ? dataItem.attachments[4].Name : "",
                    "lblSize5": hasFifthAttachment ? '(' + dataItem.attachments[4].Size.trim() + "KB" + ')' : "",
                    "imgPDF5": hasFifthAttachment ? self.getImageByType(dataItem.attachments[4].type) : "pdf_image.png",
                    "flxRemoveAttachment5": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment5": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[4].media_Id;
                            var fileName = dataItem.attachments[4].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment5": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "imgUser": dataItem.createdby != username ? ViewConstants.IMAGES.BANK_CIRCLE_ICON_MOD : ViewConstants.IMAGES.USER_IMAGE,
                    "imgToolTip": dataItem.createdby != username ? ViewConstants.IMAGES.REPLY_ARROWTIP : ViewConstants.IMAGES.REPLY_ARROWTIP_RIGHT
                };
            });
            return data;
        },
        /**
         * hasFirstAttachment :This function is used to check whether there is first Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no first Attachment
         */
        hasFirstAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 0 ? true : false;
            return has;
        },
        /**
         * hasSecondAttachment :This function is used to check whether there is second Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no second Attachment
         */
        hasSecondAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 1 ? true : false;
            return has;
        },
        /**
         * hasThirdAttachment :This function is used to check whether there is third Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no third Attachment
         */
        hasThirdAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 2 ? true : false;
            return has;
        },
        /**
         * hasFourthAttachment :This function is used to check whether there is fourth Attachment or not
         * @param {totalAttachments}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no fourth Attachment
         */
        hasFourthAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 3 ? true : false;
            return has;
        },
        /**
         * hasFifthAttachment :This function is used to check whether there is fifth Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no fifth Attachment
         */
        hasFifthAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 4 ? true : false;
            return has;
        },
        /**
         * showReplyView :This function is used to show the reply view once we click on Reply button
         */
        showReplyView: function() {
            var self = this;
            this.view.txtAreaReply.text = "";
            FormControllerUtility.disableButton(this.view.btnSendReply);
            this.view.lblWarningNotMoreThan5files.setVisibility(false);
            this.view.flxReplyButton.isVisible = false;
            this.view.flxReply.isVisible = true;
            this.view.txtAreaReply.onKeyUp = this.enableSendReplyButton.bind(this);
            this.view.segAttachment.isVisible = true;
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                //         this.view.flxReplyMessageButtons.isVisible = true;
                //         this.view.flxReplyImageAttachment.onClick = this.replyBrowseFiles.bind(this);
                //         this.view.lblWarningNotMoreThan5files.text = "";
                //         this.view.flxReplyImageAttachment.setVisibility(false);
                //         this.view.lblAttatchment.setVisibility(false);
                //Nothing
            }
            this.view.txtAreaReply.text = "";
            this.fileObject = [];
            this.view.segAttachment.data = [];
            this.view.btnCancelReply.onClick = this.hideReplyView.bind(this);
            this.view.btnSendReply.onClick = function() {
                //if(!self.view.segMessages.data[0])return;
                if (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile)
                    self.view.lblWarningNotMoreThan5files.text = "";
                FormControllerUtility.showProgressBar(self.view);
                var requestParam = {
                    "files": self.fileObject,
                    "requestid": self.view.segMessages.data[0].requestId,
                    "description": self.view.txtAreaReply.text
                };
                self.loadsMessagesNewModule().presentationController.createNewRequestOrMessage(requestParam);
            };
            this.view.forceLayout();
            //this.view.flxReplyHeader.setFocus(true);
            this.view.txtAreaReply.setFocus(true);
        },
        /**
         * enableSendReplyButton :This function is used to enable/Disable the reply button while replying to the existing request
         */
        enableSendReplyButton: function() {
            if (this.view.txtAreaReply.text.trim() === "" || CommonUtilities.isCSRMode()) {
                FormControllerUtility.disableButton(this.view.btnSendReply);
            } else {
                FormControllerUtility.enableButton(this.view.btnSendReply);
            }
        },
        /**
         * hideReplyView :This function is used to hide the reply view once we click on ReplyCancel button
         */
        hideReplyView: function() {
            this.view.flxReplyButton.isVisible = true;
            this.view.flxReply.isVisible = false;
            FormControllerUtility.enableButton(this.view.btnSendReply);
            this.view.flxReply.isVisible = false;
            if (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) {
                this.view.flxReplyMessageButtons.isVisible = false;
            }
            this.view.btnSendReply.onClick = this.showReplyView.bind(this);
            this.view.flxReplyButton.isVisible = true;
            this.view.forceLayout();
        },
        /**
         * replyBrowseFiles :This function displays the error message when more than five files are attached or invokes the konyAPI to browse the files
         */
        replyBrowseFiles: function() {
            var config = this.getBrowseFilesConfig();
            this.view.lblWarningNotMoreThan5files.setVisibility(false);
            kony.io.FileSystem.browse(config, this.replyBrowseFilesCallback);
        },
        /**
         * getBrowseFilesConfig :This function is used to get the browser files config like which type of files to be uploaded
         * @return {config} returns the documents attached
         */
        getBrowseFilesConfig: function() {
            var config = {
                selectMultipleFiles: true,
                filter: ["image/png", "application/msword", "image/jpeg", "application/pdf", "text/plain", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"]
            };
            return config;
        },
        /**
         * browseFilesCallback :This function executes once the files are uploded so that  the uploaded files are binded to the Segment while creating a new Message
         * @param {object}  event event
         * @param {object}   files is the data of the files which are uploaded
         */
        browseFilesCallback: function(event, files) {
            this.bindFilesToSegment(files, this.view.segAttachment, this.view.lblWarningNewMessage);
        },
        updateDeletedMessageDetails: function(response) {
            var self = this;
            var selectedRow = response.updateDeletedMessageDetailView.messages;
            var widgetDataMap = this.processMessagesDataMap();
            var message = this.processMessagesData(response.updateDeletedMessageDetailView.messages);
            this.view.segDeletedMessages.widgetDataMap = widgetDataMap;
            this.view.segDeletedMessages.setData(message);
            this.view.AllForms.setVisibility(false);
            this.view.flxContainer.parent.forceLayout();
            if (CommonUtilities.isCSRMode()) {
                this.view.btnRestore.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.btnRestore.skin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.btnRestore.onClick = function() {
                    FormControllerUtility.showProgressBar(self.view);
                    self.loadsMessagesNewModule().presentationController.restoreRequest(selectedRow[0].CustomerRequest_id);
                    self.loadsMessagesNewModule().presentationController.showDeletedMessagesPage();
                };
            }
            if (CommonUtilities.isCSRMode()) {
                this.view.CustomPopup4.btnYes.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.CustomPopup4.btnYes.skin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.CustomPopup4.btnYes.onClick = function() {
                    self.closeDismissPopup();
                    self.navToDeletedMessages();
                    FormControllerUtility.showProgressBar(self.view);
                    self.loadsMessagesNewModule().presentationController.hardDeleteRequest(selectedRow[0].CustomerRequest_id);
                    self.loadsMessagesNewModule().presentationController.showDeletedMessagesPage();
                };
            }
        },
        navToAlerts: function() {
            this.loadsMessagesNewModule().presentationController.showAlertsPage();
        },
        showHideInfo: function() {
            var self = this;
            if (self.view.AllForms.isVisible) {
                self.view.AllForms.isVisible = false;
            } else {
                self.view.AllForms.isVisible = true;
            }
        },
        hidePop: function() {
            this.view.AllForms.isVisible = false;
        },
        navToMessages: function() {
            this.loadsMessagesNewModule().presentationController.showMyMessagesPage();
        },
        deleteMessage: function() {
            this.view.flxDialogs.isVisible = true;
            this.view.flxLogout.isVisible = false;
            this.view.FlxDismiss.isVisible = true;
        },
        showHardDeletePopup: function() {

        },
        closeDismissPopup: function() {
            this.view.flxDialogs.isVisible = false;
            this.view.flxLogout.isVisible = false;
            this.view.FlxDismiss.isVisible = false;
        },
        yesOnClick: function() {
            // change to something else like a flag
            if (this.view.CustomPopup1.lblHeading.text === "") {

            } else {

            }
        },
        hideDismissPopup: function() {
            this.view.flxDialogs.isVisible = false;
            this.view.FlxDismiss.isVisible = false;
            this.view.AllForms.isVisible = false;
        },
        navToDeletedMessages: function() {
            this.loadsMessagesNewModule().presentationController.showDeletedMessagesPage();
        },
        onBreakpointChange: function(eventObj, width) {
            var previousForm = kony.application.getPreviousForm().id;
            var scope = this;
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            if (width !== 640) {
                if (previousForm === "frmAlerts") {
                    this.navToAlerts();
                } else if (previousForm === "frmMyMessages") {
                    this.navToMessages();
                } else if (previousForm === "frmDeletedMessage") {
                    this.navToDeletedMessages();
                }
            }
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.CustomPopup1.onBreakpointChangeComponent(scope.view.CustomPopup1, width);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },
    };
});