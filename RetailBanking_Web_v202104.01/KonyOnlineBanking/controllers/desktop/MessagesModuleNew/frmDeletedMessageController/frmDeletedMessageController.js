define(['commonUtilities', 'FormControllerUtility', 'OLBConstants', 'ViewConstants'], function(commonUtilities, FormControllerUtility, OLBConstants, ViewConstants) {
    var responsiveUtils = new ResponsiveUtils();
    return /** @alias module:frmNotificationsAndMessagesController */ {
        isDeletedTab: false,
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxFormContent.doLayout = this.setMinHeight;
        },
        preShow: function() {
            this.setActions();
            this.view.lblRequestIdValue.text = "";
            this.view.lblCategoryValue.text = "";
            this.view.lblCategoryValue2.text = "";
        },
        postShow: function() {
            this.setsetMinHeight;
        },
        setMinHeight: function() {
            if (this.screenHeightGbl === kony.os.deviceInfo().screenHeight) {
                return;
            }
            this.screenHeightGbl = kony.os.deviceInfo().screenHeight;
            var minHeightForMsg = 0;
            // screenheight - headerheight - footer height - address top - address bottom
            if (responsiveUtils.isMobile || kony.application.getCurrentBreakpoint() === 640) {
                this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - 200 + "dp";
            } else {
                this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - 270 + "dp"; //270 = header 120 + footer 150
                minHeightForMsg = kony.os.deviceInfo().screenHeight - 270;
                if (minHeightForMsg < 700) {
                    minHeightForMsg = 700;
                }
                this.view.flxContainer.minHeight = minHeightForMsg + "dp";
            }
        },
        updateFormUI: function(context) {
            if (context.showProgressBar) {
                FormControllerUtility.showProgressBar(this.view);
            }
            if (context.hideProgressBar) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (context.sideMenu) {
                this.updateHamburgerMenu(context.sideMenu);
            }
            if (context.showMessagesView) {
                this.showMessagesView(context.showMessagesView);
            }
            if (context.showDeletedRequestsView) {
                this.showDeletedRequestsView(context.showDeletedRequestsView);
            }
            if (context.showSearchDeletedRequests) {
                this.showSearchDeletedRequestsView(context.showSearchDeletedRequests);
            }
            if (context.searchRequestsView) {
                this.showSearchRequestsView(context.searchRequestsView);
            }
        },
        loadsMessagesNewModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MessagesModuleNew");
        },
        setActions: function() {
            var scopeObj = this;
            this.view.btnAlerts.onClick = this.navToAlerts;
            this.view.btnMyMessages.onClick = this.navToMessages;
            this.view.btnNewMessage.onClick = this.navToNewMessage;
            this.view.txtSearch.onKeyUp = this.showOrHideSearchCrossImage.bind(this);
            this.view.txtSearch.onDone = this.OnMessageDeleteSearchClick.bind(this);
            this.view.flxSearchIcon.onClick = this.OnMessageSearchClick;
            this.view.lblSearch.onTouchEnd = this.OnMessageDeleteSearchClick.bind(this);
            this.view.flxClearSearch.onClick = this.OnMessageDeleteSearchClick.bind(this, true);
            if (commonUtilities.isCSRMode()) {
                this.view.btnDeleteForever.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.btnDeleteForever.skin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.btnDeleteForever.onClick = function() {
                    scopeObj.showHardDeletePopup();
                };
            }
        },
        navToAlerts: function() {
            this.loadsMessagesNewModule().presentationController.showAlertsPage();
        },
        navToMessages: function() {
            this.loadsMessagesNewModule().presentationController.showMyMessagesPage({
                show: "Messages"
            });
        },
        navToNewMessage: function() {
            this.loadsMessagesNewModule().presentationController.showNewMessagePage();
        },
        /**
         * showHardDeletePopup :This function is used to show the HardDelete PopUp when clicked on Delete button for a message in DeletedMessages tab
         */
        showHardDeletePopup: function() {
            this.view.flxDialogs.isVisible = true;
            this.view.flxLogout.isVisible = false;
            this.view.FlxDismiss.isVisible = true;
        },
        /**
         * showOrHideSearchCrossImage
         */
        showOrHideSearchCrossImage: function() {
            var searchString = this.view.txtSearch.text;
            if (searchString && searchString.trim()) {
                this.view.flxClearSearch.setVisibility(true);
            } else {
                this.view.flxClearSearch.setVisibility(false);
            }
            this.view.forceLayout();
        },
        /**
         * This function is executed when all  the DeletedMessages  are retrieved
         * @param {object}  deletedRequestData - is a JSON which consists of array of all the DeletedMessages
         */
        showDeletedRequestsView: function(deletedRequestData) {
            var scopeObj = this;
            var searchString = this.view.txtSearch.text.trim();
            if (searchString === "") {
                scopeObj.setDeletedMessagesSegmentData(deletedRequestData);
            } else {
                scopeObj.OnMessageDeleteSearchClick();
            }
        },
        /**
         * This function is used to bind the search Result of the Requests to the Segment
         *@param{object} searchResult-is a JSON which  contains data of the Requests according to the Search String
         */
        showSearchRequestsView: function(searchResult) {
            var requests = searchResult.requests;
            if (requests.length === 0) {
                this.view.rtxNoSearchResults.text = kony.i18n.getLocalizedString("i18n.LocateUs.NosearchresultfoundPleasechangethesearchcriteria");
                this.view.flxNoSearchResult.isVisible = true;
                this.view.lblAccountRenewal.text = "";
                this.view.lblRequestIdValue.text = "";
                this.view.lblCategoryValue.text = "";
                this.view.lblCategoryValue2.text = "";
                this.view.segMessages.setData([]);
                this.view.segMessageAndNotification.setData([]);
                this.view.flxDelete.isVisible = false;
                this.view.btnSendReply.isVisible = false;
                this.view.btnCancelReply.setVisibility(false);
                this.view.lblWarningNotMoreThan5files.setVisibility(false);
                this.view.flxReplyImageAttachment.setVisibility(false);
                this.view.segMessageAndNotification.isVisible = false;
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.unreadMessagesCount = parseInt(searchResult.unreadSearchMessagesCount);
                this.view.flxDelete.isVisible = true;
                this.view.flxNoSearchResult.isVisible = false;
                this.view.segMessageAndNotification.isVisible = true;
                this.view.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
                this.setMessagesSegmentData(searchResult);
            }
        },
        OnMessageSearchClick: function(clearSearch) {
            FormControllerUtility.showProgressBar(this.view);
            var searchString = this.view.txtSearch.text;
            if (clearSearch === true) {
                this.view.txtSearch.text = "";
                this.view.lblSearch.setVisibility(true);
                this.loadsMessagesNewModule().presentationController.showRequests();
                this.showOrHideSearchCrossImage();
            } else if (searchString && searchString.trim()) {
                this.view.lblSearch.setVisibility(true);
                this.view.flxClearSearch.setVisibility(false);
                this.loadsMessagesNewModule().presentationController.searchRequest(searchString);
                this.view.txtSearch.text = searchString;
                this.showOrHideSearchCrossImage();
            } else {
                this.loadsMessagesNewModule().presentationController.showRequests();
            }
        },
        /**
         * This function is executed when all  the messages for the request are retrieved
         * @param {object}  messagesData - is a JSON which consists of array of all the messsages for the particular request
         */
        showMessagesView: function(messagesData) {
            var widgetDataMap = this.processMessagesDataMap();
            var message = this.processMessagesData(messagesData.messages);
            this.view.segMessages.widgetDataMap = widgetDataMap;
            this.view.segMessages.setData(message);
            this.view.AllForms.setVisibility(false);
            this.view.flxContainer.parent.forceLayout();
        },
        /**
         * getImageByType :This function is used to get the image depending on the type of the document attached
         * @param {type}  type of the Attachment can be application/pdf, text/plain, image/jpeg, application/msword(DOC) or application/vnd.openxmlformats-officedocument.wordprocessingml.document(DOCX)
         * @return {image} returns the appropriate image based on type of the document
         */
        getImageByType: function(type) {
            var image;
            switch (type) {
                case "application/pdf":
                    image = ViewConstants.IMAGES.PDF_IMAGE;
                    break;
                case "text/plain":
                    image = ViewConstants.IMAGES.TXT_IMAGE;
                    break;
                case "image/jpeg":
                    image = ViewConstants.IMAGES.JPEG_IMAGE;
                    break;
                case "application/msword":
                    image = ViewConstants.IMAGES.DOC_IMAGE;
                    break;
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    image = ViewConstants.IMAGES.DOCX_IMAGE;
                    break;
                case "image/png":
                    image = ViewConstants.IMAGES.PNG_IMAGE;
                    break;
            }
            return image;
        },
        /**
         * processRequestsDataMap :This function is used for binding the each individual Request Data to the Segment fields
         * @returns {object} widget
         */
        processRequestsDataMap: function() {
            return {
                "flxNotificationsAndMessages": "flxNotificationsAndMessages",
                "imgAttachment": "imgAttachment",
                "imgBulletIcon": "imgBulletIcon",
                "imgCurrentOne": "imgCurrentOne",
                "lblDateAndTime": "lblDateAndTime",
                "lblSegDescription": "lblSegDescription",
                "lblSegHeading": "lblSegHeading",
                "lblRowSeperator": "lblRowSeperator",
                "flxArrow": "flxArrow",
                "segNotificationsAndMessages": "segNotificationsAndMessages",
                "lblRequestIdValue": "lblRequestIdValue",
                "lblCategoryValue": "lblCategoryValue"
            };
        },
        /**
         * processRequestsData :This function is used for assigning  individual Request Data to the Segment
         * @param {data}  data consists of all the details of the  requests which are to be mapped
         * @return {object} data
         */
        processRequestsData: function(data) {
            var self = this;
            if (!data) {
                return [];
            }
            data = data.map(function(dataItem) {
                return {
                    "imgCurrentOne": {
                        "src": ViewConstants.IMAGES.ACCOUNTS_SIDEBAR_BLUE,
                        "isVisible": false
                    },
                    "imgAttachment": { //if it has attachments depending on the data
                        "src": ViewConstants.IMAGES.ATTACHMENT_GREY,
                        "isVisible": dataItem.totalAttachments > 0 ? true : false
                    },
                    "imgBulletIcon": {
                        "isVisible": self.isReadMessage(dataItem.unreadmsgs)
                    },
                    "lblRowSeperator": {
                        "isVisible": true
                    },
                    "lblArrow": {
                        "skin": "sknLblrightArrowFontIcon0273E3",
                        "isVisible": true
                    },
                    "lblDateAndTime": {
                        "text": dataItem.recentMsgDate ? commonUtilities.getDateAndTime(dataItem.recentMsgDate) : ""
                    },
                    "lblSegDescription": {
                        "text": self.labelData(dataItem.firstMessage) ? self.labelData(dataItem.firstMessage) : ""
                    },
                    "flxNotificationsAndMessages": {
                        "skin": "sknFlxffffff",
                        "hoverSkin": ViewConstants.SKINS.SKNFLXF7F7F7
                    },
                    "lblSegHeading": {
                        "text": dataItem.unreadmsgs > 0 ? dataItem.requestsubject + " (" + dataItem.unreadmsgs + ")" : dataItem.requestsubject
                    },
                    "lblRequestIdValue": {
                        "text": dataItem.id
                    },
                    "lblCategoryValue": {
                        "text": dataItem.requestcategory_id
                    },
                    "requestId": dataItem.id,
                    "softdeleteflag": dataItem.softdeleteflag
                };
            });
            return data;
        },
        /**
         * OnMessageDeleteSearchClick :This function is executed on entering any search String in the DeletedMessages Tab and click of "Go" or "Enter" and based on the search String the Requests are displayed.The search happens based on the subject field
         * @param {boolean} clearSearch clear Search
         */
        OnMessageDeleteSearchClick: function(clearSearch) {
            FormControllerUtility.showProgressBar(this.view);
            var searchString = this.view.txtSearch.text;
            if (clearSearch === true) {
                this.view.txtSearch.text = "";
                this.view.lblSearch.setVisibility(true);
                this.view.flxClearSearch.setVisibility(false);
                this.loadsMessagesNewModule().presentationController.showDeletedRequests();
                this.showOrHideSearchCrossImage();
            } else if (searchString && searchString.trim()) {
                this.view.lblSearch.setVisibility(false);
                this.view.flxClearSearch.setVisibility(true);
                this.loadsMessagesNewModule().presentationController.searchDeletedRequests(searchString);
                this.view.txtSearch.text = searchString;
                this.showOrHideSearchCrossImage();
            } else {
                this.loadsMessagesNewModule().presentationController.showDeletedRequests();
            }
        },
        /**
         * This function is used to bind the Deleted Messaages to the Segment
         * @param {object}  deletedRequestData -  is a JSON which consists of array of all the DeletedMessages
         */
        setDeletedMessagesSegmentData: function(deletedRequestData) {
            var deletedRequests = deletedRequestData.deletedRequests;
            var self = this;
            this.view.flxNoSearchResult.setVisibility(false);
            this.view.segMessageAndNotification.setVisibility(true);
            if (deletedRequests && deletedRequests.length > 0) {
                this.view.segMessageAndNotification.onRowClick = this.onMessageRowSelection.bind(this);
                var dataMap = self.processRequestsDataMap();
                deletedRequests = self.processRequestsData(deletedRequests);
                this.view.segMessageAndNotification.widgetDataMap = dataMap;
                this.view.segMessageAndNotification.setData(deletedRequests);
                this.isDeletedTab = true;
                this.onMessageRowSelection();
                this.view.flxRightMessages.isVisible = true;
            } else {
                this.view.flxRightMessages.isVisible = false;
                if (deletedRequestData.errorMsg) {
                    this.view.rtxNoSearchResults.text = deletedRequestData.errorMsg;
                } else {
                    this.view.rtxNoSearchResults.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.noRecords");
                }
                this.view.flxNoSearchResult.setVisibility(true);
                this.view.segMessageAndNotification.setVisibility(false);
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                this.view.flxRightMessages.isVisible = false;
            }
            this.view.flxContainer.parent.forceLayout();
        },
        /**
         * isReadMessage :This function checks whether there are any unread Messages
         * @param {boolean} status indicates the number of unread messages
         * @returns {boolean} status
         */
        isReadMessage: function(status) {
            if (status > 0) {
                return true;
            } else {
                return false;
            }
        },
        /**
         * labelData :This function is used to convert the html Data to normal String by removing the tags and assigning the text to a label
         * @param {object} data  html Data
         *@ return {text} normal text after converting from html to text
         */
        labelData: function(data) {
            var parser = new DOMParser();
            var res = parser.parseFromString(data, 'text/html');
            var html = res.body.textContent;
            var div = document.createElement("div");
            div.innerHTML = html;
            return div.innerText;
        },
        /**
         * This function is used to bind the search Result of the Requests to the Segment
         * @param {object} searchResults - is a JSON which  contains data of the Requests according to the Search String
         */
        showSearchDeletedRequestsView: function(searchResults) {
            var requests = searchResults.deletedRequests;
            if (requests.length === 0) {
                this.view.segMessageAndNotification.setVisibility(false);
                this.view.flxRightMessages.setVisibility(false);
                this.view.rtxNoSearchResults.text = kony.i18n.getLocalizedString("i18n.LocateUs.NosearchresultfoundPleasechangethesearchcriteria");
                this.view.lblAccountRenewal.text = "";
                this.view.lblRequestIdValue.text = "";
                this.view.lblCategoryValue.text = "";
                this.view.lblCategoryValue2.text = "";
                this.view.segMessages.setData([]);
                this.view.flxNoSearchResult.setVisibility(true);
            } else {
                this.view.flxNoSearchResult.setVisibility(false);
                this.setDeletedMessagesSegmentData(searchResults);
            }
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * This function is executed when we want to display the complete details of the Request selected
         */
        onMessageRowSelection: function() {
            var self = this;
            var isSegClicked = false;
            FormControllerUtility.showProgressBar(self.view);
            this.view.lblAccountRenewal.text = "";
            this.view.lblRequestIdValue.text = "";
            this.view.lblCategoryValue.text = "";
            this.view.lblCategoryValue2.text = "";
            this.view.segMessages.setData([]);
            var index = 0;
            var data = this.view.segMessageAndNotification.data;
            if (this.view.segMessageAndNotification.selectedRowItems.length) {
                isSegClicked = true;
                index = this.view.segMessageAndNotification.selectedRowIndex[1];
            }
            var selectedRow = data[index];
            if (!data[0].requestId) {
                data.splice(0, 1);
                index = index - 1;
                if (index === -1) {
                    index = 0;
                    selectedRow = data[0];
                    if (!selectedRow) {
                        //If there are no messages
                        this.hideCreateMessageView();
                        FormControllerUtility.hideProgressBar(this.view);
                        return;
                    }
                }
                this.view.segMessageAndNotification.setData(data);
            }
            var response = this.loadsMessagesNewModule().presentationController.getRequestsDetails(selectedRow.requestId, this.isDeletedTab);
            var prevIndex = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].imgCurrentOne.isVisible) {
                    prevIndex = i;
                }
                data[i].flxNotificationsAndMessages = {
                    "skin": ViewConstants.SKINS.SKNFLXFFFFFF
                };
                data[i].imgCurrentOne.isVisible = false;
                data[i].imgBulletIcon.isVisible = false;
            }
            data[index].flxNotificationsAndMessages = {
                "skin": ViewConstants.SKINS.SKNFLXF7F7F7
            };
            data[index].lblArrow.isVisible = true;
            data[index].imgCurrentOne.isVisible = true;
            if (response && this.isReadMessage(response.unreadmsgs)) {
                var subject = data[index].lblSegHeading.text;
                data[index].lblSegHeading.text = subject.lastIndexOf(" \(") >= 0 ? subject.substr(0, subject.indexOf(" \(")) : subject;
                this.loadsMessagesNewModule().presentationController.updateMessageAsRead(data[index].requestId);
            }
            this.view.segMessageAndNotification.setDataAt(data[index], index);
            this.view.segMessageAndNotification.setDataAt(data[prevIndex], prevIndex);
            this.view.lblAccountRenewal.text = data[index].lblSegHeading.text;
            this.view.lblRequestIdValue.text = data[index].lblRequestIdValue.text;
            this.view.lblCategoryValue.text = data[index].lblCategoryValue.text;
            this.view.lblCategoryValue2.text = data[index].lblCategoryValue.text;
            if (selectedRow.softdeleteflag === "true") {
                if (commonUtilities.isCSRMode()) {
                    this.view.btnRestore.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                    this.view.btnRestore.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                } else {
                    this.view.btnRestore.onClick = function() {
                        FormControllerUtility.showProgressBar(self.view);
                        self.loadsMessagesNewModule().presentationController.restoreRequest(selectedRow.requestId);
                    };
                }
                if (commonUtilities.isCSRMode()) {
                    this.view.CustomPopup1.btnYes.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                    this.view.CustomPopup1.btnYes.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                } else {
                    this.view.CustomPopup1.btnYes.onClick = function() {
                        self.closeDismissPopup();
                        FormControllerUtility.showProgressBar(self.view);
                        self.loadsMessagesNewModule().presentationController.hardDeleteRequest(selectedRow.requestId);
                        self.loadsMessagesNewModule().presentationController.showDeletedRequests();
                    };
                }
            } else {
                if (commonUtilities.isCSRMode()) {
                    this.view.CustomPopup1.btnYes.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                    this.view.CustomPopup1.btnYes.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                } else {
                    this.view.CustomPopup1.btnYes.onClick = function() {
                        self.closeDismissPopup();
                        FormControllerUtility.showProgressBar(self.view);
                        self.loadsMessagesNewModule().presentationController.softDeleteRequest(selectedRow.requestId);
                    };
                }
            }
            this.view.CustomPopup1.btnNo.onClick = function() {
                self.closeDismissPopup();
            };
            this.view.CustomPopup1.flxCross.onClick = function() {
                self.closeDismissPopup();
            };
            var break_point = kony.application.getCurrentBreakpoint();
            if (break_point == 640 || responsiveUtils.isMobile) {
                this.view.flxRightMessages.isVisible = false;
                if (this.deletedflag == 1) {
                    this.view.segMessageAndNotification.setVisibility(true);
                    this.view.flxRightMessages.setVisibility(false);
                    this.deletedflag = 0;
                }
                if (isSegClicked) {
                    this.navToMessagesDetailsMobile({
                        show: "DeletedMessageDetail"
                    }, response);
                    this.loadsMessagesNewModule().presentationController.showDeletedMessages(selectedRow.requestId);
                }
            } else {
                this.loadsMessagesNewModule().presentationController.showDeletedMessages(selectedRow.requestId);
                this.view.segMessageAndNotification.setVisibility(true);
            }
            FormControllerUtility.hideProgressBar(self.view);
        },
        navToMessagesDetailsMobile: function(context, response) {
            this.loadsMessagesNewModule().presentationController.showMessageDetailsMobilePage(context, response);
        },
        /**
         * closeDismissPopup :This function is used to close the Dismiss Pop Up when clicked on "No" button in the Dismiss PopUp
         */
        closeDismissPopup: function() {
            this.view.FlxDismiss.setVisibility(false);
            this.view.flxDialogs.isVisible = false;
            this.view.flxLogout.isVisible = false;
        },
        /**
         * processMessagesDataMap :This function is used for binding the each individual messages Data to the Segment fields
         * @returns {object} data
         */
        processMessagesDataMap: function() {
            return {
                "flxDummy": "flxDummy",
                "flxMessage": "flxMessage",
                "flxNameDate": "flxNameDate",
                "imgToolTip": "imgToolTip",
                "imgUser": "imgUser",
                "lblDate": "lblDate",
                "lblDummy": "lblDummy",
                "rtxMessage": "rtxMessage",
                "flxDocAttachment1": "flxDocAttachment1",
                "flxAttachmentName1": "flxAttachmentName1",
                "imgPDF1": "imgPDF1",
                "lblDocName1": "lblDocName1",
                "lblSize1": "lblSize1",
                "flxVerticalMiniSeparator1": "flxVerticalMiniSeparator1",
                "imgRemoveAttachment1": "imgRemoveAttachment1",
                "flxRemoveAttachment1": "flxRemoveAttachment1",
                "flxDownloadAttachment1": "flxDownloadAttachment1",
                "imgDownloadAttachment1": "imgDownloadAttachment1",
                "flxDocAttachment2": "flxDocAttachment2",
                "imgPDF2": "imgPDF2",
                "lblDocName2": "lblDocName2",
                "lblSize2": "lblSize2",
                "imgRemoveAttachment2": "imgRemoveAttachment2",
                "flxRemoveAttachment2": "flxRemoveAttachment2",
                "flxDownloadAttachment2": "flxDownloadAttachment2",
                "imgDownloadAttachment2": "imgDownloadAttachment2",
                "flxDocAttachment3": "flxDocAttachment3",
                "imgPDF3": "imgPDF3",
                "lblDocName3": "lblDocName3",
                "lblSize3": "lblSize3",
                "flxRemoveAttachment3": "flxRemoveAttachment3",
                "imgRemoveAttachment3": "imgRemoveAttachment3",
                "flxDownloadAttachment3": "flxDownloadAttachment3",
                "imgDownloadAttachment3": "imgDownloadAttachment3",
                "flxDocAttachment4": "flxDocAttachment4",
                "imgPDF4": "imgPDF4",
                "lblDocName4": "lblDocName4",
                "lblSize4": "lblSize4",
                "flxRemoveAttachment4": "flxRemoveAttachment4",
                "imgRemoveAttachment4": "imgRemoveAttachment4",
                "flxDownloadAttachment4": "flxDownloadAttachment4",
                "imgDownloadAttachment4": "imgDownloadAttachment4",
                "flxDocAttachment5": "flxDocAttachment5",
                "imgPDF5": "imgPDF5",
                "lblDocName5": "lblDocName5",
                "lblSize5": "lblSize5",
                "flxRemoveAttachment5": "flxRemoveAttachment5",
                "imgRemoveAttachment5": "imgRemoveAttachment5",
                "flxDownloadAttachment5": "flxDownloadAttachment5",
                "imgDownloadAttachment5": "imgDownloadAttachment5",
                "lblUser": "lblUser",
                "lblRowSeperator": "lblRowSeperator",
            };
        },
        /**
         * processMessagesData :This function is used for setting the messages data to the segment
         * @param {data}  data consists of the array of the messages
         * @returns {object} data data
         */
        processMessagesData: function(data) {
            var self = this;
            var username = self.loadsMessagesNewModule().presentationController.getCurrentUserName();
            data = data.map(function(dataItem) {
                var totalAttachments = parseInt(dataItem.totalAttachments);
                var hasFirstAttachment = self.hasFirstAttachment(totalAttachments);
                var hasSecondAttachment = self.hasSecondAttachment(totalAttachments);
                var hasThirdAttachment = self.hasThirdAttachment(totalAttachments);
                var hasFourthAttachment = self.hasFourthAttachment(totalAttachments);
                var hasFifthAttachment = self.hasFifthAttachment(totalAttachments);
                return {
                    "requestId": dataItem.CustomerRequest_id,
                    "template": dataItem.createdby != username ? "flxMessagesLeft" : "flxMessagesRight",
                    "rtxMessage": dataItem.MessageDescription,
                    "lblDate": commonUtilities.getDateAndTime(dataItem.lastmodifiedts),
                    "flxDocAttachment1": {
                        "isVisible": hasFirstAttachment ? true : false
                    },
                    "lblDocName1": hasFirstAttachment ? dataItem.attachments[0].Name : "",
                    "lblSize1": hasFirstAttachment ? '(' + dataItem.attachments[0].Size.trim() + "KB" + ')' : "",
                    "imgPDF1": hasFirstAttachment ? self.getImageByType(dataItem.attachments[0].type) : "pdf_image.png",
                    "flxRemoveAttachment1": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment1": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[0].media_Id;
                            var fileName = dataItem.attachments[0].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment1": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                    },
                    "flxDocAttachment2": {
                        "isVisible": hasSecondAttachment ? true : false
                    },
                    "lblDocName2": hasSecondAttachment ? dataItem.attachments[1].Name : "",
                    "lblSize2": hasSecondAttachment ? '(' + dataItem.attachments[1].Size.trim() + "KB" + ')' : "",
                    "imgPDF2": hasSecondAttachment ? self.getImageByType(dataItem.attachments[1].type) : "pdf_image.png",
                    "flxRemoveAttachment2": {
                        "isVisible": false
                    },
                    "lblUser": dataItem.createdby,
                    "lblRowSeperator": {
                        "isVisible": true
                    },
                    "flxDownloadAttachment2": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[1].media_Id;
                            var fileName = dataItem.attachments[1].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment2": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "flxDocAttachment3": {
                        "isVisible": hasThirdAttachment ? true : false
                    },
                    "lblDocName3": hasThirdAttachment ? dataItem.attachments[2].Name : "",
                    "lblSize3": hasThirdAttachment ? '(' + dataItem.attachments[2].Size.trim() + "KB" + ')' : "",
                    "imgPDF3": hasThirdAttachment ? self.getImageByType(dataItem.attachments[2].type) : "pdf_image.png",
                    "flxRemoveAttachment3": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment3": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[2].media_Id;
                            var fileName = dataItem.attachments[2].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment3": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "flxDocAttachment4": {
                        "isVisible": hasFourthAttachment ? true : false
                    },
                    "lblDocName4": hasFourthAttachment ? dataItem.attachments[3].Name : "",
                    "lblSize4": hasFourthAttachment ? '(' + dataItem.attachments[3].Size.trim() + "KB" + ')' : "",
                    "imgPDF4": hasFourthAttachment ? self.getImageByType(dataItem.attachments[3].type) : "pdf_image.png",
                    "flxRemoveAttachment4": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment4": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[3].media_Id;
                            var fileName = dataItem.attachments[3].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment4": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "flxDocAttachment5": {
                        "isVisible": hasFifthAttachment ? true : false
                    },
                    "lblDocName5": hasFifthAttachment ? dataItem.attachments[4].Name : "",
                    "lblSize5": hasFifthAttachment ? '(' + dataItem.attachments[4].Size.trim() + "KB" + ')' : "",
                    "imgPDF5": hasFifthAttachment ? self.getImageByType(dataItem.attachments[4].type) : "pdf_image.png",
                    "flxRemoveAttachment5": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment5": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[4].media_Id;
                            var fileName = dataItem.attachments[4].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment5": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "imgUser": dataItem.createdby != username ? ViewConstants.IMAGES.BANK_CIRCLE_ICON_MOD : ViewConstants.IMAGES.USER_IMAGE,
                    "imgToolTip": dataItem.createdby != username ? ViewConstants.IMAGES.REPLY_ARROWTIP : ViewConstants.IMAGES.REPLY_ARROWTIP_RIGHT
                };
            });
            return data;
        },
        /**
         * hasFirstAttachment :This function is used to check whether there is first Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no first Attachment
         */
        hasFirstAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 0 ? true : false;
            return has;
        },
        /**
         * hasSecondAttachment :This function is used to check whether there is second Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no second Attachment
         */
        hasSecondAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 1 ? true : false;
            return has;
        },
        /**
         * hasThirdAttachment :This function is used to check whether there is third Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no third Attachment
         */
        hasThirdAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 2 ? true : false;
            return has;
        },
        /**
         * hasFourthAttachment :This function is used to check whether there is fourth Attachment or not
         * @param {totalAttachments}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no fourth Attachment
         */
        hasFourthAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 3 ? true : false;
            return has;
        },
        /**
         * hasFifthAttachment :This function is used to check whether there is fifth Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no fifth Attachment
         */
        hasFifthAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 4 ? true : false;
            return has;
        },
        onBreakpointChange: function(eventObj, width) {
            var scope = this;
            kony.print('on breakpoint change');
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.CustomPopup1.onBreakpointChangeComponent(scope.view.CustomPopup1, width);
            if (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) {
                this.view.flxRightMessages.isVisible = false;
            }
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },
    };
});