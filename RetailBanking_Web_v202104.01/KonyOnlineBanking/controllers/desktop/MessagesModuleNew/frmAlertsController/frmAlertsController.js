define(['commonUtilities', 'FormControllerUtility', 'OLBConstants', 'ViewConstants'], function(commonUtilities, FormControllerUtility, OLBConstants, ViewConstants) {
    var responsiveUtils = new ResponsiveUtils();
    return /** @alias module:frmNotificationsAndMessagesController */ {
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onTouchEnd = this.hidePop();
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxFormContent.doLayout = this.sesetMinHeight;
        },
        preShow: function() {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            this.hidePop();
            this.view.btnMyMessages.onClick = this.navToMessages;
            this.view.btnDeletedMessages.onClick = this.navToDeletedMessages;
            this.view.btnNewMessage.onClick = this.navToNewMessage;
            this.view.flxSearchIcon.onClick = this.onSearchClick.bind(this);
            this.view.lblSearch.onTouchEnd = this.onSearchClick.bind(this);
            this.view.txtSearch.onDone = this.onSearchClick.bind(this);
            this.view.txtSearch.onKeyUp = this.showOrHideSearchCrossImage.bind(this);
            this.view.flxClearSearch.onClick = this.onSearchClick.bind(this);
            this.view.flxNoSearchResult.isVisible = false;
            if (commonUtilities.isCSRMode()) {
                this.view.btnDismiss.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.btnDismiss.skin = commonUtilities.disableSegmentButtonSkinForCSRMode(15);
            } else {
                this.view.btnDismiss.onClick = function() {
                    scopeObj.showDismissPopup();
                };
            }
            this.view.flxInfo.onClick = function() {
                if (scopeObj.view.AllForms.isVisible) {
                    scopeObj.view.AllForms.isVisible = false;
                } else {
                    scopeObj.view.AllForms.isVisible = true;
                }
            };
            this.view.CustomPopup1.btnNo.onClick = function() {
                scopeObj.closeDismissPopup();
            };
            this.view.CustomPopup1.flxCross.onClick = function() {
                scopeObj.closeDismissPopup();
            };
        },
        loadsMessagesNewModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MessagesModuleNew");
        },
        postShow: function() {
            this.setMinHeight;
        },
        screenHeightGbl: 0,
        setMinHeight: function() {
            if (this.screenHeightGbl === kony.os.deviceInfo().screenHeight) {
                return;
            }
            this.screenHeightGbl = kony.os.deviceInfo().screenHeight;
            var minHeightForMsg = 0;
            // screenheight - headerheight - footer height - address top - address bottom
            if (responsiveUtils.isMobile || kony.application.getCurrentBreakpoint() === 640) {
                this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - 200 + "dp";
            } else {
                this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - 270 + "dp"; //270 = header 120 + footer 150
                minHeightForMsg = kony.os.deviceInfo().screenHeight - 270;
                if (minHeightForMsg < 700) {
                    minHeightForMsg = 700;
                }
                this.view.flxContainer.minHeight = minHeightForMsg + "dp";
            }
        },
        updateFormUI: function(context) {
            if (context.showProgressBar) {
                FormControllerUtility.showProgressBar(this.view);
            }
            if (context.hideProgressBar) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (context.showAlertsViewModel) {
                this.showAlertsViewModel(context.showAlertsViewModel);
            }
            if (context.unreadNotificationCountViewModel) {
                this.showUnreadNotificationCount(context.unreadNotificationCountViewModel.count);
            }
            if (context.unreadMessagesCountView) {
                this.showUnreadMessagesCount(context.unreadMessagesCountView);
            }
            if (context.updateNotificationAsReadViewModel) {
                this.updateNotificationAsReadViewModel(context.updateNotificationAsReadViewModel);
            }
            if (context.searchAlertsViewModel) {
                this.showSearchAlertsViewModel(context.searchAlertsViewModel);
            }
            if (context.inFormError) {
                this.view.rtxMessageWarning.text = context.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
            if (context.dismissAlertsViewModel) {
                this.dismissAlertsViewModel(context.dismissAlertsViewModel);
            }
        },
        /**
         * showAlertsViewModel :This function is used to bind the alerts to the segment
         * @param{viewModel} viewModel consists list of Alerts Data
         */
        showAlertsViewModel: function(viewModel) {
            var data = viewModel.data;
            var self = this;
            this.view.customheadernew.activateMenu("ALERTS AND MESSAGES", "Alerts");
            self.view.lblSearch.onTouchEnd = self.onSearchClick.bind(self);
            self.view.flxClearSearch.onClick = self.onSearchClick.bind(self);
            self.view.txtSearch.onDone = self.onSearchClick.bind(self);
            self.view.txtSearch.onKeyUp = self.showOrHideSearchCrossImage.bind(self);
            this.view.AllForms.setVisibility(false);
            if (data.length === 0) {
                this.view.flxRightNotifications.isVisible = false;
                if (viewModel.errorMsg) {
                    this.view.rtxNoSearchResults.text = viewModel.errorMsg;
                } else {
                    this.view.rtxNoSearchResults.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.noAlertsMsg");
                }
                this.view.flxNoSearchResult.isVisible = true;
                this.view.segMessageAndNotification.isVisible = false;
                this.view.btnNewMessage.setVisibility(true);
                this.view.flxRightNotifications.setVisibility(false);
            } else {
                this.view.btnDismiss.isVisible = true;
                this.view.flxNoSearchResult.isVisible = false;
                this.view.segMessageAndNotification.isVisible = true;
                this.setNotificationSegmentData(data);
            }
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * onSearchClick :This function is executed on entering any search String in the Alerts Tab and click of "Go" or "Enter" and based on the search String the Alerts are displayed
         */
        onSearchClick: function() {
            var searchString = this.view.txtSearch.text;
            FormControllerUtility.showProgressBar(this.view);
            if (this.view.lblSearch.isVisible) {
                if (searchString && searchString.trim()) {
                    this.view.lblSearch.setVisibility(false);
                    this.view.flxClearSearch.setVisibility(true);
                    this.loadsMessagesNewModule().presentationController.searchAlerts(searchString.trim());
                } else {
                    this.loadsMessagesNewModule().presentationController.showAlertsPage();
                }
            } else {
                this.view.lblSearch.setVisibility(true);
                this.view.flxClearSearch.setVisibility(false);
                this.view.txtSearch.text = "";
                this.view.flxClearSearch.setVisibility(false);
                this.loadsMessagesNewModule().presentationController.showAlertsPage();
            }
        },

        /**
         * onSearchClick :This function is used to bind the search Result of the Alerts to the Segment
         * @param {JSON} viewModel is a JSON which  contains data of the Alerts according to the Search String
         */
        showSearchAlertsViewModel: function(viewModel) {
            var data = viewModel.data;
            var self = this;
            if (data.length === 0) {
                this.view.btnDismiss.isVisible = false;
                this.view.flxSearchMain.isVisible = true;
                if (viewModel.errorMsg) {
                    this.view.rtxNoSearchResults.text = viewModel.errorMsg;
                } else {
                    this.view.rtxNoSearchResults.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.emptySearchMsg");
                }
                this.view.flxNoSearchResult.isVisible = true;
                this.view.flxVerticalSeparator.isVisible = true;
                this.view.lblHeadingNotification.text = "";
                this.view.rtxNotification.text = "";
                this.view.lblDateAndTime.text = "";
                this.view.lblOfferLink.isVisible = false;
                this.view.imgBanner.isVisible = false;
                this.view.segMessageAndNotification.setData([]);
                this.view.segMessageAndNotification.isVisible = false;
            } else {
                this.view.flxNoSearchResult.isVisible = false;
                this.view.flxVerticalSeparator.isVisible = false;
                this.view.segMessageAndNotification.isVisible = true;
                this.view.flxTabs.isVisible = true;
                //TODO: count of unread notification which is unread..
                var count = data.map(function(dataItem) {
                    var unread = 1;
                    if (self.isRead(dataItem.isRead)) {
                        unread = 0;
                    }
                    return unread;
                }).reduce(function(a, b) {
                    return a + b;
                }, 0);
                this.showUnreadNotificationCount(count);
                this.setNotificationSegmentData(data);
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.view.forceLayout();
        },
        showOrHideSearchCrossImage: function() {
            var searchString = this.view.txtSearch.text;
            if (searchString && searchString.trim()) {
                this.view.flxClearSearch.setVisibility(true);
            } else {
                this.view.flxClearSearch.setVisibility(false);
            }
            this.view.forceLayout();
        },
        hidePop: function() {
            this.view.AllForms.isVisible = false;
        },
        setNotificationSegmentData: function(data) {
            var arrowVisibility = false;
            this.view.segMessageAndNotification.onRowClick = this.onRowSelection.bind(this);
            if (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) {
                arrowVisibility = true;
            }
            var dataMap = {
                "flxNotificationsAndMessages": "flxNotificationsAndMessages",
                "imgCurrentOne": "imgCurrentOne",
                "lblDateAndTime": "lblDateAndTime",
                "lblSegDescription": "lblSegDescription",
                "lblSegHeading": "lblSegHeading",
                "imgBulletIcon": "imgBulletIcon",
                "imgAttachment": "imgAttachment",
                "segNotificationsAndMessages": "segNotificationsAndMessages",
                "lblRowSeperator": "lblRowSeperator",
                "lblArrow": "lblArrow",
                "userNotificationId": "userNotificationId"
            };
            var self = this;
            data = data.map(function(dataItem) {
                return {
                    "imgCurrentOne": {
                        "src": ViewConstants.IMAGES.ACCOUNTS_SIDEBAR_BLUE,
                        "isVisible": false
                    },
                    "imgBulletIcon": {
                        "isVisible": !self.isRead(dataItem.isRead)
                    },
                    "lblRowSeperator": {
                        "isVisible": true
                    },
                    "lblArrow": {
                        "skin": "sknLblrightArrowFontIcon0273E3",
                        "isVisible": arrowVisibility
                    },
                    "imgAttachment": {
                        "src": ViewConstants.IMAGES.ATTACHMENT_GREY,
                        "isVisible": false
                    },
                    "lblDateAndTime": {
                        "text": commonUtilities.getDateAndTime(dataItem.receivedDate)
                    },
                    "lblSegDescription": {
                        "text": self.labelData(dataItem.notificationText)
                    },
                    "flxNotificationsAndMessages": {
                        "skin": "sknFlxffffff",
                        "hoverSkin": ViewConstants.SKINS.SKNFLXF7F7F7
                    },
                    "lblSegHeading": {
                        "text": dataItem.notificationSubject
                    },
                    "userNotificationId": dataItem.userNotificationId
                };
            });
            this.view.segMessageAndNotification.widgetDataMap = dataMap;
            this.view.segMessageAndNotification.setData(data);
            if (!responsiveUtils.isMobile) {
                this.onRowSelection();
            }
            this.view.forceLayout();
        },
        /**
         * isRead :This function is used to check whether the notification is read or not
         * @param {status}   status is either 0 or 1 indicating whether the  notification is read or not
         * @return {String} returns true or false depending on the value of notification is read or not
         */
        isRead: function(status) {
            if (status == 1) {
                return true;
            } else {
                return false;
            }
        },
        /**
         * labelData :This function is used to convert the html Data to normal String by removing the tags and assigning the text to a label
         * @param {object} data  html Data
         *@ return {text} normal text after converting from html to text
         */
        labelData: function(data) {
            var parser = new DOMParser();
            var res = parser.parseFromString(data, 'text/html');
            var html = res.body.textContent;
            var div = document.createElement("div");
            div.innerHTML = html;
            return div.innerText;
        },
        onRowSelection: function() {
            var scopeObj = this;
            var isSegClicked = false;
            var selectedUserNotificationId = this.view.segMessageAndNotification.data[0].userNotificationId;
            var index = 0;
            this.view.lblHeadingNotification.text = "";
            this.view.rtxNotification.text = "";
            this.view.lblDateAndTime.text = "";
            this.view.lblOfferLink.text = "";
            this.view.imgBanner.src = "";
            if (this.view.segMessageAndNotification.selectedRowItems.length) {
                isSegClicked = true;
                selectedUserNotificationId = this.view.segMessageAndNotification.selectedRowItems[0].userNotificationId;
                index = this.view.segMessageAndNotification.selectedRowIndex[1];
            }
            var data = this.view.segMessageAndNotification.data;
            var response = this.loadsMessagesNewModule().presentationController.getAlertsDetails(selectedUserNotificationId);
            this.view.lblHeadingNotification.text = response.notificationSubject;
            this.view.rtxNotification.text = response.notificationText;
            this.view.lblDateAndTime.text = commonUtilities.getDateAndTime(response.receivedDate);
            /**
             *Code to show interactive notifications based on configurations
             */
            if (applicationManager.getConfigurationManager().isInteractiveNotificationEnabled === "true") {
                if (response.notificationActionLink && response.notificationActionLink !== "") {
                    this.view.lblOfferLink.text = response.notificationActionLink;
                    this.view.lblOfferLink.isVisible = true;
                    var handCursor = document.querySelectorAll(("." + OLBConstants.SKINS.INTERACTIVE_LINK));
                    for (var i = 0; i < handCursor.length; i++) {
                        handCursor[i].style.cursor = "pointer";
                    }
                    var notificationActionLink = this.view.lblOfferLink.text;
                    if (notificationActionLink) {
                        notificationActionLink = notificationActionLink.trim().toUpperCase();
                    }
                    switch (notificationActionLink) {
                        case "PAY BILL":
                            this.view.lblOfferLink.onTouchEnd = function() {
                                scopeObj.loadsMessagesNewModule().presentationController.navigateToBillPay();
                            };
                            break;
                        case "SEND MONEY":
                            this.view.lblOfferLink.onTouchEnd = function() {
                                scopeObj.loadsMessagesNewModule().presentationController.navigateToSendMoney();
                            };
                            break;
                        case "VIEW MY ACCOUNT":
                            this.view.lblOfferLink.onTouchEnd = function() {
                                scopeObj.loadsMessagesNewModule().presentationController.navigateToViewMyAccount();
                            };
                            break;
                        case "TRANSFER MONEY":
                            this.view.lblOfferLink.onTouchEnd = function() {
                                scopeObj.loadsMessagesNewModule().presentationController.navigateToTransferPage();
                            };
                            break;
                        default:
                            this.view.lblOfferLink.onTouchEnd = function() {
                                window.open(response.notificationActionLink);
                            };
                            break;
                    }
                } else {
                    this.view.lblOfferLink.isVisible = false;
                }
            } else {
                this.view.lblOfferLink.isVisible = false;
            }
            response.imageURL = response.imageURL ? response.imageURL.trim() : "";
            if (response.imageURL !== "") {
                this.view.imgBanner.src = response.imageURL;
                this.view.imgBanner.isVisible = true;
            } else {
                this.view.imgBanner.isVisible = false;
            }
            var prevIndex = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].imgCurrentOne.isVisible) {
                    prevIndex = i;
                }
                data[i].flxNotificationsAndMessages = {
                    "skin": ViewConstants.SKINS.SKNFLXFFFFFF
                };
                data[i].imgCurrentOne.isVisible = false;
            }
            data[index].flxNotificationsAndMessages = {
                "skin": ViewConstants.SKINS.SKNFLXF7F7F7
            };
            data[index].lblArrow = {
                "skin": "sknLblrightArrowFontIcon0273E3" // fonticon in view constants
            };
            data[index].imgCurrentOne.isVisible = true;
            data[index].imgBulletIcon.isVisible = false;
            //Introduced timout in order to avoid some lazy loading problem of widget... suggested by platform
            setTimeout(function() {
                scopeObj.view.segMessageAndNotification.setDataAt(data[index], index);
                scopeObj.view.segMessageAndNotification.setDataAt(data[prevIndex], prevIndex);
            }, 500);
            //this.view.segMessageAndNotification.setDataAt(data[index], index);
            //this.view.segMessageAndNotification.setDataAt(data[prevIndex], prevIndex);
            if (!this.isRead(response.isRead)) {
                this.loadsMessagesNewModule().presentationController.updateNotificationAsRead(data[index].userNotificationId);
            }
            this.view.CustomPopup1.btnYes.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                scopeObj.closeDismissPopup();
                scopeObj.loadsMessagesNewModule().presentationController.dismissNotification(selectedUserNotificationId);
            };
            var break_point = kony.application.getCurrentBreakpoint();
            if (break_point == 640 || responsiveUtils.isMobile) {
                if (isSegClicked) {
                    this.onRowSelectionMobile(response)
                }
            } else {
                this.view.flxRightNotifications.isVisible = true;
                this.view.flxTabs.isVisible = true;
                this.view.segMessageAndNotification.isVisible = true;

            }
            this.view.forceLayout();
        },
        onRowSelectionMobile: function(response) {
            this.loadsMessagesNewModule().presentationController.showMessageDetailsMobilePage({
                show: "Alerts"
            }, response);
        },
        /**
         * showUnreadNotificationCount :This function is used to display the unread Notifications Count in the Tab
         * @param {data}  data consists of the unreadNotificationCount
         */
        showUnreadNotificationCount: function(data) {
            this.unReadNotificationCount = data;
            this.view.btnAlerts.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts") + " (" + data + ")";
            this.updateAlertsIcon();
            this.view.forceLayout();
        },
        /**
         * updateNotificationAsReadViewModel :This function is executed when the notification is marked as read
         * @param {object} viewModel consists of status whether it is success or failure and the unreadNotificationCount
         */
        updateNotificationAsReadViewModel: function(viewModel) {
            if (viewModel.status === "success") {
                if (this.unReadNotificationCount > 0) {
                    this.unReadNotificationCount = this.unReadNotificationCount - 1; // Decrement by 1
                }
                this.view.btnAlerts.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts") + " (" + this.unReadNotificationCount + ")";
                if (this.unReadNotificationCount === 0 && this.unreadMessagesCount === 0) {
                    this.view.customheadernew.lblNewMessages.isVisible = false;
                } else {
                    this.view.customheadernew.lblNewMessages.isVisible = true;
                }
                var data = this.view.segMessageAndNotification.data;
                var index = 0;
                if (this.view.segMessageAndNotification.selectedIndex) {
                    index = this.view.segMessageAndNotification.selectedIndex[1];
                }
                data[index].imgBulletIcon.isVisible = false;
                this.view.segMessageAndNotification.setDataAt(data[index], index);
            }
            this.view.forceLayout();
        },
        /**
         * updateAlertsIcon :This function is used to update the red Dot on the Alerts icon depending on whether there are any unread Messages/Notifications
         */
        updateAlertsIcon: function() {
            if (this.unreadMessagesCount !== undefined && this.unReadNotificationCount !== undefined) {
                applicationManager.getConfigurationManager().setUnreadMessageCount({
                    count: parseInt(this.unreadMessagesCount) + parseInt(this.unReadNotificationCount)
                });
                this.view.customheadernew.updateAlertIcon();
            }
        },
        /**
         * This function is used to display the unread messages Count in the "My Messages" tab
         * @param {object} unReadMsgsCountData -  consists of the unreadMessagesCount which is to be displayed
         */
        showUnreadMessagesCount: function(unReadMsgsCountData) {
            this.unreadMessagesCount = parseInt(unReadMsgsCountData.unReadMessagesCount);
            this.view.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
            this.updateAlertsIcon();
            this.view.flxMessagesMain.parent.forceLayout();
        },
        navToMessages: function() {
            this.loadsMessagesNewModule().presentationController.showMyMessagesPage({
                show: "Messages"
            });
        },
        navToNewMessage: function() {
            this.loadsMessagesNewModule().presentationController.showNewMessagePage();
        },
        navToDeletedMessages: function() {
            this.loadsMessagesNewModule().presentationController.showDeletedMessagesPage();
        },
        closeDismissPopup: function() {
            this.view.FlxDismiss.setVisibility(false);
            this.view.flxDialogs.setVisibility(false);
        },
        /**
         * showDismissPopup :This function is used to show the dismiss PopUp once the user clicks on dismiss Noification
         */
        showDismissPopup: function() {
            this.view.flxDialogs.isVisible = true;
            this.view.AllForms.isVisible = false;
            this.view.FlxDismiss.isVisible = true;
        },
        /**
         * dismissAlertsViewModel :This function is executed when the dismiss of the notification is successful
         * @param {viewModel}   viewModel consists of the Status whether it is success or failure
         */
        dismissAlertsViewModel: function(viewModel) {
            if (viewModel.status === "success") {
                this.onSearchClick();
            } else {
                FormControllerUtility.hideProgressBar(this.view);
            }
        },

        onBreakpointChange: function(eventObj, width) {
            var scope = this;
            kony.print('on breakpoint change');
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.CustomPopup1.onBreakpointChangeComponent(scope.view.CustomPopup1, width);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },

    };
});