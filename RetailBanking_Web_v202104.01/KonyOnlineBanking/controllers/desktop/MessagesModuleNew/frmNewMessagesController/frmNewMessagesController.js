define(['FormControllerUtility', 'CommonUtilities', 'CSRAssistUI', 'ViewConstants', 'OLBConstants'], function(FormControllerUtility, CommonUtilities, CSRAssistUI, ViewConstants, OLBConstants) {
    var responsiveUtils = new ResponsiveUtils();
    return /** @alias module:frmNotificationsAndMessagesController */ {
        init: function() {
            this.view.preShow = this.frmPreshow;
            this.view.postShow = this.frmPostShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxFormContent.doLayout = this.setMinHeight;
        },
        frmPreshow: function() {
            var scopeObj = this;
            this.setActions();
            this.view.tbxSubject.onKeyUp = this.enableSendButton.bind(this);
            this.view.textareaDescription.onKeyUp = this.enableSendButton.bind(this);
            this.view.segAttachment.setData([]);
            if (CommonUtilities.isCSRMode()) {
                this.view.btnNewMessageSend.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.btnNewMessageSend.focusSkin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.btnNewMessageSend.skin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.btnNewMessageSend.onClick = function() {
                    FormControllerUtility.showProgressBar(scopeObj.view);
                    var requestParam = {
                        "files": scopeObj.fileObject,
                        "categoryid": scopeObj.view.listbxCategory.selectedKey,
                        "subject": scopeObj.view.tbxSubject.text,
                        "description": scopeObj.view.textareaDescription.text,
                    };
                    scopeObj.loadsMessagesNewModule().presentationController.createNewRequestOrMessage(requestParam);
                };
            }
        },
        frmPostShow: function() {
            this.setMinHeight;
            this.view.tbxSubject.text = "";
            this.view.textareaDescription.text = "";
        },
        loadsMessagesNewModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MessagesModuleNew");
        },
        setMinHeight: function() {
            if (this.screenHeightGbl === kony.os.deviceInfo().screenHeight) {
                return;
            }
            this.screenHeightGbl = kony.os.deviceInfo().screenHeight;
            var minHeightForMsg = 0;
            // screenheight - headerheight - footer height - address top - address bottom
            if (responsiveUtils.isMobile || kony.application.getCurrentBreakpoint() === 640) {
                this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - 200 + "dp";
            } else {
                this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - 270 + "dp"; //270 = header 120 + footer 150
                minHeightForMsg = kony.os.deviceInfo().screenHeight - 270;
                if (minHeightForMsg < 700) {
                    minHeightForMsg = 700;
                }
                this.view.flxContainer.minHeight = minHeightForMsg + "dp";
            }
        },
        updateFormUI: function(context) {
            if (context.showProgressBar) {
                FormControllerUtility.showProgressBar(this.view);
            }
            if (context.hideProgressBar) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (context.showRequestsView) {
                this.showRequestsView(context.showRequestsView);
            }
            if (context.unreadMessagesCountView) {
                this.showUnreadMessagesCount(context.unreadMessagesCountView);
            }
            if (context.showMessagesView) {
                this.showMessagesView(context.showMessagesView);
            }
            if (context.updateMessageAsReadSuccessView) {
                this.updateMessageAsReadSuccessView(context.updateMessageAsReadSuccessView.readCount);
            }
            if (context.createNewRequestOrMessagesView) {
                this.showNewMessage(context.createNewRequestOrMessagesView);
            }
        },
        setActions: function() {
            this.view.btnAlerts.onClick = this.navToAlerts;
            this.view.btnMyMessages.onClick = this.navToMessages;
            this.view.btnDeletedMessages.onClick = this.navToDeletedMessages;
            this.view.txtSearch.onKeyUp = this.showOrHideSearchCrossImage;
            this.view.txtSearch.onDone = this.OnMessageSearchClick;
            this.view.flxSearchIcon.onClick = this.OnMessageSearchClick;
            this.view.lblSearch.onTouchEnd = this.OnMessageSearchClick;
            this.view.flxClearSearch.onClick = this.OnMessageSearchClick(true);
            this.view.flxImageAttachment.onClick = this.addAttachment;
            this.view.btnCancel.onClick = this.cancelMessage;
            this.view.flxImageAttachment.onClick = this.browseFiles;
        },
        showNewMessage: function(viewModel) {
            // to fill the listbox data
            if (viewModel.data) {
                var requestCategories = viewModel.data.map(function(dataItem) {
                    var keyValue = [];
                    keyValue.push(dataItem.id);
                    keyValue.push(dataItem.Name);
                    return keyValue;
                });
                this.view.listbxCategory.masterData = requestCategories;
                var newMessageData = {
                    "imgCurrentOne": {
                        "src": ViewConstants.IMAGES.ACCOUNTS_SIDEBAR_BLUE,
                        "isVisible": true
                    },
                    "flxNotificationsAndMessages": {
                        "skin": ViewConstants.SKINS.SKNFLXF7F7F7,
                        "hoverSkin": ViewConstants.SKINS.SKNFLXF7F7F7
                    },
                    "lblSegHeading": {
                        "text": kony.i18n.getLocalizedString("i18n.AlertsAndMessages.NewMessagesMod")
                    }
                };

                FormControllerUtility.hideProgressBar(this.view);
            }
        },
        /**
         * This function is used to display the unread messages Count in the "My Messages" tab
         * @param {object} unReadMsgsCountData -  consists of the unreadMessagesCount which is to be displayed
         */
        showUnreadMessagesCount: function(unReadMsgsCountData) {
            this.unreadMessagesCount = parseInt(unReadMsgsCountData.unReadMessagesCount);
            this.view.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
            this.updateAlertsIcon();
            this.view.forceLayout();
        },
        /**
         * updateAlertsIcon :This function is used to update the red Dot on the Alerts icon depending on whether there are any unread Messages/Notifications
         */
        updateAlertsIcon: function() {
            if (this.unreadMessagesCount !== undefined && this.unReadNotificationCount !== undefined) {
                applicationManager.getConfigurationManager().setUnreadMessageCount({
                    count: parseInt(this.unreadMessagesCount) + parseInt(this.unReadNotificationCount)
                });
                this.view.customheadernew.headermenu.updateAlertIcon();
            }
        },
        navToAlerts: function() {
            this.loadsMessagesNewModule().presentationController.showAlertsPage();
        },
        navToMessages: function() {
            var scopeObj = this;
            FormControllerUtility.hideProgressBar(scopeObj.view);
            this.loadsMessagesNewModule().presentationController.showMyMessagesPage({
                show: "Messages"
            });
        },
        navToDeletedMessages: function() {
            this.loadsMessagesNewModule().presentationController.showDeletedMessagesPage();
        },
        /**
         * showOrHideSearchCrossImage
         */
        showOrHideSearchCrossImage: function() {
            var searchString = this.view.txtSearch.text;
            if (searchString && searchString.trim()) {
                this.view.flxClearSearch.setVisibility(true);
            } else {
                this.view.flxClearSearch.setVisibility(false);
            }
            this.view.forceLayout();
        },
        /**
         * OnMessageSearchClick :This function is executed on entering any search String in the My Messages Tab and click of "Go" or "Enter" and based on the search String the Requests are displayed.The search happens based on the subject field
         * @param {boolean} clearSearch clear Search
         */
        OnMessageSearchClick: function(clearSearch) {
            var searchString = this.view.txtSearch.text;
            if (clearSearch === true) {
                this.view.txtSearch.text = "";
                this.view.lblSearch.setVisibility(true);
                // this.loadAlertsMessagesModule().presentationController.showRequests();
                this.showOrHideSearchCrossImage();
            } else if (searchString && searchString.trim()) {
                this.view.lblSearch.setVisibility(true);
                this.view.flxClearSearch.setVisibility(false);
                // this.loadAlertsMessagesModule().presentationController.searchRequest(searchString);
                this.view.txtSearch.text = searchString;
                this.showOrHideSearchCrossImage();
            } else {
                this.loadAlertsMessagesModule().presentationController.showRequests();
            }
        },
        clearSearchTbx: function() {},
        // include in functions so that you can remove this fun
        showNoRecords: function() {
            this.view.flxNoSearchResult.isVisible = true;
            this.view.segMessageAndNotification.isVisible = false;
        },
        addAttachment: function() {},
        enableSendButton: function() {
            var category = this.view.listbxCategory.selectedKey;
            var subject = this.view.tbxSubject.text.trim();
            var description = this.view.textareaDescription.text.trim();
            if (category && subject.length > 0 && description.length > 0 && !(CommonUtilities.isCSRMode())) {
                FormControllerUtility.enableButton(this.view.btnNewMessageSend);
            } else {
                FormControllerUtility.disableButton(this.view.btnNewMessageSend);
            }
        },

        cancelMessage: function() {
            this.loadsMessagesNewModule().presentationController.showMyMessagesPage({
                show: "Messages"
            });
        },
        /**
         * browseFiles :This function is used for opening the browser for uploading the files on click of the Attachment icon
         */
        browseFiles: function() {
            var config = this.getBrowseFilesConfig();
            this.view.lblWarningNewMessage.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                this.view.lblWarningNotMoreThan5files.setVisibility(false);
            }
            kony.io.FileSystem.browse(config, this.browseFilesCallback);
        },
        /**
         * getBrowseFilesConfig :This function is used to get the browser files config like which type of files to be uploaded
         * @return {config} returns the documents attached
         */
        getBrowseFilesConfig: function() {
            var config = {
                selectMultipleFiles: true,
                filter: ["image/png", "application/msword", "image/jpeg", "application/pdf", "text/plain", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"]
            };
            return config;
        },
        /**
         * replyBrowseFiles :This function displays the error message when more than five files are attached or invokes the konyAPI to browse the files
         */
        replyBrowseFiles: function() {
            var config = this.getBrowseFilesConfig();
            this.view.lblWarningReplyMessage.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                this.view.lblWarningNotMoreThan5files.setVisibility(false);
            }
            kony.io.FileSystem.browse(config, this.replyBrowseFilesCallback);
        },
        /**
         * browseFilesCallback :This function executes once the files are uploded so that  the uploaded files are binded to the Segment while creating a new Message
         * @param {object}  event event
         * @param {object}   files is the data of the files which are uploaded
         */
        browseFilesCallback: function(event, files) {
            this.bindFilesToSegment(files, this.view.segAttachment, this.view.lblWarningNewMessage);
        },
        /**
         * replyBrowseFilesCallback :This function executes once the files are uploded so that  the uploaded files are binded to the Segment while replying to the request
         * @param {object}  event event
         * @param {object} files files is the data of the files which are uploaded
         */
        replyBrowseFilesCallback: function(event, files) {
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile)
                this.bindFilesToSegment(files, this.view.segAttachmentRightMessage, this.view.lblWarningNotMoreThan5files);
            else
                this.bindFilesToSegment(files, this.view.segAttachmentRightMessage, this.view.lblWarningReplyMessage);
        },
        /**
         * bindFilesToSegment :This function binds the attachment data to the segment
         * @param {object}  files is a  JSON which consists of the data of the files attached ,segment for which the files data is to be binded and the warning label which shows different kinds o warnings
         * @param {object} segment segment
         * @param {string} warningLabel warningLabel
         */
        bindFilesToSegment: function(files, segment, warningLabel) {
            var self = this;
            this.fileObject = this.fileObject || [];
            warningLabel.skin = "sknRtxSSPFF000015Px";
            if (this.fileObject.length + files.length > 5) {
                warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.Maximum5AttachmentsAllowed");
                warningLabel.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                    warningLabel.left = "58dp";
                }
                this.view.flxSendNewMessage.forceLayout();
                return;
            } else if (this.isFileSizeExceeds(files)) {
                warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.SizeExceeds1MB");
                warningLabel.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                    warningLabel.left = "58dp";
                }
                this.view.flxSendNewMessage.forceLayout();
                return;
            } else if (this.isFileAlreadyAdded(files)) {
                warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.AlreadyFileAdded");
                warningLabel.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                    warningLabel.left = "58dp";
                }
                this.view.flxSendNewMessage.forceLayout();
                return;
            } else if (!this.isFileTypeSupported(files)) {
                warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.InvalidFileType");
                warningLabel.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                    warningLabel.left = "58dp";
                    this.view.flxReplyMessageButtons.forceLayout();
                }
                this.view.flxSendNewMessage.forceLayout();
                return;
            } else {
                if (files && files.length > 0) {
                    files.forEach(function(item) {
                        self.fileObject.push(item);
                    });
                    var dataToAdd = files.map(function(dataItem) {
                        var break_point = kony.application.getCurrentBreakpoint();
                        if (break_point == 640 && responsiveUtils.isMobile) {
                            docSize = "(" + dataItem.file.size + "B)";
                        } else {
                            docSize = "(" + dataItem.file.size + " Bytes)";
                        }
                        return {
                            "lblDocSize": docSize,
                            "imgPDF": self.getImageByType(dataItem.file.type),
                            "flxRemoveAttachment": {
                                "onClick": function() {
                                    var index = segment.selectedRowIndex;
                                    var sectionIndex = index[0];
                                    var rowIndex = index[1];
                                    segment.removeAt(rowIndex, sectionIndex);
                                    self.fileObject.splice(rowIndex, 1);
                                    self.view.forceLayout();
                                    self.view.flxReplyMessageButtons.setFocus(true);
                                }
                            },
                            "imgRemoveAttachment": {
                                "src": ViewConstants.IMAGES.ICON_CLOSE_BLUE
                            },
                            "rtxDocName": dataItem.file.name
                        };
                    });
                    var data = segment.data;
                    if (data && data.length > 0) {
                        dataToAdd.forEach(function(item) {
                            data.push(item);
                        });
                    } else {
                        data = dataToAdd;
                    }
                    var dataMap = {
                        "lblDocSize": "lblDocSize",
                        "flxAddAttachmentMain": "flxAddAttachmentMain",
                        "flxAttachmentName": "flxAttachmentName",
                        "flxDocAttachment": "flxDocAttachment",
                        "flxRemoveAttachment": "flxRemoveAttachment",
                        "flxVerticalMiniSeparator": "flxVerticalMiniSeparator",
                        "imgPDF": "imgPDF",
                        "imgRemoveAttachment": "imgRemoveAttachment",
                        "rtxDocName": "rtxDocName"
                    };
                    segment.widgetDataMap = dataMap;
                    segment.setData(data);
                }
            }
            this.view.forceLayout();
        },
        /**
         * isFileSizeExceeds :This function is used to check if the size of the file exceeds more than 1MB
         * @param {files}   files which consists of the data of the files
         * @return {boolean} true if the size of the file exceeds false if the file size is less than 1MB
         */
        isFileSizeExceeds: function(files) {
            var temp = files.filter(function(file) {
                return file.size > 1048576;
            });
            return temp.length > 0;
        },
        /**
         * isFileAlreadyAdded :This function is used to check if the file is already added or not
         * @param {object}   files JSON which consists of the data of the files
         * @return {boolean} true if the same file is added again or  false if the new file is added
         */
        isFileAlreadyAdded: function(files) {
            var temp = this.fileObject.filter(function(file) {
                return files.filter(function(f) {
                    return f.name == file.name;
                }).length > 0;
            });
            return temp.length > 0;
        },
        /**
         * used to check file type supported or not
         * @param {object} files files is the data of the files which are uploaded
         * @param {boolean} status status
         */
        isFileTypeSupported: function(files) {
            filetype = files[0].file.type;
            if (filetype == "image/png" || filetype == "application/msword" || filetype == "image/jpeg" || filetype == "application/pdf" || filetype == "text/plain" || filetype == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") return true;
            return false;
        },
        /**
         * getImageByType :This function is used to get the image depending on the type of the document attached
         * @param {type}  type of the Attachment can be application/pdf, text/plain, image/jpeg, application/msword(DOC) or application/vnd.openxmlformats-officedocument.wordprocessingml.document(DOCX)
         * @return {image} returns the appropriate image based on type of the document
         */
        getImageByType: function(type) {
            var image;
            switch (type) {
                case "application/pdf":
                    image = ViewConstants.IMAGES.PDF_IMAGE;
                    break;
                case "text/plain":
                    image = ViewConstants.IMAGES.TXT_IMAGE;
                    break;
                case "image/jpeg":
                    image = ViewConstants.IMAGES.JPEG_IMAGE;
                    break;
                case "application/msword":
                    image = ViewConstants.IMAGES.DOC_IMAGE;
                    break;
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    image = ViewConstants.IMAGES.DOCX_IMAGE;
                    break;
                case "image/png":
                    image = ViewConstants.IMAGES.PNG_IMAGE;
                    break;
            }
            return image;
        },
        onBreakpointChange: function(eventObj, width) {
            var scope = this;
            kony.print('on breakpoint change');
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.CustomPopup1.onBreakpointChangeComponent(scope.view.CustomPopup1, width);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },
    };
});