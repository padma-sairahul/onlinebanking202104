define({
    segmentAllPayeesRowClick: function() {
        var currForm = kony.application.getCurrentForm();
        var index = currForm.tableView.segmentBillpay.selectedIndex[1];
        var data = currForm.tableView.segmentBillpay.data;
        data[index].imgDropdown = "arrow_down.png";
        data[index].imgErrorSelected = "error_yellow.png";
        data[index].lblError = "Verify payment amount and available account balance";
        data[index].template = "flxBillPayAllPayees";
        currForm.tableView.segmentBillpay.setDataAt(data[index], index);
        /*currForm.flxCustomListBoxContainerPayFrom.isVisible=false;
        currForm.flxCustomListBoxContainerCategory.isVisible=false;*/
    },
    viewEBill: function() {
        var currForm = kony.application.getCurrentForm();
        var height_to_set = 140 + currForm.flxContainer.frame.height;
        currForm.flxViewEbill.height = height_to_set + "dp";
        currForm.flxViewEbill.isVisible = true;
        currForm.forceLayout();
    },
    /* viewPayFromListBox:function()
    {
       var currForm = kony.application.getCurrentForm();
      if(currForm.flxCustomListBoxContainerPayFrom.isVisible===true)
        {
          currForm.flxCustomListBoxContainerPayFrom.isVisible=false;
        }
      else
        {
          currForm.flxCustomListBoxContainerPayFrom.isVisible=true;
        }
       currForm.forceLayout();
    },
     viewCategoryListBoxController:function()
     {
       var currForm = kony.application.getCurrentForm();
       if(currForm.flxCustomListBoxContainerCategory.isVisible===true)
        {
          currForm.flxCustomListBoxContainerCategory.isVisible=false;
        }
      else
        {
          currForm.flxCustomListBoxContainerCategory.isVisible=true;
        }
       currForm.forceLayout();
     }*/
});