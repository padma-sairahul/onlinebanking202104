define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnSendMoney **/
    AS_Button_d43c53014f034b41bf05b1d919a01882: function AS_Button_d43c53014f034b41bf05b1d919a01882(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for btnRequestMoney **/
    AS_Button_g958dffa6ff14836a56ef3436a898153: function AS_Button_g958dffa6ff14836a56ef3436a898153(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for btnEdit **/
    AS_Button_i745c7ff63c3438a8cf411a8c7d84be5: function AS_Button_i745c7ff63c3438a8cf411a8c7d84be5(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnDelete **/
    AS_Button_j29f1d9ee1e144f3b5ca078e8be011b0: function AS_Button_j29f1d9ee1e144f3b5ca078e8be011b0(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnViewActivity **/
    AS_Button_j5ce3dedd356412e952fa448aa9c78a2: function AS_Button_j5ce3dedd356412e952fa448aa9c78a2(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_f283c997ebe1477ca8031dabc5774bf1: function AS_FlexContainer_f283c997ebe1477ca8031dabc5774bf1(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});