define("CardManagementModuleNew/userfrmCardManagementCreateTravelPlanController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        notificationObject: {},
        countries: {},
        states: {},
        cities: {},
        selectedCountry: {},
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }

            if (viewPropertiesMap.AddNewTravelPlan) {
                this.showAddNewTravelPlan(viewPropertiesMap);
            }
            if (viewPropertiesMap.eligibleCards) {
                this.showSelectCardScreen(viewPropertiesMap.eligibleCards);
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
        },
        onBreakpointChange: function(form, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "My Cards", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
        },
        initActions: function() {
            var self = this;
            this.view.flxMangeTravelPlans.onClick = function() {
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchTravelNotifications();
            }
            this.view.flxContactUs.onClick = function() {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                informationContentModule.presentationController.showContactUsPage();
            };

            this.view.btnCancel1.onClick = function() {
                if (self.notificationObject.isEditFlow) {
                    self.notificationObject.isEditFlow = false;
                }
                self.fetchTravelNotifications();
            }
            this.view.btnContinue.onClick = function() {
                if (self.validateDateRange()) {
                    FormControllerUtility.showProgressBar(self.view);
                    self.view.lblWarningTravelPlan.setVisibility(false);
                    self.getDetailsOfNotification(self.notificationObject.isEditFlow);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.getEligibleCards();
                } else {
                    self.view.lblWarningTravelPlan.setVisibility(true);
                    var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                    CommonUtilities.setText(self.view.lblWarningTravelPlan, kony.i18n.getLocalizedString('i18n.CardManagement.invalidDateError'), accessibilityConfig);
                    self.view.calFrom.skin = ViewConstants.SKINS.SKNFF0000CAL;
                    self.view.calTo.skin = ViewConstants.SKINS.SKNFF0000CAL;
                }
            }
        },
        validateDateRange: function() {
            var startDateObject = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(this.view.calFrom.date, applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
            var endDateObject = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(this.view.calTo.date, applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
            if (startDateObject < endDateObject) {
                return true;
            }
            return false;
        },
        fetchTravelNotifications: function() {
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchTravelNotifications();
        },
        setSkinToCalendar: function() {
            this.view.calFrom.skin = ViewConstants.SKINS.COMMON_CALENDAR_NOERROR;
            this.view.calTo.skin = ViewConstants.SKINS.COMMON_CALENDAR_NOERROR;
            this.view.lblWarningTravelPlan.setVisibility(false);
        },
        showAddNewTravelPlan: function(locationData) {
            var self = this;
            this.setSkinToCalendar();
            if (locationData) {
                if (locationData.country) {
                    this.setCountryObject(locationData.country);
                }
                if (locationData.states) {
                    this.setStatesObject(locationData.states);
                }
                if (locationData.city) {
                    this.setCitiesObject(locationData.city)
                }
            }
            //TODO: check what this is about:
            if (self.view.lblManageTravelPlans.text === kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan')) {
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                self.view.lblRequestID.setVisibility(false);
                self.view.lblRequestNo.setVisibility(false);
                this.view.flxOtherDestinations.setVisibility(false);
                this.view.flxDestinationList.setVisibility(false);
                this.view.calFrom.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
                this.view.calTo.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
                this.view.calFrom.onSelection = self.setSkinToCalendar;
                this.view.calTo.onSelection = self.setSkinToCalendar;
                FormControllerUtility.disableButton(this.view.btnContinue);
                this.setDefaultDataForNotifications();
                this.view.lblAnotherDestination.skin = 'skn3343a8labelSSPRegular40Pr';
                this.view.txtPhoneNumber.onKeyUp = self.validateTravelPlanData.bind(this);
                this.setFlowActions();
                CommonUtilities.hideProgressBar(this.view)
            }
        },
        setFlowActions: function() {
            var scope = this;
            this.view.txtDestination.onKeyUp = function() {
                scope.showTypeAhead();
            };
            this.view.segDestinationList.onRowClick = function() {
                var rowNo = scope.view.segDestinationList.selectedRowIndex[1];
                var dest = scope.view.segDestinationList.data[rowNo].lblListBoxValues;
                scope.view.txtDestination.text = dest.text;
                scope.view.txtDestination.onKeyUp = function() {
                    scope.showTypeAhead();
                    if (dest === scope.view.txtDestination.text) {
                        scope.enableAddButton();
                    } else {
                        scope.disableAddButton();
                    }
                }
                scope.enableAddButton();
                scope.view.flxDestinationList.setVisibility(false);
                scope.view.txtDestination.setFocus();
            };
        },
        showTypeAhead: function() {
            var countryData = [];
            var newData = {};
            var stateId;
            var countryId;
            var key;
            var dataMap = {
                "flxCustomListBox": "flxCustomListBox",
                "lblListBoxValues": "lblListBoxValues"
            };
            var tbxText = this.toTitleCase(this.view.txtDestination.text);
            if (tbxText.length > 2) {
                for (key in this.cities) {
                    if (this.cities[key].name.indexOf('' + tbxText) === 0) {
                        stateId = this.cities[key].state_id;
                        countryId = this.cities[key].country_id;
                        newData = {
                            "countryId": countryId,
                            "lblListBoxValues": {
                                "text": this.countries[countryId].name + ", " + this.states[stateId].name + ", " + this.cities[key].name,
                                "accessibilityconfig": {
                                    "a11yLabel": this.countries[countryId].name + ", " + this.states[stateId].name + ", " + this.cities[key].name
                                }
                            },
                            "template": "flxCustomListBox"
                        };
                        countryData.push(newData);
                    }
                }
                for (key in this.states) {
                    if (this.states[key].name.indexOf('' + tbxText) === 0) {
                        countryId = this.states[key].country_id;
                        newData = {
                            "countryId": countryId,
                            "lblListBoxValues": {
                                "text": this.countries[countryId].name + ", " + this.states[key].name,
                                "accessibilityconfig": {
                                    "a11yLabel": this.countries[countryId].name + ", " + this.states[key].name
                                }
                            },
                            "template": "flxCustomListBox"
                        };
                        countryData.push(newData);
                    }
                }
                for (key in this.countries) {
                    if (this.countries[key].name.indexOf('' + tbxText) === 0) {
                        newData = {
                            "countryId": countryId,
                            "lblListBoxValues": {
                                "text": "" + this.countries[key].name,
                                "accessibilityconfig": {
                                    "a11yLabel": "" + this.countries[key].name
                                }
                            },
                            "template": "flxCustomListBox"
                        };
                        countryData.push(newData);
                    }
                }
                this.view.segDestinationList.widgetDataMap = dataMap;
                this.view.segDestinationList.setData(countryData);
                this.view.flxDestinationList.setVisibility(true);
                if (this.view.lblWarningTravelPlan.isVisible === true) this.view.flxDestinationList.top = 275 + "dp";
                else if (this.view.lblRequestID.isVisible === true && this.view.lblRequestNo.isVisible === true) this.view.flxDestinationList.top = 320 + "dp";
                else this.view.flxDestinationList.top = 245 + "dp";
                this.view.forceLayout();
            } else {
                this.view.flxDestinationList.setVisibility(false);
            }
        },

        /**
         * Method to get already selected cards for edit travel notification
         * @param {Object} - cards object
         */
        getBackendCards: function(data) {
            var cardNumber, cards = [];
            if (data) {
                if (data.indexOf(",")) {
                    data = data.split(",");
                    data.map(function(dataItem) {
                        cardNumber = dataItem.substring(dataItem.length - OLBConstants.MASKED_CARD_NUMBER_LENGTH, dataItem.length);
                        cards.push({
                            "number": cardNumber
                        });
                    });
                } else {
                    data.map(function(dataItem) {
                        cardNumber = dataItem.substring(dataItem.length - OLBConstants.MASKED_CARD_NUMBER_LENGTH, dataItem.length);
                        cards.push({
                            "number": cardNumber
                        });
                    });
                }
                this.notificationObject.selectedcards = cards;
            }
        },
        /**
         * Method to set destinations for edit travel notification
         * @param {Object} - locationsObject
         */
        setDestinations: function(data) {
            var self = this;
            var destinations = [];
            if (data) {
                var dataMap = {
                    "lblDestination": "lblDestination",
                    "lblPlace": "lblPlace",
                    "lblAnotherDestination": "lblAnotherDestination",
                    "lblSeparator2": "lblSeparator2",
                    "imgClose": "imgClose"
                };
                if (data.indexOf("-") > 0) {
                    data = data.split("-");
                    data.forEach(function(dataItem) {
                        var segData = {
                            "lblDestination": {
                                "text": kony.i18n.getLocalizedString('i18n.CardManagement.destination'),
                                "accessibilityconfig": {
                                    "a11yLabel": kony.i18n.getLocalizedString('i18n.CardManagement.destination')
                                }
                            },
                            "lblPlace": {
                                "text": dataItem,
                                "accessibilityconfig": {
                                    "a11yLabel": dataItem
                                }
                            },
                            "imgClose": {
                                "onTouchEnd": self.removeAddressFromList.bind(self)
                            },
                            "lblSeparator2": "a"
                        };
                        destinations.push(segData);
                    })
                } else {
                    var segData = {
                        "lblDestination": kony.i18n.getLocalizedString('i18n.CardManagement.destination'),
                        "lblPlace": {
                            "text": data,
                            "accessibilityconfig": {
                                "a11yLabel": data
                            }
                        },
                        "imgClose": {
                            "onTouchEnd": self.removeAddressFromList.bind(self)
                        },
                        "lblSeparator2": "a"
                    };
                    destinations.push(segData);
                }
                this.view.segDestinations.widgetDataMap = dataMap;
                destinations.forEach(function(item, value) {
                    item.lblDestination = kony.i18n.getLocalizedString("i18n.CardManagement.destination") + " " + (value + 1);
                })
                this.view.segDestinations.setData(destinations);
            }
            this.validateTravelPlanData();
        },
        /**
         * Method to remove address from the list of address for create/edit travel notification
         */
        removeAddressFromList: function() {
            var self = this;
            var index = this.view.segDestinations.selectedRowIndex;
            if (index) {
                this.view.segDestinations.removeAt(index[1]);
            }
            var data = this.view.segDestinations.data;
            data.forEach(function(item, value) {
                item.lblDestination = {
                    "text": kony.i18n.getLocalizedString("i18n.CardManagement.destination") + " " + (value + 1),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.destination") + " " + (value + 1)
                    }
                };
            });
            this.view.segDestinations.setData(data);
            self.validateTravelPlanData();
        },
        /**
         * Method to validate the data entered by user for creating/editing travel notification
         */
        validateTravelPlanData: function() {
            var self = this;
            var validationUtilityManager = applicationManager.getValidationUtilManager();
            if (this.view.txtPhoneNumber.text !== "") {
                if (validationUtilityManager.isValidPhoneNumber(this.view.txtPhoneNumber.text) && this.view.txtPhoneNumber.text.length == 10 && this.view.segDestinations.data.length > 0) {
                    FormControllerUtility.enableButton(this.view.btnContinue);
                } else {
                    FormControllerUtility.disableButton(this.view.btnContinue);
                }
            } else {
                FormControllerUtility.disableButton(this.view.btnContinue);
            }
        },
        enableAddButton: function() {
            var self = this;
            if (this.view.segDestinations.data.length < applicationManager.getConfigurationManager().numberOfLocations) {
                this.view.lblAnotherDestination.skin = 'skn3343a8labelSSPRegular';
                this.view.flxAddFeatureRequestandimg.onClick = self.addAddressTOList;
            }
        },
        /**
         * Method to disable Add button on create/edit travel notification screen
         */
        disableAddButton: function() {
            this.view.lblAnotherDestination.skin = 'skn3343a8labelSSPRegular40Pr';
            this.view.flxAddFeatureRequestandimg.onClick = null;
        },
        /**
         * Method to add selected address to the list of address for create/edit travel notification
         */
        addAddressTOList: function() {
            if (this.view.txtDestination.text !== "") {
                this.view.flxOtherDestinations.setVisibility(true);
                var self = this;
                var dataMap = {
                    "lblDestination": "lblDestination",
                    "lblPlace": "lblPlace",
                    "lblAnotherDestination": "lblAnotherDestination",
                    "lblSeparator2": "lblSeparator2",
                    "imgClose": "imgClose"
                };
                var data = [{
                    "lblDestination": kony.i18n.getLocalizedString('i18n.CardManagement.destination'),
                    "lblPlace": {
                        "text": this.view.txtDestination.text,
                        "accessibilityconfig": {
                            "a11yLabel": this.view.txtDestination.text
                        }
                    },
                    "imgClose": {
                        "onClick": self.removeAddressFromList
                    },
                    "lblSeparator2": "a"
                }];
                data[0].lblDestination = kony.i18n.getLocalizedString("i18n.CardManagement.destination") + " " + (this.view.segDestinations.data.length + 1);
                this.view.segDestinations.widgetDataMap = dataMap;
                this.view.segDestinations.addAll(data);
                this.view.txtDestination.text = "";
                self.disableAddButton();
                this.validateTravelPlanData();
            }
            this.view.forceLayout();
        },
        validateTravelPlanData: function() {
            var self = this;
            var validationUtilityManager = applicationManager.getValidationUtilManager();
            if (this.view.txtPhoneNumber.text !== "") {
                if (validationUtilityManager.isValidPhoneNumber(this.view.txtPhoneNumber.text) && this.view.txtPhoneNumber.text.length == 10 && this.view.segDestinations.data.length > 0) {
                    FormControllerUtility.enableButton(this.view.btnContinue);
                } else {
                    FormControllerUtility.disableButton(this.view.btnContinue);
                }
            } else {
                FormControllerUtility.disableButton(this.view.btnContinue);
            }
        },
        setDefaultDataForNotifications: function() {
            var self = this;
            if (this.notificationObject.isEditFlow === true) {
                this.view.flxOtherDestinations.setVisibility(true);
                this.view.lblRequestID.setVisibility(true);
                this.view.lblRequestNo.setVisibility(true);
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                CommonUtilities.setText(this.view.lblManageTravelPlans, kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan'), accessibilityConfig);
                this.view.lblManageTravelPlans.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan');
                this.view.flxMangeTravelPlans.onClick = function() {
                    self.notificationObject.isEditFlow = false;
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.AddNewTravelPlan();
                }
                CommonUtilities.setText(this.view.lblMyCardsHeader, kony.i18n.getLocalizedString('i18n.CardManagement.editTravelPlan'), accessibilityConfig);
                FormControllerUtility.enableButton(this.view.btnContinue);
            } else {
                this.view.txtDestination.text = "";
                this.view.txtPhoneNumber.text = "";
                this.view.txtareaUserComments.text = "";
                this.view.segDestinations.setData([]);
                this.view.segDestinationList.setData([]);
                this.notificationObject.selectedcards = [];
                this.blockFutureDateSelection(self.view.calFrom);
                CommonUtilities.disableOldDaySelection(this.view.calTo);
                this.view.calFrom.dateComponents = self.getDateComponents(kony.os.date(applicationManager.getFormatUtilManager().getDateFormat()));
                this.view.calTo.dateComponents = self.getDateComponents(kony.os.date(applicationManager.getFormatUtilManager().getDateFormat()));
                this.view.calFrom.dateEditable = false;
                this.view.calTo.dateEditable = false;
                this.view.txtPhoneNumber.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterPhoneNumber");
            }
        },
        /**
         * Method to get details of the notification for creating/updating travel notification
         */
        getDetailsOfNotification: function(isEditFlow) {
            var self = this;
            var selectedcards = self.notificationObject.selectedcards;
            var requestId = this.notificationObject.requestId;
            self.notificationObject = {
                'fromDate': this.view.calFrom.date,
                'toDate': this.view.calTo.date,
                'phone': this.view.txtPhoneNumber.text,
                'notes': this.view.txtareaUserComments.text,
                'locations': self.getSelectedLocations()
            };
            if (selectedcards) {
                self.notificationObject.selectedcards = selectedcards;
            }
            if (isEditFlow) {
                self.notificationObject.isEditFlow = isEditFlow;
                self.notificationObject.requestId = requestId
            }
            return self.notificationObject;
        },
        /**
         * Method to set countryObject for create/edit travel notification
         * @param {Object} - countryObject
         */
        setCountryObject: function(countryList) {
            var self = this
            countryList.forEach(function(element) {
                self.countries[element.id] = {
                    "name": element.Name
                }
            })
        },
        /**
         * Method to set statesObject for create/edit travel notification
         * @param {Object} - stateObject(regionObject)
         */
        setStatesObject: function(stateList) {
            var self = this
            stateList.forEach(function(element) {
                self.states[element.id] = {
                    "name": element.Name,
                    "country_id": element.Country_id
                }
            })
        },
        /**
         * Method to set cityObject for create/edit travel notification
         * @param {Object} - cityObject
         */
        setCitiesObject: function(cityList) {
            var self = this
            cityList.forEach(function(element) {
                self.cities[element.id] = {
                    "name": element.Name,
                    "state_id": element.Region_id,
                    "country_id": element.Country_id
                }
            })
        },
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
        },
        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
    };
});