define("CardManagementModuleNew/userfrmCardManagementDeactivateCardController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.lockCard) {
                this.setUpLockCard(viewPropertiesMap.lockCard)
            }
            if (viewPropertiesMap.TndCSuccessLockCard) {
                this.showTermsAndConditions(viewPropertiesMap.TndCSuccessLockCard);
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblHeading, kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblIns1, kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline1"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblIns2, kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline2"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblIns3, kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline3"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblIns4, kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline4"), accessibilityConfig);
            CommonUtilities.setText(this.view.btnConfirm, kony.i18n.getLocalizedString('i18n.common.proceed'), accessibilityConfig);
            this.view.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.proceed');
            CommonUtilities.setText(this.view.btnModify, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), accessibilityConfig);
            this.view.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            FormControllerUtility.disableButton(this.view.btnConfirm);
            CommonUtilities.setLblCheckboxState(false, this.view.lblRememberMeIcon);
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
        },
        onBreakpointChange: function(form, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            this.view.cardDetails.onBreakpointChange(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"), CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
        },
        initActions: function() {
            var self = this;
            this.view.btnTermsAndConditions.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.height = (self.view.flxMain.frame.height + 320) + "dp";
                self.view.flxTermsAndConditionsPopUp.setVisibility(true);
                self.view.flxDialogs.setVisibility(true);
                self.view.lblTermsAndConditions.setFocus(true);
                if (CommonUtilities.isFontIconChecked(self.view.lblRememberMeIcon)) {
                    CommonUtilities.setLblCheckboxState(true, self.view.lblTCContentsCheckboxIcon);
                } else {
                    CommonUtilities.setLblCheckboxState(false, self.view.lblTCContentsCheckboxIcon);
                }
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.showTermsAndConditionsLockCard();
            };
            this.view.btnCancel.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.setVisibility(false);
                self.view.flxDialogs.setVisibility(false);
            };
            this.view.flxClose.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.setVisibility(false);
                self.view.flxDialogs.setVisibility(false);
            };
            this.view.flxTCContentsCheckbox.onClick = function() {
                CommonUtilities.toggleFontCheckbox(self.view.lblTCContentsCheckboxIcon);
            };
            this.view.btnSave.onClick = function() {
                if (CommonUtilities.isFontIconChecked(self.view.lblTCContentsCheckboxIcon)) {
                    FormControllerUtility.enableButton(self.view.btnConfirm);
                    CommonUtilities.setLblCheckboxState(true, self.view.lblRememberMeIcon);
                } else {
                    FormControllerUtility.disableButton(self.view.btnConfirm);
                    CommonUtilities.setLblCheckboxState(false, self.view.lblRememberMeIcon);
                }
                self.view.flxTermsAndConditionsPopUp.setVisibility(false);
                self.view.flxDialogs.setVisibility(false);
            };
            this.view.flxCheckbox.onTouchEnd = function() {
                CommonUtilities.toggleFontCheckbox(self.view.lblRememberMeIcon);
                if (CommonUtilities.isFontIconChecked(self.view.lblRememberMeIcon)) {
                    FormControllerUtility.enableButton(self.view.btnConfirm);
                    CommonUtilities.setLblCheckboxState(true, self.viewlblRememberMeIcon);
                } else {
                    FormControllerUtility.disableButton(self.view.btnConfirm);
                    CommonUtilities.setLblCheckboxState(false, self.view.lblRememberMeIcon);
                }
            };
            this.view.btnModify.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.navigateToManageCards();
            };
        },
        setUpLockCard: function(card) {
            this.view.cardDetails.setData(card);
            var self = this;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblWarning, kony.i18n.getLocalizedString("i18n.CardManagement.LockingCard").replace('$CardType', card.cardType.toLowerCase()).replace('$CardNumber', card.cardNumber), accessibilityConfig);
            var params = {
                'card': card
            };
            if (CommonUtilities.isCSRMode()) {
                this.view.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.flxCheckbox.onTouchEnd = function() {
                    CommonUtilities.toggleFontCheckbox(self.view.lblRememberMeIcon);
                    if (CommonUtilities.isFontIconChecked(self.view.lblRememberMeIcon)) {
                        FormControllerUtility.enableButton(self.view.btnConfirm);
                        CommonUtilities.setLblCheckboxState(true, self.view.lblRememberMeIcon);
                    } else {
                        FormControllerUtility.disableButton(self.view.btnConfirm);
                        CommonUtilities.setLblCheckboxState(false, self.view.lblRememberMeIcon);
                    }
                }.bind(this);
                this.view.btnConfirm.onClick = self.initMFAFlow.bind(this, params, kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"));
            }
        },
        initMFAFlow: function(params, action) {
            CommonUtilities.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.verifySecureAccessCodeSuccess(params, action);
        },
        showTermsAndConditions: function(TnCcontent) {
            CommonUtilities.disableButton(this.view.btnConfirm);
            this.view.lblRememberMeIcon.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            this.view.flxIAgree.setVisibility(true);
            if (TnCcontent.contentTypeId === OLBConstants.TERMS_AND_CONDITIONS_URL) {
                this.view.btnTermsAndConditions.onClick = function() {
                    window.open(TnCcontent.termsAndConditionsContent);
                }
            } else {
                this.setTnCDATASection(TnCcontent.termsAndConditionsContent);
            }
            this.view.flxClose.onClick = this.hideTermsAndConditionPopUp;
        },
        setTnCDATASection: function(content) {
            this.view.rtxTC.text = content;
            this.view.flxTCContents.isVisible = false;
            if (document.getElementById("iframe_brwBodyTnC").contentWindow.document.getElementById("viewer")) {
                document.getElementById("iframe_brwBodyTnC").contentWindow.document.getElementById("viewer").innerHTML = content;
            } else {
                if (!document.getElementById("iframe_brwBodyTnC").newOnload) {
                    document.getElementById("iframe_brwBodyTnC").newOnload = document.getElementById("iframe_brwBodyTnC").onload;
                }
                document.getElementById("iframe_brwBodyTnC").onload = function() {
                    document.getElementById("iframe_brwBodyTnC").newOnload();
                    document.getElementById("iframe_brwBodyTnC").contentWindow.document.getElementById("viewer").innerHTML = content;
                };
            }
        },
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
        },
        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
    };
});