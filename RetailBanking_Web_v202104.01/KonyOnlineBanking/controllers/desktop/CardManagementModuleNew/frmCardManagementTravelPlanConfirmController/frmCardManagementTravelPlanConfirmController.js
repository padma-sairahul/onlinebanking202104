define("CardManagementModuleNew/userfrmCardManagementConfirmController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();

    return {
        /**
         * Globals for storing travel-notification,card-image, status-skin mappings.
         */
        notificationObject: {},
        cardImages: {},
        statusSkinsLandingScreen: {},
        statusSkinsDetailsScreen: {},
        travelNotificationDataMap: {},

        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.eligibleCards) {
                this.showSelectCardScreen(viewPropertiesMap.eligibleCards);
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            this.initializeCards();
            this.updateHamburgerMenu();
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Cards");
            this.initRightContainer();
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";

            applicationManager.executeAuthorizationFramework(this);
        },
        onBreakpointChange: function(form, width) {
            var scope = this;
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "My Cards", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
        },
        initActions: function() {
            var scopeObj = this;
            this.view.flxMangeTravelPlans.onClick = function() {
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchTravelNotifications();
            }
            this.view.flxContactUs.onClick = function() {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                informationContentModule.presentationController.showContactUsPage();
            };
        },
        /**
         * initializeCards - Method to initialize the globals.
         */
        initializeCards: function() {
            this.initializeCardImages();
            this.initializeStatusSkins();
        },
        /**
         * initializeStatusSkins - Method to initialize the status skins in globals.
         */
        initializeStatusSkins: function() {
            this.statusSkinsLandingScreen['Active'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_LANDING;
            this.statusSkinsLandingScreen['Issued'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_LANDING;
            this.statusSkinsLandingScreen['Locked'] = ViewConstants.SKINS.CARDS_LOCKED_STATUS_LANDING;
            this.statusSkinsLandingScreen['Reported Lost'] = ViewConstants.SKINS.CARDS_REPORTED_LOST_STATUS_LANDING;
            this.statusSkinsLandingScreen['Replace Request Sent'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Replaced'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Cancel Request Sent'] = ViewConstants.SKINS.CARDS_CANCEL_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Cancelled'] = ViewConstants.SKINS.CARDS_CANCELLED_STATUS_LANDING;
            this.statusSkinsLandingScreen['Inactive'] = ViewConstants.SKINS.CARDS_INACTIVE_STATUS_LANDING;
            this.statusSkinsDetailsScreen['Active'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Locked'] = ViewConstants.SKINS.CARDS_LOCKED_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Reported Lost'] = ViewConstants.SKINS.CARDS_REPORTED_LOST_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Replace Request Sent'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Replaced'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Cancel Request Sent'] = ViewConstants.SKINS.CARDS_CANCEL_REQUEST_SENT_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Cancelled'] = ViewConstants.SKINS.CARDS_CANCELLED_STATUS_DETAILS;
        },
        /**
         * initializeCardImages - Method to initialize the card images in globals.
         */
        initializeCardImages: function() {
            this.cardImages['My Platinum Credit Card'] = ViewConstants.IMAGES.PREMIUM_CLUB_CREDITS;
            this.cardImages['Gold Debit Card'] = ViewConstants.IMAGES.GOLDEN_CARDS;
            this.cardImages['Premium Club Credit Card'] = ViewConstants.IMAGES.PLATINUM_CARDS;
            this.cardImages['Shopping Card'] = ViewConstants.IMAGES.SHOPPING_CARDS;
            this.cardImages['Petro Card'] = ViewConstants.IMAGES.PETRO_CARDS;
            this.cardImages['Eazee Food Card'] = ViewConstants.IMAGES.EAZEE_FOOD_CARDS;
        },
        /**
         * Method to initialize the actions and tooltips for right side flex.
         */
        initRightContainer: function() {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblContactUs, kony.i18n.getLocalizedString("i18n.footer.contactUs"), accessibilityConfig);
            this.view.lblContactUs.toolTip = kony.i18n.getLocalizedString("i18n.footer.contactUs");
            CommonUtilities.setText(this.view.lblManageTravelPlans, kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans'), accessibilityConfig);
            this.view.lblManageTravelPlans.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans');
        },
        /**
         * Method to call function of presenter to fetch travel notifications
         */
        fetchTravelNotifications: function() {},
        /**
         * This function shows the masked Secure Access Code on click of the eye icon
         * @param {Object} - The textbox widget of the Secure Access Code.
         */
        toggleSecureAccessCodeMasking: function(widgetId) {
            widgetId.secureTextEntry = !(widgetId.secureTextEntry);
        },
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
            this.view.flxMyCardsView.setVisibility(false);
        },
        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
        /**
         * Method that updates Hamburger Menu.
         * @param {Object} sideMenuModel - contains the side menu view model.
         */
        updateHamburgerMenu: function() {
            // this.view.customheadernew.customhamburger.activateMenu("ACCOUNTS", "Card Management");
        },
        /**
         * Method to return cards names with line break for manage travel plan
         * @param {String} cards Cards names seperated by comma
         * @returns {String} cards name seperated by line break
         */
        returnCardDisplayName: function(cards) {
            return cards.replace(/,/g, "<br/>");
        },
        /**
         * Method to return frontend date for Travel Plan
         * @param {Date} date in yyyy-mm-dd format
         * @returns {String} dateString which has date in frontend date format
         */
        returnFrontendDate: function(date) {
            var dateString = CommonUtilities.getFrontendDateString(date);
            return dateString;
        },
        /**
         * Method to get already selected cards for edit travel notification
         * @param {Object} - cards object
         */
        getBackendCards: function(data) {
            var cardNumber, cards = [];
            if (data) {
                if (data.indexOf(",")) {
                    data = data.split(",");
                    data.map(function(dataItem) {
                        cardNumber = dataItem.substring(dataItem.length - OLBConstants.MASKED_CARD_NUMBER_LENGTH, dataItem.length);
                        cards.push({
                            "number": cardNumber
                        });
                    });
                } else {
                    data.map(function(dataItem) {
                        cardNumber = dataItem.substring(dataItem.length - OLBConstants.MASKED_CARD_NUMBER_LENGTH, dataItem.length);
                        cards.push({
                            "number": cardNumber
                        });
                    });
                }
                this.notificationObject.selectedcards = cards;
            }
        },
        /**
         * Method to get date component
         * @param {string} - Date string
         * @returns {Object} - dateComponent Object
         */
        getDateComponent: function(dateString) {
            var dateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dateString, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase());
            return [dateObj.getDate(), dateObj.getMonth() + 1, dateObj.getFullYear()];
        },
        /**
         * changeRowTemplate - Method to toggle row template(btween selected and unselected templates for My Cards) for selected row of segment
         */
        changeRowTemplate: function() {
            var index = this.view.segMyCards.selectedRowIndex;
            var rowIndex = index[1];
            var data = this.view.segMyCards.data;
            for (var i = 0; i < data.length; i++) {
                if (i == rowIndex) {
                    data[i].imgCollapse = {
                        "src": ViewConstants.IMAGES.ARRAOW_UP,
                        "accessibilityconfig": {
                            "a11yLabel": "View Details"
                        }
                    };
                    data[i].template = "flxMyCardsExpandedNew";

                } else {
                    data[i].imgCollapse = {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Details"
                        }
                    };
                    data[i].template = (responsiveUtils.isMobile) ? "flxMyCardsCollapsedMobile" : "flxMyCardsCollapsedNew";
                }
            }
            this.view.segMyCards.setData(data);
        },
        /**
         * Method to show cards selection screen
         * @param {Object} - cards object
         */
        showSelectCardScreen: function(cards) {
            var self = this;
            var internationEligibleCards = [];
            if (this.isInternationalPlan()) {
                cards.forEach(function(item) {
                    if (item.isInternational == "true") internationEligibleCards.push(item);
                });
                cards = internationEligibleCards;
            }
            if (cards.length > 0) {
                this.view.flxNoCardsError.setVisibility(false);
                this.view.flxMyCards.setVisibility(true);
                this.view.flxMyCards.top = "0dp";
                this.view.flxMyCards.skin = "sknFlxffffffShadowdddcdc";
                this.view.segMyCards.setVisibility(true);
                this.view.lblMyCardsHeader.left = "20dp";
                var widgetDataMap = {
                    "imgCard": "imgCard",
                    "lblTravelNotificationEnabled": "lblTravelNotificationEnabled",
                    "flxCheckBox": "flxCheckBox",
                    "lblCheckBox": "lblCheckBox",
                    "flxCardDetails": "flxCardDetails",
                    "flxDetailsRow1": "flxDetailsRow1",
                    "flxDetailsRow2": "flxDetailsRow2",
                    "flxDetailsRow3": "flxDetailsRow3",
                    "lblKey1": "lblKey1",
                    "lblKey2": "lblKey2",
                    "lblKey3": "lblKey3",
                    "rtxValue1": "rtxValue1",
                    "rtxValue2": "rtxValue2",
                    "rtxValue3": "rtxValue3",
                    "lblSeparator2": "lblSeparator2",
                    "lblSeperator3": "lblSeperator3",
                    "lblCardsSeperator": "lblCardsSeperator",
                    "imgChevron": "imgChevron"
                };
                var cardSegData = [];
                var card = {};
                cards.forEach(function(dataItem) {
                    card = {
                        "lblCardsSeperator": {
                            "text": ".",
                            "height": "105px"
                        },
                        "lblSeperator3": {
                            "text": "a",
                            "height": "1"
                        },
                        "imgCard": {
                            "src": self.getImageForCard(dataItem.cardProductName),
                        },
                        "flxCheckBox": {
                            "onClick": self.toggleSelectedCardCheckbox.bind(self),
                        },
                        "lblCheckBox": {
                            "text": self.isCardSelected(dataItem),
                            "accessibilityconfig": {
                                "a11yLabel": self.isCardSelected(dataItem)
                            }
                        },
                        "lblKey1": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.cardName") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.cardName") + ":"
                            }
                        },
                        "lblKey2": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber") + ":"
                            }
                        },
                        "lblKey3": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.internationalEligible") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.internationalEligible") + ":"
                            }
                        },
                        "rtxValue1": {
                            "text": dataItem.cardProductName,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.cardProductName
                            }
                        },
                        "rtxValue2": {
                            "text": dataItem.maskedCardNumber,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.maskedCardNumber
                            }
                        },
                        "rtxValue3": {
                            "text": dataItem.isInternational === "false" ? kony.i18n.getLocalizedString('i18n.common.no') : kony.i18n.getLocalizedString('i18n.common.yes'),
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.isInternational === "false" ? kony.i18n.getLocalizedString('i18n.common.no') : kony.i18n.getLocalizedString('i18n.common.yes')
                            }
                        },
                        "template": "flxSelectFromEligibleCards"
                    };
                    cardSegData.push(card);
                });
                this.view.segMyCards.widgetDataMap = widgetDataMap;
                this.view.segMyCards.setData(cardSegData);
                this.view.forceLayout();
                this.view.flxMyCardsView.setVisibility(true);
                this.view.flxEligibleCardsButtons.setVisibility(true);
                this.view.flxApplyForCards.setVisibility(false);
                this.view.btnCardsCancel.onClick = function() {
                    FormControllerUtility.showProgressBar(this.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchTravelNotifications();
                }
                this.view.btnCardsContinue.onClick = function() {
                    self.getSelectedCards();
                    self.showConfirmationScreen(self.notificationObject);
                }
            } else {
                this.view.flxNoCardsError.setVisibility(true);
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                CommonUtilities.setText(this.view.lblNoCardsError, kony.i18n.getLocalizedString('i18n.CardManagement.noInternationalCardError'), accessibilityConfig);
                this.view.flxMyCards.setVisibility(true);
                this.view.flxMyCards.top = "0dp";
                this.view.flxMyCards.skin = "slfbox";
                this.view.segMyCards.setVisibility(false);
                this.view.flxApplyForCards.setVisibility(true);
                this.view.flxEligibleCardsButtons.setVisibility(false);
                this.view.btnApplyForCard.setVisibility(true);
                CommonUtilities.setText(this.view.btnApplyForCard, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), accessibilityConfig);
                this.view.btnApplyForCard.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                this.view.btnApplyForCard.onClick = function() {
                    FormControllerUtility.showProgressBar(this.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchTravelNotifications();
                }
            }
            CommonUtilities.hideProgressBar(this.view);
        },
        isInternationalPlan: function() {
            var userCountry;
            var Country = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchUserAddresses();
            Country.forEach(function(dataItem) {
                if (dataItem.isPrimary === "true") userCountry = dataItem.CountryName;
            });
            var selectedCountries = this.notificationObject.locations.map(function(item) {
                var arr = item.split(",");
                return arr[0];
            });
            for (var key in selectedCountries) {
                if (selectedCountries[key] === userCountry) return false;
            }
            return true;
        },
        /**
         * Method to get selected cards for creating/editing travel notification
         */
        getSelectedCards: function() {
            var cards = this.view.segMyCards.data;
            var selectedcards = []
            cards.forEach(function(dataItem) {
                if (dataItem.lblCheckBox.text === "C") selectedcards.push({
                    "name": dataItem.rtxValue1.text,
                    "number": dataItem.rtxValue2.text
                });
            });
            this.notificationObject.selectedcards = selectedcards;
        },
        /**
         * Method to show confirmation screen for create/edit travel notification
         */
        showConfirmationScreen: function(data) {
            var self = this;

            this.view.flxMyCardsView.setVisibility(false);
            this.view.flxConfirm.setVisibility(true);

            this.view.flxDestination2.setVisibility(false);
            this.view.flxDestination3.setVisibility(false);
            this.view.flxDestination4.setVisibility(false);
            this.view.flxDestination5.setVisibility(false);
            this.view.flxErrorMessage.setVisibility(false);
            this.view.flxDownload.setVisibility(false);

            var accessibilityconfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblKey1, kony.i18n.getLocalizedString('i18n.CardManagement.SelectedStartDate'), accessibilityConfig);
            CommonUtilities.setText(this.view.rtxValue1, data.fromDate, accessibilityConfig);
            CommonUtilities.setText(this.view.lblKey2, kony.i18n.getLocalizedString('i18n.CardManagement.SelectedEndDate'), accessibilityConfig);
            CommonUtilities.setText(this.view.rtxValue2, data.toDate, accessibilityConfig);
            CommonUtilities.setText(this.view.lblDestination1, kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination1'), accessibilityConfig);
            CommonUtilities.setText(this.view.rtxDestination1, data.locations[0], accessibilityConfig);
            if (data.locations[1]) {
                this.view.flxDestination2.setVisibility(true);
                CommonUtilities.setText(this.view.lblDestination2, kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination2'), accessibilityConfig);
                CommonUtilities.setText(this.view.rtxDestination2, data.locations[1], accessibilityConfig);
            }
            if (data.locations[2]) {
                this.view.flxDestination3.setVisibility(true);
                CommonUtilities.setText(this.view.lblDestination3, kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination3'), accessibilityConfig);
                CommonUtilities.setText(this.view.rtxDestination3, data.locations[2], accessibilityConfig);
            }
            if (data.locations[3]) {
                this.view.flxDestination4.setVisibility(true);
                CommonUtilities.setText(this.view.lblDestination4, kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination4'), accessibilityConfig);
                CommonUtilities.setText(this.view.rtxDestination4, data.locations[3], accessibilityConfig);
            }
            if (data.locations[4]) {
                this.view.flxDestination5.setVisibility(true);
                CommonUtilities.setText(this.view.lblDestination5, kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination5'), accessibilityConfig);
                CommonUtilities.setText(this.view.rtxDestination5, data.locations[4], accessibilityConfig);
            }
            CommonUtilities.setText(this.view.lblKey5, kony.i18n.getLocalizedString('i18n.ProfileManagement.PhoneNumber'), accessibilityConfig);
            CommonUtilities.setText(this.view.rtxValue5, data.phone, accessibilityConfig);
            CommonUtilities.setText(this.view.lblKey6, kony.i18n.getLocalizedString('i18n.CardManagement.AddInformation'), accessibilityConfig);
            CommonUtilities.setText(this.view.rtxValue6, data.notes, accessibilityConfig);
            CommonUtilities.setText(this.view.lblKey4, kony.i18n.getLocalizedString('i18n.CardManagement.selectedCards'), accessibilityConfig);
            var cards = "";
            data.selectedcards.map(function(dataItem) {
                cards = cards + dataItem.name + "-" + dataItem.number + "<br/>";
            });
            CommonUtilities.setText(this.view.rtxValueA, cards, accessibilityConfig);
            CommonUtilities.setText(this.view.btnCancelPlan, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), accessibilityConfig);
            this.view.btnCancelPlan.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.btnCancelPlan.onClick = function() {
                self.showPopUp();
            }
            CommonUtilities.setText(this.view.btnModify, kony.i18n.getLocalizedString('i18n.common.modifiy'), accessibilityConfig);
            this.view.btnModify.tooltip = kony.i18n.getLocalizedString('i18n.common.modifiy');
            this.view.btnModify.onClick = function() {
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchTravelNotifications();
            }
            CommonUtilities.setText(this.view.btnConfirm, kony.i18n.getLocalizedString('i18n.common.confirm'), accessibilityConfig);
            this.view.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirm');
            if (this.notificationObject.isEditFlow) {
                this.view.btnConfirm.onClick = function() {
                    FormControllerUtility.showProgressBar(self.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.updateTravelNotifications(self.notificationObject);
                }
            } else {
                this.view.btnConfirm.onClick = function() {
                    FormControllerUtility.showProgressBar(self.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.createTravelNotification(self.notificationObject);
                }
            }
            this.view.forceLayout();
        },
        /**
         * Method to show confirmation pop up for create/edit travel notification
         */
        showPopUp: function() {
            var self = this;
            this.view.flxAlert.height = this.view.flxHeader.frame.height + this.view.flxMain.frame.height + this.view.flxFooter.frame.height + "dp";
            this.view.flxDialogs.setVisibility(true);
            this.view.flxAlert.setVisibility(true);
            this.view.CustomAlertPopup.lblHeading.setFocus(true);
            CommonUtilities.setText(this.view.CustomAlertPopup.lblPopupMessage, kony.i18n.getLocalizedString('i18n.CardManagement.cancelCreateMsg'), CommonUtilities.getaccessibilityConfig());
            this.view.CustomAlertPopup.btnYes.onClick = function() {
                self.view.flxAlert.setVisibility(false);
                this.view.flxDialogs.setVisibility(false);
                self.notificationObject = {};
                self.fetchTravelNotifications();
            }
            this.view.CustomAlertPopup.btnNo.onClick = function() {
                self.view.flxAlert.setVisibility(false);
                this.view.flxDialogs.setVisibility(false);
            }
            this.view.CustomAlertPopup.flxCross.onClick = function() {
                self.view.flxAlert.setVisibility(false);
                this.view.flxDialogs.setVisibility(false);
            }
        },
        /**
         * Method to validate whether a card is selected or not
         * @param {Object} - cards object
         */
        isCardSelected: function(dataItem) {
            var self = this;
            var cardNumber = dataItem.maskedCardNumber;
            var selectedcards = this.notificationObject.selectedcards;
            var txt = "D";
            if (selectedcards) {
                selectedcards.forEach(function(data) {
                    if (data.number === cardNumber) {
                        txt = "C";
                        FormControllerUtility.enableButton(self.view.btnCardsContinue);
                    }
                });
            } else {
                txt = "D";
            }
            return txt;
        },
        /**
         * Method to get the image for a given card product.
         * @param {String} - card product name.
         * @returns {String} - Name of image file.
         */
        getImageForCard: function(cardProductName) {
            if (cardProductName) return this.cardImages[cardProductName];
            return ViewConstants.IMAGES.GOLDEN_CARDS;
        },
        /**
         * Method to handle selection of cards on card selection screen
         */
        toggleSelectedCardCheckbox: function() {
            var self = this;
            var selectedIndex = this.view.segMyCards.selectedRowIndex[1];
            var data = this.view.segMyCards.data;
            for (var index = 0; index < data.length; index++) {
                if (index === selectedIndex) {
                    if (data[index].lblCheckBox.text === "C") {
                        data[index].lblCheckBox.text = "D";
                    } else {
                        data[index].lblCheckBox.text = "C";
                    }
                }
                this.view.segMyCards.setData(data);
            }
            self.checkContinue(data);
        },
        /**
         * Method to enable continue button,if any of the card is selected
         * @param {Object} - cards data
         */
        checkContinue: function(data) {
            var enable = false;
            data.forEach(function(dataItem) {
                if (dataItem.lblCheckBox.text === "C") enable = true;
            })
            if (enable) {
                FormControllerUtility.enableButton(this.view.btnCardsContinue)
            } else {
                FormControllerUtility.disableButton(this.view.btnCardsContinue)
            }
        },
        /**
         * viewCardDetailsMobile : Goes to view card details flex, mobile only function
         * @member of {frmCardManagementController}
         * @param {}
         * @return {}
         * @throws {}
         */
        viewCardDetailsMobile: function() {
            if (!responsiveUtils.isMobile) {
                return;
            }
            if (this.view.flxCardDetailsMobile.isVisible == true) {
                return;
            }
            var scope = this;
            var index = this.view.segMyCards.selectedRowIndex;
            var rowIndex = index[1];
            var data = [];
            data.push(this.view.segMyCards.data[rowIndex]);
            data[0].template = "flxMyCardsExpandedMobile"
            data[0].flxMyCards = {
                "onClick": null
            };
            data[0].flxBlankSpace2 = {
                "isVisible": true
            };
            if (data[0].btnAction1 != undefined) {
                var action1 = data[0].btnAction1.onClick;
                data[0].btnAction1 = {
                    "text": data[0].btnAction1.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action1();
                        scope.view.flxHeader.setFocus(true);
                    }
                };
            }
            if (data[0].btnAction2 != undefined) {
                var action2 = data[0].btnAction2.onClick;
                data[0].btnAction2 = {
                    "text": data[0].btnAction2.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action2();
                        scope.view.flxHeader.setFocus(true);
                    }
                };
            }
            if (data[0].btnAction3 != undefined) {
                var action3 = data[0].btnAction3.onClick;
                data[0].btnAction3 = {
                    "text": data[0].btnAction3.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action3();
                        scope.view.flxHeader.setFocus(true);
                    }
                };
            }
            if (data[0].btnAction4 != undefined) {
                var action4 = data[0].btnAction4.onClick;
                data[0].btnAction4 = {
                    "text": data[0].btnAction4.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action4();
                        scope.view.flxHeader.setFocus(true);
                    }
                };
            }
            if (data[0].btnAction5 != undefined) {
                var action5 = data[0].btnAction5.onClick;
                data[0].btnAction5 = {
                    "text": data[0].btnAction5.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action5();
                    }
                };
            }
            if (data[0].btnAction6 != undefined) {
                var action6 = data[0].btnAction6.onClick;
                data[0].btnAction6 = {
                    "text": data[0].btnAction6.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action6();
                    }
                };
            }
            if (data[0].btnAction7 != undefined) {
                var action7 = data[0].btnAction7.onClick;
                data[0].btnAction7 = {
                    "text": data[0].btnAction7.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action7();
                    }
                };
            }
            if (data[0].btnAction8 != undefined) {
                var action8 = data[0].btnAction8.onClick;
                data[0].btnAction8 = {
                    "text": data[0].btnAction8.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action8();
                    }
                };
            }
            data[0].flxBlankSpace2 = {
                "height": "5dp"
            }
            data[0].flxBlankSpace = {
                "height": "5dp"
            }
            var dataMap = {
                "btnAction1": "btnAction1",
                "btnAction2": "btnAction2",
                "btnAction3": "btnAction3",
                "btnAction4": "btnAction4",
                "btnAction5": "btnAction5",
                "btnAction6": "btnAction6",
                "btnAction7": "btnAction7",
                "btnAction8": "btnAction8",
                "flxActions": "flxActions",
                "flxBlankSpace1": "flxBlankSpace1",
                "flxBlankSpace2": "flxBlankSpace2",
                "flxBlankSpace": "flxBlankSpace",
                "flxCardDetails": "flxCardDetails",
                "flxCardHeader": "flxCardHeader",
                "flxCardImageAndCollapse": "flxCardImageAndCollapse",
                "lblCardsSeperator": "lblCardsSeperator",
                "flxCollapse": "flxCollapse",
                "flxDetailsRow1": "flxDetailsRow1",
                "flxDetailsRow10": "flxDetailsRow10",
                "flxDetailsRow2": "flxDetailsRow2",
                "flxDetailsRow3": "flxDetailsRow3",
                "flxDetailsRow4": "flxDetailsRow4",
                "flxDetailsRow5": "flxDetailsRow5",
                "flxDetailsRow6": "flxDetailsRow6",
                "flxDetailsRow7": "flxDetailsRow7",
                "flxDetailsRow8": "flxDetailsRow8",
                "flxDetailsRow9": "flxDetailsRow9",
                "flxMyCards": "flxMyCards",
                "flxMyCardsExpanded": "flxMyCardsExpanded",
                "flxRowIndicatorColor": "flxRowIndicatorColor",
                "lblIdentifier": "lblIdentifier",
                "lblSeparator1": "lblSeparator1",
                "lblSeparator2": "lblSeparator2",
                "lblSeperator": "lblSeperator",
                "imgCard": "imgCard",
                "imgCollapse": "imgCollapse",
                "lblCardHeader": "lblCardHeader",
                "lblCardStatus": "lblCardStatus",
                "lblTravelNotificationEnabled": "lblTravelNotificationEnabled",
                "lblKey1": "lblKey1",
                "lblKey10": "lblKey10",
                "lblKey2": "lblKey2",
                "lblKey3": "lblKey3",
                "lblKey4": "lblKey4",
                "lblKey5": "lblKey5",
                "lblKey6": "lblKey6",
                "lblKey7": "lblKey7",
                "lblKey8": "lblKey8",
                "lblKey9": "lblKey9",
                "rtxValue1": "rtxValue1",
                "rtxValue10": "rtxValue10",
                "rtxValue2": "rtxValue2",
                "rtxValue3": "rtxValue3",
                "rtxValue4": "rtxValue4",
                "rtxValue5": "rtxValue5",
                "rtxValue6": "rtxValue6",
                "rtxValue7": "rtxValue7",
                "rtxValue8": "rtxValue8",
                "rtxValue9": "rtxValue9"
            };
            this.view.segCardDetails.widgetDataMap = dataMap;
            this.view.segCardDetails.setData(data);
            this.view.flxBackToCards.onClick = function() {
                scope.view.flxMyCardsView.setVisibility(true);
                scope.view.flxCardDetailsMobile.setVisibility(false);
                scope.view.forceLayout();
            }
            this.view.flxHeader.setFocus(true);
            this.view.flxMyCardsView.isVisible = false;
            this.view.flxCardDetailsMobile.isVisible = true;
        },
        setMobileHeader: function(text) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, text, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", accessibilityConfig);
            }
        }
    };
});