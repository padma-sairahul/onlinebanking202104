define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_e818324ee32e4f68b01618fc7c7e661b: function AS_Button_e818324ee32e4f68b01618fc7c7e661b(eventobject) {
        var self = this;
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.presentationController.showAccountsDashboard();
    },
    /** init defined for CopyfrmCardManagement **/
    AS_Form_cb747f3777de49949c15e7dd4bb1935e: function AS_Form_cb747f3777de49949c15e7dd4bb1935e(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for CopyfrmCardManagement **/
    AS_Form_d67cd24a42ea4d54906bcfc1ce2e9a8e: function AS_Form_d67cd24a42ea4d54906bcfc1ce2e9a8e(eventobject) {
        var self = this;
        this.formPreShowFunction();
    },
    /** postShow defined for CopyfrmCardManagement **/
    AS_Form_d476def7f14544aaa9a261da2e7945ba: function AS_Form_d476def7f14544aaa9a261da2e7945ba(eventobject) {
        var self = this;
        this.PostShowfrmCardManagement();
    },
    /** onDeviceBack defined for CopyfrmCardManagement **/
    AS_Form_g88c8b9b4e484773b7f4ad7db7d80a3b: function AS_Form_g88c8b9b4e484773b7f4ad7db7d80a3b(eventobject) {
        var self = this;
        kony.print("on device back");
    },
    /** onTouchEnd defined for CopyfrmCardManagement **/
    AS_Form_e4f36917d32e46f4abd3024a2e4da4ae: function AS_Form_e4f36917d32e46f4abd3024a2e4da4ae(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});