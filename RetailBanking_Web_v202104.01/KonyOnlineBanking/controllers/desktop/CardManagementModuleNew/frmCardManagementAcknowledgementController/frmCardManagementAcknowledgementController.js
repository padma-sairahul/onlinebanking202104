define("CardManagementModuleNew/userfrmCardManagementAcknowledgementController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.actionAcknowledgement) {
                this.showAcknowledgementScreen(viewPropertiesMap.card, viewPropertiesMap.actionAcknowledgement);
            }
            if (viewPropertiesMap.notificationAcknowledgement) {
                this.showTravelNotificationAcknowledgement(viewPropertiesMap.notificationAcknowledgement);
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.btnBackToCards, kony.i18n.getLocalizedString("i18n.CardManagement.BackToCards"), accessibilityConfig);
            CommonUtilities.setText(this.view.CopylblHeading0c0c724e64d604a, kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblHeading, kony.i18n.getLocalizedString("i18n.CardManagement.CardDetails"), accessibilityConfig);
            this.view.btnBackToCards.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.BackToCards");
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
        },
        onBreakpointChange: function(form, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "My Cards", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
        },
        initActions: function() {
            var scopeObj = this;
            this.view.btnBackToCards.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.navigateToManageCards();
            };
        },
        showAcknowledgementScreen: function(card, action) {
            if (action === kony.i18n.getLocalizedString("i18n.CardManagement.ActivateCard")) {
                var self = this;
                CommonUtilities.hideProgressBar(self.view);
                this.view.btnRequestReplacement.setVisibility(false);
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                if (card.isExpiring === "0") {
                    CommonUtilities.setText(this.view.lblCardAcknowledgement, kony.i18n.getLocalizedString("i18n.CardManagement.ActivateACard"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblCardTransactionMessage, kony.i18n.getLocalizedString("i18n.CardManagement.CardActivated"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblHeading, kony.i18n.getLocalizedString("i18n.CardManagement.NewCardDetails"), accessibilityConfig);
                    this.view.lblUnlockCardMessage.setVisibility(false);
                    this.view.btnLearnAbout.setVisibility(false);
                } else {
                    CommonUtilities.setText(this.view.lblCardAcknowledgement, kony.i18n.getLocalizedString("i18n.CardManagement.ActivateRenewalCard"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblCardTransactionMessage, kony.i18n.getLocalizedString("i18n.CardManagement.RenewedCardActivated"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblHeading, kony.i18n.getLocalizedString("i18n.CardManagement.CardDetails"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblUnlockCardMessage, kony.i18n.getLocalizedString("i18n.CardManagement.CutOldCard"), accessibilityConfig);
                    this.view.lblUnlockCardMessage.setVisibility(true);
                    this.view.btnLearnAbout.setVisibility(true);
                }
                CommonUtilities.setText(this.view.lblRequestID, kony.i18n.getLocalizedString("i18n.CardManagement.requestId"), accessibilityConfig);
                CommonUtilities.setText(this.view.btnLearnAbout, kony.i18n.getLocalizedString("i18n.CardManagement.LearnMore"), accessibilityConfig);
                this.view.lblRequestID.setVisibility(true);
                this.view.lblRefrenceNumber.setVisibility(true);
                this.view.btnLearnAbout.onClick = function() {
                    window.open("https://www.google.com/");
                };

                var keyValueData = {};
                keyValueData[kony.i18n.getLocalizedString("i18n.CardManagement.Card")] = card.productName + " - " + card.maskedCardNumber;
                keyValueData[kony.i18n.getLocalizedString("i18n.ChequeBookReq.account")] = card.accountName + "...." + card.maskedAccountNumber.substring(card.maskedAccountNumber.length - 3);
                keyValueData[kony.i18n.getLocalizedString("i18n.CardManagement.NameOnCard")] = card.cardHolder;
            } else {
                this.showCardOperationAcknowledgement(card, action);
            }

        },
        showCardOperationAcknowledgement: function(card, action) {
            var self = this;
            CommonUtilities.hideProgressBar(self.view);
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            switch (action) {
                case kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"): {
                    this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.LockCardAcknowledgement"));
                    CommonUtilities.setText(this.view.lblCardAcknowledgement, kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblCardTransactionMessage, kony.i18n.getLocalizedString("i18n.CardManagement.AckMessage1"), accessibilityConfig);
                    this.view.lblUnlockCardMessage.setVisibility(true);
                    CommonUtilities.setText(this.view.lblUnlockCardMessage, kony.i18n.getLocalizedString("i18n.CardManagement.AckMessage2"), accessibilityConfig);
                    break;
                }
                case "Offline_Change_Pin":
                case kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"): {
                    this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePinAcknowledgement"));
                    CommonUtilities.setText(this.view.lblCardAcknowledgement, kony.i18n.getLocalizedString("i18n.CardManagement.ChangeCardPin"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblCardTransactionMessage, card.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.CardManagement.SuccessfulChangePinAckMessage") : kony.i18n.getLocalizedString("i18n.CardManagement.SucessfulChangePinRequestAckMessage"), accessibilityConfig);
                    this.view.lblUnlockCardMessage.setVisibility(false);
                    break;
                }
                case kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"): {
                    this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardAcknowledgement"));
                    CommonUtilities.setText(this.view.lblCardAcknowledgement, kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblCardTransactionMessage, kony.i18n.getLocalizedString("i18n.CardManagement.SuccessfulUnlockCardAckMessage"), accessibilityConfig);
                    this.view.lblUnlockCardMessage.setVisibility(false);
                    break;
                }
                case kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"): {
                    this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenCardAcknowledgement"));
                    CommonUtilities.setText(this.view.lblCardAcknowledgement, kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenLower"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblCardTransactionMessage, kony.i18n.getLocalizedString("i18n.CardManagement.SucessfulRequestAckMessage"), accessibilityConfig);
                    this.view.lblUnlockCardMessage.setVisibility(false);
                    CommonUtilities.setText(this.view.btnRequestReplacement, kony.i18n.getLocalizedString("i18n.CardManagement.RequestReplacement"), accessibilityConfig);
                    this.view.btnRequestReplacement.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.RequestReplacement");
                    this.view.btnRequestReplacement.setVisibility(true);
                    this.view.btnRequestReplacement.onClick = function() {
                        self.replaceCard(card);
                    };
                    break;
                }
                case kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"): {
                    this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardAcknowledgement"));
                    CommonUtilities.setText(this.view.lblCardAcknowledgement, kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblCardTransactionMessage, kony.i18n.getLocalizedString("i18n.CardManagement.SucessfulRequestAckMessage"), accessibilityConfig);
                    this.view.lblUnlockCardMessage.setVisibility(false);
                    break;
                }
                case kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"): {
                    this.setMobileHeader(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelAck"));
                    CommonUtilities.setText(this.view.lblCardAcknowledgement, kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblCardTransactionMessage, kony.i18n.getLocalizedString("i18n.cardsManagement.cancelMsg"), accessibilityConfig);
                    this.view.lblUnlockCardMessage.setVisibility(false);
                    break;
                }
            }
            var keyValueData = {};
            keyValueData[kony.i18n.getLocalizedString("i18n.CardManagement.CardHolder")] = card.cardHolder,
                keyValueData[kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber")] = card.cardNumber,
                keyValueData[kony.i18n.getLocalizedString("i18n.CardManagement.ValidThrough")] = card.validThrough,
                keyValueData[kony.i18n.getLocalizedString("i18n.CardManagement.ServiceProvider")] = card.serviceProvider
            if (card.cardType === 'Credit') {
                keyValueData[kony.i18n.getLocalizedString("i18n.accountDetail.creditLimit")] = card.creditLimit;
                keyValueData[kony.i18n.getLocalizedString("i18n.accountDetail.availableCredit")] = card.availableCredit;
            } else if (card.cardType === 'Debit') {
                keyValueData[kony.i18n.getLocalizedString("i18n.CardManagement.WithdrawalLimit")] = card.dailyWithdrawalLimit;
            }
            this.view.keyValueList.setData(keyValueData);
            this.view.flxPrint.onClick = this.printAcknowlegement;
        },
        showTravelNotificationAcknowledgement: function(response) {
            var self = this;
            var data = this.notificationObject;

            this.view.flxPrint.setVisibility(false);
            this.view.lblRequestID.setVisibility(false);
            this.view.lblRefrenceNumber.setVisibility(false);
            this.view.btnLearnAbout.setVisibility(false);

            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblCardAcknowledgement, kony.i18n.getLocalizedString('i18n.CardManagement.travelNotification'), accessibilityConfig);
            if (this.notificationObject.isEditFlow) {
                CommonUtilities.setText(this.view.lblCardTransactionMessage, kony.i18n.getLocalizedString('i18n.CardManagement.travelNotificationUpdateMsg'), accessibilityConfig);
                CommonUtilities.setText(this.view.lblUnlockCardMessage, kony.i18n.getLocalizedString('i18n.CardManagement.requestId') + ":" + this.notificationObject.requestId, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.lblCardTransactionMessage, kony.i18n.getLocalizedString('i18n.CardManagement.travelNotificationCreatationMsg'), accessibilityConfig);
                CommonUtilities.setText(this.view.lblUnlockCardMessage, kony.i18n.getLocalizedString('i18n.CardManagement.requestId') + ":" + response.data.request_id, accessibilityConfig);
            }
            CommonUtilities.setText(this.view.lblHeading, kony.i18n.getLocalizedString('i18n.CardManagement.travelDetails'), accessibilityConfig);

            var keyValueData = {};
            keyValueData[kony.i18n.getLocalizedString('i18n.CardManagement.SelectedStartDate')] = data.fromDate;
            keyValueData[kony.i18n.getLocalizedString('i18n.CardManagement.SelectedEndDate')] = data.toDate;
            keyValueData[kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination1')] = data.locations[0];

            if (data.locations[1]) {
                keyValueData[kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination2')] = data.locations[1];
            }
            if (data.locations[2]) {
                keyValueData[kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination3')] = data.locations[2];
            }
            if (data.locations[3]) {
                keyValueData[kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination4')] = data.locations[3];
            }
            if (data.locations[4]) {
                keyValueData[kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination5')] = data.locations[4];
            }
            keyValueData[kony.i18n.getLocalizedString('i18n.ProfileManagement.PhoneNumber')] = data.phone;
            keyValueData[kony.i18n.getLocalizedString('i18n.CardManagement.AddInformation')] = data.notes;

            var cards = "";
            data.selectedcards.map(function(dataItem) {
                cards = cards + dataItem.name + "-" + dataItem.number + "<br/>";
            });

            keyValueData[kony.i18n.getLocalizedString('i18n.CardManagement.selectedCards')] = cards;

            this.view.btnBackToCards.setVisibility(true);
            CommonUtilities.setText(this.view.btnBackToCards, kony.i18n.getLocalizedString('i18n.CardManagement.goToTravelPlan'), accessibilityConfig);
            this.view.btnBackToCards.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.goToTravelPlan');
            this.view.btnBackToCards.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchTravelNotifications();
            }
            this.view.btnRequestReplacement.setVisibility(true);
            CommonUtilities.setText(this.view.btnRequestReplacement, kony.i18n.getLocalizedString('i18n.CardManagement.BackToCards'), accessibilityConfig);
            this.view.btnRequestReplacement.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.BackToCards');
            this.view.btnRequestReplacement.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.navigateToManageCards();
            }

            CommonUtilities.hideProgressBar(this.view);
            this.view.forceLayout();
        },
        replaceCard: function(card) {
            var cardsController = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController;
            cardsController.cardActionReplace(card);
        },
        printAcknowlegement: function() {
            var acknowledgementData = [];
            acknowledgementData.push({
                key: kony.i18n.getLocalizedString('i18n.common.status'),
                value: this.view.lblCardTransactionMessage.text
            });
            var transactionDetails = this.view.keyValueList.getData();
            var keys = Object.keys(transactionDetails);
            var values = Object.values(transactionDetails);
            for (var i = 0; i < keys.length; i++) {
                acknowledgementData.push({
                    key: keys[i],
                    value: values[i]
                });
            }
            var tableList = [{
                tableHeader: kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement"),
                tableRows: acknowledgementData
            }]
            var viewModel = {
                moduleHeader: this.view.lblCardAcknowledgement.text,
                tableList: tableList
            };
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.showPrintPage({
                printKeyValueGroupModel: viewModel
            });
        },
        setMobileHeader: function(text) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, text, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", accessibilityConfig);
            }
        },
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
        },
        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
    };
});