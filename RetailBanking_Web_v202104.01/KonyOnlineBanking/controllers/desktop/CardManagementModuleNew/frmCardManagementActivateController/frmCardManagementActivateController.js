define("CardManagementModuleNew/userfrmCardManagementActivateController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.unlockCard) {
                this.setUpUnlockCard(viewPropertiesMap.unlockCard)
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblHeading2, kony.i18n.getLocalizedString("i18n.CardManagement.BenefitsOfUnlockingTheCard"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblIns1, kony.i18n.getLocalizedString("i18n.CardManagement.UnlockBenefit1"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblIns2, kony.i18n.getLocalizedString("i18n.CardManagement.UnlockBenefit2"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblIns3, kony.i18n.getLocalizedString("i18n.CardManagement.UnlockBenefit3"), accessibilityConfig);
            FormControllerUtility.disableButton(this.view.btnProceed);
            CommonUtilities.setLblCheckboxState(false, this.view.lblRememberMeIcon);
            CommonUtilities.setText(this.view.btnProceed, kony.i18n.getLocalizedString('i18n.common.proceed'), accessibilityConfig);
            this.view.btnProceed.toolTip = kony.i18n.getLocalizedString('i18n.common.proceed');
            CommonUtilities.setText(this.view.btnCancelUnlock, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), accessibilityConfig);
            this.view.btnCancelUnlock.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
        },
        onBreakpointChange: function(form, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"), CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
        },
        initActions: function() {
            var self = this;
            this.view.btnTermsAndConditions.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.setVisibility(true);
                self.view.flxDialogs.setVisibility(true);
                self.view.lblTermsAndConditions.setFocus(true);
                if (CommonUtilities.isFontIconChecked(self.view.lblRememberMeIcon)) {
                    CommonUtilities.setLblCheckboxState(true, self.view.lblTCContentsCheckboxIcon);
                } else {
                    CommonUtilities.setLblCheckboxState(false, self.view.lblTCContentsCheckboxIcon);
                }
            };
            this.view.btnCancel.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.setVisibility(false);
                self.view.flxDialogs.setVisibility(false);
            };
            this.view.flxClose.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.setVisibility(false);
                self.view.flxDialogs.setVisibility(false);
            };
            this.view.flxTCContentsCheckbox.onClick = function() {
                CommonUtilities.toggleFontCheckbox(self.view.lblTCContentsCheckboxIcon);
            };
            this.view.btnSave.onClick = function() {
                if (CommonUtilities.isFontIconChecked(self.view.lblTCContentsCheckboxIcon)) {
                    CommonUtilities.setLblCheckboxState(true, self.view.lblRememberMeIcon);
                    FormControllerUtility.enableButton(self.view.btnProceed);
                } else {
                    FormControllerUtility.disableButton(self.view.btnProceed);
                    CommonUtilities.setLblCheckboxState(false, self.view.lblRememberMeIcon);
                }
                self.view.flxTermsAndConditionsPopUp.setVisibility(false);
                self.view.flxDialogs.setVisibility(false);
            };
            this.view.btnCancelUnlock.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.navigateToManageCards();
            };
        },
        setUpUnlockCard: function(card) {
            var self = this;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblWarning, kony.i18n.getLocalizedString("i18n.CardManagement.UnlockingCard").replace('$CardType', card.cardType.toLowerCase()).replace('$CardNumber', card.cardNumber), accessibilityConfig);
            if (CommonUtilities.isCSRMode()) {
                this.view.btnProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.btnProceed.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.flxCheckbox.onTouchEnd = function() {
                    CommonUtilities.toggleFontCheckbox(self.view.lblRememberMeIcon);
                    if (CommonUtilities.isFontIconChecked(self.view.lblRememberMeIcon)) {
                        FormControllerUtility.enableButton(self.view.btnProceed);
                        CommonUtilities.setLblCheckboxState(true, self.view.lblTCContentsCheckboxIcon);
                    } else {
                        FormControllerUtility.disableButton(self.view.btnProceed);
                        CommonUtilities.setLblCheckboxState(false, self.view.lblTCContentsCheckboxIcon);
                    }
                }.bind(this);
                this.view.btnProceed.onClick = function() {
                    var params = {
                        'card': card
                    };
                    self.initMFAFlow.call(self, params, kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"));
                }.bind(this);
            }
        },
        initMFAFlow: function(params, action) {
            CommonUtilities.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.verifySecureAccessCodeSuccess(params, action);
        },
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
        },
        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
    };
});