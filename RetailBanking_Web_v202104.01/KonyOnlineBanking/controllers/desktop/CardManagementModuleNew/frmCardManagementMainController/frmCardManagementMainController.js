define("CardManagementModuleNew/userfrmCardManagementMainController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Globals for storing travel-notification,card-image, status-skin mappings.
         */
        cardImages: {},
        statusSkinsLandingScreen: {},
        statusSkinsDetailsScreen: {},
        actionPermissionMap: {},
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.travelStatus) {
                this.showCardsStatus(viewPropertiesMap.travelStatus);
                applicationManager.executeAuthorizationFramework(this, "applyForNewCards");
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            this.initializeCards();
            this.updateHamburgerMenu();
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Cards");
            this.initRightContainer();
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            this.setActionPermissionMap();
            applicationManager.executeAuthorizationFramework(this);
        },
        onBreakpointChange: function(form, width) {
            var scope = this;
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "My Cards", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
                var data = this.view.segMyCards.data;
                if (data !== undefined) {
                    data.forEach(function(e) {
                        e.template = "flxMyCardsCollapsedMobile";
                        e.flxMyCards = {
                            "clipBounds": false,
                            "skin": "sknFlxffffffShadowdddcdc",
                            "onClick": scope.viewCardDetailsMobile
                        }
                    });
                    scope.view.segMyCards.setData(data);
                    this.view.forceLayout();
                }
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
                var data = this.view.segMyCards.data;
                if (data !== undefined) {
                    data.forEach(function(e) {
                        e.template = "flxMyCardsCollapsed";
                        e.flxCollapse = {
                            "onClick": scope.changeRowTemplate
                        }
                    });
                    scope.view.segMyCards.setData(data);
                    this.view.forceLayout();
                }
            }
        },
        initActions: function() {
            var scopeObj = this;
            this.view.flxMangeTravelPlans.onClick = function() {
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchTravelNotifications();
            }
            this.view.flxContactUs.onClick = function() {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                informationContentModule.presentationController.showContactUsPage();
            };
        },
        /**
         * initializeCards - Method to initialize the globals.
         */
        initializeCards: function() {
            this.initializeCardImages();
            this.initializeStatusSkins();
        },
        /**
         * initializeStatusSkins - Method to initialize the status skins in globals.
         */
        initializeStatusSkins: function() {
            this.statusSkinsLandingScreen['Active'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_LANDING;
            this.statusSkinsLandingScreen['Locked'] = ViewConstants.SKINS.CARDS_LOCKED_STATUS_LANDING;
            this.statusSkinsLandingScreen['Reported Lost'] = ViewConstants.SKINS.CARDS_REPORTED_LOST_STATUS_LANDING;
            this.statusSkinsLandingScreen['Replace Request Sent'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Replaced'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Cancel Request Sent'] = ViewConstants.SKINS.CARDS_CANCEL_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Cancelled'] = ViewConstants.SKINS.CARDS_CANCELLED_STATUS_LANDING;
            this.statusSkinsLandingScreen['Inactive'] = ViewConstants.SKINS.CARDS_INACTIVE_STATUS_LANDING;
            this.statusSkinsLandingScreen['Issued'] = ViewConstants.SKINS.CARDS_ISSUED_STATUS_LANDING;
            this.statusSkinsLandingScreen['NearingExpiry'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_LANDING;

            this.statusSkinsDetailsScreen['Active'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Locked'] = ViewConstants.SKINS.CARDS_LOCKED_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Reported Lost'] = ViewConstants.SKINS.CARDS_REPORTED_LOST_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Replace Request Sent'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Replaced'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Cancel Request Sent'] = ViewConstants.SKINS.CARDS_CANCEL_REQUEST_SENT_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Cancelled'] = ViewConstants.SKINS.CARDS_CANCELLED_STATUS_DETAILS;
        },
        /**
         * initializeCardImages - Method to initialize the card images in globals.
         */
        initializeCardImages: function() {
            this.cardImages['My Platinum Credit Card'] = ViewConstants.IMAGES.PREMIUM_CLUB_CREDITS;
            this.cardImages['Gold Debit Card'] = ViewConstants.IMAGES.GOLDEN_CARDS;
            this.cardImages['Premium Club Credit Card'] = ViewConstants.IMAGES.PLATINUM_CARDS;
            this.cardImages['Shopping Card'] = ViewConstants.IMAGES.SHOPPING_CARDS;
            this.cardImages['Petro Card'] = ViewConstants.IMAGES.PETRO_CARDS;
            this.cardImages['Eazee Food Card'] = ViewConstants.IMAGES.EAZEE_FOOD_CARDS;
        },
        /**
         * Method to initialize the actions and tooltips for right side flex.
         */
        initRightContainer: function() {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblContactUs, kony.i18n.getLocalizedString("i18n.footer.contactUs"), accessibilityConfig);
            this.view.lblContactUs.toolTip = kony.i18n.getLocalizedString("i18n.footer.contactUs");
            CommonUtilities.setText(this.view.lblManageTravelPlans, kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans'), accessibilityConfig);
            this.view.lblManageTravelPlans.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans');
        },
        setActionPermissionMap: function() {
            this.actionPermissionMap = {
                "CARD_MANAGEMENT_LOCK_CARD": kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"),
                "CARD_MANAGEMENT_REPLACE_CARD": kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"),
                "CARD_MANAGEMENT_REPORT_CARD_STOLEN": kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"),
                "CARD_MANAGEMENT_CHANGE_PIN": kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"),
                "CARD_MANAGEMENT_CANCEL_CARD": kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"),
                "CARD_MANAGEMENT_UNLOCK_CARD": kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard")
            }
        },
        getValidActions: function() {
            var scope = this;
            return Object.keys(scope.actionPermissionMap).map(function(key) {
                return scope.actionPermissionMap[key];
            });
        },
        /**
         * Method to call function of presenter to fetch travel notifications
         */
        fetchTravelNotifications: function() {},
        /**
         * This function shows the masked Secure Access Code on click of the eye icon
         * @param {Object} - The textbox widget of the Secure Access Code.
         */
        toggleSecureAccessCodeMasking: function(widgetId) {
            widgetId.secureTextEntry = !(widgetId.secureTextEntry);
        },
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
            this.view.flxMyCardsView.setVisibility(false);
        },
        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
        /**
         * Method that updates Hamburger Menu.
         * @param {Object} sideMenuModel - contains the side menu view model.
         */
        updateHamburgerMenu: function() {
            // this.view.customheadernew.customhamburger.activateMenu("ACCOUNTS", "Card Management");
        },
        /**
         * changeRowTemplate - Method to toggle row template(btween selected and unselected templates for My Cards) for selected row of segment
         */
        changeRowTemplate: function(dataItem) {
            var index = this.view.segMyCards.selectedRowIndex;
            var rowIndex = index[1];
            var data = this.view.segMyCards.data;
            for (var i = 0; i < data.length; i++) {
                if (i == rowIndex) {
                    data[i].imgCollapse = {
                        "src": ViewConstants.IMAGES.ARRAOW_UP,
                        "accessibilityconfig": {
                            "a11yLabel": "View Details"
                        }
                    };
                    data[i].template = "flxMyCardsExpandedNew";

                } else {
                    data[i].imgCollapse = {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Details"
                        }
                    };
                    data[i].template = (responsiveUtils.isMobile) ? "flxMyCardsCollapsedMobile" : "flxMyCardsCollapsedNew";
                }
            }
            // this.view.segMyCards.setData(data);
            this.view.segMyCards.setDataAt(data[rowIndex], rowIndex, index[0]);
            this.view.forceLayout();
        },
        /**
         * Method to get the image for a given card product.
         * @param {String} - card product name.
         * @returns {String} - Name of image file.
         */
        getImageForCard: function(cardProductName) {
            if (cardProductName) return this.cardImages[cardProductName];
            return ViewConstants.IMAGES.GOLDEN_CARDS;
        },
        /**
         * showCardsStatus - Method that hides all other flexes except the Cards segment.
         * @param {Array} - Array of card ids.
         */
        showCardsStatus: function(cards) {
            var self = this;
            this.setCardsData(cards);
            CommonUtilities.setText(self.view.lblMyCardsHeader, kony.i18n.getLocalizedString("i18n.CardManagement.MyCards"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.hideProgressBar(this.view);
        },
        /**
         * setCardsData - Method that binds the cards data to the segment.
         * @param {Array} - Array of JSON objects of cards.
         */
        setCardsData: function(cards) {
            if (cards.data && cards.data.length <= 0) {
                this.showCardsNotAvailableScreen();
            } else {
                var self = this;
                var dataMap = {
                    "btnAction1": "btnAction1",
                    "btnAction2": "btnAction2",
                    "btnAction3": "btnAction3",
                    "btnAction4": "btnAction4",
                    "btnAction5": "btnAction5",
                    "btnAction6": "btnAction6",
                    "btnAction7": "btnAction7",
                    "btnAction8": "btnAction8",
                    "flxActions": "flxActions",
                    "flxBlankSpace1": "flxBlankSpace1",
                    "flxBlankSpace2": "flxBlankSpace2",
                    "flxCardDetails": "flxCardDetails",
                    "flxCardHeader": "flxCardHeader",
                    "flxCardImageAndCollapse": "flxCardImageAndCollapse",
                    "lblCardsSeperator": "lblCardsSeperator",
                    "flxCollapse": "flxCollapse",
                    "flxDetailsRow1": "flxDetailsRow1",
                    "flxDetailsRow10": "flxDetailsRow10",
                    "flxDetailsRow2": "flxDetailsRow2",
                    "flxDetailsRow3": "flxDetailsRow3",
                    "flxDetailsRow4": "flxDetailsRow4",
                    "flxDetailsRow5": "flxDetailsRow5",
                    "flxDetailsRow6": "flxDetailsRow6",
                    "flxDetailsRow7": "flxDetailsRow7",
                    "flxDetailsRow8": "flxDetailsRow8",
                    "flxDetailsRow9": "flxDetailsRow9",
                    "flxMyCards": "flxMyCards",
                    "flxMyCardsExpanded": "flxMyCardsExpanded",
                    "flxRowIndicatorColor": "flxRowIndicatorColor",
                    "lblIdentifier": "lblIdentifier",
                    "lblSeparator1": "lblSeparator1",
                    "lblSeparator2": "lblSeparator2",
                    "lblSeperator": "lblSeperator",
                    "imgCard": "imgCard",
                    "imgCollapse": "imgCollapse",
                    "lblChevron": "lblChevron",
                    "lblCardHeader": "lblCardHeader",
                    "lblCardStatus": "lblCardStatus",
                    "lblTravelNotificationEnabled": "lblTravelNotificationEnabled",
                    "lblKey1": "lblKey1",
                    "lblKey10": "lblKey10",
                    "lblKey2": "lblKey2",
                    "lblKey3": "lblKey3",
                    "lblKey4": "lblKey4",
                    "lblKey5": "lblKey5",
                    "lblKey6": "lblKey6",
                    "lblKey7": "lblKey7",
                    "lblKey8": "lblKey8",
                    "lblKey9": "lblKey9",
                    "rtxValue1": "rtxValue1",
                    "rtxValue10": "rtxValue10",
                    "rtxValue2": "rtxValue2",
                    "rtxValue3": "rtxValue3",
                    "rtxValue4": "rtxValue4",
                    "rtxValue5": "rtxValue5",
                    "rtxValue6": "rtxValue6",
                    "rtxValue7": "rtxValue7",
                    "rtxValue8": "rtxValue8",
                    "rtxValue9": "rtxValue9",
                    "segMyCardsExpanded": "segMyCardsExpanded"
                };
                var cardsSegmentData = [];
                var card = {};
                cards.data = this.constructCardsViewModel(cards.data);
                var travelStatusData = cards.status;
                for (var i = 0; i < cards.data.length; i++) {
                    var dataItem = cards.data[i];
                    card = {
                        "lblCardsSeperator": {
                            "text": ".",
                            "height": "105px"
                        },
                        "flxCollapse": {
                            "isVisible": dataItem.cardStatus === "Issued" ? false : true,
                            "onClick": self.changeRowTemplate.bind(self, dataItem)
                        },
                        "flxRowIndicatorColor": {
                            "height": "190Px",
                            "skin": "sknFlxF4BA22"
                        },
                        "lblSeperator": ".",
                        "lblSeparator1": ".",
                        "lblSeparator2": ".",
                        "imgCard": {
                            "src": self.getImageForCard(dataItem.productName),
                        },
                        "imgCollapse": {
                            "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig": {
                                "a11yLabel": "View Details"
                            }
                        },
                        "lblCardHeader": {
                            "text": dataItem.productName,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.productName
                            }
                        },
                        "lblCardStatus": {
                            "text": (dataItem.isExpiring === '1' && dataItem.cardStatus === "Active") ? kony.i18n.getLocalizedString("i18n.CardManagement.NearingExpiry") : self.geti18nDrivenString(dataItem.cardStatus),
                            "skin": (dataItem.isExpiring === '1' && dataItem.cardStatus === "Active") ? self.statusSkinsLandingScreen["NearingExpiry"] : self.statusSkinsLandingScreen[dataItem.cardStatus],
                            "accessibilityconfig": {
                                "a11yLabel": (dataItem.isExpiring === '1' && dataItem.cardStatus === "Active") ? kony.i18n.getLocalizedString("i18n.CardManagement.NearingExpiry") : self.geti18nDrivenString(dataItem.cardStatus)
                            }
                        },
                        "template": (responsiveUtils.isMobile) ? "flxMyCardsCollapsedMobile" : "flxMyCardsCollapsedNew",
                        "flxDetailsRow1": {
                            "isVisible": true
                        },
                        "flxDetailsRow2": {
                            "isVisible": true
                        },
                        "flxDetailsRow3": {
                            "isVisible": true
                        },
                        "flxDetailsRow4": {
                            "isVisible": true
                        },
                        "flxDetailsRow5": {
                            "isVisible": true
                        },
                        "flxDetailsRow6": {
                            "isVisible": true
                        },
                        "flxDetailsRow7": {
                            "isVisible": true
                        },
                        "flxDetailsRow8": {
                            "isVisible": dataItem.secondaryCardHolder ? true : false
                        },
                        "flxDetailsRow9": {
                            "isVisible": false
                        },
                        "lblKey1": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber") + ":"
                            }
                        },
                        "lblKey9": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.productName") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.productName") + ":"
                            }
                        },
                        "lblKey2": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.ValidThrough") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.ValidThrough") + ":"
                            }
                        },
                        "lblKey3": {
                            "text": dataItem.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.CardManagement.DailyWithdrawalLimit") + ":" : kony.i18n.getLocalizedString("i18n.accountDetail.creditLimit") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.CardManagement.DailyWithdrawalLimit") + ":" : kony.i18n.getLocalizedString("i18n.accountDetail.creditLimit") + ":"
                            }
                        },
                        "lblKey4": {
                            "text": dataItem.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.transfers.accountName") + ":" : kony.i18n.getLocalizedString("i18n.accountDetail.availableCredit") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.transfers.accountName") + ":" : kony.i18n.getLocalizedString("i18n.accountDetail.availableCredit") + ":"
                            }
                        },
                        "lblKey5": {
                            "text": dataItem.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.common.accountNumber") + ":" : kony.i18n.getLocalizedString("i18n.CardManagement.BillingAddress") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.common.accountNumber") + ":" : kony.i18n.getLocalizedString("i18n.CardManagement.BillingAddress") + ":"
                            }
                        },
                        "lblKey6": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.ServiceProvider") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.ServiceProvider") + ":"
                            }
                        },
                        "lblKey7": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.CardHolder") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.CardHolder") + ":"
                            }
                        },
                        "lblKey8": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.SecondaryHolder") + ":",
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.SecondaryHolder") + ":"
                            }
                        },
                        "rtxValue1": {
                            "text": dataItem.maskedCardNumber,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.maskedCardNumber
                            }
                        },
                        "rtxValue9": {
                            "text": dataItem.productName,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.productName
                            }
                        },
                        "rtxValue2": {
                            "text": dataItem.validThrough,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.validThrough
                            }
                        },
                        "rtxValue3": {
                            "text": dataItem.cardType === 'Debit' ? dataItem.dailyWithdrawalLimit : dataItem.creditLimit,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.cardType === 'Debit' ? dataItem.dailyWithdrawalLimit : dataItem.creditLimit
                            }
                        },
                        "rtxValue4": {
                            "text": dataItem.cardType === 'Debit' ? dataItem.accountName : dataItem.availableCredit,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.cardType === 'Debit' ? dataItem.accountName : dataItem.availableCredit
                            }
                        },
                        "rtxValue5": {
                            "text": dataItem.cardType === 'Debit' ? dataItem.maskedAccountNumber : dataItem.billingAddress,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.cardType === 'Debit' ? dataItem.maskedAccountNumber : dataItem.billingAddress
                            }
                        },
                        "rtxValue6": {
                            "text": dataItem.serviceProvider,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.serviceProvider
                            }
                        },
                        "rtxValue7": {
                            "text": dataItem.cardHolder,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.cardHolder
                            }
                        },
                        "rtxValue8": {
                            "text": dataItem.secondaryCardHolder,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.secondaryCardHolder
                            }
                        },
                        "lblTravelNotificationEnabled": {
                            "text": cards.status[i].status.toLowerCase() === "yes" ? kony.i18n.getLocalizedString('i18n.CardManagement.TravelNotificationsEnabled') : "",
                            "accessibilityconfig": {
                                "a11yLabel": cards.status[i].status.toLowerCase() === "yes" ? kony.i18n.getLocalizedString('i18n.CardManagement.TravelNotificationsEnabled') : ""
                            }
                        },
                        "flxMyCards": {
                            "clipBounds": false,
                            "skin": "sknFlxffffffShadowdddcdc",
                            "onClick": self.viewCardDetailsMobile
                        },
                        "lblChevron": {
                            "skin": "sknLblrightArrowFontIcon0273E3"
                        }
                    };
                    var actionButtonIndex;
                    for (var index = 0; index < dataItem.actions.length; index++) {
                        actionButtonIndex = Number(index) + 1;
                        card['btnAction' + actionButtonIndex] = self.getActionButton(dataItem, dataItem.actions[index]);
                    }
                    cardsSegmentData.push(card);
                }
                this.view.segMyCards.widgetDataMap = dataMap;
                this.view.segMyCards.setData(cardsSegmentData);
                this.view.forceLayout();
                this.view.flxMyCardsView.setVisibility(true);
            }
        },
        /**
         * showCardsNotAvailableScreen - Method to show no cards screen.
         */
        showCardsNotAvailableScreen: function() {
            this.view.flxMyCardsView.setVisibility(true);
            this.view.segMyCards.setVisibility(false);
            this.view.flxNoCardsError.setVisibility(true);
            this.view.flxApplyForCards.setVisibility(true);
            CommonUtilities.setText(this.view.lblNoCardsError, kony.i18n.getLocalizedString('i18n.CardsManagement.NocardsError'), CommonUtilities.getaccessibilityConfig());
            this.view.btnApplyForCard.onClick = function() {
                var naoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
                naoModule.presentationController.showNewAccountOpening();
            };
        },
        /**
         * getActionButton - Method to get a JSON for action button based on the action.
         * @param {Object, String} - card object.
         * @param {String} actino - contains the action to be performed.
         * @returns {Object} - JSON with text and onclick function for the button.
         */
        getActionButton: function(card, action) {
            return {
                'text': action,
                'toolTip': action,
                'onClick': this.getAction(card, action),
                'isVisible': true,
                'accessibilityconfig': {
                    'a11yLabel': action
                }
            };
        },
        /**
         * getAction - Method that actually returns the action to the action.
         * @param {Object, String} - card object .
         * @param {String} action - contains the action to be performed.
         * @returns {function} - Action for the given name.
         */
        getAction: function(card, action) {
            var cardsController = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController;
            switch (action) {
                case kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"): {
                    return cardsController.cardActionLock.bind(this, card);
                }
                case kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"): {
                    return cardsController.cardActionUnlock.bind(this, card);
                }
                case kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"): {
                    return cardsController.cardActionReplace.bind(this, card);
                }
                case kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"): {
                    return cardsController.cardActionReportLost.bind(this, card);
                }
                case kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"): {
                    return cardsController.cardActionCancel.bind(this, card);
                }
                case kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"): {
                    return cardsController.cardActionChangePin.bind(this, card);
                }
            }
        },
        /**
         * geti18nDrivenString - Returns i18n driven string for card status
         * @param {String} - card status.
         * @returns {String}  - i18n driven card status.
         */
        geti18nDrivenString: function(cardStatus) {
            if (cardStatus === OLBConstants.CARD_STATUS.Active) {
                return kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE");
            }
            if (cardStatus === OLBConstants.CARD_STATUS.Inactive) {
                return kony.i18n.getLocalizedString("i18n.CardManagement.inactive");
            }
            if (cardStatus === OLBConstants.CARD_STATUS.Cancelled) {
                return kony.i18n.getLocalizedString("i18n.CardManagement.cancelled");
            }
            if (cardStatus === OLBConstants.CARD_STATUS.ReportedLost) {
                return kony.i18n.getLocalizedString("i18n.CardManagement.reportedCardLost");
            }
            if (cardStatus === OLBConstants.CARD_STATUS.ReplaceRequestSent) {
                return kony.i18n.getLocalizedString("i18n.CardManagement.replaceRequestSent");
            }
            if (cardStatus === OLBConstants.CARD_STATUS.CancelRequestSent) {
                return kony.i18n.getLocalizedString("i18n.CardManagement.cancelRequestSent");
            }
            if (cardStatus === OLBConstants.CARD_STATUS.Locked) {
                return kony.i18n.getLocalizedString("i18n.CardManagement.locked");
            }
            if (cardStatus === OLBConstants.CARD_STATUS.Replaced) {
                return kony.i18n.getLocalizedString("i18n.CardManagement.replaced");
            }
            if (cardStatus === OLBConstants.CARD_STATUS.Issued) {
                return kony.i18n.getLocalizedString("i18n.CardManagement.PendingActivation");
            }
        },
        constructCardsViewModel: function(cards) {
            var self = this;
            var cardsViewModel = [];
            cards.forEach(function(card) {
                if (card.cardType === "Debit") {
                    cardsViewModel.push(self.getDebitCardViewModel(card));
                } else if (card.cardType === "Credit") {
                    cardsViewModel.push(self.getCreditCardViewModel(card));
                }
            });
            return cardsViewModel;
        },
        /**
         * getDebitCardViewModel - Generates a viewModel for the given debit card.
         * @param {Object} - Debit card object.
         * @returns {Object}  - constructed view model for debit card.
         */
        getDebitCardViewModel: function(debitCard) {
            var mfaManager = applicationManager.getMFAManager();
            mfaManager.setServiceId("SERVICE_ID_40");
            var debitCardViewModel = [];
            var debitCardActions = [];
            debitCardViewModel.cardId = debitCard.cardId;
            debitCardViewModel.cardType = debitCard.cardType;
            debitCardViewModel.cardStatus = debitCard.cardStatus;
            debitCardViewModel.cardNumber = debitCard.cardNumber;
            debitCardViewModel.maskedCardNumber = debitCard.maskedCardNumber;
            debitCardViewModel.productName = debitCard.cardProductName;
            debitCardViewModel.validThrough = this.getValidThroughForCard(debitCard.expiryDate);
            debitCardViewModel.dailyWithdrawalLimit = CommonUtilities.formatCurrencyWithCommas(debitCard.withdrawlLimit, false, debitCard.currencyCode);
            debitCardViewModel.accountName = debitCard.accountName;
            debitCardViewModel.maskedAccountNumber = debitCard.maskedAccountNumber;
            debitCardViewModel.serviceProvider = debitCard.serviceProvider;
            debitCardViewModel.cardHolder = debitCard.cardHolderName;
            debitCardViewModel.secondaryCardHolder = debitCard.secondaryCardHolder;
            switch (debitCard.cardStatus) {
                case "Active": {
                    debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"));
                    debitCardActions.push(kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
                    debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
                    debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"));
                    break;
                }
                case "Locked": {
                    debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"));
                    debitCardActions.push(kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
                    debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
                    break;
                }
                case "Reported Lost": {
                    //TODO:
                    break;
                }
                case "Replaced": {
                    debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
                    break;
                }
                case "Replace Request Sent": {
                    debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
                    break;
                }
                default: {
                    break;
                }
            }
            var validActions = this.getValidActions();
            debitCardActions = debitCardActions.filter(function(action) {
                return validActions.indexOf(action) > -1;
            });
            debitCardViewModel.actions = debitCardActions;
            return debitCardViewModel;
        },
        /**
         * getValidThroughForCard - Generates a validThrough/expiry date for a given timestamp.
         * @param {String} - Expiry Date Timestamp.
         * @returns {String}  - Expiry Date in mm/yy format.
         */
        getValidThroughForCard: function(expiryDate) {
            if (expiryDate === null || expiryDate === undefined || expiryDate === "") return "";
            expiryDate = expiryDate.split('T')[0];
            expiryDate = expiryDate.split('-');
            return expiryDate[1] + '/' + expiryDate[0].slice(2);
        },
        /**
         * getCreditCardViewModel - Generates a viewModel for the given credit card.
         * @param {Object} - credit card object.
         * @returns {Object}  - constructed view model for credit card.
         */
        getCreditCardViewModel: function(creditCard) {
            var mfaManager = applicationManager.getMFAManager();
            var creditCardViewModel = [];
            var creditCardActions = [];
            creditCardViewModel.cardId = creditCard.cardId;
            creditCardViewModel.cardType = creditCard.cardType;
            creditCardViewModel.cardStatus = creditCard.cardStatus;
            creditCardViewModel.cardNumber = creditCard.cardNumber;
            creditCardViewModel.maskedCardNumber = creditCard.maskedCardNumber;
            creditCardViewModel.productName = creditCard.cardProductName;
            creditCardViewModel.validThrough = this.getValidThroughForCard(creditCard.expiryDate);
            creditCardViewModel.creditLimit = CommonUtilities.formatCurrencyWithCommas(creditCard.creditLimit, false, creditCard.currencyCode);
            creditCardViewModel.availableCredit = CommonUtilities.formatCurrencyWithCommas(creditCard.availableCredit, false, creditCard.currencyCode);
            creditCardViewModel.serviceProvider = creditCard.serviceProvider;
            creditCardViewModel.cardHolder = creditCard.cardHolderName;
            creditCardViewModel.secondaryCardHolder = creditCard.secondaryCardHolderName;
            creditCardViewModel.billingAddress = creditCard.billingAddress;
            switch (creditCard.cardStatus) {
                case "Active": {
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"));
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"));
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
                    break;
                }
                case "Locked": {
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"));
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
                    break;
                }
                case "Reported Lost": {
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
                    break;
                }
                case "Replaced": {
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
                    break;
                }
                case "Replace Request Sent": {
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
                    creditCardActions.push(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
                    break;
                }
                default: {
                    break;
                }
            }
            var validActions = this.getValidActions();
            creditCardActions = creditCardActions.filter(function(action) {
                return validActions.indexOf(action) > -1;
            });
            creditCardViewModel.actions = creditCardActions;
            return creditCardViewModel;
        },
        /**
         * viewCardDetailsMobile : Goes to view card details flex, mobile only function
         * @member of {frmCardManagementController}
         * @param {}
         * @return {}
         * @throws {}
         */
        viewCardDetailsMobile: function() {
            if (!responsiveUtils.isMobile) {
                return;
            }
            if (this.view.flxCardDetailsMobile.isVisible == true) {
                return;
            }
            var scope = this;
            var index = this.view.segMyCards.selectedRowIndex;
            var rowIndex = index[1];
            var data = [];
            data.push(this.view.segMyCards.data[rowIndex]);
            data[0].template = "flxMyCardsExpandedMobile"
            data[0].flxMyCards = {
                "onClick": null
            };
            data[0].flxBlankSpace2 = {
                "isVisible": true
            };
            if (data[0].btnAction1 != undefined) {
                var action1 = data[0].btnAction1.onClick;
                data[0].btnAction1 = {
                    "text": data[0].btnAction1.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action1();
                        scope.view.flxHeader.setFocus(true);
                    }
                };
            }
            if (data[0].btnAction2 != undefined) {
                var action2 = data[0].btnAction2.onClick;
                data[0].btnAction2 = {
                    "text": data[0].btnAction2.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action2();
                        scope.view.flxHeader.setFocus(true);
                    }
                };
            }
            if (data[0].btnAction3 != undefined) {
                var action3 = data[0].btnAction3.onClick;
                data[0].btnAction3 = {
                    "text": data[0].btnAction3.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action3();
                        scope.view.flxHeader.setFocus(true);
                    }
                };
            }
            if (data[0].btnAction4 != undefined) {
                var action4 = data[0].btnAction4.onClick;
                data[0].btnAction4 = {
                    "text": data[0].btnAction4.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action4();
                        scope.view.flxHeader.setFocus(true);
                    }
                };
            }
            if (data[0].btnAction5 != undefined) {
                var action5 = data[0].btnAction5.onClick;
                data[0].btnAction5 = {
                    "text": data[0].btnAction5.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action5();
                    }
                };
            }
            if (data[0].btnAction6 != undefined) {
                var action6 = data[0].btnAction6.onClick;
                data[0].btnAction6 = {
                    "text": data[0].btnAction6.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action6();
                    }
                };
            }
            if (data[0].btnAction7 != undefined) {
                var action7 = data[0].btnAction7.onClick;
                data[0].btnAction7 = {
                    "text": data[0].btnAction7.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action7();
                    }
                };
            }
            if (data[0].btnAction8 != undefined) {
                var action8 = data[0].btnAction8.onClick;
                data[0].btnAction8 = {
                    "text": data[0].btnAction8.text,
                    "onClick": function() {
                        scope.view.flxCardDetailsMobile.isVisible = false;
                        action8();
                    }
                };
            }
            data[0].flxBlankSpace2 = {
                "height": "5dp"
            }
            data[0].flxBlankSpace = {
                "height": "5dp"
            }
            var dataMap = {
                "btnAction1": "btnAction1",
                "btnAction2": "btnAction2",
                "btnAction3": "btnAction3",
                "btnAction4": "btnAction4",
                "btnAction5": "btnAction5",
                "btnAction6": "btnAction6",
                "btnAction7": "btnAction7",
                "btnAction8": "btnAction8",
                "flxActions": "flxActions",
                "flxBlankSpace1": "flxBlankSpace1",
                "flxBlankSpace2": "flxBlankSpace2",
                "flxBlankSpace": "flxBlankSpace",
                "flxCardDetails": "flxCardDetails",
                "flxCardHeader": "flxCardHeader",
                "flxCardImageAndCollapse": "flxCardImageAndCollapse",
                "lblCardsSeperator": "lblCardsSeperator",
                "flxCollapse": "flxCollapse",
                "flxDetailsRow1": "flxDetailsRow1",
                "flxDetailsRow10": "flxDetailsRow10",
                "flxDetailsRow2": "flxDetailsRow2",
                "flxDetailsRow3": "flxDetailsRow3",
                "flxDetailsRow4": "flxDetailsRow4",
                "flxDetailsRow5": "flxDetailsRow5",
                "flxDetailsRow6": "flxDetailsRow6",
                "flxDetailsRow7": "flxDetailsRow7",
                "flxDetailsRow8": "flxDetailsRow8",
                "flxDetailsRow9": "flxDetailsRow9",
                "flxMyCards": "flxMyCards",
                "flxMyCardsExpanded": "flxMyCardsExpanded",
                "flxRowIndicatorColor": "flxRowIndicatorColor",
                "lblIdentifier": "lblIdentifier",
                "lblSeparator1": "lblSeparator1",
                "lblSeparator2": "lblSeparator2",
                "lblSeperator": "lblSeperator",
                "imgCard": "imgCard",
                "imgCollapse": "imgCollapse",
                "lblCardHeader": "lblCardHeader",
                "lblCardStatus": "lblCardStatus",
                "lblTravelNotificationEnabled": "lblTravelNotificationEnabled",
                "lblKey1": "lblKey1",
                "lblKey10": "lblKey10",
                "lblKey2": "lblKey2",
                "lblKey3": "lblKey3",
                "lblKey4": "lblKey4",
                "lblKey5": "lblKey5",
                "lblKey6": "lblKey6",
                "lblKey7": "lblKey7",
                "lblKey8": "lblKey8",
                "lblKey9": "lblKey9",
                "rtxValue1": "rtxValue1",
                "rtxValue10": "rtxValue10",
                "rtxValue2": "rtxValue2",
                "rtxValue3": "rtxValue3",
                "rtxValue4": "rtxValue4",
                "rtxValue5": "rtxValue5",
                "rtxValue6": "rtxValue6",
                "rtxValue7": "rtxValue7",
                "rtxValue8": "rtxValue8",
                "rtxValue9": "rtxValue9"
            };
            this.view.segCardDetails.widgetDataMap = dataMap;
            this.view.segCardDetails.setData(data);
            this.view.flxBackToCards.onClick = function() {
                scope.view.flxMyCardsView.setVisibility(true);
                scope.view.flxCardDetailsMobile.setVisibility(false);
                scope.view.forceLayout();
            }
            this.view.flxHeader.setFocus(true);
            this.view.flxMyCardsView.isVisible = false;
            this.view.flxCardDetailsMobile.isVisible = true;
        },
        setMobileHeader: function(text) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, text, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", accessibilityConfig);
            }
        }
    };
});