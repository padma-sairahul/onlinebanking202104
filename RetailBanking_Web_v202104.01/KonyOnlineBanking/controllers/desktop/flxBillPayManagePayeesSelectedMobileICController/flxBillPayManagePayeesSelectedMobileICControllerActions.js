define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btn1 **/
    AS_Button_a06e801b576146a1997db688e2451423: function AS_Button_a06e801b576146a1997db688e2451423(eventobject, context) {
        var self = this;
        //comment
    },
    /** onClick defined for btnEbill **/
    AS_Button_a257f0ea121140d597443cc738b82e70: function AS_Button_a257f0ea121140d597443cc738b82e70(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnViewEbill **/
    AS_Button_b646898459bc43d8833907f617a83903: function AS_Button_b646898459bc43d8833907f617a83903(eventobject, context) {
        var self = this;
        //comment
    },
    /** onClick defined for btn3 **/
    AS_Button_b8b82f276a914a9f82e5fe3533aeaebb: function AS_Button_b8b82f276a914a9f82e5fe3533aeaebb(eventobject, context) {
        var self = this;
        //comment
    },
    /** onClick defined for btn2 **/
    AS_Button_b99ecc86060544d6b76d82e5b4fe9251: function AS_Button_b99ecc86060544d6b76d82e5b4fe9251(eventobject, context) {
        var self = this;
        //comment
    },
    /** onClick defined for btnActions **/
    AS_Button_hf60d527592a403ab9be014945341945: function AS_Button_hf60d527592a403ab9be014945341945(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_e4bab795cded4f20a4fddaa4f8e4ca5a: function AS_FlexContainer_e4bab795cded4f20a4fddaa4f8e4ca5a(eventobject, context) {
        var self = this;
        this.segmentManagePayeesRowClick();
    }
});