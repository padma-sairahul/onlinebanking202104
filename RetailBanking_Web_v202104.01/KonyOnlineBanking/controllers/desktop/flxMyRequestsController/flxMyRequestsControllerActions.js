define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnReminder **/
    AS_Button_bea80a9d7ceb41c789879e78f31768db: function AS_Button_bea80a9d7ceb41c789879e78f31768db(eventobject, context) {
        var self = this;
        this.showSendReminder();
    },
    /** onClick defined for btnCancel **/
    AS_Button_ed17dca1123e42a4b150b08fc4ed11b7: function AS_Button_ed17dca1123e42a4b150b08fc4ed11b7(eventobject, context) {
        var self = this;
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_b8df2fcea65d496c9d360827b6330129: function AS_FlexContainer_b8df2fcea65d496c9d360827b6330129(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});