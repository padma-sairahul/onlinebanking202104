define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnPrint **/
    AS_Button_a071d057c7914235a9f69e53d590f7df: function AS_Button_a071d057c7914235a9f69e53d590f7df(eventobject, context) {
        var self = this;
        //
    },
    /** onClick defined for flxCheckImage2Icon **/
    AS_FlexContainer_ab343322586d4763bf61ab0bd4b40826: function AS_FlexContainer_ab343322586d4763bf61ab0bd4b40826(eventobject, context) {
        var self = this;
        this.showCheckImage();
    },
    /** onClick defined for flxRememberCategory **/
    AS_FlexContainer_b1ed566536d44fcd8816b69e972bb1f5: function AS_FlexContainer_b1ed566536d44fcd8816b69e972bb1f5(eventobject, context) {
        var self = this;
        this.rememberCategory();
    },
    /** onClick defined for flxCheckImageIcon **/
    AS_FlexContainer_ea5dbca6e5264d439c60daf2d2f16566: function AS_FlexContainer_ea5dbca6e5264d439c60daf2d2f16566(eventobject, context) {
        var self = this;
        this.showCheckImage();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_f7aafd365abc49e082dc59f85100c378: function AS_FlexContainer_f7aafd365abc49e082dc59f85100c378(eventobject, context) {
        var self = this;
        this.showUnselectedRow();
    }
});