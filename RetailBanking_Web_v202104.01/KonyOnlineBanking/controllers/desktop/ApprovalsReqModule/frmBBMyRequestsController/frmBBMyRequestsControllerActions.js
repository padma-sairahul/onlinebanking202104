define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmBBMyRequests **/
    AS_Form_b4dc1053c9d44ceeba46233b043f8948: function AS_Form_b4dc1053c9d44ceeba46233b043f8948(eventobject) {
        var self = this;
        applicationManager.getNavigationManager().applyUpdates(this);
        this.adjustScreen(0);
    },
    /** onDeviceBack defined for frmBBMyRequests **/
    AS_Form_da3980a97297427da0e9962cd8cb6967: function AS_Form_da3980a97297427da0e9962cd8cb6967(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmBBMyRequests **/
    AS_Form_e5b00fc8be54424ebe21caf5a3a28cbb: function AS_Form_e5b00fc8be54424ebe21caf5a3a28cbb(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** preShow defined for frmBBMyRequests **/
    AS_Form_fb6588dfb59543a38ac1843f939aa4c9: function AS_Form_fb6588dfb59543a38ac1843f939aa4c9(eventobject) {
        var self = this;
        this.onPreShow();
    },
    /** init defined for frmBBMyRequests **/
    AS_Form_fbd92de563384a3586dd3613c35c97ad: function AS_Form_fbd92de563384a3586dd3613c35c97ad(eventobject) {
        var self = this;
        this.onInit();
    }
});