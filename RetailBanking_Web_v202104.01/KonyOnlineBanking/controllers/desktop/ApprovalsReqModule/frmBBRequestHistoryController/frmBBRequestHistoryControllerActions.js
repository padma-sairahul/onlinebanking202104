define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for frmBBRequestHistory **/
    AS_Form_babaa2960cb546ea9d6a112d27a0389a: function AS_Form_babaa2960cb546ea9d6a112d27a0389a(eventobject) {
        var self = this;
        this.onPreShow();
    },
    /** onTouchEnd defined for frmBBRequestHistory **/
    AS_Form_d9522f8b01d24a3580ec67d39b7f275f: function AS_Form_d9522f8b01d24a3580ec67d39b7f275f(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** init defined for frmBBRequestHistory **/
    AS_Form_e7e805c8b16e458287ea5149afdd28de: function AS_Form_e7e805c8b16e458287ea5149afdd28de(eventobject) {
        var self = this;
        this.onInit();
    },
    /** postShow defined for frmBBRequestHistory **/
    AS_Form_g2d9e0c6c10346329f59e9c6f852073f: function AS_Form_g2d9e0c6c10346329f59e9c6f852073f(eventobject) {
        var self = this;
        applicationManager.getNavigationManager().applyUpdates(this);
        this.adjustScreen(0);
    },
    /** onDeviceBack defined for frmBBRequestHistory **/
    AS_Form_h1d5599155334af882c936a5b71e785c: function AS_Form_h1d5599155334af882c936a5b71e785c(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    }
});