define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmBBApprovalHistory **/
    AS_Form_f7def3c7d01541ab8159e6fd173aa980: function AS_Form_f7def3c7d01541ab8159e6fd173aa980(eventobject) {
        var self = this;
        applicationManager.getNavigationManager().applyUpdates(this);
        this.adjustScreen(0);
    },
    /** onDeviceBack defined for frmBBApprovalHistory **/
    AS_Form_fabc7c3173fc4362ac1f336443bcaffc: function AS_Form_fabc7c3173fc4362ac1f336443bcaffc(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** init defined for frmBBApprovalHistory **/
    AS_Form_g0c28f60382743b481572b721f5583eb: function AS_Form_g0c28f60382743b481572b721f5583eb(eventobject) {
        var self = this;
        this.onInit();
    },
    /** onTouchEnd defined for frmBBApprovalHistory **/
    AS_Form_i1223a4a9999412697e037e694ba41fa: function AS_Form_i1223a4a9999412697e037e694ba41fa(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** preShow defined for frmBBApprovalHistory **/
    AS_Form_icbc5cf501194fa797e61630affa4366: function AS_Form_icbc5cf501194fa797e61630affa4366(eventobject) {
        var self = this;
        this.onPreShow();
    }
});