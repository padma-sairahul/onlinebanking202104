define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_af390f32ff0846609959609f2c148ec1: function AS_FlexContainer_af390f32ff0846609959609f2c148ec1(eventobject, context) {
        var self = this;
        this.toogleExpandRow();
    },
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_h5fd0822279b4440ab057539ba2123c3: function AS_FlexContainer_h5fd0822279b4440ab057539ba2123c3(eventobject, context) {
        var self = this;
        this.toogleRowCheckBox();
    }
});