define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEdit **/
    AS_Button_ce9b325ecfea4add898ee6be8f0395ee: function AS_Button_ce9b325ecfea4add898ee6be8f0395ee(eventobject, context) {
        var self = this;
        return self.showEditEmail.call(this);
    },
    /** onClick defined for btnDelete **/
    AS_Button_dd2c39c9550f4411bdd7572da91b7085: function AS_Button_dd2c39c9550f4411bdd7572da91b7085(eventobject, context) {
        var self = this;
        return self.showDeletePopup.call(this);
    }
});