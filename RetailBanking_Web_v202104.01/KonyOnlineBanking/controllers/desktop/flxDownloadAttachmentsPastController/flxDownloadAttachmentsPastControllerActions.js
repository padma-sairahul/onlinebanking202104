define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxDownloadAttachment **/
    AS_FlexContainer_c098f086593c4c75b973b72b694b92df: function AS_FlexContainer_c098f086593c4c75b973b72b694b92df(eventobject, context) {
        var self = this;
        return self.downloadSingleFile.call(this);
    }
});