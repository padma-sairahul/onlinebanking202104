define("BusinessBanking/userfrmTransactionLimitsController", ['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function(CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {
    var orientationHandler = new OrientationHandler();
   
 
    return /** @alias module:frmUserManagementController */ {

        /**
         * Method to display the footer at the end of the screen by calculating the size of screen dynamically
         * @param {integer} data value
         **/
        compName: "",
        adjustScreen: function() {
            this.view.forceLayout();
            this.view.flxFooter.isVisible = true;
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                } else {
                    this.view.flxFooter.top = mainheight + "dp";
                }
                this.view.forceLayout();
            } else {
                this.view.flxFooter.top = mainheight + "dp";
                this.view.forceLayout();
            }
        },
        /**
         * Breakpont change
         */
        onBreakpointChange: function(width) {
            this.view.customheadernew.onBreakpointChangeComponent(width);
        },
        /**
         * hide all ui flexes in user management form
         */
        resetUI: function() {
            this.adjustScreen();
        },
        /**
         * Method will invoke on form init
         
        initActions: function() {
            var scopeObj = this;
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.btnProceedRoles.onClick = function() {
               // scopeObj.navToNextForm();
              this.loadBusinessBankingModule().presentationController.setTransactionLimitsFromUserManagement(limits);
            };
            this.view.btnTransactionLimits.onClick = function() {
                scopeObj.navToNextForm();
            };
            this.view.btnCancelRoles.onClick = function() {
				scopeObj.navToPrevForm();
                //this.loadBusinessBankingModule().presentationController.navigateToUMDashboard();
            };
            this.view.InfoIconPopup.flxCross.onClick = function() {
                this.view.InfoIconPopup.setVisibility(false);
            }.bind(this);
        },
		*/
        initActions: function() {
            var scopeObj = this;
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.btnProceedRoles.onClick = function() {
                // scopeObj.navToNextForm(); have to update the transactionlimit values
                //applicationManager.getNavigationManager().navigateTo("frmConfirmAndAck");
              scopeObj.updateGlobalLimits();
            };
            this.view.flxMainWrapper.isVisible = false;
            this.view.imgCloseDowntimeWarning.onTouchEnd = function() {
                scopeObj.view.flxMainWrapper.isVisible = false;
            };
            this.view.btnTransactionLimits.onClick = function() {
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
              if(limits.transactionLimits[0].accounts[0].featurePermissions[0].limits.length<8) scopeObj.features=false;
                if (scopeObj.features) {
                    scopeObj.navToNextForm();
                } else {
                    var err = "No transaction limits found for the user";
                    scopeObj.view.flxMainWrapper.isVisible = true;
                    scopeObj.view.flxDowntimeWarning.isVisible = true;
                    CommonUtilities.setText(scopeObj.view.lblDowntimeWarning, err, accessibilityConfig);
                }
            };
            this.view.btnCancelRoles.onClick = function() {
                //this.loadBusinessBankingModule().presentationController.navigateToUMDashboard();
              scopeObj.goBackFunction();
            }.bind(this);
            this.view.InfoIconPopup.flxCross.onClick = function() {
                this.view.InfoIconPopup.setVisibility(false);
            }.bind(this);
            //            this.loadBusinessBankingModule().presentationController.getCompaniesList();
        },
      
      goBackFunction : function(){
        applicationManager.getNavigationManager().navigateTo("frmConfirmAndAck");
      },
      
 updateGlobalLimits: function() {
            var scope = this;
            var flag = 0;
            var limitsSegData = scope.view.segIndividualTransactionLimits.data;
			var bulklimitsSegData = scope.view.segBulkTransactionLimits.data;
            this.limitsValues.forEach(function(dataItem) {
                if (compName === dataItem.companyName) {
                    if (Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segIndividualTransactionLimits.data[0].tbxPerTransactionAmt.text)) <= dataItem.limitGroups[0].limits[2].value) {
                        dataItem.limitGroups[0].limits[2].value = Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segIndividualTransactionLimits.data[0].tbxPerTransactionAmt.text));
                        flag = flag + 1;
                        limitsSegData[0].tbxPerTransactionAmt.skin = "CopysknBB1";
                    } else 
						limitsSegData[0].tbxPerTransactionAmt.skin = "sknborderff0000error";
                    if (Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segIndividualTransactionLimits.data[0].tbxDailyTransactionAmt.text)) <= dataItem.limitGroups[0].limits[0].value) {
                        dataItem.limitGroups[0].limits[0].value = Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segIndividualTransactionLimits.data[0].tbxDailyTransactionAmt.text));
                        flag = flag + 1;
                        limitsSegData[0].tbxDailyTransactionAmt.skin = "CopysknBB1";
                    } else limitsSegData[0].tbxDailyTransactionAmt.skin = "sknborderff0000error";
                    if (Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segIndividualTransactionLimits.data[0].tbxWeeklyTransactionAmt.text)) <= dataItem.limitGroups[0].limits[1].value) {
                        dataItem.limitGroups[0].limits[1].value = Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segIndividualTransactionLimits.data[0].tbxWeeklyTransactionAmt.text));
                        flag = flag + 1;
                        limitsSegData[0].tbxWeeklyTransactionAmt.skin = "CopysknBB1";
                    } else limitsSegData[0].tbxWeeklyTransactionAmt.skin = "sknborderff0000error";
                    if (Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segBulkTransactionLimits.data[0].tbxPerTransactionAmt.text)) <= dataItem.limitGroups[1].limits[2].value) {
                        dataItem.limitGroups[1].limits[2].value = Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segBulkTransactionLimits.data[0].tbxPerTransactionAmt.text));
                        flag = flag + 1;
						bulklimitsSegData[0].tbxPerTransactionAmt.skin = "CopysknBB1";
                    } else 
						bulklimitsSegData[0].tbxPerTransactionAmt.skin = "sknborderff0000error";
                    if (Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segBulkTransactionLimits.data[0].tbxDailyTransactionAmt.text)) <= dataItem.limitGroups[1].limits[0].value) {
                        dataItem.limitGroups[1].limits[0].value = Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segBulkTransactionLimits.data[0].tbxDailyTransactionAmt.text));
                        flag = flag + 1;
						bulklimitsSegData[0].tbxDailyTransactionAmt.skin = "CopysknBB1";
                    } else 
						bulklimitsSegData[0].tbxDailyTransactionAmt.skin = "sknborderff0000error";
                    if (Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segBulkTransactionLimits.data[0].tbxWeeklyTransactionAmt.text) )<= dataItem.limitGroups[1].limits[1].value) {
                        dataItem.limitGroups[1].limits[1].value = Number.parseInt(applicationManager.getFormatUtilManager().deFormatAmount(scope.view.segBulkTransactionLimits.data[0].tbxWeeklyTransactionAmt.text));
                        flag = flag + 1;
						bulklimitsSegData[0].tbxWeeklyTransactionAmt.skin = "CopysknBB1";
                    } else 
						bulklimitsSegData[0].tbxWeeklyTransactionAmt.skin = "sknborderff0000error";
                }
            });
            scope.view.segIndividualTransactionLimits.setData(limitsSegData);
			scope.view.segBulkTransactionLimits.setData(bulklimitsSegData);
            if (flag === 6) {
                this.loadBusinessBankingModule().presentationController.setTransactionLimitsFromUserManagement(this.limitsValues);
                applicationManager.getNavigationManager().navigateTo("frmConfirmAndAck");
                flag = 0;
            }
        },
        loadBusinessBankingModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
        },
        /**
         * Method will invoke on form pre show
         */
        preShow: function() {
            this.view.customheadernew.forceCloseHamburger();
          FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader', 'flxMain', 'flxFooter']);
          if (OLBConstants.CLIENT_PROPERTIES.ADVANCED_FEATURE_SELECTION.toString() === "true") {
            this.view.btnTransactionLimits.isVisible = true;
          } else {
            this.view.btnTransactionLimits.isVisible = false;
          }
            this.features = true;
            limits = this.loadBusinessBankingModule().presentationController.getUserManagementData();
          this.limitsValues = this.loadBusinessBankingModule().presentationController.getTransactionLimitsFromUserManagement();
            this.setCompany(limits);
        },
        showInfoIcon: function(eveObj, content) {
            this.view.InfoIconPopup.flxAccountType.setVisibility(false);
            this.view.InfoIconPopup.flxInformation.height = "80px";
            if (this.view.InfoIconPopup.isVisible === true) {
                this.view.InfoIconPopup.setVisibility(false);
                // Populate the correct flex here
            } else {
                this.view.InfoIconPopup.lblInfo.text = content.info;
                this.view.InfoIconPopup.setVisibility(true);
                this.view.InfoIconPopup.top = content.top + "dp";
                this.view.InfoIconPopup.left = content.left + "%";
            }
        },
        setCompany: function(limits) {
            var companyData = limits.transactionLimits.map(function(dataItem) {
                var len = dataItem.cif.length;
                var cif = dataItem.cif.substring(len - 4, len);
                var limit = {
                    "flxInnerRole": "flxInnerRole",
                    "flxLabels": "flxLabels",
                    "imgArrow": "imgArrow",
                    "lblPermision": {
                        "text": "default",
                    },
                    "lblRoleName": {
                        "text": dataItem.companyName + " - ..." + cif,
                    },
                    "cif": {
                        "text": dataItem.cif,
                    },
                };
                return limit;
            });
            companyData.forEach(function(arrayElement, i) {
                if (i === 0) {
                    arrayElement.imgArrow = {
                        "isVisible": true
                    };
                } else {
                    arrayElement.imgArrow = {
                        "isVisible": false
                    };
                }
            });
            this.view.segCustomRoles.setData(companyData);
            compName = limits.transactionLimits[0].companyName; //companyData[0].lblRoleName.text.slice(0, companyData[0].lblRoleName.text.length - 10);
            this.selectedCompany = companyData[0].lblRoleName.text;
            this.view.segCustomRoles.onRowClick = this.setLimit;
            this.view.btnViewNEdit.onClick = this.resetTodefault.bind(this, limits.transactionLimits[0].cif);
            // this.loadBusinessBankingModule().presentationController.getInfinityUserDetails(limits.transactionLimits[0].cif);
            this.setDataToTransactionLimits(limits);
            this.view.forceLayout();
        },
      resetTodefault: function() {
        limits = this.loadBusinessBankingModule().presentationController.getInitUserManagementData();
        this.setCompany(limits);
      },
        setLimit: function(context) {
            var selectedRow = context.selectedRowIndex;
            var selectedID = this.view.segCustomRoles.data[selectedRow[1]].cif.text;
             var companyName = this.view.segCustomRoles.data[selectedRow[1]].lblRoleName.text;
			compName = companyName.substring(0, companyName.length-10);
            var index = this.view.segCustomRoles.selectedRowIndex[1];
            var segDataval = this.view.segCustomRoles.data;
            segDataval.forEach(function(arrayElement, i) {
                if (i === index) {
                    arrayElement.imgArrow = {
                        "isVisible": true
                    };
                } else {
                    arrayElement.imgArrow = {
                        "isVisible": false
                    };
                }
            });
            this.view.segCustomRoles.setData(segDataval);
            this.view.btnViewNEdit.onClick = this.resetTodefault.bind(this, selectedID);
            // this.loadBusinessBankingModule().presentationController.getInfinityUserDetails(selectedID);
            this.setDataToTransactionLimits(limits);
        },
      setDataToTransactionLimits: function(companyDetails) {
        for(i in companyDetails.transactionLimits)
        {
          if(compName===companyDetails.transactionLimits[i].companyName)
          {
            var limitLength = companyDetails.transactionLimits[i].limitGroups.length;
            if (limitLength > 1) {
              this.setDataToIndividualTransactionLimits(companyDetails);
              this.setDataToBulkTransaction(companyDetails);
            } else if (limitLength === 1) 
			{
              var limitgroupID = companyDetails.transactionLimits[i].limitGroups[0].limitGroupId;
              if (limitgroupID === "SINGLE_PAYMENT") 
			  {
                this.setDataToIndividualTransactionLimits(companyDetails);
				this.setNoDataToBulkPayments();
			}
              else 
			  {
                this.setDataToBulkTransactionNew(companyDetails);
				this.setNoDataToSinglePayments();
			  }
            }
          }
        }
      },
	  
	  setNoDataToBulkPayments:function()
	  {
		  this.view.segBulkTransactionLimits.widgetDataMap = this.getWidgetDataMappingForNoTransactionLimits();
                finalresult = [
                    [{
                            "flxTransactionLimitValues": "flxTransactionLimitValues",
                            "flxGroupHeaderComp": {
                                isVisible: true,
                            },
                            "lblRowHeaderTitle": "Bulk Payment Limits",
                            "imgInfo": {
                                isVisible: false,
                            },
                        },
                        [{
                            "flxNoRecordFound": "flxNoRecordFound",
                            "flxMessageWrapper": "flxMessageWrapper",
                            "imgInfo1": {
                                isVisible: true,
                            },
                            "lblNoRecordsFound": "No Transaction limits found",
                        }]
                    ]
                ];
                this.view.segBulkTransactionLimits.rowTemplate = "flxNoRecordFound";
                this.features = false;
            this.view.segBulkTransactionLimits.setData(finalresult);
	  },
	  
	  setNoDataToSinglePayments:function()
	  {
		   this.view.segIndividualTransactionLimits.widgetDataMap = this.getWidgetDataMappingForNoTransactionLimits();
                finaldata = [
                    [{
                            "flxTransactionLimitValues": "flxTransactionLimitValues",
                            "flxGroupHeaderComp": {
                                isVisible: true,
                            },
                            "lblRowHeaderTitle": "Single Payment Limits",
                            "imgInfo": {
                                isVisible: false,
                            },
                        },
                        [{
                            "flxNoRecordFound": "flxNoRecordFound",
                            "flxMessageWrapper": "flxMessageWrapper",
                            "imgInfo1": {
                                isVisible: true,
                            },
                            "lblNoRecordsFound": "No Transaction limits found",
                        }]
                    ]
                ];
                this.view.segIndividualTransactionLimits.rowTemplate = "flxNoRecordFound";
                this.features = false;
            this.view.segIndividualTransactionLimits.setData(finaldata);
		  
	  },
	  
        setDataToIndividualTransactionLimits: function(companyDetails) {
            // for individual transaction limits
            var scope = this;
            this.view.segIndividualTransactionLimits.widgetDataMap = this.getWidgetDataMappingForTransactionLimits();
            var finaldata = [];
            var segData = companyDetails.transactionLimits.map(function(dataItem) {
                if (compName === dataItem.companyName) {
                    var values = {
                        "flxTransactionLimitValues": "flxTransactionLimitValues",
                        "flxGroupHeaderComp": {
                            isVisible: true,
                        },
                        "lblRowHeaderTitle": dataItem.limitGroups[0].limitGroupName ,
                        "imgInfo": {
                            "isVisible": true,
                            "onTouchEnd": function(eveObj, content) {
                                content = {
                                    left: "50",
                                    top: "540",
                                    info: dataItem.limitGroups[0].limitGroupDescription, //dataItem.limitGroups[1].limitGroupId,
                                };
                                scope.showInfoIcon(eveObj, content);
                            },
                        },
                        "flxPerTransactionLimit": "flxPerTransactionLimit",
                        "flxPerTrnasactionAmount": "flxPerTrnasactionAmount",
                        "flxPerTransactionCurrency": "flxPerTransactionCurrency",
                        "flxPerTransactioEnterAmount": "flxPerTransactioEnterAmount",
                        "lblCurrency":{
                          "text": applicationManager.getConfigurationManager().getCurrencyCode(),
                        },
                        "lblCurrency1":{
                          "text": applicationManager.getConfigurationManager().getCurrencyCode(),
                        },
                        "lblCurrency2":{
                          "text": applicationManager.getConfigurationManager().getCurrencyCode(),
                        },
                        "tbxPerTransactionAmt": {
                            "text": "" + applicationManager.getFormatUtilManager().formatAmount(dataItem.limitGroups[0].limits[2].value), //""+limitValues.singleMaxper,
                            "onKeyUp": scope.validateTransactionLimitsUI.bind(this, "segIndividualTransactionLimits")
                        },
                        "flxDailyTransactionLimit": "flxDailyTransactionLimit",
                        "flxDailyTrnasactionAmount": "flxDailyTrnasactionAmount",
                        "flxDailyTransactionCurrency": "flxDailyTransactionCurrency",
                        "flxDailyTransactioEnterAmount": "flxDailyTransactioEnterAmount",
                        "tbxDailyTransactionAmt": {
                            "text": "" +applicationManager.getFormatUtilManager().formatAmount(dataItem.limitGroups[0].limits[0].value), //""+limitValues.singleMaxDaily,
                            "onKeyUp": scope.validateTransactionLimitsUI.bind(this, "segIndividualTransactionLimits")
                        },
                        "flxWeeklyTransactionLimit": "flxWeeklyTransactionLimit",
                        "flxWeeklyTrnasactionAmount": "flxWeeklyTrnasactionAmount",
                        "flxWeeklyTransactionCurrency": "flxWeeklyTransactionCurrency",
                        "flxWeeklyTransactioEnterAmount": "flxWeeklyTransactioEnterAmount",
                        "tbxWeeklyTransactionAmt": {
                            "text": "" + applicationManager.getFormatUtilManager().formatAmount(dataItem.limitGroups[0].limits[1].value), //""+limitValues.singleMaxWeekly,
                            "onKeyUp": scope.validateTransactionLimitsUI.bind(this, "segIndividualTransactionLimits")
                        }
                    };
                    return values;
                }
            });
            for (i = 0; i < segData.length; i++) {
                if (segData[i] !== undefined)
                //  segData[i].pop();
                    finaldata.push(segData[i]);
            }
            if (finaldata.length !== 0) {
                this.view.segIndividualTransactionLimits.rowTemplate = "flxUserManagementTransactionLimits";
            } else {
			this.setNoDataToSinglePayments();
		}
		 this.view.segIndividualTransactionLimits.setData(finaldata); 
        },
        setDataToBulkTransaction: function(companyDetails) {
            // for bulk transaction limits
            var scope = this;
            var finalresult = [];
            this.view.segBulkTransactionLimits.widgetDataMap = this.getWidgetDataMappingForTransactionLimits();
            var segmentData = companyDetails.transactionLimits.map(function(dataItem) {
                if (compName === dataItem.companyName) {
                    var values = {
                        "flxTransactionLimitValues": "flxTransactionLimitValues",
                        "flxGroupHeaderComp": {
                            isVisible: true,
                        },
                        "lblRowHeaderTitle": dataItem.limitGroups[1].limitGroupName ,
                        "imgInfo": {
                            "isVisible": true,
                            "onTouchEnd": function(eveObj, content) {
                                content = {
                                    left: "47",
                                    top: "780",
                                    info: dataItem.limitGroups[1].limitGroupDescription,//dataItem.limitGroups[0].limitGroupId,
                                };
                                scope.showInfoIcon(eveObj, content);
                            },
                        },
                        "flxPerTransactionLimit": "flxPerTransactionLimit",
                        "flxPerTrnasactionAmount": "flxPerTrnasactionAmount",
                        "flxPerTransactionCurrency": "flxPerTransactionCurrency",
                        "flxPerTransactioEnterAmount": "flxPerTransactioEnterAmount",
                        "lblCurrency":{
                          "text": applicationManager.getConfigurationManager().getCurrencyCode(),
                        },
                        "lblCurrency1":{
                          "text": applicationManager.getConfigurationManager().getCurrencyCode(),
                        },
                        "lblCurrency2":{
                          "text": applicationManager.getConfigurationManager().getCurrencyCode(),
                        },
                        "tbxPerTransactionAmt": {
                            "text": "" + applicationManager.getFormatUtilManager().formatAmount(dataItem.limitGroups[1].limits[2].value), //""+limitValues.bulkMaxPer,
                            "onKeyUp": scope.validateTransactionLimitsUI.bind(this, "segBulkTransactionLimits")
                        },
                        "flxDailyTransactionLimit": "flxDailyTransactionLimit",
                        "flxDailyTrnasactionAmount": "flxDailyTrnasactionAmount",
                        "flxDailyTransactionCurrency": "flxDailyTransactionCurrency",
                        "flxDailyTransactioEnterAmount": "flxDailyTransactioEnterAmount",
                        "tbxDailyTransactionAmt": {
                            "text": "" +applicationManager.getFormatUtilManager().formatAmount( dataItem.limitGroups[1].limits[0].value), //""+limitValues.bulkMaxDaily,
                            "onKeyUp": scope.validateTransactionLimitsUI.bind(this, "segBulkTransactionLimits")
                        },
                        "flxWeeklyTransactionLimit": "flxWeeklyTransactionLimit",
                        "flxWeeklyTrnasactionAmount": "flxWeeklyTrnasactionAmount",
                        "flxWeeklyTransactionCurrency": "flxWeeklyTransactionCurrency",
                        "flxWeeklyTransactioEnterAmount": "flxWeeklyTransactioEnterAmount",
                        "tbxWeeklyTransactionAmt": {
                            "text": "" + applicationManager.getFormatUtilManager().formatAmount (dataItem.limitGroups[1].limits[1].value), //""+limitValues.bulkMaxWeekly,
                            "onKeyUp": scope.validateTransactionLimitsUI.bind(this, "segBulkTransactionLimits")
                        }
                    };
                    return values;
                }
            });
            for (i = 0; i < segmentData.length; i++) {
                if (segmentData[i] !== undefined)
                //   segmentData[i].pop();
                    finalresult.push(segmentData[i]);
            }
            if (finalresult.length !== 0) {
                this.view.segBulkTransactionLimits.rowTemplate = "flxUserManagementTransactionLimits";
            } else {
			this.setNoDataToBulkPayments(); 
		}
		this.view.segBulkTransactionLimits.setData(finalresult); 
            this.view.forceLayout();
        },
        setDataToBulkTransactionNew: function(companyDetails) {
            // for bulk transaction limits
            var scope = this;
            var finalresult = [];
            this.view.segBulkTransactionLimits.widgetDataMap = this.getWidgetDataMappingForTransactionLimits();
            var segmentData = companyDetails.transactionLimits.map(function(dataItem) {
                if (compName === dataItem.companyName) {
                    var values = {
                        "flxTransactionLimitValues": "flxTransactionLimitValues",
                        "flxGroupHeaderComp": {
                            isVisible: true,
                        },
                        "lblRowHeaderTitle": dataItem.limitGroups[0].limitGroupName ,
                        "imgInfo": {
                            "isVisible": true,
                            "onTouchEnd": function(eveObj, content) {
                                content = {
                                    left: "47",
                                    top: "780",
                                    info: dataItem.limitGroups[0].limitGroupDescription, //dataItem.limitGroups[0].limitGroupId,
                                };
                                scope.showInfoIcon(eveObj, content);
                            },
                        },
                        "flxPerTransactionLimit": "flxPerTransactionLimit",
                        "flxPerTrnasactionAmount": "flxPerTrnasactionAmount",
                        "flxPerTransactionCurrency": "flxPerTransactionCurrency",
                        "flxPerTransactioEnterAmount": "flxPerTransactioEnterAmount",
                        "lblCurrency":{
                            "text": applicationManager.getConfigurationManager().getCurrencyCode(),
                          },
                          "lblCurrency1":{
                            "text": applicationManager.getConfigurationManager().getCurrencyCode(),
                          },
                          "lblCurrency2":{
                            "text": applicationManager.getConfigurationManager().getCurrencyCode(),
                          },
                        "tbxPerTransactionAmt": {
                            "text": "" +applicationManager.getFormatUtilManager().formatAmount( dataItem.limitGroups[0].limits[2].value), //""+limitValues.bulkMaxPer,
                            "onKeyUp": scope.validateTransactionLimitsUI.bind(this, "segBulkTransactionLimits")
                        },
                        "flxDailyTransactionLimit": "flxDailyTransactionLimit",
                        "flxDailyTrnasactionAmount": "flxDailyTrnasactionAmount",
                        "flxDailyTransactionCurrency": "flxDailyTransactionCurrency",
                        "flxDailyTransactioEnterAmount": "flxDailyTransactioEnterAmount",
                        "tbxDailyTransactionAmt": {
                            "text": "" +applicationManager.getFormatUtilManager().formatAmount( dataItem.limitGroups[0].limits[0].value), //""+limitValues.bulkMaxDaily,
                            "onKeyUp": scope.validateTransactionLimitsUI.bind(this, "segBulkTransactionLimits")
                        },
                        "flxWeeklyTransactionLimit": "flxWeeklyTransactionLimit",
                        "flxWeeklyTrnasactionAmount": "flxWeeklyTrnasactionAmount",
                        "flxWeeklyTransactionCurrency": "flxWeeklyTransactionCurrency",
                        "flxWeeklyTransactioEnterAmount": "flxWeeklyTransactioEnterAmount",
                        "tbxWeeklyTransactionAmt": {
                            "text": "" + applicationManager.getFormatUtilManager().formatAmount (dataItem.limitGroups[0].limits[1].value), //""+limitValues.bulkMaxWeekly,
                            "onKeyUp": scope.validateTransactionLimitsUI.bind(this, "segBulkTransactionLimits")
                        }
                    };
                    return values;
                }
            });
            for (i = 0; i < segmentData.length; i++) {
                if (segmentData[i] !== undefined)
                //   segmentData[i].pop();
                    finalresult.push(segmentData[i]);
            }
            if (finalresult.length !== 0) {
                this.view.segBulkTransactionLimits.rowTemplate = "flxUserManagementTransactionLimits";
            } else {
			this.setNoDataToBulkPayments();
		}
		this.view.segBulkTransactionLimits.setData(finalresult); 
            this.view.forceLayout();
        },
		
        getWidgetDataMappingForNoTransactionLimits: function() {
            var mapping = {
                "flxTransactionLimitValues": "flxTransactionLimitValues",
                "flxGroupHeaderComp": "flxGroupHeaderComp",
                "lblRowHeaderTitle": "lblRowHeaderTitle",
                "imgInfo": "imgInfo",
                "flxNoRecordFound": "flxNoRecordFound",
                "flxMessageWrapper": "flxMessageWrapper",
                "imgInfo1": "imgInfo1",
                "lblNoRecordsFound": "lblNoRecordsFound"
            };
            return mapping;
        },
        getWidgetDataMappingForTransactionLimits: function() {
            var mapping = {
                "flxTransactionLimitValues": "flxTransactionLimitValues",
                "flxGroupHeaderComp": "flxGroupHeaderComp",
                "lblRowHeaderTitle": "lblRowHeaderTitle",
                "imgInfo": "imgInfo",
                "flxPerTransactionLimit": "flxPerTransactionLimit",
                "flxPerTrnasactionAmount": "flxPerTrnasactionAmount",
                "flxPerTransactionCurrency": "flxPerTransactionCurrency",
                "flxPerTransactioEnterAmount": "flxPerTransactioEnterAmount",
                "tbxPerTransactionAmt": "tbxPerTransactionAmt",
                "flxDailyTransactionLimit": "flxDailyTransactionLimit",
                "flxDailyTrnasactionAmount": "flxDailyTrnasactionAmount",
                "flxDailyTransactionCurrency": "flxDailyTransactionCurrency",
                "flxDailyTransactioEnterAmount": "flxDailyTransactioEnterAmount",
                "tbxDailyTransactionAmt": "tbxDailyTransactionAmt",
                "flxWeeklyTransactionLimit": "flxWeeklyTransactionLimit",
                "flxWeeklyTrnasactionAmount": "flxWeeklyTrnasactionAmount",
                "flxWeeklyTransactionCurrency": "flxWeeklyTransactionCurrency",
                "flxWeeklyTransactioEnterAmount": "flxWeeklyTransactioEnterAmount",
                "tbxWeeklyTransactionAmt": "tbxWeeklyTransactionAmt",
                "lblCurrency" : "lblCurrency",
                "lblCurrency2" : "lblCurrency2",
                "lblCurrency1" : "lblCurrency1",
            };
            return mapping;
        },
        /**
         * Method will invoke on form post show
         */
     
      postShow: function() {
        this.onBreakpointChange();
        var flowType = this.loadBusinessBankingModule().presentationController.getUserManagementFlow();
        var createOrEditFlow = this.loadBusinessBankingModule().presentationController.getUserNavigationType();
        if (flowType === OLBConstants.USER_MANAGEMENT_TYPE.CUSTOM_ROLE) {
          this.view.lblUserName.isVisible = false;
          this.view.lblEmail.isVisible = false;
          if (createOrEditFlow === OLBConstants.USER_MANAGEMENT_TYPE.CREATE) {
            this.view.customheadernew.activateMenu("User Management", "Create Custom Role");
            CommonUtilities.setText(this.view.lblContentHeader, kony.i18n.getLocalizedString("i18n.konybb.CreateRoleTransactionLimits"), CommonUtilities.getaccessibilityConfig());
          } else if (createOrEditFlow === OLBConstants.USER_MANAGEMENT_TYPE.VIEW_EDIT) {
            this.view.customheadernew.activateMenu("User Management", "User Roles");
            CommonUtilities.setText(this.view.lblContentHeader, kony.i18n.getLocalizedString("i18n.konybb.ViewEditRoleTransactionLimits"), CommonUtilities.getaccessibilityConfig());
          }
        } else if (flowType === OLBConstants.USER_MANAGEMENT_TYPE.USER_CREATION) {
          this.userDetails = this.loadBusinessBankingModule().presentationController.getUserDetails()
          this.view.lblUserName.isVisible = true;
          this.view.lblEmail.isVisible = true;
          CommonUtilities.setText(this.view.lblUserName, this.userDetails.firstName + " " + this.userDetails.lastName, CommonUtilities.getaccessibilityConfig());
          CommonUtilities.setText(this.view.lblEmail, this.userDetails.email, CommonUtilities.getaccessibilityConfig());
          if (createOrEditFlow === OLBConstants.USER_MANAGEMENT_TYPE.CREATE) {
            this.view.customheadernew.activateMenu("User Management", "Create UM User");
            CommonUtilities.setText(this.view.lblContentHeader, kony.i18n.getLocalizedString("i18n.konybb.CreateUserTransactionLimits"), CommonUtilities.getaccessibilityConfig());
          } else if (createOrEditFlow === OLBConstants.USER_MANAGEMENT_TYPE.VIEW_EDIT) {
            this.view.customheadernew.activateMenu("User Management", "All Users");
            CommonUtilities.setText(this.view.lblContentHeader, kony.i18n.getLocalizedString("i18n.konybb.ViewEditUserTransactionLimits"), CommonUtilities.getaccessibilityConfig());
          }
        }
      },
        /**
         * Set foucs handlers for skin of parent flex on input focus 
         */
        /* accessibilityFocusSetup: function(){
              let widgets = [
                [this.view.tbLastName, this.view.flxLastName],
                [this.view.tbxDriversLicense, this.view.flxDriversLicense],
                [this.view.tbxEmail, this.view.flxEmail],
                [this.view.tbxMiddleName, this.view.flxMiddleName],
                [this.view.tbxName, this.view.flxName],
                [this.view.tbxPhoneNum, this.view.flxPhoneNum],
                [this.view.tbxSSN, this.view.flxSSN]            
              ]
              for(let i=0; i<widgets.length; i++){
                CommonUtilities.setA11yFoucsHandlers(widgets[i][0], widgets[i][1], this)
              }
            },*/
        /**
         * Method to update form using given context
         * @param {object} context depending on the context the appropriate function is executed to update view
         */
        //     updateFormUI: function () {
        //     },
        navToNextForm: function() {
            //applicationManager.getNavigationManager().navigateTo("frmUpdatePermissionsInBulk");   
            var ntf = new kony.mvc.Navigation("frmUpdatePermissionsInBulk");
            ntf.navigate(this.selectedCompany);
        },
        /*
             navToPrevForm: function() {
            //applicationManager.getNavigationManager().navigateTo("frmUpdatePermissionsInBulk");   
            var ntf = new kony.mvc.Navigation("frmConfirmAndAck");
            ntf.navigate(this.selectedCompany);
        },
*/
        validateTransactionLimitsUI: function(id) {
            var scopeObj = this;
            //scopeObj.view.flxErrorMessageTransferPermissions.setVisibility(false);
            //this.adjustScreen();
            // this.view.segIndividualTransactionLimits
            var limitsSegData1 = scopeObj.view.segBulkTransactionLimits.data;
            var limitsSegData2 = scopeObj.view.segIndividualTransactionLimits.data;
            var limitsSegData;
            if (id === "segBulkTransactionLimits") {
                limitsSegData = limitsSegData1;
            } else limitsSegData = limitsSegData2;
            FormControllerUtility.disableButton(this.view.btnProceedRoles);
            for (var i in limitsSegData) {
				limitsSegData[i].tbxPerTransactionAmt.skin = "CopysknBB1";
				limitsSegData[i].tbxDailyTransactionAmt.skin = "CopysknBB1";
				limitsSegData[i].tbxWeeklyTransactionAmt.skin = "CopysknBB1";
                if (limitsSegData[i].tbxPerTransactionAmt.text === "" || limitsSegData[i].tbxDailyTransactionAmt.text === "" || limitsSegData[i].tbxWeeklyTransactionAmt.text === "") {
                    FormControllerUtility.disableButton(this.view.btnProceedRoles);
                    return;
                }
            }
			if (id === "segBulkTransactionLimits") {
                scopeObj.view.segBulkTransactionLimits.setData(limitsSegData);
            } else
				scopeObj.view.segIndividualTransactionLimits.setData(limitsSegData);
            FormControllerUtility.enableButton(this.view.btnProceedRoles);
        },
    };
});