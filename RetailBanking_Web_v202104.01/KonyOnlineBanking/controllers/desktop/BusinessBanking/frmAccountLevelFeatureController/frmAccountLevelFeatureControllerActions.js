define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmAccountLevelFeature **/
    AS_Form_cd5d76cd5314450b8e149656ad7a3f86: function AS_Form_cd5d76cd5314450b8e149656ad7a3f86(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmAccountLevelFeature **/
    AS_Form_d317e3aad18242ebaaf0c863189eee67: function AS_Form_d317e3aad18242ebaaf0c863189eee67(eventobject) {
        var self = this;
        this.preShow();
    },
    /** postShow defined for frmAccountLevelFeature **/
    AS_Form_ef4c531af3a940d1b4dd2c4ca5fd97df: function AS_Form_ef4c531af3a940d1b4dd2c4ca5fd97df(eventobject) {
        var self = this;
        this.postShow();
    },
    /** onBreakpointChange defined for frmAccountLevelFeature **/
    AS_Form_fd82d79546e740189dec92050d92e24c: function AS_Form_fd82d79546e740189dec92050d92e24c(eventobject, breakpoint) {
        var self = this;
        return self.onBreakpointChange.call(this, breakpoint);
    }
});