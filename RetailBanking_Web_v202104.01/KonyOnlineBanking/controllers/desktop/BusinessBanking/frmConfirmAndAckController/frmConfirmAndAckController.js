define("BusinessBanking/userfrmConfirmAndAckController", ['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function(CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {
    var orientationHandler = new OrientationHandler();
    var userManagementData = {};
    return /** @alias module:frmUserManagementController */ {
        /**
         * Method to display the footer at the end of the screen by calculating the size of screen dynamically
         * @param {integer} data value
         **/
      segData:"",
        adjustScreen: function() {
            this.view.forceLayout();
            this.view.flxFooter.isVisible = true;
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                } else {
                    this.view.flxFooter.top = mainheight + "dp";
                }
                this.view.forceLayout();
            } else {
                this.view.flxFooter.top = mainheight + "dp";
                this.view.forceLayout();
            }
        },
        /**
         * Breakpont change
         */
        onBreakpointChange: function(width) {
            this.view.customheadernew.onBreakpointChangeComponent(width);
          var scope=this;
           var data = scope.segData;
           data=(scope.segData!=="")? scope.segData:scope.view.segmentAccRole.data;
          var datamap={
            "flxUserManagement": "flxUserManagement",
            "flxUserManagementAccAccessRole": "flxUserManagementAccAccessRole",
            "flxUserManagementAccPermissions": "flxUserManagementAccPermissions",
            "lblLeftSideContent": "lblLeftSideContent",
            "lblRIghtSideContent": "lblRIghtSideContent",
            "lblRightMostContent": "lblRightMostContent",
            "lblSeparator": "lblSeparator",
            "lblRightContentAuto": "lblRightContentAuto",
          };
          scope.view.segmentAccRole.rowTemplate="flxUserManagementAccAccessRole";
          scope.view.segmentAccRole.widgetDataMap=datamap;
          scope.view.segmentAccRole.setData(data);
             if (width <= 640 || orientationHandler.isMobile) {
                scope.view.segmentAccRole.setData(data);
             } else if (width <= 1024) {
                scope.view.segmentAccRole.setData(data);
            } else if (width <= 1366) {
                scope.view.segmentAccRole.setData(data);
            } else {
                scope.view.segmentAccRole.setData(data);
            }
        },
        /**
         * hide all ui flexes in user management form
         */
        resetUI: function() {
            this.adjustScreen();
        },
        /**
         * Method will invoke on form init
         */
        initActions: function() {
            var scopeObj = this;
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
        },
        loadBusinessBankingModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
        },
        /**
         * Method will invoke on form pre show
         */
        preShow: function() {
            this.view.customheadernew.forceCloseHamburger();
            this.configManager = applicationManager.getConfigurationManager();
            this.currencyCode = this.configManager.configurations.items.CURRENCYCODE;
            this.view.customheadernew.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
            this.view.customheadernew.flxAccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
            this.view.customheadernew.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;

            //this.view.customheadernew.customhamburger.activateMenu("User Management","Create A User");          
            this.currentVisibleFlex = "flxUserDetails";
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader', 'flxMain', 'flxFooter']);
            //applicationManager.getNavigationManager().applyUpdates(this);
            let businessBankingPresentationController = this.loadBusinessBankingModule().presentationController;
            let data = businessBankingPresentationController.getUserManagementData();
            applicationManager.getNavigationManager().updateForm({
              ackDetails: data,
            }, "frmConfirmAndAck");
        },
        /**
         * Method will invoke on form post show
         */
        postShow: function() {
            this.onBreakpointChange();
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.info.frame.height - this.view.flxFooter.info.frame.height + "dp";
            //this.view.customheadernew.activateMenu("User Management", "Create A User");
            this.flowConfig = this.loadBusinessBankingModule().presentationController.getFlowConfigs();
            this.userManagementFlow = this.flowConfig.userManagementFlow || '';
            this.userCreationFlow = this.flowConfig.userCreationFlow || '';
            this.userPermissionFlow = this.flowConfig.userPermissionFlow || '';
            this.userNavigationType = this.flowConfig.userNavigationType || '';
            this.showAllUserDetails();
            applicationManager.getNavigationManager().applyUpdates(this);
            FormControllerUtility.hideProgressBar(this.view);
        },
        showAllUserDetails: function() {
            this.userManagementData = this.loadBusinessBankingModule().presentationController.getUserManagementData();
            var flowType = this.userManagementFlow;
            if (flowType === OLBConstants.USER_MANAGEMENT_TYPE.CUSTOM_ROLE) {
                this.view.lblName.isVisible = false;
                this.view.lblEmail.isVisible = false;
            } else {
                var userfullName = this.userManagementData.userDetails.firstName + " " + this.userManagementData.userDetails.lastName;
                this.view.lblName.isVisible = true;
                this.view.lblEmail.isVisible = true;
                this.view.lblName.text = userfullName;
                this.view.lblEmail.text = this.userManagementData.userDetails.email;
            }
            if (this.flowConfig.userManagementFlow === OLBConstants.USER_MANAGEMENT_TYPE.USER_CREATION) {
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                if (this.flowConfig.userNavigationType === OLBConstants.USER_MANAGEMENT_TYPE.CREATE) {
                    this.view.customheadernew.activateMenu("User Management", "Create UM User");
                    this.view.btnViewNEdit.onClick = this.navigateToAccountAccessandRole;
                    this.view.btnEdit.onClick = this.navigateToAccountLevelFeaturePermissions;
                    this.view.btnPermissionsEdit.onClick = this.navToNonAccountFeaturePermission;
                    this.view.btnTransactionEdit.onClick = this.navToTransactionLimits;
                    this.view.btnViewNEdit.isVisible = true;
                    this.view.btnEdit.isVisible = true;
                    this.view.btnPermissionsEdit.isVisible = true;
                    this.view.btnTransactionEdit.isVisible = true;
                    CommonUtilities.setText(this.view.lblContentHeader, kony.i18n.getLocalizedString("i18n.userManagement.verifyCreateUserDetails"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblUserDetailsHeading, kony.i18n.getLocalizedString("i18n.konybb.common.UserDetails"), accessibilityConfig);
                    CommonUtilities.setText(this.view.btnAckCreateUser, kony.i18n.getLocalizedString("i18n.konybb.Common.CreateUser"), accessibilityConfig);
                    this.view.btnAckCreateUser.isVisible = true;
                    this.view.btnAckCancel.isVisible = true;
                    this.view.btnAckBack.isVisible = true;
                    this.view.btnAckCreateUser.onClick = this.createInfinityUser;
                    this.view.btnAckCancel.onClick = this.onCancelClick;
                    if (this.flowConfig.userPermissionFlow === OLBConstants.USER_MANAGEMENT_TYPE.SKIP) {
                        this.view.btnAckBack.onClick = this.onBackClickUserSkip;
                    } else if (this.flowConfig.userPermissionFlow === OLBConstants.USER_MANAGEMENT_TYPE.COPY) {
                        this.view.btnAckBack.onClick = this.onBackClickUserCopy;
                    }
                } else if (this.flowConfig.userNavigationType === OLBConstants.USER_MANAGEMENT_TYPE.VIEW_EDIT) {
                    this.view.customheadernew.activateMenu("User Management", "All Users");
                    let isEditable = applicationManager.getConfigurationManager().checkUserPermission("USER_MANAGEMENT_EDIT");
                    let isNotLoggedInUserData = applicationManager.getUserPreferencesManager().getUserId() !== this.userManagementData.userDetails.id;
                    //this.view.btnAckBack.onClick = this.loadBusinessBankingModule().presentationController.navigateToUMDashboard;
                    this.view.btnAckBack.onClick = this.onBackClickUserManagement;
                    if (isEditable && isNotLoggedInUserData) {
                        this.view.btnViewNEdit.onClick = this.navigateToAccountAccessandRole;
                        this.view.btnEdit.onClick = this.navigateToAccountLevelFeaturePermissions;
                        this.view.btnPermissionsEdit.onClick = this.navToNonAccountFeaturePermission;
                        this.view.btnTransactionEdit.onClick = this.navToTransactionLimits;
                        this.view.btnViewNEdit.isVisible = true;
                        this.view.btnEdit.isVisible = true;
                        this.view.btnPermissionsEdit.isVisible = true;
                        this.view.btnTransactionEdit.isVisible = true;
                        this.view.btnAckCreateUser.isVisible = true;
                    } else {
                        this.view.btnViewNEdit.isVisible = false;
                        this.view.btnEdit.isVisible = false;
                        this.view.btnPermissionsEdit.isVisible = false;
                        this.view.btnTransactionEdit.isVisible = false;
                        this.view.btnAckCreateUser.isVisible = false;
                    }
                    CommonUtilities.setText(this.view.lblContentHeader, kony.i18n.getLocalizedString("i18n.userManagement.verifyUserDetails"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblUserDetailsHeading, kony.i18n.getLocalizedString("i18n.konybb.common.UserDetails"), accessibilityConfig);
                    CommonUtilities.setText(this.view.btnAckCreateUser, kony.i18n.getLocalizedString("i18n.PayAPerson.Update"), accessibilityConfig);
                    this.view.btnAckCreateUser.onClick = this.updateInfinityUser;
                    this.view.btnAckCancel.isVisible = false;
                    this.view.btnAckBack.isVisible = true;
                    this.view.btnAckBack.onClick = this.onCancelClick;
                }
            } else if (this.flowConfig.userManagementFlow === OLBConstants.USER_MANAGEMENT_TYPE.CUSTOM_ROLE) {
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                if (this.flowConfig.userNavigationType === OLBConstants.USER_MANAGEMENT_TYPE.CREATE) {
                    this.view.customheadernew.activateMenu("User Management", "Create Custom Role");
                    this.view.btnViewNEdit.isVisible = true;
                    this.view.btnEdit.isVisible = true;
                    this.view.btnPermissionsEdit.isVisible = true;
                    this.view.btnTransactionEdit.isVisible = true;
                    this.view.btnViewNEdit.onClick = this.navigateToAccountAccessandRole;
                    this.view.btnEdit.onClick = this.navigateToAccountLevelFeaturePermissions;
                    this.view.btnPermissionsEdit.onClick = this.navToNonAccountFeaturePermission;
                    this.view.btnTransactionEdit.onClick = this.navToTransactionLimits;
                    CommonUtilities.setText(this.view.lblContentHeader, kony.i18n.getLocalizedString("i18n.userManagement.verifyCreateRoleDetails"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblUserDetailsHeading, kony.i18n.getLocalizedString("i18n.userManagement.roleDetails"), accessibilityConfig);
                    CommonUtilities.setText(this.view.btnAckCreateUser, kony.i18n.getLocalizedString("i18n.userManagement.CreateRole"), accessibilityConfig);
                    this.view.btnAckCreateUser.isVisible = true;
                    this.view.btnAckCancel.isVisible = true;
                    this.view.btnAckBack.isVisible = true;
                    this.view.btnAckCreateUser.onClick = this.createCustomRole;
                    this.view.btnAckCancel.onClick = this.loadBusinessBankingModule().presentationController.navigateToUserRoles;
                    if (this.flowConfig.userPermissionFlow === OLBConstants.USER_MANAGEMENT_TYPE.SKIP) {
                        this.view.btnAckBack.onClick = this.onBackClickUserSkip;
                    } else if (this.flowConfig.userPermissionFlow === OLBConstants.USER_MANAGEMENT_TYPE.COPY) {
                        this.view.btnAckBack.onClick = this.onBackClickRoleCopy;
                    }
                } else if (this.flowConfig.userNavigationType === OLBConstants.USER_MANAGEMENT_TYPE.VIEW_EDIT) {
                    this.view.customheadernew.activateMenu("User Management", "User Roles");
                    let isEditable = applicationManager.getConfigurationManager().checkUserPermission("CUSTOM_ROLES_CREATE");
                    this.view.btnAckBack.onClick = this.loadBusinessBankingModule().presentationController.navigateToUserRoles;
                    if (isEditable) {
                        this.view.btnViewNEdit.onClick = this.navigateToAccountAccessandRole;
                        this.view.btnEdit.onClick = this.navigateToAccountLevelFeaturePermissions;
                        this.view.btnPermissionsEdit.onClick = this.navToNonAccountFeaturePermission;
                        this.view.btnTransactionEdit.onClick = this.navToTransactionLimits;
                        this.view.btnViewNEdit.isVisible = true;
                        this.view.btnEdit.isVisible = true;
                        this.view.btnPermissionsEdit.isVisible = true;
                        this.view.btnTransactionEdit.isVisible = true;
                        this.view.btnAckCreateUser.isVisible = true;
                    } else {
                        this.view.btnViewNEdit.isVisible = false;
                        this.view.btnEdit.isVisible = false;
                        this.view.btnPermissionsEdit.isVisible = false;
                        this.view.btnTransactionEdit.isVisible = false;
                        this.view.btnAckCreateUser.isVisible = false;
                    }
                    CommonUtilities.setText(this.view.lblContentHeader, kony.i18n.getLocalizedString("i18n.userManagement.viewEditRoleDetails"), accessibilityConfig);
                    CommonUtilities.setText(this.view.lblUserDetailsHeading, kony.i18n.getLocalizedString("i18n.userManagement.roleDetails"), accessibilityConfig);
                    CommonUtilities.setText(this.view.btnAckCreateUser, kony.i18n.getLocalizedString("i18n.PayAPerson.Update"), accessibilityConfig);
                    this.view.btnAckCancel.isVisible = false;
                    this.view.btnAckBack.isVisible = true;
                    this.view.btnAckCreateUser.onClick = this.updateCustomRole;
                    this.view.btnAckBack.onClick = this.loadBusinessBankingModule().presentationController.navigateToUserRoles;
                }
            }
            this.showPermissionDetails(this.userManagementData);
        },
        /**
         * Set foucs handlers for skin of parent flex on input focus 
         */
        accessibilityFocusSetup: function() {
            let widgets = [
                [this.view.tbLastName, this.view.flxLastName],
                [this.view.tbxDriversLicense, this.view.flxDriversLicense],
                [this.view.tbxEmail, this.view.flxEmail],
                [this.view.tbxMiddleName, this.view.flxMiddleName],
                [this.view.tbxName, this.view.flxName],
                [this.view.tbxPhoneNum, this.view.flxPhoneNum],
                [this.view.tbxSSN, this.view.flxSSN]
            ]
            for (let i = 0; i < widgets.length; i++) {
                CommonUtilities.setA11yFoucsHandlers(widgets[i][0], widgets[i][1], this)
            }
        },
        /**
         * Method to update form using given context
         * @param {object} context depending on the context the appropriate function is executed to update view
         */
        updateFormUI: function(viewModel) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            if (viewModel.serverError) {
                this.showServerError(viewModel.serverError);
            } else {
                if (viewModel.isLoading === true) {
                    FormControllerUtility.showProgressBar(this.view);
                } else if (viewModel.isLoading === false) {
                    FormControllerUtility.hideProgressBar(this.view);
                }
                if (viewModel.createInfinityUserSuccess) {
                    applicationManager.getNavigationManager().navigateTo("frmUserCreationSuccess");
                } else if (viewModel.createInfinityUserFailure) {
                    this.view.flxMainWrapper.isVisible = true;
                    this.view.flxDowntimeWarning.isVisible = true;
                    CommonUtilities.setText(this.view.lblDowntimeWarning, kony.i18n.getLocalizedString("i18n.UserManagement.userCreationFailure"), accessibilityConfig);
                }
                if (viewModel.updateInfinityUserSuccess) {
                    //this.loadBusinessBankingModule().presentationController.navigateToUMDashboard();
                    this.loadBusinessBankingModule().presentationController.fetchAssociatedContractUsers(this.loadBusinessBankingModule().presentationController.fetchAssociatedContractUsersSuccess.bind(this.loadBusinessBankingModule().presentationController));
                } else if (viewModel.updateInfinityUserFailure) {
                    this.view.flxMainWrapper.isVisible = true;
                    this.view.flxDowntimeWarning.isVisible = true;
                    this.view.lblDowntimeWarning.text = viewModel.updateInfinityUserFailure.errorMessage;
                }
                if (viewModel.createCustomRoleSuccess) {
                    applicationManager.getNavigationManager().navigateTo("frmCustomRoleAcknowledgement");
                } else if (viewModel.createCustomRoleFailure) {
                    this.view.flxMainWrapper.isVisible = true;
                    this.view.flxDowntimeWarning.isVisible = true;
                    this.view.lblDowntimeWarning.text = viewModel.createCustomRoleFailure.errorMessage;
                }
                if (viewModel.updateCustomRoleSuccess) {
                    this.loadBusinessBankingModule().presentationController.navigateToUserRoles();
                } else if (viewModel.updateCustomRoleFailure) {
                    this.view.flxMainWrapper.isVisible = true;
                    this.view.flxDowntimeWarning.isVisible = true;
                    this.view.lblDowntimeWarning.text = viewModel.updateCustomRoleFailure.errorMessage;
                }
            }
        },
        /**
         * Method to call all the methods which will populate Data in the segments
         * @param {JSON} data 
         **/
        showPermissionDetails: function(permissionDetails) {
            if (this.userManagementFlow === OLBConstants.USER_MANAGEMENT_TYPE.USER_CREATION) {
                this.showUserDetails(permissionDetails.userDetails);
            } else if (this.userManagementFlow === OLBConstants.USER_MANAGEMENT_TYPE.CUSTOM_ROLE) {
                this.showUserDetails(permissionDetails.customRoleDetails);
            }
            this.showAccountAccess(permissionDetails.companyList);
            this.showAccountLevelPermissions(permissionDetails.accountLevelPermissions);
            this.showOtherFeaturePermissions(permissionDetails.globalLevelPermissions);
            this.showTransactionLimits(permissionDetails.transactionLimits);
        },
        /**
         * Method to populate user Details
         * @param {JSON} data 
         **/
        showUserDetails: function(details) {
            var scope = this;
            this.userManagementData = this.loadBusinessBankingModule().presentationController.getUserManagementData();
            if (this.userManagementFlow === OLBConstants.USER_MANAGEMENT_TYPE.USER_CREATION) {
                if (this.userNavigationType === OLBConstants.USER_MANAGEMENT_TYPE.CREATE) {
                    if (this.userCreationFlow === OLBConstants.USER_MANAGEMENT_TYPE.MANUAL) {
                        this.view.btnViewMoreDetails.isVisible = true;
                        this.view.btnViewMoreDetails.onClick = this.navigateToCreateUser;
                        this.view.flxFullName.isVisible = true;
                        this.view.flxDOB.isVisible = true;
                        this.view.flxEmailAddress.isVisible = true;
                        this.view.flxRegisteredPhnNo.isVisible = true;
                        this.view.flxDriversLicence.isVisible = true;
                        this.view.flxAccountNickName.isVisible = true;
                        CommonUtilities.setText(this.view.lblBankName, kony.i18n.getLocalizedString("i18n.ProfileManagement.FullName"), CommonUtilities.getaccessibilityConfig());
                        let fullName = (details.firstName || '') + " " + (details.lastName || '');
                        CommonUtilities.setText(this.view.lblBillerValue, fullName, CommonUtilities.getaccessibilityConfig());
                        CommonUtilities.setText(this.view.lblAccountTypeValue, CommonUtilities.getFrontendDateString(details.dob, applicationManager.getFormatUtilManager().getDateFormat()), CommonUtilities.getaccessibilityConfig());
                        CommonUtilities.setText(this.view.lblAccountNumberValue, details.email, CommonUtilities.getaccessibilityConfig());
                        let phoneNumber = (details.phoneCountryCode || '') + (details.phoneNumber || '');
                        CommonUtilities.setText(this.view.lblBeneficiaryNameValue, phoneNumber, CommonUtilities.getaccessibilityConfig());
                        CommonUtilities.setText(this.view.lblDriverLicenceNumber, details.drivingLicenseNumber, CommonUtilities.getaccessibilityConfig());
                        CommonUtilities.setText(this.view.lblAccountNickNameValue, details.ssn, CommonUtilities.getaccessibilityConfig());
                    } else {
                        this.view.btnViewMoreDetails.isVisible = false;
                        this.view.flxFullName.isVisible = false;
                        this.view.flxDOB.isVisible = false;
                        this.view.flxEmailAddress.isVisible = false;
                        this.view.flxRegisteredPhnNo.isVisible = false;
                        this.view.flxDriversLicence.isVisible = false;
                        this.view.flxAccountNickName.isVisible = true;
                        CommonUtilities.setText(this.view.lblAccountNickNameValue, details.ssn, CommonUtilities.getaccessibilityConfig());
                    }
                } else if (this.userNavigationType === OLBConstants.USER_MANAGEMENT_TYPE.VIEW_EDIT) {
                    if (this.userManagementData.userDetails.coreCustomerId) {
                        this.view.btnViewMoreDetails.isVisible = false;
                    } else {
                        this.view.btnViewMoreDetails.isVisible = true;
                        this.view.btnViewMoreDetails.onClick = this.navigateToCreateUser;
                    }
                    this.view.flxFullName.isVisible = true;
                    this.view.flxDOB.isVisible = true;
                    this.view.flxEmailAddress.isVisible = true;
                    this.view.flxRegisteredPhnNo.isVisible = true;
                    this.view.flxDriversLicence.isVisible = true;
                    this.view.flxAccountNickName.isVisible = true;
                    CommonUtilities.setText(this.view.lblBankName, kony.i18n.getLocalizedString("i18n.ProfileManagement.FullName"), CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(this.view.lblAccountType, kony.i18n.getLocalizedString("i18n.NAO.DOB"), CommonUtilities.getaccessibilityConfig());
                    let fullName = (details.firstName || '') + " " + (details.lastName || '');
                    CommonUtilities.setText(this.view.lblBillerValue, fullName, CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(this.view.lblAccountTypeValue, CommonUtilities.getFrontendDateString(details.dob, applicationManager.getFormatUtilManager().getDateFormat()), CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(this.view.lblAccountNumberValue, details.email, CommonUtilities.getaccessibilityConfig());
                    let phoneNumber = details.phoneNumber || '';
                    CommonUtilities.setText(this.view.lblBeneficiaryNameValue, phoneNumber, CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(this.view.lblDriverLicenceNumber, details.drivingLicenseNumber, CommonUtilities.getaccessibilityConfig());
                    CommonUtilities.setText(this.view.lblAccountNickNameValue, details.ssn, CommonUtilities.getaccessibilityConfig());
                }
            } else if (this.userManagementFlow === OLBConstants.USER_MANAGEMENT_TYPE.CUSTOM_ROLE) {
                this.view.flxFullName.isVisible = true;
                this.view.flxDOB.isVisible = true;
                this.view.flxEmailAddress.isVisible = false;
                this.view.flxRegisteredPhnNo.isVisible = false;
                this.view.flxDriversLicence.isVisible = false;
                this.view.flxAccountNickName.isVisible = false;
                var customRoleName = "";
                if (!kony.sdk.isNullOrUndefined(scope.userManagementData.customRoleDetails[0])) {
                    customRoleName = scope.userManagementData.customRoleDetails[0].customRoleName;
                } else {
                    customRoleName = scope.userManagementData.customRoleDetails.customRoleName;
                }
                CommonUtilities.setText(this.view.lblBankName, kony.i18n.getLocalizedString("i18n.konybb.Common.Name"), CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(this.view.lblAccountType, kony.i18n.getLocalizedString("i18n.Enroll.ContractName"), CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(this.view.lblBillerValue, customRoleName, CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(this.view.lblAccountTypeValue, scope.userManagementData.companyList[0].contractName, CommonUtilities.getaccessibilityConfig());
                if (this.userNavigationType === OLBConstants.USER_MANAGEMENT_TYPE.CREATE) {
                    scope.view.btnViewMoreDetails.isVisible = true;
                    scope.view.btnViewMoreDetails.onClick = function() {
                        scope.loadBusinessBankingModule().presentationController.setIsEditFlow(true);
                        scope.navigateToCreateRole();
                    };
                } else if (this.userNavigationType === OLBConstants.USER_MANAGEMENT_TYPE.VIEW_EDIT) {
                    this.view.btnViewMoreDetails.isVisible = false;
                }
            }
        },
        /**
         * Method to populate account access details
         * @param {JSON} data 
         **/
        showAccountAccess: function(companyList) {
            var newCompanyList = [];
            for (let i = 0; i < companyList.length; i++) {
                if (companyList[i].userRole != "") {
                    let companyAccountAccess = companyList[i];
                    let selectedAccCount = 0;
                    for (let j = 0; j < companyAccountAccess.accounts.length; j++) {
                        if (companyAccountAccess.accounts[j].isEnabled === "true") {
                            selectedAccCount++;
                        }
                    }
                    companyList[i].selectedAccCount = selectedAccCount;
                    newCompanyList.push(companyList[i]);
                    var companyId = companyList[i].cif;
                    if (companyId.length > 4) {
                        companyId = companyId.substring(companyId.length - 4, companyId.length);
                    }
                    companyList[i].lastFourDigitOfCompanyId = companyId;
                }
            }
            let segRowData = newCompanyList.map(function(newCompanyList) {
              var compName = CommonUtilities.truncateStringWithGivenLength(newCompanyList.companyName + "...", 10);
              var userRole = CommonUtilities.truncateStringWithGivenLength(newCompanyList.userRole + "...", 10);
               var width="36%";
              if(kony.application.getCurrentBreakpoint() !== 640){
              if( kony.application.getCurrentBreakpoint() <= 1024) width="32%";
                return {
                    "lblLeftSideContent":{
                      "text": newCompanyList.companyName + " - " + newCompanyList.lastFourDigitOfCompanyId,
                       "width":width                 
                    },
                    "lblRIghtSideContent": newCompanyList.userRole,
                    "lblRightMostContent": newCompanyList.selectedAccCount + "",
                    "lblRightContentAuto":(newCompanyList.autoSyncAccounts==="true")?"On":"Off"
                };
            }
              else{
                return {
                  "lblLeftSideContent": {
                    "text" :compName + " - " + newCompanyList.lastFourDigitOfCompanyId,
                    "toolTip" : newCompanyList.companyName + " - " + newCompanyList.lastFourDigitOfCompanyId,
                  },
                  "lblRIghtSideContent": {
                    "text" : userRole,
                    "toolTip" : newCompanyList.userRole
                  },
                    "lblRightMostContent": newCompanyList.selectedAccCount + "",
                    "lblRightContentAuto":(newCompanyList.autoSyncAccounts==="true")?"On":"Off"

                };
              }
            });
           this.segData=segRowData;
           var datamap={
            "flxUserManagement": "flxUserManagement",
            "flxUserManagementAccAccessRole": "flxUserManagementAccAccessRole",
            "flxUserManagementAccPermissions": "flxUserManagementAccPermissions",
            "lblLeftSideContent": "lblLeftSideContent",
            "lblRIghtSideContent": "lblRIghtSideContent",
            "lblRightMostContent": "lblRightMostContent",
            "lblSeparator": "lblSeparator",
            "lblRightContentAuto": "lblRightContentAuto",
          };
          this.view.segmentAccRole.rowTemplate="flxUserManagementAccAccessRole";
          this.view.segmentAccRole.widgetDataMap=datamap;
            this.view.segmentAccRole.setData(segRowData);
        },
        /**
         * Method to populate Account Level Permissions
         * @param {JSON} data 
         **/
        showAccountLevelPermissions: function(accountLevelPermissions) {
            let self = this;
            let segRowData = accountLevelPermissions.map(function(accountLevelPermissionsPerCompany, index) {
                let result = self.getAccountLevelDetailsToShow(accountLevelPermissionsPerCompany);
                let lastFourDigitOfCompanyId = accountLevelPermissionsPerCompany.cif;
                if (lastFourDigitOfCompanyId.length > 4) {
                    lastFourDigitOfCompanyId = lastFourDigitOfCompanyId.substring(lastFourDigitOfCompanyId.length - 4, lastFourDigitOfCompanyId.length);
                }
                return {
                    "lblRecipient": {
                        "text": accountLevelPermissionsPerCompany.companyName + " - " + lastFourDigitOfCompanyId,
                        "accessibilityConfig": {
                            "a11yLabel": accountLevelPermissionsPerCompany.companyName + " - " + lastFourDigitOfCompanyId,
                        }
                    },
                    "lblDropdown": "O",
                    "flxDropdown": {
                        "onClick": self.showAccountLevelAccessDetails.bind(this, self.view.segAccPermissionsSeg, index)
                    },
                    "flxDetails": {
                        "isVisible": false
                    },
                    "lblDefaultPermissionsSet": {
                        "text": kony.i18n.getLocalizedString("i18n.usermanagement.DefaultPermissionSet"),
                        "accessibilityConfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.usermanagement.DefaultPermissionSet"),
                        }
                    },
                    "lblTotalFeaturesSelected": {
                        "text": kony.i18n.getLocalizedString("i18n.usermanagement.TotalFeaturesSelected"),
                        "accessibilityConfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.usermanagement.TotalFeaturesSelected"),
                        }
                    },
                    "lblTxtDefaultPermissionsSet": {
                        "text": result.DefaultPermissionSet,
                        "accessibilityConfig": {
                            "a11yLabel": result.DefaultPermissionSet
                        }
                    },
                    "lblTxtTotalFeaturesSelected": {
                        "text": result.TotalFeaturesSelected,
                        "accessibilityConfig": {
                            "a11yLabel": result.TotalFeaturesSelected
                        }
                    },
                };
            });
            this.view.segAccPermissionsSeg.setData(segRowData);
            this.view.forceLayout();
        },
        getAccountLevelDetailsToShow: function(accountLevelPermissionsPerCompany) {
            let policyMap = {};
            let enabledCount = 0,
                totalCount = 0;
            accountLevelPermissionsPerCompany.accounts.forEach(function(account) {
                account.featurePermissions.forEach(function(feature) {
                    totalCount += feature.permissions.length;
                    feature.permissions.forEach(function(action) {
                        if (action.isEnabled.toString() === "true") {
                            enabledCount++;
                            policyMap[action.accessPolicyId] = true;
                        }
                    });
                });
            });
            let policySet = Object.keys(policyMap).join(", ");
            let result = {
                "DefaultPermissionSet": Object.keys(policyMap).join(", "),
                "TotalFeaturesSelected": enabledCount + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + totalCount
            }
            return result;
        },
        /**
         * Method to populate other feature Permissions
         * @param {JSON} data 
         **/
        showOtherFeaturePermissions: function(globalLevelPermissions) {
            let self = this;
            let segRowData = globalLevelPermissions.map(function(globalLevelPermissions, index) {
                let lastFourDigitOfCompanyId = globalLevelPermissions.cif;
                if (lastFourDigitOfCompanyId.length > 4) {
                    lastFourDigitOfCompanyId = lastFourDigitOfCompanyId.substring(lastFourDigitOfCompanyId.length - 4, lastFourDigitOfCompanyId.length);
                }
                let selected = 0,
                    total = 0;
                globalLevelPermissions.features.map(feature => {
                    feature.permissions.map(permission => {
                        total++;
                        if (permission.isEnabled.toString() === 'true') selected++;
                    });
                });
                let lblTxtTotalFeaturesSelected;
                if (total == selected) {
                    lblTxtTotalFeaturesSelected = selected + " of " + total + ' (Default)';
                } else {
                    lblTxtTotalFeaturesSelected = selected + " of " + total + ' (custom)';
                }
                return {
                  "lblRecipient": {
                    "text" : globalLevelPermissions.companyName+ " - " + lastFourDigitOfCompanyId
                  },
                    "lblDropdown": "O",
                    "flxDropdown": {
                        "onClick": self.showAccountLevelAccessDetails.bind(this, self.view.segOtherFeaturePermissionsSegMain, index)
                    },
                    "flxDetails": {
                        "isVisible": false,
                        "height": '60dp'
                    },
                    "flxDetails2": {
                        "height": '25dp'
                    },
                    "flxDetails1": {
                        "isVisible": false
                    },
                    "flxDetails3": {
                        "isVisible": false
                    },
                    "lblTotalFeaturesSelected": kony.i18n.getLocalizedString("i18n.usermanagement.TotalFeaturesSelected"),
                    "lblTxtTotalFeaturesSelected": lblTxtTotalFeaturesSelected
                };
            });
            this.view.segOtherFeaturePermissionsSegMain.setData(segRowData);
            this.view.forceLayout();
        },
        /**
         * Method for dropdown functionality
         * @param segmentName 
         **/
        showAccountLevelAccessDetails: function(selectedSegment, i) {
            let segData = selectedSegment.data;
            let rowData = segData[i];
            if (rowData.lblDropdown === "O") {
                rowData.lblDropdown = "P";
                rowData.flxDetails.isVisible = true;
            } else {
                rowData.lblDropdown = "O";
                rowData.flxDetails.isVisible = false;
            }
            selectedSegment.setDataAt(rowData, i);
        },
        /**
         * Method to populate Transaction Limits
         * @param {JSON} data 
         **/
        showTransactionLimits: function(transactionLimits) {
            let self = this;
            let segRowData = transactionLimits.map(function(transactionLimits, index) {
                let limits = self.getLimits(transactionLimits);
				let limitNames = self.getLimitNames(transactionLimits);
                let lastFourDigitOfCompanyId = transactionLimits.cif;
                if (lastFourDigitOfCompanyId.length > 4) {
                    lastFourDigitOfCompanyId = lastFourDigitOfCompanyId.substring(lastFourDigitOfCompanyId.length - 4, lastFourDigitOfCompanyId.length);
                }
                return {
                    "lblRecipient": {
                        "text": transactionLimits.companyName + " - " + lastFourDigitOfCompanyId,
                        "accessibilityConfig": {
                            "a11yLabel": transactionLimits.companyName + " - " + lastFourDigitOfCompanyId,
                        }
                    },
                    "lblDropdown": "O",
                    "flxDropdown": {
                        "onClick": self.showAccountLevelAccessDetails.bind(this, self.view.segTransactionLimitsMain, index)
                    },
                    "flxDetails": {
                        "isVisible": false
                    },
                    "lblHeader": {
                        "text": kony.i18n.getLocalizedString("i18n.UserManagement.GlobalTransactionLimits"),
                        "accessibilityConfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.UserManagement.GlobalTransactionLimits")
                        }
                    },
                    "lblSubHeader": {
                        "text": limitNames[0],
                        "accessibilityConfig": {
                            "a11yLabel": limitNames[0],
                        }
                    },
                    "lblPerTransactionLimits": {
                        "text": kony.i18n.getLocalizedString("i18n.usermanagement.PerTransactionLimit"),
                        "accessibilityConfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.usermanagement.PerTransactionLimit"),
                        }
                    },
                    "lblDailyTransactionLimit": {
                        "text": kony.i18n.getLocalizedString("i18n.usermanagement.DailyTransactionLimit"),
                        "accessibilityConfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.usermanagement.DailyTransactionLimit"),
                        }
                    },
                    "lblWeeklyTransactionLimits": {
                        "text": kony.i18n.getLocalizedString("i18n.usermanagement.WeeklyTransactionLimit"),
                        "accessibilityConfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.usermanagement.WeeklyTransactionLimit"),
                        }
                    },
                    "lblTxtPerTransactionLimits": {
                        "text": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[0]),//CommonUtilities.getDisplayCurrencyFormat(limits[0]),
                        "accessibilityConfig": {
                            "a11yLabel": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[0]),
                        }
                    },
                    "lblTxtDailyTransactionLimits": {
                        "text": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[1]),//CommonUtilities.getDisplayCurrencyFormat(limits[1]),
                        "accessibilityConfig": {
                            "a11yLabel": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[1]),
                        }
                    },
                    "lblTxtWeeklyTransactionLimits": {
                        "text": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[2]),//CommonUtilities.getDisplayCurrencyFormat(limits[2]),
                        "accessibilityConfig": {
                            "a11yLabel": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[2]),
                        }
                    },
                    "lblHeader1": {
                        "text": limitNames[1],
                        "accessibilityConfig": {
                            "a11yLabel": limitNames[1],
                        }
                    },
                    "lblPerTransactionLimit": {
                        "text": kony.i18n.getLocalizedString("i18n.usermanagement.PerTransactionLimit"),
                        "accessibilityConfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.usermanagement.PerTransactionLimit"),
                        }
                    },
                    "lblDailyTransactionLimits": {
                        "text": kony.i18n.getLocalizedString("i18n.usermanagement.DailyTransactionLimit"),
                        "accessibilityConfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.usermanagement.DailyTransactionLimit"),
                        }
                    },
                    "lblWeeklyTransactionLimit1": {
                        "text": kony.i18n.getLocalizedString("i18n.usermanagement.WeeklyTransactionLimit"),
                        "accessibilityConfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.usermanagement.WeeklyTransactionLimit"),
                        }
                    },
                    "lblPerTransactionLimitValue": {
                        "text": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[3]),//CommonUtilities.getDisplayCurrencyFormat(limits[3]),
                        "accessibilityConfig": {
                            "a11yLabel": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[3]),
                        }
                    },
                    "lblDailyTransactionLimitValue": {
                        "text": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[4]),//CommonUtilities.getDisplayCurrencyFormat(limits[4]),
                        "accessibilityConfig": {
                            "a11yLabel": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[4]),
                        }
                    },
                    "lblWeeklyTransactionLimitValue1": {
                        "text": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[5]),//CommonUtilities.getDisplayCurrencyFormat(limits[5]),
                        "accessibilityConfig": {
                            "a11yLabel": self.currencyCode + applicationManager.getFormatUtilManager().formatAmount(limits[5])
                        }
                    },
                };
            });
            this.view.segTransactionLimitsMain.setData(segRowData);
            this.view.forceLayout();
        },
        getLimits: function(transactionLimits) {
            let limits = [];
            let adder = 0;
            transactionLimits.limitGroups.forEach(function(limitGroup) {
                if (limitGroup.limitGroupId === "BULK_PAYMENT") {
                    adder = 3;
                } else {
                    adder = 0;
                }
                limitGroup.limits.forEach(function(eachLimit) {
                    switch (eachLimit.id) {
                        case "DAILY_LIMIT":
                            limits[1 + adder] = eachLimit.value;
                            break;
                        case "WEEKLY_LIMIT":
                            limits[2 + adder] = eachLimit.value;
                            break;
                        case "MAX_TRANSACTION_LIMIT":
                            limits[0 + adder] = eachLimit.value;
                            break;
                    }
                });
            });
            return limits;
        },
		getLimitNames: function(transactionLimits) {
            let limitNames = [
								kony.i18n.getLocalizedString("i18n.usermanagement.IndividualTransactionLimits"),
							    kony.i18n.getLocalizedString("i18n.usermanagement.BulkTransactionLimits")
							 ];
            transactionLimits.limitGroups.forEach(function(limitGroup) {
                if (limitGroup.limitGroupId === "BULK_PAYMENT") {
                    if(limitGroup.hasOwnProperty('limitGroupName'))limitNames[1] = limitGroup.limitGroupName;
                } else if (limitGroup.limitGroupId === "SINGLE_PAYMENT") {
                    if(limitGroup.hasOwnProperty('limitGroupName'))limitNames[0] = limitGroup.limitGroupName;
                }
            });
            return limitNames;
        },
        createInfinityUser: function() {
            FormControllerUtility.showProgressBar(this.view);
            this.loadBusinessBankingModule().presentationController.createInfinityUser(this.loadBusinessBankingModule().presentationController.userManagementData);
        },
        updateInfinityUser: function() {
            FormControllerUtility.showProgressBar(this.view);
            this.loadBusinessBankingModule().presentationController.updateInfinityUser(this.loadBusinessBankingModule().presentationController.userManagementData);
        },
        createCustomRole: function() {
            FormControllerUtility.showProgressBar(this.view);
            this.loadBusinessBankingModule().presentationController.createCustomRole(this.loadBusinessBankingModule().presentationController.userManagementData);
        },
        updateCustomRole: function() {
            FormControllerUtility.showProgressBar(this.view);
            this.loadBusinessBankingModule().presentationController.updateCustomRole(this.loadBusinessBankingModule().presentationController.userManagementData);
        },
        navigateToCreateUser: function() {
            this.loadBusinessBankingModule().presentationController.setIsEditFlow(true);
            applicationManager.getNavigationManager().navigateTo("frmCreateUserManually");
        },
        navigateToCreateRole: function() {
            this.loadBusinessBankingModule().presentationController.setIsEditFlow(true);
            applicationManager.getNavigationManager().navigateTo("frmBBCreateCustomRole");
        },
        navigateToAccountAccessandRole: function() {
            this.loadBusinessBankingModule().presentationController.setIsEditFlow(true);
          if(!kony.sdk.isNullOrUndefined(this.userManagementData.userDetails)){
            var isEnroll;
            if(this.userManagementData.userDetails.isEnrolled === "true")
              isEnroll = true;
            else
              isEnroll = false;
            applicationManager.getConfigurationManager().isEnrolled = isEnroll;
          }
            applicationManager.getNavigationManager().navigateTo("frmBBAccountAccessAndRole");
        },
        navigateToAccountLevelFeaturePermissions: function() {
            let self = this;
            let param = {
                "flowType": "FROM_ACK_FORM",
                "accountLevelPermissions": self.userManagementData.accountLevelPermissions
            };
            this.loadBusinessBankingModule().presentationController.setIsEditFlow(true);
            applicationManager.getNavigationManager().navigateTo("frmFeaturePermissions", true, param);
        },
        navToNonAccountFeaturePermission: function() {
            FormControllerUtility.showProgressBar(this.view);
            this.loadBusinessBankingModule().presentationController.setIsEditFlow(true);
            applicationManager.getNavigationManager().navigateTo("frmNonAccountLevelFeature");
        },
        navToTransactionLimits: function() {
            this.loadBusinessBankingModule().presentationController.setIsEditFlow(true);
            applicationManager.getNavigationManager().navigateTo("frmTransactionLimits");
        },
        onCancelClick: function() {
            //this.loadBusinessBankingModule().presentationController.navigateToUMDashboard();
            this.loadBusinessBankingModule().presentationController.fetchAssociatedContractUsers(this.loadBusinessBankingModule().presentationController.fetchAssociatedContractUsersSuccess.bind(this.loadBusinessBankingModule().presentationController));
        },
        onBackClickUserSkip: function() {
            this.loadBusinessBankingModule().presentationController.setIsEditFlow(false);
            applicationManager.getNavigationManager().navigateTo("frmBBAccountAccessAndRole");
        },
        onBackClickRoleCopy: function() {
            this.loadBusinessBankingModule().presentationController.setIsEditFlow(false);
            applicationManager.getNavigationManager().navigateTo("frmBBCreateCustomRole");
        },
        onBackClickUserCopy: function() {
            this.loadBusinessBankingModule().presentationController.setIsEditFlow(false);
            applicationManager.getNavigationManager().navigateTo("frmBBCopyPermission");
        },
        onBackClickUserManagement: function() {
            this.loadBusinessBankingModule().presentationController.fetchAssociatedContractUsers(this.loadBusinessBankingModule().presentationController.fetchAssociatedContractUsersSuccess.bind(this.loadBusinessBankingModule().presentationController));
        }
    };
});