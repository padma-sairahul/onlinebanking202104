define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmBBUsersDashboard **/
    AS_Form_b5f1f9be03684ce2be386084532ee632: function AS_Form_b5f1f9be03684ce2be386084532ee632(eventobject) {
        var self = this;
        this.onPostShow();
    },
    /** init defined for frmBBUsersDashboard **/
    AS_Form_e378777a228d40a081864112b2c913b1: function AS_Form_e378777a228d40a081864112b2c913b1(eventobject) {
        var self = this;
        this.onInit();
    },
    /** onTouchEnd defined for frmBBUsersDashboard **/
    AS_Form_g493fe4781fd4fea85812496938fb68a: function AS_Form_g493fe4781fd4fea85812496938fb68a(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** preShow defined for frmBBUsersDashboard **/
    AS_Form_i56ac4a75ab8411f9533ace31645d4be: function AS_Form_i56ac4a75ab8411f9533ace31645d4be(eventobject) {
        var self = this;
        this.onPreShow();
    }
});