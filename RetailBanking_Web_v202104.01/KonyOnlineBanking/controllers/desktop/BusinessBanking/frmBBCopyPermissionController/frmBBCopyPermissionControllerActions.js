define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmBBCopyPermission **/
    AS_Form_a66489ba926a4cdcbde16b2034073bf7: function AS_Form_a66489ba926a4cdcbde16b2034073bf7(eventobject) {
        var self = this;
        this.postShow();
    },
    /** preShow defined for frmBBCopyPermission **/
    AS_Form_cb5e1521c2424009a6b847b4d89ef15b: function AS_Form_cb5e1521c2424009a6b847b4d89ef15b(eventobject) {
        var self = this;
        this.preShow();
    },
    /** init defined for frmBBCopyPermission **/
    AS_Form_i97d1e73e1ee4e36b32c49ea4745cb9f: function AS_Form_i97d1e73e1ee4e36b32c49ea4745cb9f(eventobject) {
        var self = this;
        this.initActions();
    }
});