define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function(CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {
    var orientationHandler = new OrientationHandler();
    return /** @alias module:frmUserManagementController */ {
        currentVisibleFlex: "flxUserDetails",
        /**
         * Method to display the footer at the end of the screen by calculating the size of screen dynamically
         * @param {integer} data value
         **/
        adjustScreen: function() {
            this.view.forceLayout();
            this.view.flxFooter.isVisible = true;
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                } else {
                    this.view.flxFooter.top = mainheight + "dp";
                }
                this.view.forceLayout();
            } else {
                this.view.flxFooter.top = mainheight + "dp";
                this.view.forceLayout();
            }

        },
        /**
         * Breakpont change
         */
        onBreakpointChange: function(width) {
            this.view.customheadernew.onBreakpointChangeComponent(width);
        },
        /**
         * hide all ui flexes in user management form
         */
        resetUI: function() {

            this.adjustScreen();
        },

        /**
         * Method will invoke on form init
         */
        initActions: function() {
            var scopeObj = this;
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.btnBackConfirmation.onClick = this.createNewUserFlow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.btnProceedAddServices.onClick = function() {
                scopeObj.navToNextForm();
            };
        },


        /**
         * Method will invoke on form pre show
         */
        preShow: function() {
            this.view.customheadernew.forceCloseHamburger();
            //this.view.customheadernew.customhamburger.activateMenu("User Management","Create A User");          
            this.currentVisibleFlex = "flxUserDetails";
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader', 'flxMain', 'flxFooter']);
        },





        /**
         * Method will invoke on form post show
         */
        postShow: function() {
            this.onBreakpointChange();
            var data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
            this.dataval = JSON.parse(JSON.stringify(data.presentationController.getUserManagementData()));
            var contractId = [];
            this.dataval.companyList.map(company => {
                if (!contractId.includes(company.contractId)) contractId.push(company.contractId);
            });
            let roleCreation = applicationManager.getConfigurationManager().checkUserPermission("CUSTOM_ROLES_CREATE");
            if (contractId.length === 1 && roleCreation) {
                this.view.flxCreateRole.isVisible = true;
                this.view.btnCreateRole.onClick = this.navigateToCreateRole;
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                CommonUtilities.setText(this.view.lblCreateRole, kony.i18n.getLocalizedString("i18n.UserManagement.createRoleFromUser"), accessibilityConfig);
                CommonUtilities.setText(this.view.btnCreateRole, kony.i18n.getLocalizedString("i18n.customRole.createCustomRole"), accessibilityConfig);
            } else {
                this.view.flxCreateRole.isVisible = false;
            }
          this.view.customheadernew.activateMenu("User Management", "Create UM User");
            this.createInfinityUserSuccess = data.presentationController.createInfinityUserSuccess;
            this.setDataUserDetails(this.dataval);
            let isEditable = applicationManager.getConfigurationManager().checkUserPermission("USER_MANAGEMENT_VIEW");
            if (isEditable) {
                this.view.btnViewMoreDetails.isVisible = true;
                this.view.btnViewMoreDetails.onClick = this.navToViewEdit;
            } else {
                this.view.btnViewMoreDetails.isVisible = false;
            }
        },

        /**
         * Set foucs handlers for skin of parent flex on input focus 
         */
        accessibilityFocusSetup: function() {
            let widgets = [
                [this.view.tbLastName, this.view.flxLastName],
                [this.view.tbxDriversLicense, this.view.flxDriversLicense],
                [this.view.tbxEmail, this.view.flxEmail],
                [this.view.tbxMiddleName, this.view.flxMiddleName],
                [this.view.tbxName, this.view.flxName],
                [this.view.tbxPhoneNum, this.view.flxPhoneNum],
                [this.view.tbxSSN, this.view.flxSSN]
            ]
            for (let i = 0; i < widgets.length; i++) {
                CommonUtilities.setA11yFoucsHandlers(widgets[i][0], widgets[i][1], this)
            }
        },

        /**
         * Method to update form using given context
         * @param {object} context depending on the context the appropriate function is executed to update view
         */
        updateFormUI: function(viewModel) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            if (viewModel.serverError) {
                this.showServerError(viewModel.serverError);
            } else {
                if (viewModel.isLoading === true) {
                    FormControllerUtility.showProgressBar(this.view);
                } else if (viewModel.isLoading === false) {
                    FormControllerUtility.hideProgressBar(this.view);
                }
                if (viewModel.getInfinityUserFailure) {
                    this.view.flxDowntimeWarning.isVisible = true;
                    CommonUtilities.setText(this.view.lblDowntimeWarning, viewModel.getInfinityUserFailure, accesibilityConfig);
                }
                if (viewModel.getInfinityUserSuccess) {
                    this.view.flxDowntimeWarning.isVisible = false;
                    applicationManager.getNavigationManager().navigateTo("frmConfirmAndAck");
                }
            }
            this.view.forceLayout();
        },
        createNewUserFlow: function() {
            var data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
            data.presentationController.initializeFlowConfigs("createUser");
            applicationManager.getNavigationManager().navigateTo("frmCreateUserManually");
        },
        navToNextForm: function() {
            let bussBanking = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking").presentationController;
			bussBanking.fetchAssociatedContractUsers(bussBanking.fetchAssociatedContractUsersSuccess.bind(bussBanking));
        },
        navToViewEdit: function() {
            var data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
            data.presentationController.viewEditUserPermissions(this.createInfinityUserSuccess.id);
        },
        navigateToCreateRole: function() {
            var data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
            data.presentationController.initializePresentationController();
            data.presentationController.initializeFlowConfigs("createRole");
            data.presentationController.userPermissionFlow = OLBConstants.USER_MANAGEMENT_TYPE.CREATE_ROLE;
            data.presentationController.userManagementData = this.dataval;
            data.presentationController.initUserManagementData = JSON.parse(JSON.stringify(this.dataval));
            applicationManager.getNavigationManager().navigateTo("frmBBCreateCustomRole");
        },
        setDataUserDetails: function(data) {
            data = data.userDetails;
            this.view.lblBillerValue.text = data.firstName + " " +data.lastName;
            this.view.lblAccountNumberValue.text = data.email;
			this.view.lblAccountTypeValue.text = data.dob;
            this.view.lblBeneficiaryNameValue.text = data.phoneNumber;
            this.view.lblAccountNickNameValue.text = data.ssn;
            this.view.CopylblAccountNickNameValue0cd9c9252d13d46.text = data.drivingLicenseNumber;
            this.view.lblReferenceNumber.text = this.createInfinityUserSuccess.id;
            this.view.lblRightName.text = this.createInfinityUserSuccess.userName;
        }

    };
});