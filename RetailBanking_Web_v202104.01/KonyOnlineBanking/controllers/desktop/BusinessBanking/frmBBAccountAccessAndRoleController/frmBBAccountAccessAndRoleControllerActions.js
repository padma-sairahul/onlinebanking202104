define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmBBAccountAccessAndRole **/
    AS_Form_ce6ff082cf1d4ccb998a5813392bf883: function AS_Form_ce6ff082cf1d4ccb998a5813392bf883(eventobject) {
        var self = this;
        this.postShow();
    },
    /** preShow defined for frmBBAccountAccessAndRole **/
    AS_Form_f9c3210f321941b7804358504a185e3e: function AS_Form_f9c3210f321941b7804358504a185e3e(eventobject) {
        var self = this;
        this.preShow();
    },
    /** init defined for frmBBAccountAccessAndRole **/
    AS_Form_iff6eba457f843a987547813d4090417: function AS_Form_iff6eba457f843a987547813d4090417(eventobject) {
        var self = this;
        this.init();
    }
});