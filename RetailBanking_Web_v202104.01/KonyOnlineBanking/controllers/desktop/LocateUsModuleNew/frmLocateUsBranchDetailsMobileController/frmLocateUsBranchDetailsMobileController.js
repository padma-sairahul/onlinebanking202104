define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, viewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(locateUsViewModel) {
            if (!locateUsViewModel) return;
            if (locateUsViewModel.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (locateUsViewModel.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (locateUsViewModel.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
            if (locateUsViewModel.preLoginView) {
                this.showPreLoginView();
            }
            if (locateUsViewModel.postLoginView) {
                this.showPostLoginView();
            }
            if (locateUsViewModel.getAtmorBranchDetailsSuccess) {
                this.getAtmOrBranchDetailsSuccessCallback(locateUsViewModel.getAtmorBranchDetailsSuccess);
            }
            if (locateUsViewModel.getAtmorBranchDetailsFailure) {
                this.getAtmOrBranchDetailsErrorCallback();
            }
            if (locateUsViewModel.geoLocationError) {
                this.noSearchResultUI();
                this.view.LocateUs.lblNoSearchResults.text = kony.i18n.getLocalizedString("i18n.LocateUs.geolocationNoSearchData");
                FormControllerUtility.hideProgressBar(this.view);
            }
        },

        /**
         * Failed to fetch branch details
         */
        getAtmOrBranchDetailsErrorCallback: function() {
            this.view.lblBranchName.text = kony.i18n.getLocalizedString("i18n.locateus.detailsNA");
            this.view.lblDistanceAndTimeFromUser.text = "";
            this.view.lblAddressLine2.text = "";
            this.view.lblAddressLine1.text = "";
            this.view.lblPhoneNumber1.text = "";
            this.view.lblPhoneNumber2.text = "";
            this.view.segDayAndTime.setData([]);
            this.view.segBranchService.setData([]);
            this.view.lblBranchName2.text = "";
        },

        /**
         * Populates branch details section
         * @param {Object} data : contains branch details of click row
         */
        getAtmOrBranchDetailsSuccessCallback: function(data) {
            this.view.lblDistanceAndTimeFromUser.text = ""
            this.view.lblPhoneNumber2.text = "";
            if (data.addressLine2) {
                this.view.lblAddressLine2.text = data.addressLine2.trim();
            }
            if (data.addressLine1) {
                this.view.lblBranchName.text = data.addressLine1.trim();
                this.view.lblBranchName2.text = data.addressLine1.trim();
                this.view.lblAddressLine1.text = data.addressLine1.trim();
            }
            if (data.phoneNumber) {
                this.view.lblPhoneNumber1.text = data.phoneNumber.trim();
            } else {
                this.view.lblPhoneNumber1.text = "N/A";
            }
            if (data.services) {
                let services = data.services.split('||');
                let serviceData = services.map(function(dataItem) {
                    return {
                        "lblService": dataItem.trim()
                    }
                });
                this.view.segBranchService.widgetDataMap = {
                    "lblService": "lblService"
                };
                this.view.segBranchService.setData(serviceData);
            }
            if (data.workingHours) {
                let workingHours = data.workingHours.split("||");
                let dateData = workingHours.map(function(dataItem) {
                    return {
                        "lblTimings": dataItem.replace(/\?/g, " to ")
                    };
                });
                this.view.segDayAndTime.widgetDataMap = {
                    "lblTimings": "lblTimings"
                };
                this.view.segDayAndTime.setData(dateData);
            } else {
                this.view.segDayAndTime.setData([]);
            }
        },

        /**
         * Shows prelogin view of the form
         */
        showPreLoginView: function() {
            this.view.customheadernew.showPreLoginView();
        },

        /**
         * Shows postLogin view of the form
         */
        showPostLoginView: function() {
            this.view.customheadernew.showPostLoginView();
        },

        /**
         * this is helper function to get the presentationcontroller of Locate Us
         */
        getLocateUsPresentationController: function() {
            var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModuleNew");
            if (locateUsModule) {
                return locateUsModule.presentationController;
            } else {
                return null;
            }
        },

        /**
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.btnBackToMap.onClick = function() {
                scopeObj.getLocateUsPresentationController().showLocateUsPage();
            };
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            this.view.customheadernew.activateMenu("About Us", "Locate Us");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.frame.height - this.view.flxFooter.frame.height + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            if (width > 640) {
                new kony.mvc.Navigation(kony.application.getPreviousForm().id).navigate();
            }
        }
    }

});