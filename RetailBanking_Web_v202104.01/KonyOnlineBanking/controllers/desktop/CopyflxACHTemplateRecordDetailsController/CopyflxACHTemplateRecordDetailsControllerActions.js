define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onKeyUp defined for tbxAmount **/
    AS_TextField_f5ba95246210403c9b801df841b6ffb1: function AS_TextField_f5ba95246210403c9b801df841b6ffb1(eventobject, context) {
        var self = this;
        this.executeOnParent("updateTotal", eventobject);
    },
    /** onKeyUp defined for tbxCrAmount **/
    AS_TextField_a40b68930e114ee4aa8fbead08766986: function AS_TextField_a40b68930e114ee4aa8fbead08766986(eventobject, context) {
        var self = this;
        this.executeOnParent("updateTotal", eventobject);
    },
    /** onKeyUp defined for tbxTaxSubAmount **/
    AS_TextField_c809361fb3e04f4cbacc55d49560189c: function AS_TextField_c809361fb3e04f4cbacc55d49560189c(eventobject, context) {
        var self = this;
        this.executeOnParent("updateTotal", eventobject);
    }
});