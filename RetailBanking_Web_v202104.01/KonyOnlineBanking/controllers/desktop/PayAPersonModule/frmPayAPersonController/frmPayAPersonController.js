define(['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var orientationHandler = new OrientationHandler();
    return {
        postShowPayaPerson: function() {
            var self = this;
            var context1 = {
                "widget": this.view.tableView.flxSendReminderCalender,
                "anchor": "bottom"
            };
            this.view.tableView.clndrRequiredDate.setContext(context1);
            context1 = {
                "widget": this.view.tableView.flxRequestMoneyCalender,
                "anchor": "bottom"
            };
            this.view.tableView.clndrRequestMoneyRequiredDate.setContext(context1);
            context1 = {
                "widget": this.view.tableView.flxCalenderdup,
                "anchor": "bottom"
            };
            this.view.tableView.clndrSendOndup.setContext(context1);
            context1 = {
                "widget": this.view.tableView.flxCalender,
                "anchor": "bottom"
            };
            this.view.tableView.clndrSendOn.setContext(context1);
            this.view.confirmation.flximgInfoIcon.onClick = function() {
                if (self.view.AllFormsConfirm.isVisible === true) {
                    self.view.AllFormsConfirm.isVisible = false;
                } else {
                    self.view.AllFormsConfirm.isVisible = true;
                    if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                        self.view.AllFormsConfirm.imgToolTip.left = self.view.confirmation.flxTotal.info.frame.x + 45 + "px";
                    } else {
                        self.view.AllFormsConfirm.imgToolTip.left = "48%";
                    }
                    self.view.AllFormsConfirm.top = self.view.confirmation.flxTotal.info.frame.y + 45 + "px";
                    self.view.AllFormsConfirm.imgCross.onTouchEnd = function() {
                        self.view.AllFormsConfirm.isVisible = false;
                    }
                }
            };
            this.view.Activatep2p.flximgInfoIconsend.onClick = function() {
                if (self.view.AllForms.isVisible === false) {
                    self.view.AllForms.isVisible = true;
                    self.view.AllForms.isVisible = true;
                    if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                        self.view.AllForms.imgToolTip.left = self.view.Activatep2p.flxAccoFrSending.info.frame.x + 45 + "px";
                    } else {
                        self.view.AllForms.imgToolTip.left = "48%";
                    }
                    self.view.AllForms.top = self.view.Activatep2p.flxAccoFrSending.info.frame.y + 45 + "px";
                    self.view.AllForms.imgCross.onTouchEnd = function() {
                        self.view.AllForms.isVisible = false;
                    }
                } else {
                    self.view.AllForms.isVisible = false;
                }
            };
            this.view.AllForms.flxCross.onClick = function() {
                self.view.AllForms.isVisible = !self.view.AllForms.isVisible;
            }
            this.view.tableView.Search.txtSearch.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.SearchMessage");
            applicationManager.getNavigationManager().applyUpdates(this);
            this.AdjustScreen();
        },
        //UI Code
        AdjustScreen: function(data) {
            if (data === undefined || data === null || data === "") {
                data = 0;
            }
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.info.frame.height + this.view.flxContainer.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + data + "dp";
                } else {
                    this.view.flxFooter.top = mainheight + data + "dp";
                }
            } else {
                this.view.flxFooter.top = mainheight + data + "dp";
            }
            this.view.forceLayout();
            this.initializeResponsiveViews();
        },
        /*################################## Refactored Code ###############################################*/
        /**
         * updateFormUi - the entry point method for the form controller.
         * @param {Object}  viewPropertiesMap - it contins the set of view properties and keys.
         */
        updateFormUI: function(viewPropertiesMap) {
            var self = this;
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.notEligible) {
                this.showNotEligibleScreen();
            }
            if (viewPropertiesMap.activation) {
                this.preActivationScreen();
            }
            if (viewPropertiesMap.TnCresponse) {
                this.showTermsAndConditionsSuccessScreen(viewPropertiesMap.TnCresponse);
            }
            if (viewPropertiesMap.addRecipient) {
                this.showAddRecipient();
            }
            if (viewPropertiesMap.showSendMoneyToNewRecipientView) {
                this.showSendMoneyToNewRecipient();
            }
            if (viewPropertiesMap.TnCcontentTransfer) {
                this.sendMoneyToPerson(viewPropertiesMap.PayeeObj, viewPropertiesMap.TnCcontentTransfer);
            }
            if (viewPropertiesMap.SendMoneyTab) {
                this.setSendRequestSegmentData(viewPropertiesMap.SendMoneyTab, false, viewPropertiesMap.pagination);
                this.sentOrRequestSortMap = [{
                    name: 'nickName',
                    imageFlx: this.view.tableView.imgSortName,
                    clickContainer: this.view.tableView.flxName
                }];
                FormControllerUtility.setSortingHandlers(this.sentOrRequestSortMap, this.onSentOrRequestSortClickHandler, this);
                FormControllerUtility.updateSortFlex(this.sentOrRequestSortMap, viewPropertiesMap.pagination);
            }
            if (viewPropertiesMap.ManageRecipientsTab) {
                this.setManageRecipientSegmentData(viewPropertiesMap.ManageRecipientsTab, viewPropertiesMap.pagination);
                this.manageRecipientSortMap = [{
                    name: 'nickName',
                    imageFlx: this.view.tableView.imgSortName,
                    clickContainer: this.view.tableView.flxName
                }];
                FormControllerUtility.setSortingHandlers(this.manageRecipientSortMap, this.onMangeRecipientSortClickHandler, this);
                FormControllerUtility.updateSortFlex(this.manageRecipientSortMap, viewPropertiesMap.pagination);
            }
            if (viewPropertiesMap.pagination) {
                //this.updatePaginationValue(viewPropertiesMap.pagination);
            }
            if (viewPropertiesMap.sentTransactions) {
                this.setSentSegmentData(viewPropertiesMap.sentTransactions, viewPropertiesMap.pagination);
                this.sentSortMap = [{
                        name: 'nickName',
                        imageFlx: this.view.tableView.imgSentSortTo,
                        clickContainer: this.view.tableView.flxSentTo
                    },
                    {
                        name: 'transactionDate',
                        imageFlx: this.view.tableView.imgSentSortDate,
                        clickContainer: this.view.tableView.flxSentDate
                    },
                    {
                        name: 'amount',
                        imageFlx: this.view.tableView.imgSentSortAmount,
                        clickContainer: this.view.tableView.flxSentAmount
                    }
                ];
                FormControllerUtility.setSortingHandlers(this.sentSortMap, this.onSentSortClickHandler, this);
                FormControllerUtility.updateSortFlex(this.sentSortMap, viewPropertiesMap.pagination);
            }
            if (viewPropertiesMap.editRecipientAcknowledgement) {
                this.showAcknowledgementEditRecipient(viewPropertiesMap.editRecipientAcknowledgement);
            }
            if (viewPropertiesMap.addRecipientAcknowledgement) {
                this.showAcknowledgementAddRecipient(viewPropertiesMap.addRecipientAcknowledgement.payPersonJSON, viewPropertiesMap.addRecipientAcknowledgement.result);
            }
            if (viewPropertiesMap.noMoreRecords) {
                this.showNoMoreRecords();
            }
            if (viewPropertiesMap.noRecipients) {
                this.showNoRecipients();
            }
            if (viewPropertiesMap.noSentTransactions) {
                this.showNoTransactions();
            }
            if (viewPropertiesMap.deactivationAcknowledgement) {
                this.showDeactivateP2PAcknowledgement(viewPropertiesMap.deactivationAcknowledgement);
            }
            if (viewPropertiesMap.showPayAPersonActivity) {
                this.showPayAPersonViewActivity(viewPropertiesMap.showPayAPersonActivity);
            }
            if (viewPropertiesMap.showActivation) {
                this.showActivateP2PScreen(viewPropertiesMap.userJSON, viewPropertiesMap.paymentAccounts);
            }
            if (viewPropertiesMap.p2pSettings) {
                this.showP2PSettingsScreen(viewPropertiesMap.userJSON, viewPropertiesMap.paymentAccounts);
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.paymentAccounts) {
                this.setPayFromData(viewPropertiesMap.paymentAccounts);
                if (viewPropertiesMap.sendPaymentAccounts) {
                    this.setSendPaymentAccounts(viewPropertiesMap.sendPaymentAccounts, viewPropertiesMap.accountCurrency);
                }
            }
            if (viewPropertiesMap.sendMoneyData) {
                this.onSendMoneyBtnClick(viewPropertiesMap.sendMoneyData);
            }
            if (viewPropertiesMap.showRequestSendMoneyAck) {
                this.showAcknowledgementSendMoney(viewPropertiesMap.requestObj, viewPropertiesMap.status, viewPropertiesMap.accountName, viewPropertiesMap.accountBalance);
            }
            if (viewPropertiesMap.inFormError) {
                this.view.flxDowntimeWarning.setVisibility(true);
                this.view.rtxDowntimeWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxDowntimeWarning.forceLayout();
                self.AdjustScreen(); //doremon
            } else if (viewPropertiesMap.inFormError === false) {
                this.view.flxDowntimeWarning.setVisibility(false);
                this.view.flxDowntimeWarning.forceLayout();
                self.AdjustScreen();
            }
            if (viewPropertiesMap.searchPayAPerson) {
                this.setManageRecipientSegmentData(viewPropertiesMap.searchPayAPerson.payAPersonData, {}, true); //editone
            }
            this.AdjustScreen();
        },
        /**
         * this method is used to show the not eligible screen.
         */
        showNotEligibleScreen: function() {
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
            }, {
                text: kony.i18n.getLocalizedString("i18n.Common.NotEligible"),
            }]);
            this.view.OptionsAndProceed.flxDetails.top = "220dp";
            this.view.OptionsAndProceed.flxDetails.isVisible = false;
            this.view.OptionsAndProceed.flxWarning.setVisibility(true);
            this.view.OptionsAndProceed.flxSeparator2.isVisible = false;
            this.view.OptionsAndProceed.flxActions.isVisible = true;
            this.view.OptionsAndProceed.lblWarning.text = kony.i18n.getLocalizedString("i18n.p2p.notEligiblemsg");
            this.view.OptionsAndProceed.btnProceed.text = kony.i18n.getLocalizedString("i18n.WireTransfer.CreateNewAccount");
            this.view.OptionsAndProceed.btnCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.OptionsAndProceed.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.WireTransfer.CreateNewAccount");
            this.view.OptionsAndProceed.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.OptionsAndProceed.btnProceed.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule").presentationController.showNewAccountOpening();
            };
            this.view.OptionsAndProceed.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule').presentationController.showAccountsDashboard();
            };
            this.showView(["flxOptionsAndProceed"]);
            this.AdjustScreen();
            this.view.forceLayout();
        },
        /**
         * preActivationScreen - Method that sets the UI for preActivation screen.
         */
        preActivationScreen: function() {
            var self = this;
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson")
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.payAPersonActivation")
            }]);
            this.view.OptionsAndProceed.setVisibility(true);
            this.view.flxNoRecipient.setVisibility(false);
            this.view.OptionsAndProceed.btnCancel.setVisibility(false);
            this.view.OptionsAndProceed.flxWarning.setVisibility(false);
            this.view.OptionsAndProceed.flxIAgree.setVisibility(true);
            this.view.OptionsAndProceed.flxDetails.setVisibility(true);
            this.view.OptionsAndProceed.btnProceed.text = kony.i18n.getLocalizedString("i18n.common.proceed");
            this.view.OptionsAndProceed.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
            this.view.OptionsAndProceed.lblIns1.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelineOne");
            this.view.OptionsAndProceed.lblIns2.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelineTwo");
            this.view.OptionsAndProceed.lblIns3.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelineThree");
            this.view.OptionsAndProceed.lblIns4.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelineFour");
            this.view.OptionsAndProceed.lblHeader.text = kony.i18n.getLocalizedString("i18n.PayAPerson.activatePayAPerson");
            this.view.OptionsAndProceed.lblHeading2.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelinesTitle");
            this.view.confirmDialog.lblKeyAmount.text = kony.i18n.getLocalizedString("i18n.transfers.lblAmount") + "(" + applicationManager.getConfigurationManager().getCurrencyCode() + ")";
            this.view.OptionsAndProceed.lblCheckbox.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            if (CommonUtilities.isCSRMode()) {
                this.view.OptionsAndProceed.btnProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.OptionsAndProceed.btnProceed.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.OptionsAndProceed.btnProceed.focusSkin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.OptionsAndProceed.btnProceed.hoverSkin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.OptionsAndProceed.btnProceed.onClick = function() {
                    if (self.view.OptionsAndProceed.lblCheckbox.text === OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
                        self.view.OptionsAndProceed.flxWarning.setVisibility(true);
                        self.view.OptionsAndProceed.lblWarning.text = kony.i18n.getLocalizedString("i18n.billPay.activateBillPayProceedError");
                    } else {
                        var preferences = {};
                        preferences.showActivation = true;
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.loadP2PSettingsScreen(preferences);
                    }
                };
            }
            this.showView(["flxOptionsAndProceed"]);
            this.AdjustScreen();
        },
        /**
         * updatePaginationValue - This method is used to update the pagination widget values.
         * @param{Object} values - it contains the set of values like offset, limit.
         */
        updatePaginationValue: function(values, paginationText) {
            this.view.tableView.lblPagination.text = (values.offset + 1) + " - " + (values.offset + values.limit) + " " + paginationText;
            if (values.offset >= values.paginationRowLimit) {
                this.view.tableView.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
                this.view.tableView.flxPaginationPrevious.setEnabled(true);
            } else {
                this.view.tableView.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
                this.view.tableView.flxPaginationPrevious.setEnabled(false);
            }
            if (values.limit < values.paginationRowLimit) {
                this.view.tableView.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
                this.view.tableView.flxPaginationNext.setEnabled(false);
            } else {
                this.view.tableView.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
                this.view.tableView.flxPaginationNext.setEnabled(true);
            }
        },
        /**
         * setSendRequestSegmentData - Method that binds the data to sent segment.
         * @param {array}  data - list of recipients.
         * @param {Boolean} paginationStatus - flag to determine if we need to show the pagination or not.
         */
        setSendRequestSegmentData: function(data, isSearch, paginationValues) {
            var scopeObj = this;
            this.view.customheader.customhamburger.activateMenu("Pay A Person", "Send Money");
            this.showTableView();
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                toolTip: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                toolTip: kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
            }]);
            if (kony.application.getCurrentBreakpoint() <= 1024 || orientationHandler.isTablet) {
                this.view.flxRightWrapper.setVisibility(false);
            } else {
                this.view.flxRightWrapper.setVisibility(true);
            }
            this.view.p2pActivity.setVisibility(false);
            this.view.tableView.setVisibility(true);
            this.view.tableView.flxTableHeaders.setVisibility(true);
            this.view.tableView.flxSearch1.setVisibility(false);
            this.view.tableView.flxTableHeaders.flxSendRequestWrapper.setVisibility(true);
            this.view.tableView.flxTableHeaders.flxSentWrapper.setVisibility(false);
            this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(false);
            this.setSkinActive(this.view.tableView.flxTabs.btnSendRequest);
            this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
            this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
            this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
            this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
            this.view.tableView.flxPaginationNext.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.fetchNextRecipientsList.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController, "SendMoneyTab");
            this.view.tableView.flxPaginationPrevious.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.fetchPreviousRecipientsList.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController, "SendMoneyTab");
            this.view.tableView.flxPagination.setVisibility(true);
            if (!isSearch) {
                this.view.tableView.flxPagination.setVisibility(true);
            } else {
                this.view.tableView.flxPagination.setVisibility(false);
            }
            var dataMap = {
                "btnRequest": "btnRequest",
                "btnSend": "btnSend",
                "flxColumn1": "flxColumn1",
                "flxColumn2": "flxColumn2",
                "flxColumn3": "flxColumn3",
                "flxDetails": "flxDetails",
                "flxDropdown": "flxDropdown",
                "flxIdentifier": "flxIdentifier",
                "btnIdentifier": "btnIdentifier",
                "lblIdentifier": "lblIdentifier",
                "flxName": "flxName",
                "flxPrimaryContact": "flxPrimaryContact",
                "flxRequestAction": "flxRequestAction",
                "flxRow": "flxRow",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxSendAction": "flxSendAction",
                "flxSendRequest": "flxSendRequest",
                "flxSendRequestSelected": "flxSendRequestSelected",
                "flxSeperator": "flxSeperator",
                "imgDropdown": "imgDropdown",
                "lblFullName": "lblFullName",
                "lblFullName2": "lblFullName2",
                "lblName": "lblName",
                "lblPrimaryContact": "lblPrimaryContact",
                "lblRegisteredEmail1": "lblRegisteredEmail1",
                "lblRegisteredEmail2": "lblRegisteredEmail2",
                "lblRegisteredEmails": "lblRegisteredEmails",
                "lblRegisteredPhone": "lblRegisteredPhone",
                "lblRegisteredPhone1": "lblRegisteredPhone1",
                "lblRegisteredPhone2": "lblRegisteredPhone2",
                "lblSeparator": "lblSeparator",
                "lblSeparatorSelected": "lblSeparatorSelected",
                "imgRowSelected": "imgRowSelected",
                "lblEmpty": "lblEmpty"
            };
            data = data.map(function(dataItem) {
                var height = 205;
                if (dataItem.secondaryEmail) {
                    height = height + 23;
                }
                if (dataItem.secondaryPhoneNumber) {
                    height = height + 23;
                }
                var identifierHeight = height + 50;
                var flxDetails = {
                    "height": height + "dp"
                };
                return {
                    "btnRequest": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Request"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.PayAPerson.Request"),
                    },
                    "btnSend": {
                        "text": kony.i18n.getLocalizedString("i18n.common.send"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.common.send"),
                        "onClick": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.onSendMoney.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController, dataItem)
                    },
                    "imgDropdown": {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Transaction details"
                        }
                    },
                    "lblFullName": kony.i18n.getLocalizedString("i18n.ProfileManagement.FullName"),
                    "lblFullName2": dataItem.name,
                    "lblName": dataItem.nickName ? dataItem.nickName : dataItem.name,
                    "lblPrimaryContact": dataItem.primaryContactForSending,
                    "lblRegisteredEmail1": dataItem.email ? dataItem.email : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblRegisteredEmail2": dataItem.secondaryEmail ? dataItem.secondaryEmail : " ",
                    "lblRegisteredEmails": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredEmail"),
                    "lblRegisteredPhone": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredPhone"),
                    "lblRegisteredPhone1": dataItem.phone ? dataItem.phone : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblRegisteredPhone2": dataItem.secondaryPhoneNumber2 ? dataItem.secondaryPhoneNumber2 : " ",
                    "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxSendRequestMobile" : "flxSendRequest",
                    "lblSeparator": ".",
                    "lblSeparatorSelected": "lblSeparatorSelected",
                    "imgRowSelected": ViewConstants.IMAGES.ARRAOW_UP,
                    "btnIdentifier": {
                        text: "u"
                    },
                    "flxDetails":flxDetails,
                    "flxIdentifier": {
                        "width": "0.63%",
                        "height": identifierHeight + "dp"
                    },
                    "lblEmpty": " "
                };
            });
            if (data.length !== 0) {
                this.view.tableView.segP2P.widgetDataMap = dataMap;
                this.view.tableView.segP2P.setData(data);
                if (!isSearch)
                    this.updatePaginationValue(paginationValues, kony.i18n.getLocalizedString("i18n.transfers.external_accounts"));
                this.view.tableView.flxNoTransactions.setVisibility(false);
                this.showView(["flxTableView", "flxWhatElse", "flxAddRecipientButton"]);
            } else {
                if (!isSearch) {
                    this.showNoRecipients();
                } else {
                    this.showNoSearchRecipients();
                }
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * setManageRecipientSegmentData - Sets the data for Manage Recipient segment.
         * @param {array} managepayeesData- A list of recipients.
         */
        setManageRecipientSegmentData: function(managePayeesData, paginationValues, isSearch) {
            var self = this;
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                toolTip: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient"),
                toolTip: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient"),
            }]);
            this.view.customheader.customhamburger.activateMenu("Pay A Person", "My Recipients");
            this.showTableView();
            this.view.tableView.flxTableHeaders.setVisibility(true);
            this.view.tableView.flxSearch1.setVisibility(true);
            this.view.tableView.flxTableHeaders.flxSendRequestWrapper.setVisibility(true);
            this.view.tableView.flxTableHeaders.flxSentWrapper.setVisibility(false);
            this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(false);
            this.view.tableView.flxNoTransactions.setVisibility(false);
            // this.view.tableView.Search.setVisibility(false);
            this.view.tableView.flxPagination.setVisibility(true);
            this.setSkinInActive(this.view.tableView.flxTabs.btnSendRequest);
            this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
            this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
            this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
            this.setSkinActive(this.view.tableView.flxTabs.btnManageRecepient);
            //this.view.tableView.Search.txtSearch.text = "";
            this.view.tableView.flxPaginationNext.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.fetchNextRecipientsList.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController, "ManageRecipientsTab");
            this.view.tableView.flxPaginationPrevious.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.fetchPreviousRecipientsList.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController, "ManageRecipientsTab");
            if (!isSearch) {
                this.view.tableView.flxPagination.setVisibility(true);
            } else {
                this.view.tableView.flxPagination.setVisibility(false);
            }
            managePayeesData = managePayeesData.map(function(payeeRecord) {
                var btnDelete = {
                    "onClick": CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : function() {
                        self.deleteP2PRecipient();
                    },
                    "text": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                };
                var btnEdit = {
                    "onClick": function() {
                        self.editP2PRecipient();
                    },
                    "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                };
                var btnRequestMoney = {
                    "onClick": function() {
                        var data = self.view.tableView.segP2P.data;
                        var index = self.view.tableView.segP2P.selectedRowIndex[1];
                        var selectedData = data[index];
                        //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.onRequestMoney(selectedData);
                    },
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
                };
                var btnSendMoney = {
                    "onClick": function() {
                        var data = self.view.tableView.segP2P.data;
                        var index = self.view.tableView.segP2P.selectedRowIndex[1];
                        var selectedData = data[index];
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.onSendMoney(selectedData);
                    },
                    "text": kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                };
                var btnViewActivity = {
                    "onClick": function() {
                        var data = self.view.tableView.segP2P.data;
                        var index = self.view.tableView.segP2P.selectedRowIndex[1];
                        var selectedData = data[index];
                        self.setP2PActivityDetails(selectedData);
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getRecipientActivity(selectedData["PayPersonId"]);
                    },
                    "text": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                };
                var height = 258;
                if(payeeRecord.secondaryEmail){height = height+18;}
                if(payeeRecord.secondaryPhoneNumber){height = height+18;}
                var dataObject = {
                    "btnDelete": btnDelete,
                    "btnEdit": btnEdit,
                    "btnRequestMoney": btnRequestMoney,
                    "btnSendMoney": btnSendMoney,
                    "btnViewActivity": btnViewActivity,
                    "flxBottomSeperator": "flxBottomSeperator",
                    "flxColumn1": "flxColumn1",
                    "flxColumn2": "flxColumn2",
                    "flxColumn3": "flxColumn3",
                    "flxDeleteAction": "flxDeleteAction",
                    "flxDetails": "flxDetails",
                    "flxDropdown": "flxDropdown",
                    "flxEditAction": "flxEditAction",
                    "btnIdentifier": "btnIdentifier",
                    "lblIdentifier": "lblIdentifier",
                    "ManageRecipient": "ManageRecipient",
                    "ManageRecipientSelected": "ManageRecipientSelected",
                    "flxName": "flxName",
                    "flxPrimaryContact": "flxPrimaryContacy",
                    "flxRow": "flxRow",
                    "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                    "flxSeperator": "flxSeperator",
                    "imgDropdown": {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Transaction details"
                        }
                    },
                    "lblName": payeeRecord.nickName ? payeeRecord.nickName : payeeRecord.name,
                    "lblPrimaryContact": payeeRecord.primaryContactForSending,
                    "lblRegisteredEmail1": payeeRecord.email ? payeeRecord.email : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblRegisteredEmail2": payeeRecord.secondaryEmail ? payeeRecord.secondaryEmail : "",
                    "lblRegisteredEmails": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredEmail"),
                    "lblRegisteredPhone": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredPhone"),
                    "lblRegisteredPhone1": payeeRecord.phone ? payeeRecord.phone : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblRegisteredPhone2": payeeRecord.secondaryPhoneNumber ? payeeRecord.secondaryPhoneNumber : "",
                    "recipientID": payeeRecord.PayPersonId,
                    "recipientNickName": payeeRecord.nickName,
                    "lblSeparator": "lblSeparator",
                    "lblSeparatorSelected": "lblSeparatorSelected",
                    "imgRowSelected": ViewConstants.IMAGES.ARRAOW_UP,
                    "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxManageRecipientMobile" : "flxManageRecipient",
                    "email": payeeRecord.email,
                    "secondaryEmail": payeeRecord.secondaryEmail,
                    "secondaryPhone": payeeRecord.secondaryPhoneNumber,
                    "name": payeeRecord.name,
                    "nickName": payeeRecord.nickName,
                    "phone": payeeRecord.phone,
                    //                     "btnIdentifier":{text:"s"},
                    "PayPersonId": payeeRecord.PayPersonId,
                    "flxIdentifier": {
                        "width": "0.63%",
                        "height":height+"dp"
                    },
                    "lblEmpty": " "
                };
                if (CommonUtilities.isCSRMode()) {
                    dataObject.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                }
                return dataObject;
            });
            var dataMap = {
                "btnDelete": "btnDelete",
                "btnEdit": "btnEdit",
                "btnRequestMoney": "btnRequestMoney",
                "btnSendMoney": "btnSendMoney",
                "btnViewActivity": "btnViewActivity",
                "flxBottomSeperator": "flxBottomSeperator",
                "flxColumn1": "flxColumn1",
                "flxColumn2": "flxColumn2",
                "flxColumn3": "flxColumn3",
                "flxDeleteAction": "flxDeleteAction",
                "flxDetails": "flxDetails",
                "flxDropdown": "flxDropdown",
                "flxEditAction": "flxEditAction",
                "flxIdentifier": "flxIdentifier",
                "lblIdentifier": "lblIdentifier",
                "ManageRecipient": "ManageRecipient",
                "ManageRecipientSelected": "ManageRecipientSelected",
                "flxName": "flxName",
                "flxPrimaryContact": "flxPrimaryContact",
                "flxRow": "flxRow",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxSeperator": "flxSeperator",
                "imgDropdown": "imgDropdown",
                "lblName": "lblName",
                "lblPrimaryContact": "lblPrimaryContact",
                "lblRegisteredEmail1": "lblRegisteredEmail1",
                "lblRegisteredEmail2": "lblRegisteredEmail2",
                "lblRegisteredEmails": "lblRegisteredEmails",
                "lblRegisteredPhone": "lblRegisteredPhone",
                "lblRegisteredPhone1": "lblRegisteredPhone1",
                "lblRegisteredPhone2": "lblRegisteredPhone2",
                "lblSeparator": "lblSeparator",
                "lblSeparatorSelected": "lblSeparatorSelected",
                "imgRowSelected": "imgRowSelected",
                "lblEmpty": "lblEmpty"
            };
            this.view.tableView.segP2P.widgetDataMap = dataMap;
            if (managePayeesData.length > 0) {
                this.view.tableView.segP2P.setData(managePayeesData);
                if (!isSearch)
                    this.updatePaginationValue(paginationValues, kony.i18n.getLocalizedString("i18n.transfers.external_accounts"));
                this.showView(["flxTableView", "flxWhatElse", "flxAddRecipientButton"]);
            } else {
                if (!isSearch)
                    this.showNoRecipients();
                else {
                    this.showNoSearchRecipients();
                }
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * showNoMoreRecords - Handles zero records scenario in navigation.
         */
        showNoMoreRecords: function() {
            this.view.tableView.imgPaginationNext.src = "pagination_next_inactive.png";
            this.view.tableView.flxPaginationNext.setEnabled(false);
            kony.ui.Alert(kony.i18n.getLocalizedString("i18n.navigation.norecordsfound"));
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * showNoRecipient - Handles zero records scenario in send/request segment.
         */
        showNoRecipients: function() {
            this.showView(["flxNoRecipient", "flxWhatElse", "flxAddRecipientButton"]);
            this.view.noRecipients.lblNoRecipientsWarning.text = kony.i18n.getLocalizedString("i18n.PayAPerson.noRecipientsFound");
            this.view.noRecipients.btnSendMoney.onClick = this.showSendMoneyToNewRecipient.bind(this);
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         *  Method used to show no transactions view.
         */
        showNoTransactions: function() {
            this.setSkinInActive(this.view.tableView.flxTabs.btnSendRequest);
            this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
            this.setSkinActive(this.view.tableView.flxTabs.btnSent);
            this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
            this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
            this.view.tableView.flxPagination.setVisibility(false);
            this.view.tableView.flxNoTransactions.setVisibility(true);
            this.view.tableView.flxNoTransactions.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString("i18n.PayAPerson.noRecipientsFound");
            this.showView(["flxTableView", "flxWhatElse", "flxAddRecipientButton"]);
            this.view.tableView.segP2P.setVisibility(false);
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * showNoSearchRecipients - Handles zero records scenario in send/request segment.
         */
        showNoSearchRecipients: function() {
            this.view.tableView.flxPagination.setVisibility(false);
            this.view.tableView.flxNoTransactions.setVisibility(true);
            this.view.tableView.flxNoTransactions.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString("i18n.PayAPerson.NoRecords");
            this.showView(["flxTableView", "flxWhatElse", "flxAddRecipientButton"]);
            this.view.tableView.segP2P.setVisibility(false);
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * setSentSegmentData -  Method used to set the Data for the sent tab.
         * @param {Array} - Array of transactions.
         * @returns {VOID}
         * @throws {}
         */
        setSentSegmentData: function(transactions, paginationValues) {
            var self = this;
            this.view.customheader.customhamburger.activateMenu("Pay A Person", "History");
            var configurationManager = applicationManager.getConfigurationManager();
            var scopeObj = this;
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                toolTip: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.Sent"),
                toolTip: kony.i18n.getLocalizedString("i18n.PayAPerson.Sent"),
            }]);
            this.view.tableView.flxTableHeaders.setVisibility(true);
            this.view.tableView.flxSearch1.setVisibility(false);
            this.view.tableView.flxTableHeaders.flxSendRequestWrapper.setVisibility(false);
            this.view.tableView.flxTableHeaders.flxSentWrapper.flxSentRow.flxSentTo.lblSentTo.text = kony.i18n.getLocalizedString("i18n.transfers.lblTo");
            this.view.tableView.flxTableHeaders.flxSentWrapper.flxSentRow.flxSentUsing.lblSentUsing.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Using");
            this.view.tableView.flxTableHeaders.flxSentWrapper.setVisibility(true);
            this.view.tableView.flxTableHeaders.flxSentWrapper.flxSentRow.setVisibility(true);
            this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(false);
            this.view.tableView.segP2P.setVisibility(true);
            this.view.tableView.flxPagination.setVisibility(true);
            this.setSkinInActive(this.view.tableView.flxTabs.btnSendRequest);
            this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
            this.setSkinActive(this.view.tableView.flxTabs.btnSent);
            this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
            this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
            this.view.tableView.flxPaginationNext.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.fetchNextSentTransactions.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController);
            this.view.tableView.flxPaginationPrevious.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.fetchPreviousSentTransactions.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController);
            var dataMap = {
                "btnModify": "btnModify",
                "btnViewActivity": "btnViewActivity",
                "btnDownloadReport": "btnDownloadReport",
                "flxAmount": "flxAmount",
                "flxBottomSeperator": "flxBottomSeperator",
                "flxColumn1": "flxColumn1",
                "flxColumn2": "flxColumn2",
                "flxColumn3": "flxColumn3",
                "flxColumn4": "flxColumn4",
                "flxDate": "flxDate",
                "flxDetails": "flxDetails",
                "flxDropdown": "flxDropdown",
                "flxIdentifier": "flxIdentifier",
                "flxModifyAction": "flxModifyAction",
                "flxRow": "flxRow",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxSentSelected": "flxSentSelected",
                "flxSeperator": "flxSeperator",
                "flxTo": "flxTo",
                "flxUsing": "flxUsing",
                "flxFrequency": "flxFrequency",
                "flxRecurrence": "flxRecurrence",
                "lblFrequencyTitle": "lblFrequencyTitle",
                "lblFrequencyValue": "lblFrequencyValue",
                "lblReccurrenceTitle": "lblReccurrenceTitle",
                "lblReccurrenceValue": "lblReccurrenceValue",
                "lblIdentifier": "lblIdentifier",
                "imgDropdown": "imgDropdown",
                "lblAmount": "lblAmount",
                "lblDate": "lblDate",
                "lblDeliveredOn": "lblDeliveredOn",
                "lblDeliveredOn1": "lblDeliveredOn1",
                "lblNote": "lblNote",
                "lblNote1": "lblNote1",
                "lblReferenceNo": "lblReferenceNo",
                "lblReferenceNumber1": "lblReferenceNumber1",
                "lblTo": "lblTo",
                "lblUsing": "lblUsing",
                "lblTransactionHeader": "lblTransactionHeader",
                "flxSegTransactionHeader": "flxSegTransactionHeader",
                // "lblSeparator": "lblSeparator",
                "lblSeparator1": "lblSeparator1",
                "lblSeparatorSelected": "lblSeparatorSelected",
                "btnCancelThisOccurrence": "btnCancelThisOccurrence",
                "btnCancelSeries": "btnCancelSeries",
                "imgRowSelected": "imgRowSelected",
                "flxDetailsRow2": "flxDetailsRow2",
                "flxSentSelectedMobile": "flxSentSelectedMobile",
            };
            var pending = [];
            var posted = [];
            for (var index in transactions) {
                transactions[index].onCancel = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSentTransactionsView();
                };
                if (transactions[index].statusDescription === "Successful") {
                    posted.push(transactions[index]);
                } else {
                    transactions[index].isEditFlow = true;
                    pending.push(transactions[index]);
                }
            }
            var postedHeader = {
                "flxSegTransactionHeader": {
                    "skin": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "sknFlxf8f7f8" : "sknFlxffffff",
                },
                "lblTransactionHeader": kony.i18n.getLocalizedString("i18n.accounts.posted"),
                "lblSeparator": {
                    "text": ".",
                    "isVisible": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "true" : "false",
                }
            };
            var postedTransactions = posted.map(function(dataItem) {
                var repeatButtonVisible = true;
                var downloadButtonVisible = true;
                var repeatButtonOnclick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.onSendMoney.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController, dataItem);
                var downloadButtonOnclick = self.getReportForDownload.bind(self, dataItem);
                if (dataItem.isPaypersonDeleted === "true" || !dataItem.personId) {
                    repeatButtonVisible = false;
                    downloadButtonVisible = false;
                    downloadButtonOnclick = null;
                    repeatButtonOnclick = null;
                }
                return {
                    "btnModify": {
                        "text": kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                        onClick: repeatButtonOnclick,
                        "isVisible": repeatButtonVisible,
                    },
                    "btnViewActivity": {
                        "text": kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
                        onClick: self.showTransactionReport.bind(self, dataItem),
                        "isVisible": false,
                    },
                    "btnDownloadReport": {
                        "text": kony.i18n.getLocalizedString("i18n.transfers.downloadReport"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.transfers.downloadReport"),
                        onClick: downloadButtonOnclick,
                        "isVisible": downloadButtonVisible,
                    },
                    "btnCancelThisOccurrence": {
                        "isVisible": false,
                    },
                    "btnCancelSeries": {
                        "isVisible": false,
                    },
                    "flxDetailsRow2": {
                        "isVisible": true,
                    },
                    "lblSeparator1": {
                        "isVisible": true,
                    },
                    "imgDropdown": {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Transaction Details"
                        }
                    },
                    "lblAmount": dataItem.amount ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(dataItem.amount, false, applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblDate": dataItem.transactionDate ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getFormattedDateString(dataItem.transactionDate) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblDeliveredOn": kony.i18n.getLocalizedString("i18n.PayAPerson.DeliveredOn"),
                    "lblDeliveredOn1": dataItem.transactionDate ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getFormattedDateString(dataItem.transactionDate) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblNote": kony.i18n.getLocalizedString("i18n.PayAPerson.Note"),
                    "lblNote1": dataItem.transactionsNotes ? dataItem.transactionsNotes : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblReferenceNo": kony.i18n.getLocalizedString("i18n.PayAPerson.ReferenceNumber"),
                    "lblReferenceNumber1": dataItem.transactionId,
                    "lblTo": dataItem.payPersonName ? dataItem.payPersonName : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblUsing": dataItem.p2pContact ? dataItem.p2pContact : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblFrequencyTitle": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                    "lblFrequencyValue": dataItem.frequencyType,
                    "lblIdentifier": ".",
                    "flxRecurrence": {
                        "isVisible": dataItem.frequencyType === configurationManager.OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false : true
                    },
                    "lblReccurrenceTitle": kony.i18n.getLocalizedString("i18n.accounts.recurrence"),
                    "lblReccurrenceValue": dataItem.recurrenceDesc ? dataItem.recurrenceDesc : "-",
                    "lblSeparator": ".",
                    "lblSeparatorSelected": "lblSeparatorSelected",
                    "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxSentMobile" : "flxSent",
                    "imgRowSelected": ViewConstants.IMAGES.ARRAOW_UP,
                    "flxIdentifier": {
                        "skin": "sknflx4a902",
                        "width": "5dp"
                    },
                    "flxSentSelectedMobile": {
                        "height": "400px"
                    }
                };
            });
            var pendingHeader = {
                "flxSegTransactionHeader": {
                    "skin": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "sknFlxf8f7f8" : "sknFlxffffff",
                },
                "lblTransactionHeader": kony.i18n.getLocalizedString("i18n.billPay.scheduled"),
                "lblSeparator": {
                    "text": ".",
                    "isVisible": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "true" : "false",
                }
            };
            var pendingTransactions = pending.map(function(dataItem) {
                var btnCancelSeriesVisible = dataItem.frequencyType === configurationManager.OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false : true;
                var isMobileAndVisible = btnCancelSeriesVisible && (kony.application.getCurrentBreakpoint() === 640 && orientationHandler.isMobile);
                var data = {
                    "btnModify": {
                        "text": kony.i18n.getLocalizedString("i18n.transfers.Modify"),
                        onClick: kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.onSendMoney.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController, dataItem),
                        "toolTip": kony.i18n.getLocalizedString("i18n.transfers.Modify"),
                    },
                    "btnViewActivity": {
                        "isVisible": false
                    },
                    "btnCancelThisOccurrence": {
                        "isVisible": true,
                        "text": dataItem.frequencyType === configurationManager.OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.Cancel") : kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
                        "toolTip": dataItem.frequencyType === configurationManager.OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.Cancel") : kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
                        "onClick": dataItem.frequencyType === configurationManager.OLBConstants.TRANSACTION_RECURRENCE.ONCE ? scopeObj.onCancelClick.bind(scopeObj, dataItem) : scopeObj.onCancelOccurrenceClick.bind(scopeObj, dataItem),
                    },
                    "btnCancelSeries": {
                        "isVisible": btnCancelSeriesVisible,
                        "text": kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
                        "onClick": scopeObj.onCancelSeriesClick.bind(scopeObj, dataItem),
                    },
                    "flxDetailsRow2": {
                        "isVisible": true
                    },
                    "imgDropdown": {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Transaction Details"
                        }
                    },
                    "lblAmount": dataItem.amount ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(dataItem.amount, false, applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblDate": dataItem.transactionDate ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getFormattedDateString(dataItem.transactionDate) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblDeliveredOn": kony.i18n.getLocalizedString("i18n.PayAPerson.DeliveredOn"),
                    "lblDeliveredOn1": dataItem.scheduledDate ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getFormattedDateString(dataItem.scheduledDate) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblNote": kony.i18n.getLocalizedString("i18n.PayAPerson.Note"),
                    "lblNote1": dataItem.transactionsNotes ? dataItem.transactionsNotes : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblReferenceNo": kony.i18n.getLocalizedString("i18n.PayAPerson.ReferenceNumber"),
                    "lblReferenceNumber1": dataItem.transactionId,
                    "lblTo": dataItem.payPersonName ? dataItem.payPersonName : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblUsing": dataItem.p2pContact ? dataItem.p2pContact : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblFrequencyTitle": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                    "lblFrequencyValue": dataItem.frequencyType,
                    "flxRecurrence": {
                        "isVisible": dataItem.frequencyType === configurationManager.OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false : true,
                    },
                    "lblReccurrenceTitle": kony.i18n.getLocalizedString("i18n.accounts.recurrence"),
                    "lblReccurrenceValue": dataItem.recurrenceDesc ? dataItem.recurrenceDesc : "-",
                    "lblSeparator": ".",
                    "lblSeparatorSelected": ".",
                    "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxSentMobile" : "flxSent",
                    "flxSentSelectedMobile": {
                        "height": "440px"
                    },
                    "imgRowSelected": ViewConstants.IMAGES.ARRAOW_UP,
                    "lblEmpty": " "
                };
                if (isMobileAndVisible) {
                    data.btnCancelThisOccurrence.left = "0";
                    data.btnCancelThisOccurrence.width = "50%";
                    data.btnCancelSeries.left = "50%";
                    data.btnCancelSeries.width = "50%";
                }
                return data;
            });
            var viewModel = [];
            var pendingViewModel = [];
            if (pendingTransactions.length > 0) {
                pendingViewModel.push(pendingHeader, pendingTransactions);
                viewModel.push(pendingViewModel);
            };
            var postedViewModel = [];
            if (postedTransactions.length > 0) {
                postedViewModel.push(postedHeader, postedTransactions);
                viewModel.push(postedViewModel);
            };
            this.view.tableView.segP2P.widgetDataMap = dataMap;
            this.view.tableView.segP2P.setData(viewModel);
            this.view.tableView.flxNoTransactions.setVisibility(false);
            // this.view.tableView.Search.setVisibility(false);
            this.view.tableView.flxSendMoney.setVisibility(false);
            this.showView(["flxTableView", "flxWhatElse", "flxAddRecipientButton"]);
            paginationValues.limit = transactions.length;
            this.updatePaginationValue(paginationValues, "Transactions");
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * This method is used to delete a recipient from manage recipients tab in pay a person.
         */
        deleteP2PRecipient: function() {
            var self = this;
            var data = this.view.tableView.segP2P.data;
            var index = this.view.tableView.segP2P.selectedRowIndex[1];
            var payeeID = data[index].recipientID;
            self.showQuitScreen({
                "negative": function() {
                    self.closeQuitScreen();
                },
                "positive": function() {
                    self.closeQuitScreen();
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.deleteRecipient(payeeID);
                }
            }, kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"), kony.i18n.getLocalizedString("i18n.PayAPerson.DeleteRecipientMessage"));
        },
        /**
         * This method is used to show the popup.
         * @param {object} actions - this takes an object with an action for "yes" and "No".
         * @param {String} header - The header message for that popup.
         * @param {String} message - The message for the popup.
         */
        showQuitScreen: function(actions, header, message) {
            if (header) {
                this.view.CustomPopup1.lblHeading.text = header;
            }
            if (message) {
                this.view.CustomPopup1.lblPopupMessage.text = message;
            }
            this.view.CustomPopup1.btnNo.onClick = actions.negative;
            this.view.CustomPopup1.btnYes.onClick = actions.positive;
            var height = this.view.flxHeader.info.frame.height + this.view.flxContainer.info.frame.height + this.view.flxFooter.info.frame.height;
            this.view.flxQuit.height = height + "dp";
            this.view.flxQuit.left = "0%";
            this.view.flxQuit.setVisibility(true);
            this.view.AllFormsConfirm.isVisible = false;
            this.view.customheader.topmenu.setFocus(true);
        },
        /**
         * This method is used to dismiss the popup.
         */
        closeQuitScreen: function() {
            this.view.flxQuit.setVisibility(false);
        },
        /**
         * This method shows the confirmation screen for add recipient.
         */
        showConfirmationAddRecipient: function() {
            var self = this;
            var payPersonJSON = {};
            var primaryContactForSending = [];
            var phoneText = "";
            var emailText = "";
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson()
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient"),
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipientConfirmation")
            }]);
            this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ConfirmmRecipientDetails");
            this.view.lblHeadingAddRecipient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient");
            this.view.CustomPopup1.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Quit");
            this.view.CustomPopup1.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.PayAPerson.cancelAddRecipientAlert");
            if (this.view.AddRecipient.flxThree.isVisible) {
                if (this.view.AddRecipient.tbxPhoneValue1.text) {
                    phoneText = this.view.AddRecipient.tbxPhoneValue1.text;
                    payPersonJSON["phone"] = this.view.AddRecipient.tbxPhoneValue1.text;
                    primaryContactForSending = primaryContactForSending.concat([
                        [this.view.AddRecipient.tbxPhoneValue1.text, this.view.AddRecipient.tbxPhoneValue1.text]
                    ]);
                }
                if (this.view.AddRecipient.tbxPhoneValue2.text) {
                    phoneText = phoneText + "<br>" + this.view.AddRecipient.tbxPhoneValue2.text;
                    payPersonJSON["secondaryPhoneNumber"] = this.view.AddRecipient.tbxPhoneValue2.text;
                    primaryContactForSending = primaryContactForSending.concat([
                        [this.view.AddRecipient.tbxPhoneValue2.text, this.view.AddRecipient.tbxPhoneValue2.text]
                    ]);
                }
            } else {
                if (this.view.AddRecipient.tbxEmailValue1.text) {
                    emailText = this.view.AddRecipient.tbxEmailValue1.text;
                    payPersonJSON["email"] = this.view.AddRecipient.tbxEmailValue1.text;
                    primaryContactForSending = primaryContactForSending.concat([
                        [this.view.AddRecipient.tbxEmailValue1.text, this.view.AddRecipient.tbxEmailValue1.text]
                    ]);
                }
                if (this.view.AddRecipient.tbxEmailValue2.text) {
                    emailText = emailText + "<br>" + this.view.AddRecipient.tbxEmailValue2.text;
                    payPersonJSON["secondaryEmail"] = this.view.AddRecipient.tbxEmailValue2.text;
                    primaryContactForSending = primaryContactForSending.concat([
                        [this.view.AddRecipient.tbxEmailValue2.text, this.view.AddRecipient.tbxEmailValue2.text]
                    ]);
                }
            }
            this.view.flxBottom.setVisibility(false);
            payPersonJSON["name"] = CommonUtilities.changedataCase(this.view.AddRecipient.tbxRecipientValue.text.trim());
            payPersonJSON["nickName"] = CommonUtilities.changedataCase(this.view.AddRecipient.tbxNickNameValue.text.trim());
            var data = {};
            data[kony.i18n.getLocalizedString("i18n.StopCheckPayments.PayeeName")] = CommonUtilities.changedataCase(this.view.AddRecipient.tbxRecipientValue.text.trim());
            data[kony.i18n.getLocalizedString("i18n.TransferEur.nickName")] = this.view.AddRecipient.tbxNickNameValue.text.trim() ? CommonUtilities.changedataCase(this.view.AddRecipient.tbxNickNameValue.text.trim()) : "";
            data[kony.i18n.getLocalizedString("i18n.payAperson.phoneNumbers")] = phoneText ? phoneText : kony.i18n.getLocalizedString("i18n.common.none");
            data[kony.i18n.getLocalizedString("i18n.payaAPerson.emailAddress")] = emailText ? emailText : kony.i18n.getLocalizedString("i18n.common.none");
            this.view.confirmation.flxPrimaryContact.setVisibility(true);
            this.view.confirmation.lbxPrimaryContact.setVisibility(false);
            this.view.confirmation.flxCheckBox.setVisibility(false);
            this.view.confirmation.lblValuePrimaryContact.setVisibility(true);
            if (this.view.AddRecipient.tbxPhoneValue1.text) {
                this.view.confirmation.lblValuePrimaryContact.text = this.view.AddRecipient.tbxPhoneValue1.text;
            } else {
                this.view.confirmation.lblValuePrimaryContact.text = this.view.AddRecipient.tbxEmailValue1.text;
            }
            var actions = {
                "cancel": function() {
                    self.showQuitScreen({
                        "negative": function() {
                            self.closeQuitScreen();
                        },
                        "positive": function() {
                            self.closeQuitScreen();
                            self.showAddRecipient();
                        }
                    });
                },
                "modify": function() {
                    self.modifyRecipient("addRecipient");
                },
                "confirm": function() {
                    payPersonJSON["primaryContactForSending"] = self.view.confirmation.lblValuePrimaryContact.text;
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.createP2PPayee(payPersonJSON);
                },
            };
            FormControllerUtility.enableButton(self.view.confirmation.confirmButtons.btnConfirm);
            this.setConfirmationValues(data, actions, {});
            this.AdjustScreen();
            this.showView(["flxConfirmation"]);
            this.view.flxHeader.setFocus(true);
        },
        /**
         * This method shows the add recipient in pay a person.
         */
        showAddRecipient: function() {
            var self = this;
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson()
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient"),
            }]);
            this.view.AddRecipient.flxContactDetailRadio1.onClick = function() {
                if (!self.view.AddRecipient.flxThree.isVisible) {
                    self.RadioBtnAction(self.view.AddRecipient.imgRadioBtnAccountType2, self.view.AddRecipient.imgRadioBtnAccountType1);
                    self.view.AddRecipient.flxThree.isVisible = true;
                    self.view.AddRecipient.flxFour.isVisible = false;
                    self.view.AddRecipient.tbxEmailValue1.text = "";
                    self.view.AddRecipient.tbxEmailValue2.text = "";
                    FormControllerUtility.disableButton(self.view.AddRecipient.btnAdd);
                }
            };
            this.view.AddRecipient.flxAccountTypeRadio2.onClick = function() {
                if (!self.view.AddRecipient.flxFour.isVisible) {
                    self.RadioBtnAction(self.view.AddRecipient.imgRadioBtnAccountType1, self.view.AddRecipient.imgRadioBtnAccountType2);
                    self.view.AddRecipient.flxThree.isVisible = false;
                    self.view.AddRecipient.flxFour.isVisible = true;
                    self.view.AddRecipient.tbxPhoneValue1.text = "";
                    self.view.AddRecipient.tbxPhoneValue2.text = "";
                    FormControllerUtility.disableButton(self.view.AddRecipient.btnAdd);
                }
            };
            this.resetAddRecipientUI();
            this.enableRadioButton(self.view.AddRecipient.imgRadioBtnAccountType1, self.view.AddRecipient.imgRadioBtnAccountType2);
            this.view.AddRecipient.flxThree.isVisible = true;
            this.view.AddRecipient.flxFour.isVisible = false;
            this.view.customheader.customhamburger.activateMenu("Pay A Person", "Add Recipient");
            this.view.AddRecipient.tbxRecipientValue.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterRecipientName");
            this.view.AddRecipient.tbxNickNameValue.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNickName");
            this.view.AddRecipient.btnAdd.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ADD");
            this.view.AddRecipient.btnAdd.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.ADD");
            this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterRecipientDetails")
            this.view.lblHeadingAddRecipient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient");
            this.view.AddRecipient.tbxRecipientValue.onKeyUp = function() {
                self.checkIsValidInput();
            }
            this.view.AddRecipient.tbxPhoneValue1.onKeyUp = function() {
                self.checkIsValidInput();
            }
            this.view.AddRecipient.tbxEmailValue1.onKeyUp = function() {
                self.checkIsValidInput();
            }
            this.view.AddRecipient.tbxPhoneValue2.onKeyUp = function() {
                self.checkIsValidInput();
            }
            this.view.AddRecipient.tbxEmailValue2.onKeyUp = function() {
                self.checkIsValidInput();
            }
            if (CommonUtilities.isCSRMode()) {
                CommonUtilities.disableButtonActionForCSRMode();
                this.view.AddRecipient.btnAdd.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.AddRecipient.btnAdd.hoverSkin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.AddRecipient.btnAdd.focusSkin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.AddRecipient.btnAdd.onClick = this.showConfirmationAddRecipient.bind(this);
            }
            this.view.AddRecipient.btnCancel.onClick = this.hideAddRecipient.bind(this);
            FormControllerUtility.disableButton(this.view.AddRecipient.btnAdd);
            this.showView(["flxAddRecipient", "flxWhatElse"]);
            this.view.lblHeader.text = "Add Recipient";
            this.AdjustScreen();
        },
        /**
         * This method resets the UI for the add recipient screen in pay a person.
         */
        resetAddRecipientUI: function() {
            this.view.AddRecipient.tbxRecipientValue.text = "";
            this.view.AddRecipient.tbxNickNameValue.text = "";
            this.view.AddRecipient.tbxEmailValue1.text = "";
            this.view.AddRecipient.tbxEmailValue2.text = "";
            this.view.AddRecipient.tbxPhoneValue1.text = "";
            this.view.AddRecipient.tbxPhoneValue2.text = "";
            this.view.AddRecipient.CopytbxNickNameValue0d3080bbd2c5640.text = "";
            this.view.forceLayout();
        },
        /**
         * This method shows the acknowledgement page for add recipient in pay a person.
         */
        showAcknowledgementAddRecipient: function(payPersonJSON, result) {
            var self = this;
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient"),
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipientAcknowledgement")
            }]);
            var message = {
                "message": kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipientSuccessMessage"),
                "image": ViewConstants.IMAGES.SUCCESS_GREEN,
            };
            this.view.btnAcknowledgementTwo.setVisibility(true);
            this.view.btnAcknowledgementOne.setVisibility(true);
            this.view.btnAcknowledgementThree.setVisibility(true);
            this.view.confirmation.flxPrimaryContact.setVisibility(false);
            var phoneText = "";
            if (payPersonJSON["phone"]) {
                phoneText = phoneText + payPersonJSON["phone"];
            }
            if (payPersonJSON["secondaryPhoneNumber"]) {
                phoneText = phoneText + "<br>" + payPersonJSON["secondaryPhoneNumber"];
            }
            var emailText = "";
            if (payPersonJSON["email"]) {
                emailText = emailText + payPersonJSON["email"];
            }
            if (payPersonJSON["secondaryEmail"]) {
                emailText = emailText + "<br>" + payPersonJSON["secondaryEmail"];
            }
            var details = {};
            details[kony.i18n.getLocalizedString("i18n.BillPay.BillerName")] = payPersonJSON["name"];
            details[kony.i18n.getLocalizedString("i18n.PayPerson.nickName")] = payPersonJSON["nickName"] ? payPersonJSON["nickName"] : "";
            details[kony.i18n.getLocalizedString("i18n.payAperson.phoneNumbers")] = phoneText ? phoneText : kony.i18n.getLocalizedString("i18n.common.none");
            details[kony.i18n.getLocalizedString("i18n.PayPerson.emailAddress")] = emailText ? emailText : kony.i18n.getLocalizedString("i18n.common.none");
            details[kony.i18n.getLocalizedString("i18n.PayPerson.primaryContactForSending")] = payPersonJSON["primaryContactForSending"];
            this.setAcknowledgementValues(message, details, {}, {
                "btnTwo": {
                    "text": kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                    "action": function() {
                        payPersonJSON["lblPrimaryContact"] = payPersonJSON["primaryContactForSending"];
                        payPersonJSON["PayPersonId"] = result.PayPersonId;
                        payPersonJSON["context"] = "";
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.onSendMoney(payPersonJSON);
                    }
                },
                "btnOne": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
                    "action": function() {
                        payPersonJSON["PayPersonId"] = result.PayPersonId;
                        //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.onRequestMoney(payPersonJSON);
                    }
                },
                "btnThree": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.AddAnotherRecipient"),
                    "action": function() {
                        self.showAddRecipient();
                    }
                }
            });
            this.view.btnAcknowledgementTwo.toolTip = this.view.btnAcknowledgementTwo.text;
            this.view.btnAcknowledgementOne.toolTip = this.view.btnAcknowledgementOne.text;
            this.view.btnAcknowledgementThree.toolTip = this.view.btnAcknowledgementThree.text;
            this.view.btnAcknowledgementOne.setVisibility(false);
            this.showView(["flxAcknowledgement"]);
            FormControllerUtility.hideProgressBar(this.view);
            this.view.flxHeader.setFocus(true);
            this.AdjustScreen();
        },
        /**
         * this method is used to show the modify screen for the add recipient and edit recipient for pay a person.
         * @param {String} view - This represents the view to be shown add recipient or edit recipient.
         */
        modifyRecipient: function(view) {
            if (view === "addRecipient") {
                this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterRecipientDetails");
            } else {
                this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientDetails")
            }
            this.view.breadcrumb.lblBreadcrumb3.text = "";
            this.showView(["flxAddRecipient", "flxWhatElse"]);
            this.AdjustScreen();
        },
        /**
         * @param {object} data - this contains the details like recipient name, nickname, phone, email and primary contact for sending.
         * @param {object} actions - this contains the set of actions.
         * @param {object} extraData - this contains the set of details like primary contact for sending.
         */
        setConfirmationValues: function(data, actions, extraData, TnCcontentTransfer) {
            var self = this;
            var target, prop, key, value;
            this.view.confirmation.setVisibility(true);
            this.view.flxDowntimeWarning.setVisibility(false);
            target = this.view.confirmation.flxKeyValues.widgets();
            var i = 0;
            for (prop in data) {
                key = target[i].widgets()[0].widgets()[0].widgets()[0];
                value = target[i].widgets()[0].widgets()[2].widgets()[0];
                key.text = prop;
                value.text = data[prop];
                i++;
            }
            if (Object.keys(data).length > 5) {
                if (Object.keys(data).length > 6) {
                    this.view.confirmation.flxKeyValues.keyValueSix.setVisibility(true);
                    this.view.confirmation.flxKeyValues.keyValueFive.setVisibility(true);
                    this.view.confirmation.flxPrimaryContact.setVisibility(true);
                } else {
                    this.view.confirmation.flxKeyValues.keyValueSix.setVisibility(true);
                    this.view.confirmation.flxKeyValues.keyValueFive.setVisibility(true);
                    this.view.confirmation.flxPrimaryContact.setVisibility(false);
                }
            } else {
                this.view.confirmation.flxKeyValues.keyValueFive.setVisibility(false);
                this.view.confirmation.flxKeyValues.keyValueSix.setVisibility(false);
            }
            if (Object.keys(extraData).length !== 0) {
                this.view.confirmation.flxTotal.isVisible = true;
                this.view.confirmation.flxCheckBox.isVisible = true;
                target = this.view.confirmation.flxTotal.widgets();
                i = 0;
                for (prop in extraData) {
                    key = target[i].widgets()[0].widgets()[0].widgets()[0].widgets()[0];
                    value = target[i].widgets()[0].widgets()[0].widgets()[2].widgets()[0];
                    key.text = prop;
                    value.text = extraData[prop];
                    i++;
                }
            } else {
                this.view.confirmation.flxTotal.isVisible = false;
                // this.view.confirmation.flxCheckBox.isVisible = false;
            }
            if (TnCcontentTransfer) {
                if (TnCcontentTransfer.alreadySigned) {
                    this.view.confirmation.flxCheckBox.setVisibility(false);
                } else {
                    CommonUtilities.disableButton(this.view.confirmation.confirmButtons.btnConfirm);
                    this.view.confirmation.lblCheckBoxIcon.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                    this.view.confirmation.flxCheckBox.setVisibility(true);
                    if (TnCcontentTransfer.contentTypeId === OLBConstants.TERMS_AND_CONDITIONS_URL) {
                        this.view.confirmation.btnTermsAndConditions.onClick = function() {
                            window.open(TnCcontentTransfer.termsAndConditionsContent);
                        }
                    } else {
                        this.view.confirmation.btnTermsAndConditions.onClick = this.showTermsAndConditionPopUp;
                    }
                    this.view.flxClose.onClick = this.hideTermsAndConditionPopUp;
                    this.view.confirmation.lblCheckBoxIcon.onTouchEnd = this.toggleTnC.bind(this, this.view.confirmation.lblCheckBoxIcon, this.view.confirmation.confirmButtons.btnConfirm);
                    this.setTnCDATASection(TnCcontentTransfer.termsAndConditionsContent);
                }
            } else {
                this.view.confirmation.flxCheckBox.isVisible = false;
            }
            this.view.confirmation.confirmButtons.btnCancel.onClick = actions.cancel;
            this.view.confirmation.confirmButtons.btnModify.onClick = actions.modify;
            this.view.confirmation.confirmButtons.btnConfirm.onClick = CommonUtilities.debounce(actions.confirm.bind(self), OLBConstants.FUNCTION_WAIT, false);
            this.showView(["flxConfirmation"]);
            this.view.flxHeader.setFocus(true);
        },
        /**
         * This method is used to set the acknowledgement values.
         * @param {object} message - This contains a success message and an image.
         * @param {object} details - This contains the details specific to the service like recipient name, nickname,phone , email.
         * @param {object} extraMessage - This contains additional details for the service.
         * @param {object} actions - this contains the set of actions to be shown in the acknowledgement page.
         */
        setAcknowledgementValues: function(message, details, extraMessage, actions) {
            this.view.acknowledgment.lblTransactionMessage.text = message.message;
            this.view.acknowledgment.ImgAcknowledged.src = message.image;
            if (Object.keys(extraMessage).length !== 0) {
                this.view.acknowledgment.flxBalance.setVisibility(true);
                this.view.acknowledgment.lblRefrenceNumber.setVisibility(true);
                this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(true);
                this.view.acknowledgment.lblAccType.setVisibility(true);
                this.view.acknowledgment.lblBalance.setVisibility(true);
                this.view.acknowledgment.lblRefrenceNumberValue.text = extraMessage.referenceNumber;
                this.view.acknowledgment.lblAccType.text = extraMessage.accountType;
                this.view.acknowledgment.lblBalance.text = extraMessage.amount;
            } else {
                this.view.acknowledgment.flxBalance.setVisibility(false);
                this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(false);
                this.view.acknowledgment.lblRefrenceNumber.setVisibility(false);
                this.view.acknowledgment.lblAccType.setVisibility(false);
                this.view.acknowledgment.lblBalance.setVisibility(false);
            }
            var target = this.view.confirmDialog.flxMain.widgets();
            var i = 2;
            for (var prop in details) {
                var key = target[i].widgets()[0].widgets()[0];
                var value = target[i].widgets()[1].widgets()[0];
                key.text = prop;
                value.text = details[prop];
                i++;
            }
            if (Object.keys(details).length === 6) {
                this.view.confirmDialog.flxContainerSix.isVisible = true;
                this.view.confirmDialog.flxContainerSeven.isVisible = false;
                this.view.confirmDialog.flxContainerEight.isVisible = false;
            } else if (Object.keys(details).length === 7) {
                this.view.confirmDialog.flxContainerSix.isVisible = true;
                this.view.confirmDialog.flxContainerSeven.isVisible = true;
                this.view.confirmDialog.flxContainerEight.isVisible = false;
            } else if (Object.keys(details).length === 8) {
                this.view.confirmDialog.flxContainerSix.isVisible = true;
                this.view.confirmDialog.flxContainerSeven.isVisible = true;
                this.view.confirmDialog.flxContainerEight.isVisible = true;
            } else {
                this.view.confirmDialog.flxContainerSix.isVisible = false;
                this.view.confirmDialog.flxContainerSeven.isVisible = false;
                this.view.confirmDialog.flxContainerEight.isVisible = false;
            }
            if (Object.keys(actions).length !== 0) {
                if (actions.btnOne.text === "null") {
                    this.view.btnAcknowledgementOne.isVisible = false;
                    this.view.btnAcknowledgementTwo.text = actions.btnTwo.text;
                    this.view.btnAcknowledgementTwo.onClick = actions.btnTwo.action;
                    this.view.btnAcknowledgementThree.text = actions.btnThree.text;
                    this.view.btnAcknowledgementThree.onClick = actions.btnThree.action
                } else {
                    this.view.btnAcknowledgementOne.text = actions.btnOne.text;
                    this.view.btnAcknowledgementOne.onClick = actions.btnOne.action;
                    this.view.btnAcknowledgementTwo.text = actions.btnTwo.text;
                    this.view.btnAcknowledgementTwo.onClick = actions.btnTwo.action;
                    this.view.btnAcknowledgementThree.text = actions.btnThree.text;
                    this.view.btnAcknowledgementThree.onClick = actions.btnThree.action;
                }
            }
        },
        /**
         * This method is used to edit the recipient in pay a person.
         */
        editP2PRecipient: function() {
            var self = this;
            var data = this.view.tableView.segP2P.data;
            var index = this.view.tableView.segP2P.selectedRowIndex[1];
            var selectedData = data[index];
            selectedData["lblName"] = selectedData["name"];
            this.resetAddRecipientUI();
            this.view.lblHiddenValue.text = selectedData["recipientID"];
            this.view.AddRecipient.tbxRecipientValue.onKeyUp = function() {
                self.checkIsValidInput();
            };
            this.view.AddRecipient.tbxPhoneValue1.onKeyUp = function() {
                self.checkIsValidInput();
            };
            this.view.AddRecipient.tbxEmailValue1.onKeyUp = function() {
                self.checkIsValidInput();
            };
            this.view.AddRecipient.tbxPhoneValue2.onKeyUp = function() {
                self.checkIsValidInput();
            };
            this.view.AddRecipient.tbxEmailValue2.onKeyUp = function() {
                self.checkIsValidInput();
            };
            if (selectedData["lblRegisteredPhone1"] === kony.i18n.getLocalizedString("i18n.common.none")) {
                self.enableRadioButton(self.view.AddRecipient.imgRadioBtnAccountType2, self.view.AddRecipient.imgRadioBtnAccountType1);
                self.view.AddRecipient.flxThree.isVisible = false;
                self.view.AddRecipient.flxFour.isVisible = true;
                self.view.AddRecipient.tbxPhoneValue1.text = "";
                self.view.AddRecipient.tbxPhoneValue2.text = "";
                self.view.AddRecipient.tbxEmailValue1.text = selectedData["lblRegisteredEmail1"];
                self.view.AddRecipient.tbxEmailValue2.text = selectedData["lblRegisteredEmail2"];
            } else {
                self.enableRadioButton(self.view.AddRecipient.imgRadioBtnAccountType1, self.view.AddRecipient.imgRadioBtnAccountType2);
                self.view.AddRecipient.flxThree.isVisible = true;
                self.view.AddRecipient.flxFour.isVisible = false;
                self.view.AddRecipient.tbxEmailValue1.text = "";
                self.view.AddRecipient.tbxEmailValue2.text = "";
                self.view.AddRecipient.tbxPhoneValue1.text = selectedData["lblRegisteredPhone1"];
                self.view.AddRecipient.tbxPhoneValue2.text = selectedData["lblRegisteredPhone2"];
            }
            this.view.AddRecipient.flxContactDetailRadio1.onClick = function() {
                if (!self.view.AddRecipient.flxThree.isVisible) {
                    self.RadioBtnAction(self.view.AddRecipient.imgRadioBtnAccountType2, self.view.AddRecipient.imgRadioBtnAccountType1);
                    self.view.AddRecipient.flxThree.isVisible = true;
                    self.view.AddRecipient.flxFour.isVisible = false;
                    self.view.AddRecipient.tbxEmailValue1.text = "";
                    self.view.AddRecipient.tbxEmailValue2.text = "";
                    FormControllerUtility.disableButton(self.view.AddRecipient.btnAdd);
                }
            };
            this.view.AddRecipient.flxAccountTypeRadio2.onClick = function() {
                if (!self.view.AddRecipient.flxFour.isVisible) {
                    self.RadioBtnAction(self.view.AddRecipient.imgRadioBtnAccountType1, self.view.AddRecipient.imgRadioBtnAccountType2);
                    self.view.AddRecipient.flxThree.isVisible = false;
                    self.view.AddRecipient.flxFour.isVisible = true;
                    self.view.AddRecipient.tbxPhoneValue1.text = "";
                    self.view.AddRecipient.tbxPhoneValue2.text = "";
                    FormControllerUtility.disableButton(self.view.AddRecipient.btnAdd);
                }
            };
            this.view.AddRecipient.tbxRecipientValue.text = selectedData["lblName"];
            this.view.AddRecipient.tbxNickNameValue.text = selectedData["recipientNickName"];
            FormControllerUtility.enableButton(this.view.AddRecipient.btnAdd);
            this.view.AddRecipient.btnAdd.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Update");
            this.view.AddRecipient.btnAdd.toolTip = kony.i18n.getLocalizedString("i18n.PayAPerson.Update");
            this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientDetails");
            this.view.lblHeadingAddRecipient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient");
            this.view.AddRecipient.btnAdd.onClick = this.showConfirmationEditRecipient.bind(this);
            this.view.AddRecipient.btnCancel.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showManageRecipientsView.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController);
            this.showView(["flxAddRecipient", "flxWhatElse"]);
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSendMoneyTabView();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient"),
            }]);
            this.AdjustScreen();
        },
        /**
         * This method is used to show the confirmation page to edit recipient.
         */
        showConfirmationEditRecipient: function() {
            var self = this;
            var payeeName = this.view.AddRecipient.tbxRecipientValue.text.trim();
            var nickName = this.view.AddRecipient.tbxNickNameValue.text.trim();
            var validationUtilityManager = applicationManager.getValidationUtilManager();
            var payPersonJSON = {};
            payPersonJSON["id"] = this.view.lblHiddenValue.text;
            payPersonJSON["name"] = payeeName;
            payPersonJSON["nickName"] = nickName;
            if (validationUtilityManager.isValidEmail(this.view.AddRecipient.tbxEmailValue1.text.trim()) && this.view.AddRecipient.tbxEmailValue1.text.trim() !== "") {
                payPersonJSON["email"] = this.view.AddRecipient.tbxEmailValue1.text.trim();
            }
            if (validationUtilityManager.isValidEmail(this.view.AddRecipient.tbxEmailValue2.text.trim()) && this.view.AddRecipient.tbxEmailValue2.text.trim() !== "") {
                payPersonJSON["secondaryEmail"] = this.view.AddRecipient.tbxEmailValue2.text.trim();
            }
            if (validationUtilityManager.isValidPhoneNumber(this.view.AddRecipient.tbxPhoneValue1.text.trim()) && this.view.AddRecipient.tbxPhoneValue1.text.trim() !== "") {
                payPersonJSON["phone"] = this.view.AddRecipient.tbxPhoneValue1.text.trim();
            }
            if (validationUtilityManager.isValidPhoneNumber(this.view.AddRecipient.tbxPhoneValue2.text.trim()) && this.view.AddRecipient.tbxPhoneValue2.text.trim() !== "") {
                payPersonJSON["secondaryPhoneNumber"] = this.view.AddRecipient.tbxPhoneValue2.text.trim();
            }
            if (!validationUtilityManager.isValidEmail(payPersonJSON["email"]) && validationUtilityManager.isValidEmail(payPersonJSON["secondaryEmail"])) {
                payPersonJSON["email"] = payPersonJSON["secondaryEmail"];
                payPersonJSON["secondaryEmail"] = undefined;
            }
            if (!validationUtilityManager.isValidPhoneNumber(payPersonJSON["phone"]) && validationUtilityManager.isValidPhoneNumber(payPersonJSON["secondaryPhoneNumber"])) {
                payPersonJSON["phone"] = payPersonJSON["secondaryPhoneNumber"];
                payPersonJSON["secondaryPhoneNumber"] = undefined;
            }
            payPersonJSON["addRecipient"] = false;
            var primaryContactForSending = [];
            var phoneText = "";
            if (payPersonJSON["phone"]) {
                phoneText = phoneText + payPersonJSON["phone"];
                primaryContactForSending = primaryContactForSending.concat([
                    [payPersonJSON["phone"], payPersonJSON["phone"]]
                ]);
            }
            if (payPersonJSON["secondaryPhoneNumber"]) {
                phoneText = phoneText + "<br>" + payPersonJSON["secondaryPhoneNumber"];
                primaryContactForSending = primaryContactForSending.concat([
                    [payPersonJSON["secondaryPhoneNumber"], payPersonJSON["secondaryPhoneNumber"]]
                ]);
            }
            var emailText = "";
            if (payPersonJSON["email"]) {
                emailText = emailText + payPersonJSON["email"];
                primaryContactForSending = primaryContactForSending.concat([
                    [payPersonJSON["email"], payPersonJSON["email"]]
                ]);
            }
            if (payPersonJSON["secondaryEmail"]) {
                emailText = emailText + "<br>" + payPersonJSON["secondaryEmail"];
                primaryContactForSending = primaryContactForSending.concat([
                    [payPersonJSON["secondaryEmail"], payPersonJSON["secondaryEmail"]]
                ]);
            }
            var data = {};
            data[kony.i18n.getLocalizedString("i18n.PayPerson.recipientName")] = CommonUtilities.changedataCase(payeeName);
            data[kony.i18n.getLocalizedString("i18n.PayPerson.nickName")] = nickName ? CommonUtilities.changedataCase(nickName) : "";
            data[kony.i18n.getLocalizedString("i18n.PayPerson.phoneNumbers")] = phoneText ? phoneText : kony.i18n.getLocalizedString("i18n.common.none");
            data[kony.i18n.getLocalizedString("i18n.PayPerson.emailAddress")] = emailText ? emailText : kony.i18n.getLocalizedString("i18n.common.none");
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSendMoneyTabView();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showManageRecipientsView();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientConfirmation")
            }]);
            this.view.confirmation.flxPrimaryContact.setVisibility(true);
            this.view.confirmation.lbxPrimaryContact.setVisibility(false);
            this.view.confirmation.flxCheckBox.setVisibility(false);
            this.view.confirmation.lblValuePrimaryContact.setVisibility(true);
            if (payPersonJSON["phone"]) {
                this.view.confirmation.lblValuePrimaryContact.text = payPersonJSON["phone"];
            } else {
                this.view.confirmation.lblValuePrimaryContact.text = payPersonJSON["email"];
            }
            var scopeObj = this;
            var actions = {
                "cancel": function() {
                    scopeObj.showQuitScreen({
                        "negative": function() {
                            scopeObj.closeQuitScreen();
                        },
                        "positive": function() {
                            scopeObj.closeQuitScreen();
                            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showManageRecipientsView();
                        }
                    });
                },
                "modify": function() {
                    scopeObj.modifyRecipient("editRecipient");
                },
                "confirm": function() {
                    payPersonJSON["primaryContactForSending"] = scopeObj.view.confirmation.lblValuePrimaryContact.text;
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.editRecipient(payPersonJSON);
                },
            };
            if (CommonUtilities.isCSRMode()) {
                actions.confirm = function() {
                    CommonUtilities.disableButtonActionForCSRMode();
                }
                this.view.confirmation.confirmButtons.btnConfirm.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.confirmation.confirmButtons.btnConfirm.hoverSkin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.confirmation.confirmButtons.btnConfirm.focusSkin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                FormControllerUtility.enableButton(self.view.confirmation.confirmButtons.btnConfirm);
            }
            this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ConfirmmRecipientDetails")
            this.view.lblHeadingAddRecipient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient");
            this.setConfirmationValues(data, actions, {});
            this.AdjustScreen();
            this.showView(["flxConfirmation"]);
            this.view.flxHeader.setFocus(true);
        },
        /**
         * This method is used to show the acknowledgement page for edit recipient.
         * @param {object} payPersonJSON - object contains info name, nickName, email, phone.
         * @param {object} result - object contains result.
         */
        showAcknowledgementEditRecipient: function(payPersonJSON, result) {
            var message = {
                "message": kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientAckMessage"),
                "image": ViewConstants.IMAGES.SUCCESS_GREEN,
            };
            var phoneText = "";
            if (payPersonJSON["phone"]) {
                phoneText = phoneText + payPersonJSON["phone"];
            }
            if (payPersonJSON["secondaryPhoneNumber"]) {
                phoneText = phoneText + "<br>" + payPersonJSON["secondaryPhoneNumber"];
            }
            var emailText = "";
            if (payPersonJSON["email"]) {
                emailText = emailText + payPersonJSON["email"];
            }
            if (payPersonJSON["secondaryEmail"]) {
                emailText = emailText + "<br>" + payPersonJSON["secondaryEmail"];
            }
            var details = {};
            details[kony.i18n.getLocalizedString("i18n.PayPerson.recipientName")] = payPersonJSON["name"];
            details[kony.i18n.getLocalizedString("i18n.PayPerson.nickName")] = payPersonJSON["nickName"];
            details[kony.i18n.getLocalizedString("i18n.PayPerson.phoneNumbers")] = phoneText ? phoneText : kony.i18n.getLocalizedString("i18n.common.none");
            details[kony.i18n.getLocalizedString("i18n.PayPerson.emailAddress")] = emailText ? emailText : kony.i18n.getLocalizedString("i18n.common.none");
            details[kony.i18n.getLocalizedString("i18n.PayPerson.primaryContactForSending")] = payPersonJSON["primaryContactForSending"];
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSendMoneyTabView();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientAcknowledgement")
            }]);
            this.view.confirmation.flxPrimaryContact.setVisibility(false);
            this.setAcknowledgementValues(message, details, {}, {
                "btnTwo": {
                    "text": kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
                    "action": function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.onSendMoney(payPersonJSON);
                    }
                },
                "btnOne": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
                    "action": function() {
                        //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.onRequestMoney(payPersonJSON);
                    }
                },
                "btnThree": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.BackToManageRecipient"),
                    "action": function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showManageRecipientsView();
                    }
                }
            });
            this.view.btnAcknowledgementTwo.toolTip = this.view.btnAcknowledgementTwo.text;
            this.view.btnAcknowledgementOne.toolTip = this.view.btnAcknowledgementOne.text;
            this.view.btnAcknowledgementThree.toolTip = this.view.btnAcknowledgementThree.text;
            this.view.btnAcknowledgementOne.setVisibility(false);
            this.showView(["flxAcknowledgement"]);
            FormControllerUtility.hideProgressBar(this.view);
            this.view.flxHeader.setFocus(true);
            this.AdjustScreen();
        },
        /**
         * Shows the pay a person deactivate screen.
         */
        showP2PDeregister: function() {
            var self = this;
            this.view.flxNoRecipient.setVisibility(false);
            this.view.OptionsAndProceed.isVisible = true;
            this.view.flxAddRecipient.setVisibility(false);
            this.view.flxRightWrapper.setVisibility(false);
            this.view.flxOptionsAndProceed.setVisibility(true);
            this.view.OptionsAndProceed.flxDetails.setVisibility(true);
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson")
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.payAPersonDeregister")
            }]);
            this.view.OptionsAndProceed.flxWarning.setVisibility(false);
            this.view.OptionsAndProceed.lblHeader.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pTitle");
            this.view.OptionsAndProceed.lblHeading2.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pWarning");
            this.view.OptionsAndProceed.lblIns1.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pGuidelineOne");
            this.view.OptionsAndProceed.lblIns2.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pGuidelineTwo");
            this.view.OptionsAndProceed.lblIns3.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pGuidelineThree");
            this.view.OptionsAndProceed.lblIns4.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pGuidelineFour");
            this.view.OptionsAndProceed.flxIAgree.setVisibility(false);
            var previousState = this.view.flxTableView.isVisible ? "flxTableView" : this.view.flxNoRecipient.isVisible ? "flxNoRecipient" : "flxAddRecipient";
            this.view.flxTableView.setVisibility(false);
            this.view.OptionsAndProceed.btnProceed.text = kony.i18n.getLocalizedString("i18n.common.proceed");
            if (CommonUtilities.isCSRMode()) {
                this.view.OptionsAndProceed.btnProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.OptionsAndProceed.btnProceed.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.OptionsAndProceed.btnProceed.hoverSkin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.OptionsAndProceed.btnProceed.focusSkin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.OptionsAndProceed.btnProceed.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.deactivateP2P();
                };
            }
            this.view.OptionsAndProceed.btnCancel.onClick = function() {
                self.view.flxOptionsAndProceed.setVisibility(false);
                self.view[previousState].setVisibility(true);
                if (kony.application.getCurrentBreakpoint() <= 1024 || orientationHandler.isTablet) {
                    this.view.flxRightWrapper.setVisibility(false);
                } else {
                    this.view.flxRightWrapper.setVisibility(true);
                }
                self.AdjustScreen();
            };
            this.AdjustScreen();
        },
        /**
         * This method shows the acknowledgement page for pay a person deactivation.
         * @param {object} userData - object contains the user data like userName, phone, email, defaultAccountForDeposit , defaultAccountForSending.
         */
        showDeactivateP2PAcknowledgement: function(userData) {
            var self = this;
            var message = {
                "message": kony.i18n.getLocalizedString("i18n.PayPerson.deactivateAcknowledgementMessage"),
                "image": ViewConstants.IMAGES.SUCCESS_GREEN,
            };
            var details = {};
            details[kony.i18n.getLocalizedString("i18n.PayPerson.yourName")] = userData.userName;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.registeredPhone")] = userData.phone;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.registeredEmail")] = userData.email;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.defaultAccountForDeposit")] = userData.defaultAccountForDeposit;
            details[kony.i18n.getLocalizedString("i18n.WireTransfer.DefaultAccountForSending")] = userData.defaultAccountForSending;
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayPerson.deactivateAcknowledgement"),
            }]);
            this.view.confirmation.flxPrimaryContact.setVisibility(false);
            this.view.btnAcknowledgementThree.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Done");
            this.setAcknowledgementValues(message, details, {}, {});
            this.view.btnAcknowledgementOne.setVisibility(false);
            this.view.btnAcknowledgementTwo.setVisibility(false);
            this.view.btnAcknowledgementThree.setVisibility(true);
            this.view.btnAcknowledgementThree.toolTip = this.view.btnAcknowledgementThree.text;
            this.view.btnAcknowledgementThree.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.loadAccountsModule();
            };
            FormControllerUtility.hideProgressBar(this.view);
            this.showView(["flxAcknowledgement"]);
            this.view.flxHeader.setFocus(true);
            this.AdjustScreen();
        },
        /**
         * Sets the data for View Activity Segment.
         * @param {object} selectedData - contains data like
         */
        showPayAPersonViewActivity: function(selectedData) {
            var self = this;
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient"),
            }, {
                text: kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
            }]);
            this.view.tableView.setVisibility(false);
            this.view.p2pActivity.setVisibility(true);
            var dataMap = {
                "lblFrom": "lblFrom",
                "lblDate": "lblDate",
                "lblAmount": "lblAmount",
                "lblStatus": "lblStatus",
                "lblRunningStatus": "lblRunningStatus",
                "lblAmountHeader": "lblAmountHeader",
                "lblFromHeader": "lblFromHeader",
                "amountTransferredTillNow": "amountTransferredTillNow"
            };
            var data = selectedData.map(function(payee) {
                return {
                    "lblFrom": payee.fromAccountName,
                    "lblFromHeader": "From:",
                    "lblAmountHeader": "Running Balance: ",
                    "lblDate": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getFormattedDateString(payee.transactionDate),
                    "lblAmount": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(payee.amount),
                    "lblStatus": payee.statusDescription,
                    "lblRunningStatus": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(payee.fromAccountBalance),
                    "amountTransferredTillNow": payee.amountTransferedTillNow,
                    "template": "flxSortP2PActivity"
                }
            });
            this.view.p2pActivity.lblAmountDeducted.text = data[0] ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(data[0]["amountTransferredTillNow"]) : kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(0);
            this.view.p2pActivity.segP2PActivity.widgetDataMap = dataMap;
            if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
                this.view.p2pActivity.flxSort.setVisibility(false);
                for (i = 0; i < data.length; i++) {
                    data[i].template = "flxP2PActivityMobile";
                }
            } else {
                this.view.p2pActivity.flxSort.setVisibility(true);
            }
            this.view.p2pActivity.segP2PActivity.setData(data);
            this.view.p2pActivity.btnBackToRecipients.onClick = function() {
                self.view.p2pActivity.setVisibility(false);
                self.view.tableView.setVisibility(true);
                self.setP2PBreadcrumbData([{
                    text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                    callback: function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                    }
                }, {
                    text: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient"),
                }]);
                self.AdjustScreen(170);
            };
            this.view.p2pActivity.btnClose.onClick = function() {
                self.view.p2pActivity.setVisibility(false);
                self.view.tableView.setVisibility(true);
                self.setP2PBreadcrumbData([{
                    text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                    callback: function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                    }
                }, {
                    text: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient"),
                }]);
            };
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         *  Sets the data for activity details for P2P.
         * @param {object} selectedRecord - object contains data like email, phone, name, toaccount and amount transferred tll now.
         */
        setP2PActivityDetails: function(selectedRecord) {
            this.view.p2pActivity.lblToAccount.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ToAccount");
            this.view.p2pActivity.lblAccountHolderName.text = selectedRecord["lblName"];
            this.view.p2pActivity.lblAccountHolderEmail.text = selectedRecord["email"];
            this.view.p2pActivity.lblAccountHolderNumber.text = selectedRecord["phone"];
            this.view.p2pActivity.lblAmountDeductedTitle.text = kony.i18n.getLocalizedString("i18n.PayAPerson.AmountTransferredTillNow");
        },
        /**
         * This method is used to switch between the two radio buttons.
         * @param {Object} RadioBtn1 - the widget reference to the first widget reference.
         * @param {Object} RadioBtn2 - the widget reference to the second widget reference.
         */
        RadioBtnAction: function(RadioBtn1, RadioBtn2) {
            if (RadioBtn1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
                RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
                RadioBtn1.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
                RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
                RadioBtn2.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
            } else {
                RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
                RadioBtn2.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
                RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
                RadioBtn1.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
            }
        },
        /**
         * This method is used to enable radio button.
         * @param {Object} radioBtn1 - the widget reference to the radio button.
         * @param {Object} radioBtn2 - the widget reference to the radio button.
         */
        enableRadioButton: function(radioBtn1, radioBtn2) {
            radioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            radioBtn1.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
            radioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
            radioBtn2.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
        },
        /**
         * This method is used to hide the add recipient screen.
         */
        hideAddRecipient: function() {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
        },
        /**
         * This method is used to show the table view in pay a person.
         */
        showTableView: function() {
            this.view.flxBottom.setVisibility(true);
            if (kony.application.getCurrentBreakpoint() <= 1024 || orientationHandler.isTablet) {
                this.view.flxRightWrapper.setVisibility(false);
            } else {
                this.view.flxRightWrapper.setVisibility(true);
            }
            this.showView(["flxAddRecipientButton", "flxTableView", "flxWhatElse"]);
            FormControllerUtility.enableButton(this.view.noRecipients.btnSendMoney);
            FormControllerUtility.enableButton(this.view.noRecipients.btnRequestMoney);
            this.view.tableView.flxSendMoney.setVisibility(false);
            this.view.tableView.flxSendReminder.setVisibility(false);
            this.view.tableView.flxRequestMoney.setVisibility(false);
            this.view.tableView.flxHorizontalLine2.setVisibility(false);
            this.view.tableView.flxTableHeaders.setVisibility(false);
            this.view.tableView.flxSearch1.setVisibility(false);
            this.view.tableView.flxHorizontalLine3.setVisibility(false);
            this.view.tableView.segP2P.setVisibility(true);
            this.view.tableView.btnSendRequest.setEnabled(true);
            this.view.tableView.btnMyRequests.setEnabled(true);
            this.view.tableView.btnSent.setEnabled(true);
            this.view.tableView.btnRecieved.setEnabled(true);
            this.view.tableView.btnManageRecepient.setEnabled(true);
        },
        /**
         * showSecondaryActions - Shows the Secondary actions flex (What else do you want? flex).
         */
        showSecondaryActions: function() {
            var scopeObj = this;
            if (scopeObj.view.secondaryActions.origin) {
                scopeObj.view.secondaryActions.origin = false;
                return;
            }
            scopeObj.view.secondaryActions.top = this.view.flxWhatElse.info.frame.y + 50 + "dp";
            scopeObj.view.secondaryActions.isVisible = true;
            scopeObj.view.secondaryActions.imgToolTip.setFocus(true);
            scopeObj.view.imgDropdown.src = ViewConstants.IMAGES.ARRAOW_UP;
            scopeObj.view.secondaryActions.forceLayout();
            scopeObj.AdjustScreen();
        },
        /**
         * This method is used to set the UI for send money.
         */
        showSendMoney: function() {
            this.showTableView();
            this.setSkinActive(this.view.tableView.flxTabs.btnSendRequest);
            this.setSkinActive(this.view.tableView.flxTabs.btnMyRequests);
            this.setSkinActive(this.view.tableView.flxTabs.btnSent);
            this.setSkinActive(this.view.tableView.flxTabs.btnRecieved);
            this.setSkinActive(this.view.tableView.flxTabs.btnManageRecepient);
            this.view.tableView.flxSendMoney.setVisibility(true);
            this.view.tableView.flxSendReminder.setVisibility(false);
            this.view.tableView.flxRequestMoney.setVisibility(false);
            this.view.tableView.flxHorizontalLine2.setVisibility(false);
            this.view.tableView.flxTableHeaders.setVisibility(false);
            this.view.tableView.flxHorizontalLine3.setVisibility(false);
            this.view.tableView.segP2P.setVisibility(false);
            this.view.tableView.flxSearch1.setVisibility(false);
            this.view.flxHeader.setFocus(true);
        },
        toggleSearch: function() {
            if (this.view.tableView.Search.isVisible === true) {
                this.view.tableView.imgSearch.src = ViewConstants.IMAGES.SEARCH_BLUE;
                this.view.tableView.Search.isVisible = false;
            } else {
                this.view.tableView.imgSearch.src = ViewConstants.IMAGES.SELECTED_SEARCH;
                this.view.tableView.Search.isVisible = true;
            }
        },
        /**
         * sets the breadcrumb value across the pay a person .
         * @param {Object} breadCrumbArray - Contains an array of objects with each object containing a text and a callback parameters in them.
         */
        setP2PBreadcrumbData: function(breadCrumbArray) {
            var self = this;
            if (kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.checkP2pEligibility()) {
                this.view.breadcrumb.setBreadcrumbData(breadCrumbArray);
            } else {
                for (var breadCrumb in breadCrumbArray) {
                    breadCrumbArray[breadCrumb].callback = function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.NavigateToPayAPerson();
                    };
                }
                this.view.breadcrumb.setBreadcrumbData(breadCrumbArray);
            }
        },
        /**
         * This method is used to show the transaction report from sent segment tab in pay a person.
         * @param {Object} transaction - this is the transaction object.
         */
        showTransactionReport: function(transaction) {
            var self = this;
            if (transaction.viewReportLink) {
                this.view.p2pReport.imgReport.src = transaction.viewReportLink;
                var height = self.view.flxHeader.info.frame.height + self.view.flxContainer.info.frame.height + self.view.flxFooter.info.frame.height;
                self.view.p2pReport.height = height + "dp";
                self.view.p2pReport.setVisibility(true);
                self.view.p2pReport.flxZoom.setVisibility(false)
                self.view.p2pReport.flxFlip.setVisibility(false);
                self.view.p2pReport.lblHeader.setFocus(true);
                self.view.p2pReport.btnClose.onClick = function() {
                    self.view.p2pReport.setVisibility(false);
                };
            }
        },
        /**
         * This method is used to get the transaction report for selected transaction in sent tab for posted transactions.
         * @param {Object} transaction - this is the transaction object.
         */
        getReportForDownload: function(transaction) {
            var payaPersonModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
            payaPersonModule.presentationController.downloadTransactionReport(transaction);
        },


        /**
         * This method is used as a keyup to check if the entered input while creating a pay a person recipient is valid or not.
         */
        checkIsValidInput: function() {
            var self = this;
            var validationUtilityManager = applicationManager.getValidationUtilManager();
            var name = this.view.AddRecipient.tbxRecipientValue.text.trim();
            if (name !== undefined && name !== null && name !== "") {
                if (this.view.AddRecipient.flxThree.isVisible) {
                    var phone1 = this.view.AddRecipient.tbxPhoneValue1.text.trim();
                    var phone2 = this.view.AddRecipient.tbxPhoneValue2.text.trim();
                    if (phone1 !== "" && validationUtilityManager.isValidPhoneNumber(phone1) && phone1.length == 10 && phone2 === "") {
                        FormControllerUtility.enableButton(this.view.AddRecipient.btnAdd);
                    } else if (phone1 !== "" && validationUtilityManager.isValidPhoneNumber(phone1) && phone1.length == 10 && phone2 !== "" && validationUtilityManager.isValidPhoneNumber(phone2) && phone2.length == 10) {
                        FormControllerUtility.enableButton(this.view.AddRecipient.btnAdd);
                    } else {
                        FormControllerUtility.disableButton(this.view.AddRecipient.btnAdd);
                    }
                } else {
                    var email1 = this.view.AddRecipient.tbxEmailValue1.text.trim();
                    var email2 = this.view.AddRecipient.tbxEmailValue2.text.trim();
                    if (email1 !== "" && validationUtilityManager.isValidEmail(email1) && email2 === "") {
                        FormControllerUtility.enableButton(this.view.AddRecipient.btnAdd);
                    } else if (email1 !== "" && validationUtilityManager.isValidEmail(email1) && email2 !== "" && validationUtilityManager.isValidEmail(email2)) {
                        FormControllerUtility.enableButton(this.view.AddRecipient.btnAdd);
                    } else {
                        FormControllerUtility.disableButton(this.view.AddRecipient.btnAdd);
                    }
                }
            } else {
                FormControllerUtility.disableButton(this.view.AddRecipient.btnAdd);
            }
        },
        /**
         *  Handles cancel transaction on click.
         * @param {Object} transaction - this is a transaction object used to cancel.
         */
        onCancelClick: function(transaction) {
            var self = this;
            this.showQuitScreen({
                negative: function() {
                    self.closeQuitScreen();
                },
                positive: function() {
                    self.closeQuitScreen();
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.cancelTransaction(transaction);
                }
            }, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), kony.i18n.getLocalizedString("I18n.billPay.QuitTransactionMsg"));
        },
        /**
         *  Handles cancel transaction occurrence on click.
         * @param {Object} transaction - this is a transaction object used to cancel.
         */
        onCancelOccurrenceClick: function(transaction) {
            var self = this;
            this.showQuitScreen({
                negative: function() {
                    self.closeQuitScreen();
                },
                positive: function() {
                    self.closeQuitScreen();
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.cancelTransactionOccurrence(transaction);
                }
            }, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), kony.i18n.getLocalizedString("i18n.common.cancelOccurrenceMessage"));
        },
        /**
         *  Handles cancel transaction series on click.
         * @param {Object} transaction - this is a transaction object used to cancel.
         */
        onCancelSeriesClick: function(transaction) {
            var self = this;
            this.showQuitScreen({
                negative: function() {
                    self.closeQuitScreen();
                },
                positive: function() {
                    self.closeQuitScreen();
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.cancelTransactionSeries(transaction);
                }
            }, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), kony.i18n.getLocalizedString("i18n.common.cancelSeriesMessage"));
        },
        /**
         * Shows pay a person Activation screen.
         * @param  {Object} userJSON - contains info like userfirstname, userLastName, phone and email.
         * @param {object} paymentAccounts - contians the list of transactions.
         */
        showActivateP2PScreen: function(userJSON, paymentAccounts) {
            var self = this;
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson")
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.payAPersonActivation")
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.P2PSettings")
            }]);
            this.view.lblHeadingActivateP2P.text = kony.i18n.getLocalizedString("i18n.PayAPerson.activatePayAPerson");
            this.view.Activatep2p.tbxNameValue.text = userJSON.userName;
            this.view.Activatep2p.tbxRegisteredPhoneValue.text = userJSON.phone;
            this.view.Activatep2p.tbxRegisteredEmailValue.text = userJSON.email;
            this.view.Activatep2p.listbxAcccFrDeposit.masterData = this.showAccountsForSelectionP2PSettings(paymentAccounts);
            this.view.Activatep2p.listbxAccFrSending.masterData = this.showAccountsForSelectionP2PSettings(paymentAccounts);
            this.view.Activatep2p.btnCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                this.view.Activatep2p.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                this.view.Activatep2p.btnCancel.setVisibility(true);
            this.view.Activatep2p.btnCancel.onClick = function() {
                self.showView(["flxOptionsAndProceed"]);
            };
            this.view.Activatep2p.btnConfirm.text = kony.i18n.getLocalizedString("i18n.transfers.Confirm"),
                this.view.Activatep2p.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Confirm"),
                this.view.Activatep2p.btnConfirm.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.ActivateP2P({
                        "defaultFromAccount": self.view.Activatep2p.listbxAccFrSending.selectedKey,
                        "defaultToAccount": self.view.Activatep2p.listbxAcccFrDeposit.selectedKey,
                    });
                };
            //Updating UI for editing settings
            this.view.Activatep2p.lblNameValue.setVisibility(false);
            this.view.Activatep2p.tbxNameValue.setVisibility(true);
            this.view.Activatep2p.tbxNameValue.setEnabled(false);
            this.view.Activatep2p.lblRegisteredPhoneValue.setVisibility(false);
            this.view.Activatep2p.tbxRegisteredPhoneValue.setVisibility(true);
            this.view.Activatep2p.tbxRegisteredPhoneValue.setEnabled(false);
            this.view.Activatep2p.lblRegisteredEmailValue.setVisibility(false);
            this.view.Activatep2p.tbxRegisteredEmailValue.setVisibility(true);
            this.view.Activatep2p.tbxRegisteredEmailValue.setEnabled(false);
            this.view.Activatep2p.lblDefaultDepositAccountValue.setVisibility(false);
            this.view.Activatep2p.listbxAcccFrDeposit.setVisibility(true);
            this.view.Activatep2p.lblDefaultSendingAccountValue.setVisibility(false);
            this.view.Activatep2p.listbxAccFrSending.setVisibility(true);
            this.showView(["flxActivateP2P"]);
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * Shows the edit- pay a person settings screen.
         * @param  {Object} userJSON - contains info like userfirstname, userLastName, phone and email.
         * @param {object} paymentAccounts - contians the list of transactions.
         */
        showP2PSettingsScreen: function(userJSON, paymentAccounts) {
            var self = this;
            var p2pAccounts = this.showAccountsForSelectionP2PSettings(paymentAccounts);
            var defaultToAccount = self.getMaskedAccountNameForID(p2pAccounts, kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getDefaultP2PToAccount());
            var defaultFromAccount = self.getMaskedAccountNameForID(p2pAccounts, kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getDefaultP2PFromAccount());
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson()
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
            }]);
            this.view.lblHeadingActivateP2P.text = kony.i18n.getLocalizedString("i18n.PayAPerson.P2PPaymentSettings");
            this.view.Activatep2p.lblHeader.text = kony.i18n.getLocalizedString("i18n.PayAPerson.P2PSettings");
            this.view.Activatep2p.lblNameValue.text = userJSON.userName;
            this.view.Activatep2p.lblRegisteredPhoneValue.text = userJSON.phone;
            this.view.Activatep2p.lblRegisteredEmailValue.text = userJSON.email;
            this.view.Activatep2p.lblDefaultDepositAccountValue.text = defaultToAccount;
            this.view.Activatep2p.lblDefaultSendingAccountValue.text = defaultFromAccount;
            this.showView(["flxActivateP2P"]);
            this.view.Activatep2p.btnCancel.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.BACK");
            this.view.Activatep2p.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.BACK");
            this.view.Activatep2p.btnCancel.setVisibility(true);
            this.view.Activatep2p.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
            };
            this.view.Activatep2p.btnConfirm.text = kony.i18n.getLocalizedString("i18n.billPay.Edit");
            this.view.Activatep2p.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
            this.view.Activatep2p.btnConfirm.onClick = function() {
                self.showEditP2PSettingsScreen(userJSON, paymentAccounts);
            };
            this.view.Activatep2p.lblNameValue.setVisibility(true);
            this.view.Activatep2p.tbxNameValue.setVisibility(false);
            this.view.Activatep2p.lblRegisteredPhoneValue.setVisibility(true);
            this.view.Activatep2p.listbxRegisteredPhoneValue.setVisibility(false);
            this.view.Activatep2p.tbxRegisteredPhoneValue.setVisibility(false);
            this.view.Activatep2p.lblRegisteredEmailValue.setVisibility(true);
            this.view.Activatep2p.flxCheckbox.setVisibility(true);
            this.view.Activatep2p.listbxRegisteredEmailValue.setVisibility(false);
            this.view.Activatep2p.tbxRegisteredEmailValue.setVisibility(false);
            this.view.Activatep2p.lblDefaultDepositAccountValue.setVisibility(true);
            this.view.Activatep2p.listbxAcccFrDeposit.setVisibility(false);
            this.view.Activatep2p.lblDefaultSendingAccountValue.setVisibility(true);
            this.view.Activatep2p.listbxAccFrSending.setVisibility(false);
            this.AdjustScreen();
        },
        /**
         * This method is used to get the payemnt accounts to show in pay a person settings page.
         * @param {Object} presentAccounts - contains the list of accounts.
         */
        showAccountsForSelectionP2PSettings: function(presentAccounts) {
            var list = [];
            for (var i = 0; i < presentAccounts.length; i++) {
                var tempList = [];
                tempList.push(presentAccounts[i].accountID);
                var tempAccountNumber = presentAccounts[i].accountID;
                tempList.push(presentAccounts[i].accountName + " ..." + tempAccountNumber.slice(-4));
                list.push(tempList);
            }
            return list;
        },
        /**
         * This method is used to get the maskedAccount Name.
         * @param {Objects} accounts - contains the list of accounts.
         * @param {Number} accountID - contains the accountID.
         */
        getMaskedAccountNameForID: function(accounts, accountID) {
            var paymentAccount;
            for (var index in accounts) {
                if (accounts[index][0] === accountID) {
                    paymentAccount = accounts[index][1];
                    break;
                }
            }
            return paymentAccount;
        },
        /**
         * This method is used to show the edit pay a person screen.
         * @param {Object} UserJSON - contains info like user full name, user phone , user email.
         * @param {Object} paymentAccounts - contains all the payment accounts which support p2p transfers.
         */
        showEditP2PSettingsScreen: function(userJSON, paymentAccounts) {
            var self = this;
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson()
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
            }]);
            this.view.lblHeadingActivateP2P.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ModifyP2PPaymentSettings");
            this.view.Activatep2p.tbxNameValue.text = userJSON.userName;
            this.view.Activatep2p.tbxRegisteredPhoneValue.text = userJSON.phone;
            this.view.Activatep2p.tbxRegisteredEmailValue.text = userJSON.email;
            this.view.Activatep2p.listbxAcccFrDeposit.masterData = this.showAccountsForSelectionP2PSettings(paymentAccounts);
            this.view.Activatep2p.listbxAccFrSending.masterData = this.showAccountsForSelectionP2PSettings(paymentAccounts);
            this.view.Activatep2p.listbxAcccFrDeposit.selectedKey = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getDefaultP2PToAccount();
            this.view.Activatep2p.listbxAccFrSending.selectedKey = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getDefaultP2PFromAccount();
            this.view.Activatep2p.btnCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.Activatep2p.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.Activatep2p.btnCancel.setVisibility(true);
            this.view.Activatep2p.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
            };
            this.view.Activatep2p.btnConfirm.text = kony.i18n.getLocalizedString("i18n.transfers.Confirm");
            this.view.Activatep2p.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Confirm");
            if (CommonUtilities.isCSRMode()) {
                this.view.Activatep2p.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.Activatep2p.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.Activatep2p.btnConfirm.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.Activatep2p.btnConfirm.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.Activatep2p.btnConfirm.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.updateP2PPreferencesForUser({
                        "defaultFromAccount": self.view.Activatep2p.listbxAccFrSending.selectedKey,
                        "defaultToAccount": self.view.Activatep2p.listbxAcccFrDeposit.selectedKey,
                    });
                };
            }
            this.view.Activatep2p.lblNameValue.setVisibility(false);
            this.view.Activatep2p.tbxNameValue.setVisibility(true);
            this.view.Activatep2p.tbxNameValue.setEnabled(false);
            this.view.Activatep2p.lblRegisteredPhoneValue.setVisibility(false);
            this.view.Activatep2p.tbxRegisteredPhoneValue.setVisibility(true);
            this.view.Activatep2p.tbxRegisteredPhoneValue.setEnabled(false);
            this.view.Activatep2p.lblRegisteredEmailValue.setVisibility(false);
            this.view.Activatep2p.tbxRegisteredEmailValue.setVisibility(true);
            this.view.Activatep2p.tbxRegisteredEmailValue.setEnabled(false);
            this.view.Activatep2p.lblDefaultDepositAccountValue.setVisibility(false);
            this.view.Activatep2p.listbxAcccFrDeposit.setVisibility(true);
            this.view.Activatep2p.lblDefaultSendingAccountValue.setVisibility(false);
            this.view.Activatep2p.listbxAccFrSending.setVisibility(true);
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * This method is used to initialize the what else do you want to do flex in pay a person.
         */
        initializeWhatElseFlex: function() {
            var scopeObj = this;
            var whatElseWidgetDataMap = {
                "lblUsers": "lblUsers",
                "lblSeparator": "lblSeparator"
            };
            var whatElseWidgetData = [{
                "lblUsers": {
                    "text": kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pTitle"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pTitle")
                },
                "lblSeparator": "."
            }, {
                "lblUsers": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.P2PSettingsTitle"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.PayAPerson.P2PSettingsTitle")
                },
                "lblSeparator": "."
            }];
            this.view.secondaryActions.segAccountTypes.widgetDataMap = whatElseWidgetDataMap;
            this.view.secondaryActions.segAccountTypes.setData(whatElseWidgetData);
            this.view.secondaryActions.segAccountTypes.onRowClick = function() {
                var selectedIndex = scopeObj.view.secondaryActions.segAccountTypes.selectedRowIndex;
                var selectedKey = scopeObj.view.secondaryActions.segAccountTypes.data[selectedRowIndex[1]]['lblUsers']["text"];
                if (selectedKey === kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pTitle")) {
                    scopeObj.showP2PDeregister();
                } else if (selectedKey === kony.i18n.getLocalizedString("i18n.PayAPerson.P2PSettingsTitle")) {
                    scopeObj.showP2PSettings();
                }
            };
        },
        /**
         * This method is used to show the pay a person settings flow.
         */
        showP2PSettings: function() {
            var preferences = {};
            preferences.p2pSettings = true;
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.loadP2PSettingsScreen(preferences);
        },
        /**
         * This method is used to set the masterdata for from accounts in send money screen in pay a person.
         * @param {Object} accounts - contains the list of accounts.
         */
        setSendPaymentAccounts: function(accounts, accountsWithCurrency) {
            var scopeObj = this;
            this.view.tableView.listbxFromAccount.masterData = accounts;
            this.setFromAccountCurrency(accountsWithCurrency);
            this.view.tableView.listbxFromAccount.onSelection = function() {
                scopeObj.setFromAccountCurrency(accountsWithCurrency);
            };
        },
        /**
         * This mehod is used to set the pay from field in the send money form of pay a person.
         * @param {Object} data - contains the set of accounts.
         */
        setPayFromData: function(data) {
            this.view.tableView.listbxDetails.masterData = this.generatePayFromData(data);
            var defaultAccount = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getDefaultP2PFromAccount();
            this.view.tableView.listbxDetails.selectedKey = defaultAccount ? defaultAccount : this.view.tableView.listbxDetails.masterData[0][0];
        },
        /**
         * This method is used to generate the accounts to show for pay a person send money.
         * @param {Object} data - contains the list of accounts.
         */
        generatePayFromData: function(data) {
            var list = [];
            this.accountinfo = [];
            var i = 0;
            for (i = 0; i < data.length; i++) {
                var tempList = [];
                tempList.push(data[i].accountID);
                tempList.push(data[i].accountName);
                list.push(tempList);
            }
            for (i = 0; i < data.length; i++) {
                var temp = [];
                temp.push(data[i].accountID);
                temp.push(data[i].availableBalance);
                this.accountinfo.push(temp);
            }
            return list;
        },
        /**
         * used to get the amount
         * @param {number} amount amount
         * @returns {number} amount
         */
        deformatAmount: function(amount) {
            return applicationManager.getFormatUtilManager().deFormatAmount(amount);
        },
        getTnC: function(dataItem) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showConfimPage(dataItem);
        },
        /**
         * This method is used to show the confirmation screen for pay a person send money.
         * @param {Object} payeeObj - This contains the pay a person info like name, nickname, email, phone,primary contact for sending and transaction object fields like from account number and amount.
         */
        sendMoneyToPerson: function(PayeeObj, TnCcontentTransfer) {
            var self = this;
            self.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyConfirmation"),
            }]);
            var result = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule")
                .presentationController.validatePayAPersonAmount({
                    amount: self.deformatAmount(self.view.tableView.tbxAmount.text),
                    fromAccountNumber: self.view.tableView.listbxFromAccount.selectedKeyValue[0]
                });
            var resultDate = self.validateBillPayDate();
            if (!result.isAmountValid) {
                self.view.tableView.lblErrorAmount.text = result.errMsg;
                self.view.tableView.lblErrorAmount.setVisibility(true);
                return;
            } else if (this.view.tableView.listbxFrequency.selectedKey !== 'Once' && this.view.tableView.listbxHowLong.selectedKey !== 'NO_OF_RECURRENCES' && !resultDate.isDateValid) {
                var message = kony.i18n.getLocalizedString("i18n.transfers.errors.invalidDeliverByDate");
                self.view.rtxDowntimeWarning.text = message;
                self.view.flxDowntimeWarning.setVisibility(true);
                self.AdjustScreen();
                self.view.flxDowntimeWarning.setFocus(true);
                self.AdjustScreen();
                return;
            }
            self.view.flxBottom.setVisibility(false);
            var sendMoneyData = {
                "From": self.view.tableView.listbxFromAccount.selectedKeyValue[1],
                "To": self.view.tableView.lblPaytoValue.text,
                "Amount": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency((self.view.tableView.tbxAmount.text + ""), false, this.view.tableView.lblCurrency.text),
                "Date": self.view.tableView.clndrSendOn.date,
                "Notes": self.view.tableView.tbxOptional.text,
                "Frequency": self.view.tableView.listbxFrequency.selectedKeyValue[1]
            };
            var totalPaymentMoney = self.view.tableView.tbxAmount.text;
            totalPaymentMoney = self.deformatAmount(totalPaymentMoney.toString());
            var extraData = {};
            if (applicationManager.getConfigurationManager().serviceFeeFlag === "true") {
                var fee = applicationManager.getConfigurationManager().p2pServiceFee;
                extraData = {
                    "Fees": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(fee),
                    "Total": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency((Number(totalPaymentMoney) + Number(fee)))
                };
                totalPaymentMoney = Number(Number(totalPaymentMoney) + Number(fee));
            } else {
                self.view.confirmation.flxCheckBox.isVisible = false;
                FormControllerUtility.enableButton(self.view.confirmation.confirmButtons.btnConfirm);
            }
            self.view.CustomPopup1.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Quit");
            self.view.CustomPopup1.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.PayAPerson.CancelAlert");
            var actions = {
                "cancel": function() {
                    self.showQuitScreen({
                        "negative": function() {
                            self.closeQuitScreen();
                        },
                        "positive": function() {
                            self.closeQuitScreen();
                            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                        }
                    });
                },
                "modify": function() {
                    self.view.flxConfirmation.setVisibility(false);
                    self.view.AllFormsConfirm.isVisible = false;
                    self.view.flxTableView.setVisibility(true);
                    self.view.flxAddRecipientButton.setVisibility(true);
                    self.view.flxWhatElse.setVisibility(true);
                    self.view.forceLayout();
                    self.AdjustScreen();
                },
                "confirm": function() {
                    if ((CommonUtilities.isCSRMode() && PayeeObj.context === "sendMoneyToNewRecipient") || (CommonUtilities.isCSRMode() && PayeeObj.isEditFlow)) {
                        CommonUtilities.disableButtonActionForCSRMode();
                        self.view.confirmation.confirmButtons.btnConfirm.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                        self.view.confirmation.confirmButtons.btnConfirm.hoverSkin = FormControllerUtility.disableButtonSkinForCSRMode();
                        self.view.confirmation.confirmButtons.btnConfirm.focusSkin = FormControllerUtility.disableButtonSkinForCSRMode();
                    } else {
                        var validateDateRange = function() {
                            var dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
                            var targetDate = self.view.tableView.clndrSendOn.date;
                            var todayDateObj = CommonUtilities.getServerDateObject();
                            var targetDateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(targetDate, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase())
                            if (todayDateObj < targetDateObj) {
                                return "1";
                            }
                            return "0";
                        };
                        var sendMoneyPaymentJSON = {
                            "From": self.view.tableView.listbxFromAccount.selectedKeyValue[1],
                            "To": self.view.tableView.lblPaytoValue.text,
                            "Amount": self.view.tableView.tbxAmount.text,
                            "Date": self.view.tableView.clndrSendOn.date,
                            "Note": self.view.tableView.tbxOptional.text,
                            "fromAccountNumber": self.view.tableView.listbxFromAccount.selectedKeyValue[0],
                            "frequencyType": self.view.tableView.listbxFrequency.selectedKeyValue[0],
                            "p2pContact": self.view.tableView.listbxSentby.selectedKeyValue[1],
                            "personId": PayeeObj.PayPersonId ? PayeeObj.PayPersonId : PayeeObj.personId,
                            "amount": totalPaymentMoney,
                            "scheduledDate": self.view.tableView.clndrSendOn.date,
                            "transactionsNotes": self.view.tableView.tbxOptional.text,
                            "numberOfRecurrences": self.view.tableView.tbxNoOfRecurrences.text,
                            "frequencyStartDate": self.view.tableView.clndrSendOn.date,
                            "frequencyEndDate": self.view.tableView.flxCalenderEndingOn.clndrEndingOn.date,
                            "hasHowLong": self.view.tableView.listbxHowLong.selectedKey,
                            "context": PayeeObj.context ? PayeeObj.context : "",
                            "isScheduled": validateDateRange(),
                            "email": PayeeObj.email,
                            "secondaryEmail": PayeeObj.secondaryEmail,
                            "phone": PayeeObj.phone,
                            "secondaryPhoneNumber": PayeeObj.secondaryPhone,
                            "payPersonPhone": PayeeObj.payPersonPhone,
                            "payPersonEmail": PayeeObj.payPersonEmail,
                            "payPersonName": PayeeObj.payPersonName,
                            "name": PayeeObj.name,
                            "nickName": PayeeObj.nickName,
                            "transactionCurrency": self.returnCurrencyCode(self.view.tableView.lblCurrency.text)
                        };
                        if (PayeeObj.statusDescription !== null && PayeeObj.statusDescription !== "Successful") {
                            sendMoneyPaymentJSON.transactionId = PayeeObj.transactionId;
                        }
                        if (PayeeObj.context !== "") {
                            sendMoneyPaymentJSON.payPersonObject = self.sendPayeeObj;
                        }
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.checkMFAP2PSendMoney(sendMoneyPaymentJSON);
                    }
                },
            };
            self.setConfirmationValues(sendMoneyData, actions, extraData, TnCcontentTransfer);
            self.view.forceLayout();
            self.AdjustScreen();
        },
        returnCurrencyCode: function(currencySymbol) {
            return applicationManager.getFormatUtilManager().getCurrencySymbolCode(currencySymbol);
        },
        getDateObj: function(dateComponents) {
            var date = new Date();
            date.setDate(dateComponents[0]);
            date.setMonth(parseInt(dateComponents[1]) - 1);
            date.setFullYear(dateComponents[2]);
            date.setHours(0, 0, 0, 0)
            return date;
        },
        /**
         * used to check Date validations for OneTime Payment.
         */
        validateBillPayDate: function() {
            var resultDate = {
                isDateValid: false
            };
            var sendOnDate = this.getDateObj(this.view.tableView.clndrSendOn.dateComponents);
            var deliverByDate = this.getDateObj(this.view.tableView.flxCalenderEndingOn.clndrEndingOn.dateComponents);
            if (sendOnDate.getTime() < deliverByDate.getTime()) {
                resultDate.isDateValid = true;
            }
            return resultDate;
        },
        /**
         * This method is used to show the send money in the pay a person screen.
         * @param {Object} dataItem - contains the data for pay a person like name, email, phone, primary contact for sending and transaction object fields like amount, from account number, frequency, recurrence.
         */
        onSendMoneyBtnClick: function(dataItem) {
            var self = this;
            var scopeObj = this;
            this.setSkinActive(this.view.tableView.flxTabs.btnSendRequest);
            this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
            this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
            this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
            this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
            this.view.flxOptionsAndProceed.setVisibility(false);
            this.view.lblHeader.text = kony.i18n.getLocalizedString("i18n.p2p.PayAPerson");
            this.view.tableView.tbxNoOfRecurrences.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNumberOfRecurrences");
            this.view.tableView.tbxNoOfRecurrences.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNumberOfRecurrences");
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
            }]);
            self.sendPayeeObj = dataItem;

            function callOnKeyUp() {
                self.checkValidityP2PTransferForm();
            }

            function cancelSendMoney() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
            }
            this.view.btnCancel.onClick = function() {
                scopeObj.view.flxTermsAndConditionsPopUp.setVisibility(false);
            };
            this.view.flxClose.onClick = function() {
                scopeObj.view.flxTermsAndConditionsPopUp.setVisibility(false);
            };
            this.view.btnSave.onClick = function() {
                FormControllerUtility.setLblCheckboxState(FormControllerUtility.isFontIconChecked(scopeObj.view.lblTCContentsCheckBoxIcon), scopeObj.view.confirmation.lblCheckBoxIcon);
                if (FormControllerUtility.isFontIconChecked(scopeObj.view.confirmation.lblCheckBoxIcon)) {
                    FormControllerUtility.enableButton(scopeObj.view.confirmation.confirmButtons.btnConfirm);
                } else {
                    FormControllerUtility.disableButton(scopeObj.view.confirmation.confirmButtons.btnConfirm);
                }
                scopeObj.view.flxTermsAndConditionsPopUp.setVisibility(false);
            };
            this.view.flxTCContentsCheckbox.onClick = function() {
                FormControllerUtility.toggleFontCheckbox(scopeObj.view.lblTCContentsCheckBoxIcon);
            };
            this.view.confirmation.btnTermsAndConditions.onClick = function() {
                FormControllerUtility.setLblCheckboxState(FormControllerUtility.isFontIconChecked(scopeObj.view.confirmation.lblCheckBoxIcon), scopeObj.view.lblTCContentsCheckBoxIcon);
                scopeObj.view.flxTermsAndConditionsPopUp.height = scopeObj.view.flxHeader.info.frame.height + scopeObj.view.flxMainContainer.info.frame.height + scopeObj.view.flxFooter.info.frame.height;
                scopeObj.view.flxTermsAndConditionsPopUp.setVisibility(true);
            };
            this.view.tableView.segP2P.setVisibility(false);
            this.setSendMoneyUI(dataItem);
            this.view.tableView.flxTableHeaders.setVisibility(false);
            this.view.tableView.flxSearch1.setVisibility(false);
            this.view.tableView.flxSendMoney.setVisibility(true);
            this.view.tableView.segP2P.setVisibility(false);
            this.view.flxAcknowledgement.setVisibility(false);
            this.view.flxTableView.setVisibility(true);
            this.view.flxAddRecipientButton.setVisibility(true);
            this.view.flxWhatElse.setVisibility(true);
            if (kony.application.getCurrentBreakpoint() <= 1024 || orientationHandler.isTablet) {
                this.view.flxRightWrapper.setVisibility(false);
            } else {
                this.view.flxRightWrapper.setVisibility(true);
            }
            this.view.tableView.flxPagination.setVisibility(false);
            this.view.tableView.flxRequestMoney.setVisibility(false);
            // this.view.tableView.Search.setVisibility(false);
            this.view.tableView.tbxNoOfRecurrences.onKeyUp = callOnKeyUp;
            this.view.tableView.listbxFrequency.onSelection = this.onFrequencyChanged.bind(this);
            this.view.tableView.listbxHowLong.onSelection = this.onHowLongChange.bind(this);
            this.view.tableView.btnSendMoney.onClick = this.getTnC.bind(this, dataItem);
            this.view.tableView.btnCancel.onClick = dataItem.onCancel || cancelSendMoney;
            this.view.confirmation.flxCheckBox.onclick = self.termsAndConditionsonsClick;
            this.view.confirmation.lblCheckBoxIcon.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            this.view.confirmation.lblCheckBoxIcon.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            FormControllerUtility.disableButton(this.view.confirmation.confirmButtons.btnConfirm);
            this.view.forceLayout();
            this.AdjustScreen();
        },
        /**
         * This method is used to show the acknowledgement send money screen.
         * @param {Object} requestObj - this contains p2p recipient info of name, nickname, phone, email and transaction object info like amount, frequncy and recurrenc.
         * @param {Object} status - ths contains the info like transactionId.
         * @param {String} accountName - contains the from account name.
         * @param {Numebr} accountBalance - contains the from account balance.
         */
        showAcknowledgementSendMoney: function(requestObj, status, accountName, accountBalance) {
            this.view.tableView.tbxNoOfRecurrences.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNumberOfRecurrences");
            this.setP2PBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
                callback: function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                }
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyConfirmation"),
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyAcknowledgement"),
            }]);
            if (status.status == "Pending") {
                var message = {
                    "message": kony.i18n.getLocalizedString("i18n.PayAPerson.PendingMoneyMessage"),
                    "image": ViewConstants.IMAGES.SUCCESS_GREEN,
                };
            }
            if (status.status == "Sent") {
                var message = {
                    "message": kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyMessage") + " " + requestObj.To,
                    "image": ViewConstants.IMAGES.SUCCESS_GREEN,
                };
            }

            var extraMessage = {
                "referenceNumber": status.referenceId ? status.referenceId : status.transactionId,
                "accountType": accountName,
                "amount": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(accountBalance, false, this.view.tableView.lblCurrency.text),
            }
            var details = {};
            details[kony.i18n.getLocalizedString("i18n.PayPerson.recipientName")] = requestObj.To;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.using")] = requestObj.p2pContact;
            details[kony.i18n.getLocalizedString("i18n.StopCheckPayments.Amount")] = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(requestObj.amount, false, this.view.tableView.lblCurrency.text);
            if (requestObj.frequencyType === "Once") {
                details[kony.i18n.getLocalizedString("i18n.StopCheckPayments.Date")] = requestObj.Date;
            } else {
                message.message = kony.i18n.getLocalizedString("i18n.common.scheduled.acknowledgement") + " " + requestObj.To;
                if (requestObj.hasHowLong === "ON_SPECIFIC_DATE") {
                    details[kony.i18n.getLocalizedString("i18n.transfers.start_date")] = requestObj.frequencyStartDate;
                    details[kony.i18n.getLocalizedString("i18n.transfers.end_date")] = requestObj.frequencyEndDate;
                }
                if (requestObj.hasHowLong === "NO_OF_RECURRENCES") {
                    details[kony.i18n.getLocalizedString("i18n.StopCheckPayments.Date")] = requestObj.Date;
                    details[kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrencescolon")] = requestObj.numberOfRecurrences;
                }
            }
            details[kony.i18n.getLocalizedString("i18n.PayAPerson.Note")] = requestObj.transactionsNotes;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.defaultAccountForSendingMoney")] = requestObj.From;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.frequency")] = requestObj.frequencyType;
            var actions = {
                "btnOne": {
                    "text": "null",
                    "action": function() {}
                },
                "btnTwo": {
                    "text": kony.i18n.getLocalizedString("i18n.transfers.makeanothertransfer"),
                    "action": function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                    }
                },
                "btnThree": {
                    "text": kony.i18n.getLocalizedString("i18n.payaperson.viewSentTrasactions"),
                    "action": function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSentTransactionsView();
                    }
                }
            };
            if (requestObj.context === "sendMoneyToNewRecipient") {
                requestObj.payPersonObject.primaryContactForSending = requestObj.p2pContact;
                requestObj.payPersonObject.transactionId = status.referenceId;
                if (requestObj.personId === undefined || requestObj.personId === null || requestObj.personId === "") {
                    actions.btnOne.text = kony.i18n.getLocalizedString("i18n.payaperson.savereciepient");
                    actions.btnOne.toolTip = kony.i18n.getLocalizedString("i18n.payaperson.savereciepient");
                    actions.btnOne.action = function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.createP2PPayee(requestObj.payPersonObject);
                    };
                    this.view.btnAcknowledgementOne.setVisibility(true);
                }
            }
            this.setAcknowledgementValues(message, details, extraMessage, actions);
            this.view.btnAcknowledgementTwo.toolTip = this.view.btnAcknowledgementTwo.text;
            this.view.btnAcknowledgementOne.toolTip = this.view.btnAcknowledgementOne.text;
            this.view.btnAcknowledgementThree.toolTip = this.view.btnAcknowledgementThree.text;
            this.showView(["flxAcknowledgement"]);
            this.view.flxHeader.setFocus(true);
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * This method is used to set the send money screen UI in pay a person.
         * @param {Object} dataItem - this contains the info like pay a person name, nickname, phone, email and transaction object fields like amount, frequency and recurrence.
         */
        setSendMoneyUI: function(dataItem) {
            var self = this;
            this.view.tableView.listbxFrequency.masterData = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.listboxFrequencies(dataItem.context);
            var dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
            this.view.tableView.clndrSendOn.dateFormat = dateFormat;
            this.view.tableView.flxCalenderEndingOn.clndrEndingOn.dateFormat = dateFormat;
            var date = CommonUtilities.getServerDateObject();
            this.view.tableView.lblCurrency.text = applicationManager.getConfigurationManager().getCurrencyCode();
            this.view.tableView.clndrSendOn.dateComponents = [date.getDate(), date.getMonth() + 1, date.getFullYear()];
            this.view.tableView.flxCalenderEndingOn.clndrEndingOn.dateComponents = [date.getDate(), date.getMonth() + 1, date.getFullYear()];
            this.view.tableView.tbxNoOfRecurrences.text = "";
            if (dataItem.statusDescription && dataItem.statusDescription !== "Successful") {
                this.view.tableView.clndrSendOn.dateFormat = dateFormat;
                if (dataItem.scheduledDate) {
                    var dateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dataItem.scheduledDate, "YYYY-MM-DD");
                    CommonUtilities.disableOldDaySelection(this.view.tableView.clndrSendOn);
                    this.view.tableView.clndrSendOn.dateComponents = [dateObj.getDate(), dateObj.getMonth() + 1, dateObj.getFullYear()];
                } else {
                    CommonUtilities.disableOldDaySelection(this.view.tableView.clndrSendOn);
                }
            } else {
                if (dataItem.frequencyStartDate) {
                    var startDate = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dataItem.frequencyStartDate, "YYYY-MM-DD");
                    dataItem.frequencyStartDate = CommonUtilities.getFrontendDateString(dataItem.frequencyStartDate);
                    CommonUtilities.disableOldDaySelection(this.view.tableView.clndrSendOn);
                    this.view.tableView.clndrSendOn.dateComponents = [startDate.getDate(), startDate.getMonth() + 1, startDate.getFullYear()];
                } else {
                    this.view.tableView.clndrSendOn.dateComponents = CommonUtilities.getServerDateComponent();
                    CommonUtilities.disableOldDaySelection(this.view.tableView.clndrSendOn);
                }
            }
            if (dataItem.frequencyEndDate) {
                var endDate = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dataItem.frequencyEndDate, "YYYY-MM-DD");
                dataItem.frequencyEndDate = CommonUtilities.getFrontendDateString(dataItem.frequencyEndDate);
                CommonUtilities.disableOldDaySelection(this.view.tableView.flxCalenderEndingOn.clndrEndingOn);
                this.view.tableView.flxCalenderEndingOn.clndrEndingOn.dateComponents = [endDate.getDate(), endDate.getMonth() + 1, endDate.getFullYear()];
            } else {
                this.view.tableView.flxCalenderEndingOn.clndrEndingOn.dateComponents = CommonUtilities.getServerDateComponent();
                CommonUtilities.disableOldDaySelection(this.view.tableView.flxCalenderEndingOn.clndrEndingOn);
            }
            this.view.tableView.tbxOptional.text = dataItem.transactionsNotes ? dataItem.transactionsNotes : "";
            this.view.tableView.tbxAmount.text = dataItem.amount ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.formatCurrency(dataItem.amount, true) : "";
            var fullName = dataItem.payPersonName ? dataItem.payPersonName : dataItem.name;
            var payAPersonDefaultAccountNumber = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.getDefaultP2PFromAccount();
            var defaultAccountNumber = dataItem.fromAccountNumber || payAPersonDefaultAccountNumber || this.view.tableView.listbxFromAccount.masterData[0][0];
            this.view.tableView.listbxFromAccount.selectedKey = defaultAccountNumber;
            this.view.tableView.lblPaytoValue.text = fullName;
            this.view.tableView.listbxFrequency.selectedKey = dataItem.frequencyType ? dataItem.frequencyType : this.view.tableView.listbxFrequency.masterData[0][0];
            if (dataItem.numberOfRecurrences && dataItem.numberOfRecurrences !== null && dataItem.numberOfRecurrences !== "" && dataItem.numberOfRecurrences !== "0") {
                this.view.tableView.listbxHowLong.selectedKey = "NO_OF_RECURRENCES";
                this.view.tableView.tbxNoOfRecurrences.text = dataItem.numberOfRecurrences ? dataItem.numberOfRecurrences : "";
            } else if (dataItem.frequencyStartDate !== null && dataItem.frequencyStartDate !== "" && dataItem.frequencyEndDate !== null && dataItem.frequencyEndDate !== "") {
                this.view.tableView.listbxHowLong.selectedKey = "ON_SPECIFIC_DATE";
            }
            var emailMasterDrop = [];
            var sendRecep = self.checkRequriedValues(dataItem);
            for (var i = 0; i < sendRecep.length; i++) {
                if (sendRecep[i] !== null) {
                    var data = [];
                    data.push(sendRecep[i]);
                    data.push(sendRecep[i])
                    if (sendRecep[i] !== undefined && sendRecep[i] !== "") {
                        emailMasterDrop.push(data);
                    }
                }
            }
            this.view.tableView.listbxSentby.masterData = emailMasterDrop;
            if (dataItem.p2pContact) {
                this.view.tableView.listbxSentby.selectedKey = dataItem.p2pContact ? dataItem.p2pContact : this.view.tableView.listbxSentby.masterData[0][0];
            }
            if (dataItem.lblPrimaryContact !== "" || dataItem.lblPrimaryContact !== null || dataItem.lblPrimaryContact !== undefined) {
                this.view.tableView.listbxSentby.selectedKey = dataItem.lblPrimaryContact ? dataItem.lblPrimaryContact : this.view.tableView.listbxSentby.masterData[0][0];
            }
            if (self.view.tableView.tbxAmount.text !== "" && self.view.tableView.tbxAmount.text !== null) {
                FormControllerUtility.enableButton(this.view.tableView.btnSendMoney);
            } else {
                FormControllerUtility.disableButton(this.view.tableView.btnSendMoney);
            }
            self.getFrequencyAndFormLayout(this.view.tableView.listbxFrequency.selectedKey, this.view.tableView.listbxHowLong.selectedKey);
            this.view.tableView.lblErrorAmount.setVisibility(false);
            self.view.flxConfirmation.setVisibility(false);
            self.view.AllFormsConfirm.isVisible = false;
            self.view.flxTableView.setVisibility(true);
            self.view.flxAddRecipientButton.setVisibility(true);
            self.view.flxWhatElse.setVisibility(true);
            this.view.forceLayout();
        },
        setFromAccountCurrency: function(accountsWithCurrency) {
            var fromAccountCurrencyCode = this.getFromAccount(accountsWithCurrency, this.view.tableView.listbxFromAccount.selectedKey);
            this.view.tableView.lblCurrency.text = applicationManager.getFormatUtilManager().getCurrencySymbol(fromAccountCurrencyCode);
        },
        getFromAccount: function(data, value) {
            data = data.filter(function(item) {
                return item[0] === value
            });
            return data[0][1];
        },
        checkIfAllAreFilled: function() {
            if (this.view.tableView.tbxRequestMoneyAmount.text) {
                FormControllerUtility.enableButton(this.view.tableView.btnRequestMoneySendRequest);
            } else {
                FormControllerUtility.disableButton(this.view.tableView.btnRequestMoneySendRequest);
            }
        },
        showTermsAndConditionsSuccessScreen: function(TnCcontent) {
            if (TnCcontent.alreadySigned) {
                this.view.OptionsAndProceed.flxIAgree.setVisibility(false);
            } else {
                CommonUtilities.disableButton(this.view.OptionsAndProceed.btnProceed);
                this.view.OptionsAndProceed.lblCheckbox.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                this.view.OptionsAndProceed.flxIAgree.setVisibility(true);
                if (TnCcontent.contentTypeId === OLBConstants.TERMS_AND_CONDITIONS_URL) {
                    this.view.OptionsAndProceed.btnTC.onClick = function() {
                        window.open(TnCcontent.termsAndConditionsContent);
                    }
                } else {
                    this.view.OptionsAndProceed.btnTC.onClick = this.showTermsAndConditionPopUp;
                }
                this.view.flxClose.onClick = this.hideTermsAndConditionPopUp;
                this.view.OptionsAndProceed.lblCheckbox.onTouchEnd = this.toggleTnC.bind(this, this.view.OptionsAndProceed.lblCheckbox, this.view.OptionsAndProceed.btnProceed);
                this.setTnCDATASection(TnCcontent.termsAndConditionsContent);
            }
        },
        showTermsAndConditionPopUp: function() {
            var height = this.view.flxHeader.info.frame.height + this.view.flxContainer.info.frame.height + this.view.flxFooter.info.frame.height;
            this.view.flxTermsAndConditionsPopUp.height = height + "dp";
            this.view.flxTermsAndConditionsPopUp.setVisibility(true);
        },
        hideTermsAndConditionPopUp: function() {
            this.view.flxTermsAndConditionsPopUp.setVisibility(false);
        },
        setTnCDATASection: function(content) {
            this.view.rtxTC.text = content;
            this.view.flxTCContents.isVisible = false;
            FormControllerUtility.setHtmlToBrowserWidget(this, this.view.brwBodyTnC, content);
        },
        toggleTnC: function(widget, buttonWidget) {
            CommonUtilities.toggleFontCheckbox(widget);
            if (widget.text === OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED)
                CommonUtilities.disableButton(buttonWidget);
            else
                CommonUtilities.enableButton(buttonWidget);
        },
        /**
         * This method handles the terms and conditions click in the send oney confirmation screen.
         */
        termsAndConditionsonsClick: function() {
            var self = this;
            if (self.view.confirmation.lblCheckBoxIcon.text === ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
                self.view.confirmation.lblCheckBoxIcon.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                self.view.confirmation.lblCheckBoxIcon.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                FormControllerUtility.enableButton(self.view.confirmation.confirmButtons.btnConfirm);
            } else {
                self.view.confirmation.lblCheckBoxIcon.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                self.view.confirmation.lblCheckBoxIcon.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                FormControllerUtility.disableButton(self.view.confirmation.confirmButtons.btnConfirm);
            }
            self.view.forceLayout();
        },
        /**
         * This method is used as an onclick for the frequency change.
         */
        onFrequencyChanged: function() {
            var self = this;
            self.getFrequencyAndFormLayout(this.view.tableView.listbxFrequency.selectedKey,
                this.view.tableView.listbxHowLong.selectedKey);
            self.checkValidityP2PTransferForm();
        },
        /**
         * This method is used as an onclick for the frequency change in the send money for pay a person.
         */
        onHowLongChange: function() {
            var self = this;
            self.getForHowLongandFormLayout(this.view.tableView.listbxHowLong.selectedKey);
            self.checkValidityP2PTransferForm();
        },
        /**
         * This method is used to validate if all mandatory fields are present or not.
         */
        checkValidityP2PTransferForm: function() {
            var self = this;
            var re = new RegExp("^([0-9])+(\.[0-9]{1,2})?$");
            var disableCreateP2PButton = function() {
                FormControllerUtility.disableButton(this.view.tableView.btnSendMoney);
            }.bind(this);
            this.view.tableView.lblErrorAmount.setVisibility(false);
            if (this.view.tableView.tbxAmount.text === null || this.view.tableView.tbxAmount.text === "" || isNaN(self.deformatAmount(this.view.tableView.tbxAmount.text)) || !re.test(self.deformatAmount(this.view.tableView.tbxAmount.text)) || (parseFloat(self.deformatAmount(this.view.tableView.tbxAmount.text)) <= 0)) {
                disableCreateP2PButton();
                return;
            }
            if (this.view.tableView.listbxFrequency.selectedKey !== "Once" && this.view.tableView.listbxHowLong.selectedKey === "NO_OF_RECURRENCES" && this.view.tableView.tbxNoOfRecurrences.text === "") {
                disableCreateP2PButton();
                return;
            }
            if (isNaN(this.view.tableView.tbxNoOfRecurrences.text) || parseInt(this.view.tableView.tbxNoOfRecurrences.text) <= 0) {
                disableCreateP2PButton();
                return;
            }
            FormControllerUtility.enableButton(this.view.tableView.btnSendMoney);
        },
        /**
         * This method is used to get frequency and form layout in pay a person.
         */
        getFrequencyAndFormLayout: function(frequencyValue, howLangValue) {
            if (frequencyValue !== "Once" && howLangValue !== 'NO_OF_RECURRENCES') {
                this.makeLayoutfrequencyWeeklyDate();
            } else if (frequencyValue !== "Once" && howLangValue === 'NO_OF_RECURRENCES') {
                this.makeLayoutfrequencyWeeklyRecurrences();
            } else {
                this.makeLayoutfrequencyOnce();
            }
        },
        getForHowLongandFormLayout: function(value) {
            if (value === "ON_SPECIFIC_DATE") {
                this.makeLayoutfrequencyWeeklyDate();
            } else if (value === "NO_OF_RECURRENCES") {
                this.makeLayoutfrequencyWeeklyRecurrences();
            } else if (value === "CONTINUE_UNTIL_CANCEL") {
                this.makeLayoutfrequencyWeeklyCancel();
            }
        },
        makeLayoutfrequencyWeeklyDate: function() {
            this.view.tableView.lblHowLong.setVisibility(true);
            this.view.tableView.listbxHowLong.setVisibility(true);
            this.view.tableView.lblSendOn.text = kony.i18n.getLocalizedString("i18n.transfers.start_date");
            this.view.tableView.lblNoOfRecurrences.text = kony.i18n.getLocalizedString("i18n.transfers.end_date");
            this.view.tableView.flxCalenderEndingOn.setVisibility(true);
            this.view.tableView.lblNoOfRecurrences.setVisibility(true);
            this.view.tableView.tbxNoOfRecurrences.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                this.view.tableView.flxSendMoney.height = "930dp";
            }
            this.view.forceLayout();
        },
        makeLayoutfrequencyWeeklyRecurrences: function() {
            this.view.tableView.lblHowLong.setVisibility(true);
            this.view.tableView.listbxHowLong.setVisibility(true);
            this.view.tableView.flxCalenderEndingOn.setVisibility(false);
            this.view.tableView.lblNoOfRecurrences.setVisibility(true);
            this.view.tableView.tbxNoOfRecurrences.setVisibility(true);
            if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                this.view.tableView.flxSendMoney.height = "930dp";
            }
            this.view.tableView.lblSendOn.text = kony.i18n.getLocalizedString("i18n.transfers.send_on");
            this.view.tableView.lblNoOfRecurrences.text = kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrencescolon");
        },
        makeLayoutfrequencyWeeklyCancel: function() {
            /*  this.view.lblForhowLong.setVisibility(true);
             this.view.lbxForHowLong.setVisibility(true);
             this.view.flxCalEndingOn.setVisibility(false);
             this.view.lblNoOfRecOrEndingOn.setVisibility(false);
             this.view.tbxNoOfRecurrences.setVisibility(false);
             this.view.forceLayout(); */
        },
        makeLayoutfrequencyOnce: function() {
            this.view.tableView.lblSendOn.text = kony.i18n.getLocalizedString("i18n.transfers.send_on");
            this.view.tableView.lblHowLong.setVisibility(false);
            this.view.tableView.listbxHowLong.setVisibility(false);
            this.view.tableView.lblNoOfRecurrences.setVisibility(false);
            this.view.tableView.tbxNoOfRecurrences.setVisibility(false);
            this.view.tableView.flxCalenderEndingOn.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                this.view.tableView.flxSendMoney.height = "750dp";
            }
            this.view.forceLayout();
        },
        checkRequriedValues: function(dataItem) {
            return [dataItem.email, dataItem.secondaryEmail2, dataItem.phone, dataItem.secondaryPhone, dataItem.secondaryPhoneNumber2, dataItem.payPersonPhone, dataItem.secondaryPhoneNumber, dataItem.secondoryPhoneNumber, dataItem.payPersonEmail, dataItem.secondaryEmail];
        },
        /**
         * This method is used to turn the visibility on to the specified views.
         * @param {object} views - Contains the set of views to be made visible.
         */
        showView: function(views) {
            this.view.flxActivateP2P.setVisibility(false);
            this.view.flxAcknowledgement.setVisibility(false);
            this.view.flxAddRecipientButton.setVisibility(false);
            this.view.flxAddRecipient.setVisibility(false);
            this.view.flxTableView.setVisibility(false);
            this.view.flxPrint.setVisibility(false);
            this.view.flxNoRecipient.setVisibility(false);
            this.view.flxOptionsAndProceed.setVisibility(false);
            this.view.flxConfirmation.setVisibility(false);
            this.view.AllFormsConfirm.isVisible = false;
            this.view.flxWhatElse.setVisibility(false);
            for (var i = 0; i < views.length; i++) {
                this.view[views[i]].isVisible = true;
                if (views[i] === "flxAcknowledgement") {
                    if (CommonUtilities.isPrintEnabled()) {
                        if (kony.application.getCurrentBreakpoint() == 640 || kony.application.getCurrentBreakpoint() == 1024) {
                            this.view.flxPrint.setVisibility(false);
                        } else {
                            this.view.flxPrint.setVisibility(true);
                        }
                        this.view.lblPrintfontIconConfirm.onTouchStart = this.onClickPrint;
                        this.view.lblPrintfontIconConfirm.toolTip = kony.i18n.getLocalizedString("i18n.accounts.print");
                    } else {
                        this.view.flxPrint.setVisibility(false);
                    }
                }
            }
            this.view.forceLayout();
            this.initializeResponsiveViews();
        },

        init: function() {
            FormControllerUtility.setRequestUrlConfig(this.view.brwBodyTnC);
        },

        /**
         * This method is used to set the inital actions for pay a person.
         */
        initActions: function() {
            var scopeObj = this;
            if (kony.application.getCurrentBreakpoint() <= 1024 || orientationHandler.isTablet) {
                this.view.flxRightWrapper.setVisibility(false);
            } else {
                this.view.flxRightWrapper.setVisibility(true);
            }
            this.view.tableView.listbxFrequency.masterData = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.listboxFrequencies();
            this.view.tableView.listbxHowLong.masterData = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.listboxForHowLong();
            this.view.tableView.btnManageRecepient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient");
            this.view.tableView.btnSendRequest.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSendMoneyTabView(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController);
                scopeObj.view.tableView.flxSearch1.setVisibility(false);
            }
            this.view.tableView.btnMyRequests.onClick = function() {
                //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.fetchRequests(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController);
                scopeObj.view.tableView.flxSearch1.setVisibility(true);
            }
            this.view.tableView.btnSent.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSentTransactionsView();
                scopeObj.view.tableView.flxSearch1.setVisibility(false);
            }
            this.view.tableView.btnRecieved.onClick = function() {
                // kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showReceivedSegment();
                scopeObj.view.tableView.flxSearch1.setVisibility(true);
            };
            this.view.tableView.btnManageRecepient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showManageRecipientsView();
            };
            this.view.flxAddRecipientButton.btnSendMoneyNewRecipient.onClick = this.showSendMoneyToNewRecipient;
            this.view.flxWhatElse.onClick = function() {
                scopeObj.showSecondaryActions();
            };
            this.view.flxWhatElse.onTouchStart = function() {
                if (scopeObj.view.secondaryActions.isVisible) {
                    scopeObj.view.secondaryActions.origin = true;
                    if (kony.application.getCurrentBreakpoint() == 640 || kony.application.getCurrentBreakpoint() == 1024) {
                        scopeObj.view.imgDropdown.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                        scopeObj.view.secondaryActions.isVisible = false;
                        scopeObj.AdjustScreen();
                    }
                }
            }
            this.view.imgDropdown.onTouchEnd = function() {
                if (scopeObj.view.secondaryActions.isVisible) {
                    scopeObj.view.secondaryActions.origin = true;
                    if (kony.application.getCurrentBreakpoint() == 640 || kony.application.getCurrentBreakpoint() == 1024) {
                        scopeObj.view.imgDropdown.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                        scopeObj.view.secondaryActions.isVisible = false;
                        scopeObj.AdjustScreen();
                    }
                }
            }
            if (applicationManager.getConfigurationManager().canSearchP2PPersons === "true") {
                this.view.tableView.flxSearch.setVisibility(true);
                this.view.tableView.Search.btnConfirm.onClick = this.onSearchBtnClick.bind(this);
                this.view.tableView.flxSearch.onClick = this.toggleSearchBox.bind(this);
                this.view.tableView.Search.txtSearch.onKeyUp = this.onTxtSearchKeyUp.bind(this);
                this.view.tableView.Search.flxClearBtn.onClick = this.onSearchClearBtnClick.bind(this);
                this.view.tableView.Search.txtSearch.onDone = this.onP2PSearchDone.bind(this);
            } else {
                this.view.tableView.flxSearch.setVisibility(false);
            }
            if (applicationManager.getConfigurationManager().payApersonOneTimePayment === "true") {
                this.view.flxAddRecipientButton.btnSendMoneyNewRecipient.setVisibility(true);
                this.view.flxAddRecipientButton.btnSendMoneyNewRecipient.onClick = this.showSendMoneyToNewRecipient;
            } else {
                this.view.flxAddRecipientButton.btnSendMoneyNewRecipient.setVisibility(false);
            }
            scopeObj.initializeWhatElseFlex();
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            }
        },
        /**
         * This method is used as an onclick handler for send money tab.
         */
        onSentOrRequestSortClickHandler: function(event, data) {
            var scopeObj = this;
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSendMoneyTabView(data);
            //scopeObj.setSearchFlexVisibility(false);
        },
        /**
         * This method is used as an onclick handler for manage recipients tab.
         */
        onMangeRecipientSortClickHandler: function(event, data) {
            var scopeObj = this;
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showManageRecipientsView(data);
            //scopeObj.setSearchFlexVisibility(false);
        },
        /**
         * This method is sued as an onclick handler for sent transactions tab.
         */
        onSentSortClickHandler: function(event, data) {
            var scopeObj = this;
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSentTransactionsView(data);
            //scopeObj.setSearchFlexVisibility(false);
        },
        /**
         * This method is used to show the send money to new recipient in pay a person.
         */
        showSendMoneyToNewRecipient: function() {
            var self = this;
            this.showAddRecipient();
            this.view.lblHeader.text = kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyToNewRecipient");
            this.view.AddRecipient.btnAdd.text = kony.i18n.getLocalizedString("i18n.Pay.SendMoney");
            this.view.AddRecipient.btnAdd.toolTip = kony.i18n.getLocalizedString("i18n.Pay.SendMoney");
            this.view.AddRecipient.btnCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.AddRecipient.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.AddRecipient.btnAdd.onClick = function() {
                var payPerson = {};
                payPerson["context"] = "sendMoneyToNewRecipient";
                self.pickRecipientValuesForSendMoney(payPerson);
            };
            this.view.AddRecipient.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController);
            };
            this.view.customheader.customhamburger.activateMenu("Pay A Person", "Send Money to New Recipient");
            this.AdjustScreen();
        },
        /**
         * This method is used to pick the recipient values to pass to the send money screen in pay a person.
         * @param {Object} payPersonJSON - contains the context "sendMoneyToNewRecipient".
         */
        pickRecipientValuesForSendMoney: function(payPersonJSON) {
            var self = this;
            var validationUtilityManager = applicationManager.getValidationUtilManager();
            var payeeName = this.view.AddRecipient.tbxRecipientValue.text.trim();
            var nickName = this.view.AddRecipient.tbxNickNameValue.text.trim();
            payPersonJSON["name"] = CommonUtilities.changedataCase(payeeName);
            payPersonJSON["nickName"] = CommonUtilities.changedataCase(nickName);
            if (validationUtilityManager.isValidEmail(this.view.AddRecipient.tbxEmailValue1.text.trim()) && this.view.AddRecipient.tbxEmailValue1.text.trim() !== "") {
                payPersonJSON["email"] = this.view.AddRecipient.tbxEmailValue1.text.trim();
            }
            if (validationUtilityManager.isValidEmail(this.view.AddRecipient.tbxEmailValue2.text.trim()) && this.view.AddRecipient.tbxEmailValue2.text.trim() !== "") {
                payPersonJSON["secondaryEmail"] = this.view.AddRecipient.tbxEmailValue2.text.trim();
            }
            if (validationUtilityManager.isValidPhoneNumber(this.view.AddRecipient.tbxPhoneValue1.text.trim()) && this.view.AddRecipient.tbxPhoneValue1.text.trim() !== "") {
                payPersonJSON["phone"] = this.view.AddRecipient.tbxPhoneValue1.text.trim();
            }
            if (validationUtilityManager.isValidPhoneNumber(this.view.AddRecipient.tbxPhoneValue2.text.trim()) && this.view.AddRecipient.tbxPhoneValue2.text.trim() !== "") {
                payPersonJSON["secondaryPhoneNumber"] = this.view.AddRecipient.tbxPhoneValue2.text.trim();
            }
            if (!validationUtilityManager.isValidEmail(payPersonJSON["email"]) && validationUtilityManager.isValidEmail(payPersonJSON["secondaryEmail"])) {
                payPersonJSON["email"] = payPersonJSON["secondaryEmail"];
                payPersonJSON["secondaryEmail"] = undefined;
            }
            if (!validationUtilityManager.isValidPhoneNumber(payPersonJSON["phone"]) && validationUtilityManager.isValidPhoneNumber(payPersonJSON["secondaryPhoneNumber"])) {
                payPersonJSON["phone"] = payPersonJSON["secondaryPhoneNumber"];
                payPersonJSON["secondaryPhoneNumber"] = undefined;
            }
            this.view.flxAddRecipient.setVisibility(false);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('PayAPersonModule').presentationController.onSendMoney(payPersonJSON);
        },
        /**
         * This method acts as the preshow for the pay a person form.
         */
        frmPayAPersonPreShow: function() {
            var self = this;
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "P2P");
            this.view.flxDowntimeWarning.setVisibility(false);
            this.view.tableView.skin = "slFbox";
            this.view.customheader.topmenu.flxMenu.skin = "slFbox";
            this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
            this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
            this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.forceCloseHamburger();
            this.view.p2pReport.isVisible = false;
            this.view.p2pActivity.isVisible = false;
            this.view.Activatep2p.lblNotify.setVisibility(true);
            this.view.btnAddRecipient.toolTip = kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient");
            this.view.btnSendMoneyNewRecipient.toolTip = kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyToNewRecipient");
            this.view.tableView.tbxAmount.onKeyUp = CommonUtilities.validateAmountFieldKeyPress.bind(this, this.view.tableView.tbxAmount);
            this.view.tableView.tbxAmount.onKeyUp = function() {
                self.checkValidityP2PTransferForm();
            };
            this.view.tableView.tbxAmount.onBeginEditing = CommonUtilities.removeDelimitersForAmount.bind(this, this.view.tableView.tbxAmount);
            this.view.tableView.tbxAmount.onEndEditing = CommonUtilities.validateAndFormatAmount.bind(this, this.view.tableView.tbxAmount);
            this.initActions();
            this.view.flxTableView.isVisible = true;
            this.view.tableView.flxCalenderEndingOn.clndrEndingOn.hidePreviousNextMonthDates = true;
            this.view.tableView.clndrSendOn.hidePreviousNextMonthDates = true;

            this.view.forceLayout();

            FormControllerUtility.updateWidgetsHeightInInfo(this, ['customheader', 'flxFooter', 'flxHeader', 'flxContainer', 'Activatep2p.flxAccoFrSending', 'confirmation.flxTotal', 'flxWhatElse','flxFormContent']);
        },
        /**
         * This method is used as an onclick for the search button in the pay a person screen.
         */
        onP2PSearchDone: function() {
            if (this.view.tableView.Search.txtSearch.text.trim() !== "") {
                this.onSearchBtnClick();
            }
        },
        /**
         * This method is used to check if the search string is valid or not and then to call the service for search pay a person.
         */
        onSearchBtnClick: function() {
            var scopeObj = this;
            var searchKeyword = scopeObj.view.tableView.Search.txtSearch.text.trim();
            if (searchKeyword.length >= 0 && scopeObj.prevSearchText !== searchKeyword) {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.searchPayAPerson({
                    'searchKeyword': searchKeyword
                });
                scopeObj.searchView = true;
                scopeObj.prevSearchText = '';
            }
        },
        /**
         * This method is used to toggle the search box.
         * @param {Boolean} searchView - contains the boolean to show the search flex or not.
         */
        toggleSearchBox: function(searchView) {
            if (this.view.tableView.Search.isVisible) {
                this.AdjustScreen(-70);
            } else {
                this.AdjustScreen(100);
            }
            this.setSearchFlexVisibility(!this.view.tableView.Search.isVisible);
            if (this.view.tableView.flxTabs.btnSendRequest.skin === ViewConstants.SKINS.TAB_INACTIVE || this.searchView === true) {
                if (searchView === false) {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPayAPerson();
                } else {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showSendMoneyTabView();
                }
            }
            if (!this.view.tableView.Search.isVisible) {
                this.searchView = false;
                this.prevSearchText = '';
            }
            this.view.forceLayout();
        },
        /**
         * Set search flex visibility.
         * @param {boolean} flag - denotes the visibility of the search flex.
         *
         */
        setSearchFlexVisibility: function(flag) {
            if (typeof flag === "boolean") {
                this.view.tableView.Search.txtSearch.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.SearchMessage");
                this.view.tableView.imgSearch.src = flag ? ViewConstants.IMAGES.SELECTED_SEARCH : ViewConstants.IMAGES.SEARCH_BLUE;
                this.view.tableView.Search.setVisibility(flag);
                if (flag === true) {
                    this.view.tableView.Search.txtSearch.text = '';
                    this.view.tableView.Search.txtSearch.setFocus(true);
                    this.disableSearch();
                }
            }
        },
        /**
         * This method is used to disable the search.
         */
        disableSearch: function() {
            //FormControllerUtility.disableButton(this.view.tableView.Search.btnConfirm);
            this.view.tableView.Search.flxClearBtn.setVisibility(false);
        },
        /**
         * This method is used to enable search.
         */
        enableSearch: function() {
            //FormControllerUtility.enableButton(this.view.tableView.Search.btnConfirm);
            this.view.tableView.Search.flxClearBtn.setVisibility(true);
            this.view.forceLayout();
        },
        /**
         * This method acts as the keyup for the search.
         */
        onTxtSearchKeyUp: function() {
            var scopeObj = this;
            var searchKeyword = scopeObj.view.tableView.Search.txtSearch.text.trim();
            if (searchKeyword.length > 0) {
                scopeObj.enableSearch();
            } else {
                scopeObj.disableSearch();
            }
        },
        /**
         * Acts as the on click event for the search clear button in pay a person.
         */
        onSearchClearBtnClick: function() {
            var scopeObj = this;
            scopeObj.view.tableView.Search.txtSearch.text = "";
            if (this.searchView === true) {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showManageRecipientsView(); //edittwo
            }
            this.searchView = false;
            this.view.tableView.Search.flxClearBtn.isVisible = false;
        },
        /**
         * Used to set the active skin to the tab.
         * @param {Object} obj - contains the widget reference.
         */
        setSkinActive: function(obj) {
            obj.skin = ViewConstants.SKINS.TAB_ACTIVE;
        },
        /**
         * Used to set the in-active skin to the tab.
         * @param {Object} obj - contains the widget reference.
         */
        setSkinInActive: function(obj) {
            obj.skin = ViewConstants.SKINS.TAB_INACTIVE;
            obj.hoverSkin = ViewConstants.SKINS.TAB_HOVER;
        },
        responsiveViews: {},
        initializeResponsiveViews: function() {
            this.responsiveViews["flxTableView"] = this.isViewVisible("flxTableView");
            this.responsiveViews["flxActivateP2P"] = this.isViewVisible("flxActivateP2P");
            this.responsiveViews["flxAcknowledgement"] = this.isViewVisible("flxAcknowledgement");
            this.responsiveViews["flxOptionsAndProceed"] = this.isViewVisible("flxOptionsAndProceed");
            this.responsiveViews["flxConfirmation"] = this.isViewVisible("flxConfirmation");
            this.responsiveViews["flxNoRecipient"] = this.isViewVisible("flxNoRecipient");
            this.responsiveViews["flxAddRecipient"] = this.isViewVisible("flxAddRecipient");
            this.responsiveViews["flxRightWrapper"] = this.isViewVisible("flxRightWrapper");
            this.responsiveViews["flxLetsAuthenticateModule"] = this.isViewVisible("flxLetsAuthenticateModule");
            this.responsiveViews["flxOptionToRecieveAccessCode"] = this.isViewVisible("flxOptionToRecieveAccessCode");
            this.responsiveViews["flxSecureAccessCode"] = this.isViewVisible("flxSecureAccessCode");
            this.responsiveViews["flxEnterCVVCode"] = this.isViewVisible("flxEnterCVVCode");
            this.responsiveViews["flxAnswerSecurityQuestions"] = this.isViewVisible("flxAnswerSecurityQuestions");
            this.responsiveViews["flxLogout"] = this.isViewVisible("flxLogout");
            this.responsiveViews["flxQuit"] = this.isViewVisible("flxQuit");
            this.responsiveViews["flxLoading"] = this.isViewVisible("flxLoading");
            this.responsiveViews["p2pReport"] = this.isViewVisible("p2pReport");
            this.responsiveViews["p2pActivity"] = this.isViewVisible("p2pActivity");
        },
        isViewVisible: function(container) {
            if (this.view[container].isVisible) {
                return true;
            } else {
                return false;
            }
        },
        /**
         * onBreakpointChange : Handles ui changes on .
         * @param {integer} width - current browser width
         */
        responsiveFonts: null,
        onBreakpointChange: function(width) {
            kony.print('on breakpoint change');
            var scope = this;
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.CustomPopup1.onBreakpointChangeComponent(scope.view.CustomPopup1, width);
            this.view.CustomPopupCancel.onBreakpointChangeComponent(scope.view.CustomPopupCancel, width);
            orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.customheader.onBreakpointChangeComponent(width);
            this.setupFormOnTouchEnd(width);
            var views;
            var scope = this;
            if (this.responsiveFonts == null)
                this.responsiveFonts = new ResponsiveFonts();
            var mobileTemplates = {
                "flxSendRequest": "flxSendRequestMobile",
                "flxSendRequestSelected": "flxSendRequestSelectedMobile",
                "flxSent": "flxSentMobile",
                "flxSentSelected": "flxSentSelectedMobile",
                "flxManageRecipient": "flxManageRecipientMobile",
                "flxManageRecipientSelected": "flxManageRecipientSelectedMobile",
                "flxSortP2PActivity": "flxP2PActivityMobile",
            };
            var desktopTemplates = {
                "flxSendRequestMobile": "flxSendRequest",
                "flxSendRequestSelectedMobile": "flxSendRequestSelected",
                "flxSentMobile": "flxSent",
                "flxSentSelectedMobile": "flxSentSelected",
                "flxManageRecipientMobile": "flxManageRecipient",
                "flxManageRecipientSelectedMobile": "flxManageRecipientSelected",
                "flxP2PActivityMobile": "flxSortP2PActivity",
            };
            var data;
            if (width === 640 || orientationHandler.isMobile) {
                this.view.tableView.tbxAmount.padding = [5, 0, 0, 0];
                this.view.p2pActivity.flxSort.setVisibility(false);
                this.responsiveFonts.setMobileFonts();
                views = Object.keys(this.responsiveViews);
                views.forEach(function(e) {
                    scope.view[e].isVisible = scope.responsiveViews[e];
                });
                this.view.tableView.flxMain.skin = "slFbox";
                this.view.tableView.flxSendMoneyHeader.skin = "sknflxffffffnoborder";
                this.view.tableView.flxSendMoneyDetails.skin = "sknFlxffffffShadowdddcdc3pxradius";
                this.view.tableView.flxTabs.skin = "sknFlxffffffShadowdddcdc";
                this.view.tableView.flxHeaderReq.skin = "sknflxffffffnoborder";
                this.view.tableView.flxSendReminderDetails.skin = "sknFlxffffffShadowdddcdc3pxradius";
                this.view.tableView.flxRequestMoneyHeader.skin = "sknflxffffffnoborder";
                this.view.tableView.flxRequestMoneyDetails.skin = "sknFlxffffffShadowdddcdc3pxradius";
                this.view.flxRightWrapper.isVisible = false;
                this.view.tableView.Search.skin = "sknFlexF9F9F9";
                this.view.tableView.Search.isVisible = true;
                this.view.tableView.btnSendMoney.left = "2%";
                this.view.tableView.btnSendMoney.width = "96%";
                //this.view.tableView.btnSendMoney.top = "690dp";
                this.view.tableView.btnCancel.left = "2%";
                this.view.tableView.btnCancel.width = "96%";
                //this.view.tableView.btnCancel.top = "750dp";
                this.view.tableView.flxbuttons.height = "160dp";
                this.view.tableView.flxSegmentContainer.top = "0dp";
                this.view.tableView.flxSendMoneyHeader.top = "0dp";
                data = this.view.tableView.segP2P.data;
                if (data == undefined) return;
                data.map(function(e) {
                    if (e.template == undefined) {
                        e[1].map(function(f) {
                            if (mobileTemplates[f.template] == undefined)
                                return;
                            return f.template = mobileTemplates[f.template];
                        });
                        return e;
                    } else {
                        if (mobileTemplates[e.template] == undefined)
                            return;
                        return e.template = mobileTemplates[e.template];
                    }
                });
                this.view.tableView.segP2P.setData(data);
            } else {
                this.view.p2pActivity.flxSort.setVisibility(true);
                this.responsiveFonts.setDesktopFonts();
                views = Object.keys(this.responsiveViews);
                views.forEach(function(e) {
                    scope.view[e].isVisible = scope.responsiveViews[e];
                });
                this.view.customheader.lblHeaderMobile.text = "";
                if (width == 1024 || orientationHandler.isTablet) {
                    this.view.tableView.lblCurrency.left = 40 + "dp";
                    this.view.tableView.tbxAmount.padding = [3, 0, 0, 0];
                    this.view.flxRightWrapper.isVisible = false;
                } else {
                    this.view.flxRightWrapper.isVisible = true;
                }
                if (width > 1024 && orientationHandler.isTablet) {
                    this.view.confirmDialog.width = "100%";
                }
                this.view.tableView.Search.skin = "slFbox";
                this.view.tableView.flxMain.skin = "sknFlxffffffShadowdddcdc";
                this.view.tableView.flxTabs.skin = "slFbox";
                this.view.tableView.btnSendMoney.top = "585dp";
                this.view.tableView.btnCancel.top = "585dp";
                this.view.tableView.btnSendMoney.width = "32.36%";
                this.view.tableView.btnCancel.left = "64.85%";
                this.view.tableView.btnSendMoney.width = "32.36%";
                this.view.tableView.btnCancel.left = "29.34%";
                this.view.tableView.flxSendMoneyDetails.skin = "sknflxffffffnoborder";
                this.view.tableView.flxSendReminderDetails.skin = "sknflxffffffnoborder";
                this.view.tableView.flxRequestMoneyDetails.skin = "sknflxffffffnoborder";
                //  this.view.tableView.Search.isVisible = false;
                data = this.view.tableView.segP2P.data;
                if (data == undefined) return;
                data.map(function(e) {
                    if (e.template == undefined) {
                        e[1].map(function(f) {
                            if (desktopTemplates[f.template] == undefined)
                                return;
                            return f.template = desktopTemplates[f.template];
                        });
                        return e;
                    } else {
                        if (desktopTemplates[e.template] == undefined)
                            return;
                        return e.template = desktopTemplates[e.template];
                    }
                });
                this.view.tableView.segP2P.setData(data);
            }
            //             if (width == 1024) {
            //                 this.view.flxRightWrapper.isVisible = false;
            //             }
            this.AdjustScreen();
        },
        setupFormOnTouchEnd: function(width) {
            if (width == 640 || orientationHandler.isMobile) {
                this.view.onTouchEnd = function() {}
                this.nullifyPopupOnTouchStart();
            } else {
                if (width == 1024 || orientationHandler.isTablet) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else {
                    this.view.onTouchEnd = function() {
                        hidePopups();
                    }
                }
                var userAgent = kony.os.deviceInfo().userAgent;
                if (userAgent.indexOf("iPad") != -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                }
            }
        },
        nullifyPopupOnTouchStart: function() {},
        /**
         * onClick action of print button
         * @returns {void} - None
         */
        onClickPrint: function() {
            var scopeObj = this;
            var subData = [],
                tableList = [];
            subData.push({
                key: kony.i18n.getLocalizedString("i18n.common.status"),
                value: this.view.acknowledgment.lblTransactionMessage.text
            });
            if (this.view.acknowledgment.lblRefrenceNumber.isVisible) {
                subData.push({
                    key: this.view.acknowledgment.lblRefrenceNumber.text,
                    value: this.view.acknowledgment.lblRefrenceNumberValue.text
                });
                subData.push({
                    key: this.view.acknowledgment.lblAccType.text,
                    value: this.view.acknowledgment.lblBalance.text
                });
            }
            subData.push({
                key: this.view.confirmDialog.lblKey.text,
                value: this.view.confirmDialog.lblValue.text
            });
            subData.push({
                key: this.view.confirmDialog.lblKeyTo.text,
                value: this.view.confirmDialog.lblValueTo.text
            });
            subData.push({
                key: this.view.confirmDialog.lblKeyAmount.text,
                value: this.view.confirmDialog.RichText0bfcfe81705e240.text.replace('<br>', '\n')
            });
            subData.push({
                key: this.view.confirmDialog.lblKeyDate.text,
                value: this.view.confirmDialog.CopyRichText0j287807d04b44c.text.replace('<br>', '\n')
            });
            subData.push({
                key: this.view.confirmDialog.lblKeyDescription.text,
                value: this.view.confirmDialog.lblValueDescription.text
            });
            if (this.view.confirmDialog.flxContainerSix.isVisible) {
                subData.push({
                    key: this.view.confirmDialog.CopylblKeyDescription0g25d166a3c8342.text,
                    value: this.view.confirmDialog.CopylblValueDescription0e7e996a47e5744.text
                });
            }
            if (this.view.confirmDialog.flxContainerSeven.isVisible) {
                subData.push({
                    key: this.view.confirmDialog.CopylblKeyDescription0i0dcc6c4c5134e.text,
                    value: this.view.confirmDialog.CopylblValueDescription0b68a3d2d9c5942.text
                });
            }
            if (this.view.confirmDialog.flxContainerEight.isVisible) {
                subData.push({
                    key: this.view.confirmDialog.CopylblKeyDescription0a12fa9a6e7084d.text,
                    value: this.view.confirmDialog.CopylblValueDescription0j4d08d9160fb4d.text
                });
            }
            tableList.push({
                tableHeader: this.view.confirmDialog.confirmHeaders.lblHeading.text,
                tableRows: subData
            });
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule").presentationController.showPrintPage({
                printKeyValueGroupModel: {
                    "moduleHeader": scopeObj.view.lblHeader.text,
                    "printCallback": scopeObj.navigateToAcknowledgement,
                    "tableList": tableList
                }
            });
        },
        /**
         * callback function binded to cancel button of print view
         * @returns {void} - None
         */
        navigateToAcknowledgement: function() {
            applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
            applicationManager.getNavigationManager().updateForm({}, "frmPayAPerson");
        },
    }
});