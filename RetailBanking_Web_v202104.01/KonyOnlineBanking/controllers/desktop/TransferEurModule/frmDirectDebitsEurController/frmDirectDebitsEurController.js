define(['FormControllerUtility', 'CommonUtilities', 'ViewConstants', 'OLBConstants'], function(FormControllerUtility, CommonUtilities, ViewConstants, OLBConstants) {
    var responsiveUtils = new ResponsiveUtils();
    var orientationHandler = new OrientationHandler();
    var pageNumber;
    var totalNoOfRecords;
    var recordsPerPage = 10;
    var records = [];
    var searchView;
    var filesToBeDownloaded = [];
    var transactionObject = {};
    return {
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferEurModule").presentationController;
            this.initActions();
        },
        onBreakpointChange: function(form, width) {
            var scope = this;
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.DeletePopup.onBreakpointChangeComponent(scope.view.DeletePopup, width);
            FormControllerUtility.setupFormOnTouchEnd(width);
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },
        onNavigate: function() {
            var scope = this;
            var params = {};
            var tokenParams = kony.sdk.getCurrentInstance().tokens.DbxUserLogin.provider_token.params.security_attributes;
            var isCombinedUser = kony.sdk.getCurrentInstance().tokens.DbxUserLogin.provider_token.params.user_attributes.isCombinedUser;
            params.entitlement = {};
            params.isCombinedUser = isCombinedUser;
            params.entitlement.features = JSON.parse(tokenParams.features);
            params.entitlement.permissions = JSON.parse(tokenParams.permissions);
            this.view.tabs.setContext(params);
            var selectedTab = this.view.tabs.tabDefaultSelected;
            this.view.tabs.setSelectedTab(selectedTab);
            var paginationDetails = this.view.pagination.getDefaultOffsetAndLimit();
            this.view.tabs.onError = this.onError;
            this.view.tabs.onTabClick = this.onTabClick;
            this.view.SearchAndFilter.onError = this.onError;
            this.view.SearchAndFilter.onSearchDone = this.onSearchDone;
            this.view.SearchAndFilter.onFilterSelect = this.onFilterSelect;
            this.view.pagination.fetchPaginatedRecords = this.fetchPaginatedRecords;
            this.view.pagination.onError = this.onError;
            this.view.List.updatePaginationBar = this.updatePaginationBar;
            this.view.List.onResetPagination = this.onResetPagination;
            params.tabSelected = selectedTab;
            // params.defaultFilter = "All";
            params.offset = paginationDetails.offset;
            params.limit = paginationDetails.limit;
            this.view.List.showPagination = this.showPagination;
            this.view.List.hidePagination = this.hidePagination;
            this.view.List.showCancelPopup = this.showCancelPopup;
			this.view.List.showSkipPopup = this.showSkipPopup;
            this.view.List.onError = this.onError;
            this.view.List.setFormScope(scope);
            this.view.List.setFormContext(params);
            this.view.List.onButtonAction = this.onButtonAction;
        },
        preShow: function() {
            this.view.customheadernew.activateMenu("EUROTRANSFERS", "Manage Payments");
            FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxHeader', 'flxFooter']);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.info.frame.height - this.view.flxFooter.info.frame.height + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            applicationManager.executeAuthorizationFramework(this);
            this.accessibilityFocusSetup();
        },
        /**
         * Set foucs handlers for skin of parent flex on input focus 
         */
        accessibilityFocusSetup: function() {
            let widgets = [];
            for (let i = 0; i < widgets.length; i++) {
                CommonUtilities.setA11yFoucsHandlers(widgets[i][0], widgets[i][1], this)
            }
        },
        initActions: function() {
            var scopeObj = this;
            this.view.flxNewPayment.onClick = function() {
                scopeObj.presenter.showTransferScreen({
                    context: "MakePayment"
                })
            };
            this.view.flxPaymentActivities.onClick = function() {
                scopeObj.presenter.showTransferScreen({
                    context: ""
                })
            };
            this.view.flxManageBeneficiaries.onClick = function() {
                scopeObj.presenter.showTransferScreen({
                    context: "ManageBeneficiaries"
                })
            };
            this.view.flxCross.onClick = function() {
                scopeObj.view.flxSuccessMessage.setVisibility(false);
            };
			this.view.flxSuccessMessage.setVisibility(false);
            this.view.flxDowntimeWarning.setVisibility(false);
        },
        /**
         * updateFormUI - the entry point method for the form controller.
         * @param {Object} viewModel - it contains the set of view properties and keys.
         */
        updateFormUI: function(viewModel) {
            if (viewModel.isLoading === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewModel.isLoading === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
        },
        /***
         * onError event - Tabs component
         * @params {Object} err 
         ***/
      onError: function(err) {
        FormControllerUtility.hideProgressBar(this.view);
        this.view.flxDowntimeWarning.setVisibility(true);
        this.view.flxSuccessMessage.setVisibility(false);
        this.view.rtxDowntimeWarning.text = err.dbpErrMsg;
      },
        /**
         * onTabClick event - Tabs component
         * @params {String} tabId - Id of the tabs that is clicked
         **/
        onTabClick: function(tabId) {
            kony.print(tabId);
            var scopeObj = this;
            if (tabId === "transfersTab") {
                scopeObj.presenter.showTransferScreen({
                    context: "PastPayments"
                });
            } else if (tabId === "recurringTab") {
                scopeObj.presenter.showTransferScreen({
                    context: "ScheduledPayments"
                });
            } else if (tabId === "directDebitsTab") {
                scopeObj.presenter.showTransferScreen({
                    context: "DirectDebits"
                });
            }
        },
        /**
         * Method to handle onDone event of Search Textbox
         * @param {String} searchKeyword - contains entered text in Search Textbox
         */
        onSearchDone: function(searchKeyword) {
			FormControllerUtility.showProgressBar(this.view);
            this.view.List.onSearch(searchKeyword);
        },
        /**
         * Method to handle onRowClick event of Filter Dropdown
         * @param {String} selectedFilter - contains selected filter info
         */
        onFilterSelect: function(selectedFilter) {
			FormControllerUtility.showProgressBar(this.view);
            this.view.List.onFilter(selectedFilter);
        },
        fetchPaginatedRecords: function(offset, limit) {
            this.view.List.onPagination(offset, limit);
        },
        onResetPagination: function() {
            this.view.pagination.resetStartIndex();
        },
        updatePaginationBar: function(paginatedRecordsLength, totalNoOfRecords) {
            this.view.flxFormContent.setContentOffset({ x: "0%", y: "0%" }, true);
			FormControllerUtility.hideProgressBar(this.view);
            this.view.pagination.updatePaginationBar(paginatedRecordsLength, totalNoOfRecords);
        },
        showPagination: function() {
            this.view.pagination.setVisibility(true);
        },
        hidePagination: function() {
            this.view.pagination.setVisibility(false);
        },
        showCancelPopup: function (response) {
            if (response.dbpErrMsg) {
                this.view.flxDowntimeWarning.setVisibility(true);
                this.view.flxSuccessMessage.setVisibility(false);
                this.view.rtxDowntimeWarning.text = response.dbpErrMsg
            } else {
                this.view.flxSuccessMessage.setVisibility(true);
                this.view.flxDowntimeWarning.setVisibility(false);
                this.view.lblSuccessAcknowledgement.text = kony.i18n.getLocalizedString("i18n.DirectDebits.Cancel")
                this.view.lblRefrenceNumberValue.text = response.orderId;
            }
        },
        showSkipPopup: function (response) {
            if (response.dbpErrMsg) {
                this.view.flxDowntimeWarning.setVisibility(true);
                this.view.flxSuccessMessage.setVisibility(false);
                this.view.rtxDowntimeWarning.text = response.dbpErrMsg;
            } else {
                this.view.flxSuccessMessage.setVisibility(true);
                this.view.flxDowntimeWarning.setVisibility(false);
                this.view.lblSuccessAcknowledgement.text = kony.i18n.getLocalizedString("i18n.DirectDebits.SkipPayment")
                this.view.lblRefrenceNumberValue.text =  response.orderId || response.directDebitId;
            }
        },
        /**
         * Method to handle button onClick event
         * @param {String} buttonId - contains clicked button id
         * @param {Object} data - contains service response data
         */
        onButtonAction: function(buttonId, data) {
            switch (buttonId) {}
        },
    };
});