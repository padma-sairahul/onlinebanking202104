define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnRequest **/
    AS_Button_e3f8eb1bc83944f2bfb9238b8df50615: function AS_Button_e3f8eb1bc83944f2bfb9238b8df50615(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for btnSend **/
    AS_Button_e4c7cafa6aea46569a6a0f92fc625d72: function AS_Button_e4c7cafa6aea46569a6a0f92fc625d72(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_ba061a04371b4e1ea7f3b04c34cf0a61: function AS_FlexContainer_ba061a04371b4e1ea7f3b04c34cf0a61(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});