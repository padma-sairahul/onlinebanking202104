define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEbill **/
    AS_Button_b963395ecd424c5e8028762fa2710476: function AS_Button_b963395ecd424c5e8028762fa2710476(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnActivateEbill **/
    AS_Button_e77b16ed0b3c4bdc8e16829452579a01: function AS_Button_e77b16ed0b3c4bdc8e16829452579a01(eventobject, context) {
        var self = this;
        this.executeOnParent("activateEBill");
    }
});