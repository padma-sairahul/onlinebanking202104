define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmInternationalTransfer **/
    AS_Form_c0ab0b290a274b1eac1d7a3d07cccfcc: function AS_Form_c0ab0b290a274b1eac1d7a3d07cccfcc(eventobject) {
        var self = this;
        return self.postshow.call(this);
    },
    /** preShow defined for frmInternationalTransfer **/
    AS_Form_ea047d0120aa454bbf780d34437bf52f: function AS_Form_ea047d0120aa454bbf780d34437bf52f(eventobject) {
        var self = this;
        this.preshow();
    },
    /** onDeviceBack defined for frmInternationalTransfer **/
    AS_Form_i9d26d4fc14a431c9ef409dc147edf06: function AS_Form_i9d26d4fc14a431c9ef409dc147edf06(eventobject) {
        var self = this;
        kony.print("Back Navigation Disabled");
    }
});