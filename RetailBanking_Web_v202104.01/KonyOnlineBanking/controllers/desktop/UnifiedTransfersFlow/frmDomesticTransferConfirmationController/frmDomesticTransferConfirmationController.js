define(['FormControllerUtility'],function(FormControllerUtility){

  return {
    onNavigate:function(params) { 
      var scope = this;  
      if(kony.application.getCurrentBreakpoint() === "640") {
        this.view.flxHeader.height = "50dp";
      }
      this.view.confirmTransfer.setContext(params,scope);
    },
    preshow : function() {
      var scope = this;
      this.view.confirmTransfer.modifyTransfer = function() { 
        scope.modifyTransfer();
      };
      this.view.confirmTransfer.cancelReviewYes = function() { 
        scope.cancelReviewYes();
      };
      this.view.confirmTransfer.onError = this.onError;
      FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxHeader', 'flxFooter','flxMain','flxLogout']);
    },
    //Event triggered when modify btn triggered in component
    modifyTransfer : function(context) {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmDomesticTransfer",false,context);
    },
    //Event triggered when cance transaction confirm btn triggered in component
    cancelReviewYes : function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },
    //Event triggered when confirm btn triggered in component
    confirmTransfer : function(context) {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmDomesticTransferAcknowledgement",false,context);
    },
    confirmTransferSuccess: function(params) {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmDomesticTransferAcknowledgement",false,params);

    },
    confirmTransferError: function(params) {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmDomesticTransfer",false,params);
    },
    confirmTransferMFA: function(params) {
      var navMan = applicationManager.getNavigationManager();
      if(params.MFAAttributes.MFAType === "SECURE_ACCESS_CODE"){     
        navMan.navigateTo("UnifiedTransfersFlow/frmEmailOrSMS",false,params);
      }
      else if(params.MFAAttributes.MFAType === "SECURITY_QUESTIONS"){
        navMan.navigateTo("UnifiedTransfersFlow/frmSecurityQuestions",false,params);
      }
    },
    onError : function(errObj){
      alert(JSON.stringify(errObj));
    },
    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };
});

