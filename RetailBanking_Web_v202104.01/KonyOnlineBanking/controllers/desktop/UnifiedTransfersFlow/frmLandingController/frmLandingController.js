/* updated test commit */
define(['FormControllerUtility'],function(FormControllerUtility){

  return {
    //Type your controller code here 
     /**
     * preShow
     * @api : preShow    
     * @return : NA
     */
   preShow : function(){
     FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxHeader', 'flxFooter','flxMain','flxLogout','flxFormContent']);
   },
    
    /**
     * onNavigate
     * @api : onNavigate
     * gets trigerred when the preceeding form gets navigated to Landing form
     * @return : NA
     */
    onNavigate : function() {
      var scope = this;
      var params = {};
      var tokenParams = kony.sdk.getCurrentInstance().tokens.DbxUserLogin.provider_token.params.security_attributes;
      params.entitlement = {};
      params.entitlement.features = JSON.parse(tokenParams.features);
      params.entitlement.permissions = JSON.parse(tokenParams.permissions);
      this.view.transferTypeSelection1.setContext(params);
      this.view.transferTypeSelection2.setContext(params);
      this.view.transferTypeSelection3.setContext(params);
      this.view.transferTypeSelection4.setContext(params);
      this.view.transferTypeSelection1.transferTypeDetails = function(trannsferTypeDetails){
        scope.sameBankFlow(trannsferTypeDetails);
      };
      this.view.transferTypeSelection2.transferTypeDetails = function(trannsferTypeDetails){
        scope.DomesticFlow(trannsferTypeDetails);
      };
      this.view.transferTypeSelection3.transferTypeDetails = function(trannsferTypeDetails){
        scope.InternationalFlow(trannsferTypeDetails);
      };
      this.view.transferTypeSelection4.transferTypeDetails = function(trannsferTypeDetails){
        scope.PayaPersonFlow(trannsferTypeDetails);
      };
      this.view.transferTypeSelection1.hideTile = function() {
        scope.view.flxContainer1.setVisibility(false);
      };
      this.view.transferTypeSelection2.hideTile = function() {
        scope.view.flxContainer2.setVisibility(false);
      };
      this.view.transferTypeSelection3.hideTile = function() {
        scope.view.flxContainer3.setVisibility(false);
      };
      this.view.transferTypeSelection4.hideTile = function() {
        scope.view.flxContainer4.setVisibility(false);
      };
    },

    /**
     * sameBankFlow
     * @api : sameBankFlow
     * navigates to input component same bank transfer flow
     * @return : NA
     */
    sameBankFlow: function(param) {
      if(param.clickedButton === "MakeTransfer") {
        var navMan = applicationManager.getNavigationManager();
        navMan.navigateTo("UnifiedTransfersFlow/frmSameBankTransfer",false,param);
      }
      else if(param.clickedButton === "AddNewAccount") {
        param.payeeType = "New Payee";
        var nav=new kony.mvc.Navigation("UnifiedAddBeneficiary/frmSameBankAddBeneficiary");
        nav.navigate(param);
      }
    },

    /**
     * DomesticFlow
     * @api : DomesticFlow
     * navigates to input component domestic transfer flow
     * @return : NA
     */
    DomesticFlow: function(param) {
      if(param.clickedButton === "MakeTransfer") {
        var navMan = applicationManager.getNavigationManager();
        navMan.navigateTo("UnifiedTransfersFlow/frmDomesticTransfer",false,param);
      }
       else if(param.clickedButton === "AddNewAccount") {
        param.payeeType = "New Payee";
        var nav=new kony.mvc.Navigation("UnifiedAddBeneficiary/frmDomesticAddBeneficiary");
        nav.navigate(param);
      }
    },

    /**
     * InternationalFlow
     * @api : InternationalFlow
     * navigates to input component international transfer flow
     * @return : NA
     */
    InternationalFlow: function(param) {
      if(param.clickedButton === "MakeTransfer") {
        var navMan = applicationManager.getNavigationManager();
        navMan.navigateTo("UnifiedTransfersFlow/frmInternationalTransfer",false,param);
      }
      else if(param.clickedButton === "AddNewAccount") {
        param.payeeType = "New Payee";
        var nav=new kony.mvc.Navigation("UnifiedAddBeneficiary/frmInternationalAddBeneficiary");
        nav.navigate(param);
      }
    },

    /**
     * PayaPersonFlow
     * @api : PayaPersonFlow
     * navigates to input component pay a person flow
     * @return : NA
     */
    PayaPersonFlow: function(param) {
      if(param.clickedButton === "MakeTransfer") {
        var navMan = applicationManager.getNavigationManager();
        navMan.navigateTo("UnifiedTransfersFlow/frmP2PTransfer",false,param);
      }
      else if(param.clickedButton === "AddNewAccount") {
        param.payeeType = "New Payee";
        var nav=new kony.mvc.Navigation("UnifiedAddBeneficiary/frmPayaPersonAddBeneficiary");
        nav.navigate(param);
      }
    },
    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };
});


