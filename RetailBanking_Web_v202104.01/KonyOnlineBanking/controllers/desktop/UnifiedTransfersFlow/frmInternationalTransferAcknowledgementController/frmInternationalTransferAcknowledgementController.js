define(['FormControllerUtility'],function(FormControllerUtility){

  return {
    
    onNavigate:function(params) {
      var scope = this;    
      this.view.TransferAcknowledgement.setContext(params,scope);            
    },
     /**
     * preShow
     * @api : preShow    
     * @return : NA
     */
   preShow : function(){
     FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxHeader', 'flxFooter','flxMain','flxLogout']);
   },
    transferActivities: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("frmPastPaymentsNew");
    },

    newTransfer: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },

    savePayee: function(params) {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmSavePayeeforOTT",false,params);
    },
    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };
});

