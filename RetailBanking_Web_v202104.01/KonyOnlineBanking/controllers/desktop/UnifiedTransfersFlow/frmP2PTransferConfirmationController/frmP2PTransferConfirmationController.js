define(['FormControllerUtility'],function(FormControllerUtility){

  return {
    onNavigate:function(params) { 
      var scope = this;      
      this.view.confirmTransfer.setContext(params,scope);

    },
    preshow: function(){
      var scope = this;
      this.view.confirmTransfer.modifyTransfer = function() { 
        scope.modifyTransfer();
      };
      this.view.confirmTransfer.cancelReviewYes = function() { 
        scope.cancelReviewYes();
      };
      this.view.confirmTransfer.onError = this.onError;
      FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxHeader', 'flxFooter','flxMain','flxLogout']);
    },
    //Event triggered when modify btn triggered in component
    modifyTransfer: function(context) {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmP2PTransfer",false,context);
    },
    //Event triggered when cance transaction confirm btn triggered in component
    cancelReviewYes: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },
    //Event triggered when confirm btn triggered in component
    confirmTransferSuccess: function(context) {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmP2PTransferAcknowledgement",false,context);
    },
	confirmTransferError: function(params) {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmP2PTransfer",false,params);
    },
    onError : function(errObj){
      alert(JSON.stringify(errObj));
    },
    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };
});

