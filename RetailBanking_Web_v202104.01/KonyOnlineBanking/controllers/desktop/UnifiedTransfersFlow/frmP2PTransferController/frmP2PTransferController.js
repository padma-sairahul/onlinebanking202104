define(['FormControllerUtility'],function(FormControllerUtility){

  return {
    transferError : false,
    onNavigate : function(param){
      var scope = this;
      if(param.transferFail){
        this.transferError = param.transferFail;
        delete param['transferFail'];
      }
      this.view.unifiedTransfers.setContext(param,scope);
    },
    preshow : function(){
      var scope = this;
      this.view.unifiedTransfers.onError = this.onError;
      this.view.unifiedTransfers.onCancelTransfer = this.onCancelTransfer;
      this.view.flxAddaccountHeader.onTouchStart = this.sameBankTransfer;
      this.view.flxAddKonyAccount.onTouchStart = this.domesticTransfer;
      this.view.flxAddNonKonyAccount.onTouchStart = this.internationalTransfer;
      this.view.flxPersonToPerson.onTouchStart = this.p2pTransfer;
      scope.view.flxClose.onClick = function(){scope.view.flxErrorLayout.isVisible = false;}
      FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxHeader', 'flxFooter','flxMain','flxLogout']);
    },
    postshow : function(){
      if(this.transferError !== false){
        this.confirmTransferError(this.transferError);
      }    
    },
    makeTransfer: function(params) {   
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmP2PTransferConfirmation",false,params);
    },
    confirmTransferError : function(errorMessage){
      this.view.flxErrorLayout.isVisible = true;
      this.view.lblErrorDescription.text = errorMessage;
      this.view.forceLayout();
    },

    sameBankTransfer: function(){
      var selectedTrasferType = { "transferType" : "Same Bank Transfer",
                                 "clickedButton" : "MakeTransfer",
                                 "flowType" : "add",
                                 "payeeType" : "Existing Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmSameBankTransfer",false,selectedTrasferType);
    },

    domesticTransfer: function(){
      var selectedTrasferType = { "transferType" : "Domestic Transfer",
                                 "clickedButton" : "MakeTransfer",
                                 "flowType" : "add",
                                 "payeeType" : "Existing Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmDomesticTransfer",false,selectedTrasferType);
    },

    internationalTransfer: function(){
      var selectedTrasferType = { "transferType" : "International Transfer",
                                 "clickedButton" : "MakeTransfer",
                                 "flowType" : "add",
                                 "payeeType" : "Existing Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmInternationalTransfer",false,selectedTrasferType);
    },

    p2pTransfer: function(){
      var selectedTrasferType = { "transferType" : "Pay a Person",
                                 "clickedButton" : "MakeTransfer",
                                 "flowType" : "add",
                                 "payeeType" : "Existing Payee"
                                };
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmP2PTransfer",false,selectedTrasferType);
    },

    onError : function(errObj){
      alert(JSON.stringify(errObj));
    },
    onCancelTransfer : function(){
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },
    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }

  };
});

