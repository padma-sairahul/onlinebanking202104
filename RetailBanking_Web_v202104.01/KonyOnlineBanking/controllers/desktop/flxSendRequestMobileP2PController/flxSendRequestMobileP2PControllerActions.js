define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnSend **/
    AS_Button_d208bb09e87b448e9df29728cfefca60: function AS_Button_d208bb09e87b448e9df29728cfefca60(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_i654047e21894e71816df1952e446d8d: function AS_FlexContainer_i654047e21894e71816df1952e446d8d(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});