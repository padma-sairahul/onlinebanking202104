define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAddRole **/
    AS_Button_db79ef55ee564a7e98f73df8c9dc6693: function AS_Button_db79ef55ee564a7e98f73df8c9dc6693(eventobject, context) {
        var self = this;
        this.executeOnParent("viewSelectedRole");
    },
    /** onClick defined for flxSelectRole **/
    AS_FlexContainer_e6dd93510126461a99dedae2610730c7: function AS_FlexContainer_e6dd93510126461a99dedae2610730c7(eventobject, context) {
        var self = this;
        this.executeOnParent("SelectedRole");
    }
});