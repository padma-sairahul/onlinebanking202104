define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxResetUserImg **/
    AS_FlexContainer_ab598141057f466fbe88bf62ea6f3e59: function AS_FlexContainer_ab598141057f466fbe88bf62ea6f3e59(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onClick defined for flxUserId **/
    AS_FlexContainer_faae1b36140b4af480b5a4f0b73ed343: function AS_FlexContainer_faae1b36140b4af480b5a4f0b73ed343(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_acb36abda63a455d96cbe21e14a6ccaa: function AS_FlexContainer_acb36abda63a455d96cbe21e14a6ccaa(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_b7ca3d08133143f1b92746a9d0d18e1d: function AS_FlexContainer_b7ca3d08133143f1b92746a9d0d18e1d(eventobject) {
        var self = this;
        //this.closeHamburgerMenu();
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_cc4d8c9e32304406b0b35ff3a5973c01: function AS_Button_cc4d8c9e32304406b0b35ff3a5973c01(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_c7749033cdc2442cb77658d27e7fb201: function AS_Button_c7749033cdc2442cb77658d27e7fb201(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onKeyUp defined for tbxAccountNumberKA **/
    AS_TextField_d5e065dc1db14b209a4e3b9c69c78fa4: function AS_TextField_d5e065dc1db14b209a4e3b9c69c78fa4(eventobject) {
        var self = this;
        this.validateInternalError();
    },
    /** onKeyUp defined for tbxAccountNumberAgainKA **/
    AS_TextField_c76c01aac05c48b9a4074f0f1dbe903f: function AS_TextField_c76c01aac05c48b9a4074f0f1dbe903f(eventobject) {
        var self = this;
        this.validateInternalError();
    },
    /** onKeyUp defined for tbxBeneficiaryNameKA **/
    AS_TextField_d19132ba6a3e4992b51b02e2ec2e59f8: function AS_TextField_d19132ba6a3e4992b51b02e2ec2e59f8(eventobject) {
        var self = this;
        this.validateInternalError();
    },
    /** onKeyUp defined for tbxAccountNickNameKA **/
    AS_TextField_idc8a6a8aeda479bae2101cabefaa4d9: function AS_TextField_idc8a6a8aeda479bae2101cabefaa4d9(eventobject) {
        var self = this;
        this.validateInternalError();
    },
    /** onClick defined for btnCancelKA **/
    AS_Button_i405d01bcaaa4d2c9d0b89970c2b23e3: function AS_Button_i405d01bcaaa4d2c9d0b89970c2b23e3(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for btnAddAccountKA **/
    AS_Button_ced7635af2294e4c8727e74757098961: function AS_Button_ced7635af2294e4c8727e74757098961(eventobject) {
        var self = this;
        this.addInternalAccount();
    },
    /** onClick defined for btnIntCancelKA **/
    AS_Button_ha70ac5384114372b3ade66a9fdbf7a4: function AS_Button_ha70ac5384114372b3ade66a9fdbf7a4(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_aea2297ae7904f4b877c0da521c9fa8c: function AS_Button_aea2297ae7904f4b877c0da521c9fa8c(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_b7d47467c15f43b3a054c9f525424b54: function AS_Button_b7d47467c15f43b3a054c9f525424b54(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_bf34e27f6c5e4d929ffb77ef097a053e: function AS_Button_bf34e27f6c5e4d929ffb77ef097a053e(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_j0508225b4b64d4b98eb8a4e15ce5fd9: function AS_Button_j0508225b4b64d4b98eb8a4e15ce5fd9(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_d9738e344ce8492195c8d1eb591f9161: function AS_Button_d9738e344ce8492195c8d1eb591f9161(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for flxMainContainer **/
    AS_FlexContainer_f59b9b4c894b4e86b127875972812208: function AS_FlexContainer_f59b9b4c894b4e86b127875972812208(eventobject) {
        var self = this;
        //test
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_gcceee1be6754a54a11011eb38141ce2: function AS_Button_gcceee1be6754a54a11011eb38141ce2(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_ba4cedaa943d4de49a91b0e8a4782d3e: function AS_Button_ba4cedaa943d4de49a91b0e8a4782d3e(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_ff509648feb942d586cccf42145d7a0e: function AS_Button_ff509648feb942d586cccf42145d7a0e(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_cb05748fc49d4df7bc6b719f84b23693: function AS_Button_cb05748fc49d4df7bc6b719f84b23693(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_f989e58d86f44e09aae7f6f0d2df9de5: function AS_Button_f989e58d86f44e09aae7f6f0d2df9de5(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** preShow defined for frmAddInternalAccountEur **/
    AS_Form_e8f02d8689d24eeba6a031d7cb748d19: function AS_Form_e8f02d8689d24eeba6a031d7cb748d19(eventobject) {
        var self = this;
        this.preshowFrmAddAccount();
    },
    /** postShow defined for frmAddInternalAccountEur **/
    AS_Form_j4ef8dcebbdc4f92bb7effd44fbf8a3e: function AS_Form_j4ef8dcebbdc4f92bb7effd44fbf8a3e(eventobject) {
        var self = this;
        this.postShowAddInternalAccount();
    },
    /** onDeviceBack defined for frmAddInternalAccountEur **/
    AS_Form_df8a927363674735a8e1be4b30670c7a: function AS_Form_df8a927363674735a8e1be4b30670c7a(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmAddInternalAccountEur **/
    AS_Form_a66bf9349a6f45568ca8b867fb1bdeff: function AS_Form_a66bf9349a6f45568ca8b867fb1bdeff(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});