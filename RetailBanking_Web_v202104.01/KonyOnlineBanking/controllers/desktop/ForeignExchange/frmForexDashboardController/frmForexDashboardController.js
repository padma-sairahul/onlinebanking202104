define(['FormControllerUtility'], function(FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {

        init: function() {
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ForeignExchange").presentationController;
        },

        preShow: function() {
            this.view.customheadernew.activateMenu(kony.i18n.getLocalizedString("i18n.kony.exchangeRates.ExchangeRatesHeader"), "");
            this.setCountryCodeForExchange();
            this.setFeatures();
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader', 'flxFooter', 'flxMain','flxFormContent']);
        },

        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.info.frame.height - this.view.flxFooter.info.frame.height + "dp";
        },

        onBreakpointChange: function(form, width) {
            FormControllerUtility.setupFormOnTouchEnd(width);
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },

        setFeatures: function() {
            this.view.foreignExchange.setFeaturesAndPermissions(applicationManager.getConfigurationManager().getUserFeatures(), applicationManager.getConfigurationManager().getUserPermissions());
        },

        setCountryCodeForExchange: function() {
            var countryCode = "";
            var userAddresses = applicationManager.getUserPreferencesManager().getUserObj().Addresses;
            if (Array.isArray(userAddresses) && userAddresses.length > 0) {
                userAddresses.forEach(function(address) {
                    if (address.isPrimary === "true") {
                        countryCode = address.CountryCode;
                    }
                });
            }
            this.view.foreignExchange.setCountryCode(countryCode);
        }

    };
});