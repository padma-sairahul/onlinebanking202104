define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmForexDashboard **/
    AS_Form_a09971ae49914bceb7ad9545cec39442: function AS_Form_a09971ae49914bceb7ad9545cec39442(eventobject) {
        var self = this;
        self.init();
    },
    /** postShow defined for frmForexDashboard **/
    AS_Form_a6abd548364e49a19ba1cfaf299b7f60: function AS_Form_a6abd548364e49a19ba1cfaf299b7f60(eventobject) {
        var self = this;
        return self.postShow.call(this);
    },
    /** preShow defined for frmForexDashboard **/
    AS_Form_da81c7208cbb471bbd8f7eefe0a1d2df: function AS_Form_da81c7208cbb471bbd8f7eefe0a1d2df(eventobject) {
        var self = this;
        return self.preShow.call(this);
    }
});