define({
    AdjustScreen: function() {
        var currentForm = kony.application.getCurrentForm();
        currentForm.forceLayout();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currentForm.customheader.frame.height + currentForm.flxMain.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currentForm.flxFooter.frame.height;
            if (diff > 0)
                currentForm.flxFooter.top = mainheight + diff + 150 + "dp";
            else
                currentForm.flxFooter.top = mainheight + 150 + "dp";
            currentForm.forceLayout();
        } else {
            currentForm.flxFooter.top = mainheight + 150 + "dp";
            currentForm.forceLayout();
        }
    },
});