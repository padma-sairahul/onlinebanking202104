define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnAddNewNumber **/
    AS_Button_f04d450f44a640cb92c6e2b6616e6651: function AS_Button_f04d450f44a640cb92c6e2b6616e6651(eventobject) {
        var self = this;
        return self.showPhoneNumbers.call(this);
    },
    /** onClick defined for btnEditUsernameProceed **/
    AS_Button_e28f1fdbbd6848608cfc4d5cc5c8cf8e: function AS_Button_e28f1fdbbd6848608cfc4d5cc5c8cf8e(eventobject) {
        var self = this;
        this.checkUsername();
    },
    /** onSelection defined for lbxQuestion1 **/
    AS_ListBox_bcee014d1916494bb40f0211468bef85: function AS_ListBox_bcee014d1916494bb40f0211468bef85(eventobject) {
        var self = this;
        this.setQuestionForListBox1();
    },
    /** onBeginEditing defined for tbxAnswer1 **/
    AS_TextField_e8b918ffd1e346d69ade6a3ba28cf7e2: function AS_TextField_e8b918ffd1e346d69ade6a3ba28cf7e2(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer1();
    },
    /** onEndEditing defined for tbxAnswer1 **/
    AS_TextField_f16827f71b0b4a05a6105875d69ccad8: function AS_TextField_f16827f71b0b4a05a6105875d69ccad8(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer1 **/
    AS_TextField_ebd6c200f7a147fdbadccba7ef34a835: function AS_TextField_ebd6c200f7a147fdbadccba7ef34a835(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onSelection defined for lbxQuestion2 **/
    AS_ListBox_fbbaa4b20678452abc020047435b4175: function AS_ListBox_fbbaa4b20678452abc020047435b4175(eventobject) {
        var self = this;
        this.setQuestionForListBox2();
    },
    /** onBeginEditing defined for tbxAnswer2 **/
    AS_TextField_c91e7fd235ab4c86a976fe91125ab9bc: function AS_TextField_c91e7fd235ab4c86a976fe91125ab9bc(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer2();
    },
    /** onEndEditing defined for tbxAnswer2 **/
    AS_TextField_cdc78aa38b754f3eb226983ccc81f741: function AS_TextField_cdc78aa38b754f3eb226983ccc81f741(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer2 **/
    AS_TextField_bc5789457a334c96b5a4bab1fa9d4356: function AS_TextField_bc5789457a334c96b5a4bab1fa9d4356(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onSelection defined for lbxQuestion3 **/
    AS_ListBox_h9cd9f4d38e74908a6950a38a71840cd: function AS_ListBox_h9cd9f4d38e74908a6950a38a71840cd(eventobject) {
        var self = this;
        this.setQuestionForListBox3();
    },
    /** onBeginEditing defined for tbxAnswer3 **/
    AS_TextField_a4df2f702e2b41469192171af404200e: function AS_TextField_a4df2f702e2b41469192171af404200e(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer3();
    },
    /** onEndEditing defined for tbxAnswer3 **/
    AS_TextField_c334d6fb9d754df8a1793e406187ec74: function AS_TextField_c334d6fb9d754df8a1793e406187ec74(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer3 **/
    AS_TextField_f98f602a3bb04949873ff697ca329ec4: function AS_TextField_f98f602a3bb04949873ff697ca329ec4(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onSelection defined for lbxQuestion4 **/
    AS_ListBox_e5f42253d68741bb895df3034fc5c402: function AS_ListBox_e5f42253d68741bb895df3034fc5c402(eventobject) {
        var self = this;
        this.setQuestionForListBox4();
    },
    /** onBeginEditing defined for tbxAnswer4 **/
    AS_TextField_d0397c608c3c49a3bd5c6162a84fb3ce: function AS_TextField_d0397c608c3c49a3bd5c6162a84fb3ce(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer4();
    },
    /** onEndEditing defined for tbxAnswer4 **/
    AS_TextField_g5c4623ac856423aa1e68ed73f266cc0: function AS_TextField_g5c4623ac856423aa1e68ed73f266cc0(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer4 **/
    AS_TextField_d78df21bab3d47c7ac2d165914a4af02: function AS_TextField_d78df21bab3d47c7ac2d165914a4af02(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onSelection defined for lbxQuestion5 **/
    AS_ListBox_ad146730bc1c4d2fbd9ff019b5323733: function AS_ListBox_ad146730bc1c4d2fbd9ff019b5323733(eventobject) {
        var self = this;
        this.setQuestionForListBox5();
    },
    /** onBeginEditing defined for tbxAnswer5 **/
    AS_TextField_ef0742c517924d3e9b8c8ac9545bd8ec: function AS_TextField_ef0742c517924d3e9b8c8ac9545bd8ec(eventobject, changedtext) {
        var self = this;
        this.onEditingAnswer1();
    },
    /** onEndEditing defined for tbxAnswer5 **/
    AS_TextField_ff82f6f1997843bab08c2f30f8498141: function AS_TextField_ff82f6f1997843bab08c2f30f8498141(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onKeyUp defined for tbxAnswer5 **/
    AS_TextField_jb2317ca4f4048d18ccdf92d14a0411f: function AS_TextField_jb2317ca4f4048d18ccdf92d14a0411f(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.settings.tbxAnswer1.text, this.view.settings.tbxAnswer2.text, this.view.settings.tbxAnswer3.text, this.view.settings.tbxAnswer4.text, this.view.settings.tbxAnswer5.text);
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_cff920382e1e477ab3b92c9f4b59d7fc: function AS_FlexContainer_cff920382e1e477ab3b92c9f4b59d7fc(eventobject) {
        var self = this;
        this.CloseTermsAndConditions();
    },
    /** onClick defined for btnCancel **/
    AS_Button_j11414a39cff46a1a18e242e3d9058a7: function AS_Button_j11414a39cff46a1a18e242e3d9058a7(eventobject) {
        var self = this;
        this.CloseTermsAndConditions();
    },
    /** onClick defined for btnSave **/
    AS_Button_c8ea025c0db24c678d59f914069014d0: function AS_Button_c8ea025c0db24c678d59f914069014d0(eventobject) {
        var self = this;
        this.SaveTermsAndConditions();
    },
    /** init defined for frmProfileManagement **/
    AS_Form_a2e2f9a8fbd64f00a8033cd9cd1c8fd9: function AS_Form_a2e2f9a8fbd64f00a8033cd9cd1c8fd9(eventobject) {
        var self = this;
        this.initProfileSettingsMenu();
    },
    /** postShow defined for frmProfileManagement **/
    AS_Form_b7b4d384166e4c168c5651b714b239fe: function AS_Form_b7b4d384166e4c168c5651b714b239fe(eventobject) {
        var self = this;
        this.postShowProfileManagement();
    },
    /** onDeviceBack defined for frmProfileManagement **/
    AS_Form_fe83062d2bdb4ffb973260d7c19d2fd8: function AS_Form_fe83062d2bdb4ffb973260d7c19d2fd8(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmProfileManagement **/
    AS_Form_d366c36537dd41778b825ab8f10d506f: function AS_Form_d366c36537dd41778b825ab8f10d506f(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});