define({
    segmentManagePayeesRowClick: function() {
        var currForm = kony.application.getCurrentForm();
        var index = currForm.tableView.segmentBillpay.selectedRowIndex[1];
        var data = currForm.tableView.segmentBillpay.data;
        for (i = 0; i < data.length; i++) {
            if (i == index) {
                data[i].imgDropdown = "chevron_up.png";
                if (kony.application.getCurrentBreakpoint() == 640) {
                    data[i].template = "flxBillPayManagePayeesSelectedMobile";
                } else {
                    data[i].template = "flxBillPayManagePayeesSelected";
                }
            } else {
                data[i].imgDropdown = "arrow_down.png";
                if (kony.application.getCurrentBreakpoint() == 640) {
                    data[i].template = "flxBillPayManagePayeesMobile";
                } else {
                    data[i].template = "flxBillPayManagePayees";
                }
            }
        }
        currForm.tableView.segmentBillpay.setData(data);
        this.AdjustScreen(320);
    },
    viewEBill: function() {
        var currForm = kony.application.getCurrentForm();
        var height_to_set = 140 + currForm.flxContainer.info.frame.height;
        currForm.flxViewEbill.height = height_to_set + "dp";
        currForm.flxViewEbill.isVisible = true;
        this.AdjustScreen(220);
        currForm.forceLayout();
    },
    showPayABill: function() {
        var currForm = kony.application.getCurrentForm();
        currForm.payABill.isVisible = true;
        currForm.breadcrumb.setBreadcrumbData([{
            text: "BILL PAY"
        }, {
            text: "PAY A BILL"
        }]);
        currForm.btnConfirm.setVisibility(false);
        currForm.tableView.isVisible = false;
        this.AdjustScreen(220);
    },
    AdjustScreen: function(data) {
        var currentForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currentForm.customheader.info.frame.height + currentForm.flxContainer.info.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currentForm.flxFooter.info.frame.height;
            if (diff > 0)
                currentForm.flxFooter.top = mainheight + diff + data + "dp";
            else
                currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        } else {
            currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        }
    },
});