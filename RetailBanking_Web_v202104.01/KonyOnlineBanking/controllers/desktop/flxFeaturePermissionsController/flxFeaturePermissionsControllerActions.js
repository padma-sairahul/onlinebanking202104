define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxClickAction1 **/
    AS_FlexContainer_a14769bc486742759e0d97473e915b21: function AS_FlexContainer_a14769bc486742759e0d97473e915b21(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction6 **/
    AS_FlexContainer_a7c58e63337145c296f7ef4e4b90e811: function AS_FlexContainer_a7c58e63337145c296f7ef4e4b90e811(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxPermissionClickable **/
    AS_FlexContainer_b1a11ee99b344634bc5ae74b013f715c: function AS_FlexContainer_b1a11ee99b344634bc5ae74b013f715c(eventobject, context) {
        var self = this;
        return self.selectOrUnselectEntireFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction18 **/
    AS_FlexContainer_b6ae1c680d0e4c168ea850ab4f3da201: function AS_FlexContainer_b6ae1c680d0e4c168ea850ab4f3da201(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction14 **/
    AS_FlexContainer_c04e1bf78dad4c149538d8b2dbc5eafa: function AS_FlexContainer_c04e1bf78dad4c149538d8b2dbc5eafa(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction7 **/
    AS_FlexContainer_c969688a2448419c9cbecb043cbea07e: function AS_FlexContainer_c969688a2448419c9cbecb043cbea07e(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction16 **/
    AS_FlexContainer_c9a9542ad9cd44b0b6c6e1922f00e4af: function AS_FlexContainer_c9a9542ad9cd44b0b6c6e1922f00e4af(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction12 **/
    AS_FlexContainer_cc2d473263744d95b6bd428dfbbd81f0: function AS_FlexContainer_cc2d473263744d95b6bd428dfbbd81f0(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction19 **/
    AS_FlexContainer_cf3c2f0c8af34a7a95df1b28ec559d84: function AS_FlexContainer_cf3c2f0c8af34a7a95df1b28ec559d84(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction2 **/
    AS_FlexContainer_d846cf83c8a14a988f9dfb11e6ffbd77: function AS_FlexContainer_d846cf83c8a14a988f9dfb11e6ffbd77(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction13 **/
    AS_FlexContainer_d89550b834a94df0abf7be1772787b87: function AS_FlexContainer_d89550b834a94df0abf7be1772787b87(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction15 **/
    AS_FlexContainer_dc1f7912ecd14e198faf9cf1151f6f77: function AS_FlexContainer_dc1f7912ecd14e198faf9cf1151f6f77(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction3 **/
    AS_FlexContainer_f1294b7a2c7643f6a4c2bdd7c3c9d0a8: function AS_FlexContainer_f1294b7a2c7643f6a4c2bdd7c3c9d0a8(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction11 **/
    AS_FlexContainer_fb736bd407a34e76b1fef864bcdef67e: function AS_FlexContainer_fb736bd407a34e76b1fef864bcdef67e(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction4 **/
    AS_FlexContainer_fbb8a1ff4de5412ca8da1a342c045f6b: function AS_FlexContainer_fbb8a1ff4de5412ca8da1a342c045f6b(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction20 **/
    AS_FlexContainer_fe3a0e77bdc1471786a6b67146a5e818: function AS_FlexContainer_fe3a0e77bdc1471786a6b67146a5e818(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction9 **/
    AS_FlexContainer_ff20fd4bb264421f8325cab2f0c01e3f: function AS_FlexContainer_ff20fd4bb264421f8325cab2f0c01e3f(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction8 **/
    AS_FlexContainer_g965934d76ca44699d0b904fb1226a35: function AS_FlexContainer_g965934d76ca44699d0b904fb1226a35(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction17 **/
    AS_FlexContainer_ga457bbc3b854af5a94d45b2666c832f: function AS_FlexContainer_ga457bbc3b854af5a94d45b2666c832f(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction5 **/
    AS_FlexContainer_h1cbffb29dde4533b6f96cf0a59c98c0: function AS_FlexContainer_h1cbffb29dde4533b6f96cf0a59c98c0(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    },
    /** onClick defined for flxClickAction10 **/
    AS_FlexContainer_jef3431e672b489da88fc0803ea54875: function AS_FlexContainer_jef3431e672b489da88fc0803ea54875(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, eventobject, context);
    }
});