define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxRating2 **/
    AS_FlexContainer_a49cf300cc3f4d0cb4fad85cb727ecc9: function AS_FlexContainer_a49cf300cc3f4d0cb4fad85cb727ecc9(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(2);
    },
    /** onClick defined for flxRating4 **/
    AS_FlexContainer_aa2bc1b5609943ecb0f89c4e960948f1: function AS_FlexContainer_aa2bc1b5609943ecb0f89c4e960948f1(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(4);
    },
    /** onClick defined for flxRating5 **/
    AS_FlexContainer_e3227bc37d364a17a32b0fbfb06a3ec0: function AS_FlexContainer_e3227bc37d364a17a32b0fbfb06a3ec0(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(5);
    },
    /** onClick defined for flxRating1 **/
    AS_FlexContainer_e87c0e7df5b24800bb04c7a98792bf7a: function AS_FlexContainer_e87c0e7df5b24800bb04c7a98792bf7a(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(1);
    },
    /** onClick defined for flxRating3 **/
    AS_FlexContainer_h98a06a4f664424386179d46d7e854f2: function AS_FlexContainer_h98a06a4f664424386179d46d7e854f2(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(3);
    }
});