define({
    toggleCheckBox: function() {
        var index = kony.application.getCurrentForm().segProductsNUO.selectedIndex[1];
        var data = kony.application.getCurrentForm().segProductsNUO.data;
        for (i = 0; i < data.length; i++) {
            if (i == index) {
                kony.print("index:" + index);
                if (data[i].lblCheckBox.text === "D") {
                    data[i].lblCheckBox.text = "C";
                    data[i].lblCheckBox.skin = "sknlblOLBFonts0273E420pxOlbFontIcons";
                } else {
                    data[i].lblCheckBox.text = "D";
                    data[i].lblCheckBox.skin = "sknlblOLBFontsE3E3E320pxOlbFontIcons";
                }
            }
            kony.application.getCurrentForm().segProductsNUO.setData(data);
        }
    },
    KnowMoreAction: function() {
        var currForm = kony.application.getCurrentForm();
        currForm.flxDownTimeWarning.setVisibility(false);
        currForm.flxMainContainer.setVisibility(false);
        currForm.flxAccounts.setVisibility(true);
        currForm.forceLayout();
    },
});