define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnKnowMore **/
    AS_Button_c525cb1880224ca897fec1ada5c33bff: function AS_Button_c525cb1880224ca897fec1ada5c33bff(eventobject, context) {
        var self = this;
        this.KnowMoreAction();
    },
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_d4dce64eb1b54dbd956619e35e67a199: function AS_FlexContainer_d4dce64eb1b54dbd956619e35e67a199(eventobject, context) {
        var self = this;
        this.toggleCheckBox();
    }
});