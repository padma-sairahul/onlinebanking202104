define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxRadioPayDueAmount **/
    AS_FlexContainer_ha6e37f4e61140798013b0929e5a8264: function AS_FlexContainer_ha6e37f4e61140798013b0929e5a8264(eventobject) {
        var self = this;
        this.payDueAmountRadioButton("Due");
    },
    /** onClick defined for flxRadioPayOtherAmount **/
    AS_FlexContainer_i412db0bd6874fb3ae06e24eea023c96: function AS_FlexContainer_i412db0bd6874fb3ae06e24eea023c96(eventobject) {
        var self = this;
        this.payDueAmountRadioButton("Other");
    },
    /** onClick defined for btnConfirm **/
    AS_Button_f5d7c3ed4af04f9285d2926333ea33da: function AS_Button_f5d7c3ed4af04f9285d2926333ea33da(eventobject) {
        var self = this;
        this.loanDataAfterConfirmation();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_f1159265018c413fb008a253c33c686f: function AS_FlexContainer_f1159265018c413fb008a253c33c686f(eventobject) {
        var self = this;
        this.quitPopUpNo();
    },
    /** preShow defined for frmPayDueAmount **/
    AS_Form_g4f57f598ab34e358b6b3294048f52f5: function AS_Form_g4f57f598ab34e358b6b3294048f52f5(eventobject) {
        var self = this;
        this.frmPayDueAmountPreShow();
    },
    /** postShow defined for frmPayDueAmount **/
    AS_Form_cd94e734981b49cdbeb5ab4d34f4f525: function AS_Form_cd94e734981b49cdbeb5ab4d34f4f525(eventobject) {
        var self = this;
        this.postShowPayDueAmount();
    },
    /** onDeviceBack defined for frmPayDueAmount **/
    AS_Form_b0e7160a6b374e60be051b61112ea7d0: function AS_Form_b0e7160a6b374e60be051b61112ea7d0(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmPayDueAmount **/
    AS_Form_ced0930a179e4de8b1c410fe3ae6d697: function AS_Form_ced0930a179e4de8b1c410fe3ae6d697(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});