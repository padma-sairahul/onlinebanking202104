define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnSend **/
    AS_Button_cbd6d66aba7f43aa98f76f5e002708db: function AS_Button_cbd6d66aba7f43aa98f76f5e002708db(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for btnRequest **/
    AS_Button_ff559dbcc49b4fd6982385687406aa7e: function AS_Button_ff559dbcc49b4fd6982385687406aa7e(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_c9ceffbd88af4168a02d6e36c248570f: function AS_FlexContainer_c9ceffbd88af4168a02d6e36c248570f(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});