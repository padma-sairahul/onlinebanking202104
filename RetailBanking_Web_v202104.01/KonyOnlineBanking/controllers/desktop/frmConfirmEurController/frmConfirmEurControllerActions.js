define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxResetUserImg **/
    AS_FlexContainer_fd186d76518e41ac977c7e97d46f9f6f: function AS_FlexContainer_fd186d76518e41ac977c7e97d46f9f6f(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onClick defined for flxUserId **/
    AS_FlexContainer_jcf5b586ddc3439a97ce2fc9a70a6461: function AS_FlexContainer_jcf5b586ddc3439a97ce2fc9a70a6461(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_f5cdb8878dd546dc8a67e1b718285f96: function AS_FlexContainer_f5cdb8878dd546dc8a67e1b718285f96(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_b465b5ed256445328cfb86284a6b2498: function AS_FlexContainer_b465b5ed256445328cfb86284a6b2498(eventobject) {
        var self = this;
        //this.closeHamburgerMenu();
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_h889c841ee924bdc9f9ed9ffae9f9ece: function AS_Button_h889c841ee924bdc9f9ed9ffae9f9ece(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_be6e14e20bd54286b179b8558ab94b23: function AS_Button_be6e14e20bd54286b179b8558ab94b23(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_ge3c1cfaae634c2fa11a45b6f39e6c63: function AS_Button_ge3c1cfaae634c2fa11a45b6f39e6c63(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_ff433c159ac543659817af024de725c7: function AS_Button_ff433c159ac543659817af024de725c7(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_eed3fb4f40214e3b9e17c70c60b9084c: function AS_Button_eed3fb4f40214e3b9e17c70c60b9084c(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_ab9ae68743434f7a843c220f47c2b61f: function AS_Button_ab9ae68743434f7a843c220f47c2b61f(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_f23f91c71b5341b48d5209e8ea2207b8: function AS_Button_f23f91c71b5341b48d5209e8ea2207b8(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_i4965569143c4824855bfbd047608d56: function AS_Button_i4965569143c4824855bfbd047608d56(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_b6e11e0b57f84cd9a25a682910fe1f26: function AS_Button_b6e11e0b57f84cd9a25a682910fe1f26(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_ef95b832a9fd440a85e192fa49d7a00c: function AS_Button_ef95b832a9fd440a85e192fa49d7a00c(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_a4b8beb4e67c41ec82ba938f6bcf67c7: function AS_Button_a4b8beb4e67c41ec82ba938f6bcf67c7(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_bf486db1705d4382896edde9de91ef66: function AS_Button_bf486db1705d4382896edde9de91ef66(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** init defined for frmConfirmEur **/
    AS_Form_bf0773ccab5647dfa84bcc09220917d7: function AS_Form_bf0773ccab5647dfa84bcc09220917d7(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmConfirmEur **/
    AS_Form_j16e4f59294548d4a8867b498d62e55c: function AS_Form_j16e4f59294548d4a8867b498d62e55c(eventobject) {
        var self = this;
        this.initActions();
    },
    /** postShow defined for frmConfirmEur **/
    AS_Form_daa19b36be61468486da2a2df674a489: function AS_Form_daa19b36be61468486da2a2df674a489(eventobject) {
        var self = this;
        this.postShowFrmConfirm();
    },
    /** onDeviceBack defined for frmConfirmEur **/
    AS_Form_dbc4fcfc700e4cbaa4389703869cc02f: function AS_Form_dbc4fcfc700e4cbaa4389703869cc02f(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmConfirmEur **/
    AS_Form_ha62cff7265c490aa5d93753d235b319: function AS_Form_ha62cff7265c490aa5d93753d235b319(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onClick defined for flxInfo **/
    AS_FlexContainer_b6236045de154d17bb1b82f8aa51ec0c: function AS_FlexContainer_b6236045de154d17bb1b82f8aa51ec0c(eventobject, context) {
        var self = this;
    }
});