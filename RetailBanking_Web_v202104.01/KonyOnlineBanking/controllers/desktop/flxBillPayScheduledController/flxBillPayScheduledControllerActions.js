define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEdit **/
    AS_Button_abe9618c0a9142fb9f45b3d9162fe341: function AS_Button_abe9618c0a9142fb9f45b3d9162fe341(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_cd9db55751e345759c337457bcb80a01: function AS_FlexContainer_cd9db55751e345759c337457bcb80a01(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    }
});