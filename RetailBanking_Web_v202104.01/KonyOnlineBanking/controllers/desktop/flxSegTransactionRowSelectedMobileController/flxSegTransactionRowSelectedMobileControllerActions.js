define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnRepeat **/
    AS_Button_f2bb536747c04093831a6d5844d00bad: function AS_Button_f2bb536747c04093831a6d5844d00bad(eventobject, context) {
        var self = this;
        this.showEditRule();
    },
    /** onClick defined for btnPrint **/
    AS_Button_g89a4ed5581347018aed99110d3dee60: function AS_Button_g89a4ed5581347018aed99110d3dee60(eventobject, context) {
        var self = this;
        kony.print("test");
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_ab5949667f464e659ba9b8ae552e273c: function AS_FlexContainer_ab5949667f464e659ba9b8ae552e273c(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for flxRememberCategory **/
    AS_FlexContainer_d82ea47c2ef14de9aa314a6d92c5c949: function AS_FlexContainer_d82ea47c2ef14de9aa314a6d92c5c949(eventobject, context) {
        var self = this;
        this.rememberCategory();
    }
});