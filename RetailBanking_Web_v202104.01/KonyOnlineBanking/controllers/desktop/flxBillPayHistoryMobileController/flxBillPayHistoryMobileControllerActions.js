define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_d640ee7a734d4dcdac5b0e16b3f92d39: function AS_FlexContainer_d640ee7a734d4dcdac5b0e16b3f92d39(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    },
    /** onClick defined for btnRepeat **/
    AS_Button_b1552ee70639452e9a81c5b87cbd7638: function AS_Button_b1552ee70639452e9a81c5b87cbd7638(eventobject, context) {
        var self = this;
        var currForm = kony.application.getCurrentForm();
        _kony.mvc.GetController(currForm.id, true).presenter.onloadPayABill()
    }
});