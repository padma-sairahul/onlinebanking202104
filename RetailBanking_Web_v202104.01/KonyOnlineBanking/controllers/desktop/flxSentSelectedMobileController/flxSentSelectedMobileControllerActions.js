define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnModify **/
    AS_Button_a74bdcdfd2474c64bd4b7a5cd4f2c4ee: function AS_Button_a74bdcdfd2474c64bd4b7a5cd4f2c4ee(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_d2fd18c8d2ba4c75944a3cfcd5a2b73a: function AS_FlexContainer_d2fd18c8d2ba4c75944a3cfcd5a2b73a(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});