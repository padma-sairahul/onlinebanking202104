define({
    showSelectedRow: function() {
        var previousIndex;
        var index = kony.application.getCurrentForm().WireTransferContainer.segWireTransfers.selectedRowIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().WireTransferContainer.segWireTransfers.data;
        for (i = 0; i < data.length; i++) {
            if (i == rowIndex) {
                data[i].imgDropdown = "arrow_up.png";
                data[i].template = "flxManageRecipientsSelected1";
            } else {
                data[i].imgDropdown = "arrow_down.png";
                data[i].template = "flxManageRecipientsUnselected1";
            }
        }
        kony.application.getCurrentForm().WireTransferContainer.segWireTransfers.setData(data);
        kony.application.getCurrentForm().forceLayout();
        this.AdjustScreen();
    },
    //UI Code
    AdjustScreen: function() {
        this.view.forceLayout();
        var currForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currForm.customheader.frame.height + currForm.flxMain.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currForm.flxFooter.frame.height;
            if (diff > 0)
                currForm.flxFooter.top = mainheight + diff + 40 + "dp";
            else
                currForm.flxFooter.top = mainheight + 40 + "dp";
        } else {
            currForm.flxFooter.top = mainheight + 40 + "dp";
        }
        currForm.forceLayout();
    },
});