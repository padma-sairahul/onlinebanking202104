/************************************************************************************************/
define(["CommonUtilities", "FormControllerUtility", "OLBConstants", "ViewConstants", "CampaignUtility"], function(CommonUtilities, FormControllerUtility, OLBConstants, ViewConstants, CampaignUtility) {
    var offset = OLBConstants.DEFAULT_OFFSET; //pagination offset.
    var responsiveUtils = new ResponsiveUtils();
    return {
        profileAccess: "",
        init: function() {
            var self = this;
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            var scopeObj = this;
            this.view.flxAddaccountHeader.onClick = function() {
                self.addInternalAccount();
            };
            this.view.flxAddKonyAccount.onClick = function() {
                self.addExternalAccount();
            };
            this.view.flxAddNonKonyAccount.onClick = function() {
                self.addInternationalAccount();
            };
            this.view.flxAddInternationalAccount.onClick = function() {
                self.addP2PAccount();
            };
            this.fastTransferPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule").presentationController;
            this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.FastTransfer.ScheduledTransfers");
            this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.FastTransfer.ScheduledTransfers");
            this.view.btnRecent.text = kony.i18n.getLocalizedString("i18n.FastTransfer.PastTransfers");
            this.view.btnRecent.toolTip = kony.i18n.getLocalizedString("i18n.FastTransfer.PastTransfers");
            this.view.lblTransfers.text = kony.i18n.getLocalizedString("i18n.FastTransfer.TransferActivities");
            this.view.CustomPopupCancel.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
            this.pastTransactionsSortMap = [{
                    name: "transactionDate",
                    imageFlx: this.view.imgSortDate,
                    clickContainer: this.view.flxSortDate
                },
                {
                    name: "toaccountname",
                    imageFlx: this.view.imgSortDescription,
                    clickContainer: this.view.flxSortDescription
                },
                {
                    name: "amount",
                    imageFlx: this.view.imgSortType,
                    clickContainer: this.view.flxSortAmount
                }
            ];
            this.scheduledTransactionsSortMap = [{
                    name: "scheduledDate",
                    imageFlx: this.view.imgSortDate,
                    clickContainer: this.view.flxSortDate
                },
                {
                    name: "toaccountname",
                    imageFlx: this.view.imgSortDescription,
                    clickContainer: this.view.flxSortDescription
                },
                {
                    name: "amount",
                    imageFlx: this.view.imgSortType,
                    clickContainer: this.view.flxSortAmount
                }
            ];
            this.resetFormUI();
            this.view.btnMakeTransfer.hoverSkin = "sknBtn72727215pxSSPBgf8f7f8";
            this.view.btnMakeTransfer.onClick = function() {
                scopeObj.getScheduledTransactions();
            };
            this.view.flxSearch.setVisibility(false);
            this.view.btnRecent.onClick = function() {
                scopeObj.getPastTransactions();
            };
            this.view.CustomPopupCancel.btnNo.onClick = this.view.CustomPopupCancel.flxCross.onClick = function() {
                scopeObj.view.flxCancelPopup.setVisibility(false);
                scopeObj.view.flxDialogs.setVisibility(false);
            }
            this.view.lblScheduleAPayment.onTouchEnd = function() {
                applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen();
            }
        },
        preShow: function() {
            this.profileAccess = applicationManager.getUserPreferencesManager().profileAccess;
            CampaignUtility.fetchPopupCampaigns();
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader', 'flxFooter', 'flxMain']);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.info.frame.height - this.view.flxFooter.info.frame.height + "dp";
            this.view.customheadernew.forceCloseHamburger();
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer history");
            this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.FastTransfer.ScheduledTransfers");
            this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.FastTransfer.ScheduledTransfers");
            this.view.btnRecent.text = kony.i18n.getLocalizedString("i18n.FastTransfer.PastTransfers");
            this.view.btnRecent.toolTip = kony.i18n.getLocalizedString("i18n.FastTransfer.PastTransfers");
            applicationManager.getNavigationManager().applyUpdates(this);
            applicationManager.executeAuthorizationFramework(this);
        },
        showInternalAccFlx: function() {
            this.view.flxAddaccountHeader.setVisibility(true);
        },
        hideInternalAccFlx: function() {
            this.view.flxAddaccountHeader.setVisibility(false);
        },
        showExternalAccFlx: function() {
            this.view.flxAddKonyAccount.setVisibility(true);
        },
        hideExternalAccFlx: function() {
            this.view.flxAddKonyAccount.setVisibility(false);
        },
        showInternationalAccFlx: function() {
            this.view.flxAddNonKonyAccount.setVisibility(true);
        },
        hideInternationalAccFlx: function() {
            this.view.flxAddNonKonyAccount.setVisibility(false);
        },
        showP2PAccFlx: function() {
            this.view.flxAddInternationalAccount.setVisibility(true);
        },
        hideP2PAccFlx: function() {
            this.view.flxAddInternationalAccount.setVisibility(false);
            this.view.flxSeperator3.setVisibility(false);
        },
        onBreakpointChange: function(form, width) {
            var scope = this
            FormControllerUtility.setupFormOnTouchEnd(width);
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent();
            this.view.customfooternew.onBreakpointChangeComponent();
            this.view.CustomPopupCancel.onBreakpointChangeComponent(scope.view.CustomPopupCancel, width);
            this.view.deletePopup.onBreakpointChangeComponent(scope.view.deletePopup, width);
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);

        },
        addInternalAccount: function() {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule").presentationController.showTransferScreen({
                initialView: "addDBXAccount"
            });
        },
        addExternalAccount: function() {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule").presentationController.showTransferScreen({
                initialView: "addExternalAccount"
            });
        },
        addInternationalAccount: function() {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule").presentationController.showTransferScreen({
                initialView: "addInternationalAccount"
            });
        },
        addP2PAccount: function() {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule").presentationController.showTransferScreen({
                initialView: "addRecipient"
            });
        },
        /**
         * Rest form UI
         */
        resetFormUI: function() {
            this.view.flxMakeTransferError.setVisibility(false);
        },
        /** Method to patch update any section of form
         * @param  {object} viewModel Data for updating form sections
         */
        updateFormUI: function(viewModel) {
            if (viewModel.serverError) {
                this.showServerError(viewModel.serverError);
            } else {
                this.showServerError(false);
                if (viewModel.isLoading === true) {
                    FormControllerUtility.showProgressBar(this.view);
                } else if (viewModel.isLoading === false) {
                    FormControllerUtility.hideProgressBar(this.view);
                }
            }
            if (viewModel.scheduledTransactionsData) {
                this.showScheduledTransactions(viewModel.scheduledTransactionsData);
                this.initializeSearch(viewModel.scheduledTransactionsData);
            }
            if (viewModel.pastTransactionsData) {
                this.showPastTransactions(viewModel.pastTransactionsData);
                this.initializeSearch(viewModel.pastTransactionsData);
            }
            if (viewModel.noMoreRecords) {
                this.showNoMoreRecords();
            }
            if (viewModel.campaign) {
                CampaignUtility.showCampaign(viewModel.campaign, this.view, "flxMain");
            }
        },
        /**
         * showNoMoreRecords - Handles zero records scenario in navigation.
         */
        showNoMoreRecords: function() {
            this.view.transfermain.tablePagination.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
            this.view.transfermain.tablePagination.flxPaginationNext.onClick = function() {};
            kony.ui.Alert(kony.i18n.getLocalizedString("i18n.navigation.norecordsfound"));
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to update to view for scheduled transactions
         * @param {object} scheduledTransactionsData  - scheduled transactions data
         */
        showScheduledTransactions: function(scheduledTransactionsData) {
            var scopeObj = this;
            var breakpoint = kony.application.getCurrentBreakpoint();
            if (breakpoint == "1366" || breakpoint == "1380") {
                this.view.lblActions.left = "38px";
            }
            this.view.btnMakeTransfer.skin = ViewConstants.SKINS.PFM_CATEGORIZEDMONTHLYSPENDING_BTNUNCATEGORIXED;
            this.view.btnRecent.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELECTED;
            if (scheduledTransactionsData.transactions && scheduledTransactionsData.transactions.length) {
                var onCancelCreateTransfer = this.getScheduledTransactions || scheduledTransactionsData.onCancel;
                this.bindFastTransfertWidgetMap(true);
                this.showTransactons(
                    scheduledTransactionsData.transactions.map(this.createTransactionSegmentData.bind(this, onCancelCreateTransfer))
                );
                this.sortFlex(kony.i18n.getLocalizedString("i18n.FastTransfer.ScheduledTransfers"));
                this.setScheduledTransactionsPagination(scheduledTransactionsData.transactions, scheduledTransactionsData.config);
                FormControllerUtility.updateSortFlex(this.scheduledTransactionsSortMap, scheduledTransactionsData.config);
            } else {
                this.showNoTransactons();
            }
            this.view.customheadernew.setFocus(true);
            this.view.customheadernew.flxContextualMenu.setVisibility(false);
            this.view.customheadernew.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
            this.view.customheadernew.imgLblTransfers.text = "O";
        },
        bindFastTransfertWidgetMap: function(isScheduled) {
            this.view.segmentTransfers.widgetDataMap = {
                "btnCancelThisOccurence": isScheduled ? "btnScheduledDelete" : "btnReport",
                "btnRepeat": isScheduled ? "btnCancelScheduledOccurrence" : "btnRecentRepeat",
                "btnAction": isScheduled ? "btnScheduledRepeat" : "btnAction",
                "flxActions": "flxActions",
                "flxDropdown": "flxDropdown",
                "lblAmount": "lblAmount",
                "lblDate": "lblDate",
                "lblIcon": "lblIcon",
                "lblIconSendTo": "lblIconSendTo",
                "lblIconFrom": "lblIconFrom",
                "lblFromAccountTitle": "lblFromAccountTitle",
                "lblFromAccountValue": "lblFromAccountValue",
                "lblIdentifier": "lblIdentifier",
                "lblNotesTitle1": "lblNoteTitle",
                "lblNotesValue1": "lblNoteValue",
                "flxNotesTitle": "flxNotesTitle",
                "lblNotesTitle": "lblFrequencyTitle",
                "lblNotesValue": "frequencyType",
                "lblFrequencyTitle": "lblRecurrenceTitle",
                "lblFrequencyValue": isScheduled ? "recurrenceDescription" : "newRecurrenceValue",
                "lblReferenceNumberTitle": "lblReferenceNumberTitle",
                "lblReferenceNumberValue": "lblReferenceNumberValue",
                "lblSendTo": "lblSendTo",
                "lblSeparator": "lblSeparator",
                "lblRecurrenceTitle": "lblStatusTitle",
                "lblRecurrenceValue": "lblStatusValue",
                "lblSeparatorLineActions": "lblSeparatorLineActions",
                "flxRowOne": "flxRowOne",
                "flxIdentifier": "flxIdentifier",
                "flxFastPastTransfersSelected": "flxFastPastTransfersSelected",
                "lblDropdown": "lblDropdown",
                "flxIcon": "flxIcon",
                "flxIconSendTo": "flxIconSendTo",
                "flxIconFrom": "flxIconFrom",
                "flxRow": "flxRow",
                "flxDetail": "flxDetail",
                "flxAmount": "flxAmount",
                "flxDate": "flxDate",
                "flxNoteTitle": "flxNoteTitle",
                "flxReferenceNumberTitle": "flxReferenceNumberTitle",
                "flxRecurrenceTitle": "flxRecurrenceTitle",
                "lblRowSeperator": "lblRowSeperator",
                "flxStatusTitle": "flxStatusTitle",
                "flxFromAccountTitle": "flxFromAccountTitle",
                "lblSeparatorLineAction1": "lblSeparatorLineAction1",
                "lblSeparatorLineAction2": "lblSeparatorLineAction2",
                "flxSentTo": "flxSentTo",
                "lblSeperator2": "lblSeperator2",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper"
            };
        },
        /**
         * Entry Point Method of Scheduled Tab
         */
        getScheduledTransactions: function() {
            this.fastTransferPC.getScheduledUserTransactions();
        },
        /**
         * Configure Paginations for Recent Transfers
         * @param {object} config configuration to show pagination
         */
        setScheduledTransactionsPagination: function(transactions, config) {
            this.setPaginationPreviousScheduled(config);
            this.setPaginationNextScheduled();
            this.view.tablePagination.lblPagination.text =
                (config.offset + 1) +
                "-" +
                (config.offset + transactions.length) +
                " " +
                kony.i18n.getLocalizedString("i18n.common.transactions");
            if (transactions.length < ViewConstants.MAGIC_NUMBERS.LIMIT) {
                this.view.tablePagination.flxPaginationNext.onClick = function() {};
                this.view.tablePagination.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
            }
        },
        /**Configure pagination for previous button for Scheduled transfers
         * @param {object} config pagination values
         */
        setPaginationPreviousScheduled: function(config) {
            var scopeObj = this;
            if (config.offset <= 0) {
                this.view.tablePagination.flxPaginationPrevious.onClick = function() {};
                this.view.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
            } else {
                this.view.tablePagination.flxPaginationPrevious.onClick = function() {
                    scopeObj.getPreviousScheduledTransactions(config);
                };
                this.view.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
            }
        },
        /**
         * Configure Pagination for Next Button Of Scheduled Transfers
         */
        setPaginationNextScheduled: function() {
            var scopeObj = this;
            this.view.tablePagination.flxPaginationNext.onClick = function() {
                scopeObj.getNextScheduledTransactions();
            };
            this.view.tablePagination.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
        },
        /**Called when Pagination is triggered for next Scheduled Tranctions
         */
        getNextScheduledTransactions: function() {
            this.view.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
            this.fastTransferPC.fetchNextScheduledUserTransactions();
        },
        /**Called when previos button is triggered from pagination
         * @returns {object} config configuration values for pagination
         */
        getPreviousScheduledTransactions: function(config) {
            if (config.offset >= ViewConstants.MAGIC_NUMBERS.LIMIT) {
                this.fastTransferPC.fetchPreviousScheduledUserTransactions();
            } else {
                this.view.transfermain.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
            }
        },
        repeatTransaction: function(transaction, onCancelCreateTransfer) {
            applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen({
                transactionObject: transaction,
                onCancelCreateTransfer: onCancelCreateTransfer
            });
        },
        editScheduledTransaction: function(transaction, onCancelCreateTransfer) {
            applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen({
                editTransactionObject: transaction,
                onCancelCreateTransfer: onCancelCreateTransfer
            });
        },
        deleteTransaction: function(transaction) {
            var scopeObj = this;
            this.view.flxCancelPopup.setVisibility(true);
            this.view.CustomPopupCancel.btnYes.text = kony.i18n.getLocalizedString('i18n.common.yes');
            this.view.flxDialogs.setVisibility(true);
            this.view.CustomPopupCancel.flxCross.setFocus(true);
            this.view.CustomPopupCancel.btnYes.onClick = function() {
                scopeObj.view.flxCancelPopup.setVisibility(false);
                scopeObj.view.flxDialogs.setVisibility(false);
                scopeObj.fastTransferPC.cancelTransaction(transaction);
            }
        },
        downloadReport: function(transactionId) {
            var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
            var transactionObject = new transactionsModel({
                "transactionId": transactionId
            });
            this.fastTransferPC.fetchTransactionsReportFastTransfer(transactionObject);
        },
        onCancelOccurrence: function(transaction) {
            var scopeObj = this;
            this.view.flxCancelPopup.setVisibility(true);
            this.view.CustomPopupCancel.btnYes.text = kony.i18n.getLocalizedString('i18n.common.yes');
            this.view.flxDialogs.setVisibility(true);
            this.view.CustomPopupCancel.flxCross.setFocus(true);
            this.view.CustomPopupCancel.btnYes.onClick = function() {
                scopeObj.view.flxCancelPopup.setVisibility(false);
                scopeObj.view.flxDialogs.setVisibility(false);
                scopeObj.fastTransferPC.cancelTransactionOccurrence(transaction);
            }
        },

        isCreateAllowed: function(transaction) {
            var serviceName = transaction.serviceName;
            var fromAccount = transaction.fromAccountNumber;
            var configManager = applicationManager.getConfigurationManager();
            if (serviceName !== null && serviceName !== "" && configManager.checkAccountAction(fromAccount, serviceName)) return true;
            else return false;
        },

        getToAccountText: function(transaction) {
            if (transaction.payPersonName) {
                return transaction.payPersonName;
            }
            return ((transaction.toAccountName ? CommonUtilities.truncateStringWithGivenLength(transaction.toAccountName + "....", 22) : "") + (transaction.toAccountNumber ? CommonUtilities.getLastFourDigit(transaction.toAccountNumber) : ""));
        },


        /**
         * Create View Model For Transactions for a segment
         * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
         * @param  {Array} data Array of transactions model
         */
        createTransactionSegmentData: function(onCancelCreateTransfer, transaction) {
            var scopeObj = this;
            var mobileWidth = "100%";
            var mobilewidth1 = "100%";
            var mobilewidth2 = "100%"
            var mobileLine1Visibility = false;
            var mobileLine2Visibility = false;
            var isCreateAllowed = this.isCreateAllowed(transaction);
            //var isCombined = configurationManager.isCombinedUser == "true" ? true : false;
            var isSingleCustomerProfile = applicationManager.getUserPreferencesManager().isSingleCustomerProfile === false ? true : false;
            var width = kony.application.getCurrentBreakpoint();
            var orientationHandler = new OrientationHandler();
            if (kony.application.getCurrentBreakpoint() == 640) {
                if (transaction.frequencyType !== OLBConstants.TRANSACTION_RECURRENCE.ONCE) {
                    mobileWidth = "33.33%";
                    mobilewidth1 = "25%";
                    mobilewidth2 = "41.66%"

                    mobileLine1Visibility = false;
                    mobileLine2Visibility = false;
                } else {
                    mobileWidth = "50%";
                    mobilewidth1 = "50%";
                    mobilewidth2 = "50%";
                    mobileLine1Visibility = false;
                    mobileLine2Visibility = false;
                }
            }
            var dataObject = {
                flxIcon: {
                    //isVisible: isCombined
                    isVisible: this.profileAccess === "both" ? true : false
                },
                flxIconSendTo: {
                    //isVisible: isCombined
                    isVisible: this.profileAccess === "both" ? true : false
                },
                flxIconFrom: {
                    //isVisible: isCombined
                    isVisible: this.profileAccess === "both" ? true : false
                },
                lblIcon: {
                    //isVisible: isCombined,
                    isVisible: this.profileAccess === "both" ? true : false,
                    text: transaction.isBusinessPayee === "1" ? "r" : "s"
                },
                lblIconSendTo: {
                    //isVisible: isCombined,
                    isVisible: this.profileAccess === "both" ? true : false,
                    text: transaction.isBusinessPayee === "1" ? "r" : "s"
                },
                lblIconFrom: {
                    //isVisible: isCombined,
                    isVisible: this.profileAccess === "both" ? true : false,
                    text: this.displayIcon(transaction.fromAccountNumber)
                },
                lblSeparatorLineAction1: {
                    isVisible: mobileLine1Visibility
                },
                lblSeparatorLineAction2: {
                    isVisible: mobileLine2Visibility
                },
                btnAction: {
                    width: kony.application.getCurrentBreakpoint() == 640 ? "100%" : "110dp",
                    text: kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                    toolTip: kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                    isVisible: isCreateAllowed,
                    onClick: this.repeatTransaction.bind(this, transaction, onCancelCreateTransfer)
                },
                btnScheduledRepeat: {
                    width: kony.application.getCurrentBreakpoint() == 640 ? mobilewidth1 : "110dp",
                    right: kony.application.getCurrentBreakpoint() == 1024 ? "11px" : "4%",
                    text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                    toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                    isVisible: true,
                    onClick: this.editScheduledTransaction.bind(this, transaction, onCancelCreateTransfer)
                },
                btnScheduledDelete: {
                    isVisible: true,
                    width: mobileWidth,
                    text: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer") : kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
                    toolTip: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer") : kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
                    isVisible: true,
                    onClick: CommonUtilities.isCSRMode() ? FormControllerUtility.disableButtonActionForCSRMode() : this.deleteTransaction.bind(this, transaction)
                },
                btnReport: {
                    isVisible: true,
                    width: kony.application.getCurrentBreakpoint() == 640 ? "100%" : "110dp",
                    text: kony.i18n.getLocalizedString("i18n.transfers.downloadReport"),
                    toolTip: kony.i18n.getLocalizedString("i18n.transfers.downloadReport"),
                    onClick: CommonUtilities.isCSRMode() ? FormControllerUtility.disableButtonActionForCSRMode() : this.downloadReport.bind(this, transaction.transactionId)
                },
                btnRecentRepeat: {
                    isVisible: false
                },
                btnCancelScheduledOccurrence: {
                    width: mobilewidth2,
                    isVisible: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false : true,
                    text: kony.i18n.getLocalizedString("i18n.common.cancelOccurrence"),
                    toolTip: kony.i18n.getLocalizedString("i18n.common.cancelOccurrence"),
                    isVisible: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false : true,
                    onClick: CommonUtilities.isCSRMode() ? FormControllerUtility.disableButtonActionForCSRMode() : this.onCancelOccurrence.bind(this, transaction)
                },
                "flxDropdown": "flxDropdown",
                "lblDropdown": ViewConstants.FONT_ICONS.CHEVRON_DOWN,
                "flxIdentifier": {
                    isVisible: false
                },
                "flxFastPastTransfersSelected": {
                    height: "50dp"
                },
                "template": (kony.application.getCurrentBreakpoint() == 640) ? "flxFastPastTransfersMobile" : "flxFastPastTransfersSelected",
                "btnCancel": {
                    "text": "Cancel",
                    "accessibilityconfig": {
                        "a11yLabel": "Cancel",
                    }
                },
                "lblFromAccountTitle": {
                    "text": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
                    }
                },
                "lblRecurrenceTitle": {
                    "text": kony.i18n.getLocalizedString("i18n.transfers.lblRecurrences"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.lblRecurrences"),
                    }
                },
                "lblReferenceNumberTitle": {
                    "text": kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
                    }
                },
                "lblSeparator": {
                    "text": "lblSeparator"
                },
                "lblStatusTitle": {
                    "text": kony.i18n.getLocalizedString("i18n.billPay.Status"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.Status"),
                    }
                },
                "lblNoteTitle": {
                    "text": kony.i18n.getLocalizedString("i18n.transfers.Description"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.Description"),
                    }
                },
                "category": transaction.category,
                "frequencyType": transaction.frequencyType,
                "transactionType": transaction.transactionType,
                "fromAccountNumber": transaction.fromAccountNumber,
                "toAccountNumber": transaction.toAccountNumber,
                "lblAmount": {
                    "text": CommonUtilities.formatCurrencyWithCommas(transaction.amount, false, transaction.transactionCurrency),
                    "accessibilityconfig": {
                        "a11yLabel": CommonUtilities.formatCurrencyWithCommas(transaction.amount, false, transaction.transactionCurrency),
                    }
                },
                "externalAccountNumber": transaction.ExternalAccountNumber,
                "serviceName": transaction.serviceName,
                "lblSendTo": {
                    "text": this.getToAccountText(transaction),
                    "accessibilityconfig": this.getToAccountText(transaction),
                    "left": ((width === 640 || orientationHandler.isMobile) ? "0dp" : (this.profileAccess === "both" ? "10dp" : "43dp"))
                },
                "lblLatestScheduledTransaction": {
                    "text": this.getDateFromDateStr(transaction.transactionDate),
                    "accessibilityconfig": {
                        "a11yLabel": this.getDateFromDateStr(transaction.transactionDate),
                    }
                },
                "newRecurrenceValue": (transaction.recurrenceDesc && transaction.recurrenceDesc !== "None") ? transaction.recurrenceDesc : "-",
                "lblDate": {
                    "text": transaction.isScheduled === "true" || kony.sdk.isNullOrUndefined(transaction.transactionDate) ? this.getDateFromDateStr(transaction.scheduledDate) : this.getDateFromDateStr(transaction.transactionDate),
                    "accessibilityconfig": {
                        "a11yLabel": transaction.isScheduled === "true" || kony.sdk.isNullOrUndefined(transaction.transactionDate) ? this.getDateFromDateStr(transaction.scheduledDate) : this.getDateFromDateStr(transaction.transactionDate),
                    }
                },
                "scheduledDate": this.getDateFromDateStr(transaction.transactionDate),
                "frequencyEndDate": this.getDateFromDateStr(transaction.frequencyEndDate),
                "lblStatusValue": {
                    "text": transaction.statusDescription || "-",
                    "accessibilityconfig": {
                        "a11yLabel": transaction.statusDescription,
                    }
                },
                "lblReferenceNumberValue": {
                    "text": transaction.transactionId,
                    "accessibilityconfig": {
                        "a11yLabel": transaction.transactionId,
                    }
                },
                "lblRecurrenceValue": {
                    "text": transaction.numberOfRecurrences,
                    "accessibilityconfig": {
                        "a11yLabel": transaction.numberOfRecurrences,
                    }
                },
                "recurrenceDescription": (transaction.recurrenceDesc && transaction.recurrenceDesc !== "None") ? transaction.recurrenceDesc : "-",
                "lblFromAccountValue": {
                    "text": CommonUtilities.truncateStringWithGivenLength(transaction.fromAccountName + "....", 22) + CommonUtilities.getLastFourDigit(transaction.fromAccountNumber),
                    "left": ((width === 640 || orientationHandler.isMobile) ? "0dp" : (isSingleCustomerProfile ? "10dp" : "0dp")),
                    "accessibilityconfig": {
                        "a11yLabel": CommonUtilities.getAccountDisplayName({
                            name: transaction.fromAccountName,
                            accountID: transaction.fromAccountNumber,
                            nickName: transaction.fromNickName,
                            Account_id: transaction.fromAccountNumber
                        }),
                    }
                },
                "lblNoteValue": {
                    "text": (transaction.transactionsNotes && transaction.transactionsNotes !== "None") ? transaction.transactionsNotes : "-",
                    "accessibilityconfig": {
                        "a11yLabel": transaction.transactionsNotes,
                    }
                },
                "lblIdentifier": {
                    "text": "lblIdentifier",
                    "accessibilityconfig": {
                        "a11yLabel": "lblIdentifier",
                    }
                },
                "lblSeparatorLineActions": {
                    "text": "lblSeparatorLineActions"
                },
                "flxRowOne": {
                    "text": "flxRowOne",
                    "accessibilityconfig": {
                        "a11yLabel": "flxRowOne",
                    }
                },
                "lblRowSeperator": {
                    "text": "lblRowSeperator",
                    "accessibilityconfig": {
                        "a11yLabel": "lblRowSeperator",
                    }
                },
                "lblSeperator2": {
                    "text": "lblSeperator2",
                    "accessibilityconfig": {
                        "a11yLabel": "lblSeperator2",
                    }
                },
                "lblFrequencyTitle": {
                    "text": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                    }
                },
                "flxSelectedRowWrapper": {
                    "skin": "slFbox"
                }

            };
            if (CommonUtilities.isCSRMode()) {
                dataObject.btnScheduledDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
            }
            return dataObject;
        },

        displayIcon: function(accountNumber) {
            var isBusinessAccount = "false";
            var accounts = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule").presentationController.accounts;
            for (i = 0; i < accounts.length; i++) {
                if (accounts[i].accountID === accountNumber)
                    isBusinessAccount = accounts[i].isBusinessAccount
            }
            return isBusinessAccount === "true" ? "r" : "s";
        },
        /**
         * Get Front End Date String
         * @param {string} dateStr - Date String from backend
         */
        getDateFromDateStr: function(dateStr) {
            if (dateStr) {
                return CommonUtilities.getFrontendDateString(dateStr);
            } else {
                return "";
            }
        },
        /**
         * Method to update to view for scheduled transactions
         * @param {object} scheduledTransactionsData  - scheduled transactions data
         */
        showPastTransactions: function(pastTransactionsData) {
            var scopeObj = this;
            this.view.lblActions.left = "33px";
            this.view.btnMakeTransfer.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELECTED;
            this.view.btnRecent.skin = ViewConstants.SKINS.PFM_CATEGORIZEDMONTHLYSPENDING_BTNUNCATEGORIXED;
            if (pastTransactionsData.pastTransfers && pastTransactionsData.pastTransfers.length) {
                var onCancelCreateTransfer = this.getPastTransactions || pastTransactionsData.onCancel;
                this.bindFastTransfertWidgetMap(false);
                this.showTransactons(
                    pastTransactionsData.pastTransfers.map(this.createTransactionSegmentData.bind(this, onCancelCreateTransfer))
                );
                this.sortFlex(kony.i18n.getLocalizedString("i18n.FastTransfer.PastTransfers"));
                this.setPastTransactionsPagination(pastTransactionsData.pastTransfers, pastTransactionsData.config);
                FormControllerUtility.updateSortFlex(this.pastTransactionsSortMap, pastTransactionsData.config);
            } else {
                this.showNoTransactons();
            }
            this.view.customheadernew.setFocus(true);
        },
        /**
         * Entry Point Method of Past Tab
         */
        getPastTransactions: function() {
            this.fastTransferPC.getPastUserTransactions();
        },
        /**
         * Configure Paginations for Past Transfers
         * @param {object} config configuration to show pagination
         */
        setPastTransactionsPagination: function(transactions, config) {
            this.setPaginationPreviousPast(config);
            this.setPaginationNextPast();
            this.view.tablePagination.lblPagination.text =
                (config.offset + 1) +
                "-" +
                (config.offset + transactions.length) +
                " " +
                kony.i18n.getLocalizedString("i18n.common.transactions");
            if (transactions.length < ViewConstants.MAGIC_NUMBERS.LIMIT) {
                this.view.tablePagination.flxPaginationNext.onClick = function() {};
                this.view.tablePagination.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
            }
        },
        /**Configure pagination for previous button for Past transfers
         * @param {object} config pagination values
         */
        setPaginationPreviousPast: function(config) {
            var scopeObj = this;
            if (config.offset <= 0) {
                this.view.tablePagination.flxPaginationPrevious.onClick = function() {};
                this.view.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
            } else {
                this.view.tablePagination.flxPaginationPrevious.onClick = function() {
                    scopeObj.getPreviousPastTransactions(config);
                };
                this.view.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
            }
        },
        /**
         * Configure Pagination for Next Button Of Past Transfers
         */
        setPaginationNextPast: function() {
            var scopeObj = this;
            this.view.tablePagination.flxPaginationNext.onClick = function() {
                scopeObj.getNextPastTransactions();
            };
            this.view.tablePagination.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
        },
        /**Called when Pagination is triggered for next Past Tranctions
         */
        getNextPastTransactions: function() {
            this.view.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
            this.fastTransferPC.fetchNextPastUserTransactions();
        },
        /**Called when previos button is triggered from pagination
         * @returns {object} config configuration values for pagination
         */
        getPreviousPastTransactions: function(config) {
            if (config.offset >= ViewConstants.MAGIC_NUMBERS.LIMIT) {
                this.fastTransferPC.fetchPreviousPastUserTransactions();
            } else {
                this.view.transfermain.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
            }
        },
        /**
         * Method to display  transactions.
         * @param {transactions []} data  - transactions data
         */
        showTransactons: function(data) {
            var width = kony.application.getCurrentBreakpoint();
            var orientationHandler = new OrientationHandler();
            var isCombined = configurationManager.isCombinedUser === "true" ? true : false;
            this.view.flxNoTransactions.setVisibility(false);
            this.view.segmentTransfers.setData(data);
            this.view.segmentTransfers.setVisibility(true);
            this.view.flxSort.setVisibility(true);
            this.view.tablePagination.setVisibility(true);
            this.view.tablePagination.flxPagination.setVisibility(true);
            if (isCombined) {
                if (!(width === 640 || orientationHandler.isMobile)) {
                    this.view.flxSearch.setVisibility(true);
                } else {
                    this.view.flxSearch.setVisibility(false);
                }
            }
            this.view.forceLayout();
        },
        /**
         * Method to display no transactions ui.
         */
        showNoTransactons: function() {
            this.view.flxNoTransactions.setVisibility(true);
            this.view.segmentTransfers.setVisibility(false);
            this.view.flxSort.setVisibility(false);
            this.view.tablePagination.setVisibility(false);
            this.view.tablePagination.flxPagination.setVisibility(false);
            this.view.flxSearch.setVisibility(true);
            this.view.forceLayout();
        },
        /**Configure Sort Flex
         * @param  {string} tab Type of tab and shows sort flex
         */
        sortFlex: function(tab, config) {
            this.view.lblSortDescription.text = kony.i18n.getLocalizedString("i18n.transfers.lblTo");
            this.view.lblSortType.text = kony.i18n.getLocalizedString("i18n.konybb.Common.Amount");
            if (tab === kony.i18n.getLocalizedString("i18n.FastTransfer.ScheduledTransfers")) {
                this.view.lblSortDate.text = kony.i18n.getLocalizedString("i18n.billpay.scheduledDate");
                FormControllerUtility.setSortingHandlers(
                    this.scheduledTransactionsSortMap,
                    this.onScheduledTransactionsSortClickHandler,
                    this
                );
                CommonUtilities.Sorting.updateSortFlex(
                    this.scheduledTransactionsSortMap,
                    config
                );
            } else {
                this.view.lblSortDate.text = kony.i18n.getLocalizedString("i18n.konybb.Common.Date");
                FormControllerUtility.setSortingHandlers(
                    this.pastTransactionsSortMap,
                    this.onPastTransactionsSortClickHandler,
                    this
                );
                CommonUtilities.Sorting.updateSortFlex(
                    this.pastTransactionsSortMap,
                    config
                );
            }
        },
        /** On Scheduled Transactions Sort click handler.
         * @param  {object} event object
         * @param  {object} data New Sorting Data
         */
        onScheduledTransactionsSortClickHandler: function(event, data) {
            //  this.fastTransferPC.getScheduledTransactions(data);
            this.fastTransferPC.fetchScheduledUserTransactions(data);
        },
        /** On Past Transactions Sort click handler.
         * @param  {object} event object
         * @param  {object} data New Sorting Data
         */
        onPastTransactionsSortClickHandler: function(event, data) {
            // this.fastTransferPC.getPastTransactions(data);
            this.fastTransferPC.fetchPastUserTransactions(data);
        },
        /**
         * Method to show server error
         * @param {Boolean} status true/false
         */
        showServerError: function(status) {
            if (status === false) {
                this.view.flxMakeTransferError.setVisibility(false);
            } else {
                this.view.rtxMakeTransferError.text = status.errorMessage;
                this.view.flxMakeTransferError.setVisibility(true);
                this.view.flxMakeTransferError.setFocus(true);
            }
            this.view.flxMain.forceLayout();
        },

        initializeSearch: function(transactions) {
            this.view.Search.txtSearch.placeholder = "Search using keywords such as recipient name, amount,....";
            this.view.Search.txtSearch.text = "";
            this.view.Search.flxClearBtn.setVisibility(false);
            this.view.Search.flxClearBtn.onClick = this.onSearchClearBtnClick.bind(this, transactions);
            this.view.Search.txtSearch.onKeyUp = this.onTxtSearchKeyUp.bind(this);
            this.view.Search.txtSearch.onDone = this.onSearchBtnClick.bind(this, transactions);
            this.view.Search.btnConfirm.onClick = this.onSearchBtnClick.bind(this, transactions);
        },
        /**
         * method used to enable or disable the clear button.
         */
        onTxtSearchKeyUp: function() {
            var scopeObj = this;
            var searchKeyword = scopeObj.view.Search.txtSearch.text.trim();
            if (searchKeyword.length > 0) {
                scopeObj.view.Search.flxClearBtn.setVisibility(true);
            } else {
                scopeObj.view.Search.flxClearBtn.setVisibility(false);
            }
            this.view.flxSearch.forceLayout();
        },

        /**
         * method used to clear search
         */
        onSearchClearBtnClick: function(transactions) {
            var scopeObj = this;
            scopeObj.view.Search.txtSearch.text = "";
            scopeObj.view.Search.flxClearBtn.setVisibility(false);
            this.view.flxNoTransactions.setVisibility(false);
            this.view.tablePagination.setVisibility(true);
            this.view.flxSort.setVisibility(true);
            this.view.flxSegment.setVisibility(true);
            this.view.flxRowSeperator.setVisibility(true);
            if (transactions.config.defaultSortBy !== "scheduledDate") {
                this.getPastTransactions();
            } else {
                this.getScheduledTransactions();
            }
        },

        /**
         * method to handle the search account functionality
         */
        onSearchBtnClick: function(transactions) {
            var scopeObj = this;
            var searchQuery = scopeObj.view.Search.txtSearch.text.trim();
            if (searchQuery !== "") {
                var data = scopeObj.getSearchData(transactions);
                if (transactions.config.defaultSortBy !== "scheduledDate") {
                    if (data.pastTransfers.length > 0) {
                        scopeObj.showPastTransactions(data);
                    } else {
                        scopeObj.view.flxNoTransactions.setVisibility(true);
                        scopeObj.view.tablePagination.setVisibility(false);
                        scopeObj.view.flxSort.setVisibility(false);
                        scopeObj.view.flxSegment.setVisibility(false);
                        scopeObj.view.flxRowSeperator.setVisibility(false);
                    }
                } else if (data.transactions.length > 0) {
                    scopeObj.showScheduledTransactions(data);
                } else {
                    scopeObj.view.flxNoTransactions.setVisibility(true);
                    scopeObj.view.tablePagination.setVisibility(false);
                    scopeObj.view.flxSort.setVisibility(false);
                    scopeObj.view.flxSegment.setVisibility(false);
                    scopeObj.view.flxRowSeperator.setVisibility(false);
                }
            } else {
                scopeObj.view.Search.flxClearBtn.setVisibility(false);
                this.view.flxNoTransactions.setVisibility(false);
                this.view.tablePagination.setVisibility(true);
                this.view.flxSort.setVisibility(true);
                this.view.flxSegment.setVisibility(true);
                this.view.flxRowSeperator.setVisibility(true);
                if (transactions.config.defaultSortBy !== "scheduledDate") {
                    this.getPastTransactions();
                } else {
                    this.getScheduledTransactions();
                }
            }
            scopeObj.view.forceLayout();
        },

        /**
         * method to get data from search and filter values
         */
        getSearchData: function(transactions) {
            var scopeObj = this;
            var searchQuery = scopeObj.view.Search.txtSearch.text.trim();
            var data = (transactions.config.defaultSortBy !== "scheduledDate") ? transactions.pastTransfers : transactions.transactions;
            if (!kony.sdk.isNullOrUndefined(searchQuery) && searchQuery !== "") {
                var rowdata = data.filter(function(record) {
                    return (record["toAccountName"] && record["toAccountName"].toUpperCase().indexOf(searchQuery.toUpperCase()) !== -1) ||
                        (record["transactionDate"] && record["transactionDate"].toUpperCase().indexOf(searchQuery.toUpperCase()) !== -1) ||
                        (record["amount"] && record["amount"].toUpperCase().indexOf(searchQuery.toUpperCase()) !== -1) ||
                        (record["payPersonName"] && record["payPersonName"].toUpperCase().indexOf(searchQuery.toUpperCase()) !== -1);
                });
                data = rowdata;
            }
            if (transactions.config.defaultSortBy !== "scheduledDate") {
                transactions.pastTransfers = data;
            } else {
                transactions.transactions = data;
            }
            return transactions;
        },
    };
});