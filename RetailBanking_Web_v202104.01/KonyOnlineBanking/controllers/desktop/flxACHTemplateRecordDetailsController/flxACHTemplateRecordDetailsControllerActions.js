define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onKeyUp defined for tbxCrAmount **/
    AS_TextField_a40b68930e114ee4aa8fbead08766986: function AS_TextField_a40b68930e114ee4aa8fbead08766986(eventobject, context) {
        var self = this;
        this.enableOrDisableProceed();
        this.executeOnParent("updateTotal", eventobject);
    },
    /** onKeyUp defined for tbxCrAccountNumber **/
    AS_TextField_b47bc42f0e9447d083367dc3e98119c2: function AS_TextField_b47bc42f0e9447d083367dc3e98119c2(eventobject, context) {
        var self = this;
        return self.enableOrDisableProceed.call(this);
    },
    /** onKeyUp defined for tbxEmpIDNum **/
    AS_TextField_b4fc352f08be43a6913f29ff6a5f5ef5: function AS_TextField_b4fc352f08be43a6913f29ff6a5f5ef5(eventobject, context) {
        var self = this;
        return self.enableOrDisableProceed.call(this);
    },
    /** onKeyUp defined for tbxCrTRCNumber **/
    AS_TextField_bd4173508d8e46f8b24c18cbbf5a14aa: function AS_TextField_bd4173508d8e46f8b24c18cbbf5a14aa(eventobject, context) {
        var self = this;
        return self.enableOrDisableProceed.call(this);
    },
    /** onKeyUp defined for tbxTaxSubAmount **/
    AS_TextField_c809361fb3e04f4cbacc55d49560189c: function AS_TextField_c809361fb3e04f4cbacc55d49560189c(eventobject, context) {
        var self = this;
        this.enableOrDisableProceed();
        this.executeOnParent("updateTotal", eventobject);
    },
    /** onKeyUp defined for tbxAdditionalInfo **/
    AS_TextField_c9b02cc37fc546d789d828fbd4bf0dee: function AS_TextField_c9b02cc37fc546d789d828fbd4bf0dee(eventobject, context) {
        var self = this;
        return self.enableOrDisableProceed.call(this);
    },
    /** onKeyUp defined for tbxCrName **/
    AS_TextField_cb2e0009de3a4623ad8a1a5b6be9c1fc: function AS_TextField_cb2e0009de3a4623ad8a1a5b6be9c1fc(eventobject, context) {
        var self = this;
        return self.enableOrDisableProceed.call(this);
    },
    /** onKeyUp defined for tbxCrDetailID **/
    AS_TextField_d0bfc42490b748dd9c1effc474332341: function AS_TextField_d0bfc42490b748dd9c1effc474332341(eventobject, context) {
        var self = this;
        return self.enableOrDisableProceed.call(this);
    },
    /** onKeyUp defined for tbxAmount **/
    AS_TextField_f5ba95246210403c9b801df841b6ffb1: function AS_TextField_f5ba95246210403c9b801df841b6ffb1(eventobject, context) {
        var self = this;
        this.executeOnParent("updateTotal", eventobject);
    },
    /** onKeyUp defined for tbxTrcNum **/
    AS_TextField_f89712e0b27442438b6a26314c964ad4: function AS_TextField_f89712e0b27442438b6a26314c964ad4(eventobject, context) {
        var self = this;
        return self.enableOrDisableProceed.call(this);
    },
    /** onKeyUp defined for tbxAccNum **/
    AS_TextField_j84f540967ec47a09b0dc044d6c48f5c: function AS_TextField_j84f540967ec47a09b0dc044d6c48f5c(eventobject, context) {
        var self = this;
        return self.enableOrDisableProceed.call(this);
    }
});