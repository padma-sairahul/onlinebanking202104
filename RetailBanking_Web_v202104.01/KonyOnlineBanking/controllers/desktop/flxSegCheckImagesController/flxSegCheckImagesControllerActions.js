define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnPrint **/
    AS_Button_i7c90d4584754c3f9ff4edd11045eb01: function AS_Button_i7c90d4584754c3f9ff4edd11045eb01(eventobject, context) {
        var self = this;
        //
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_ae2cef4b1e1c4f0dbf84337630161f52: function AS_FlexContainer_ae2cef4b1e1c4f0dbf84337630161f52(eventobject, context) {
        var self = this;
        this.showUnselectedRow();
    },
    /** onClick defined for flxRememberCategory **/
    AS_FlexContainer_bed7def0eb764bbd8169c937997f136d: function AS_FlexContainer_bed7def0eb764bbd8169c937997f136d(eventobject, context) {
        var self = this;
        this.rememberCategory();
    },
    /** onClick defined for flxCheckImageIcon **/
    AS_FlexContainer_cc1e2029ada84f2baa35080f842ce11c: function AS_FlexContainer_cc1e2029ada84f2baa35080f842ce11c(eventobject, context) {
        var self = this;
        this.showCheckImage();
    },
    /** onClick defined for flxCheckImage2Icon **/
    AS_FlexContainer_e619b0225ab34d6cb3a177f20f6bd392: function AS_FlexContainer_e619b0225ab34d6cb3a177f20f6bd392(eventobject, context) {
        var self = this;
        this.showCheckImage();
    }
});