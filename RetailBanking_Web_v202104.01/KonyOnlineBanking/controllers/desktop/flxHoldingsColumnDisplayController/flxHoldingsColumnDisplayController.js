define({ 

 //Type your controller code here 
  init:function(){
    //data initialization can go here ?
    this.view.flxCheckbox.onTouchEnd = this.checkboxToggle;
  },
  checkboxToggle:function(){
   // var visibility = this.view.imgCheckBox.isVisible;
    var visibility = !(this.view.imgCheckBox.isVisible);
    this.view.imgCheckBox.isVisible = visibility;
    this.view.imgUncheckBox.isVisible = !visibility;
  }

 });