define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnRequest **/
    AS_Button_d1dd18489eab4a788c2588949b040ffa: function AS_Button_d1dd18489eab4a788c2588949b040ffa(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for btnSend **/
    AS_Button_e478105bcc6d4ae48763cdf7b74fdc64: function AS_Button_e478105bcc6d4ae48763cdf7b74fdc64(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_hf021f59b524435d9b0c357136a0a2f2: function AS_FlexContainer_hf021f59b524435d9b0c357136a0a2f2(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});