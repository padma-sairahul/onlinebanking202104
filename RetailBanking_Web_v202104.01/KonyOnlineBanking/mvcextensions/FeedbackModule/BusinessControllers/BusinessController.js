define([], function() {
    function Feedback_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(Feedback_BusinessController, kony.mvc.Business.Controller);
    Feedback_BusinessController.prototype.initializeBusinessController = function() {};
    Feedback_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return Feedback_BusinessController;
});