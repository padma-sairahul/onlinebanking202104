define([], function() {
    /**
     * User defined business controller
     * @constructor
     * @extends kony.mvc.Business.Controller
     */
    function MessagesNewBusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(MessagesNewBusinessController, kony.mvc.Business.Controller);
    /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller gets initialized
     * @method  */

    MessagesNewBusinessController.prototype.initializeBusinessController = function() {};
    /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller is told to execute a command
     * @method
     * @param {Object} kony.mvc.Business.Command Object
     */
    MessagesNewBusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };

    return MessagesNewBusinessController;

});