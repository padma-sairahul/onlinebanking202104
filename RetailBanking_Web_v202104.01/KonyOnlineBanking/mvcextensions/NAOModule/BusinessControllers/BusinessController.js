define([], function() {
    function NAO_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(NAO_BusinessController, kony.mvc.Business.Controller);
    NAO_BusinessController.prototype.initializeBusinessController = function() {};
    NAO_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return NAO_BusinessController;
});