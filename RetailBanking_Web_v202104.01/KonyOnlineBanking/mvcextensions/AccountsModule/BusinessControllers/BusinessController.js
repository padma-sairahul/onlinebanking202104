define([], function() {
    function Accounts_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(Accounts_BusinessController, kony.mvc.Business.Controller);
    getExternalAccountTransactionsCommand = function(params, completionCallback) {
        return new kony.mvc.Business.Command("com.kony.accounts.getExternalAccountTransactions", params, completionCallback);
    };
    getScheduledAccountTransactionsCommand = function(params, completionCallback) {
        return new kony.mvc.Business.Command("com.kony.accounts.getScheduledAccountTransactions", params, completionCallback);
    };
    GetAccountsCommand = function(params, completionCallback) {
        return new kony.mvc.Business.Command("com.kony.accounts.getAccounts", params, completionCallback);
    };
    updateFavouriteStatusCommand = function(params, completionCallback) {
        return new kony.mvc.Business.Command("com.kony.accounts.updateFavouriteStatus", params, completionCallback);
    };
    GetActionsCommand = function(params, completionCallback) {
        return new kony.mvc.Business.Command("com.kony.accounts.getActions", params, completionCallback);
    };
    getRecentAccountTransactionsCommand = function(params, completionCallback) {
        return new kony.mvc.Business.Command("com.kony.accounts.getRecentAccountTransactions", params, completionCallback);
    };
    getExternalAccountsCommand = function(params, completionCallback) {
        return new kony.mvc.Business.Command("com.kony.accounts.getExternalAccounts", params, completionCallback);
    };
    getDashboardConfigurationsCommand = function(params, completionCallback) {
        return new kony.mvc.Business.Command("com.kony.accounts.getDashboardConfigurations", params, completionCallback);
    };
    Accounts_BusinessController.prototype.initializeBusinessController = function() {};
    Accounts_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return Accounts_BusinessController;
});