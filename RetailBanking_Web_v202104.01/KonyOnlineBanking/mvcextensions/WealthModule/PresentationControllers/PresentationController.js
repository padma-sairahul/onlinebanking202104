define(['CommonUtilities'], function(CommonUtilities) {
  /**
     * User defined presentation controller
     * @constructor 
     * @extends kony.mvc.Presentation.BasePresenter
     */
  function Wealth_PresentationController() {
    scope_WealthPresentationController = this;
    kony.mvc.Presentation.BasePresenter.call(this);
    scope_WealthPresentationController.isFirst = true;
    scope_WealthPresentationController.jointAccountDetails = {};
    scope_WealthPresentationController.isJointAccount = false;
    scope_WealthPresentationController.instrumentAction = '';
    scope_WealthPresentationController.portfolioTotalValue = '';
    scope_WealthPresentationController.portfolioTotalValueCurr = '';
    scope_WealthPresentationController.selectedInstrDetails = {};
    scope_WealthPresentationController.selectedCurrencies = [];
    scope_WealthPresentationController.viewOrdersTab = false;
    scope_WealthPresentationController.holdingsType = "";
    scope_WealthPresentationController.tradeCurrency = "";
    scope_WealthPresentationController.instrumentId = "";
    scope_WealthPresentationController.quantity = "0";
    scope_WealthPresentationController.currentPosition = "0.00";
    scope_WealthPresentationController.cashBalance = "";
    scope_WealthPresentationController.reload = "";
    this.portofId = '';
    this.accObj = {};
    this.instrumentDetails={};
    this.instrumentPricingData={};
    this.instrumentCurrentPosition={};
  }

  inheritsFrom(Wealth_PresentationController, kony.mvc.Presentation.BasePresenter);

  this.ISIN = "";
  this.RICCode = "";
  this.navPage = "";
  this.instrumentId = "";

  Wealth_PresentationController.prototype.getPortfolioId = function() {
    return this.portofId;
  };

  Wealth_PresentationController.prototype.setPortfolioId = function(params) {
    this.portofId = params;
  };

  Wealth_PresentationController.prototype.getAccountsListObj = function() {
    return this.accObj;
  };

  Wealth_PresentationController.prototype.setAccountsListObj = function(params) {
    this.accObj = params;
  };
  
   Wealth_PresentationController.prototype.getInstrumentDetails = function() {
    return this.instrumentDetails;
  };

  Wealth_PresentationController.prototype.setInstrumentDetails = function(params) {
    this.instrumentDetails = params;
  };
  
   Wealth_PresentationController.prototype.getInstrumentPricingData = function() {
    return this.instrumentPricingData;
  };

  Wealth_PresentationController.prototype.setInstrumentPricingData = function(params) {
    this.instrumentPricingData = params;
  };
  
    Wealth_PresentationController.prototype.getInstrumentCurrentPosition = function() {
    return this.instrumentCurrentPosition;
  };

  Wealth_PresentationController.prototype.setInstrumentCurrentPosition = function(params) {
    this.instrumentCurrentPosition = params;
  };
  
  /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
  Wealth_PresentationController.prototype.initializePresentationController = function() {

  };

  //   Wealth_PresentationController.prototype.fetchStockDetails=function(){

  //     var navManager = applicationManager.getNavigationManager();
  //     if(kony.application.getCurrentForm().id ==="frmInstrumentDetails"){
  //       var data = navManager.getCustomInfo('frmTopNews');
  //       var stock = {};
  //       stock.flag = true;
  //       data.stock = stock;
  //       navManager.setCustomInfo('frmTopNews', data);
  //       navManager.navigateTo("frmTopNews");
  //     }
  //   };
  Wealth_PresentationController.prototype.getInstrumentsList=function(params){

    applicationManager.getWealthManager().getSearchList(params,this.getInstrumentsListOnSuccess.bind(this), this.getInstrumentsListOnError.bind(this));


  };
  Wealth_PresentationController.prototype.getInstrumentsListOnSuccess=function(data){
    applicationManager.getNavigationManager().updateForm({
      InstrumentsList: data
    },"frmPortfolioOverview");
  };
  Wealth_PresentationController.prototype.getInstrumentsListOnError=function(){

  };


  Wealth_PresentationController.prototype.getCashBalanceDetails=function(params){
    //params = {"portfolioCode" : "T_AI_CMCPTF3"};
    applicationManager.getWealthManager().getAssets(params, this.getCashBalanceDetailsOnSuccess.bind(this), this.getCashBalanceDetailsOnError.bind(this));
  };

  Wealth_PresentationController.prototype.getCashBalanceDetailsOnSuccess=function(data){
    let currentFormName = kony.application.getCurrentForm().id;
    let getNewCurrencies = scope_WealthPresentationController.selectedCurrencies;
    if(getNewCurrencies.length){
      data.cashAccounts = [
        ...data.cashAccounts,
        ...getNewCurrencies
      ];
    }
    scope_WealthPresentationController.portfolioTotalValue = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(data.instrumentTotal[0].marketValue, data.referenceCurrency);
    scope_WealthPresentationController.portfolioTotalValueCurr = data.referenceCurrency;
    if(applicationManager.getNavigationManager().getCurrentForm() != "frmPortfolioOverview")
      {
      applicationManager.getPresentationUtility().dismissLoadingScreen();   
      }
    if(currentFormName === "frmPlaceOrder"){
      let controller = applicationManager.getPresentationUtility().getController('frmPlaceOrder', true);
      scope_WealthPresentationController.cashBalance = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(data.totalCashBalance, data.totalCashBalanceCurrency);
      controller.setCashBalance(data);    
    }
    else{
    applicationManager.getNavigationManager().updateForm({
      cashCurrencyList:data
    }, currentFormName);
      }
  };

  Wealth_PresentationController.prototype.getCashBalanceDetailsOnError=function(){

  };


  Wealth_PresentationController.prototype.getCashCurrencyDetailsOnSuccess=function(data){
    var allHoldings = [];
    var cashHoldings = [];
    var stockHoldings = [];

    allHoldings = data.holdings;
    for (var eachObj in allHoldings){
      var newElem = allHoldings[eachObj];
      if(allHoldings[eachObj].AssetClass === "Cash"){
        cashHoldings.push(newElem);
      }
      else if(allHoldings[eachObj].AssetClass != "Cash"){

        newElem.Price=applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(allHoldings[eachObj].Price, allHoldings[eachObj].PriceCurrency);
        newElem.AveragePrice=applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(allHoldings[eachObj].AveragePrice, allHoldings[eachObj].AveragePriceCurrency);
        newElem.MarketValue=applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(allHoldings[eachObj].MarketValue, allHoldings[eachObj].MarketValueCurrency);
        newElem.Weight = parseFloat(allHoldings[eachObj].Weight).toFixed(2);
        newElem.UnrealizedPL = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(allHoldings[eachObj].UnrealizedPL, allHoldings[eachObj].MarketValueCurrency);
        newElem.CostValue = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(allHoldings[eachObj].CostValue, allHoldings[eachObj].MarketValueCurrency);
        newElem.UnrealizedPLPercent = parseFloat(allHoldings[eachObj].UnrealizedPLPercent).toFixed(2);
        newElem.ExchangeRate = parseFloat(allHoldings[eachObj].ExchangeRate).toFixed(2);
        newElem.CostExchangeRate = parseFloat(allHoldings[eachObj].CostExchangeRate).toFixed(2);
        newElem.Img="contextual_menu.png";     
        stockHoldings.push(newElem);
      }
    }
    applicationManager.getNavigationManager().updateForm({
      cashCurrencyList: cashHoldings
    });
    applicationManager.getNavigationManager().updateForm({
      HoldingsList: stockHoldings
    });

  };

  // Holdings List
  Wealth_PresentationController.prototype.getHoldings = function(params) {
    let currentFormName = kony.application.getCurrentForm().id;
    if(currentFormName !== "frmPlaceOrder"){
      applicationManager.getPresentationUtility().showLoadingScreen();
    }
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getHoldingList(params, this.getHoldingsSuccess.bind(this), this.getHoldingsError.bind(this));
  };
  Wealth_PresentationController.prototype.getHoldingsSuccess = function(response) {
    //applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager =  applicationManager.getNavigationManager();
    if (scope_WealthPresentationController.instrumentAction === 'Sell' || scope_WealthPresentationController.instrumentAction === 'Buy' || scope_WealthPresentationController.instrumentAction === 'Modify'){
    //  applicationManager.getPresentationUtility().dismissLoadingScreen();
      response.RICCode = this.RICCode;
     
      navManager.updateForm({
        holdingsList: response
      }, 'frmPlaceOrder');
    } else {
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      if (scope_WealthPresentationController.instrumentAction === 'SearchInstrument' || scope_WealthPresentationController.instrumentAction === 'Watchlist'){

        var dataCurrentPosition = navManager.getCustomInfo('frmInstrumentDetails');
        if (dataCurrentPosition === undefined) {
          dataCurrentPosition = {};
        }
        if (response.portfolioHoldings.length>0){
          dataCurrentPosition.portfolioDetails = response.portfolioHoldings[0];
        } else {
          dataCurrentPosition.portfolioDetails={};
        }
        navManager.setCustomInfo('frmInstrumentDetails', dataCurrentPosition);

        applicationManager.getNavigationManager().updateForm({
          currentPosition:  dataCurrentPosition.portfolioDetails
        },"frmInstrumentDetails");
      }else{

        applicationManager.getNavigationManager().updateForm({
          portfolioHoldingsList: response
        }, "frmPortfolioOverview");
      }
    }
  };
  
  Wealth_PresentationController.prototype.getHoldingsError = function(err) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  };


  //Transactions List
  Wealth_PresentationController.prototype.getTransactionDetails = function(params) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getTransactionList(params, this.getTransactionDetailsSuccess.bind(this), this.getTransactionDetailsError.bind(this));
  };
  Wealth_PresentationController.prototype.getTransactionDetailsSuccess = function(response) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getNavigationManager().updateForm({
      portfolioTransactionList: response
    },"frmPortfolioOverview");

  };
  Wealth_PresentationController.prototype.getTransactionDetailsError = function(err) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  };

  Wealth_PresentationController.prototype.fetchCurrentPosition=function(params){
    applicationManager.getWealthManager().getCurrentPositions(params,this.getCurrentPositionsOnSuccess.bind(this), this.getCurrentPositionsOnError.bind(this));
  };
  Wealth_PresentationController.prototype.getCurrentPositionsOnSuccess=function(data){
    applicationManager.getNavigationManager().updateForm({
      currentPosition: data
    },"frmInstrumentDetails");
  };
  Wealth_PresentationController.prototype.getCurrentPositionsOnError=function(){

  };
  Wealth_PresentationController.prototype.fetchInstrumentDetails=function(params){
    applicationManager.getWealthManager().getIndexDetails(params,this.getIndexDetailsOnSuccess.bind(this), this.getIndexDetailsOnError.bind(this));
  };
  Wealth_PresentationController.prototype.getIndexDetailsOnSuccess=function(data){
    applicationManager.getNavigationManager().updateForm({
      InstrumentDetails: data
    },"frmInstrumentDetails");
  };
  Wealth_PresentationController.prototype.getIndexDetailsOnError=function(){

  };
  Wealth_PresentationController.prototype.getCashCurrencyDetailsOnError=function(){

  };
  Wealth_PresentationController.prototype.fetchStockNews=function(params){
    applicationManager.getWealthManager().getStockNewsDetails(params,this.getStocksOnSuccess.bind(this), this.getStocksOnError.bind(this));
  };
  Wealth_PresentationController.prototype.getStocksOnSuccess=function(data){
    applicationManager.getNavigationManager().updateForm({
      StockNews: data
    },"frmInstrumentDetails");
  };
  Wealth_PresentationController.prototype.getStocksOnError=function(){

  };
  Wealth_PresentationController.prototype.fetchDocuments=function(params){
    applicationManager.getWealthManager().getDocuments(params,this.getDocumentsOnSuccess.bind(this), this.getDocumentsOnError.bind(this));
  };
  Wealth_PresentationController.prototype.getDocumentsOnSuccess=function(data){
    applicationManager.getNavigationManager().updateForm({
      Documents: data
    },"frmInstrumentDetails");
  };
  Wealth_PresentationController.prototype.getDocumentsOnError=function(){

  };
  //getPortfolioList
  Wealth_PresentationController.prototype.getPortfolioList = function(params) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getPortfolioList(params, this.getPortfolioListSuccess.bind(this), this.getPortfolioListError.bind(this));
  };
  Wealth_PresentationController.prototype.getPortfolioListSuccess = function(obj) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    scope_WealthPresentationController.jointAccountDetails = obj.PortfolioList;
    var data = {};
//    var marketData = {};
    data.response = obj;
//    marketData.response = obj.GetSimpleData_Response_2.SimpleDataResult ? obj.GetSimpleData_Response_2.SimpleDataResult.ItemResponse[0].Item : null;
    var controller = applicationManager.getPresentationUtility().getController('frmDashboard', true);
    var navMan = applicationManager.getNavigationManager();
    navMan.setCustomInfo("frmDashboard", data);
    //         var controller = applicationManager.getPresentationUtility().getController('CopyfrmAccountsLanding', true);
    //         controller.loadInvestmentAccounts(data);
    applicationManager.getNavigationManager().updateForm({
      InvestmentAccountsData: data
    },"frmDashboard");
    var currentFormName = kony.application.getCurrentForm().id;
    if(currentFormName === "frmDashboard"){
       controller.portfolioListLoaded();
    }
  };
  Wealth_PresentationController.prototype.getPortfolioListError = function(err) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
     var currentFormName = kony.application.getCurrentForm().id;
    if(currentFormName === "frmDashboard"){
       var controller = applicationManager.getPresentationUtility().getController('frmDashboard', true);
       controller.portfolioListLoaded();
    }
    
  };
  //getAssetsList
  Wealth_PresentationController.prototype.getAssetsList = function(params) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getAssetsList(params, this.getAssetsListSuccess.bind(this), this.getAssetsListError.bind(this));
  };
  Wealth_PresentationController.prototype.getAssetsListSuccess = function(obj) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var data = {};
    data.response = obj;
    var controller = applicationManager.getPresentationUtility().getController('frmDashboard', true);
    //frmDashboard
    controller.loadAssetsDetails(data);
  };
  Wealth_PresentationController.prototype.getAssetsListError = function(err) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  }
  // get activity list
  Wealth_PresentationController.prototype.getRecentActivity = function(params) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getRecentActivity(params, this.getRecentActivitySuccess.bind(this), this.getRecentActivityError.bind(this));
  };
  Wealth_PresentationController.prototype.getRecentActivitySuccess = function(obj) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var data = {};
    data.response = obj;
    var controller = applicationManager.getPresentationUtility().getController('CopyfrmAccountsLanding', true);
    controller.loadActivityListData(data);
  };
  Wealth_PresentationController.prototype.getRecentActivityError = function(err) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  };
  /**
     * 
     **/
  Wealth_PresentationController.prototype.getDashboardGraphDetails = function(params) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getDashboardDetails(params, this.getDashboardDetailsSuccessCallback.bind(this), this.getDashboardDetailsErrorCallback.bind(this));
  };
  /**
     *
     **/
  Wealth_PresentationController.prototype.getDashboardDetailsSuccessCallback = function(obj) {
    var navManager = applicationManager.getNavigationManager();
    var data = {};
    data.response = obj;
    var controller = applicationManager.getPresentationUtility().getController('frmDashboard', true);
    controller.lineChartData(data);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  };
  /**
     *
     **/
  Wealth_PresentationController.prototype.getDashboardDetailsErrorCallback = function() {
    var data = {};
    data.response = {};
    var controller = applicationManager.getPresentationUtility().getController('frmDashboard', true);
    controller.lineChartData(data);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  };
  Wealth_PresentationController.prototype.getPortfolioAndGraphDetails = function(params) {
     if(applicationManager.getNavigationManager().getCurrentForm() != "frmPortfolioOverview"){
    applicationManager.getPresentationUtility().showLoadingScreen();
    }
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getPortfolioDetails(params, this.getPortfolioDetailsSuccessCallback.bind(this), this.getPortfolioDetailsErrorCallback.bind(this));
  };
  /**
     *
     **/
  Wealth_PresentationController.prototype.getPortfolioDetailsSuccessCallback = function(obj) {
    var navManager = applicationManager.getNavigationManager();
    var data = {};
    data.response = obj;
    scope_WealthPresentationController.portfolioTotalValue = applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(obj.marketValue, obj.referenceCurrency);
    scope_WealthPresentationController.portfolioTotalValueCurr = obj.referenceCurrency;
    var controller;
      controller = applicationManager.getPresentationUtility().getController('frmPortfolioOverview', true);
      controller.bindNewGraphData(data); 
  };
  /**
     *
     **/
  Wealth_PresentationController.prototype.getPortfolioDetailsErrorCallback = function() {

  };

  //getInstrumenttDetails
  Wealth_PresentationController.prototype.getProductDetailsById = function(params) {  
     let currentFormName = kony.application.getCurrentForm().id;
    if(currentFormName !== "frmPlaceOrder"){
      applicationManager.getPresentationUtility().showLoadingScreen();
    }
    applicationManager.getWealthManager().getInstrumentDetailsById(params, this.getProductDetailsSuccessById.bind(this), this.getProductDetailsErrorById.bind(this));
  };

  Wealth_PresentationController.prototype.getProductDetailsSuccessById = function(data) {
    let currentFormName = kony.application.getCurrentForm().id;
    if(data.error) {
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      applicationManager.getNavigationManager().updateForm({
        instrumentError: data.error
      }, currentFormName);
    } else {
       scope_WealthPresentationController.tradeCurrency = data.instrumentMinimal[0]? data.instrumentMinimal[0].priceCurrency:'';
      if (scope_WealthPresentationController.instrumentAction === 'Sell' || scope_WealthPresentationController.instrumentAction === 'Buy' || scope_WealthPresentationController.instrumentAction === 'Modify'){
       // applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager =  applicationManager.getNavigationManager();
        navManager.updateForm({
          ProductDetails: data
        }, 'frmPlaceOrder');
      } else 
        if (scope_WealthPresentationController.instrumentAction === 'Reload'){
          applicationManager.getPresentationUtility().dismissLoadingScreen();
          let controller = applicationManager.getPresentationUtility().getController('frmInstrumentDetails', true);
          controller.refreshInstrumentDetails(data); 

        }else{
          var instrumentDetailsData = applicationManager.getNavigationManager().getCustomInfo('frmInstrumentDetails');
          if(instrumentDetailsData===undefined){
            instrumentDetailsData={};
          }
          instrumentDetailsData.productDetails=data;
          applicationManager.getNavigationManager().setCustomInfo('frmInstrumentDetails', instrumentDetailsData);

          applicationManager.getPresentationUtility().dismissLoadingScreen();
          applicationManager.getNavigationManager().navigateTo("frmInstrumentDetails");
        }
    }
  };

  Wealth_PresentationController.prototype.getProductDetailsErrorById = function(data) {
    let currentFormName = kony.application.getCurrentForm().id;
    var instrumentDetailsData = applicationManager.getNavigationManager().getCustomInfo('frmInstrumentDetails');
    if(instrumentDetailsData===undefined){
      instrumentDetailsData={};
    }
    instrumentDetailsData.productDetails=data;
    applicationManager.getNavigationManager().setCustomInfo('frmInstrumentDetails', instrumentDetailsData);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    if(currentFormName === "frmPlaceOrder"){
      return;
    }
    else
    applicationManager.getNavigationManager().navigateTo("frmInstrumentDetails");
  };
  
  
  Wealth_PresentationController.prototype.getHistoricalDataInstrument = function(params){
    applicationManager.getWealthManager().getHistoricalCurrencyRate(params,this.getHistoricalDataInstrumentOnSuccess.bind(this), this.getHistoricalDataInstrumentOnError.bind(this));
  };
  Wealth_PresentationController.prototype.getHistoricalDataInstrumentOnSuccess = function(obj){
   
    var data = {};
    data.response = obj;
    var controller = applicationManager.getPresentationUtility().getController('frmInstrumentDetails', true);
    controller.lineChartData(data);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
   
  };
  Wealth_PresentationController.prototype.getHistoricalDataInstrumentOnError = function(){
    applicationManager.getPresentationUtility().dismissLoadingScreen();    
  };

  Wealth_PresentationController.prototype.getHistoricalCurrencyRate = function(params){
    applicationManager.getWealthManager().getHistoricalCurrencyRate(params,this.getHistoricalCurrencyRateOnSuccess.bind(this), this.getHistoricalCurrencyRateOnError.bind(this));
  };
  Wealth_PresentationController.prototype.getHistoricalCurrencyRateOnSuccess = function(obj){
    var data = {};
    data.response = obj;
    var controller = applicationManager.getPresentationUtility().getController('frmCurrencyConverter', true);
    controller.lineChartData(data);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  };
  Wealth_PresentationController.prototype.getHistoricalCurrencyRateOnError = function(){
    applicationManager.getPresentationUtility().dismissLoadingScreen();    
  };

  Wealth_PresentationController.prototype.getCurrencyRate = function(params){
    applicationManager.getWealthManager().getCurrencyRate(params,this.getCurrencyRateOnSuccess.bind(this), this.getCurrencyRateOnError.bind(this));
  };
  Wealth_PresentationController.prototype.getCurrencyRateOnSuccess = function(obj){
    var controller = applicationManager.getPresentationUtility().getController('frmCurrencyConverter', true);
    controller.setRate(obj);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  };
  Wealth_PresentationController.prototype.getCurrencyRateOnError = function(){
    applicationManager.getPresentationUtility().dismissLoadingScreen();    
  };

  Wealth_PresentationController.prototype.createOrder = function(params){
    applicationManager.getWealthManager().createOrder(params,this.createOrderOnSuccess.bind(this), this.createOrderOnError.bind(this));
  };
  Wealth_PresentationController.prototype.createOrderOnSuccess = function(obj){
    let currentFormName = kony.application.getCurrentForm().id;
    var controller = applicationManager.getPresentationUtility().getController(currentFormName, true);
    controller.setSuccess(obj);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  };
  Wealth_PresentationController.prototype.createOrderOnError = function(){
    let currentFormName = kony.application.getCurrentForm().id;
    var controller = applicationManager.getPresentationUtility().getController(currentFormName, true);
    controller.setError(obj);
    applicationManager.getPresentationUtility().dismissLoadingScreen();    
  };

  Wealth_PresentationController.prototype.getCurrencyList = function(){
    applicationManager.getWealthManager().getCurrencyList(this.getCurrencyListOnSuccess.bind(this), this.getCurrencyListOnError.bind(this));
  };
  Wealth_PresentationController.prototype.getCurrencyListOnSuccess = function(obj){
    var controller = applicationManager.getPresentationUtility().getController('frmCurrencyConverter', true);
    controller.setCurrencyList(obj.AddCurrency);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  };
  Wealth_PresentationController.prototype.getCurrencyListOnError = function(){
    applicationManager.getPresentationUtility().dismissLoadingScreen();    
  };
  /**
     * navigate to frmWatchlist
     **/
  Wealth_PresentationController.prototype.getWatchList = function(params) {
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getWatchList(params, this.getWatchListSuccess.bind(this), this.getWatchListError.bind(this));
    var navMan = applicationManager.getNavigationManager();
    navMan.navigateTo("frmWatchlist");
  };
  Wealth_PresentationController.prototype.getWatchListSuccess = function(response) {
    var navManager = applicationManager.getNavigationManager();
    data=[{ 
      "lblInstruName": "Amazon",
      "lblISIN": "1",
      "lblCurrency": "USD",
      "lblLatest": "2000",
      "lblChange": "",
      "lblDateTime": "2021020312000",
      "lblBid": "2300",
      "lblAsk": "25",
      "lblVolume": "2000"
    },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "+0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "+0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "+0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "+0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "+0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "+0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "+0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "+0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "-0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          },
          { 
            "lblInstruName": "Google",
            "lblISIN": "2",
            "lblCurrency": "USD",
            "lblLatest": "3000",
            "lblChange": "+0.65%",
            "lblDateTime": "2021020312000",
            "lblBid": "2600",
            "lblAsk": "45",
            "lblVolume": "2500"
          }
         ]; 
    navManager.updateForm({
      watchListDetails: data
    }, "frmWatchlist");     
  };
  Wealth_PresentationController.prototype.getWatchListError = function(err) {

  };


  Wealth_PresentationController.prototype.getTopNews = function(params) {

    applicationManager.getPresentationUtility().showLoadingScreen();
    applicationManager.getWealthManager().getTopNews(params, this.getTopNewsSuccess.bind(this), this.getTopNewsError.bind(this));

  };

  Wealth_PresentationController.prototype.getTopNewsSuccess = function(response) {

    applicationManager.getPresentationUtility().dismissLoadingScreen();

    var navMan = applicationManager.getNavigationManager();
    var data = navMan.getCustomInfo('frmTopNews');
    if(data===undefined){
      data={};
    }
    data.News=response;
    navMan.setCustomInfo('frmTopNews', data);

    navMan.updateForm({
      NewsList: response
    }, 'frmTopNews');



  };

  Wealth_PresentationController.prototype.getTopNewsError = function(err) {
    let currentFormName = kony.application.getCurrentForm().id;

    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getNavigationManager().updateForm({
      errorResponse: err.errorMessage
    }, currentFormName);   
  };



  Wealth_PresentationController.prototype.getStockNewsDetails = function(params) {

    applicationManager.getPresentationUtility().showLoadingScreen();
    applicationManager.getWealthManager().getStockNewsDetails(params, this.getStockNewsDetailsSuccess.bind(this), this.getStockNewsDetailsError.bind(this));

  };

  Wealth_PresentationController.prototype.getStockNewsDetailsSuccess = function(response) {

    applicationManager.getPresentationUtility().dismissLoadingScreen();

    var navMan = applicationManager.getNavigationManager();
    var data = navMan.getCustomInfo('frmTopNews');
    if(data===undefined){
      data={};
    }
    data.News=response;
    navMan.setCustomInfo('frmTopNews', data);

    navMan.updateForm({
      NewsList: response
    }, 'frmTopNews');



  };

  Wealth_PresentationController.prototype.getStockNewsDetailsError = function(err) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getNavigationManager().updateForm({
      NewsError: err
    }, 'frmTopNews');
  };

  Wealth_PresentationController.prototype.fetchNewsDetails=function(){

    let navManager = applicationManager.getNavigationManager();
    let data = navManager.getCustomInfo('frmTopNews');
     if(data===undefined){
      data={};
    }
    let stock = false;
    let params ;
    if(kony.application.getCurrentForm().id==="frmInstrumentDetails"){
     
      let portRicCode = applicationManager.getNavigationManager().getCustomInfo('frmInstrumentDetails').portfolioDetails.RICCode;
      let prodDetails = applicationManager.getNavigationManager().getCustomInfo('frmInstrumentDetails').productDetails;
      let instrumentRicCode = "";
      if (prodDetails.instrumentMinimal[0])
       instrumentRicCode = prodDetails.instrumentMinimal[0].RICCode;
      else
        instrumentRicCode = prodDetails.instrumentMinimal.RICCode;

      data.stock = true;

      params = {
        "RICCode": portRicCode?portRicCode: instrumentRicCode,
        "pageSize": 4,
        "pageOffset": 0

      };
      this.getStockNewsDetails(params);
    }else{
      data.stock = false;
      params = {
        "Topic": "OLUSBUS",
        "pageSize": 4,
        "pageOffset": 0
      };
      this.getTopNews(params);
    }
    navManager.setCustomInfo('frmTopNews', data);
    navManager.navigateTo("frmTopNews");
  };

  Wealth_PresentationController.prototype.getHoldingsCurrentPosition = function(params) {
    this.ISIN = params.ISIN?params.ISIN:"";
    this.RICCode = params.RICCode?params.RICCode:"";
    this.navPage = params.navPage?params.navPage:"";
    this.instrumentId = params.instrumentId?params.instrumentId:"";
     let currentFormName = kony.application.getCurrentForm().id;
    if(currentFormName !== "frmPlaceOrder" || scope_WealthPresentationController.reload == "Reload"){
      applicationManager.getPresentationUtility().showLoadingScreen();
    }
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getHoldingList(params, this.getHoldingsCurrentPositionSuccess.bind(this), this.getHoldingsCurrentPositionError.bind(this));
  };

  Wealth_PresentationController.prototype.getHoldingsCurrentPositionSuccess = function(response) {
    var navManager = applicationManager.getNavigationManager();
    var paramsProdDetails;
    var dataCustom = navManager.getCustomInfo('frmInstrumentDetails');
    if (dataCustom === undefined) {
      dataCustom = {};
    }
    if (response.portfolioHoldings && (response.portfolioHoldings).length>0){
      dataCustom.portfolioDetails = response.portfolioHoldings[0];
      scope_WealthPresentationController.quantity = response.portfolioHoldings[0].quantity;
      scope_WealthPresentationController.tradeCurrency = response.portfolioHoldings[0].secCCy;
      scope_WealthPresentationController.currentPosition =  response.portfolioHoldings[0].marketValPOS;
      scope_WealthPresentationController.instrumentId = response.portfolioHoldings[0].holdingsId;
      scope_WealthPresentationController.holdingsType = response.portfolioHoldings[0].holdingsType;
      paramsProdDetails = {
        "ISINCode": response.portfolioHoldings[0].ISIN?response.portfolioHoldings[0].ISIN:"",
        "RICCode": response.portfolioHoldings[0].RICCode?response.portfolioHoldings[0].RICCode:"",
        "instrumentId": response.portfolioHoldings[0].holdingsId?response.portfolioHoldings[0].holdingsId:""
      };

    } else {
      scope_WealthPresentationController.currentPosition = "0.00";
      scope_WealthPresentationController.quantity = "0";
      dataCustom = {};
      dataCustom.portfolioDetails={};
      dataCustom.portfolioDetails.RICCode = this.RICCode;
      paramsProdDetails = {
        "ISINCode": this.ISIN,
        "RICCode": this.RICCode,
        "instrumentId": this.instrumentId
      };
    }
    navManager.setCustomInfo('frmInstrumentDetails', dataCustom);

    this.getProductDetailsById(paramsProdDetails);
  };

  Wealth_PresentationController.prototype.getHoldingsCurrentPositionError = function(response){
    let currentFormName = kony.application.getCurrentForm().id;

    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getNavigationManager().updateForm({
      instrumentError: response.errorMessage
    }, currentFormName);   
  };

  // Create Market order
  Wealth_PresentationController.prototype.createMarketOrder = function(param) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthmanager = applicationManager.getWealthManager();
    wealthmanager.createMarketOrder(param, this.createMarketOrderSuccess, this.createMarketOrderFailure);
  };
  Wealth_PresentationController.prototype.createMarketOrderSuccess = function(response) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    let currentFormName = kony.application.getCurrentForm().id;
    applicationManager.getNavigationManager().updateForm({
      CreateOrderResponse: response
    }, currentFormName);

  };
  Wealth_PresentationController.prototype.createMarketOrderFailure = function(error) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    let currentFormName = kony.application.getCurrentForm().id;
    applicationManager.getNavigationManager().updateForm({
      CreateOrderResponseError: error
    }, currentFormName);   
  };

  // Modify Market order
  Wealth_PresentationController.prototype.modifyMarketOrder = function(param) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthmanager = applicationManager.getWealthManager();
    wealthmanager.modifyMarketOrder(param, this.modifyMarketOrderSuccess, this.modifyMarketOrderFailure);
  };
  Wealth_PresentationController.prototype.modifyMarketOrderSuccess = function(response) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    let currentFormName = kony.application.getCurrentForm().id;
    applicationManager.getNavigationManager().updateForm({
      ModifyOrderResponse: response
    }, currentFormName);

  };
  Wealth_PresentationController.prototype.modifyMarketOrderFailure = function(error) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    let currentFormName = kony.application.getCurrentForm().id;
    applicationManager.getNavigationManager().updateForm({
     // ModifyOrderResponseError: error
      CreateOrderResponseError: error
    }, currentFormName);   
  };

  // Get Order Details

  Wealth_PresentationController.prototype.getOrdersDetails = function(params) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getOrdersDetails(params, this.getOrdersDetailsSuccess.bind(this), this.getOrdersDetailsError.bind(this));
  };
  Wealth_PresentationController.prototype.getOrdersDetailsSuccess = function(response) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    let currentFormName = kony.application.getCurrentForm().id;
    applicationManager.getNavigationManager().updateForm({
      GetOrderDetailsResponse: response
    }, currentFormName);

  };
  Wealth_PresentationController.prototype.getOrdersDetailsError = function(error) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    let currentFormName = kony.application.getCurrentForm().id;
    applicationManager.getNavigationManager().updateForm({
      GetOrderDetailsResponseError: error
    }, currentFormName);
  };

  // Cancel Order

  Wealth_PresentationController.prototype.cancelOrder = function(params) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.cancelOrder(params, this.cancelOrderSuccess.bind(this), this.cancelOrderError.bind(this));
  };
  Wealth_PresentationController.prototype.cancelOrderSuccess = function(response) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    // let currentFormName = kony.application.getCurrentForm().id;
    // applicationManager.getNavigationManager().updateForm({
    //   CancelOrderResponse: response
    // }, currentFormName);

  };
  Wealth_PresentationController.prototype.cancelOrderError = function(error) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    // let currentFormName = kony.application.getCurrentForm().id;
    // applicationManager.getNavigationManager().updateForm({
    //   CancelOrderResponseError: error
    // }, currentFormName);
  };


  Wealth_PresentationController.prototype.getFavoriteInstruments = function(params) {
     let currentFormName = kony.application.getCurrentForm().id;
    if(currentFormName !== "frmPlaceOrder"){
      applicationManager.getPresentationUtility().showLoadingScreen();
    }
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getFavoriteInstruments(params, this.getFavoriteInstrumentsSuccess.bind(this), this.getFavoriteInstrumentsError.bind(this));
  };

  Wealth_PresentationController.prototype.getFavoriteInstrumentsSuccess = function(response) {
    let currentFormName = kony.application.getCurrentForm().id;
    if(currentFormName !== "frmPlaceOrder"){
      applicationManager.getPresentationUtility().dismissLoadingScreen();
    }
    var controller = applicationManager.getPresentationUtility().getController(kony.application.getCurrentForm().id, true);
    controller.setStarValue(response.favoriteInstruments);

  };
  Wealth_PresentationController.prototype.getFavoriteInstrumentsError = function(error) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();

  };


  Wealth_PresentationController.prototype.updateFavouriteInstruments = function(params) {
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.updateUserFavouriteInstruments(params, this.updateFavouriteInstrumentsSuccess.bind(this), this.updateFavouriteInstrumentsError.bind(this));
  };

  Wealth_PresentationController.prototype.updateFavouriteInstrumentsSuccess = function(response) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();    

  };
  Wealth_PresentationController.prototype.updateFavouriteInstrumentsError = function(error) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();

  };

  Wealth_PresentationController.prototype.getUserFavouriteInstruments = function(params) {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var wealthManager = applicationManager.getWealthManager();
    wealthManager.getUserFavouriteInstruments(params, this.getUserFavouriteInstrumentsSuccess.bind(this), this.getUserFavouriteInstrumentsError.bind(this));
  };

  Wealth_PresentationController.prototype.getUserFavouriteInstrumentsSuccess = function(response) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();

    var controller = applicationManager.getPresentationUtility().getController(kony.application.getCurrentForm().id, true);
    controller.setFavourite(response);

  };
  Wealth_PresentationController.prototype.getUserFavouriteInstrumentsError = function(error) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();

  };
  
  return Wealth_PresentationController;
});