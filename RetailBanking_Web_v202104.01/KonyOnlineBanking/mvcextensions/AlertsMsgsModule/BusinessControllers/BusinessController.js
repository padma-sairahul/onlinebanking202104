define([], function() {
    function AlertsMsgs_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(AlertsMsgs_BusinessController, kony.mvc.Business.Controller);
    AlertsMsgs_BusinessController.prototype.initializeBusinessController = function() {};
    AlertsMsgs_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return AlertsMsgs_BusinessController;
});