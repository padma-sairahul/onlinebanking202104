define([], function() {
    function MultiFactorAuthentication_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(MultiFactorAuthentication_BusinessController, kony.mvc.Business.Controller);
    MultiFactorAuthentication_BusinessController.prototype.initializeBusinessController = function() {};
    MultiFactorAuthentication_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return MultiFactorAuthentication_BusinessController;
});