define(['CommonUtilities', 'OLBConstants', 'SCAConfiguration'], function(CommonUtilities, OLBConstants, SCAConfiguration) {
  this.securityQuestionsPayload = "";
  this.newUserName = "";
  var saveState=false;
  this.dataForSegment=[];
  this.features=[];
  var navigationArray = [];
    function Profile_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(Profile_PresentationController, kony.mvc.Presentation.BasePresenter);
    Profile_PresentationController.prototype.initializeUserProfileClass = function() {
        this.PhoneTypes = {
            '': '',
            'Mobile': 'Mobile',
            'Work': 'Work',
            'Home': 'Home',
            'Other': 'Other'
        };
        this.AddressTypes = {
            "ADR_TYPE_WORK": 'Work',
            "ADR_TYPE_HOME": 'Home'
        };
        this.accountTypeConfig = {};
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING)] = {
            sideImage: 'accounts_sidebar_turquoise.png',
            skin: 'sknflxhex26d0cecode',
            image: 'account_change_turquoise.png'
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING)] = {
            sideImage: 'accounts_sidebar_purple.png',
            skin: 'sknflxhex9060B7code',
            image: 'account_change_purple.png',
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD)] = {
            sideImage: 'accounts_sidebar_yellow.png',
            skin: 'sknflxhexf4ba22code',
            image: 'account_change_yellow.png',
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT)] = {
            sideImage: 'accounts_sidebar_blue.png',
            skin: 'sknflxhex4a90e2code',
            image: 'account_change_turquoise.png'
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE)] = {
            sideImage: 'accounts_sidebar_brown.png',
            skin: 'sknflxhex8D6429code',
            image: 'account_change_yellow.png',
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN)] = {
            sideImage: 'accounts_sidebar_brown.png',
            skin: 'sknflxhex8D6429code',
            image: 'account_change_yellow.png',
        };
        this.accountTypeConfig['Default'] = {
            sideImage: 'accounts_sidebar_turquoise.png',
            skin: 'sknflxhex26d0cecode',
            image: 'account_change_turquoise.png'
        };
    };
    /**
     * Method to fetch alerts and mapping to UI
     * @param {Object} addressSelection - contains if he selected country or state.
     */
    Profile_PresentationController.prototype.getSpecifiedCitiesAndStates = function(addressSelection, addressId, states) {
        var self = this;
        var data = [];
        if (addressSelection === "country") {
            var statesList = [];
            statesList.push(["lbl1", "Select a State"]);
            for (var i = 0; i < Object.keys(states).length; ++i) {
                if (states[i][2] === addressId) {
                    statesList.push([states[i][0], states[i][1]]);
                }
            }
            data = {
                "states": statesList
            };
        } else if (addressSelection === "state") {
            var cityList = [];
            cityList.push(["lbl2", "Select a City"]);
            for (var j = 0; j < Object.keys(states).length; ++j) {
                if (states[j][2] === addressId) {
                    cityList.push([states[j][0], states[j][1]]);
                }
            }
            data = {
                "cities": cityList
            };
        }
        return data;
    };
  Profile_PresentationController.prototype.fetchAccountsList = function(cif, contractID){
	var formController = applicationManager.getPresentationUtility().getController('frmApprovalMatrixAccountLevel', true);
    formController.showLoadingScreen();
    var userId = applicationManager.getUserPreferencesManager().getUserId();
    var params = {"userId":userId};
    var businessUserManager = applicationManager.getBusinessUserManager();
    businessUserManager.getInfinityUserAccounts(params, this.fetchAccountsListSuccess.bind(this,cif,contractID),this.fetchAccountsListFailure.bind(this, cif));

  };

  Profile_PresentationController.prototype.fetchAccountsListSuccess = function(cif, contractId, data){
    var data = data.contracts;
    var accountsList = [];
    var contractCustomers = []
    var accountsList = [];
   	for(var i=0;i<data.length;i++){
      if(!kony.sdk.isNullOrUndefined(data[i].contractId) && data[i].contractId==contractId){
        contractCustomers = data[i].contractCustomers;
        break;
      }
    }
    for(var i=0;i<contractCustomers.length;i++){
      if(!kony.sdk.isNullOrUndefined(contractCustomers[i].id) && contractCustomers[i].id==cif){
        accountsList = contractCustomers[i].coreCustomerAccounts;
        break;
      }
    }
    this.processAccountsData(accountsList);
    var formController = applicationManager.getPresentationUtility().getController('frmApprovalMatrixAccountLevel', true);
    formController.hideLoadingScreen();
  };
  
  Profile_PresentationController.prototype.fetchAccountsListFailure = function(cif,data){
    this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule');
    var accountsList = this.presenter.presentationController.accounts;
    var accountDataforCif=[];
    accountsList.forEach(item=>{
      if(item.Membership_id===cif){
        accountDataforCif.push(item);
      }
    });
    this.processAccountsData(accountDataforCif);
    var formController = applicationManager.getPresentationUtility().getController('frmApprovalMatrixAccountLevel', true);
    formController.hideLoadingScreen();
  };
  
  
  Profile_PresentationController.prototype.navigateToEditPermissionForm = function(data){
     var viewModel = {
       isNavigateToPermission:true,
       data:data
     };
        applicationManager.getNavigationManager().updateForm(viewModel, '');
  };
  Profile_PresentationController.prototype.processAccountsData = function(data) {
    var accProcessedData = [];
    for (var i = 0; i < data.length; i++) {
      accProcessedData[i] = {};
      var name = "";
      if (data[i].nickName === null || data[i].nickName === undefined) {
        name = data[i].accountName;
      } else {
        name = data[i].nickName;
      }
      accProcessedData[i].accountName = data[i].accountName;
      accProcessedData[i].nickName = data[i].nickName;
      var accountID = data[i].accountID || data[i].accountId;
      accProcessedData[i].accountID = accountID;
      accProcessedData[i].accountType = data[i].accountType;
      accProcessedData[i].accountPreference = data[i].accountPreference;
      accProcessedData[i].processedName = this.formatText(name, 24, accountID, 4);
    }
    this.processViewFormattedData(accProcessedData)
    };
   Profile_PresentationController.prototype.formatText=function(accountName,noOfChars,accountNumber,beginIndex){
      var truncatedAccName = "";
      var truncatedAccNum="";
      var formattedAccName ="";
      if (accountName && accountNumber && accountName.length > noOfChars) {
        truncatedAccName = accountName.substring(0, noOfChars - 1);
      } else {
        truncatedAccName = accountName;
      }
      if (accountNumber && accountNumber.length > beginIndex) {
        truncatedAccNum = accountNumber.substr(accountNumber.length - beginIndex);
      } else {
        if(accountNumber){
          truncatedAccNum = accountNumber;
        }
      }
      if(truncatedAccNum){
        formattedAccName = truncatedAccName + "..." + truncatedAccNum;
      }
      else{
        formattedAccName = truncatedAccName ;
      }
      return formattedAccName;
    };
  Profile_PresentationController.prototype.processViewFormattedData = function(data) {
    var processedData = {}
    for (var i = 0; i < data.length; i++) {
      if (!processedData.hasOwnProperty(data[i].accountType)) {
        processedData[data[i].accountType] = [];
      }
      if (processedData.hasOwnProperty(data[i].accountType)) {
        processedData[data[i].accountType].push(data[i]);
      }
    }
    processedData=this.orderByPriority(processedData);
    this.segregateAccountsByType(processedData);
     };
  
  Profile_PresentationController.prototype.segregateAccountsByType = function (viewBindData){
    var segData=[];
    if(viewBindData){
        for(var key in viewBindData){
          var sectionHeaderData={};
          var combinedData=[];
          if(key !== "CreditCard"){
            if (viewBindData[key].length > 1) {
              sectionHeaderData["lblHeader"] = key + " "+ kony.i18n.getLocalizedString("kony.i18n.approvalMatrix.accounts");
            } else {
              sectionHeaderData["lblHeader"] = key + " "+ kony.i18n.getLocalizedString("kony.18n.approvalMatrix.account");
            }
          }
          else{
            if (viewBindData[key].length > 1) {
              sectionHeaderData["lblHeader"] = kony.i18n.getLocalizedString("kony.18n.approvalMatrix.creditcards");
            } else {
              sectionHeaderData["lblHeader"] = kony.i18n.getLocalizedString("i18n.Accounts.backendCreditCard");
            }
          }
          var rowDataForSection=this.sortByPrefrence(viewBindData[key]);
          if(rowDataForSection.length>0){
            combinedData.push(sectionHeaderData);
            combinedData.push(rowDataForSection);
            segData.push(combinedData);
          }
        }
      }
    this.dataForSegment=segData;
    this.setSegmentDatainController(segData);
   };
  Profile_PresentationController.prototype.sortByPrefrence = function(accountsCollection) {
    if (accountsCollection.length > 1) accountsCollection.sort(function(record1, record2) {
      return record1.accountPreference - record2.accountPreference;
    });
    return accountsCollection;
  };
  Profile_PresentationController.prototype.orderByPriority = function(data) {
    var cm = applicationManager.getConfigurationManager();
    var prioritizedData = {};
    var metaData = cm.getAccountTypesMetaData();
    for (var key1 in metaData) {
      if (data[metaData[key1].backendValue]) {
        prioritizedData[metaData[key1].backendValue] = data[metaData[key1].backendValue];
      }
    }
    return prioritizedData;
  };
  Profile_PresentationController.prototype.setSegmentDatainController = function(segData) {
    var viewModel={
      "isSegData":true,
      "data":segData
    };
        applicationManager.getNavigationManager().updateForm(viewModel, "frmApprovalMatrixAccountLevel");
    };
  Profile_PresentationController.prototype.navigateToAccountLevel = function(contractDetails){
    applicationManager.getNavigationManager().navigateTo("frmApprovalMatrixAccountLevel");
    var viewModel={
      "isContractsPresent": true,
      "contractDetails" : contractDetails
    };    
    navigationArray.push(viewModel);
    applicationManager.getNavigationManager().updateForm(viewModel,"frmApprovalMatrixAccountLevel");
  };
  
  Profile_PresentationController.prototype.filterNameArray=function(array,searchKey) {
    return array.filter(item=> ((item.accountName)?item.accountName.toLowerCase().includes(searchKey):"") || (item.accountID?item.accountID.toLowerCase().includes(searchKey):"") || (item.processedName?item.processedName.toLowerCase().includes(searchKey):""));

  };
  Profile_PresentationController.prototype.searchSegmentDataforAccountLevel = function(searchKey){
    if(searchKey!==""){
      var segData=this.dataForSegment;
      var result=segData
      .map(data =>{
        const filteredData = this.filterNameArray(data[1],searchKey.toLowerCase());
        if(filteredData.length > 0){
          const tempArray = [];
          tempArray.push(data[0]);
          tempArray.push(filteredData);
          return tempArray;
        }
      })
      .filter(data => data !== (null || undefined ));
      this.setSegmentDatainController(result);
    }
    else{
      this.setSegmentDatainController(this.dataForSegment);
    }
  };
Profile_PresentationController.prototype.sortSegmentDataforAccountLevel = function(segmentData, sectionIndex, imgWidget, sortKey){
    var sortIcon = segmentData[sectionIndex][1][0][imgWidget].src;
    var rowData = segmentData[sectionIndex][1];
    var sortOrder = "desc";
    if(sortIcon === "sorting_previous.png"){
      sortOrder = "asc";
      segmentData[sectionIndex][1][0][imgWidget].src = "sorting_next.png";
    }
    else if(sortIcon === "sorting_next.png" || sortIcon === "sorting.png"){
      segmentData[sectionIndex][1][0][imgWidget].src = "sorting_previous.png";
    }
	rowData.sort(function(obj1, obj2){
      var order = (sortOrder === "desc") ? -1 : 1;
      if(obj1[sortKey] > obj2[sortKey]){
        return order;
      }
      else if(obj1[sortKey] < obj2[sortKey]){
        return -1 * order;
      }
      else{
        return 0;
      }
    });
	segmentData[sectionIndex][1] = rowData;
  };
    /**
     * Method to fetch user profile info and mapping to UI
     */
    Profile_PresentationController.prototype.showProfileSettings = function() {
        this.showUserProfile();
        this.initializeUserProfileClass();
        applicationManager.getNavigationManager().updateForm({
            "isLoading": true
        });
    };
    /**
     * Method to show secure access code settings
     */
    Profile_PresentationController.prototype.showSecureAccessSettings = function() {
        this.initializeUserProfileClass();
        var viewModel = {};
        viewModel.secureAccessSettings = true;
        applicationManager.getNavigationManager().updateForm(viewModel, 'frmProfileManagement');
    };
    /**
     * Method to show setting screen
     * @param {Object} viewModel - Data to be mapped at setting's screen
     */
    Profile_PresentationController.prototype.showSettingsScreen = function(viewModel) {
        applicationManager.getNavigationManager().updateForm(viewModel, "frmProfileManagement");
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
    };
    /**
     * Method to get External Account edit data from the post show of frmProfileSettings
     * @returns {JSON} External Account Edit Object
     */
    Profile_PresentationController.prototype.getEditExternalAccount = function() {
        if (this.editExternalAccountData) {
            var showEditExternalAccount = JSON.parse(JSON.stringify(this.editExternalAccountData));
            this.editExternalAccountData.flow = '';
            return showEditExternalAccount;
        }
        return null;
    }
    /**
     * Method to edit external account
     * @param {Object} data - JSON consisting account data
     */
    Profile_PresentationController.prototype.showEditExternalAccount = function(data) {
        this.editExternalAccountData = {
            flow: 'editExternalAccounts',
            data: data
        }
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
    };
    /**
     * Method used to attach phone numbers to accounts after updation of phone numbers.
     * @param {Object} phoneId - contains the communication id.
     * @param {Object} accountIds - contains the account ids.
     */
    Profile_PresentationController.prototype.attachPhoneNumberToAccounts = function(phoneId, accountIds) {
        var accountsArray = [];
        var i, params;
        if (accountIds.length === 0) {
            this.attachPhoneNumberToAccountsSuccess();
        } else {
            for (i = 0; i < accountIds.length; i++) {
                var accounts = {
                    "accountNumber": accountIds[i],
                    "phone": phoneId
                };
                accountsArray[i] = accounts;
            }
            accountsArray = JSON.stringify(accountsArray);
            accountsArray = accountsArray.replace(/"/g, "'");
            params = {
                "accountli": accountsArray
            };
        }
        applicationManager.getAccountManager().updateAccountPhoneNumber(params, this.attachPhoneNumberToAccountsSuccess.bind(this), this.attachPhoneNumberToAccountsFailure.bind(this));
    };
    /**
     * Method used as success call back for the attach phone numbers to accounts.
     *@param {Object} response - contains the service resonse.
     */
    Profile_PresentationController.prototype.attachPhoneNumberToAccountsSuccess = function(response) {
        //  applicationManager.getAccountManager().fetchInternalAccounts(function(){},function(){});
        this.fetchUser("ContactNumbers");
    };
    /**
     * Method used as failure call back for the attach phone numbers to accounts service.
     *@param {String} errorMessage - contains the error message for the service.
     */
    Profile_PresentationController.prototype.attachPhoneNumberToAccountsFailure = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            addPhoneViewModel: {
                serverError: errorMessage
            }
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    /**
     * Method used to delete Phone.
     * @param {Object} phoneObj - contains the phone Obj.
     */
    Profile_PresentationController.prototype.deletePhone = function(phoneObj) {
        this.showProgressBar();
        var params = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "deleteCommunicationID": phoneObj.id,
            "communicationType": "phoneNumbers"
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchUser.bind(this, "ContactNumbers"), this.deleteEmailFailure.bind(this));
    };
    /**
     *Method used to edit phone number in profile management.
     * @param {Object} id - contains the communication Id.
     * @param {Object} viewmodel - contains the viewModel.
     */
    Profile_PresentationController.prototype.editPhoneNumber = function(id, viewModel) {
        this.showProgressBar();
        var phoneNumbers = [{
            "id": id,
            "Extension": viewModel.Extension,
            "isPrimary": (viewModel.isPrimary === true) ? "1" : "0",
            "isAlertsRequired": (viewModel.isAlertsRequired === true) ? "1" : "0",
            "phoneNumber": viewModel.phoneNumber,
            "phoneCountryCode": viewModel.phoneCountryCode,
            "phoneExtension": viewModel.phoneExtension,
            "isTypeBusiness": viewModel.isTypeBusiness
        }];
        phoneNumbers = JSON.stringify(phoneNumbers);
        phoneNumbers = phoneNumbers.replace(/"/g, "'");
        var params = {
            "phoneNumbers": phoneNumbers,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        if(OLBConstants.IS_SCA_ENABLED){
            let action = SCAConfiguration.UPDATE_PHONE_NUMBER;
            SCAConfiguration.sendSCANotificationRequest(params, action, this.attachPhoneNumberToAccountsSuccess.bind(this),this.editPhoneNumberFailure.bind(this));
        } else {
            applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.attachPhoneNumberToAccountsSuccess.bind(this), /*this.attachPhoneNumberToAccounts.bind(this, viewModel.phoneNumber, viewModel.services),*/ this.editPhoneNumberFailure.bind(this));
        }
    };
    /**
     *Method used as failure call back for the edit phone number.
     *@param {String} errorMessage - contains the error message.
     */
    Profile_PresentationController.prototype.editPhoneNumberFailure = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            editPhoneViewModel: {
                serverError: errorMessage
            }
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    /**
     * Method used to get the edit phone view.
     * @param {Object} phoneObj - contains the phone Obj.
     */
    Profile_PresentationController.prototype.editPhoneView = function(phoneObj) {
        var accounts = applicationManager.getAccountManager().getInternalAccounts();
        /* var services = accounts.map(function(account) {
             return {
                 id: account.accountID,
                 name: CommonUtilities.getAccountDisplayName(account),
             };
         });*/
        var newPhoneModel = {
            phoneTypes: this.objectToListBoxArray(this.PhoneTypes),
            phoneTypeSelected: phoneObj.Extension,
            countryType: 'domestic',
            phoneNumber: '',
            phoneCountryCode: "",
            ext: '',
            isPrimary: false,
            isAlertsRequired: false,
            //    services: services,
            recievePromotions: false,
            isTypeBusiness: ''
        };
        newPhoneModel.Extension = phoneObj.Extension;
        newPhoneModel.value = phoneObj.Value;
        newPhoneModel.phoneNumber = phoneObj.phoneNumber;
        newPhoneModel.phoneCountryCode = phoneObj.phoneCountryCode;
        newPhoneModel.recievePromotions = phoneObj.receivePromotions === "1";
        newPhoneModel.isPrimary = phoneObj.isPrimary === "true";
        newPhoneModel.isAlertsRequired = phoneObj.isAlertsRequired === "true";
        newPhoneModel.isTypeBusiness = phoneObj.isTypeBusiness;
        /*  newPhoneModel.services = accounts.map(function(account) {
              return {
                  id: account.accountID,
                  name: CommonUtilities.getAccountDisplayName(account),
                  selected: account.phoneId === phoneObj.phoneNumber
              };
          });*/
        newPhoneModel.id = phoneObj.id;
        newPhoneModel.onBack = this.getPhoneDetails.bind(undefined, phoneObj);
        applicationManager.getNavigationManager().updateForm({
            isLoading: false,
            editPhoneViewModel: newPhoneModel
        }, "frmProfileManagement");
    };
    /**
     *Method used as success call back for save phone NUmber service.
     * @param {Object} viewModel - contains the context to be shown in the profile.
     * @para {Object} response - contains the service response.
     */
    Profile_PresentationController.prototype.savePhoneNumberSuccessCallBack = function(viewModel, response) {
        //this.attachPhoneNumberToAccounts(viewModel.phoneNumber);/*, viewModel.services);
        this.attachPhoneNumberToAccountsSuccess();
    };
    /**
     * Method used as failure call back for the save phone number.
     * @param {String} errorMessage - contains the error message of the service call.
     */
    Profile_PresentationController.prototype.savePhoneNumberFailureCallBack = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            addPhoneViewModel: {
                serverError: errorMessage
            }
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    /**
     * Method used to save phone Number in profile.
     *@param {Object} viewModel - contains the context to be shown in profile.
     */
    Profile_PresentationController.prototype.savePhoneNumber = function(viewModel) {
        this.showProgressBar();
        var phoneNumbers = [{
            "isPrimary": (viewModel.isPrimary === true) ? "1" : "0",
            "isAlertsRequired": (viewModel.isAlertsRequired === true) ? "1" : "0",
            "phoneNumber": viewModel.phoneNumber,
            "phoneCountryCode": viewModel.phoneCountryCode,
            "phoneExtension": viewModel.phoneExtension,
            "Extension": viewModel.type
        }];
        phoneNumbers = JSON.stringify(phoneNumbers);
        phoneNumbers = phoneNumbers.replace(/"/g, "'");
        var params = {
            "phoneNumbers": phoneNumbers,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        if(OLBConstants.IS_SCA_ENABLED){
            let action = SCAConfiguration.ADD_NEW_PHONE_NUMBER;
            SCAConfiguration.sendSCANotificationRequest(params, action, this.savePhoneNumberSuccessCallBack.bind(this, viewModel),this.savePhoneNumberFailureCallBack.bind(this));
        } else {
            applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.savePhoneNumberSuccessCallBack.bind(this, viewModel), this.savePhoneNumberFailureCallBack.bind(this));
        }
    };
    /**
     * Method used to show the user phone numbers in profile manangement.
     */
    Profile_PresentationController.prototype.showUserPhones = function() {
        var viewProperties = {
            phoneList: applicationManager.getUserPreferencesManager().getEntitlementPhoneNumbers(),
            isLoading: false
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    /**
     * Method used to show the add user phone number in profile manangement.
     */
    Profile_PresentationController.prototype.getAddPhoneNumberView = function() {
        var accounts = applicationManager.getAccountManager().getInternalAccounts();
        /*var services = accounts.map(function(account) {
            return {
                id: account.accountID,
                name: CommonUtilities.getAccountDisplayName(account),
            };
        });*/
        var phoneViewModel = {
            phoneTypes: this.objectToListBoxArray(this.PhoneTypes),
            phoneTypeSelected: "Mobile",
            countryType: 'domestic',
            phoneNumber: '',
            phoneCountryCode: '',
            phoneExtension: '',
            ext: '',
            isPrimary: false,
            isAlertsRequired: false,
            //    services: services,
            recievePromotions: false
        };
        applicationManager.getNavigationManager().updateForm({
            isLoading: false,
            addPhoneViewModel: phoneViewModel
        }, "frmProfileManagement");
    };
    /**
     * Method used to get phone details.
     * @param {Object} phoneObject - contains the phone object.
     */
    Profile_PresentationController.prototype.getPhoneDetails = function(phoneObject) {
        /*  var accounts = applicationManager.getAccountManager().getInternalAccounts();
          var services = accounts.map(function(account) {
              return {
                  id: account.accountID,
                  name: CommonUtilities.getAccountDisplayName(account),
                  selected: account.phoneId === phoneObject.phoneNumber
              };
          });*/
        applicationManager.getNavigationManager().updateForm({
            phoneDetails: {
                phone: phoneObject //,
                //   services: services
            },
            isLoading: false
        }, "frmProfileManagement");
    };
    /**
     * Method used to get list box array.
     * @param {Object} obj - contains the object containing the data to be used to construct the box array.
     */
    Profile_PresentationController.prototype.objectToListBoxArray = function(obj) {
        var list = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                list.push([key, obj[key]]);
            }
        }
        return list;
    };
    /**
     * Method used to fetch entitlements.
     */
    Profile_PresentationController.prototype.fetchUser = function(requiredView) {
        applicationManager.getUserPreferencesManager().fetchUser(this.fetchUserSuccess.bind(this, requiredView), this.fetchUserFailure.bind(this));
    };
    /**
     * Method used as the success call back for fetch entitlements service call.
     */
    Profile_PresentationController.prototype.fetchUserSuccess = function(requiredView, response) {
        var viewProperties;
        if (requiredView === "ContactNumbers") {
            viewProperties = {
                phoneList: response[0].ContactNumbers,
            };
        } else if (requiredView === "EmailIds") {
            viewProperties = {
                emailList: response[0].EmailIds
            };
        } else if (requiredView === "Addresses") {
            viewProperties = {
                addressList: response[0].Addresses
            };
        } else if (requiredView === "AlertCommunication") {
            viewProperties = {
                alertCommunication: response[0]
            };
        }
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    /**
     * Method used as failure call back for fetch entitlements service call.
     */
    Profile_PresentationController.prototype.fetchUserFailure = function(requiredView,errorMessage) {
        this.hideProgressBar();
        if (requiredView === "Addresses") {
            var viewProperties = {
                  isLoading: false,
                  addressError: errorMessage
              };
              applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
        }
        else{
            CommonUtilities.showServerDownScreen();
        }
       
    };
    /**
     * Method used as the failure call back for the save email service.
     * @param {String} errorMessage - contains the error message.
     */
    Profile_PresentationController.prototype.saveEmailFailureCallBack = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            emailError: errorMessage
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    /**
     * Method used to save email.
     * @param {Object} context - contains the email id, value, description, extension .
     */
    Profile_PresentationController.prototype.saveEmail = function(context) {
        var isCombinedUser = applicationManager.getConfigurationManager().getConfigurationValue('isCombinedUser') === "true";
        this.showProgressBar();

        function sameEmail(emailIds, emailAddress) {
            for (var i = 0; i < emailIds.length; i++) {
                var existingEmail = emailIds[i].Value;
                if (existingEmail.toUpperCase() === emailAddress.toUpperCase()) {
                    return true;
                }
            }
            return false;
        }
        var entitlementEmailIds = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
        var personalEmailIdCount = 0;
        var emailIds;
        var params;
        if (sameEmail(entitlementEmailIds, context.value)) {
            this.saveEmailFailureCallBack(kony.i18n.getLocalizedString("i18n.profile.emailAlreadyExists"));
        } else {
            if (isCombinedUser) {
                entitlementEmailIds.forEach(function(email) {
                    if (!email.isTypeBusiness)
                        personalEmailIdCount = personalEmailIdCount + 1;
                });
                if (personalEmailIdCount < 3) {
                    emailIds = [{
                        "isPrimary": (context.isPrimary === true) ? "1" : "0",
                        "isAlertsRequired": (context.isAlertsRequired === true) ? "1" : "0",
                        "value": context.value,
                        "Extension": "Personal",
                        "isTypeBusiness": context.isTypeBusiness
                    }];
                    emailIds = JSON.stringify(emailIds);
                    emailIds = emailIds.replace(/"/g, "'");
                    params = {
                        "EmailIds": emailIds,
                        "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
                        "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
                    };
                    if(OLBConstants.IS_SCA_ENABLED){
                        let action = SCAConfiguration.ADD_NEW_EMAIL;
                        SCAConfiguration.sendSCANotificationRequest(params, action, this.fetchUser.bind(this, "EmailIds"), this.saveEmailFailureCallBack.bind(this));
                    } else {
                        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchUser.bind(this, "EmailIds"), this.saveEmailFailureCallBack.bind(this));
                    }
                } else {
                    this.saveEmailFailureCallBack("We currently do not support adding more than three emails for a user");
                }
            } else {
                if (entitlementEmailIds.length < 3) {
                    emailIds = [{
                        "isPrimary": (context.isPrimary === true) ? "1" : "0",
                        "isAlertsRequired": (context.isAlertsRequired === true) ? "1" : "0",
                        "value": context.value,
                        "Extension": "Personal"
                    }];
                    emailIds = JSON.stringify(emailIds);
                    emailIds = emailIds.replace(/"/g, "'");
                    params = {
                        "EmailIds": emailIds,
                        "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
                        "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
                    };
                    if(OLBConstants.IS_SCA_ENABLED){
                        let action = SCAConfiguration.ADD_NEW_EMAIL;
                        SCAConfiguration.sendSCANotificationRequest(params, action, this.fetchUser.bind(this, "EmailIds"), this.saveEmailFailureCallBack.bind(this));
                    } else {
                        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchUser.bind(this, "EmailIds"), this.saveEmailFailureCallBack.bind(this));
                    }
                } else {
                    this.saveEmailFailureCallBack("We currently do not support adding more than three emails for a user");
                }
            }

        }
    };
    /**
     * Method used to show the user email view of profile management.
     */
    Profile_PresentationController.prototype.showUserEmail = function() {
        this.showProgressBar();
        var viewProperties = {
            emailList: applicationManager.getUserPreferencesManager().getEntitlementEmailIds(),
            isLoading: false
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    /**
     * Method used to delete email.
     * @param {Object} emailObj - contains the email object.
     */
    Profile_PresentationController.prototype.deleteEmail = function(emailObj) {
        this.showProgressBar();
        var params = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "deleteCommunicationID": emailObj.id,
            "communicationType": "EmailIds"
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchUser.bind(this, "EmailIds"), this.deleteEmailFailure.bind(this));
    };
    /**
     * Method used as the failure call back for delete email.
     * @param {String} errorMessage - contains the error message.
     */
    Profile_PresentationController.prototype.deleteEmailFailure = function(errorMessage) {
        //navigating to serverDown Screen incase of delete email failure.
        this.fetchUserFailure(errorMessage);
    };
    /**
     * Method used to show the progress bar in the profile management.
     */
    Profile_PresentationController.prototype.showProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "isLoading": true
        }, "frmProfileManagement");
    };
    /**
     * Method used to hide the progress bar in the profile management.
     */
    Profile_PresentationController.prototype.hideProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "isLoading": false
        }, "frmProfileManagement");
    };
    /**
     * Method used to edit email in profile management.
     * @param {Object} context - contains the email value, id, extension, isPrimary, description.
     */
    Profile_PresentationController.prototype.editEmail = function(Context) {
        this.showProgressBar();
        var emailIds = [{
            "id": Context.id,
            "Extension": Context.extension,
            "isPrimary": (Context.isPrimary === true) ? "1" : "0",
            "isAlertsRequired": (Context.isAlertsRequired === true) ? "1" : "0",
            "isTypeBusiness": Context.isTypeBusiness,
            "value": Context.email
        }];
        emailIds = JSON.stringify(emailIds);
        emailIds = emailIds.replace(/"/g, "'");
        var params = {
            "EmailIds": emailIds,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        if(OLBConstants.IS_SCA_ENABLED){
            let action = SCAConfiguration.UPDATE_EMAIL_ADDRESS;
            SCAConfiguration.sendSCANotificationRequest(params, action, this.fetchUser.bind(this, "EmailIds"),this.editEmailFailure.bind(this));
        } else {
            applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchUser.bind(this, "EmailIds"), this.editEmailFailure.bind(this));
        }
    };
    /**
     * Method used as the failure call back for the edit email service call.
     * @param {String} errorMessage - contains the error message for edit service.
     */
    Profile_PresentationController.prototype.editEmailFailure = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            editEmailError: errorMessage
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    /**
     * Method used to edit alert communications in profile management.
     * @param {Object} context - contains the email value, id, extension, isPrimary, description.
     */
    Profile_PresentationController.prototype.updateAlertCommunication = function(Context) {
        this.showProgressBar();
        var emailIds = Context.emailIds;
        emailIds = JSON.stringify(emailIds);
        emailIds = emailIds.replace(/"/g, "'");
        var phoneNumbers = Context.phoneNumbers;
        phoneNumbers = JSON.stringify(phoneNumbers);
        phoneNumbers = phoneNumbers.replace(/"/g, "'");
        var params = {
            "phoneNumbers": phoneNumbers,
            "EmailIds": emailIds,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchUser.bind(this, "AlertCommunication"), this.editEmailFailure.bind(this));
    };
    /**
     * Method used to show user addresses.
     */
    Profile_PresentationController.prototype.showUserAddresses = function() {
        applicationManager.getNavigationManager().updateForm({
            "addressList": applicationManager.getUserPreferencesManager().getEntitlementAddresses(),
            "isLoading": false
        }, "frmProfileManagement");
    };
    /**
     * Method used to delete user addresses.
     */
    Profile_PresentationController.prototype.deleteAddress = function(AddressObj) {
        this.showProgressBar();
        var params = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "deleteAddressID": AddressObj.Address_id
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchUser.bind(this, "Addresses"), this.deleteAddressFailure.bind(this));
    };
    /**
     * Method used as failure call back for the delete address.
     * @param {String} errorMessage - contains the service error message.
     */
    Profile_PresentationController.prototype.deleteAddressFailure = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            addressError: errorMessage
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    Profile_PresentationController.prototype.getAddNewAddressView = function() {
        this.showProgressBar();
        var asyncManager = applicationManager.getAsyncManager();
        var userPrefManager = applicationManager.getUserPreferencesManager();
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(userPrefManager, 'getCountryList'),
                asyncManager.asyncItem(userPrefManager, 'getStatesList'),
                asyncManager.asyncItem(userPrefManager, 'getCityList'),
            ],
            this.getAddNewAddressViewSuccess.bind(this)
        );
    };
    /**
     * Method used to get the add address view model.
     * @param {Object} countries - contains the countries list.
     * @param {Object} states - contains the states list.
     * @param {Object} Cities - contains the cities list.
     */
    Profile_PresentationController.prototype.AddAddressViewModel = function(countries, states, cities) {
        var countryNew = [];
        countryNew.push(["1", "Select a Country"]);
        var stateNew = [];
        stateNew.push(["lbl1", "Select a State"]);
        var cityNew = [];
        cityNew.push(["lbl2", "Select a City"]);
        return {
            serverError: null,
            addressTypes: this.objectToListBoxArray(this.AddressTypes),
            addressTypeSelected: "Work",
            addressLine1: '',
            addressLine2: '',
            countries: countries.map(function(country) {
                return countryNew.push([country.id, country.Name]);
            }),
            countryNew: countryNew,
            states: states.map(function(state) {
                return stateNew.push([state.id, state.Name, state.Country_id]);
            }),
            stateNew: stateNew,
            cities: cities.map(function(city) {
                return cityNew.push([city.id, city.Name, city.Region_id]);
            }),
            cityNew: cityNew,
            citySelected: cities[0].id,
            stateSelected: states[0].id,
            countrySelected: countries[0].id,
            city: '',
            isPreferredAddress: false,
            zipcode: '',
        };
    };
    /**
     * Method used as success call back for the address view model.
     * @param {Object} syncResponseObject - contains the async response object.
     */
    Profile_PresentationController.prototype.getAddNewAddressViewSuccess = function(syncResponseObject) {
        if (syncResponseObject.isAllSuccess()) {
            var viewModel = this.AddAddressViewModel(syncResponseObject.responses[0].data.records, syncResponseObject.responses[1].data.records, syncResponseObject.responses[2].data.records);
            viewModel.addressTypeSelected = "ADR_TYPE_WORK";
            viewModel.countrySelected = "1";
            viewModel.stateSelected = "lbl1";
            viewModel.citySelected = "lbl2";
            applicationManager.getNavigationManager().updateForm({
                isLoading: false,
                addNewAddress: viewModel
            }, "frmProfileManagement");
        } else {
            var viewProperties = {
                isLoading: false,
                addressError: errorMessage
            };
            applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
        }
        this.hideProgressBar();
    };
    /**
     * Method used to save address.
     * @param {Object} addressObj - contains the address Object.
     */
    Profile_PresentationController.prototype.saveAddress = function(addressObj) {
        this.showProgressBar();
        var addresses = [{
            Addr_type: addressObj.Addr_type,
            addrLine1: addressObj.addrLine1,
            addrLine2: addressObj.addrLine2,
            City_id: addressObj.citySelected,
            countryCode: addressObj.countrySelected,
            ZipCode: addressObj.zipcode,
            isPrimary: (addressObj.isPreferredAddress === true) ? "1" : "0",
            Region_id: addressObj.stateSelected
        }];
        addresses = JSON.stringify(addresses);
        addresses = addresses.replace(/"/g, "'");
        var params = {
            "Addresses": addresses,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchUser.bind(this, "Addresses"), this.fetchUserFailure.bind(this,"Addresses"));
    };
    /**
     * Method used to get edit address view.
     * @param{Object} address - contains the address object.
     */
    Profile_PresentationController.prototype.getEditAddressView = function(address) {
        this.showProgressBar();
        var asyncManager = applicationManager.getAsyncManager();
        var userPrefManager = applicationManager.getUserPreferencesManager();
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(userPrefManager, 'getCountryList'),
                asyncManager.asyncItem(userPrefManager, 'getStatesList'),
                asyncManager.asyncItem(userPrefManager, 'getCityList'),
            ],
            this.getEditAddressViewSuccess.bind(this, address)
        );
    };
    /**
     * Method used as sucess call bakc for the get edit address vie.
     * @param {Object} address - contains the address object.
     * @param {Object} syncRespnoseObject - contains the async response object.
     */
    Profile_PresentationController.prototype.getEditAddressViewSuccess = function(address, syncResponseObject) {
        if (syncResponseObject.isAllSuccess()) {
            var viewModel = this.AddAddressViewModel(syncResponseObject.responses[0].data.records, syncResponseObject.responses[1].data.records, syncResponseObject.responses[2].data.records);
            viewModel.addressId = address.Address_id;
            viewModel.addressLine1 = address.AddressLine1;
            viewModel.addressLine2 = address.AddressLine2;
            viewModel.addressTypeSelected = address.AddressType;
            viewModel.city = address.City_id;
            viewModel.isPreferredAddress = address.isPrimary === "true";
            viewModel.zipcode = address.ZipCode;
            viewModel.countrySelected = address.Country_id;
            viewModel.stateSelected = address.Region_id;
            viewModel.citySelected = address.City_id;
            viewModel.hidePrefferedAddress = address.isPrimary === "true";
            applicationManager.getNavigationManager().updateForm({
                isLoading: false,
                editAddress: viewModel
            }, "frmProfileManagement");
        } else {
            var viewProperties = {
                isLoading: false,
                addressEditError: errorMessage
            };
            applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
        }
        this.hideProgressBar();
    };
    /**
     * Method used to get the current user name.
     */
    Profile_PresentationController.prototype.getUserName = function() {
        return applicationManager.getUserPreferencesManager().getCurrentUserName();
    };
    /**
     * Method used to get the password rules.
     */
    Profile_PresentationController.prototype.getPasswordRules = function() {
        applicationManager.getUserPreferencesManager().getPasswordPolicies(this.getPasswordRulesSuccess.bind(this), this.getPasswordRulesFailure.bind(this));
    };
    /**
     * Method used as success call back for the password rules.
     * @param {Object} response - contains the service response for the password rules.
     */
    Profile_PresentationController.prototype.getPasswordRulesSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            usernamepasswordRules: response.records
        }, "frmProfileManagement");
    };
    /**
     * Method used as failure call back for the get password rules.
     */
    Profile_PresentationController.prototype.getPasswordRulesFailure = function() {
        CommonUtilities.showServerDownScreen();
    };
    Profile_PresentationController.prototype.getPasswordRulesAndPolicies = function() {
        applicationManager.getUserPreferencesManager().fetchPasswordRulesAndPolicy(this.getPasswordRulesAndPoliciesSuccess.bind(this), this.getPasswordRulesAndPoliciesFailure.bind(this));
    };
    /**
     * Method used as success call back for the password rules.
     * @param {Object} response - contains the service response for the password rules.
     */
    Profile_PresentationController.prototype.getPasswordRulesAndPoliciesSuccess = function(response) {
        var validationUtility = applicationManager.getValidationUtilManager();
        validationUtility.createRegexForPasswordValidation(response.passwordrules);
        applicationManager.getNavigationManager().updateForm({
            usernamepasswordRules: {
                "passwordpolicies": response.passwordpolicy
            }
        }, "frmProfileManagement");
    };
    /**
     * Method used as failure call back for the get password rules.
     */
    Profile_PresentationController.prototype.getPasswordRulesAndPoliciesFailure = function() {
        CommonUtilities.showServerDownScreen();
    };
    /**
     * Method to check exisitng password
     * @param {Object} viewModel - Password entered by user
     */
    Profile_PresentationController.prototype.checkExistingPassword = function(viewModel) {
        var password = viewModel;
        applicationManager.getUserPreferencesManager().checkExistingPassword(password, this.checkExistingPasswordSuccess.bind(this), this.checkExistingPasswordFailure.bind(this));
    };
    /**
     * Method to check exisitng password
     * @param {String} response - contains the existing password response.
     */
    Profile_PresentationController.prototype.checkExistingPasswordSuccess = function(response) {
        if (response.result === "The user is verified") {
            applicationManager.getNavigationManager().updateForm({
                showVerificationByChoice: response
            }, "frmProfileManagement");
        } else if (response.result === "Invalid Credentials") {
            applicationManager.getNavigationManager().updateForm({
                wrongPassword: "Wrong Password"
            }, "frmProfileManagement");
        } else {
            applicationManager.getNavigationManager().updateForm({
                passwordExists: "password exists"
            }, "frmProfileManagement");
        }
    };
    /**
     * Method to check exisitng password
     * @param {String} ErrorMessage - error message
     */
    Profile_PresentationController.prototype.checkExistingPasswordFailure = function(viewModel) {
        applicationManager.getNavigationManager().updateForm({
            passwordExistsServerError: "password exists"
        }, "frmProfileManagement");
    };
    /**
     * Method to request OTP
     */
    Profile_PresentationController.prototype.requestOtp = function(securityQuestionsPayload) {
        this.securityQuestionsPayload = securityQuestionsPayload;
        var mfaManager = applicationManager.getMFAManager();
        var params = {
            "MFAAttributes": {
                "serviceKey": mfaManager.getServicekey()
            }
        }
        mfaManager.requestUpdateSecurityQuestionsOTP(params, this.requestOtpSuccess.bind(this), this.requestOtpFailure.bind(this));
    };
    /**
     * Method to check exisitng password
     */
    Profile_PresentationController.prototype.requestOtpSuccess = function(response) {
        var mfaJSON = {
            "flowType": "SECURITYQUESTION_RESET",
            "response": response
        };
        applicationManager.getMFAManager().initMFAFlow(mfaJSON);
    };
    /**
     * Method used as failure call back for requestOTP service.
     */
    Profile_PresentationController.prototype.requestOtpFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            requestOtpError: response
        }, "frmProfileManagement");
    };
    /**
     * Method to verify otp
     * @param {String} viewModel - OTP
     */
    Profile_PresentationController.prototype.verifyOtp = function(viewModel) {
        var otpJSON = {
            "Otp": viewModel
        };
        applicationManager.getUserPreferencesManager().VerifySecureAccessCode(otpJSON, this.verifyOtpSuccess.bind(this), this.verifyOtpFailure.bind(this));
    };
    /**
     * Method used as success call back for verify otp.
     */
    Profile_PresentationController.prototype.verifyOtpSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            verifyOtp: response.isOtpVerified
        }, "frmProfileManagement");
    };
    /**
     * Method used as failure call back for verify otp.
     */
    Profile_PresentationController.prototype.verifyOtpFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            verifyOtpError: response.errorMessage
        }, "frmProfileManagement");
    };
    /**
     * Method to check existance of security questions
     */
    Profile_PresentationController.prototype.checkSecurityQuestions = function() {
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        var securityQuestionsStatus = applicationManager.getUserPreferencesManager().isSecurityQuestionsConfigured();
        if (securityQuestionsStatus) {
            applicationManager.getNavigationManager().updateForm({
                SecurityQuestionExists: "Questions Exist"
            }, "frmProfileManagement");
        } else {
            applicationManager.getNavigationManager().updateForm({
                SecurityQuestionExists: "Questions doesnt Exist"
            }, "frmProfileManagement");
        }
    };
    /**
     * Method to verify Security Question and answers
     * @param {JSON} data - JSON consisting of question and answers
     */
    Profile_PresentationController.prototype.verifyQuestionsAnswer = function(data) {
        data = JSON.stringify(data);
        data = data.replace(/"/g, "'");
        applicationManager.getUserPreferencesManager().verifySecurityQuestions(data, this.verifyQuestionsAnswerSuccess.bind(this), this.verifyQuestionsAnswerFailure.bind(this));
    };
    /**
     * Method used as failure call back for requestOTP service.
     */
    Profile_PresentationController.prototype.verifyQuestionsAnswerSuccess = function(response) {
        if (response.verifyStatus === 'true') {
            applicationManager.getNavigationManager().updateForm({
                verifyQuestion: "security questions"
            }, "frmProfileManagement");
        } else {
            applicationManager.getNavigationManager().updateForm({
                verifyQuestionAnswerError: "security questions"
            }, "frmProfileManagement");
        }
    };
    /**
     * Method used as failure call back for requestOTP service.
     */
    Profile_PresentationController.prototype.verifyQuestionsAnswerFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            verifyQuestionAnswerError: "security questions"
        }, "frmProfileManagement");
    };
    /**
     * Method to get Security questions to be answeres
     */
    Profile_PresentationController.prototype.getAnswerSecurityQuestions = function() {
        var scopeObj = this;
        var param = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
        };
        applicationManager.getUserPreferencesManager().fetchSecurityQuestions(param, this.getAnswerSecurityQuestionsSuccess.bind(this), this.getAnswerSecurityQuestionsFailure.bind(this));
    };
    /**
     * Method to get Security questions to be answeres
     */
    Profile_PresentationController.prototype.getAnswerSecurityQuestionsSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            answerSecurityQuestion: response.records
        }, "frmProfileManagement");
    };
    /**
     * Method to get Security questions to be answeres
     */
    Profile_PresentationController.prototype.getAnswerSecurityQuestionsFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            getAnswerSecurityQuestionError: response
        }, "frmProfileManagement");
    };
    /**
     * Method to change the answers of the security questions
     * @param {Object} data - questions and their answers
     */
    Profile_PresentationController.prototype.updateSecurityQuestions = function() {
        var data = this.securityQuestionsPayload;
        data = JSON.stringify(data);
        data = data.replace(/"/g, "'");
        var params = {
            "MFAAttributes": {
                "serviceKey": applicationManager.getMFAManager().getServicekey()
            },
            "securityQuestions": data,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        applicationManager.getUserPreferencesManager().resetSecurityQuestions(params, this.updateSecurityQuestionsSuccess.bind(this), this.updateSecurityQuestionsFailure.bind(this));
    };
    /**
     * Method to change the answers of the security questions
     * @param {Object} data - questions and their answers
     */
    Profile_PresentationController.prototype.updateSecurityQuestionsSuccess = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        applicationManager.getNavigationManager().updateForm({
            update: "security questions"
        }, "frmProfileManagement");
    };
    /**
     * Method to change the answers of the security questions
     * @param {Object} data - questions and their answers
     */
    Profile_PresentationController.prototype.updateSecurityQuestionsFailure = function(data) {
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        applicationManager.getNavigationManager().updateForm({
            updateSecurityQuestionError: "security questions"
        }, "frmProfileManagement");
    };
    /**
     * Method to fetch all the security questions
     * @param {Object} context - JSON to handle the security questions
     * @param {function} staticSetQuestions - function binded to handle the response
     */
    Profile_PresentationController.prototype.fetchSecurityQuestions = function(context, staticSetQuestions) {
        applicationManager.getUserPreferencesManager().fetchAllSecurityQuestions(this.fetchSecurityQuestionsSuccess.bind(this, context, staticSetQuestions), this.fetchSecurityQuestionsFailure.bind(this, context, staticSetQuestions));
    };
    /**
     * Method to fetch all the security questions
     * @param {Object} context - JSON to handle the security questions
     * @param {function} staticSetQuestions - function binded to handle the response
     */
    Profile_PresentationController.prototype.fetchSecurityQuestionsSuccess = function(context, staticSetQuestions, response) {
        var mfaManager = applicationManager.getMFAManager();
        mfaManager.setMFAFlowType("SECURITYQUESTION_RESET");
        mfaManager.setServicekey(response.serviceKey);
        var i = 0;
        while (i < response.records.length) {
            context.securityQuestions[i] = response.records[i].SecurityQuestion;
            context.flagToManipulate[i] = "false";
            i++;
        }
        staticSetQuestions(context, response.records);
    };
    /**
     * Method to fetch all the security questions
     * @param {Object} context - JSON to handle the security questions
     * @param {function} staticSetQuestions - function binded to handle the response
     */
    Profile_PresentationController.prototype.fetchSecurityQuestionsFailure = function(context, staticSetQuestions, response) {
        staticSetQuestions(context, response.records);
    };
    /**
     * Method to update secure access option
     * @param {Object} viewModel - Secure access data which needs to be updated
     */
    Profile_PresentationController.prototype.updateSecureAccessOptions = function(context) {
        applicationManager.getUserPreferencesManager().updateSecureAccessSettings(context, this.updateSecureAccessOptionsSuccess.bind(this), this.updateSecureAccessOptionsFailure.bind(this));
    };
    /**
     * Method used as success call back for updateSecureAccessOptions
     * @param {Object} resonse - service response for updateSecureAccessOptions.
     */
    Profile_PresentationController.prototype.updateSecureAccessOptionsSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            secureAccessOption: response
        }, "frmProfileManagement");
    };
    /**
     * Method method used as failure call back for updateSecureAccessOptions.
     * @param {Object} resonse - service response for updateSecureAccessOptions.
     */
    Profile_PresentationController.prototype.updateSecureAccessOptionsFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            secureAccessOptionError: response
        }, "frmProfileManagement");
    };
    /**
     * Method used to fetch alerts.
     * @param {Object} alertType - contains the type of alert to fetch.
     */
    Profile_PresentationController.prototype.fetchAlerts = function(alertType) {
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        this.showProgressBar();
        var self = this;
        var params = {
            userName: applicationManager.getUserPreferencesManager().getCurrentUserName(),
            alertTypeName: alertType
        };
        applicationManager.getAlertsManager().fetchProfileAlerts(params, this.fetchAlertsSuccess.bind(this), this.fetchAlertsFailure.bind(this));
    };
    /**
     * Method method used as failure call back for updateSecureAccessOptions.
     * @param {Object} resonse - service response for updateSecureAccessOptions.
     */
    Profile_PresentationController.prototype.fetchAlertsFailure = function(response) {
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            "AlertsError": response
        }, 'frmProfileManagement');
    };
    /**
     * updateAlerts- Method to update alerts and mapping to UI
     * @param {Object} response -  alerts list and alert type
     */
    Profile_PresentationController.prototype.updateAlerts = function(alertsData, responseData) {
        var self = this;
        this.showProgressBar();
        //         var params = {
        //             AlertCategoryId: alertsData.AlertCategoryId,
        //             isSubscribed: alertsData.isSubscribed,
        //             channelPreference: alertsData.channelPreference,
        //             typePreference: alertsData.typePreference
        //         };
        //         if(alertsData.AccountId) {
        //             params.AccountId = alertsData.AccountId;
        //         } else if(alertsData.AccountTypeId) {
        //             params.AccountTypeId = alertsData.AccountTypeId;
        //         }
        applicationManager.getAlertsManager().setAlertPreferences(alertsData, this.updateAlertsSuccess.bind(this, responseData), this.updateAlertsFailure.bind(this));
    };
    /**
     * updateAlertsSuccess- Method to update alerts and mapping to UI
     * @param {Object} response -  alerts list and alert type
     */
    Profile_PresentationController.prototype.updateAlertsSuccess = function(responseData) {
        applicationManager.getNavigationManager().updateForm({
            "alertsSaved": responseData
        }, 'frmProfileManagement');
        this.hideProgressBar();
    };
    /**
     * updateAlerts- Method to update alerts and mapping to UI
     * @param {Object} response -  contains the error response.
     */
    Profile_PresentationController.prototype.updateAlertsFailure = function(response) {
        this.hideProgressBar();
        CommonUtilities.showServerDownScreen();
    };
    /**
     * Method to get user emails.
     */
    Profile_PresentationController.prototype.getUserEmail = function() {
        applicationManager.getNavigationManager().updateForm({
            "emails": userObjectToEmailList(applicationManager.getUserPreferencesManager().getEntitlementEmailIds())
        }, 'frmProfileManagement');
    };
    /**
     * Method used to fetch the profile
     */
    Profile_PresentationController.prototype.showUserProfile = function() {
        applicationManager.getUserPreferencesManager().fetchUserProfile(this.showUserProfileSuccess.bind(this), this.showUserProfileFailure.bind(this));
    };
    /**
     * Method used to fetch the profile
     */
    Profile_PresentationController.prototype.showUserProfileSuccess = function(response) {
        response[0].userImageURL = applicationManager.getUserPreferencesManager().getUserImage();
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        if (!kony.sdk.isNullOrUndefined(response[0].ContactNumbers)) {
        	for (i=0; i<response[0].ContactNumbers.length; i++){
          		if (!kony.sdk.isNullOrUndefined(response[0].ContactNumbers[i].Extension)){
                  response[0].ContactNumbers[i].Extension = "";
                }
     		}
        }
        applicationManager.getNavigationManager().updateForm({
            "userProfile": response[0],
            "isLoading": false
        }, 'frmProfileManagement');
    };
    /**
     * Method used to fetch the profile
     */
    Profile_PresentationController.prototype.showUserProfileFailure = function(response) {
        CommonUtilities.showServerDownScreen();
    };
    /**
     * Method used to fetch the consent management
     */
    Profile_PresentationController.prototype.showConsentManagement = function() {
        applicationManager.getNavigationManager().updateForm({
            isLoading: true
        }, 'frmProfileManagement');
        applicationManager.getSettingsManager().getConsentData(this.showConsentManagementSuccess.bind(this), this.showConsentManagementFailure.bind(this));
    };
    /**
     * Method used to fetch the profile
     */
    Profile_PresentationController.prototype.showConsentManagementSuccess = function(response) {
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            "consentResponse": response,
            "isLoading": false
        }, 'frmProfileManagement');

    };
    /**
     * Method used to fetch the profile
     */
    Profile_PresentationController.prototype.showConsentManagementFailure = function(response) {
        //CommonUtilities.showServerDownScreen();
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            "consentError": response.errorMessage,
            "action": "get"
        }, 'frmProfileManagement');
    };
    /**
     * Method used to fetch the manage account access
     */
    Profile_PresentationController.prototype.showManageAccountAccess = function() {
        applicationManager.getNavigationManager().updateForm({
            "isLoading": true
        }, 'frmProfileManagement');
        applicationManager.getSettingsManager().getPSD2ConsentData(this.showManageAccountAccessSuccess.bind(this), this.showManageAccountAccessFailure.bind(this));
    };
  
  Profile_PresentationController.prototype.showManageAccountAccessSuccess = function(response) {
    applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
    if (response.consentTypes){
      applicationManager.getNavigationManager().updateForm({
        "manageAccountAccessResponse": response.consentTypes,
        "isLoading": false
      }, 'frmProfileManagement');
    }else{
      applicationManager.getNavigationManager().updateForm({
        "managePSD2Error": kony.i18n.getLocalizedString("i18n.ProfileManagement.consent.psd2.noConsents"),
        "action": "get",
        "isLoading": false
      }, 'frmProfileManagement');
    }

  };
  
    Profile_PresentationController.prototype.showManageAccountAccessFailure = function(response) {
        // CommonUtilities.showServerDownScreen();
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            "managePSD2Error": response.errorMessage,
            "action": "get",
            "isLoading": false
        }, 'frmProfileManagement');
    };

    /**
     * Method used to fetch the profile Image
     */
    Profile_PresentationController.prototype.showUserProfileImage = function() {
        applicationManager.getUserPreferencesManager().fetchUserImage(this.showUserProfileImageSuccess.bind(this), this.showUserProfileImageFailure.bind(this));
    };
    /**
     * Method used to fetch the profile Image
     */
    Profile_PresentationController.prototype.showUserProfileImageSuccess = function(response) {
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        var formController = applicationManager.getPresentationUtility().getController('frmProfileManagement', true);
        formController.bindUploadedImage(response.UserImage);
        this.hideProgressBar();
    };
    /**
     * Method used to fetch the profile Image
     */
    Profile_PresentationController.prototype.showUserProfileImageFailure = function(response) {
        this.hideProgressBar();
        CommonUtilities.showServerDownScreen();
    };

    /**
     * Method to update the password
     * @param {Object} password - contains the password.
     */
    Profile_PresentationController.prototype.updatePassword = function(oldPassword, newPassword) {
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var params = {
            "oldPassword": oldPassword,
            "newPassword": newPassword
        };
        applicationManager.getUserPreferencesManager().updateUserPassword(params, this.updatePasswordSuccess.bind(this), this.updatePasswordFailure.bind(this));
    };
    /**
     * Method to update the password
     * @param {Object} response - contains the response from the service.
     */
    Profile_PresentationController.prototype.updatePasswordSuccess = function(response) {
        var mfaManager = applicationManager.getMFAManager();
        if (response && response.MFAAttributes) {
            if (response.MFAAttributes.isMFARequired == "true") {
                var mfaJSON = {
                    "flowType": "UPDATE_PASSWORD",
                    "response": response
                };
                applicationManager.getMFAManager().initMFAFlow(mfaJSON);
            }
        } else {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.onPasswordChange();
        }
    };
    /**
     * Method used as the failure call back for update password.
     * @param {String} viewModel - Data required to update password
     */
    Profile_PresentationController.prototype.updatePasswordFailure = function(response) {
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            passwordServerError: response.serverErrorRes ? response.serverErrorRes : response
        }, "frmProfileManagement");
    };
    /**
     * Method to Check existing secure access option
     */
    Profile_PresentationController.prototype.checkSecureAccess = function() {
        applicationManager.getUserPreferencesManager().getSecureAccessCodeOptions(this.checkSecureAccessSuccess.bind(this), this.checkSecureAccessFailure.bind(this));
    };
    /**
     * Method to Check existing secure access option
     */
    Profile_PresentationController.prototype.checkSecureAccessSuccess = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        applicationManager.getNavigationManager().updateForm({
            securityAccess: response[0]
        }, "frmProfileManagement");
    };
    /**
     * Method to Check existing secure access option
     */
    Profile_PresentationController.prototype.checkSecureAccessFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            checkSecurityAccessError: response
        }, "frmProfileManagement");
    };
    /**
     * Method to show setting screen
     * @param {String} userName - Contains the userName.
     */
    Profile_PresentationController.prototype.updateUsername = function(userName) {
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var params = {
            "newUserName": userName,
            "oldUserName": userPreferencesManager.getCurrentUserName()
        };
        this.newUserName = userName;
        applicationManager.getUserPreferencesManager().updateUserName(params, this.updateUsernameSuccess.bind(this), this.updateUsernameFailure.bind(this));
    };
    /**
     * Method used as success call back for update username
     * @param {Object} response - contains the response .
     */
    Profile_PresentationController.prototype.updateUsernameSuccess = function(response) {
        var mfaManager = applicationManager.getMFAManager();
        var userpreferencesManager = applicationManager.getUserPreferencesManager();
        if (response && response.MFAAttributes) {
            if (response.MFAAttributes.isMFARequired == "true") {
                var mfaJSON = {
                    "flowType": "UPDATE_USERNAME",
                    "response": response,
                    "userName": this.newUserName
                };
                applicationManager.getMFAManager().initMFAFlow(mfaJSON);
            }
        } else {
            userpreferencesManager.setCurrentUserName(this.newUserName);
            userpreferencesManager.saveUserName(this.newUserName);
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.onUsernameChange();
        }
    };
    /**
     * Method used as failure call back for update user name
     * @param {Object} response - contains the response for update user name.
     */
    Profile_PresentationController.prototype.updateUsernameFailure = function(response) {
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            updateUsernameError: response
        }, "frmProfileManagement");
    };
    /**
     * Method used to update address.
     *@param {Object} context - contains the user address Object.
     */
    Profile_PresentationController.prototype.updateAddress = function(context) {
        var addresses = [{
                "Addr_type": context.Addr_type,
                "isPrimary": (context.isPrimary === true) ? "1" : "0",
                "addrLine1": context.addrLine1,
                "addrLine2": context.addrLine2,
                "ZipCode": context.ZipCode,
                "City_id": context.City_id,
                "Addr_id": context.addressId,
                "countryCode":context.countrySelected,
                "Region_id": context.Region_id
            }],
            addresses = JSON.stringify(addresses);
        addresses = addresses.replace(/"/g, "'");
        var params = {
            "Addresses": addresses,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchUser.bind(this, "Addresses"), this.fetchUserFailure.bind(this, "Addresses"));
    };

    /**
     * Method used to save the user profile pic.
     *@ param {String} base64String - contains the base 64 string.
     */
    Profile_PresentationController.prototype.userImageUpdate = function(base64String) {
        var params = {
            "UserImage": base64String
        };
        applicationManager.getUserPreferencesManager().updateUserProfileImage(params, this.showUserProfileImage.bind(this), this.fetchUserFailure.bind(this));
    };
    /**
     * Method used to Delete the user profile pic.
     *@ param {String} base64String - contains the base 64 string.
     */
    Profile_PresentationController.prototype.userImageDelete = function() {
        var params = {}
        applicationManager.getUserPreferencesManager().deleteUserProfileImage(params, this.showUserProfileImage.bind(this), this.fetchUserFailure.bind(this));
    };
    /******************************************Ankit Refactored******************************************
     * ***************************************************************************************************
     * ***************************************************************************************************
     */
    /**
     * Method used to show the user email view of profile management.
     */
    Profile_PresentationController.prototype.getUserEmail = function() {
        this.showProgressBar();
        var viewProperties = {
            emails: applicationManager.getUserPreferencesManager().getEntitlementEmailIds(),
            isLoading: false
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };
    /**
     *Method to navigate frmProfileManagement and show account preference page where accounts are sorted as per their preference order
     */
    Profile_PresentationController.prototype.showPreferredAccounts = function() {
        this.getPreferredAccounts();
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            showPrefferedView: true,
            isLoading: true
        }, 'frmProfileManagement');
    }
    /**
     * Method to show account preference page where accounts are sorted as per their preference order
     *
     */
    Profile_PresentationController.prototype.getPreferredAccounts = function() {
        applicationManager.getNavigationManager().updateForm({
            isLoading: true
        }, 'frmProfileManagement');
        var self = this;
        /* Commented this line as per the bug number ARB-11106. Removed the aggregate accounts call temperorily
        if (applicationManager.getConfigurationManager().getConfigurationValue("isAggregatedAccountsEnabled") === "true") {
            function completionCallback(asyncResponse) {
                if (asyncResponse.isAllSuccess()) {
                    self.fetchAccountsSuccess(asyncResponse.responses);
                } else {
                    self.fetchAccountsFailure();
                }
            }
            var username = applicationManager.getUserPreferencesManager().getUserObj().userName;
            var asyncManager = applicationManager.getAsyncManager();
            asyncManager.callAsync([
                asyncManager.asyncItem(
                    applicationManager.getAccountManager(),
                    "fetchInternalAccounts"
                ),
                asyncManager.asyncItem(
                    applicationManager.getAccountManager(),
                    "fetchExternalAccountsData", [username]
                )
            ], completionCallback);
        } else {
            applicationManager.getAccountManager().fetchInternalAccounts(this.fetchAccountsSuccess.bind(this), this.fetchAccountsFailure.bind(this));
        }
        */
        applicationManager.getAccountManager().fetchInternalAccounts(this.fetchAccountsSuccess.bind(this), this.fetchAccountsFailure.bind(this));
    }
    /**
     *Method is invoked when fetching of accounts is successful. It covers both scenarios of internal and external accounts
     * @param {Collection} accounts list of all the accounts i.e internal account and external accounts based on aggregatedAccount flag of configurations
     */
    Profile_PresentationController.prototype.fetchAccountsSuccess = function(accounts) {
        var finalAccounts = [],
            self = this;
        /* Commented this line as per the bug number ARB-11106. Removed the aggregate accounts call temperorily
        if (applicationManager.getConfigurationManager().getConfigurationValue("isAggregatedAccountsEnabled") === "true") {
            accounts[0].data.forEach(function(internalAccount) {
                finalAccounts.push(internalAccount);
            });
            accounts[1].data.forEach(function(externalAccount) {
                finalAccounts.push(self.processExternalAccountsData(externalAccount));
            });
        } else {
            finalAccounts = accounts;
        }*/
        finalAccounts = accounts;
        applicationManager.getNavigationManager().updateForm({
            getPreferredAccountsList: finalAccounts
        }, 'frmProfileManagement');
    };
    /**
     *  Process external Account Data
     * @param {Collection} rawData External accounts JSON
     * @returns Collection of altered data
     */
    Profile_PresentationController.prototype.processExternalAccountsData = function(rawData) {
        var isError = function(error) {
            try {
                if (error && error.trim() !== "") {
                    return true;
                }
                return false;
            } catch (error) {}
        };
        var account = {};
        account.accountName = rawData.AccountName;
        account.nickName = rawData.NickName;
        account.accountID = rawData.Number;
        account.accountType = rawData.TypeDescription;
        account.availableBalance = rawData.AvailableBalance;
        account.currentBalance = rawData.AvailableBalance;
        account.outstandingBalance = rawData.AvailableBalance;
        account.availableBalanceUpdatedAt = (rawData.LastUpdated) ? (CommonUtilities.getTimeDiferenceOfDate(rawData.LastUpdated)) : kony.i18n.getLocalizedString('i18n.AcountsAggregation.timeDifference.justNow');
        if (String(rawData.FavouriteStatus).trim().toLowerCase() === "true") {
            account.favouriteStatus = "1";
        } else {
            account.favouriteStatus = "0";
        }
        account.bankLogo = rawData.BankLogo;
        account.isError = isError(rawData.error);
        account.israwData = true;
        account.bankName = rawData.BankName;
        account.userName = (rawData.Username) ? rawData.Username : rawData.username;
        account.bankId = rawData.Bank_id;
        account.externalAccountId = rawData.Account_id;
        account.accountHolder = rawData.AccountHolder;
        account.isExternalAccount = true;
        return account;
    };
    /**
     *Method that gets called in case fetching of accounts fails
     */
    Profile_PresentationController.prototype.fetchAccountsFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            "getPreferredAccountsList": {
                "errorCase": true
            }
        }, 'frmProfileManagement');
    };
    /**
     * Method to save preferred account data
     * @param {JSON} data -Json containing account info like favouriteStatus, NickName, E-statements etc
     */
    Profile_PresentationController.prototype.savePreferredAccountsData = function(data) {
        if (data.external) {
            var newdata = {
                "NickName": data.NickName,
                "FavouriteStatus": data.FavouriteStatus,
                "Account_id": data.Account_id,
            }
            applicationManager.getAccountManager().updateExternalAccountFavouriteStatus(newdata, this.updateAccountPreferenceSuccess.bind(this), this.updateAccountPreferenceFailure.bind(this));
        } else {
            applicationManager.getAccountManager().updateUserAccountSettingsForEstatements(data, this.updateAccountPreferenceSuccess.bind(this), this.updateAccountPreferenceFailure.bind(this));
        }
    };
    /**
     * Method that gets called on success of saving account preferences
     */
    Profile_PresentationController.prototype.updateAccountPreferenceSuccess = function() {
        this.getPreferredAccounts();
    };
    /**
     * Method that gets called in case of saving account preferences failure
     */
    Profile_PresentationController.prototype.updateAccountPreferenceFailure = function() {
        this.hideProgressBar();
        applicationManager.getNavigationManager().updateForm({
            errorEditPrefferedAccounts: true
        }, 'frmProfileManagement');
    };
    /**
     * * Method to change the preference Number of the accounts
     * @param {JSON} updatedAccounts Accounts with account number and their preference number
     */
    Profile_PresentationController.prototype.setAccountsPreference = function(updatedAccounts) {
        var data = JSON.stringify(updatedAccounts);
        data = data.replace(/"/g, "'");
        var final = {};
        final['accountli'] = data;
        applicationManager.getAccountManager().setAccountsPreference(final, this.getPreferredAccounts.bind(this), this.getPreferredAccounts.bind(this));
    };
    /**
     * Method to get default user accounts from user object
     */
    Profile_PresentationController.prototype.getDefaultUserProfile = function() {
        var userObj = applicationManager.getUserPreferencesManager().getUserObj();
        var data = {
            defaultTransferAccount: userObj['default_account_transfers'],
            defaultBillPayAccount: userObj['default_account_billPay'],
            defaultP2PAccount: userObj['default_from_account_p2p'],
            defaultCheckDepositAccount: userObj['default_account_deposit']
        };
        this.getDefaultAccountNames(data);
    };
    /**
     * Method to get Default account name for the default user accounts
     * @param {JSON} accountNumbers Default account numbers of logged in user
     */
    Profile_PresentationController.prototype.getDefaultAccountNames = function(accountNumbers) {
        var defaultNames = [];
        this.defaultAccounts = accountNumbers;
        var accounts = applicationManager.getAccountManager().getInternalAccounts()
        for (var keys in accountNumbers) {
            if (accountNumbers[keys] && accountNumbers[keys] !== "-1") {
                for (var i in accounts) {
                    if (accountNumbers[keys] === accounts[i].accountID) {
                        defaultNames[keys] = accounts[i].accountName;
                        break;
                    }
                }
            } else {
                defaultNames[keys] = "None";
            }
        }
        applicationManager.getNavigationManager().updateForm({
            "showDefautUserAccounts": defaultNames
        }, 'frmProfileManagement');
    };
    /**
     * Method to get List of accounts for selecting default accounts for different transaction types
     */
    Profile_PresentationController.prototype.getAccountsList = function() {
        var defaultAccounts = this.defaultAccounts;
        var getDefaultSelectedKey = function(data) {
            if (data && data !== "-1") {
                return data;
            } else {
                return 'undefined';
            }
        };
        var getAccountsListViewModel = {};
        getAccountsListViewModel['TransfersAccounts'] = applicationManager.getAccountManager().getFromTransferSupportedAccounts();
        getAccountsListViewModel['defaultTransfersAccounts'] = getDefaultSelectedKey(defaultAccounts.defaultTransferAccount);
        getAccountsListViewModel['BillPayAccounts'] = applicationManager.getAccountManager().getBillPaySupportedAccounts();
        getAccountsListViewModel['defaultBillPayAccounts'] = getDefaultSelectedKey(defaultAccounts.defaultBillPayAccount);
        getAccountsListViewModel['P2PAccounts'] = applicationManager.getAccountManager().getFromTransferSupportedAccounts();
        getAccountsListViewModel['defaultP2PAccounts'] = getDefaultSelectedKey(defaultAccounts.defaultP2PAccount);
        getAccountsListViewModel['CheckDepositAccounts'] = applicationManager.getAccountManager().getDepositSupportedAccounts();
        getAccountsListViewModel['defaultCheckDepositAccounts'] = getDefaultSelectedKey(defaultAccounts.defaultCheckDepositAccount);
        applicationManager.getNavigationManager().updateForm({
            "getAccountsList": getAccountsListViewModel
        }, 'frmProfileManagement');
    };
    /**
     * Method to save default accounts for different type of transactions
     * @param {JSON} defaultAccounts Accounts with default account numbers
     */
    Profile_PresentationController.prototype.saveDefaultAccounts = function(defaultAccounts) {
        applicationManager.getUserPreferencesManager().updateUserDetails(defaultAccounts, this.saveDefaultAccountsSuccess.bind(this), this.saveDefaultAccountsFailure.bind(this));
    };
    /**
     * Method that gets called when saving default accounts is successful
     */
    Profile_PresentationController.prototype.saveDefaultAccountsSuccess = function() {
        applicationManager.getUserPreferencesManager().fetchUser(this.getDefaultUserProfile.bind(this), this.saveDefaultAccountsFailure.bind(this))
    }
    /**
     * Method that gets called when saving default accounts is failed
     */
    Profile_PresentationController.prototype.saveDefaultAccountsFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            "showDefautUserAccounts": {
                errorCase: true
            }
        }, 'frmProfileManagement');
    }
    /**
     * Method to save external account data
     * @member of Profile_PresentationController
     * @param {JSON} data - accounts data
     * @returns {void} - None
     * @throws {void} -None
     */
    Profile_PresentationController.prototype.saveExternalAccountsData = function(data) {
        var newdata = {
            "NickName": data.NickName,
            "FavouriteStatus": data.FavouriteStatus,
            "Account_id": data.Account_id,
        }
        applicationManager.getAccountManager().updateExternalAccountFavouriteStatus(newdata, this.saveExternalAccountsDataSuccess.bind(this), this.saveExternalAccountsDataFailure.bind(this));
    };
    Profile_PresentationController.prototype.saveExternalAccountsDataSuccess = function() {
        applicationManager.getNavigationManager().updateForm({
            errorSaveExternalAccounts: {
                error: false
            }
        });
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountsModule.presentationController.showAccountsDashboard();
    }
    Profile_PresentationController.prototype.saveExternalAccountsDataFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            errorSaveExternalAccounts: true
        });
    }
    /**
     * Method to to fetch acoount alerts data.
     */
    Profile_PresentationController.prototype.fetchAccountAlerts = function(alertID) {
        var scopeObj = this;
        applicationManager.getNavigationManager().updateForm({
            isLoading: true
        }, 'frmProfileManagement');
        applicationManager.getAlertsManager().getCustomerAccountAlertPreference(this.fetchAccountAlertsSuccess.bind(this, alertID), this.fetchAccountAlertsFailure.bind(this));
    }
    /**Success callback for fetch account alerts
     * @param {Object} response - contains the service response for account type Alerts.
     */
    Profile_PresentationController.prototype.fetchAccountAlertsSuccess = function(alertID, response) {
        applicationManager.getNavigationManager().updateForm({
            isLoading: false,
            accountAlertsData: {
                "accountAlerts": response.accountInfo,
                "alertID": alertID
            }
        }, 'frmProfileManagement');
    }
    /**failure callback for fetch account alerts
     */
    Profile_PresentationController.prototype.fetchAccountAlertsFailure = function() {
        this.fetchUserFailure(); // basically, sever down screen function
    }
    /**
     * Method to to fetch alerts data and channels basing on category to be shown.
     * @param {String} alertID - ID of the Alert Type
     */
    Profile_PresentationController.prototype.fetchAlertsDataById = function(params) {
        var scopeObj = this;
        applicationManager.getNavigationManager().updateForm({
            isLoading: true
        }, 'frmProfileManagement');

        function completionCallback(asyncResponse) {
            if (asyncResponse.isAllSuccess()) {
                scopeObj.fetchAlertsSuccess(asyncResponse.responses, params);
            } else {
                scopeObj.fetchUserFailure(); // basically, sever down screen function
            }
        }
        var asyncManager = applicationManager.getAsyncManager();
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(applicationManager.getAlertsManager(), "fetchAlertsInCategory", [params]),
            ], completionCallback);
    };
    /**Success callback for fetch alerts by id
     * @param {Object} response - contains the service response for the Alerts channel and category.
     * @param {String} AlertId - ID of the Alert
     */
    Profile_PresentationController.prototype.fetchAlertsSuccess = function(responses, params) {
        var alertsData = {
            "alertsData": responses[0].data,
            "isAlertTypeSelected": responses[0].data.categorySubscription.isSubscribed,
            "isInitialLoad": responses[0].data.categorySubscription.isInitialLoad,
            "AlertCategoryId": params.AlertCategoryId,
            "accountId": params.AccountId,
            "accountTypeId": params.AccountTypeId
        };
        applicationManager.getNavigationManager().updateForm({
            AlertsDataById: alertsData
        }, 'frmProfileManagement');
    };
    /**
     * Method to to fetch the type of customer alert category to be shown.
     * @param {String} entryPoint - Dicides which flow to be called
     */
    Profile_PresentationController.prototype.fetchAlertsCategory = function(entryPoint, accountID) {
        var self = this;
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        this.showProgressBar();
        var asyncManager = applicationManager.getAsyncManager();
        var servicesToCallInAsync = [
            asyncManager.asyncItem(applicationManager.getAlertsManager(), 'fetchAlertsCategory'),
            asyncManager.asyncItem(applicationManager.getTermsAndConditionManager(), 'fetchTermsAndConditionsPostLogin', [{
                "languageCode": kony.i18n.getCurrentLocale().replace("_", "-"),
                "termsAndConditionsCode": OLBConstants.TNC_FLOW_TYPES.Estatements_TnC
            }])
            //service call to fetch approval rules to be added once contract is ready
        ];

//         if (applicationManager.getConfigurationManager().checkUserPermission("APPROVAL_MATRIX_VIEW")) {
//             servicesToCallInAsync.push(asyncManager.asyncItem(applicationManager.getBusinessUserManager(), 'fetchAllAccountsForOrganization'));
//             servicesToCallInAsync.push(asyncManager.asyncItem(applicationManager.getBusinessUserManager(), 'fetchApprovalRules'));
//         }
        asyncManager.callAsync(servicesToCallInAsync,
            self.onFetchCallComplete.bind(self, entryPoint, accountID)
        );
    };
    Profile_PresentationController.prototype.onFetchCallComplete = function(entryPoint, accountID, syncResponseObject) {
        var scopeObj = this;
        var approvalMatrixEntitlement = applicationManager.getConfigurationManager().checkUserPermission("APPROVAL_MATRIX_VIEW");
        var allServicesPassed;
//         if (approvalMatrixEntitlement) {
//             allServicesPassed = syncResponseObject.responses[0].isSuccess && syncResponseObject.responses[1].isSuccess &&
//                 syncResponseObject.responses[2].isSuccess && syncResponseObject.responses[3].isSuccess;
//         } else {
            allServicesPassed = syncResponseObject.responses[0].isSuccess && syncResponseObject.responses[1].isSuccess;
//         }
        if (allServicesPassed) {
//             if (approvalMatrixEntitlement) {
//                 scopeObj.fetchAlertsCategorySuccess(entryPoint, syncResponseObject.responses[0].data, syncResponseObject.responses[2].data.contractCustomers[0].accounts, accountID);
//                 scopeObj.setApprovalRulesForOrganization(syncResponseObject.responses[3].data);
//             } else {
                scopeObj.fetchAlertsCategorySuccess(entryPoint, syncResponseObject.responses[0].data, null, accountID);
//             }
            scopeObj.getTnCOnSuccess(syncResponseObject.responses[1].data);
        } else {
            applicationManager.getNavigationManager().updateForm({
                "isLoading": false
            });
            CommonUtilities.showServerDownScreen();
        }

    };

    Profile_PresentationController.prototype.setApprovalRulesForOrganization = function(approvalRules) {
        var rules = [];

        for (var i = 0; i < approvalRules.length; i++) {
            var rule = {
                "ruleId": approvalRules[i]["id"],
                "ruleName": approvalRules[i]["name"],
                "numberOfApprovals": approvalRules[i]["numberOfApprovals"]
            }
            rules.push(rule);
        }
        CommonUtilities.setApprovalRules(rules);
    };

    Profile_PresentationController.prototype.getTnCOnSuccess = function(TnCresponse) {
        applicationManager.getNavigationManager().updateForm({
            "isLoading": false,
            "TnCcontent": TnCresponse
        });
    };
  
    Profile_PresentationController.prototype.showDeviceManagementScreen = function(TnCresponse) {
      applicationManager.getNavigationManager().updateForm({
        "isLoading": false,
        "showRegisteredDevices": true
      });
    };
  
    /**
     * Success callback for customer alert category to be shown.
     * @param {String} entryPoint - Dicides which flow to be called
     * @param {Object} response - contains the service response for the Alerts Category.
     */
    Profile_PresentationController.prototype.fetchAlertsCategorySuccess = function(entryPoint, response, companyAccounts, accountID) {
        var scopeObj = this;
        if (kony.sdk.isNullOrUndefined(companyAccounts)) {
            companyAccounts = [];
        }
        applicationManager.getNavigationManager().updateForm({
            Alerts: response,
            companyAccounts: scopeObj.processOrganizationAccounts(companyAccounts.slice(0, 10))
        }, 'frmProfileManagement');
        if (entryPoint === "profileSettings") {
            this.showProfileSettings();
        }
        if (entryPoint === "accountSettings") {
            this.showPreferredAccounts();
        }
        if (entryPoint === "alertSettings") {
            this.fetchFirstAlerttype();
        }
        if (entryPoint === "alertSettings2") {
            this.fetchThirdAlerttype(accountID);
        }
        if(entryPoint === "securityQuestions"){
           this.showSecurityQuestionsScreen();
       }
        if( entryPoint === "approvalMatrix") {
         this.setContractDetailsForApprovalMatrices();
         //this.fetchFirstAccountApprovalMatrix(companyAccounts);
       }
        if( entryPoint === "consentManagement") {
           this.showConsentManagement();
        }
        if (entryPoint === "approvalMatrix") {
            this.fetchFirstAccountApprovalMatrix(companyAccounts);
        }
        if (entryPoint === "consentManagement") {
            this.showConsentManagement();
        }
        if (entryPoint === "manageAccountAccess") {
            this.showManageAccountAccess();
        }
        if (entryPoint === "deviceManagement") {
            this.showDeviceManagementScreen();
        }
    };

    Profile_PresentationController.prototype.processOrganizationAccounts = function(companyAccounts) {

        if (companyAccounts.length === 0) {
            return companyAccounts;
        }
        var AccountsArray = [];
        (companyAccounts).forEach(function(obj) {
            var AccObj = {};
            AccObj.displayName = obj.accountName + " - X" + CommonUtilities.getLastFourDigit(obj.accountId);
            AccObj.AccountName = obj.accountName;
            AccObj.Account_id = obj.accountId;
            AccountsArray.push(AccObj);
        });

        return AccountsArray;
    };
    /**
     * Error callback for customer alert category to be shown.
     */
    Profile_PresentationController.prototype.fetchAlertsCategoryFailure = function(response) {}
    /**
     * Entry method to profile settings
     * @param {String} entryPoint - Dicides which flow to be called
     */
    Profile_PresentationController.prototype.enterProfileSettings = function(entryPoint) {
      this.showProgressBar();
      this.initializeUserProfileClass();
      navigationArray.splice(0,navigationArray.length);
      if(entryPoint === "approvalMatrix"){
        this.setContractDetailsForApprovalMatrices();
      }
      else{
        this.fetchAlertsCategory(entryPoint);
      }
    };
    /**
     * Method to navigate to profile form
     */
    Profile_PresentationController.prototype.fetchFirstAlerttype = function() {
        applicationManager.getNavigationManager().updateForm({
            fetchFirstAlert: {}
        }, 'frmProfileManagement');
    };
    /**
     * Method to navigate to profile form
     */
    Profile_PresentationController.prototype.fetchThirdAlerttype = function(accountID) {
        applicationManager.getNavigationManager().updateForm({
            fetchThirdAlert: {
                accountID
            }
        }, 'frmProfileManagement');
    };
    /**
     * Method to navigate to profile form - approvalMatrix
     */
    Profile_PresentationController.prototype.fetchFirstAccountApprovalMatrix = function(companyAccounts) {
        applicationManager.getNavigationManager().updateForm({
            fetchFirstAccountApprovalMatrix: {
                "companyAccounts": companyAccounts
            }
        }, 'frmProfileManagement');
    };
    /**
     * Method to fetch the username rules and policies
     */
    Profile_PresentationController.prototype.getUsernameRulesAndPolicies = function() {
        applicationManager.getUserPreferencesManager().fetchUsernameRulesAndPolicy(this.getUsernameRulesAndPoliciesSuccess.bind(this), this.getUsernameRulesAndPoliciesFailure.bind(this));
    };
    /**
     * Method used as success call back for the password rules.
     * @param {Object} response - contains the service response for the password rules.
     */
    Profile_PresentationController.prototype.getUsernameRulesAndPoliciesSuccess = function(response) {
        var validationUtility = applicationManager.getValidationUtilManager();
        validationUtility.createRegexForUsernameValidation(response.usernamerules);
        applicationManager.getNavigationManager().updateForm({
            usernamepolicies: {
                "usernamepolicies": response.usernamepolicy
            }
        }, "frmProfileManagement");
    };
    /**
     *failure callback of method getUsernameRulesAndPolicies
     */
    Profile_PresentationController.prototype.getUsernameRulesAndPoliciesFailure = function() {};
    Profile_PresentationController.prototype.showUserNameAndPassword = function() {
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        applicationManager.getNavigationManager().updateForm({
            usernameAndPasswordLanding: {}
        }, 'frmProfileManagement');
    };
    Profile_PresentationController.prototype.showSecurityQuestionsScreen = function() {
        applicationManager.getNavigationManager().updateForm({
            showSecurityQuestion: true,
            isLoading: true
        }, 'frmProfileManagement');
    };
  
  Profile_PresentationController.prototype.getAccountActionCustomerApproverList = function( requestParams ) {
    applicationManager.getBusinessUserManager().getAccountActionCustomerApproverList(requestParams, 
                                                                                     this.getAccountActionCustomerApproverListSuccess.bind(this),
                                                                                     this.getAccountActionCustomerApproverListFailure.bind(this));     
  };
  
  Profile_PresentationController.prototype.getAccountActionCustomerApproverListSuccess = function( approversResponse ) {
    applicationManager.getNavigationManager().updateForm({
      approversResponse: {
        approvers : approversResponse["Approvers"]
      },
      isLoading:false
    }, "frmEditApprovalMatrixLimits");
      
  };
  
  Profile_PresentationController.prototype.getAccountActionCustomerApproverListFailure = function( error ) {
    applicationManager.getNavigationManager().updateForm({
      approversResponse: {
        error : error.errorMessage
      },
      isLoading:false
    }, "frmEditApprovalMatrixLimits");
  }; 
  Profile_PresentationController.prototype.fetchApprovalRules = function() {
     applicationManager.getBusinessUserManager().fetchApprovalRules(this.fetchApprovalRulesSuccess.bind(this),
                                                                    this.fetchApprovalRulesFailure.bind(this));     
  };
  
  Profile_PresentationController.prototype.fetchApprovalRulesSuccess = function( approversRules ) {
    applicationManager.getNavigationManager().updateForm({
      approversRules: {
        rules : approversRules
      },
      isLoading:false
    }, "frmEditApprovalMatrixLimits");
      
  };
  
  Profile_PresentationController.prototype.fetchApprovalRulesFailure = function( error ) {
    applicationManager.getNavigationManager().updateForm({
      approversResponse: {
        error : error.errorMessage
      }     
    }, "frmEditApprovalMatrixLimits");
  }; 
  
  Profile_PresentationController.prototype.saveApprovalMatrixForAccountAndFeatureAction = function( currentState ) {
    applicationManager.getBusinessUserManager().updateApprovalMatrixPerFeatureAction(currentState, 
                                                                                                  this.onSaveApprovalMatrixSuccess.bind(this),
                                                                                                  this.onSaveApprovalMatrixFailure.bind(this));     
  };
  //todo
  Profile_PresentationController.prototype.onSaveApprovalMatrixSuccess = function( approvalMatrix ) {
    saveState=true;
    applicationManager.getNavigationManager().updateForm({
      approvalMatrixUpdated : { isSuccessful : true }      
    }, "frmEditApprovalMatrixLimits");      
  };
  
  Profile_PresentationController.prototype.onSaveApprovalMatrixFailure = function( error ) {
    applicationManager.getNavigationManager().updateForm({
      approvalMatrixUpdated : { isSuccessful : false,
                                errorMessage : error.errorMessage
                              }      
    }, "frmEditApprovalMatrixLimits");  
  }; 
  
  Profile_PresentationController.prototype.getCoreCustomerFeatureActionLimits = function( editData ) {
    var params = {
      "coreCustomerId" : editData.coreCustomerID
    };
    applicationManager.getBusinessUserManager().getCoreCustomerFeatureActionLimits(params, 
												this.onGetActionLimitSuccess.bind(this, editData),
                                                this.onGetActionLimitFailure.bind(this, editData));     
  };
  
  Profile_PresentationController.prototype.onGetActionLimitSuccess = function( editData, actionLimitResponse ) {
    if(kony.sdk.isNullOrUndefined(actionLimitResponse["features"]) || actionLimitResponse["features"].length == 0)
      this.onGetActionLimitFailure();
    else{
      var features = actionLimitResponse["features"];
      for(var feature=0; feature<features.length; feature++){
        var actions = features[feature].actions;
        for(var action=0; action<actions.length; action++){
          if( editData["actionId"] === actions[action]["actionId"] ) {
            if(!kony.sdk.isNullOrUndefined(actions[action].limits)){
              var limits = actions[action].limits;
              for(var limit=0; limit<limits.length; limit++){
                if(limits[limit].id === editData["frequency"]){
                  editData["featureAction"]["featureActionLimit"] = limits[limit]["value"];
                  break;
                }
              }
            }
          }
        }
      }
	  applicationManager.getNavigationManager().navigateTo("frmEditApprovalMatrixLimits");
      applicationManager.getNavigationManager().updateForm({
         editData : editData        
      }, "frmEditApprovalMatrixLimits");   
    }
  };
  
  Profile_PresentationController.prototype.onGetActionLimitFailure = function( editData, error ) {
    applicationManager.getNavigationManager().updateForm({
      EditModelError : {
        error : error        
      } 
    }, "frmViewApprovalMatrix");  
  };   

    /**
     * Method used to edit consent management.
     * @param {Object} context - contains the consentBlock, consentType, consentGiven, blockNotes, consentTypeName.
     */
    Profile_PresentationController.prototype.editConsentManagement = function(consentDetails) {
        this.showProgressBar();

        var consentData = {
            "arrangementId": consentDetails.arrangementId,
            "consent": JSON.stringify({
                'consentTypes': consentDetails.consents
            }, ['consentTypes', 'consentType', 'consentGiven', 'subTypes', 'consentSubType', 'consentSubTypeGiven']).replaceAll('"', "'").replaceAll("consentSubTypeGiven", "subTypeConsentGiven")
        };
        applicationManager.getSettingsManager().updateConsentData(consentData, this.editConsentManagementSuccess.bind(this), this.editConsentManagementFailure.bind(this));
    };

    /**
     * Method used to fetch the 
     */
     Profile_PresentationController.prototype.editConsentManagementSuccess = function(response) {
		if(response.errorMessage!== undefined){
			applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
            applicationManager.getNavigationManager().updateForm({
            "consentError": response.errorMessage,
            "action": "update"
        }, 'frmProfileManagement');
		} else {
        this.showConsentManagement();
		}
    };

    /**
     * Method used as the failure call back for the edit consent management service call.
     * @param {String} errorMessage - contains the error message for edit service.
     */
    Profile_PresentationController.prototype.editConsentManagementFailure = function(response) {
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            "consentError": response.errorMessage,
            "action": "update"
        }, 'frmProfileManagement');
    };

    /**
     * Method used to edit the revoke account access.
     */
    Profile_PresentationController.prototype.updateRevokeAccountAccess = function(accountData) {
      this.showProgressBar();
      var mfaManager = applicationManager.getMFAManager();
      mfaManager.setMFAFlowType("PSD2_TPP_CONSENT_REVOKE");
      
      
        var accountAccessData = {
            "arrangementId": accountData,
            "consent": JSON.stringify({
                'consentStatuses': [{
                    'consentStatus': 'revokedByPsu'
                }]
            }).replaceAll('"', "'")
        };
        applicationManager.getSettingsManager().updatePSD2ConsentData(accountAccessData, this.updateRevokeAccountAccessSuccess.bind(this), this.updateRevokeAccountAccessFailure.bind(this));
    };

    /**
     * Method used as the success call back for the edit the revoke account access service call.
     */
    Profile_PresentationController.prototype.updateRevokeAccountAccessSuccess = function(response) {
      var mfaManager = applicationManager.getMFAManager();
      if (response && response.MFAAttributes) {
        if (response.MFAAttributes.isMFARequired == "true") {
          var mfaJSON = {
            "flowType": "PSD2_TPP_CONSENT_REVOKED",
            "response": response
          };
          applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }
      } else {
        this.showManageAccountAccess();
      }
    };
    /**
     * Method used as the failure call back for the edit the revoke account access service call.
     * @param {String} errorMessage - contains the error message for edit service.
     */
    Profile_PresentationController.prototype.updateRevokeAccountAccessFailure = function(response) {
              
       applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            "managePSD2Error": response.errorMessage,
            "action": "update"
        }, 'frmProfileManagement');
    }

    Profile_PresentationController.prototype.setContractDetailsForApprovalMatrices = function(response){
      var contracts = applicationManager.getUserPreferencesManager().getUserObj().CoreCustomers;
      var contractFilterData = this.getContractsList(contracts);
      applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
      if(contracts.length > 0){
        applicationManager.getNavigationManager().updateForm({
          approvalMatrixContractDetails: {
            "contractDetails" : contracts,
            "contractFilter" : contractFilterData
          },
          "isLoading" : true
        }, 'frmProfileManagement');
      }
      else{
        this.aprovalMatrixServiceFailure({"entryPoint" : "frmProfileManagement"}, {
          "errorMessage" : "No Core Customers are available", // adding a custom message for dev purpose 
          "isContractsEmpty" : true
        })
      }
    };
  
  Profile_PresentationController.prototype.getActionNameBasedOnPermissions = function(){
    var isViewEnabled = applicationManager.getConfigurationManager().checkUserPermission('APPROVAL_MATRIX_VIEW');
    var isEditEnabled = applicationManager.getConfigurationManager().checkUserPermission('APPROVAL_MATRIX_MANAGE');
    var btnText = (isViewEnabled) ? kony.i18n.getLocalizedString("i18n.locateus.view") : "";
    btnText += (isViewEnabled && isEditEnabled) ? ("/" + kony.i18n.getLocalizedString("i18n.billPay.Edit"))
    											: ((isEditEnabled) ? kony.i18n.getLocalizedString("i18n.billPay.Edit") : "");
    btnText += " " + kony.i18n.getLocalizedString("konybb.approvalMatrix.matrix");
    return btnText;
  };
  
  Profile_PresentationController.prototype.getContractsList = function(response){
    var contracts = new Set();
    var result = [];
    contracts.add("All"); // default filter
    response.forEach(function(contract){
      contracts.add(contract.contractName);
    });
    contracts.forEach(function(contract){
      result.push({"contract": contract});	
    });
    return result;
  };
  
  Profile_PresentationController.prototype.sortSegmentData = function(segmentData, sectionIndex, imgWidget, sortKey){
    var sortIcon = segmentData[sectionIndex][0][imgWidget].src;
    var rowData = segmentData[sectionIndex][1];
    var sortOrder = "desc";
    if(sortIcon === "sorting_previous.png"){
      sortOrder = "asc";
      segmentData[sectionIndex][0][imgWidget].src = "sorting_next.png";
    }
    else if(sortIcon === "sorting_next.png" || sortIcon === "sorting.png"){
      segmentData[sectionIndex][0][imgWidget].src = "sorting_previous.png";
    }
	rowData.sort(function(obj1, obj2){
      var order = (sortOrder === "desc") ? -1 : 1;
      if(obj1[sortKey] > obj2[sortKey]){
        return order;
      }
      else if(obj1[sortKey] < obj2[sortKey]){
        return -1 * order;
      }
      else{
        return 0;
      }
    });
	segmentData[sectionIndex][1] = rowData;
  };
  
  Profile_PresentationController.prototype.filterSegmentData = function(segmentData, sectionIndex, filterValue){
    if(filterValue === "All")
      return segmentData;
    var rowData = segmentData[sectionIndex][1];
    var segData = [[segmentData[sectionIndex][0], []]];
    segData[sectionIndex][1] = rowData.filter(function(obj){
      return obj.contractName === filterValue;
    });
    return segData;
  };
  
  Profile_PresentationController.prototype.searchSegmentData = function(segmentData, sectionIndex, searchKey){
    if(searchKey === "")
      return segmentData;
    searchKey = searchKey.toLowerCase();
    var rowData = segmentData[sectionIndex][1];
    var segData = [[segmentData[sectionIndex][0], []]];
    segData[sectionIndex][1] = rowData.filter(function(obj){
      var isContractName = obj.contractName ? obj.contractName.toLowerCase().includes(searchKey) : false;
      var isCustomerName = obj.coreCustomerName ? obj.coreCustomerName.toLowerCase().includes(searchKey) : false;
      var isCustomerID = obj.coreCustomerID ? obj.coreCustomerID.includes(searchKey) : false;
      return isContractName || isCustomerName || isCustomerID;
    });
    return segData;
  };
  
  Profile_PresentationController.prototype.isApprovalMatrixDisabled = function(rowData){
    var request = {
      "contractId" : rowData.contractId,
      "cif" : rowData.coreCustomerID
    };
    applicationManager.getNavigationManager().updateForm({
      isLoading : true
    }, 'frmProfileManagement');
  	applicationManager.getBusinessUserManager().isApprovalMatrixDisabled(request, this.navigateToViewMatrix.bind(this, rowData), this.aprovalMatrixServiceFailure.bind(this, rowData));
  };
  
  Profile_PresentationController.prototype.navigateToViewMatrix = function(rowData, response){
    var isDisabled = (response.isDisabled === "true");
    applicationManager.getNavigationManager().setCustomInfo("frmViewApprovalMatrix", isDisabled);
    this.fetchApprovalMatrix(rowData);
  };
  
  Profile_PresentationController.prototype.aprovalMatrixServiceFailure = function(contracts, error){
    var currentForm = kony.application.getCurrentForm().id;
    if(currentForm !== contracts.entryPoint){
      applicationManager.getNavigationManager().navigateTo(contracts.entryPoint);
    }
    applicationManager.getNavigationManager().updateForm({
      approvalMatrixError : error,
      isLoading : false
      }, contracts.entryPoint);
  };
  
  Profile_PresentationController.prototype.noServiceNavigateToViewMatrix = function(){
    applicationManager.getNavigationManager().navigateTo("frmViewApprovalMatrix");
  };
  
  Profile_PresentationController.prototype.backNavigateToViewMatrix = function() {
    navigationArray.pop();
    applicationManager.getNavigationManager().navigateTo("frmViewApprovalMatrix");
    applicationManager.getNavigationManager().updateForm(navigationArray[navigationArray.length-1], "frmViewApprovalMatrix");
  };
  
  Profile_PresentationController.prototype.noServiceNavigateToAccountLevel = function() {
    navigationArray.pop();
    applicationManager.getNavigationManager().navigateTo("frmApprovalMatrixAccountLevel");
    applicationManager.getNavigationManager().updateForm(navigationArray[navigationArray.length-1], "frmApprovalMatrixAccountLevel");
  };
  
  Profile_PresentationController.prototype.fetchApprovalMatrix = function(rowData){
    var request = {
      "cif" : rowData.coreCustomerID,
      "contractId" : rowData.contractId,
      "accountId" : (kony.sdk.isNullOrUndefined(rowData.accountId))? "" : rowData.accountId
    };
    applicationManager.getBusinessUserManager().fetchApprovalMatrix(request, this.onFetchApprovalMatrixSuccess.bind(this, rowData),
                                                                    this.aprovalMatrixServiceFailure.bind(this, rowData));
  };
  
  Profile_PresentationController.prototype.getFinAndNonFinMatrix = function(matrix) {
    var allFeatureMatrix = [];
    var finMatrix = {};
    var nonFinMatrix = {};
    var actions = Object.keys(matrix);
    for(var action = 0; action < actions.length; action++){
      var actionObj = matrix[actions[action]];
      if(actionObj.actionType === "NON_MONETARY"){
        nonFinMatrix[actionObj.actionId] = actionObj;
      }
      else
        finMatrix[actionObj.actionId] = actionObj;
    }
    allFeatureMatrix.push(finMatrix);
    allFeatureMatrix.push(nonFinMatrix);
    return allFeatureMatrix;
  };
  
  Profile_PresentationController.prototype.onFetchApprovalMatrixSuccess = function(rowData, response){
    var processedMatrix = this.getProcessedApprovalMatrix(response);
    var allFeatureMatrix = this.getFinAndNonFinMatrix(processedMatrix);
    var processedFeatures = this.getFeaturesFromMatrix(processedMatrix);
    this.features=processedFeatures;
    applicationManager.getNavigationManager().navigateTo("frmViewApprovalMatrix");
    var viewModel={
      isLoading: true,
      approvalMatrix: allFeatureMatrix[0],
      nonFinMatrix: allFeatureMatrix[1],
      features: processedFeatures,
      contractDetails: rowData,
      entryPoint: rowData.entryPoint
    };
    if(saveState){
      var tempentrypoint=navigationArray[navigationArray.length-1].entryPoint
      navigationArray.pop();
      navigationArray.push(viewModel);
      navigationArray[navigationArray.length-1].entryPoint=tempentrypoint;
      saveState=false;
    }
    else{
    navigationArray.push(viewModel);
    }
    applicationManager.getNavigationManager().updateForm(viewModel, "frmViewApprovalMatrix");
  };

  Profile_PresentationController.prototype.processLimitTypes = function(limitTypes){
    var processedData = {};
    for (var limitType = 0; limitType < limitTypes.length; limitType++) {
      var limitId = limitTypes[limitType].limitTypeId;
      var actions = limitTypes[limitType].actions;
      for (var action = 0; action < actions.length; action++) {
        if (kony.sdk.isNullOrUndefined(processedData[actions[action].actionId])) {
          processedData[actions[action].actionId] = {
            "actionName": actions[action]["actionName"],
            "actionId": actions[action]["actionId"],
            "featureId": actions[action]["featureId"],
            "featureName": actions[action]["featureName"],
            "actionType": actions[action]["actionType"]
          };
        }
        processedData[actions[action].actionId][limitId] = actions[action].limits;
      }
    }
    return processedData;
  };
  
  Profile_PresentationController.prototype.getProcessedApprovalMatrix = function(response, isCommonResponse) {
    var data = response.common !== undefined ? response.common.limitTypes : response.accounts[0].limitTypes;        
    return this.processLimitTypes(data);
  };

  Profile_PresentationController.prototype.getFeaturesFromMatrix = function(matrix){
    var features = [];
    var actions = Object.keys(matrix);
    for(var action = 0; action < actions.length; action++){
      var actionName = matrix[actions[action]]["featureName"]+" - "+matrix[actions[action]]["actionName"];
      var actionObject = {
        "text" : (actionName.length > 35) ? (actionName.substring (0,35) + "...") : actionName,
        "toolTip" : actionName
      }
      features.push({
        "actionId" : actions[action],
        "actionName" : actionObject,
        "imgArrow" : {
          "isVisible" : false
        },
        "flxSelectRole" : {
          "isVisible" : false
        },
        "isMonetary": matrix[actions[action]].actionType === "MONETARY" ? true : false
      });
    }
    return features;
  };
  
  Profile_PresentationController.prototype.updateApprovalMatrixStatus = function(contracts, isDisabled){
    var request = {
      "contractId" : contracts.contractId,
      "cif" : contracts.coreCustomerID,
      "disable" : isDisabled
    };
    applicationManager.getBusinessUserManager().updateApprovalMatrixStatus(request, this.updateApprovalMatrixStatusSuccess.bind(this, contracts), this.updateApprovalMatrixStatusFailure.bind(this));
  };
  
  Profile_PresentationController.prototype.updateApprovalMatrixStatusSuccess = function(contracts, response){
    var isDisabled = applicationManager.getNavigationManager().getCustomInfo("frmViewApprovalMatrix");
    applicationManager.getNavigationManager().setCustomInfo("frmViewApprovalMatrix", !isDisabled);
    this.fetchApprovalMatrix(contracts);
  };
  
  Profile_PresentationController.prototype.updateApprovalMatrixStatusFailure = function(error){
    applicationManager.getNavigationManager().updateForm({
      updateStatusError : error,
      isLoading : false
      }, "frmViewApprovalMatrix");
  };
  /**
   * Method used to get T&C for disable ebankingAccess.
   */
  Profile_PresentationController.prototype.getTncContent = function() {
     var config = applicationManager.getConfigurationManager();
    var locale=config.getLocale();
    var termsAndConditions=config.getTermsAndConditions();
     var param={
    "languageCode": termsAndConditions[locale],
     "termsAndConditionsCode": termsAndConditions["OnlineBankingAccess"]
     };
     applicationManager.getTermsAndConditionManager().fetchTermsAndConditionsPostLogin(param,this.getTermsandConditionsSuccessCallBack.bind(this),this.getTermsandConditionsErrorCallback.bind(this));
  };
 Profile_PresentationController.prototype.getTermsandConditionsSuccessCallBack = function(response){
        applicationManager.getNavigationManager().updateForm({
            "managePSD2Error": response.errorMessage,
            "action": "revoke",
        }, 'frmProfileManagement');
    };
    /**
     * Method used to get T&C for disable ebankingAccess.
     */
    Profile_PresentationController.prototype.getTncContent = function() {
        var config = applicationManager.getConfigurationManager();
        var locale = config.getLocale();
        var termsAndConditions = config.getTermsAndConditions();
        var param = {
            "languageCode": termsAndConditions[locale],
            "termsAndConditionsCode": termsAndConditions["OnlineBankingAccess"]
        };
        applicationManager.getTermsAndConditionManager().fetchTermsAndConditionsPostLogin(param, this.getTermsandConditionsSuccessCallBack.bind(this), this.getTermsandConditionsErrorCallback.bind(this));
    };
    Profile_PresentationController.prototype.getTermsandConditionsSuccessCallBack = function(response) {
        applicationManager.getNavigationManager().updateForm({
            termsAndConditionsContent: response.termsAndConditionsContent
        }, "frmProfileManagement");
    };
    Profile_PresentationController.prototype.getTermsandConditionsErrorCallback = function(response) {
        CommonUtilities.showServerDownScreen();
    };
    /**
     * Method used to disable ebankingAccess.
     */
    Profile_PresentationController.prototype.disableEBankingAccess = function() {
        var userManager = applicationManager.getUserPreferencesManager();
        var userName = userManager.getUserObj().userName;
        var params = {
            "UserName": userName,
            "Status": "SUSPENDED"
        };
        userManager.updateUserStatus(params, this.disableEBankingAccessSuccess, this.disableEBankingAccessError);
    };

    Profile_PresentationController.prototype.disableEBankingAccessSuccess = function(response) {
        var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMod.presentationController.disableEBankingLogout();
    };

    Profile_PresentationController.prototype.disableEBankingAccessError = function(error) {
        CommonUtilities.showServerDownScreen();
    };
  Profile_PresentationController.prototype.searchFeaturesSegmentData = function(searchKey){
    var data=this.features;
    if(searchKey === ""){
      this.setFeaturesSegmentDatainController(data);
    }
    else{
      var searchData=[];
      searchData= data.filter(function(obj){
        return obj.actionName.text.toLowerCase().includes(searchKey.toLowerCase());
      });
      this.setFeaturesSegmentDatainController(searchData);
    }
  };
  Profile_PresentationController.prototype.setFeaturesSegmentDatainController = function(searchData) {
    var viewModel={
      "isFeatures":true,
      "data":searchData
    };
    applicationManager.getNavigationManager().updateForm(viewModel, "frmViewApprovalMatrix");
  };

    /********************************************************************** */
    return Profile_PresentationController;
});