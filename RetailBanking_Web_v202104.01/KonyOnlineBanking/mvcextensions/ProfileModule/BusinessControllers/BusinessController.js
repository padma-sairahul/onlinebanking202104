define([], function() {
    function Profile_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(Profile_BusinessController, kony.mvc.Business.Controller);
    Profile_BusinessController.prototype.initializeBusinessController = function() {};
    Profile_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return Profile_BusinessController;
});