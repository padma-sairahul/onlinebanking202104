define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {
    var frmP2PSendMoneyTab = "frmP2PSendMoneyTab";
    var frmP2PAddRecipientDetails = "frmP2PAddRecipientDetails";
    var frmP2PAddRecipient = "frmP2PAddRecipient";
    var frmP2PConfirmAddRecipient = "frmP2PConfirmAddRecipient";
    var frmP2PSendMoney = "frmP2PSendMoney";
    var frmP2PSettingsReview = "frmP2PSettingsReview";
    var frmP2PSettingsEdit = "frmP2PSettingsEdit";
    var frmP2PNotEligible = "frmP2PNotEligible";
    var frmP2PActivate = "frmP2PActivate";
    var frmP2PNoRecipients = "frmP2PNoRecipients";
    var frmP2PRecipienDetails = "frmP2PRecipienDetails";
    var frmP2PConfirmTransfer = "frmP2PConfirmTransfer";
    var frmP2PActivity = "frmP2PActivity";
    var frmP2PManageRecipientsTab = "frmP2PManageRecipientsTab";
    var frmP2PEditRecipient = "frmP2PEditRecipient";
    var frmP2PDeactivate = "frmP2PDeactivate";
    /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
    function PersonToPerson_PresentationController() {
        var configurationManager = applicationManager.getConfigurationManager();
        kony.mvc.Presentation.BasePresenter.call(this);
        this.sentOrRequestSortConfig = {
            'sortBy': 'nickName',
            'defaultSortBy': 'nickName',
            'order': configurationManager.OLBConstants.ASCENDING_KEY,
            'defaultOrder': configurationManager.OLBConstants.ASCENDING_KEY,
        };
        this.manageRecipientSortConfig = {
            'sortBy': 'nickName',
            'defaultSortBy': 'nickName',
            'order': configurationManager.OLBConstants.ASCENDING_KEY,
            'defaultOrder': configurationManager.OLBConstants.ASCENDING_KEY,
        };
        this.requestObj = "";
        this.addRecipientData = {};
    }

    inheritsFrom(PersonToPerson_PresentationController, kony.mvc.Presentation.BasePresenter);

    /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
    PersonToPerson_PresentationController.prototype.initializePresentationController = function() {};
    var frequencies = {
        "Once": "i18n.transfers.frequency.once",
        "Daily": "i18n.Transfers.Daily",
        "Weekly": "i18n.Transfers.Weekly",
        "BiWeekly": "i18n.Transfers.EveryTwoWeeks",
        "Monthly": "i18n.Transfers.Monthly",
        "Quarterly": "i18n.Transfers.Quaterly",
        "Half Yearly": "i18n.Transfers.HalfYearly",
        "Yearly": "i18n.Transfers.Yearly"
    };
    var forHowLong = {
        ON_SPECIFIC_DATE: "i18n.transfers.lbxOnSpecificDate",
        NO_OF_RECURRENCES: "i18n.transfers.lblNumberOfRecurrences"
    };
    var generateFromAccounts = function(fromAccount) {
        return [fromAccount.accountID, getFormattedAccountName(fromAccount)];
    };
    var getFormattedAccountName = function(account) {
        return CommonUtilities.getAccountDisplayName(account) + " " + CommonUtilities.getDisplayBalance(account);
    };
    var generateAccountsWithCurrency = function(fromAccount) {
        return [fromAccount.accountID, fromAccount.currencyCode];
    };

    /**
     * This method is used to search for recipients.
     * @param {String} data - Contains the search string.
     */
    PersonToPerson_PresentationController.prototype.searchPayAPerson = function(data) {
        this.showProgressBar("frmPayAPerson");
        if (data && data.searchKeyword.length >= 0) {
            var searchInputs = {
                'searchString': data.searchKeyword
            };
            //var criteria = kony.mvc.Expression.eq("searchString", searchInputs.searchString);
            applicationManager.getRecipientsManager().getP2PRecipientList(searchInputs, this.searchPayAPersonSuccess.bind(this, searchInputs), this.searchPayAPersonFailure.bind(this));
        }
    };
    /**
     * This method acts as the success call back for the searchPayAPerson method.
     * @param {Object} searchInputs - contains the search string used for the search.
     * @param {Object} response - contains the response for the search.
     */
    PersonToPerson_PresentationController.prototype.searchPayAPersonSuccess = function(searchInputs, response) {
        var viewProperties = {};
        viewProperties.searchPayAPerson = {
            payAPersonData: response,
            searchInputs: searchInputs
        };
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };
    /**
     * This method is used as the failure callback for the searchPayAPerson.
     * @param {String} errmsg - this contains the error message for the pay a person.
     */
    PersonToPerson_PresentationController.prototype.searchPayAPersonFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };

    /**
     *  Method used to show send money to new recipient.
     */
    PersonToPerson_PresentationController.prototype.showSendMoneyToNewRecipientView = function() {
        this.showProgressBar();
        var viewProperties = {};
        viewProperties.showSendMoneyToNewRecipientView = true;
        applicationManager.getNavigationManager().navigateTo(frmP2PAddRecipient)
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };

    /**
     *  This method fetches the next set of sent transactions for pay a person.
     */
    PersonToPerson_PresentationController.prototype.fetchNextSentTransactions = function() {
        this.showProgressBar();
        applicationManager.getPaginationManager().getNextPage();
        this.fetchSentTransactions();
    };

    /**
     *  This method fetches the previous set of sent transactions for pay a person.
     */
    PersonToPerson_PresentationController.prototype.fetchPreviousSentTransactions = function() {
        this.showProgressBar();
        applicationManager.getPaginationManager().getPreviousPage();
        this.fetchSentTransactions();
    };

    /**
     * Navigate to deactivate P2P page
     */
    PersonToPerson_PresentationController.prototype.showDeactivateP2P = function() {
        this.showProgressBar();
        applicationManager.getNavigationManager().navigateTo(frmP2PDeactivate);
        this.hideProgressBar();
    };

    /**
     * This method is used to deactivate the pay a person.
     */
    PersonToPerson_PresentationController.prototype.deactivateP2P = function() {
        this.showProgressBar();
        applicationManager.getUserPreferencesManager().deactivateP2P(this.deactivateP2PSuccess.bind(this), this.deactivateP2PFailure.bind(this));
    };

    /**
     * This method is used as the success call back for the deactivateP2P.
     * @param {object} response - contains the success message for  deactivateP2P.
     */
    PersonToPerson_PresentationController.prototype.deactivateP2PSuccess = function(response) {
        var viewProperties = {};
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        viewProperties.deactivationAcknowledgement = {
            "name": userPreferencesManager.getCurrentUserName(),
            "phone": userPreferencesManager.getUserPhone(),
            "email": userPreferencesManager.getUserEmail(),
            "defaultAccountForDeposit": userPreferencesManager.getUserObj()["default_to_account_p2p"],
            "defaultAccountForSending": userPreferencesManager.getUserObj()["default_from_account_p2p"]
        };
        applicationManager.getNavigationManager().navigateTo(frmP2PConfirmAddRecipient);
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PConfirmAddRecipient);
        this.hideProgressBar();
    };

    /**
     * This method is used as the failure call back for the deactivateP2P.
     * @param {object} errmsg - contains the error message for  deactivateP2P.
     */
    PersonToPerson_PresentationController.prototype.deactivateP2PFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };

    /**
     * This method acts as the success call back method for edit recipient.
     * @param {object}  payPersonJSON - contains info like name, nickname, phone, email and primary contact.
     * @param {object} response - contains the response for edit recipient service.
     */
    PersonToPerson_PresentationController.prototype.editRecipientSuccess = function(payPersonJSON, response) {
        var viewProperties = {};
        viewProperties.editRecipientAcknowledgement = payPersonJSON;
        applicationManager.getNavigationManager().navigateTo(frmP2PConfirmAddRecipient)
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PConfirmAddRecipient);
        this.hideProgressBar();
    };

    /**
     * This method is used as the failure call back for the edit recipient
     * @param {String} errmsg - Contains the error message for the service failure.
     */
    PersonToPerson_PresentationController.prototype.editRecipientFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.editRecipientFailure = errmsg;
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };

    /**
     * This method is used to edit recipient.
     * @param {object} - contains info like id, nickname, name,phone, email.
     */
    PersonToPerson_PresentationController.prototype.editRecipient = function(payeeData) {
        this.showProgressBar();
        var payeeJSON = {
            "PayPersonId": payeeData.id,
            "nickName": payeeData.nickName,
            "name": payeeData.name,
            "phone": payeeData.phone,
            "email": payeeData.email,
            "secondaryEmail": payeeData.secondaryEmail,
            "secondaryPhoneNumber": payeeData.secondaryPhoneNumber,
            "primaryContactForSending": payeeData.primaryContactForSending
        };
        applicationManager.getRecipientsManager().editP2PRecipient(payeeJSON, this.editRecipientSuccess.bind(this, payeeJSON), this.editRecipientFailure.bind(this));
    };

    /**
     *  deletes a payaperson recipient.
     * @param {object}  payeeID - payee Id to be deleted.
     */
    PersonToPerson_PresentationController.prototype.deleteRecipient = function(payeeID) {
        this.showProgressBar();
        var params = {
            "PayPersonId": payeeID
        };
        applicationManager.getRecipientsManager().deleteP2PRecipient(params, this.deleteRecipientSuccess.bind(this), this.deleteRecipientFailure.bind(this));
    };

    /**
     * This method acts as the success Call back for the deleteRecipient.
     * @param {object}  response - status of deleting a recipient - success or failure.
     */
    PersonToPerson_PresentationController.prototype.deleteRecipientSuccess = function(response) {
        this.fetchCurrentRecipientsList();
    };

    /**
     * This method acts as the failure method for deleteRecipient.
     * @param {object}  payeeID - payee Id to be deleted.
     */
    PersonToPerson_PresentationController.prototype.deleteRecipientFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            "inFormError": response
        });
        this.hideProgressBar();
    };

    /**
     * This function is used to fetch the current set(page) of recipients.
     * @param {string} view - this represents the view to be shown.
     */
    PersonToPerson_PresentationController.prototype.fetchCurrentRecipientsList = function() {
        this.showProgressBar();
        var PaginationManager = applicationManager.getPaginationManager();
        PaginationManager.getCurrentPage();
        this.fetchRecipientsList("ManageRecipientsTab");
    };

    /**
     * navigates to edit recipient details form
     * @param {JSON} payeeData - payee's data
     */
    PersonToPerson_PresentationController.prototype.editP2PRecipient = function(payeeData) {
        this.showProgressBar();
        let viewProperties = {};
        viewProperties.payeeData = payeeData;
        applicationManager.getNavigationManager().navigateTo(frmP2PEditRecipient);
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PEditRecipient);
        this.hideProgressBar();
    };

    /**
     * getPayPersonViewActivity - fetches pay a person recipient activity list.
     * @param {object} - contanis payPersonId.
     */
    PersonToPerson_PresentationController.prototype.getRecipientActivity = function(payPersonId, personData = null) {
        this.showProgressBar();
        applicationManager.getTransactionManager().getRecipientActivity({
            "personId": payPersonId
        }, this.getRecipientActivitySuccess.bind(this, personData), this.getRecipientActivityFailure.bind(this));
    };

    /**
     * This method acts as the success call back for the getRecipientActivity.
     * @param {array} response - contanis the list of transactions.
     */
    PersonToPerson_PresentationController.prototype.getRecipientActivitySuccess = function(personData, response) {
        var viewProperties = {};
        viewProperties.showPayAPersonActivity = response;
        viewProperties.personData = personData;
        applicationManager.getNavigationManager().navigateTo(frmP2PActivity)
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PActivity);
        this.hideProgressBar();
    };

    /**
     * This method acts as teh failure call back for the getRecipientActivity.
     * @param {String} - contanis error message.
     */
    PersonToPerson_PresentationController.prototype.getRecipientActivityFailure = function(errMsg) {
        applicationManager.getNavigationManager().updateForm({
            "inFormError": errMsg
        });
        this.hideProgressBar();
    };

    /**
     * This method returns for how long listbox masterdata for send money
     */
    PersonToPerson_PresentationController.prototype.listboxForHowLong = function(dateString) {
        var list = [];
        for (var key in forHowLong) {
            if (forHowLong.hasOwnProperty(key)) {
                list.push([key, kony.i18n.getLocalizedString(forHowLong[key])]);
            }
        }
        return list;
    };

    /**
     * This method returns frequencies listbox masterdata for send money
     */
    PersonToPerson_PresentationController.prototype.listboxFrequencies = function(context) {
        if (context == "sendMoneyToNewRecipient") {
            var list = [];
            list.push(["Once", kony.i18n.getLocalizedString(frequencies["Once"])]);
            return list;
        }
        var list = [];
        for (var key in frequencies) {
            if (frequencies.hasOwnProperty(key)) {
                list.push([key, kony.i18n.getLocalizedString(frequencies[key])]);
            }
        }
        return list;
    };

    /**
     * This method is used to check if the entered amount is eligible to start MFA in pay a person send money screen.
     * @param {Object} requestObj - contains the info about pay a person recipient - name, nickname, email, phone and transaction object fields.
     */
    PersonToPerson_PresentationController.prototype.checkMFAP2PSendMoney = function(requestObj) {
        var self = this;
        self.createP2PSendMoney(requestObj);
    };

    PersonToPerson_PresentationController.prototype.showP2PTransferReview = function(sendMoneyPaymentJSON, TnCcontentTransfer) {
        this.showProgressBar();
        applicationManager.getNavigationManager().navigateTo(frmP2PRecipienDetails);
        applicationManager.getNavigationManager().updateForm({
            "sendMoneyPaymentJSON": sendMoneyPaymentJSON,
            "TnCcontentTransfer": TnCcontentTransfer,
        });
    };
    /**
     * This method is used to create a send money transaction in pay a person.
     * @param {Object} requestObj - This contains info about the pay a person recipient and the transaction object.
     */
    PersonToPerson_PresentationController.prototype.createP2PSendMoney = function(requestObj) {
        this.showProgressBar();
        this.requestObj = requestObj;
        var mfaManager = applicationManager.getMFAManager();
        var displayName = "PayAPerson";
        applicationManager.getPresentationUtility().MFA.getServiceIdBasedOnDisplayName(displayName);
        var mfaParams = {
            serviceName: mfaManager.getServiceId(),
        };
        var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        if (requestObj.frequencyType == applicationManager.getConfigurationManager().OLBConstants.TRANSACTION_RECURRENCE.ONCE) {
            this.requestObj.numberOfRecurrences = "";
            this.requestObj.frequencyStartDate = "";
            this.requestObj.frequencyEndDate = "";
            this.requestObj.frequencyType = this.requestObj.frequencyType;
        } else if (this.requestObj.hasHowLong == "NO_OF_RECURRENCES") {
            this.requestObj.frequencyStartDate = "";
            this.requestObj.frequencyEndDate = "";
            this.requestObj.frequencyType = this.requestObj.frequencyType;
            this.requestObj.numberOfRecurrences = this.requestObj.numberOfRecurrences;
        } else if (this.requestObj.hasHowLong == "ON_SPECIFIC_DATE") {
            this.requestObj.numberOfRecurrences = "";
            this.requestObj.frequencyType = this.requestObj.frequencyType;
            this.requestObj.frequencyEndDate = this.requestObj.frequencyEndDate;
            this.requestObj.frequencyStartDate = this.requestObj.frequencyStartDate;
        }
        var newTransaction = new transactionsModel({
            'fromAccountNumber': this.requestObj.fromAccountNumber,
            'amount': (Number(this.requestObj.amount)).toFixed(2),
            'transactionsNotes': this.requestObj.transactionsNotes,
            'toAccountNumber': this.requestObj.toAccountNumber,
            'frequencyType': this.requestObj.frequencyType,
            'transactionType': "P2P",
            'isScheduled': this.requestObj.isScheduled,
            'scheduledDate': this.requestObj.scheduledDate,
            'personId': this.requestObj.personId,
            'p2pContact': this.requestObj.p2pContact,
            'numberOfRecurrences': this.requestObj.numberOfRecurrences,
            'frequencyStartDate': this.requestObj.frequencyStartDate,
            'frequencyEndDate': this.requestObj.frequencyEndDate,
            'transactionId': this.requestObj.transactionId,
            'MFAAttributes': mfaParams,
            'transactionCurrency': this.requestObj.transactionCurrency,
            'fee': applicationManager.getConfigurationManager().serviceFeeFlag === "true" ? applicationManager.getConfigurationManager().p2pServiceFee : ""
        });
        /**
         * Used for saving payee name in one time payment flow.
         */
        if (this.requestObj.context === "sendMoneyToNewRecipient") {
            newTransaction.payPersonName = this.requestObj.name, newTransaction.payPersonNickName = this.requestObj.nickName
        }
        if (this.requestObj.transactionId != null && this.requestObj.transactionId != "" && this.requestObj.transactionId !== undefined) {
            mfaManager.setMFAOperationType("UPDATE");
            applicationManager.getTransactionManager().updateTransaction(newTransaction, this.createP2PSendMoneySuccess.bind(this), this.createP2PSendMoneyFailure.bind(this));
        } else {
            if (applicationManager.getConfigurationManager().serviceFeeFlag === "true") {
                this.requestObj.fee = applicationManager.getConfigurationManager().p2pServiceFee;
            }
            mfaManager.setMFAOperationType("CREATE");
            applicationManager.getTransactionManager().createP2PTransaction(newTransaction, this.createP2PSendMoneySuccess.bind(this), this.createP2PSendMoneyFailure.bind(this));
        }
    };
    /**
     * This method is used as the success call back for the create send money.
     * @param {Object} requestObj - This contains info about the pay a person recipient and the transaction object.
     * @param {Object} responseData - contains the transactionId and success or failure info.
     */
    PersonToPerson_PresentationController.prototype.createP2PSendMoneySuccess = function(response) {
        var viewProperties = {};
        var mfaManager = applicationManager.getMFAManager();
        if (response.referenceId || response.transactionId) {
            viewProperties.showRequestSendMoneyAck = true;
            viewProperties.status = response;
            viewProperties.requestObj = this.requestObj;
            this.getAccountByID(this.requestObj.fromAccountNumber, viewProperties);
        } else {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "P2P_CREATE",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }
    };

    /**
     * This method is used to get the account details based on the from account number.
     * @param {Number} accountID - contains the account number.
     * @param {Object} viewProperties - contains the set of view properties map to be updated in pay a person form.
     */
    PersonToPerson_PresentationController.prototype.getAccountByID = function(accountID, viewProperties) {
        var accountManager = applicationManager.getAccountManager();
        accountManager.fetchInternalAccounts(this.fetchUserAccountAndNavigateSuccess.bind(this, accountID, viewProperties), this.fetchUserAccountAndNavigatesFailure.bind(this));
    };
    PersonToPerson_PresentationController.prototype.fetchUserAccountAndNavigateSuccess = function(accountID, viewProperties) {
        applicationManager.getNavigationManager().navigateTo(frmP2PConfirmTransfer);
        var account = applicationManager.getAccountManager().getInternalAccountByID(accountID);
        viewProperties.accountBalance = account.availableBalance;
        viewProperties.accountName = account.accountName;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };
    PersonToPerson_PresentationController.prototype.fetchUserAccountAndNavigatesFailure = function() {
        CommonUtilities.showServerDownScreen();
    }

    /**
     * This method is used as the failure call back for the create p2p send money.
     * @param {Obejct} requestObj - This contains info about the pay a person recipient and the transaction object.
     * @param {Object} errmsg - contains the error message in it.
     */
    PersonToPerson_PresentationController.prototype.createP2PSendMoneyFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        if (applicationManager.getConfigurationManager().serviceFeeFlag === "true") {
            this.requestObj.amount = String(Number(this.requestObj.amount) - Number(this.requestObj.fee));
        }
        this.requestObj.frequencyStartDate = CommonUtilities.sendDateToBackend(this.requestObj.frequencyStartDate);
        this.requestObj.frequencyEndDate = CommonUtilities.sendDateToBackend(this.requestObj.frequencyEndDate);
        this.requestObj.scheduledDate = CommonUtilities.sendDateToBackend(this.requestObj.scheduledDate);
        viewProperties.sendMoneyData = this.requestObj;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };

    /**
     * This method is used to check if the entered amount in the send money screen is valid amount or not.The range values are obtained from the entitlements.
     * @param {Number} amount - contains the amount value.
     */
    PersonToPerson_PresentationController.prototype.validatePayAPersonAmount = function(amount) {
        var minP2PLimit = parseFloat(applicationManager.getConfigurationManager().minP2PLimit);
        var maxP2PLimit = parseFloat(applicationManager.getConfigurationManager().maxP2PLimit);
        var result = {
            isAmountValid: false
        };
        if (amount < minP2PLimit) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.minTransactionError") + " " + CommonUtilities.formatCurrencyWithCommas(minP2PLimit);
        } else if (amount > maxP2PLimit) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.maxTransactionError") + " " + CommonUtilities.formatCurrencyWithCommas(maxP2PLimit);
        } else {
            result.isAmountValid = true;
        }
        return result;
    };
    /**
     * Send money TnC flow
     */
    PersonToPerson_PresentationController.prototype.getTnCP2PTransfer = function(dataItem) {
        this.showProgressBar();
        var self = this;
        var termsAndConditionModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TermsAndConditionsModule");
        termsAndConditionModule.presentationController.showTermsAndConditions(OLBConstants.TNC_FLOW_TYPES.P2P_TnC, self.getTnCOnSuccess.bind(self, dataItem), self.getTnCOnFailure.bind(self));
    };
    PersonToPerson_PresentationController.prototype.getTnCOnSuccess = function(dataItem, TnCresponse) {
        applicationManager.getNavigationManager().updateForm({
            "TnCcontentTransfer": TnCresponse,
            "PayeeObj": dataItem
        });
        this.hideProgressBar();
    };
    PersonToPerson_PresentationController.prototype.getTnCOnFailure = function() {};

    /**
     * This method is used to get the default p2p FROM account
     */
    PersonToPerson_PresentationController.prototype.getDefaultP2PFromAccount = function() {
        return applicationManager.getUserPreferencesManager().getDefaultFromAccountforP2P();
    };
    /**
     * ActivateP2P - This method is used to activate pay a person Service for a user.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     */
    PersonToPerson_PresentationController.prototype.ActivateP2P = function(preferencesObject) {
        this.showProgressBar();
        applicationManager.getUserPreferencesManager().activateP2P(this.ActivateP2PSuccess.bind(this, preferencesObject), this.ActivateP2PFailure.bind(this));
    };

    /**
     * This method acts as the success call back for the ActivateP2P method.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     * @Param {object} response - contains the response object for activate P2p.
     */
    PersonToPerson_PresentationController.prototype.ActivateP2PSuccess = function(preferencesObject, response) {
        this.updateP2PPreferencesForUser(preferencesObject);
    };

    /**
     * This method acts as the failure call back for the Activate Pay a person Service.
     * @param {String} errmsg - contains error message.
     */
    PersonToPerson_PresentationController.prototype.ActivateP2PFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };

    /**
     * This method is used to update the pay a person preferences for a user.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     */
    PersonToPerson_PresentationController.prototype.updateP2PPreferencesForUser = function(preferencesObject) {
        this.showProgressBar();
        var param = {
            "default_to_account_p2p": preferencesObject.defaultToAccount,
            "default_from_account_p2p": preferencesObject.defaultFromAccount
        };
        applicationManager.getUserPreferencesManager().updateP2PPreferencesForUser(param, this.updateP2PPreferencesForUserSuccess.bind(this, param), this.updateP2PPreferencesForUserFailure.bind(this));
    };

    /**
     * This method acts as the success call back for the update pay a person preferences service.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     * @param {Object} response - contins the status for the update pay a person preferences service call. with success or failure.
     */
    PersonToPerson_PresentationController.prototype.updateP2PPreferencesForUserSuccess = function(preferencesObject, response) {
        applicationManager.getUserPreferencesManager().fetchUser(this.showPayAPerson.bind(this), this.updateP2PPreferencesForUserFailure.bind(this));
    };

    /**
     * This method is used as the failure call back for the update pay a person preferences service.
     * @param {String} errmsg - contains error message.
     */
    PersonToPerson_PresentationController.prototype.updateP2PPreferencesForUserFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };

    /**
     * This function acts as the entry point method to the pay a person module.
     * @param {object}  requiredView - this represents the view that needed to be shown. By default it is sendMoneyTab.
     * @param {object} [transactionObject] - transactionObject is used to repeat a transfer.
     */
    PersonToPerson_PresentationController.prototype.showPayAPerson = function(requiredView, transactionObject) {
        var self = this;
        applicationManager.getNavigationManager().navigateTo(frmP2PSendMoneyTab);
        self.showProgressBar();
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var payApersonEligibility = userPreferencesManager.checkP2PEligibilityForUser();
        applicationManager.getAccountManager().fetchInternalAccounts(function() {}, function() {});
        applicationManager.getAccountManager().checkP2PPermissionForAccounts();
        if (payApersonEligibility === 'NotEligible') {
            self.showNotEligibleView();
        } else {
            if (payApersonEligibility !== 'Activated') {
                self.showTermsAndConditions();
            } else {
                switch (requiredView) {
                    default:
                        self.showSendMoneyTabView();
                        break;
                    case "repeatTransaction":
                        self.selectedAccountId = transactionObject ? transactionObject.fromAccountNumber : null;
                        self.onSendMoney(transactionObject);
                        break;
                    case "sendMoneyTab":
                        self.selectedAccountId = transactionObject ? transactionObject.accountID : null;
                        self.showSendMoneyTabView();
                        break;
                    case "SentTransactionsTab":
                        self.showSentTransactionsView();
                        break;
                    case "ManageRecipients":
                        self.showManageRecipientsView();
                        break;
                    case "AddRecipient":
                        self.showAddRecipientView();
                        break;
                    case "NoRecipients":
                        self.showNoRecipientView();
                        break;
                    case "sendMoneyToNewRecipient":
                        self.showSendMoneyToNewRecipientView();
                        break;
                }
            }
        }
    };

    /**
     * This function shows no recipients view
     */
    PersonToPerson_PresentationController.prototype.showNoRecipientView = function() {
        applicationManager.getNavigationManager().navigateTo(frmP2PNoRecipients);
        this.hideProgressBar();
    };

    /**
     * This function is used to show the Activation View.
     */
    PersonToPerson_PresentationController.prototype.showActivationView = function(response) {
        this.showProgressBar();
        applicationManager.getNavigationManager().navigateTo(frmP2PActivate);
        var viewProperties = {};
        viewProperties.activation = true;
        viewProperties.TnCresponse = response;
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PActivate);
        this.hideProgressBar();
    };

    /**
     * This function is used to show the Not Eligible View.
     */
    PersonToPerson_PresentationController.prototype.showNotEligibleView = function() {
        this.showProgressBar();
        applicationManager.getNavigationManager().navigateTo(frmP2PNotEligible);
        this.hideProgressBar();
    };

    PersonToPerson_PresentationController.prototype.showTermsAndConditions = function() {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TermsAndConditionsModule").presentationController.showTermsAndConditions(OLBConstants.TNC_FLOW_TYPES.P2P_Activation_TnC, this.showActivationView.bind(this), this.getTnCOnFailure.bind(this));
    }
    PersonToPerson_PresentationController.prototype.getTnCOnFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            "inFormError": response
        });
        this.hideProgressBar();
    };

    /**
     * This method is used to update the pay a person preferences for a user.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     */
    PersonToPerson_PresentationController.prototype.updateP2PPreferencesForUser = function(preferencesObject) {
        this.showProgressBar();
        var param = {
            "default_to_account_p2p": preferencesObject.defaultToAccount,
            "default_from_account_p2p": preferencesObject.defaultFromAccount
        };
        applicationManager.getUserPreferencesManager().updateP2PPreferencesForUser(param, this.updateP2PPreferencesForUserSuccess.bind(this, param), this.updateP2PPreferencesForUserFailure.bind(this));
    };

    /**
     * This method acts as the success call back for the update pay a person preferences service.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     * @param {Object} response - contins the status for the update pay a person preferences service call. with success or failure.
     */
    PersonToPerson_PresentationController.prototype.updateP2PPreferencesForUserSuccess = function(preferencesObject, response) {
        applicationManager.getUserPreferencesManager().fetchUser(this.showSendMoneyTabView.bind(this), this.updateP2PPreferencesForUserFailure.bind(this));
    };

    /**
     * This method is used as the failure call back for the update pay a person preferences service.
     * @param {String} errmsg - contains error message.
     */
    PersonToPerson_PresentationController.prototype.updateP2PPreferencesForUserFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };

    /**
     * This function navigated to activate P2P settings view
     */
    PersonToPerson_PresentationController.prototype.ativateP2PSettings = function() {
        let viewProperties = {}
        let userPreferencesManager = applicationManager.getUserPreferencesManager();
        viewProperties.userJSON = {
            "userName": userPreferencesManager.getUserObj()["userfirstname"] + " " + userPreferencesManager.getUserObj()["userlastname"],
            "phone": userPreferencesManager.getUserPhone(),
            "email": userPreferencesManager.getUserEmail(),
        };
        viewProperties.paymentAccounts = applicationManager.getAccountManager().getInternalAccounts();
        viewProperties.activateP2P = true;
        applicationManager.getNavigationManager().navigateTo(frmP2PSettingsEdit);
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PSettingsEdit);
    };

    /**
     * This method navigates to edit P2P settings form with data of payee
     * @param {JSON} userJSON - data of payee
     * @param {JSON} paymentAccounts - payee accounts
     */
    PersonToPerson_PresentationController.prototype.editP2PSettingsScreen = function(userJSON, paymentAccounts) {
        applicationManager.getNavigationManager().navigateTo(frmP2PSettingsEdit);
        applicationManager.getNavigationManager().updateForm({
            "p2pSettingsEdit": true,
            "userJSON": userJSON,
            "paymentAccounts": paymentAccounts
        }, frmP2PSettingsEdit);
    };

    /**
     * This method is used to get the default p2p TO account
     */
    PersonToPerson_PresentationController.prototype.getDefaultP2PToAccount = function() {
        return applicationManager.getUserPreferencesManager().getDefaultToAccountforP2P();
    };

    /**
     * This method is used to get the default p2p FROM account
     */
    PersonToPerson_PresentationController.prototype.getDefaultP2PFromAccount = function() {
        return applicationManager.getUserPreferencesManager().getDefaultFromAccountforP2P();
    };

    /**
     * This method is used to show the pay a person settings screen in pay a person.
     */
    PersonToPerson_PresentationController.prototype.loadP2PSettingsScreen = function() {
        let viewProperties = {}
        viewProperties.p2pSettings = true;
        this.showProgressBar();
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        viewProperties.userJSON = {
            "userName": userPreferencesManager.getUserObj()["userfirstname"] + " " + userPreferencesManager.getUserObj()["userlastname"],
            "phone": userPreferencesManager.getUserPhone(),
            "email": userPreferencesManager.getUserEmail(),
        };
        viewProperties.paymentAccounts = applicationManager.getAccountManager().getInternalAccounts();
        applicationManager.getNavigationManager().navigateTo(frmP2PSettingsReview);
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PSettingsReview);
        this.hideProgressBar();
    };
    /**
     * This function is used to show the Add recipient page
     */
    PersonToPerson_PresentationController.prototype.showAddRecipientView = function() {
        this.showProgressBar();
        applicationManager.getNavigationManager().navigateTo(frmP2PAddRecipient);
        this.hideProgressBar();
    };

    /**
     * This function navigates to review add recipients screen
     * @param {JSON} recipient - data of the recipient
     */
    PersonToPerson_PresentationController.prototype.showAddRecipientDetailsView = function(recipient) {
        this.showProgressBar();
        this.addRecipientData = recipient;
        let viewProperties = {};
        viewProperties.recipientDetails = recipient;
        if (recipient["id"])
            viewProperties.editRecipientView = true
        applicationManager.getNavigationManager().navigateTo(frmP2PAddRecipientDetails);
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PAddRecipientDetails);
        this.hideProgressBar();
    };

    /**
     * This function navigates to add recipient form with filled data of recipeint
     */
    PersonToPerson_PresentationController.prototype.showAddRecipientModifyDetails = function() {
        this.showProgressBar();
        applicationManager.getNavigationManager().navigateTo(frmP2PAddRecipient);
        applicationManager.getNavigationManager().updateForm({
            "recipientDetails": this.addRecipientData
        }, frmP2PAddRecipient);
        this.hideProgressBar();
    };

    /**
     * This function navigates to edit recipient form with filled data of recipeint
     */
    PersonToPerson_PresentationController.prototype.showEditRecipientModifyDetails = function() {
        this.showProgressBar();
        applicationManager.getNavigationManager().navigateTo(frmP2PEditRecipient);
        applicationManager.getNavigationManager().updateForm({
            "recipientDetails": this.addRecipientData
        }, frmP2PEditRecipient);
    };


    /**
     * This method is used to create a recipient.
     * @param {object} payPersonJSON - contains info like name, nickname, phone, email.
     */
    PersonToPerson_PresentationController.prototype.createP2PPayee = function() {
        this.showProgressBar();
        applicationManager.getRecipientsManager().createP2PRecipient(this.addRecipientData, this.createP2PPayeeSuccess.bind(this, this.addRecipientData), this.createP2PPayeeFailure.bind(this));
    };
    /**
     * This method is used as the success call back for create pay a person recipient.
     * @param {object} payPersonJSON - contains info like name, nickname, phone, email.
     * @param {response} response - contains the response to the create recipient service.
     */
    PersonToPerson_PresentationController.prototype.createP2PPayeeSuccess = function(payPersonJSON, response) {
        var self = this;
        if (payPersonJSON.transactionId) {
            payPersonJSON.PayPersonId = response.PayPersonId;
            self.updateP2PTransactionWithPayee(payPersonJSON);
        } else {
            var viewProperties = {};
            viewProperties.addRecipientAcknowledgement = {};
            viewProperties.addRecipientAcknowledgement.payPersonJSON = payPersonJSON;
            viewProperties.addRecipientAcknowledgement.result = response;
            applicationManager.getNavigationManager().navigateTo(frmP2PConfirmAddRecipient)
            applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PConfirmAddRecipient);
        }
        this.hideProgressBar();
    };
    /**
     *This method is used as the failure call back for the create recipient.
     * @param {String} errmsg - error message for the failure of create recipient.
     */
    PersonToPerson_PresentationController.prototype.createP2PPayeeFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };

    /**
     * This method is used to update the transaciton with newly added recipient.
     * @param {object} res - contains the status for updating the transaction.
     */
    PersonToPerson_PresentationController.prototype.updateP2PTransactionWithPayee = function(res) {
        this.showProgressBar();
        var requestObj = {};
        requestObj.transactionId = res.transactionId;
        requestObj.personId = res.PayPersonId;
        var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var newTransaction = new TransactionModel({
            'transactionId': res.transactionId,
            'personId': res.PayPersonId
        });
        applicationManager.getTransactionManager().updateTransaction(newTransaction, this.updateP2PTranasctionWithPayeeSuccess.bind(this, res), this.updateP2PTranasctionWithPayeeFailure.bind(this));
    };

    /**
     * This method acts as the success call back for updateP2PTransactionWithPayee.
     * @param {object} res - contains the status for updating the transaction.
     */
    PersonToPerson_PresentationController.prototype.updateP2PTranasctionWithPayeeSuccess = function(requestObj, res) {
        var viewProperties = {};
        viewProperties.addRecipientAcknowledgement = {};
        viewProperties.addRecipientAcknowledgement.payPersonJSON = requestObj;
        viewProperties.addRecipientAcknowledgement.result = res;
        applicationManager.getNavigationManager().navigateTo(frmP2PConfirmAddRecipient)
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PConfirmAddRecipient);
        this.hideProgressBar();
    };
    /**
     * This method is used as the failure call back for the updateP2PTransactionWithPayee.
     * @param {object} errmsg - contains the error message for failure of updateP2PTransactionWithPayee.
     */
    PersonToPerson_PresentationController.prototype.updateP2PTranasctionWithPayeeFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties);
        this.hideProgressBar();
    };
    /**
     * This function is used to show the Send Money Tab View.
     */
    PersonToPerson_PresentationController.prototype.showSendMoneyTabView = function(sortingInputs) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.resetValues();
        this.fetchRecipientsList("SendMoneyTab", sortingInputs);
    };

    /**
     * This function is used to fetch the recipients(pay a person payees) list.
     *@param {String} view - this is used the required view  either SendMoneyTab or manageRecipientsTab.
     */
    PersonToPerson_PresentationController.prototype.fetchRecipientsList = function(view, sortingInputs) {
        var params, sortConfig;
        var paginationManager = applicationManager.getPaginationManager();
        if (view === "SendMoneyTab") {
            sortConfig = this.sentOrRequestSortConfig;
        } else {
            sortConfig = this.manageRecipientSortConfig;
        }
        params = paginationManager.getValues(sortConfig, sortingInputs);
        //var criteria = kony.mvc.Expression.and(kony.mvc.Expression.eq("sortBy", params.sortBy), kony.mvc.Expression.eq("order", params.order), kony.mvc.Expression.eq("offset", params.offset), kony.mvc.Expression.eq("limit", params.limit));
        var criteria = {
          "sortBy": params.sortBy,
          "order": params.order,
          "offset": params.offset,
          "limit": params.limit
        }
      	var recipientsManager = applicationManager.getRecipientsManager();
        recipientsManager.getP2PRecipientList(criteria, this.fetchRecipientsListSuccess.bind(this, view, sortConfig), this.fetchRecipientsListFailure.bind(this, view));
    };
    /**
     * This function acts as the success call back for the fetchRecipientsList.
     *@param {array} response - it contains a list of recipients.
     *@param {String} view - it represents the view to be displayed.
     */
    PersonToPerson_PresentationController.prototype.fetchRecipientsListSuccess = function(view, sortConfig, response) {
        if (view === "SendMoneyTab")
            applicationManager.getNavigationManager().navigateTo(frmP2PSendMoneyTab);
        else
            applicationManager.getNavigationManager().navigateTo(frmP2PManageRecipientsTab);
        var viewProperties = {};
        var paginationManager = applicationManager.getPaginationManager();
        if (response.length > 0) {
            viewProperties[view] = response;
            paginationManager.updatePaginationValues();
            viewProperties.pagination = paginationManager.getValues(sortConfig);
            viewProperties.pagination.limit = response.length;
        } else {
            var values = paginationManager.getValues(sortConfig);
            if (values.offset === 0) {
                viewProperties[view] = response;
            } else {
                viewProperties.noMoreRecords = true;
            }
        }
        if (view === "SendMoneyTab")
            applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PSendMoneyTab);
        else
            applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PManageRecipientsTab);
        this.hideProgressBar();
    };

    /**
     * This function acts as the failure call back for the fetchRecipientsList.
     * @param {String} response - the error message for the service.
     */
    PersonToPerson_PresentationController.prototype.fetchRecipientsListFailure = function(view, response) {
        let formToNavigate = "frmP2PSendMoneyTab"
        if (view !== "SendMoneyTab")
            formToNavigate = frmP2PManageRecipientsTab
        applicationManager.getNavigationManager().navigateTo(formToNavigate);
        applicationManager.getNavigationManager().updateForm({
            "inFormError": response
        }, formToNavigate);
        this.hideProgressBar();
    };

    /**
     * This function is used to fetch the next set(page) of recipients.
     * @param {string} View - this represents the view to be shown.
     */
    PersonToPerson_PresentationController.prototype.fetchNextRecipientsList = function(view) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getNextPage();
        this.fetchRecipientsList(view);
    };

    /**
     * This function is used to fetch the previous set(page) of recipients.
     * @param{string} View - this represents the view to be shown.
     */
    PersonToPerson_PresentationController.prototype.fetchPreviousRecipientsList = function(view) {
        this.showProgressBar();
        var PaginationManager = applicationManager.getPaginationManager();
        PaginationManager.getPreviousPage();
        this.fetchRecipientsList(view);
    };

    /**
     * This method is used to fetch the sent transactions and show the sent transactions tab view in pay a person.
     */
    PersonToPerson_PresentationController.prototype.showSentTransactionsView = function(sortingCurrentState) {
        this.showProgressBar();
        applicationManager.getPaginationManager().resetValues();
        this.fetchSentTransactions(sortingCurrentState);
    };

    /**
     *  This method is used to fetch the sent transactions in pay a person.
     */
    PersonToPerson_PresentationController.prototype.fetchSentTransactions = function(sortingCurrentState) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        var values = paginationManager.getValues(this.sentSortConfig, sortingCurrentState);
        applicationManager.getTransactionManager().fetchPayAPersonSentTransactions(values, this.fetchSentTransactionsSuccess.bind(this), this.fetchSentTransactionsFailure.bind(this));
    };

    /**
     *  This method acts as the success call back for the fetchSentTransactions.
     *  @param {array} response - contains the list of transaction objects.
     */
    PersonToPerson_PresentationController.prototype.fetchSentTransactionsSuccess = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmP2PSentTab");
        var viewProperties = {};
        var paginationManager = applicationManager.getPaginationManager();
        if (response.length > 0) {
            paginationManager.updatePaginationValues();
            viewProperties.pagination = paginationManager.getValues(this.sentSortConfig);
            viewProperties.sentTransactions = response;
        } else {
            var values = paginationManager.getValues(this.sentSortConfig);
            if (values.offset === 0) {
                viewProperties.noSentTransactions = true;
            } else {
                viewProperties.noMoreRecords = true;
            }
        }
        applicationManager.getNavigationManager().navigateTo("frmP2PSentTab");
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmP2PSentTab");
        this.hideProgressBar();
    };
    /**
     *  This method acts as a failure call back for the fetchSentTransactions.
     */
    PersonToPerson_PresentationController.prototype.fetchSentTransactionsFailure = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmP2PSentTab");
        applicationManager.getNavigationManager().updateForm({
            "inFormError": response
        }, "frmP2PSentTab");
        this.hideProgressBar();
    };

    /**
     * This function is used to show manage recipients tab view.
     */
    PersonToPerson_PresentationController.prototype.showManageRecipientsView = function(sortingInputs) {
        this.showProgressBar();
        applicationManager.getPaginationManager().resetValues();
        this.fetchRecipientsList("ManageRecipientsTab", sortingInputs);
    };

    /**
     * This method is used to show the send money screen in pay a person.
     * @param {Object} data - This contains the data to be shown in the send money screen.
     */
    PersonToPerson_PresentationController.prototype.onSendMoney = function(data) {
        this.showProgressBar();
        var self = this;
        var viewProperties = {};
        if (data["amount"])
            data["amount"] = "" + data["amount"];
        viewProperties.sendMoneyData = data;
        viewProperties.paymentAccounts = applicationManager.getAccountManager().getP2PFromSupportedAccounts();
        viewProperties.sendPaymentAccounts = viewProperties.paymentAccounts.map(generateFromAccounts);
        viewProperties.accountCurrency = viewProperties.paymentAccounts.map(generateAccountsWithCurrency);
        if (self.selectedAccountId) {
            viewProperties.sendMoneyData.fromAccountNumber = self.selectedAccountId;
            self.selectedAccountId = null;
        }
        applicationManager.getNavigationManager().navigateTo(frmP2PSendMoney)
        applicationManager.getNavigationManager().updateForm(viewProperties, frmP2PSendMoney);
        this.hideProgressBar();
    };

    /**
     * To format date string
     */
    PersonToPerson_PresentationController.prototype.getFormattedDateString = function(dateString) {
        return CommonUtilities.getFrontendDateString(dateString);
    };

    /**
     * To format amount
     */
    PersonToPerson_PresentationController.prototype.formatCurrency = function(amountString, isCurrencySumbolNotRequired, currencySymbolCode) {
        return CommonUtilities.formatCurrencyWithCommas(amountString, isCurrencySumbolNotRequired, currencySymbolCode);
    };
    /**
     * This method is used to show ProgressBar.
     */
    PersonToPerson_PresentationController.prototype.showProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
    };

    /**
     * This method is used to hide ProgressBar.
     */
    PersonToPerson_PresentationController.prototype.hideProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": false
        });
    };
    return PersonToPerson_PresentationController;
});