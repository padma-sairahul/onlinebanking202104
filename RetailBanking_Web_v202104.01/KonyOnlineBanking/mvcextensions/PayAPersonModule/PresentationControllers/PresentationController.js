define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {
    function PayAPerson_PresentationController() {
        var configurationManager = applicationManager.getConfigurationManager();
        kony.mvc.Presentation.BasePresenter.call(this);
        this.sentOrRequestSortConfig = {
            'sortBy': 'nickName',
            'defaultSortBy': 'nickName',
            'order': configurationManager.OLBConstants.ASCENDING_KEY,
            'defaultOrder': configurationManager.OLBConstants.ASCENDING_KEY,
        };
        this.manageRecipientSortConfig = {
            'sortBy': 'nickName',
            'defaultSortBy': 'nickName',
            'order': configurationManager.OLBConstants.ASCENDING_KEY,
            'defaultOrder': configurationManager.OLBConstants.ASCENDING_KEY,
        };
        this.sentSortConfig = {
            'sortBy': 'transactionDate',
            'defaultSortBy': 'transactionDate',
            'order': configurationManager.OLBConstants.DESCENDING_KEY,
            'defaultOrder': configurationManager.OLBConstants.DESCENDING_KEY,
        };
        this.receivedSortConfig = {
            'sortBy': 'transactionDate',
            'defaultSortBy': 'transactionDate',
            'order': configurationManager.OLBConstants.DESCENDING_KEY,
            'defaultOrder': configurationManager.OLBConstants.DESCENDING_KEY,
        };
        this.requestObj = "";
    }
    inheritsFrom(PayAPerson_PresentationController, kony.mvc.Presentation.BasePresenter);
    PayAPerson_PresentationController.prototype.initializePresentationController = function() {};
    var frequencies = {
        "Once": "i18n.transfers.frequency.once",
        "Daily": "i18n.Transfers.Daily",
        "Weekly": "i18n.Transfers.Weekly",
        "BiWeekly": "i18n.Transfers.EveryTwoWeeks",
        "Monthly": "i18n.Transfers.Monthly",
        "Quarterly": "i18n.Transfers.Quaterly",
        "Half Yearly": "i18n.Transfers.HalfYearly",
        "Yearly": "i18n.Transfers.Yearly"
    };
    var forHowLong = {
        ON_SPECIFIC_DATE: "i18n.transfers.lbxOnSpecificDate",
        NO_OF_RECURRENCES: "i18n.transfers.lblNumberOfRecurrences"
    };
    var generateFromAccounts = function(fromAccount) {
        return [fromAccount.accountID, getFormattedAccountName(fromAccount)];
    };
    var getFormattedAccountName = function(account) {
        return CommonUtilities.getAccountDisplayName(account) + " " + CommonUtilities.getDisplayBalance(account);
    };
    var generateAccountsWithCurrency = function(fromAccount) {
        return [fromAccount.accountID, fromAccount.currencyCode];
    };
    PayAPerson_PresentationController.prototype.listboxForHowLong = function(dateString) {
        var list = [];
        for (var key in forHowLong) {
            if (forHowLong.hasOwnProperty(key)) {
                list.push([key, kony.i18n.getLocalizedString(forHowLong[key])]);
            }
        }
        return list;
    };
    PayAPerson_PresentationController.prototype.listboxFrequencies = function(context) {
        if (context == "sendMoneyToNewRecipient") {
            var list = [];
            list.push(["Once", kony.i18n.getLocalizedString(frequencies["Once"])]);
            return list;
        }
        var list = [];
        for (var key in frequencies) {
            if (frequencies.hasOwnProperty(key)) {
                list.push([key, kony.i18n.getLocalizedString(frequencies[key])]);
            }
        }
        return list;
    };
    /** Present frmPrintTransfer
     * @param {object} viewModel Details of the Account
     */
    PayAPerson_PresentationController.prototype.showPrintPage = function(data) {
        applicationManager.getNavigationManager().navigateTo("frmPrintTransfer");
        applicationManager.getNavigationManager().updateForm(data, 'frmPrintTransfer');
    }
    PayAPerson_PresentationController.prototype.getFormattedDateString = function(dateString) {
        return CommonUtilities.getFrontendDateString(dateString);
    };
    /*
    To format amount
    */
    PayAPerson_PresentationController.prototype.formatCurrency = function(amountString, isCurrencySumbolNotRequired, currencySymbolCode) {
        return CommonUtilities.formatCurrencyWithCommas(amountString, isCurrencySumbolNotRequired, currencySymbolCode);
    };
    /**
     * This function acts as the entry point method to the pay a person module.
     * @param {object}  requiredView - this represents the view that needed to be shown. By default it is sendMoneyTab.
     * @param {object} [transactionObject] - transactionObject is used to repeat a transfer.
     */
    PayAPerson_PresentationController.prototype.showPayAPerson = function(requiredView, transactionObject) {
        var self = this;
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        self.showProgressBar();
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var payApersonEligibility = userPreferencesManager.checkP2PEligibilityForUser();
        applicationManager.getAccountManager().fetchInternalAccounts(function() {}, function() {});
        applicationManager.getAccountManager().checkP2PPermissionForAccounts();
        if (payApersonEligibility === 'NotEligible') {
            self.showNotEligibleView();
        } else {
            if (payApersonEligibility !== 'Activated') {
                self.showTermsAndConditions();
            } else {
                switch (requiredView) {
                    default:
                        self.showSendMoneyTabView();
                        break;
                    case "repeatTransaction":
                        self.selectedAccountId = transactionObject ? transactionObject.fromAccountNumber : null;
                        self.onSendMoney(transactionObject);
                        break;
                    case "sendMoneyTab":
                        self.selectedAccountId = transactionObject ? transactionObject.accountID : null;
                        self.showSendMoneyTabView();
                        break;
                    case "SentTransactionsTab":
                        self.showSentTransactionsView();
                        break;
                    case "ManageRecipients":
                        self.showManageRecipientsView();
                        break;
                    case "AddRecipient":
                        self.showAddRecipientView();
                        break;
                    case "sendMoneyToNewRecipient":
                        self.showSendMoneyToNewRecipientView();
                        break;
                }
            }
        }
    };
    PayAPerson_PresentationController.prototype.showTermsAndConditions = function() {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TermsAndConditionsModule").presentationController.showTermsAndConditions(OLBConstants.TNC_FLOW_TYPES.P2P_Activation_TnC, this.showActivationView.bind(this), this.getTnCOnFailure.bind(this));
    }
    PayAPerson_PresentationController.prototype.getTnCOnFailure = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        applicationManager.getNavigationManager().updateForm({
            "inFormError": response
        }, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This function is used to show the Not Eligible View.
     */
    PayAPerson_PresentationController.prototype.showNotEligibleView = function() {
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        this.showProgressBar();
        var viewProperties = {};
        viewProperties.notEligible = true;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This function is used to show the Activation View.
     */
    PayAPerson_PresentationController.prototype.showActivationView = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        this.showProgressBar();
        var viewProperties = {};
        viewProperties.activation = true;
        viewProperties.TnCresponse = response;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This function is used to show the add Recipient View.
     */
    PayAPerson_PresentationController.prototype.showAddRecipientView = function() {
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        this.showProgressBar();
        var viewProperties = {};
        viewProperties.addRecipient = true;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This function is used to show the Send Money Tab View.
     */
    PayAPerson_PresentationController.prototype.showSendMoneyTabView = function(sortingInputs) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.resetValues();
        this.fetchRecipientsList("SendMoneyTab", sortingInputs);
    };
    /**
     * This function is used to show manage recipients tab view.
     */
    PayAPerson_PresentationController.prototype.showManageRecipientsView = function(sortingInputs) {
        this.showProgressBar();
        applicationManager.getPaginationManager().resetValues();
        this.fetchRecipientsList("ManageRecipientsTab", sortingInputs);
    };
    /**
     * This function is used to fetch the recipients(pay a person payees) list.
     *@param {String} view - this is used the required view  either SendMoneyTab or manageRecipientsTab.
     */
    PayAPerson_PresentationController.prototype.fetchRecipientsList = function(view, sortingInputs) {
        var params, sortConfig;
        var paginationManager = applicationManager.getPaginationManager();
        if (view === "SendMoneyTab") {
            sortConfig = this.sentOrRequestSortConfig;
        } else {
            sortConfig = this.manageRecipientSortConfig;
        }
        params = paginationManager.getValues(sortConfig, sortingInputs);
        //var criteria = kony.mvc.Expression.and(kony.mvc.Expression.eq("sortBy", params.sortBy), kony.mvc.Expression.eq("order", params.order), kony.mvc.Expression.eq("offset", params.offset), kony.mvc.Expression.eq("limit", params.limit));
        var criteria = {
          "sortBy": params.sortBy,
          "order": params.order,
          "offset": params.offset,
          "limit": params.limit
        }
      	var recipientsManager = applicationManager.getRecipientsManager();
        recipientsManager.getP2PRecipientList(criteria, this.fetchRecipientsListSuccess.bind(this, view, sortConfig), this.fetchRecipientsListFailure.bind(this));
    };
    /**
     * This function acts as the success call back for the fetchRecipientsList.
     *@param {array} response - it contains a list of recipients.
     *@param {String} view - it represents the view to be displayed.
     */
    PayAPerson_PresentationController.prototype.fetchRecipientsListSuccess = function(view, sortConfig, response) {
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        var viewProperties = {};
        var paginationManager = applicationManager.getPaginationManager();
        if (response.length > 0) {
            viewProperties[view] = response;
            paginationManager.updatePaginationValues();
            viewProperties.pagination = paginationManager.getValues(sortConfig);
            viewProperties.pagination.limit = response.length;
        } else {
            var values = paginationManager.getValues(sortConfig);
            if (values.offset === 0) {
                viewProperties[view] = response;
            } else {
                viewProperties.noMoreRecords = true;
            }
        }
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This function acts as the failure call back for the fetchRecipientsList.
     *@param {String} response - the error message for the service.
     */
    PayAPerson_PresentationController.prototype.fetchRecipientsListFailure = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        applicationManager.getNavigationManager().updateForm({
            "inFormError": response
        }, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This function is used to fetch the next set(page) of recipients.
     * @param {string} View - this represents the view to be shown.
     */
    PayAPerson_PresentationController.prototype.fetchNextRecipientsList = function(view) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getNextPage();
        this.fetchRecipientsList(view);
    };
    /**
     * This function is used to fetch the previous set(page) of recipients.
     * @param{string} View - this represents the view to be shown.
     */
    PayAPerson_PresentationController.prototype.fetchPreviousRecipientsList = function(view) {
        this.showProgressBar();
        var PaginationManager = applicationManager.getPaginationManager();
        PaginationManager.getPreviousPage();
        this.fetchRecipientsList(view);
    };
    /**
     * This function is used to fetch the current set(page) of recipients.
     * @param {string} view - this represents the view to be shown.
     */
    PayAPerson_PresentationController.prototype.fetchCurrentRecipientsList = function() {
        this.showProgressBar();
        var PaginationManager = applicationManager.getPaginationManager();
        PaginationManager.getCurrentPage();
        this.fetchRecipientsList("ManageRecipientsTab");
    };
    /**
     * This method is used to fetch the sent transactions and show the sent transactions tab view in pay a person.
     */
    PayAPerson_PresentationController.prototype.showSentTransactionsView = function(sortingCurrentState) {
        this.showProgressBar();
        applicationManager.getPaginationManager().resetValues();
        this.fetchSentTransactions(sortingCurrentState);
    };
    /**
     *  This method is used to fetch the sent transactions in pay a person.
     */
    PayAPerson_PresentationController.prototype.fetchSentTransactions = function(sortingCurrentState) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        var values = paginationManager.getValues(this.sentSortConfig, sortingCurrentState);
        applicationManager.getTransactionManager().fetchPayAPersonSentTransactions(values, this.fetchSentTransactionsSuccess.bind(this), this.fetchSentTransactionsFailure.bind(this));
    };
    /**
     *  This method acts as the success call back for the fetchSentTransactions.
     *  @param {array} response - contains the list of transaction objects.
     */
    PayAPerson_PresentationController.prototype.fetchSentTransactionsSuccess = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        var viewProperties = {};
        var paginationManager = applicationManager.getPaginationManager();
        if (response.length > 0) {
            paginationManager.updatePaginationValues();
            viewProperties.pagination = paginationManager.getValues(this.sentSortConfig);
            viewProperties.sentTransactions = response;
        } else {
            var values = paginationManager.getValues(this.sentSortConfig);
            if (values.offset === 0) {
                viewProperties.noSentTransactions = true;
            } else {
                viewProperties.noMoreRecords = true;
            }
        }
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     *  This method acts as a failure call back for the fetchSentTransactions.
     *
     */
    PayAPerson_PresentationController.prototype.fetchSentTransactionsFailure = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        applicationManager.getNavigationManager().updateForm({
            "inFormError": response
        }, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     *  This method fetches the next set of sent transactions for pay a person.
     *
     */
    PayAPerson_PresentationController.prototype.fetchNextSentTransactions = function() {
        this.showProgressBar();
        applicationManager.getPaginationManager().getNextPage();
        this.fetchSentTransactions();
    };
    /**
     *  This method fetches the previous set of sent transactions for pay a person.
     *
     */
    PayAPerson_PresentationController.prototype.fetchPreviousSentTransactions = function() {
        this.showProgressBar();
        applicationManager.getPaginationManager().getPreviousPage();
        this.fetchSentTransactions();
    };
    /**
     *  deletes a payaperson recipient.
     * @param {object}  payeeID - payee Id to be deleted.
     */
    PayAPerson_PresentationController.prototype.deleteRecipient = function(payeeID) {
        this.showProgressBar();
        var params = {
            "PayPersonId": payeeID
        };
        applicationManager.getRecipientsManager().deleteP2PRecipient(params, this.deleteRecipientSuccess.bind(this), this.deleteRecipientFailure.bind(this));
    };
    /**
     * This method acts as the success Call back for the deleteRecipient.
     * @param {object}  response - status of deleting a recipient - success or failure.
     */
    PayAPerson_PresentationController.prototype.deleteRecipientSuccess = function(response) {
        this.fetchCurrentRecipientsList();
    };
    /**
     * This method acts as the failure method for deleteRecipient.
     * @param {object}  payeeID - payee Id to be deleted.
     */
    PayAPerson_PresentationController.prototype.deleteRecipientFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            "inFormError": response
        }, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method acts as the success call back method for edit recipient.
     * @param {object}  payPersonJSON - contains info like name, nickname, phone, email and primary contact.
     * @param {object} response - contains the response for edit recipient service.
     */
    PayAPerson_PresentationController.prototype.editRecipientSuccess = function(payPersonJSON, response) {
        var viewProperties = {};
        viewProperties.editRecipientAcknowledgement = payPersonJSON;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used as the failure call back for the edit recipient
     * @param {String} errmsg - Contains the error message for the service failure.
     */
    PayAPerson_PresentationController.prototype.editRecipientFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.editRecipientFailure = errmsg;
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used to edit recipient.
     * @param {object} - contains info like id, nickname, name,phone, email.
     */
    PayAPerson_PresentationController.prototype.editRecipient = function(payeeData) {
        this.showProgressBar();
        var payeeJSON = {
            "PayPersonId": payeeData.id,
            "nickName": payeeData.nickName,
            "name": payeeData.name,
            "phone": payeeData.phone,
            "email": payeeData.email,
            "secondaryEmail": payeeData.secondaryEmail,
            "secondaryPhoneNumber": payeeData.secondaryPhoneNumber,
            "primaryContactForSending": payeeData.primaryContactForSending
        };
        applicationManager.getRecipientsManager().editP2PRecipient(payeeJSON, this.editRecipientSuccess.bind(this, payeeJSON), this.editRecipientFailure.bind(this));
    };
    /**
     * This method is used to create a recipient.
     * @param {object} payPersonJSON - contains info like name, nickname, phone, email.
     */
    PayAPerson_PresentationController.prototype.createP2PPayee = function(payPersonJSON) {
        this.showProgressBar();
        applicationManager.getRecipientsManager().createP2PRecipient(payPersonJSON, this.createP2PPayeeSuccess.bind(this, payPersonJSON), this.createP2PPayeeFailure.bind(this));
    };
    /**
     * This method is used as the success call back for create pay a person recipient.
     * @param {object} payPersonJSON - contains info like name, nickname, phone, email.
     * @param {response} response - contains the response to the create recipient service.
     */
    PayAPerson_PresentationController.prototype.createP2PPayeeSuccess = function(payPersonJSON, response) {
        var self = this;
        if (payPersonJSON.transactionId) {
            payPersonJSON.PayPersonId = response.PayPersonId;
            self.updateP2PTransactionWithPayee(payPersonJSON);
        } else {
            var viewProperties = {};
            viewProperties.addRecipientAcknowledgement = {};
            viewProperties.addRecipientAcknowledgement.payPersonJSON = payPersonJSON;
            viewProperties.addRecipientAcknowledgement.result = response;
            applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        }
        this.hideProgressBar();
    };
    /**
     *This method is used as the failure call back for the create recipient.
     * @param {String} errmsg - error message for the failure of create recipient.
     */
    PayAPerson_PresentationController.prototype.createP2PPayeeFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used to update the transaciton with newly added recipient.
     * @param {object} res - contains the status for updating the transaction.
     */
    PayAPerson_PresentationController.prototype.updateP2PTransactionWithPayee = function(res) {
        this.showProgressBar();
        var self = this;
        var requestObj = {};
        requestObj.transactionId = res.transactionId;
        requestObj.personId = res.PayPersonId;
        var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var newTransaction = new TransactionModel({
            'transactionId': res.transactionId,
            'personId': res.PayPersonId
        });
        applicationManager.getTransactionManager().updateP2PTransaction(newTransaction, this.updateP2PTranasctionWithPayeeSuccess.bind(this, res), this.updateP2PTranasctionWithPayeeFailure.bind(this));
    };
    /**
     * This method acts as the success call back for updateP2PTransactionWithPayee.
     * @param {object} res - contains the status for updating the transaction.
     */
    PayAPerson_PresentationController.prototype.updateP2PTranasctionWithPayeeSuccess = function(requestObj, res) {
        var viewProperties = {};
        viewProperties.addRecipientAcknowledgement = {};
        viewProperties.addRecipientAcknowledgement.payPersonJSON = requestObj;
        viewProperties.addRecipientAcknowledgement.result = res;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used as the failure call back for the updateP2PTransactionWithPayee.
     * @param {object} errmsg - contains the error message for failure of updateP2PTransactionWithPayee.
     */
    PayAPerson_PresentationController.prototype.updateP2PTranasctionWithPayeeFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used to deactivate the pay a person.
     */
    PayAPerson_PresentationController.prototype.deactivateP2P = function() {
        this.showProgressBar();
        applicationManager.getUserPreferencesManager().deactivateP2P(this.deactivateP2PSuccess.bind(this), this.deactivateP2PFailure.bind(this));
    };
    /**
     * This method is used as the success call back for the deactivateP2P.
     * @param {object} response - contains the success message for  deactivateP2P.
     */
    PayAPerson_PresentationController.prototype.deactivateP2PSuccess = function(response) {
        var viewProperties = {};
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        viewProperties.deactivationAcknowledgement = {
            "userName": userPreferencesManager.getUserObj()["userfirstname"] + " " + userPreferencesManager.getUserObj()["userlastname"],
            "phone": this.getPrimaryDetails(userPreferencesManager.getEntitlementPhoneNumbers()),
            "email": this.getPrimaryDetails(userPreferencesManager.getEntitlementEmailIds()),
            "defaultAccountForDeposit": userPreferencesManager.getUserObj()["default_to_account_p2p"],
            "defaultAccountForSending": userPreferencesManager.getUserObj()["default_from_account_p2p"]
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used as the failure call back for the deactivateP2P.
     * @param {object} errmsg - contains the error message for  deactivateP2P.
     */
    PayAPerson_PresentationController.prototype.deactivateP2PFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used to load accounts module.
     */
    PayAPerson_PresentationController.prototype.loadAccountsModule = function() {
        this.showProgressBar();
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule');
        accountsModule.presentationController.showAccountsDashboard();
    };
    /**
     * This method is used to update the view of pay a person.
     * @param {String} frm - denotes the form name.
     * @param {object} data - object contains the keymap of viewProperties to be updated.
     */
    PayAPerson_PresentationController.prototype.showView = function(frm, data) {
        applicationManager.getNavigationManager().updateForm(data, frm);
    };
    /**
     * This method is used to update the pay a person preferences for a user.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     */
    PayAPerson_PresentationController.prototype.updateP2PPreferencesForUser = function(preferencesObject) {
        this.showProgressBar();
        var param = {
            "default_to_account_p2p": preferencesObject.defaultToAccount,
            "default_from_account_p2p": preferencesObject.defaultFromAccount
        };
        applicationManager.getUserPreferencesManager().updateP2PPreferencesForUser(param, this.updateP2PPreferencesForUserSuccess.bind(this, param), this.updateP2PPreferencesForUserFailure.bind(this));
    };
    /**
     * This method acts as the success call back for the update pay a person preferences service.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     * @param {Object} response - contins the status for the update pay a person preferences service call. with success or failure.
     */
    PayAPerson_PresentationController.prototype.updateP2PPreferencesForUserSuccess = function(preferencesObject, response) {
        applicationManager.getUserPreferencesManager().fetchUser(this.showPayAPerson.bind(this), this.updateP2PPreferencesForUserFailure.bind(this));
    };
    /**
     * This method is used as the failure call back for the update pay a person preferences service.
     * @param {String} errmsg - contains error message.
     */
    PayAPerson_PresentationController.prototype.updateP2PPreferencesForUserFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * getPayPersonViewActivity - fetches pay a person recipient activity list.
     * @param {object} - contanis payPersonId.
     */
    PayAPerson_PresentationController.prototype.getRecipientActivity = function(payPersonId) {
        this.showProgressBar();
        applicationManager.getTransactionManager().getRecipientActivity({
            "personId": payPersonId
        }, this.getRecipientActivitySuccess.bind(this), this.getRecipientActivityFailure.bind(this));
    };
    /**
     * This method acts as the success call back for the getRecipientActivity.
     * @param {array} response - contanis the list of transactions.
     */
    PayAPerson_PresentationController.prototype.getRecipientActivitySuccess = function(response) {
        var viewProperties = {};
        viewProperties.showPayAPersonActivity = response;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method acts as teh failure call back for the getRecipientActivity.
     * @param {String} - contanis error message.
     */
    PayAPerson_PresentationController.prototype.getRecipientActivityFailure = function(errMsg) {
        applicationManager.getNavigationManager().updateForm({
            "inFormError": errMsg
        }, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * ActivateP2P - This method is used to activate pay a person Service for a user.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     */
    PayAPerson_PresentationController.prototype.ActivateP2P = function(preferencesObject) {
        this.showProgressBar();
        applicationManager.getUserPreferencesManager().activateP2P(this.ActivateP2PSuccess.bind(this, preferencesObject), this.ActivateP2PFailure.bind(this));
    };
    /**
     * This method acts as the success call back for the ActivateP2P method.
     * @param {object} preferencesObject - contains default to account for p2p , default from account for p2p.
     * @Param {object} response - contains the response object for activate P2p.
     */
    PayAPerson_PresentationController.prototype.ActivateP2PSuccess = function(preferencesObject, response) {
        this.updateP2PPreferencesForUser(preferencesObject);
    };
    /**
     * This method acts as the failure call back for the Activate Pay a person Service.
     * @param {String} errmsg - contains error message.
     */
    PayAPerson_PresentationController.prototype.ActivateP2PFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used to show ProgressBar.
     */
    PayAPerson_PresentationController.prototype.showProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        }, "frmPayAPerson");
    };
    /**
     * This method is used to hide ProgressBar.
     */
    PayAPerson_PresentationController.prototype.hideProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": false
        }, "frmPayAPerson");
    };
    /**
     *  This method is used to cancel a transaction series.
     * @param {Object} transaction - this contains the transaction object.
     */
    PayAPerson_PresentationController.prototype.cancelTransactionSeries = function(transaction) {
        this.showProgressBar();
        applicationManager.getTransactionManager().deleteP2PTransaction({
            transactionId: transaction.transactionId,
            transactionType: transaction.transactionType
        }, this.cancelTransactionSeriesSuccess.bind(this), this.cancelTransactionSeriesFailure.bind(this));
    };
    /**
     *  This method is used as the success call back for the cancelTransaction series.
     * @param {Object} response - This contains the response to the cancelTransaction Series.
     */
    PayAPerson_PresentationController.prototype.cancelTransactionSeriesSuccess = function(response) {
        this.showSentTransactionsView();
    };
    /**
     *  This method is used as the failure call back for the cancelTransaction Series.
     * @param {Object} errmsg - This contains the errmsg for the service.
     */
    PayAPerson_PresentationController.prototype.cancelTransactionSeriesFailure = function(errmsg) {
        applicationManager.getNavigationManager().updateForm({
            "inFormError": errmsg
        }, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     *  This method is used to cancel a transaction occurrence.
     * @param {Object} transaction - this contains the transaction object.
     */
    PayAPerson_PresentationController.prototype.cancelTransactionOccurrence = function(transaction) {
        this.showProgressBar();
        applicationManager.getTransactionManager().deleteP2PRecurrenceTransaction(transaction, this.cancelTransactionOccurrenceSuccess.bind(this), this.cancelTransactionOccurrenceFailure.bind(this));
    };
    /**
     *  This method is used as the success call back for the cancelTransaction series.
     * @param {Object} response - This contains the response to the cancelTransaction Series.
     */
    PayAPerson_PresentationController.prototype.cancelTransactionOccurrenceSuccess = function(response) {
        this.showSentTransactionsView();
    };
    /**
     *  This method is used as the failure call back for the cancelTransaction Series.
     * @param {Object} errmsg - This contains the errmsg for the service.
     */
    PayAPerson_PresentationController.prototype.cancelTransactionOccurrenceFailure = function(errmsg) {
        applicationManager.getNavigationManager().updateForm({
            "inFormError": errmsg
        }, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used to cancel a transaction.
     * @param {Object} transaction - this contains the transaction object.
     */
    PayAPerson_PresentationController.prototype.cancelTransaction = function(transaction) {
        this.showProgressBar();
        applicationManager.getTransactionManager().deleteP2PTransaction({
            transactionId: transaction.transactionId,
            transactionType: transaction.transactionType
        }, this.cancelTransactionSuccess.bind(this), this.cancelTransactionFailure.bind(this));
    };
    /**
     *  This method is used as the success call back for the cancelTransaction.
     * @param {Object} response - This contains the response to the cancelTransaction.
     */
    PayAPerson_PresentationController.prototype.cancelTransactionSuccess = function(response) {
        this.showSentTransactionsView();
    };
    /**
     *  This method is used as the failure call back for the cancelTransaction .
     * @param {Object} errmsg - This contains the errmsg for the service.
     */
    PayAPerson_PresentationController.prototype.cancelTransactionFailure = function(errmsg) {
        applicationManager.getNavigationManager().updateForm({
            "inFormError": errmsg
        }, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used to get the default p2p TO account
     */
    PayAPerson_PresentationController.prototype.getDefaultP2PToAccount = function() {
        return applicationManager.getUserPreferencesManager().getDefaultToAccountforP2P();
    };
    /**
     * This method is used to get the default p2p FROM account
     */
    PayAPerson_PresentationController.prototype.getDefaultP2PFromAccount = function() {
        return applicationManager.getUserPreferencesManager().getDefaultFromAccountforP2P();
    };
    /**
     * This method is used to show the send money screen in pay a person.
     * @param {Object} data - This contains the data to be shown in the send money screen.
     */
    PayAPerson_PresentationController.prototype.onSendMoney = function(data) {
        this.showProgressBar();
        var self = this;
        var viewProperties = {};
        if (data["amount"])
            data["amount"] = "" + data["amount"];
        viewProperties.sendMoneyData = data;
        viewProperties.paymentAccounts = applicationManager.getAccountManager().getP2PFromSupportedAccounts();
        viewProperties.sendPaymentAccounts = viewProperties.paymentAccounts.map(generateFromAccounts);
        viewProperties.accountCurrency = viewProperties.paymentAccounts.map(generateAccountsWithCurrency);
        if (self.selectedAccountId) {
            viewProperties.sendMoneyData.fromAccountNumber = self.selectedAccountId;
            self.selectedAccountId = null;
        }
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used to check if the entered amount is eligible to start MFA in pay a person send money screen.
     * @param {Object} requestObj - contains the info about pay a person recipient - name, nickname, email, phone and transaction object fields.
     */
    PayAPerson_PresentationController.prototype.checkMFAP2PSendMoney = function(requestObj) {
        var self = this;
        self.createP2PSendMoney(requestObj);
    };
    /**
     * This method is used to check if the entered amount in the send money screen is valid amount or not.The range values are obtained from the entitlements.
     * @param {Number} amount - contains the amount value.
     */
    PayAPerson_PresentationController.prototype.validatePayAPersonAmount = function(data) {
        var result = {
            isAmountValid: false
        };
        var userAccounts = this.p2pPaymentLimits.accounts;
        var userLimits = {};
        for (var i = 0; i < userAccounts.length; i++) {
            if (userAccounts[i].accountId === data.fromAccountNumber) {
                userLimits = userAccounts[i].limits;
                break;
            }
        }
        if (parseFloat(data.amount) < parseFloat(userLimits.MIN_TRANSACTION_LIMIT)) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.minTransactionError") + " " + this.formatAmount(userLimits.MIN_TRANSACTION_LIMIT);
        } else if (parseFloat(data.amount) > parseFloat(userLimits.MAX_TRANSACTION_LIMIT)) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.maxTransactionError") + " " + this.formatAmount(userLimits.MAX_TRANSACTION_LIMIT);
        } else {
            if (userLimits.PRE_APPROVED_TRANSACTION_LIMIT && userLimits.AUTO_DENIED_TRANSACTION_LIMIT) {
                if (parseFloat(data.amount) > parseFloat(userLimits.AUTO_DENIED_TRANSACTION_LIMIT)) {
                    result.errMsg = kony.i18n.getLocalizedString("i18n.common.maxTransactionError") + " " + this.formatAmount(userLimits.AUTO_DENIED_TRANSACTION_LIMIT);
                } else {
                    result.isAmountValid = true;
                }
            } else {
                result.isAmountValid = true;
            }
        }
        return result;
    };
    /**
     * used to Format the amount
     * @param {string} amount amount
     * @param {boolean} currencySymbolNotRequired currency symbol required
     * @returns {string} formated amount
     */
    PayAPerson_PresentationController.prototype.formatAmount = function(amount, currencySymbolNotRequired) {
        if (currencySymbolNotRequired) {
            return applicationManager.getFormatUtilManager().formatAmount(amount);
        } else {
            return applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(amount);
        }
    };
    PayAPerson_PresentationController.prototype.fetchLimits = function() {
        applicationManager.getConfigurationManager().fetchLimitsForAnAction("P2P_CREATE", this.fetchLimitsSuccess.bind(this), this.fetchLimitsError);
    };
    /**
     * fetch transaction limits succcess callback
     */
    PayAPerson_PresentationController.prototype.fetchLimitsSuccess = function(response) {
        this.p2pPaymentLimits = response;
        this.showConfimPage();
    };
    /**
     * fetch transaction limits failure callback
     */
    PayAPerson_PresentationController.prototype.fetchLimitsError = function(response) {
        CommonUtilities.showServerDownScreen();
    };
    /**
     * This method is used to create a send money transaction in pay a person.
     * @param {Object} requestObj - This contains info about the pay a person recipient and the transaction object.
     */
    PayAPerson_PresentationController.prototype.createP2PSendMoney = function(requestObj) {
        this.requestObj = requestObj;
        var mfaManager = applicationManager.getMFAManager();
        var displayName = "PayAPerson";
        applicationManager.getPresentationUtility().MFA.getServiceIdBasedOnDisplayName(displayName);
        var mfaParams = {
            serviceName: mfaManager.getServiceId(),
        };
        this.showProgressBar("frmPayAPerson");
        var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        if (requestObj.frequencyType == applicationManager.getConfigurationManager().OLBConstants.TRANSACTION_RECURRENCE.ONCE) {
            this.requestObj.numberOfRecurrences = "";
            this.requestObj.frequencyStartDate = "";
            this.requestObj.frequencyEndDate = "";
            this.requestObj.frequencyType = this.requestObj.frequencyType;
        } else if (this.requestObj.hasHowLong == "NO_OF_RECURRENCES") {
            this.requestObj.frequencyStartDate = "";
            this.requestObj.frequencyEndDate = "";
            this.requestObj.frequencyType = this.requestObj.frequencyType;
            this.requestObj.numberOfRecurrences = this.requestObj.numberOfRecurrences;
        } else if (this.requestObj.hasHowLong == "ON_SPECIFIC_DATE") {
            this.requestObj.numberOfRecurrences = "";
            this.requestObj.frequencyType = this.requestObj.frequencyType;
            this.requestObj.frequencyEndDate = this.requestObj.frequencyEndDate;
            this.requestObj.frequencyStartDate = this.requestObj.frequencyStartDate;
        }
        var newTransaction = new transactionsModel({
            'fromAccountNumber': this.requestObj.fromAccountNumber,
            'amount': (Number(this.requestObj.amount)).toFixed(2),
            'transactionsNotes': this.requestObj.transactionsNotes,
            'toAccountNumber': this.requestObj.toAccountNumber,
            'frequencyType': this.requestObj.frequencyType,
            'transactionType': "P2P",
            'isScheduled': this.requestObj.isScheduled,
            'scheduledDate': this.requestObj.scheduledDate,
            'personId': this.requestObj.personId,
            'p2pContact': this.requestObj.p2pContact,
            'numberOfRecurrences': this.requestObj.numberOfRecurrences,
            'frequencyStartDate': this.requestObj.frequencyStartDate,
            'frequencyEndDate': this.requestObj.frequencyEndDate,
            'transactionId': this.requestObj.transactionId,
            'MFAAttributes': mfaParams,
            'transactionCurrency': this.requestObj.transactionCurrency,
            'fee': applicationManager.getConfigurationManager().serviceFeeFlag === "true" ? applicationManager.getConfigurationManager().p2pServiceFee : ""
        });
        /**
         * Used for saving payee name in one time payment flow.
         */
        if (this.requestObj.context === "sendMoneyToNewRecipient") {
            newTransaction.payPersonName = this.requestObj.name, newTransaction.payPersonNickName = this.requestObj.nickName
        }
        if (this.requestObj.transactionId != null && this.requestObj.transactionId != "" && this.requestObj.transactionId !== undefined) {
            mfaManager.setMFAFlowType("P2P_EDIT");
            applicationManager.getTransactionManager().updateP2PTransaction(newTransaction, this.createP2PSendMoneySuccess.bind(this), this.createP2PSendMoneyFailure.bind(this));
        } else {
            if (applicationManager.getConfigurationManager().serviceFeeFlag === "true") {
                this.requestObj.fee = applicationManager.getConfigurationManager().p2pServiceFee;
            }
            mfaManager.setMFAFlowType("P2P_CREATE");
            applicationManager.getTransactionManager().createP2PTransaction(newTransaction, this.createP2PSendMoneySuccess.bind(this), this.createP2PSendMoneyFailure.bind(this));
        }
    };
    /**
     * This method is used as the success call back for the create send money.
     * @param {Object} requestObj - This contains info about the pay a person recipient and the transaction object.
     * @param {Object} responseData - contains the transactionId and success or failure info.
     */
    PayAPerson_PresentationController.prototype.createP2PSendMoneySuccess = function(response) {
        var viewProperties = {};
        var mfaManager = applicationManager.getMFAManager();
        if (response.referenceId || response.transactionId) {
            viewProperties.showRequestSendMoneyAck = true;
            viewProperties.status = response;
            viewProperties.requestObj = this.requestObj;
            this.getAccountByID(this.requestObj.fromAccountNumber, viewProperties);
        } else {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": applicationManager.getMFAManager().getMFAFlowType(),
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }
    };
    /**
     * This method is used to get the account details based on the from account number.
     * @param {Number} accountID - contains the account number.
     * @param {Object} viewProperties - contains the set of view properties map to be updated in pay a person form.
     */
    PayAPerson_PresentationController.prototype.getAccountByID = function(accountID, viewProperties) {
        var accountManager = applicationManager.getAccountManager();
        accountManager.fetchInternalAccounts(this.fetchUserAccountAndNavigateSuccess.bind(this, accountID, viewProperties), this.fetchUserAccountAndNavigatesFailure.bind(this));
    };
    PayAPerson_PresentationController.prototype.fetchUserAccountAndNavigateSuccess = function(accountID, viewProperties) {
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        var account = applicationManager.getAccountManager().getInternalAccountByID(accountID);
        viewProperties.accountBalance = account.availableBalance;
        viewProperties.accountName = account.accountName;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar("frmPayAPerson");
    };
    PayAPerson_PresentationController.prototype.fetchUserAccountAndNavigatesFailure = function() {
        CommonUtilities.showServerDownScreen();
    }
    /**
     * This method is used as the failure call back for the create p2p send money.
     * @param {Obejct} requestObj - This contains info about the pay a person recipient and the transaction object.
     * @param {Object} errmsg - contains the error message in it.
     */
    PayAPerson_PresentationController.prototype.createP2PSendMoneyFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        if (applicationManager.getConfigurationManager().serviceFeeFlag === "true") {
            this.requestObj.amount = String(Number(this.requestObj.amount) - Number(this.requestObj.fee));
        }
        this.requestObj.frequencyStartDate = CommonUtilities.sendDateToBackend(this.requestObj.frequencyStartDate);
        this.requestObj.frequencyEndDate = CommonUtilities.sendDateToBackend(this.requestObj.frequencyEndDate);
        this.requestObj.scheduledDate = CommonUtilities.sendDateToBackend(this.requestObj.scheduledDate);
        viewProperties.sendMoneyData = this.requestObj;
        applicationManager.getNavigationManager().navigateTo("frmPayAPerson");
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar("frmPayAPerson");
    };
    /**
     * This method is used to show the pay a person settings screen in pay a person.
     * @param {Object} viewProperties - contains the list of properties to be updated in the pay a person screen.
     */
    PayAPerson_PresentationController.prototype.loadP2PSettingsScreen = function(viewProperties) {
        this.showProgressBar("frmPayAPerson");
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        viewProperties.userJSON = {
            "userName": userPreferencesManager.getUserObj()["userfirstname"] + " " + userPreferencesManager.getUserObj()["userlastname"],
            "phone": this.getPrimaryDetails(userPreferencesManager.getEntitlementPhoneNumbers()),
            "email": this.getPrimaryDetails(userPreferencesManager.getEntitlementEmailIds())
        };
        viewProperties.paymentAccounts = applicationManager.getAccountManager().getInternalAccounts();
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
    };
    /**
     * Method to get user's primary details
     */
    PayAPerson_PresentationController.prototype.getPrimaryDetails = function(data) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].isPrimary === "true") {
                return data[i].Value;
            }
        }
    };
    /**
     * This method is used to search for recipients.
     * @param {String} data - Contains the search string.
     */
    PayAPerson_PresentationController.prototype.searchPayAPerson = function(data) {
        this.showProgressBar("frmPayAPerson");
        var self = this;
        if (data && data.searchKeyword.length >= 0) {
            var searchInputs = {
                'searchString': data.searchKeyword
            };
            //var criteria = kony.mvc.Expression.eq("searchString", searchInputs.searchString);
            applicationManager.getRecipientsManager().getP2PRecipientList(searchInputs, this.searchPayAPersonSuccess.bind(this, searchInputs), this.searchPayAPersonFailure.bind(this));
        }
    };
    /**
     * This method acts as the success call back for the searchPayAPerson method.
     * @param {Object} searchInputs - contains the search string used for the search.
     * @param {Object} response - contains the response for the search.
     */
    PayAPerson_PresentationController.prototype.searchPayAPersonSuccess = function(searchInputs, response) {
        var viewProperties = {};
        viewProperties.searchPayAPerson = {
            payAPersonData: response,
            searchInputs: searchInputs
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
    };
    /**
     * This method is used as the failure callback for the searchPayAPerson.
     * @param {String} errmsg - this contains the error message for the pay a person.
     */
    PayAPerson_PresentationController.prototype.searchPayAPersonFailure = function(errmsg) {
        var viewProperties = {};
        viewProperties.inFormError = errmsg;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    /**
     * This method is used to check the eligibility of user for p2p service.
     */
    PayAPerson_PresentationController.prototype.checkP2pEligibility = function() {
        return applicationManager.getUserPreferencesManager().getUserObj()["isP2PActivated"];
    };
    /**
     *  Method used to show send money to new recipient.
     */
    PayAPerson_PresentationController.prototype.showSendMoneyToNewRecipientView = function() {
        var viewProperties = {};
        viewProperties.showSendMoneyToNewRecipientView = true;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmPayAPerson");
        this.hideProgressBar();
    };
    //need to update with async
    PayAPerson_PresentationController.prototype.showConfimPage = function(dataItem) {
        if (dataItem) this.dataItem = dataItem;
        if (!this.p2pPaymentLimits && !this.TnCresponse) {
            this.showProgressBar();
            this.fetchLimits();
            this.getTnCP2PTransfer();
        }
        if (this.p2pPaymentLimits && this.TnCresponse) {
            applicationManager.getNavigationManager().updateForm({
                "TnCcontentTransfer": this.TnCresponse,
                "PayeeObj": this.dataItem
            }, "frmPayAPerson");
            this.hideProgressBar();
        }
    }
    PayAPerson_PresentationController.prototype.getTnCP2PTransfer = function() {
        var self = this;
        var termsAndConditionModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TermsAndConditionsModule");
        termsAndConditionModule.presentationController.showTermsAndConditions(OLBConstants.TNC_FLOW_TYPES.P2P_TnC, self.getTnCOnSuccess.bind(self), self.getTnCOnFailure.bind(self));
    };
    PayAPerson_PresentationController.prototype.getTnCOnSuccess = function(TnCresponse) {
        this.TnCresponse = TnCresponse;
        this.showConfimPage();
    };
    PayAPerson_PresentationController.prototype.getTnCOnFailure = function() {};
    /** fetch and download the transaction report of the requested transaction
     * @param {object} transaction object with transaction id of the transaction whose report has to be downloaded.
     */
    PayAPerson_PresentationController.prototype.downloadTransactionReport = function(transactionObj) {
      this.showProgressBar();
      let params = {
        "transactionId": transactionObj.transactionId
      };
      applicationManager.getTransactionManager().generateTransactionReport(params, this.generateTransactionReportSuccess.bind(this), this.generateTransactionReportFailure.bind(this));
    };

    PayAPerson_PresentationController.prototype.generateTransactionReportSuccess = function(successResponse) {
      var downloadReportURL = applicationManager.getTransactionManager().fetchTransactionReport(successResponse);
      var data = {
        "url": downloadReportURL
      };
      CommonUtilities.downloadFile(data);
      this.hideProgressBar();
    };
  
    PayAPerson_PresentationController.prototype.generateTransactionReportFailure = function(error) {
      this.hideProgressBar();
      applicationManager.getNavigationManager().updateForm({
        "serverError": error
      })
    };


    return PayAPerson_PresentationController;
});