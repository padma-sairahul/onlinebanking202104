define(['CommonUtilities', 'IBANUtils', 'OLBConstants'], function(CommonUtilities, IBANUtils, OLBConstants) {
    var wireTransferForm = "frmWireTransfer";
    var progressBarShowCount = 0;
    this.transferData = "";

    function WireTransferPresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    var recipientSortConfig = {
        'sortBy': 'nickName',
        'defaultSortBy': 'nickName',
        'order': OLBConstants.ASCENDING_KEY,
        'defaultOrder': OLBConstants.ASCENDING_KEY,
        'offset': OLBConstants.DEFAULT_OFFSET,
        'limit': OLBConstants.PAGING_ROWS_LIMIT
    }
    var recentTransactionSortConfig = {
        'sortBy': 'transactionDate',
        'defaultSortBy': 'transactionDate',
        'order': OLBConstants.DESCENDING_KEY,
        'defaultOrder': OLBConstants.DESCENDING_KEY,
        'offset': OLBConstants.DEFAULT_OFFSET,
        'limit': OLBConstants.PAGING_ROWS_LIMIT
    }
    inheritsFrom(WireTransferPresentationController, kony.mvc.Presentation.BasePresenter);
    /**Entry Point method - Checks for wire transfer activation and shows appropriate view . Check configuration inside.
     * @param {Object} context Context for initial view
     * @param {Transaction} [context.transactionObject] Transaction object for repeating wire transfer.
     * @param {string} [context.landingPageView] For Specifying what view to show makeTransfer|wireTransferHistory|myRecipients|addRecipient
     */
    WireTransferPresentationController.prototype.showWireTransfer = function(context) {
        context = context || {};
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.resetValues();
        applicationManager.getNavigationManager().navigateTo(wireTransferForm);
        applicationManager.getNavigationManager().updateForm({
            resetForm: {}
        })
        if (!this.checkForActivation()) {
            this.showActivationScreen();
        } else {
            this.showLandingPageView(context.landingPageView);
        }
    }
    /** Present frmPrintTransfer
     * @param {object} viewModel Details of the Account
     */
    WireTransferPresentationController.prototype.showPrintPage = function(data) {
        applicationManager.getNavigationManager().navigateTo("frmPrintTransfer");
        applicationManager.getNavigationManager().updateForm(data, 'frmPrintTransfer');
    }
    /**
     * Shows the landing page view
     * @param {string} [landingPageView=makeTransfer] For Specifying what view to show makeTransfer|wireTransferHistory|myRecipients|addRecipient
     */
    WireTransferPresentationController.prototype.showLandingPageView = function(landingPageView) {
        landingPageView = landingPageView || "makeTransfer";
        switch (landingPageView) {
            case "makeTransfer":
                this.showMakeTransferRecipientList();
                break;
            case "wireTransferHistory":
                this.showRecentTransactions();
                break;
            case "myRecipients":
                this.showManageRecipientList();
                break;
            case "addRecipient":
                this.showAddPayee(applicationManager.getConfigurationManager().OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC);
                break;
            case "oneTimeTransfer":
                this.showOneTimeTransfer();
                break;
            default:
                this.showMakeTransferRecipientList();
        }
    }
    /**
     * Checkes from cached user preferences for wire transfer activation
     * @returns {boolean} returns true if wire transfer is activated or not
     */
    WireTransferPresentationController.prototype.checkForActivation = function() {
        var userObj = applicationManager.getUserPreferencesManager().getUserObj();
        var wireTransferEligible = userObj.isWireTransferEligible;
        var wireTransferActivated = userObj.isWireTransferActivated;
        //Known MF issue - boolean and string both checks needed
        return (wireTransferActivated === "true" || wireTransferActivated === true);
    }
    /**
     * Calls UserPreferencesManager for activating wire transfer for user
     * @param {string} defaultAccountForWireTransfers Default Account for wire transfers
     */
    WireTransferPresentationController.prototype.activateWireTransfer = function(defaultAccountForWireTransfers) {
        this.showProgressBar();
        applicationManager.getUserPreferencesManager().activateWireTransferForUser(defaultAccountForWireTransfers, this.onActivationSuccess.bind(this), this.onActivationError.bind(this));
    }
    /**
     * Calls if activation is successfull for user, and show landing page view   *
     */
    WireTransferPresentationController.prototype.onActivationSuccess = function() {
        this.hideProgressBar();
        var userObj = applicationManager.getUserPreferencesManager().getUserObj();
        userObj.isWireTransferActivated = "true";
        this.showLandingPageView();
    }
    /**
     * Calls if activation is not successfull for user, and shows error page
     */
    WireTransferPresentationController.prototype.onActivationError = function() {
        CommonUtilities.showServerDownScreen();
    }
    /**
     * Shows Wire Transfer Activation Screen
     */
    WireTransferPresentationController.prototype.showActivationScreen = function() {
        this.showProgressBar();
        applicationManager.getAccountManager().fetchCheckingAccounts(this.onCheckingAccountSuccess.bind(this), this.serverError.bind(this));
    }
    /**
     * @param {Account[]} checkingAccounts List of checking accounts
     * Called when fetching checking accounts is successfull for activation
     */
    WireTransferPresentationController.prototype.onCheckingAccountSuccess = function(checkingAccounts) {
        this.hideProgressBar();
        applicationManager.getNavigationManager().updateForm({
            activationScreen: {
                checkingAccounts: checkingAccounts
            }
        }, wireTransferForm)
    }
    /**
     * Shows make transfer recipient list.
     */
    WireTransferPresentationController.prototype.showMakeTransferRecipientList = function(sortingInputs) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.resetValues();
        this.fetchMakeTransferRecipientList(sortingInputs);
    }
    /**
     * Shows make transfer recipient list.
     */
    WireTransferPresentationController.prototype.fetchMakeTransferRecipientList = function(sortingInputs) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        var searchString = sortingInputs ? sortingInputs.searchString : null;
        var params = {};
        if (typeof searchString === "string" && searchString.length > 0) {
            params.searchString = searchString;
        } else {
            params = paginationManager.getValues(recipientSortConfig, sortingInputs);
        }
        applicationManager.getRecipientsManager().fetchWireTransferRecipients(params, this.makeTransferRecipientFetchSuccess.bind(this, searchString), this.serverError.bind(this));
    }
    /**
     * Called when recipient list fetch is successful for make transfer
     * @param {Payee[]} wireTransferRecipients Array of Payee Objects to be shown on UI
     */
    WireTransferPresentationController.prototype.makeTransferRecipientFetchSuccess = function(searchString, wireTransferRecipients) {
        var paginationValues = applicationManager.getPaginationManager().getValues(recipientSortConfig);
        paginationValues.limit = wireTransferRecipients.length;
        this.hideProgressBar();
        if (wireTransferRecipients.length > 0 || paginationValues.offset === 0) {
            applicationManager.getPaginationManager().updatePaginationValues();
            applicationManager.getNavigationManager().updateForm({
                makeTransferRecipients: {
                    recipients: wireTransferRecipients,
                    config: paginationValues,
                    searchString: searchString
                }
            })
        } else if (paginationValues.offset !== 0) {
            applicationManager.getNavigationManager().updateForm({
                noMoreRecords: true
            });
        }
    }
    /**
     * Increments the page and fetch the new page for make transfer recipients
     */
    WireTransferPresentationController.prototype.getNextMakeTransferRecipients = function() {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getNextPage();
        this.fetchMakeTransferRecipientList();
    }
    /**
     * Decrements the page and fetch the previous page for make transfer recipients
     */
    WireTransferPresentationController.prototype.getPrevioustMakeTransferRecipients = function() {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getPreviousPage();
        this.fetchMakeTransferRecipientList();
    }
    /**
     * Shows make transfer recipient list.
     */
    WireTransferPresentationController.prototype.showManageRecipientList = function(sortingInputs) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.resetValues();
        this.fetchManageRecipientList(sortingInputs);
    }
    /**
     * Shows make transfer recipient list.
     */
    WireTransferPresentationController.prototype.fetchManageRecipientList = function(sortingInputs) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        var searchString = sortingInputs ? sortingInputs.searchString : null;
        var params = {};
        if (typeof searchString === "string" && searchString.length > 0) {
            params.searchString = searchString;
        } else {
            params = paginationManager.getValues(recipientSortConfig, sortingInputs);
        }
        applicationManager.getRecipientsManager().fetchWireTransferRecipients(params, this.manageRecipientFetchSuccess.bind(this, searchString), this.serverError.bind(this));
    }
    /**
     * Called when recipient list fetch is successful for make transfer
     * @param {Payee[]} wireTransferRecipients Array of Payee Objects to be shown on UI
     */
    WireTransferPresentationController.prototype.manageRecipientFetchSuccess = function(searchString, wireTransferRecipients) {
        var paginationValues = applicationManager.getPaginationManager().getValues(recipientSortConfig);
        paginationValues.limit = wireTransferRecipients.length;
        this.hideProgressBar();
        if (wireTransferRecipients.length > 0 || paginationValues.offset === 0) {
            applicationManager.getPaginationManager().updatePaginationValues();
            applicationManager.getNavigationManager().updateForm({
                manageRecipients: {
                    recipients: wireTransferRecipients,
                    config: paginationValues,
                    searchString: searchString
                }
            })
        } else if (paginationValues.offset !== 0) {
            applicationManager.getNavigationManager().updateForm({
                noMoreRecords: true
            });
        }
        this.hideProgressBar();
    }
    /**
     * Increments the page and fetch the new page for make transfer recipients
     */
    WireTransferPresentationController.prototype.getNextManageRecipients = function() {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getNextPage();
        this.fetchManageRecipientList();
    }
    /**
     * Decrements the page and fetch the previous page for make transfer recipients
     */
    WireTransferPresentationController.prototype.getPreviousManageRecipients = function() {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getPreviousPage();
        this.fetchManageRecipientList();
    }
    WireTransferPresentationController.prototype.updateRecipient = function(editRecipient, recipientAccountDetails, config) {
        var data = {
            "payeeName": editRecipient.payeeName,
            "zipCode": editRecipient.zipCode,
            "cityName": editRecipient.cityName,
            "state": editRecipient.state,
            "addressLine1": editRecipient.addressLine1,
            "addressLine2": editRecipient.addressLine2,
            "type": editRecipient.type,
            "country": editRecipient.country,
            "payeeId": editRecipient.payeeId,
            "wireAccountType": editRecipient.wireAccountType,
            "routingCode": recipientAccountDetails.routingCode,
            "swiftCode": recipientAccountDetails.swiftCode,
            "internationalRoutingCode": recipientAccountDetails.internationalRoutingCode,
            "accountNumber": recipientAccountDetails.accountNumber,
            "payeeNickName": recipientAccountDetails.payeeNickName,
            "bankName": recipientAccountDetails.bankName,
            "bankAddressLine1": recipientAccountDetails.bankAddressLine1,
            "bankAddressLine2": recipientAccountDetails.bankAddressLine2,
            "bankCity": recipientAccountDetails.bankCity,
            "bankState": recipientAccountDetails.bankState,
            "bankZip": recipientAccountDetails.bankZip
        };
        applicationManager
            .getRecipientsManager()
            .updateWireTransferRecipient(data, this.onWireRecipientUpdatedSuccess.bind(this, data), this.onWireRecipientUpdateError.bind(this));
    }
    WireTransferPresentationController.prototype.serverError = function() {
        CommonUtilities.showServerDownScreen();
    }
    WireTransferPresentationController.prototype.onWireRecipientUpdatedSuccess = function(data) {
        applicationManager.getNavigationManager().updateForm({
            "editRecipientSuccessMsg": data
        });
    }
    WireTransferPresentationController.prototype.onWireRecipientUpdateError = function() {}
    /**
     * Noitifies the UI for loading state.
     */
    WireTransferPresentationController.prototype.showProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "isLoading": true
        });
    };
    /**
     * Shows Make Transfer for a Payee Object
     * @param {Payee} payeeObject Payee Object for make transfer needs to be shown
     * @param {Object} [transactionObject] Transaction Object for repeat
     */
    WireTransferPresentationController.prototype.showMakeTransferForPayee = function(payeeObject, transactionObject) {
        this.showProgressBar();
        applicationManager.getAccountManager().fetchCheckingAccounts(this.onCheckingAccountSuccessForMakeTransfer.bind(this, payeeObject, transactionObject), this.serverError.bind(this));
    }
    /**
     * Calls when checking accounts are successfully fetched for Make Transfer also shows the UI
     * @param {Payee} payeeObject Payee Object for make transfer needs to be shown
     * @param {Account[]} checkingAccounts Checking Accounts for selection
     */
    WireTransferPresentationController.prototype.onCheckingAccountSuccessForMakeTransfer = function(payeeObject, transactionObject, checkingAccounts) {
        this.hideProgressBar();
        applicationManager.getNavigationManager().updateForm({
            makeTransfer: {
                payee: payeeObject,
                checkingAccounts: checkingAccounts,
                transactionObject: transactionObject
            }
        });
    }
    /**
     * Starts MFA for Wire Transfer
     * @param {Object} transferData Object containg data for wire transfer
     */
    WireTransferPresentationController.prototype.authorizeWireTransfer = function(transferData) {
        //Removed OLD MFA
        this.createWireTransfer(transferData);
    }
    /**
     * Calls Transaction Manager and creates a transaction of wire transfer type.
     * @param {Object} transferData Object containing data for wie transfer
     */
    WireTransferPresentationController.prototype.createWireTransfer = function(transferData) {
        this.showProgressBar();
        var mfaManager = applicationManager.getMFAManager();
        mfaManager.setMFAOperationType("CREATE");
        var displayName = applicationManager.getPresentationUtility().MFA.getDisplayNameForTransfer(transferData.payeeObject.wireAccountType);
        applicationManager.getPresentationUtility().MFA.getServiceIdBasedOnDisplayName(displayName);
        var mfaParams = {
            serviceName: mfaManager.getServiceId(),
        };
        var transactionManager = applicationManager.getTransactionManager();
        var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var newTransaction = new transactionsModel({
            "amount": transferData.amount,
            "payeeCurrency": transferData.currency,
            "transactionCurrency": transferData.currency,
            "payeeId": transferData.payeeObject.payeeId,
            "transactionsNotes": transferData.reasonForTransfer,
            "fromAccountNumber": transferData.fromAccountNumber,
            //"fee":transferData.transactionFee,
            'MFAAttributes': mfaParams,
            "transactionType": applicationManager.getConfigurationManager().OLBConstants.TRANSACTION_TYPE.WIRE
        });
        transactionManager.createTransaction(newTransaction, this.showMakeTransferAcknowledgement.bind(this), this.showMakeTransferError.bind(this));
    }
    /**
     * Calls when wire transfer is successfull
     * @param {Object} transactionResponse Object containing transaction response
     */
    WireTransferPresentationController.prototype.showMakeTransferAcknowledgement = function(transactionResponse) {
        var mfaManager = applicationManager.getMFAManager();
        if (transactionResponse.referenceId) {
            this.hideProgressBar();
            applicationManager.getNavigationManager().navigateTo(wireTransferForm);
            applicationManager.getNavigationManager().updateForm({
                wireTransferAcknowledgement: {
                    referenceId: transactionResponse.referenceId
                }
            });
        } else {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "WIRE_TRANSFERS",
                "response": transactionResponse
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }
    };
    /**
     * Calls when wire transfer is not successfull
     * @param {Object} transactionResponse Object containing transaction response
     */
    WireTransferPresentationController.prototype.showMakeTransferError = function(transactionResponse) {
        this.hideProgressBar();
        applicationManager.getNavigationManager().navigateTo(wireTransferForm);
        applicationManager.getNavigationManager().updateForm({
            wireTransferError: {
                errorMessage: transactionResponse.errorMessage
            }
        })
    }
    /**
     * Deperecated Method - As its pointing to OLD method of multi factor authentication
     * Calls MultiFcator Authorization
     * @param {function} transferCallback Calls when MultiFactorAuthorization is successfull
     * @param {string|number} amount Amount of transaction, to decide whether to call multifactor authorization or not.
     * @param {string} [hamburgerItem] Hamburger Child item
     */
    WireTransferPresentationController.prototype.startMultiFactorAuthorization = function(transferCallback, amount, hamburgerItem) {
        if ((applicationManager.getConfigurationManager().isMFAEnabledForWireTransfer === "true") && Number(amount) > Number(applicationManager.getConfigurationManager().minimumAmountForMFAWireTransfer) && !CommonUtilities.isCSRMode()) {
            var mfaModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('MultiFactorAuthenticationModule');
            mfaModule.presentationController.startSecureAccessCodeFlow({
                hamburgerSelection: {
                    selection1: "WIRE TRANSFER",
                    selection2: hamburgerItem
                },
                breadcrumb: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer"),
                cancelCallback: this.showWireTransfer.bind(this),
                termsAndConditions: kony.i18n.getLocalizedString("i18n.WireTransfers.TandC"),
                successCallback: transferCallback
            });
        } else {
            transferCallback();
        }
    }
    /**
     * Method to show Recent Transactions
     */
    WireTransferPresentationController.prototype.showRecentTransactions = function(sortingInputs) {
        this.showProgressBar();
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.resetValues();
        this.fetchWireTransactions(sortingInputs);
    }
    /**
     * Shows make transfer recipient list.
     */
    WireTransferPresentationController.prototype.fetchWireTransactions = function(sortingInputs) {
        var paginationManager = applicationManager.getPaginationManager();
        var paginationValues = paginationManager.getValues(recentTransactionSortConfig, sortingInputs);
        var params = {
            firstRecordNumber: paginationValues.offset,
            lastRecordNumber: paginationValues.limit,
            sortBy: paginationValues.sortBy,
            order: paginationValues.order
        }
        this.showProgressBar();
        applicationManager.getTransactionManager()
            .fetchWireTransactions(params,
                this.onWireTransactionFetchSuccess.bind(this),
                this.serverError.bind(this));
    }
    /**
     * Called when transaction list fetch is successful for make transfer
     * @param {object[]} transactions List of transactions
     */
    WireTransferPresentationController.prototype.onWireTransactionFetchSuccess = function(transactions) {
        var paginationValues = applicationManager.getPaginationManager().getValues(recipientSortConfig);
        paginationValues.limit = transactions.length;
        if (transactions.length > 0 || paginationValues.offset === 0) {
            applicationManager.getPaginationManager().updatePaginationValues();
            applicationManager.getNavigationManager().updateForm({
                wireTransactions: {
                    transactions: transactions,
                    config: paginationValues
                }
            })
        } else if (paginationValues.offset !== 0) {
            applicationManager.getNavigationManager().updateForm({
                noMoreRecords: true
            });
        }
        this.hideProgressBar();
    }
    /**
     * Increments the page and fetch the new page for make transfer recipients
     */
    WireTransferPresentationController.prototype.getNextWireTransactions = function() {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getNextPage();
        this.fetchWireTransactions();
    }
    /**
     * Decrements the page and fetch the previous page for make transfer recipients
     */
    WireTransferPresentationController.prototype.getPreviousWireTransactions = function() {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getPreviousPage();
        this.fetchWireTransactions();
    }
    /**
     * Noitifies the UI for end of a loading state.
     */
    WireTransferPresentationController.prototype.hideProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "isLoading": false
        });
    };
    /**
     * Entry Point Method of Add Payee
     * @param {string} type type of the recipient "International/Domestic"
     */
    WireTransferPresentationController.prototype.showAddPayee = function(type) {
        this.showProgressBar();
        var asyncManager = applicationManager.getAsyncManager();
        var recipientManager = applicationManager.getRecipientsManager();
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        var asyncItems = [
            asyncManager.asyncItem(recipientManager, 'fetchRegionsByCountryCode', [OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY_NAME])
        ]
        if (type === applicationManager.getConfigurationManager().OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL) {
            asyncItems.push(asyncManager.asyncItem(recipientManager, 'fetchCountriesList'))
        }
        asyncManager.callAsync(asyncItems, this.onAddPayeeDataFetchSuccess.bind(this, type))
    }
    /**
     * Shows the add payee on UI - Called when initial data is loaded
     * @param {string} type Type of Recipient - Domestic/International
     * @param {AsyncResponse} asyncResponse Response of Async Calls
     */
    WireTransferPresentationController.prototype.onAddPayeeDataFetchSuccess = function(type, asyncResponse) {
        this.hideProgressBar();
        if (asyncResponse.isAllSuccess()) {
            var responses = asyncResponse.responses;
            var countries = responses[1] ? responses[1].data.records : [];
            this.showAddRecipientOnUI(responses[0].data.region_details_view, countries, type);
        } else {
            this.serverError();
        }
    }
    /**
     * Save Wire Transfer Payee
     * @param {Object} recipientData Data of the recipient
     * @param {string} type type of the recipient International/Domestic
     */
    WireTransferPresentationController.prototype.saveWireTransferPayee = function(recipientData, type) {
        this.showProgressBar();
        var recipientManager = applicationManager.getRecipientsManager();
        var data = {
            "payeeNickName": recipientData.recipientAccountDetails.payeeNickName,
            "payeeAccountNumber": recipientData.recipientAccountDetails.payeeAccountNumber,
            "payeeName": recipientData.recipientDetails.payeeName,
            "zipCode": recipientData.recipientDetails.zipCode,
            "cityName": recipientData.recipientDetails.cityName,
            "state": recipientData.recipientDetails.state,
            "addressLine1": recipientData.recipientDetails.addressLine1,
            "addressLine2": recipientData.recipientDetails.addressLine2,
            "type": recipientData.recipientDetails.type,
            "country": recipientData.recipientDetails.country,
            "bankName": recipientData.recipientAccountDetails.bankName,
            "bankAddressLine1": recipientData.recipientAccountDetails.bankAddressLine1,
            "bankAddressLine2": recipientData.recipientAccountDetails.bankAddressLine2,
            "bankCity": recipientData.recipientAccountDetails.bankCity,
            "bankState": recipientData.recipientAccountDetails.bankState,
            "bankZip": recipientData.recipientAccountDetails.bankZip,
            "wireAccountType": type,
        }
        if (type === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
            data.routingCode = recipientData.recipientAccountDetails.routingCode;
            data.country = recipientData.recipientDetails.country;
        } else {
            data.swiftCode = recipientData.recipientAccountDetails.swiftCode;
            data.country = recipientData.recipientDetails.country;
            if (IBANUtils.isCountrySupportsIBAN(recipientData.recipientDetails.country)) {
                data.IBAN = recipientData.recipientAccountDetails.IBAN;
            } else {
                data.internationalRoutingCode = recipientData.recipientAccountDetails.internationalRoutingCode;
            }
        }
        recipientManager.saveWireTransferRecipient(data, this.showAddRecipientAcknowledgement.bind(this, data), this.onAddPayeeError.bind(this, type));
    }
    /**
     * Starts MFA for One Time Wire Transfer
     * @param {Object} transferData Object containg data for wire transfer
     */
    WireTransferPresentationController.prototype.authorizeOneTimeTransfer = function(transferData) {
        //Removed old MFA
        this.createOneTimeWireTransfer(transferData);
    }
    WireTransferPresentationController.prototype.createOneTimeWireTransfer = function(transferData) {
        this.transferData = transferData;
        this.showProgressBar();
        var mfaManager = applicationManager.getMFAManager();
        mfaManager.setMFAOperationType("CREATE");
        var displayName = applicationManager.getPresentationUtility().MFA.getDisplayNameForTransfer(transferData.recipientDetails.wireAccountType);
        applicationManager.getPresentationUtility().MFA.getServiceIdBasedOnDisplayName(displayName);
        var mfaParams = {
            serviceName: mfaManager.getServiceId(),
        };
        var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var newTransaction = new transactionsModel({
            'wireAccountType': transferData.recipientDetails.wireAccountType,
            'payeeName': transferData.recipientDetails.payeeName,
            'payeeAddressLine1': transferData.recipientDetails.addressLine1,
            'payeeAddressLine2': transferData.recipientDetails.addressLine2,
            'cityName': transferData.recipientDetails.cityName,
            'state': transferData.recipientDetails.state,
            'zipCode': transferData.recipientDetails.zipCode,
            'payeeType': transferData.recipientDetails.type,
            'payeeAccountNumber': transferData.recipientAccountDetails.payeeAccountNumber,
            // 'reasonForTransfer': context.reasonForTransfer,
            'payeeNickName': transferData.recipientAccountDetails.payeeNickName,
            'bankName': transferData.recipientAccountDetails.bankName,
            'bankAddressLine1': transferData.recipientAccountDetails.bankAddressLine1,
            'bankAddressLine2': transferData.recipientAccountDetails.bankAddressLine2,
            'bankCity': transferData.recipientAccountDetails.bankCity,
            'bankState': transferData.recipientAccountDetails.bankState,
            'bankZip': transferData.recipientAccountDetails.bankZip,
            'fromAccountNumber': transferData.transactionDetails.fromAccountNumber,
            'payeeCurrency': transferData.transactionDetails.currency,
            'transactionCurrency': transferData.transactionDetails.currency,
            // 'country': transferData.recipientDetails.country,
            'amount': transferData.transactionDetails.amount,
            'transactionType': applicationManager.getConfigurationManager().OLBConstants.TRANSACTION_TYPE.WIRE,
            'transactionsNotes': transferData.transactionDetails.reasonForTransfer,
            //'fee':transferData.transactionDetails.transactionFee,
            'MFAAttributes': mfaParams
        });
        if (transferData.recipientDetails.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
            newTransaction.routingNumber = transferData.recipientAccountDetails.routingCode;
            newTransaction.country = OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY_NAME;
        } else {
            newTransaction.swiftCode = transferData.recipientAccountDetails.swiftCode;
            newTransaction.country = transferData.recipientDetails.country;
            if (IBANUtils.isCountrySupportsIBAN(transferData.recipientDetails.country)) {
                newTransaction.IBAN = transferData.recipientAccountDetails.IBAN;
            } else {
                newTransaction.internationalRoutingCode = transferData.recipientAccountDetails.internationalRoutingCode;
            }
        }
        applicationManager
            .getTransactionManager()
            .createTransaction(newTransaction, this.showOneTimeTransferAcknowledgement.bind(this), this.showOneTimeTransferError.bind(this));
    }
    /**
     * Calls when wire transfer is successfull
     * @param {Object} transferData Data of the transfer
     * @param {Object} transactionResponse Object containing transaction response
     */
    WireTransferPresentationController.prototype.showOneTimeTransferAcknowledgement = function(transactionResponse) {
        var mfaManager = applicationManager.getMFAManager();
        if (transactionResponse.referenceId) {
            this.hideProgressBar();
            applicationManager.getNavigationManager().navigateTo(wireTransferForm);
            applicationManager.getNavigationManager().updateForm({
                oneTimeTransferAcknowledgement: {
                    referenceId: transactionResponse.referenceId,
                    transferData: this.transferData
                }
            })
        } else {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "ONE_TIME_WIRE_TRANSFERS",
                "response": transactionResponse
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }
    }
    /**
     * Calls when wire transfer is not successfull
     * @param {Object} response Data of the transfer
     */
    WireTransferPresentationController.prototype.showOneTimeTransferError = function(response) {
        this.hideProgressBar();
        applicationManager.getNavigationManager().navigateTo(wireTransferForm);
        applicationManager.getNavigationManager().updateForm({
            oneTimeTransferError: {
                errorMessage: response.errorMessage
            }
        })
    }
    /**
     * Handles the error for add payee
     */
    WireTransferPresentationController.prototype.onAddPayeeError = function(type, response) {
        this.hideProgressBar();
        applicationManager.getNavigationManager().updateForm({
            saveRecipientServerError: {
                errorMessage: response.errorMessage,
                type: type
            }
        })
    }
    /**
     * Show Add Recipient on UI
     * @param {Object[]} states list of states
     * @param {Object[]} countries list of countries
     * @param {string} type type of recipient - International/Domestic
     */
    WireTransferPresentationController.prototype.showAddRecipientOnUI = function(states, countries, type) {
        applicationManager.getNavigationManager().updateForm({
            addPayee: {
                states: states,
                countries: countries,
                type: type
            }
        })
    }
    /**
     * Calls when add payee is successfull
     * @param {Object} data Object containing data of payee
     * @param {Object} addPayeeResponse Object containing add payee response
     */
    WireTransferPresentationController.prototype.showAddRecipientAcknowledgement = function(data, addPayeeResponse) {
        this.hideProgressBar();
        data.payeeId = addPayeeResponse.payeeId;
        applicationManager.getNavigationManager().updateForm({
            addRecipientAcknowledgement: data
        })
    }
    /**
     * Fetch the states by given country Id
     * @param {string} countryId Id of the country
     */
    WireTransferPresentationController.prototype.fetchStates = function(countryId) {
        this.showProgressBar();
        var recipientManager = applicationManager.getRecipientsManager();
        recipientManager.fetchRegionsByCountryCode(countryId, this.onFetchStateSuccess.bind(this), this.serverError.bind(this));
    }
    /**
     * Calls when fetch state is successful and map states on UI
     * @param {Object[]} states Id of the country
     */
    WireTransferPresentationController.prototype.onFetchStateSuccess = function(states) {
        this.hideProgressBar();
        applicationManager.getNavigationManager().updateForm({
            states: states.region_details_view
        })
    }
    /**
     * Entry Point Method of One Time Transfer
     * @param {string} type type of the recipient "International/Domestic"
     */
    WireTransferPresentationController.prototype.showOneTimeTransfer = function() {
        this.showProgressBar();
        var asyncManager = applicationManager.getAsyncManager();
        var recipientManager = applicationManager.getRecipientsManager();
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        var asyncItems = [
            asyncManager.asyncItem(recipientManager, 'fetchRegionsByCountryCode', [OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY_NAME]),
            asyncManager.asyncItem(recipientManager, 'fetchCountriesList'),
            asyncManager.asyncItem(applicationManager.getAccountManager(), 'fetchCheckingAccounts')
        ]
        asyncManager.callAsync(asyncItems, this.onOneTimeTransferDataSucccess.bind(this))
    }
    /**
     * One Time TRansfer Success
     * @param {AsyncResponse} asyncResponse Response of Async Calls
     */
    WireTransferPresentationController.prototype.onOneTimeTransferDataSucccess = function(asyncResponse) {
        this.hideProgressBar();
        var responses = asyncResponse.responses;
        if (asyncResponse.isAllSuccess()) {
            this.showOneTimeTransferOnUI(responses[0].data.region_details_view, responses[1].data.records, responses[2].data);
        } else {
            this.serverError.bind(this);
        }
    }
    /**
     * Show Add Recipient on UI
     * @param {Object[]} states list of states
     * @param {Object[]} countries list of countries
     * @param {Object[]} checkingAccounts list of countries
     */
    WireTransferPresentationController.prototype.showOneTimeTransferOnUI = function(states, countries, checkingAccounts) {
        applicationManager.getNavigationManager().updateForm({
            oneTimeTransfer: {
                states: states,
                countries: countries,
                checkingAccounts: checkingAccounts
            }
        })
    }
    /**
     * Entry Point Method of One Time Transfer
     * @param {string} payee Payee Object
     */
    WireTransferPresentationController.prototype.showEditPayee = function(payee) {
        this.showProgressBar();
        var asyncManager = applicationManager.getAsyncManager();
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        var recipientManager = applicationManager.getRecipientsManager();
        var country = payee.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL ?
            payee.country :
            OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY_NAME;
        var asyncItems = [
            asyncManager.asyncItem(recipientManager, 'fetchRegionsByCountryCode', [country]),
            asyncManager.asyncItem(recipientManager, 'fetchCountriesList')
        ]
        asyncManager.callAsync(asyncItems, this.onEditPayeeDataSucccess.bind(this, payee))
    }
    /**
     * Shows the add payee on UI - Called when initial data is loaded
     * @param {AsyncResponse} asyncResponse Response of Async Calls
     */
    WireTransferPresentationController.prototype.onEditPayeeDataSucccess = function(payee, asyncResponse) {
        this.hideProgressBar();
        var responses = asyncResponse.responses;
        if (asyncResponse.isAllSuccess()) {
            this.showEditPayeeOnUI(payee, responses[0].data.region_details_view, responses[1].data.records);
        } else {
            this.serverError.bind(this);
        }
    }
    /**
     * Show Add Recipient on UI
     * @param {Object} payee Payee OBject
     * @param {Object[]} states list of states
     * @param {Object[]} countries list of countries
     */
    WireTransferPresentationController.prototype.showEditPayeeOnUI = function(payee, states, countries) {
        applicationManager.getNavigationManager().updateForm({
            editPayee: {
                states: states,
                countries: countries,
                payee: payee
            }
        })
    }
    WireTransferPresentationController.prototype.showAccountDetailsForInboundTransfers = function() {
        this.showProgressBar();
        applicationManager.getAccountManager().fetchCheckingAccounts(this.checkingAccountsForInboundSuccess.bind(this), this.serverError.bind(this));
    }
    WireTransferPresentationController.prototype.checkingAccountsForInboundSuccess = function(checkingAccounts) {
        this.hideProgressBar();
        applicationManager.getNavigationManager()
            .updateForm({
                accountDetailsForInbound: {
                    checkingAccounts: checkingAccounts
                }
            })
    }
    WireTransferPresentationController.prototype.fetchRecipientTransactions = function(recipient) {
        this.showProgressBar();
        var params = {
            "payeeId": recipient.payeeId,
            "limit": applicationManager.getConfigurationManager().OLBConstants.WIRE_ACTIVITY_LIMIT
        };
        applicationManager.getTransactionManager().fetchRecipientWireTransactions(params, this.onViewActivitySuccess.bind(this, recipient), this.serverError.bind(this))
    }
    WireTransferPresentationController.prototype.onViewActivitySuccess = function(recipient, transactions) {
        this.hideProgressBar();
        applicationManager.getNavigationManager()
            .updateForm({
                recipientActivity: {
                    transactions: transactions,
                    recipient: recipient
                }
            })
    }
    WireTransferPresentationController.prototype.savePayeeAfterTransfer = function(transactionId, data) {
        this.showProgressBar();
        applicationManager.getRecipientsManager()
            .savePayeeAfterWireTransfer(transactionId, this.savePayeeAfterTransferSuccess.bind(this, data), this.showServerErrorFlex.bind(this));
    }
    WireTransferPresentationController.prototype.savePayeeAfterTransferSuccess = function(recipientData, response) {
        var data = {
            "payeeNickName": recipientData.recipientAccountDetails.payeeNickName,
            "payeeAccountNumber": recipientData.recipientAccountDetails.payeeAccountNumber,
            "payeeName": recipientData.recipientDetails.payeeName,
            "zipCode": recipientData.recipientDetails.zipCode,
            "cityName": recipientData.recipientDetails.cityName,
            "state": recipientData.recipientDetails.state,
            "addressLine1": recipientData.recipientDetails.addressLine1,
            "addressLine2": recipientData.recipientDetails.addressLine2,
            "type": recipientData.recipientDetails.type,
            "country": recipientData.recipientDetails.country,
            "swiftCode": recipientData.recipientAccountDetails.swiftCode,
            "routingCode": recipientData.recipientAccountDetails.routingCode,
            "bankName": recipientData.recipientAccountDetails.bankName,
            "bankAddressLine1": recipientData.recipientAccountDetails.bankAddressLine1,
            "bankAddressLine2": recipientData.recipientAccountDetails.bankAddressLine2,
            "bankCity": recipientData.recipientAccountDetails.bankCity,
            "bankState": recipientData.recipientAccountDetails.bankState,
            "bankZip": recipientData.recipientAccountDetails.bankZip,
            "IBAN": recipientData.recipientAccountDetails.IBAN,
            "wireAccountType": type,
            "internationalRoutingCode": recipientData.internationalRoutingCode
        }
        applicationManager.getNavigationManager()
            .updateForm({
                addRecipientAcknowledgement: data
            })
    }
    WireTransferPresentationController.prototype.deleteRecipient = function(recipient) {
        applicationManager.getRecipientsManager()
            .deletePayeeById(recipient.payeeId, this.deletePayeeSuccess.bind(this), this.deletePayeeError.bind(this))
    }
    WireTransferPresentationController.prototype.deletePayeeSuccess = function() {
        this.showManageRecipientList();
    }
    WireTransferPresentationController.prototype.deletePayeeError = function() {}
    /**
     * Show Server Flex on UI
     * @param {string} errorMessage Non breaking error message
     */
    WireTransferPresentationController.prototype.showServerErrorFlex = function(errorMessage) {
        applicationManager.getNavigationManager()
            .updateForm({
                serverError: errorMessage
            });
    }
    WireTransferPresentationController.prototype.getTnC = function(type) {
        var flowType;
        if (type === OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL)
            flowType = OLBConstants.TNC_FLOW_TYPES.International_WireTransfer_TnC;
        else if (type === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC)
            flowType = OLBConstants.TNC_FLOW_TYPES.WireTransfer_TnC;
        else
            flowType = OLBConstants.TNC_FLOW_TYPES.OneTime_WireTransfer_TnC;
        var self = this;
        var termsAndConditionModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TermsAndConditionsModule");
        termsAndConditionModule.presentationController.showTermsAndConditions(flowType, self.getTnCSuccess.bind(self), self.getTnCOnFailure.bind(self));
    };
    WireTransferPresentationController.prototype.getTnCSuccess = function(response) {
        var scopeObj = this;
        scopeObj.showTnCView(response);
    };
    WireTransferPresentationController.prototype.getTnCOnFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            "TnCFailure": true
        }, "frmWireTransfer");
        this.showServerErrorFlex(response);
    };
    WireTransferPresentationController.prototype.showTnCView = function(TnCcontent) {
        var self = this;
        applicationManager.getNavigationManager().updateForm({
            "TnCcontent": TnCcontent
        }, "frmWireTransfer")
    };
    return WireTransferPresentationController;
});