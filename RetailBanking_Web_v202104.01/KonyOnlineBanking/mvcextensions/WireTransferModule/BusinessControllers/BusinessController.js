define([], function() {
    function WireTransfer_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(WireTransfer_BusinessController, kony.mvc.Business.Controller);
    WireTransfer_BusinessController.prototype.initializeBusinessController = function() {};
    WireTransfer_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return WireTransfer_BusinessController;
});