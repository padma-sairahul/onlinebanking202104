define([], function() {
    var BusinessController = kony.mvc.Business.Controller;
    var CommandExecutionEngine = kony.mvc.Business.CommandExecutionEngine;
    var CommandHandler = kony.mvc.Business.CommandHandler;
    var Command = kony.mvc.Business.Command;

    function CardManagement_BusinessController() {
        BusinessController.call(this);
    }
    inheritsFrom(CardManagement_BusinessController, BusinessController);
    CardManagement_BusinessController.prototype.initializeBusinessController = function() {};
    CardManagement_BusinessController.prototype.execute = function(command) {
        BusinessController.prototype.execute.call(this, command);
    };
    return CardManagement_BusinessController;
});