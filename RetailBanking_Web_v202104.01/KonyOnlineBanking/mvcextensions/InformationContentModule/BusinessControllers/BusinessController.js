define([], function() {
    function InformationContent_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(InformationContent_BusinessController, kony.mvc.Business.Controller);
    InformationContent_BusinessController.prototype.initializeBusinessController = function() {};
    InformationContent_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return InformationContent_BusinessController;
});