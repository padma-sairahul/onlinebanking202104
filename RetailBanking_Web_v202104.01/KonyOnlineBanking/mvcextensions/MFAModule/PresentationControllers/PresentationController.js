/**
 * @module MultiFactorAuthenticationPresentationController
 */
define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {

    /**
     *  Multi Factor Authentication PresentationController.
     * @class
     * @alias module:MultiFactorAuthenticationPresentationController
     */

    this.communicationType = null;

    function MultiFactorAuthenticationPresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }

    inheritsFrom(MultiFactorAuthenticationPresentationController, kony.mvc.Presentation.BasePresenter);

    MultiFactorAuthenticationPresentationController.prototype.initializePresentationController = function() {};
    /**
     * used to show the billPay Page and executes the particular Page.
     * @param {string} frm  used to load the form 
     * @param {object}  data  used to load the particular form and having key value pair.
     */
    MultiFactorAuthenticationPresentationController.prototype.showView = function(frm, data) {
        applicationManager.getNavigationManager().navigateTo(frm);
        applicationManager.getNavigationManager().updateForm(data, frm);
    };

    /**
     * cancelCallback - Dummy function. Whenever an MFA flow is started, cancelCallback will contain the actual cancel on click for that operation.
     */
    MultiFactorAuthenticationPresentationController.prototype.cancelCallback = function() {};

    /**
     * successCallback - Dummy function. Whenever an MFA flow is started, successCallback will contain the actual success callback for that operation.
     */
    MultiFactorAuthenticationPresentationController.prototype.successCallback = function() {};




    MultiFactorAuthenticationPresentationController.prototype.showProgressBar = function(form, isVisible) {
        var progressContext = {
            "key": BBConstants.COMMON_LOADING_INDICATIOR,
            "responseData": isVisible
        };
        applicationManager.getNavigationManager().updateForm(progressContext, form);
    };

    /**
     * Wrapper method to be passed as successcallback,updates desired form with help navigationObject,dataProcessor,and response
     *@param {object} navigaton object
     *@param {function} dataProcessor - this function is used to process and return formatted response.
     *@param response from service call
     */
    MultiFactorAuthenticationPresentationController.prototype.completeSuccessCall = function(navigationObject, dataProcessor, response) {
        if (!kony.sdk.isNullOrUndefined(navigationObject)) {
            if (!kony.sdk.isNullOrUndefined(navigationObject.onSuccess)) {
                var processedData;
                if (!kony.sdk.isNullOrUndefined(dataProcessor) && dataProcessor !== "" && dataProcessor instanceof Function) {
                    processedData = dataProcessor(navigationObject, response);
                } else {
                    processedData = response;
                }

                if (!kony.sdk.isNullOrUndefined(processedData)) {
                    var navigationContext = {};
                    navigationContext.context = {
                        key: navigationObject.onSuccess.context.key,
                        responseData: processedData
                    };
                    navigationContext.form = navigationObject.onSuccess.form;
                    applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);
                } else {
                    kony.print("Response is null or undefined");
                }
            }
        }
    };

    /**
     *This method calls service to resnd the otp
     *@params navObject the success and failure form with context methods
     *to be executed on load of the form
     */

    MultiFactorAuthenticationPresentationController.prototype.resendOTP = function(navObject) {
        this.showProgressBar(kony.application.getCurrentForm().id, true);

        if (navObject.requestData.transferData.MFAAttributes.serviceName === "SERVICE_ID_14" && navObject.requestData.transferData.bulkPayString) {
            /*Bulk Bill Pay scenario*/
            applicationManager.getMultiFactorAuthenticationManager().billPayVerifyOTP(navObject.requestData.resendOTPData,
                this.completeSuccessCall.bind(this, navObject, "resendOTPSuccess"),
                this.completeSuccessCall.bind(this, navObject, "resendOTPFailure"));
        } else if (navObject.requestData.transferData.transactionId) {
            /* Edit Transaction Scenario */
            applicationManager.getTransactionManager().updateTransaction(navObject.requestData.resendOTPData,
                this.completeSuccessCall.bind(this, navObject, "resendOTPSuccess"),
                this.completeSuccessCall.bind(this, navObject, "resendOTPFailure"));
        } else if (navObject.requestData.transferData.isScheduled === "true" || navObject.requestData.transferData.transactionType === "BillPay") {

            /* Edit Transaction Scenario */
            applicationManager.getTransactionManager().updateTransaction(navObject.requestData.resendOTPData,
                this.completeSuccessCall.bind(this, navObject, "resendOTPSuccess"),
                this.completeSuccessCall.bind(this, navObject, "resendOTPFailure"));
        } else {
            /*Other Transaction scenario*/
            applicationManager.getMultiFactorAuthenticationManager().verifyOTP(navObject.requestData.resendOTPData,
                this.completeSuccessCall.bind(this, navObject, "resendOTPSuccess"),
                this.completeSuccessCall.bind(this, navObject, "resendOTPFailure"));

        }
    };

    /**
     * Success callback for resend OTP Scenario
     *@param navObject the original object with request data and success/failure callbacks
     *@param response the response received from service call
     */

    MultiFactorAuthenticationPresentationController.prototype.resendOTPSuccess = function(navObject, response) {
        /* Hide the Loading indicator after receiveing a new OTP */
        this.showProgressBar(kony.application.getCurrentForm().id, false);
        return response;
    };

    MultiFactorAuthenticationPresentationController.prototype.resendOTPFailure = function(navObject, response) {
        /* Hide the Loading indicator after failing to receive a new OTP */
        this.showProgressBar(kony.application.getCurrentForm().id, false);
        return response;
    };

    /*
     *This method calls service to validate the OTP entered by the user
     *@param navObject which contains the otp payload and success and
     *failure callbacks
     */
    MultiFactorAuthenticationPresentationController.prototype.verifyOTP = function(navObject) {
        /* Show Loading Screen while service is being called */
        this.showProgressBar(kony.application.getCurrentForm().id, true);
        if (navObject.requestData.verifyOTPData.MFAAttributes.serviceName === "SERVICE_ID_14" && navObject.requestData.transferData.bulkPayString) {
            /*Bulk Bill Pay scenario*/

            applicationManager.getMultiFactorAuthenticationManager().billPayVerifyOTP(navObject.requestData.verifyOTPData, this.verifyOTPSuccess.bind(this, navObject), this.verifyOTPFailure.bind(this, navObject));
        } else if (navObject.requestData.transferData.transactionId) {

            /* Edit Transfer Flow*/
            applicationManager.getTransactionManager().updateTransaction(navObject.requestData.verifyOTPData, this.verifyOTPSuccess.bind(this, navObject), this.verifyOTPFailure.bind(this, navObject));

        } else if (navObject.requestData.transferData.isScheduled === "true") {

            /* Edit Bill Pay Transfer Flow*/
            applicationManager.getTransactionManager().updateTransaction(navObject.requestData.verifyOTPData, this.verifyOTPSuccess.bind(this, navObject), this.verifyOTPFailure.bind(this, navObject));
        } else {
            /*Other Transaction scenario*/

            applicationManager.getMultiFactorAuthenticationManager().verifyOTP(navObject.requestData.verifyOTPData, this.verifyOTPSuccess.bind(this, navObject), this.verifyOTPFailure.bind(this, navObject));
        }
    };

    /**
     *This method deals with the success response of OTPvalidation for various transactions
     *@param navObject - 
     *@param response - actual response received from the service call
     *
     */

    MultiFactorAuthenticationPresentationController.prototype.verifyOTPSuccess = function(navObject, response) {
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        if (response.referenceId) {
            /* Normal Transaction, Single Bill Pay and Edit Transactions Response and Wire Transfer*/

            /* generic data setup*/
            response.transferData = navObject.requestData.transferData;
            response.transferData.referenceId = response.referenceId;
            navObject.onSuccess.context.responseData = response;

            /* for billpay single */
            if (navObject.requestData.transferData.transactionType === "BillPay") {

                /* normal singe bill pay */

                billPayModule.presentationController.getAccountDataByAccountId(navObject);
            } else {
                /*Normal Transfer Scenario*/
                transferModule.presentationController.fetchUserAccountAndNavigate(navObject);
            }

        } else if (navObject.requestData.requstData && navObject.requestData.requstData.payeeObject && navObject.requestData.requstData.payeeObject.wireAccountType) {
            /* Wire Account Type*/
            this.noServiceNavigate(navObject);
        } else if (response.transactionId && navObject.requestData.transferData.transactionId) {

            /* this is for  edit flow */
            if (navObject.requestData.transferData.transactionType === "BillPay") {

                var billPayEditrObj = {
                    requestData: navObject.requestData,
                    onSuccess: {
                        "form": "BillPayModule/frmAcknowledgement",
                        "module": "BillPayModule",
                        "context": {
                            "key": BBConstants.BILLPAY_BILL_ACK,
                            "responseData": null
                        }
                    }
                };
                billPayModule.presentationController.getAccountDataByAccountId(billPayEditrObj);
            } else {
                var navigationObject = {
                    requestData: null,
                    paginationManager: null,
                    onFailure: {

                        form: "TransferModule/frmTransfers",
                        module: "TransferModule",
                        context: {
                            key: "NO_TRANSACTIONS",
                            responseData: null
                        }
                    },
                    onSuccess: {
                        form: "TransferModule/frmTransfers",
                        module: "TransferModule",
                        context: {
                            key: "SHOW_TRANSFERS",
                            responseData: null
                        }
                    }

                };

                transferModule.presentationController.getScheduledUserTransactions(navigationObject);
            }

        } else if (JSON.parse(navObject.requestData.transferData.bulkPayString).length === response.length) {
            /*Bulk Bill Pay Scenario*/
            response.isBBTransactionCreated = false;
            response = {
                "bulkPay": response
            };
            var navigationContext = {};
            navigationContext.context = {
                key: navObject.onSuccess.context.key,
                responseData: response
            };
            navigationContext.form = navObject.onSuccess.form;
            /*Hide Loading Screen before navigating to next form*/
            applicationManager.getNavigationManager().navigateTo(navigationContext.form);
            applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);
        }
    };
    /**
  *This method handles the OTP verification failure scenarios 
  and re directs to proper form
  @param navObject - 
  @response - The actual response received by the services
  *
  *  
  */

    MultiFactorAuthenticationPresentationController.prototype.verifyOTPFailure = function(navObject, response) {
        /*hide progress bar in case of failure scenario and show appropritae message*/
        this.showProgressBar(kony.application.getCurrentForm().id, false);
        var navigationContext = {};
        navigationContext.context = {
            key: navObject.onFailure.context.key,
            responseData: response
        };
        navigationContext.form = navObject.onFailure.form;
        applicationManager.getNavigationManager().navigateTo(navigationContext.form);
        applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);

    };

    /*Lock and Logout the user after all the un successful attempts are exhaustd*/
    MultiFactorAuthenticationPresentationController.prototype.lockAndLogout = function(navObject) {
        var navigationContext = {};
        /*Log out user*/
        this.showProgressBar(navObject.onSuccess.form, true);
        navigationContext.context = {
            key: navObject.onSuccess.context.key,
            responseData: navObject.requestData.message
        };
        navigationContext.form = navObject.onSuccess.form;
        applicationManager.getNavigationManager().navigateTo(navigationContext.form);
        applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);

    };

    /*Cancel the MFA Transaction and take back to the starting page */

    MultiFactorAuthenticationPresentationController.prototype.cancelMFA = function(navObject) {
        var navigationContext = {};
        this.showProgressBar(navObject.onSuccess.form, true);
        navigationContext.context = {
            key: navObject.onSuccess.context.key,
            responseData: null
        };
        navigationContext.form = navObject.onSuccess.form;
        applicationManager.getNavigationManager().navigateTo(navigationContext.form);
        applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);

    };

    /**
     *This method submits the security questions answers to validate a transaction
     *
     */

    MultiFactorAuthenticationPresentationController.prototype.submitAnswers = function(navObject) {
        this.showProgressBar(kony.application.getCurrentForm().id, true);

        if (navObject.requestData.secureityQuestionData.MFAAttributes.serviceName === "SERVICE_ID_14" && navObject.requestData.transferData.bulkPayString) {
            /*Bulk Bill Pay scenario*/

            applicationManager.getMultiFactorAuthenticationManager().billPaySubmitAnswers(navObject.requestData.secureityQuestionData,
                this.submitAnswerSuccess.bind(this, navObject),
                this.submitAnswerFailure.bind(this, navObject));
        } else if (navObject.requestData.transferData.transactionId) {

            /* Edit Transfer Flow*/
            applicationManager.getTransactionManager().updateTransaction(navObject.requestData.secureityQuestionData,
                this.submitAnswerSuccess.bind(this, navObject),
                this.submitAnswerFailure.bind(this, navObject));

        } else if (navObject.requestData.transferData.isScheduled === "true") {

            /* Edit Bill Pay Transfer Flow*/
            applicationManager.getTransactionManager().updateTransaction(navObject.requestData.secureityQuestionData,
                this.submitAnswerSuccess.bind(this, navObject),
                this.submitAnswerFailure.bind(this, navObject));
        } else {
            /*Other Transaction scenario*/

            applicationManager.getMultiFactorAuthenticationManager().submitAnswers(navObject.requestData.secureityQuestionData,
                this.submitAnswerSuccess.bind(this, navObject),
                this.submitAnswerFailure.bind(this, navObject));
        }

    };

    /**
     *Success callback for Security Questions
     *@param navObject
     *@response the respons received from the service call
     */
    MultiFactorAuthenticationPresentationController.prototype.submitAnswerSuccess = function(navObject, response) {
        /* MFA security Question validation failed.Extra Set of questions has come */
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        if (response.MFAAttributes) {
            navObject.onFailure.context.responseData = response;

            /*Hide Loading Screen before navigating to next form*/
            applicationManager.getNavigationManager().updateForm(navObject.onFailure.context, navObject.onFailure.form);

        } else if (response.referenceId) {

            response.transferData = navObject.requestData.transferData;
            response.transferData.referenceId = response.referenceId;
            navObject.onSuccess.context.responseData = response;

            if (navObject.requestData.transferData.transactionType === "BillPay") {
                /* Sinlge Bill Pay Scenario */

                billPayModule.presentationController.getAccountDataByAccountId(navObject);
            } else if (navObject.requestData.requstData && navObject.requestData.requstData.payeeObject && navObject.requestData.requstData.payeeObject.wireAccountType) {
                /* wire Transfer*/
                this.noServiceNavigate(navObject);
            } else {
                /*Normal Transfer Scenario*/
                transferModule.presentationController.fetchUserAccountAndNavigate(navObject);
            }
        } else if (response.transactionId && navObject.requestData.transferData.transactionId) {


            /* this is for  edit flow */
            if (navObject.requestData.transferData.transactionType === "BillPay") {

                var billPayEditrObj = {
                    requestData: navObject.requestData,
                    onSuccess: {
                        "form": "BillPayModule/frmAcknowledgement",
                        "module": "BillPayModule",
                        "context": {
                            "key": BBConstants.BILLPAY_BILL_ACK,
                            "responseData": null
                        }
                    }
                };
                billPayModule.presentationController.getAccountDataByAccountId(billPayEditrObj);
            } else {
                var navigationObject = {
                    requestData: null,
                    paginationManager: null,
                    onFailure: {

                        form: "TransferModule/frmTransfers",
                        module: "TransferModule",
                        context: {
                            key: "NO_TRANSACTIONS",
                            responseData: null
                        }
                    },
                    onSuccess: {
                        form: "TransferModule/frmTransfers",
                        module: "TransferModule",
                        context: {
                            key: "SHOW_TRANSFERS",
                            responseData: null
                        }
                    }

                };

                transferModule.presentationController.getScheduledUserTransactions(navigationObject);
            }

        } else if (JSON.parse(navObject.requestData.transferData.bulkPayString).length === response.length) {
            /*Bulk Bill Pay Scenario*/
            response.isBBTransactionCreated = false;
            response = {
                "bulkPay": response
            };
            var navigationContext = {};
            navigationContext.context = {
                key: navObject.onSuccess.context.key,
                responseData: response
            };
            navigationContext.form = navObject.onSuccess.form;
            /*Hide Loading Screen before navigating to next form*/
            applicationManager.getNavigationManager().navigateTo(navigationContext.form);
            applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);
        }
    };

    /**
     *Failure callback for Security Question service call
     *
     */

    MultiFactorAuthenticationPresentationController.prototype.submitAnswerFailure = function(navObject, response) {
        var navigationContext = {};
        navigationContext.context = {
            key: navObject.onFailure.context.key,
            responseData: response
        };
        navigationContext.form = navObject.onFailure.form;
        applicationManager.getNavigationManager().navigateTo(navigationContext.form);
        applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);

    };

    /**
     *This method navigates to any function given in navObject onSuccess
     *
     **/
    MultiFactorAuthenticationPresentationController.prototype.navigateToForm = function(navObject, response) {

        var navigationContext = {};
        navigationContext.context = {
            key: navObject.onSuccess.context.key,
            responseData: response
        };
        navigationContext.form = navObject.onSuccess.form;
        applicationManager.getNavigationManager().navigateTo(navigationContext.form);
        applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);

    };

    /**
     * used to get the user priamry contact number
     * @returns {string} contact number
     */
    MultiFactorAuthenticationPresentationController.prototype.getPrimaryContactNumber = function() {
        return applicationManager.getUserPreferencesManager().getUserObj().phone;
    };

    /**
     * used to get the email id
     * @returns {string} contact number
     */
    MultiFactorAuthenticationPresentationController.prototype.getPrimaryEmail = function() {
        return applicationManager.getUserPreferencesManager().getUserObj().email;
    };
    /**
     * used to show the secure access settings page
     */
    MultiFactorAuthenticationPresentationController.prototype.showSecureAccessSettings = function() {
        var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
        profileModule.presentationController.showSecureAccessSettings();
    };

    MultiFactorAuthenticationPresentationController.prototype.noServiceNavigate = function(navObj) {
        applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
        applicationManager.getNavigationManager().updateForm({
            "key": navObj.onSuccess.context.key,
            "responseData": navObj.onSuccess.context.responseData
        }, navObj.onSuccess.form);

    };

    return MultiFactorAuthenticationPresentationController;
});