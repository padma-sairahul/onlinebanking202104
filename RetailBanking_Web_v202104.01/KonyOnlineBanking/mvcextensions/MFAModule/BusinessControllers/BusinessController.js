define([], function() {

    function MultiFactorAuthentication_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }

    inheritsFrom(MultiFactorAuthentication_BusinessController, kony.mvc.Business.Controller);

    MultiFactorAuthentication_BusinessController.prototype.initializeBusinessController = function() {

    };

    MultiFactorAuthentication_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };

    /*Re-send OTP Servic call*/

    MultiFactorAuthentication_BusinessController.prototype.resendOTP = function(params, presentationSuccess, presentataionFailure) {
        var otpResendObj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        otpResendObj.save(params, saveCompletionCallback, "online");

        function saveCompletionCallback(status, data, error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status, data, error);
            if (obj.status === true) {
                presentationSuccess(obj.data);
            } else {
                presentataionFailure(obj.error);
            }
        }
    };

    /* Verify OTP Service call*/

    MultiFactorAuthentication_BusinessController.prototype.verifyOTP = function(params, presentationSuccess, presentataionFailure) {
        var otpResendObj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        otpResendObj.save(params, saveCompletionCallback, "online");

        function saveCompletionCallback(status, data, error) {
            if (data) {
                if (!kony.sdk.isNullOrUndefined(data.referenceId)) {
                    presentationSuccess(data);
                } else if (!kony.sdk.isNullOrUndefined(data.dbpErrMsg) || !kony.sdk.isNullOrUndefined(data.dbpErrCode) || !kony.sdk.isNullOrUndefined(data.MFAAttributes)) {

                    presentataionFailure(data);
                }
            } else {
                presentataionFailure(error);
            }
        }
    };

    /* Bulk Bill Pay verify OTP Service */

    MultiFactorAuthentication_BusinessController.prototype.billPayVerifyOTP = function(params, presentationSuccess, presentataionFailure) {
        var self = this;
        var transactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Transactions');
        transactionModel.customVerb('createBulkBillPay', params, saveCompletionCallback);

        function saveCompletionCallback(status, data, error) {
            if (data) {
                if (!kony.sdk.isNullOrUndefined(data.dbpErrMsg) || !kony.sdk.isNullOrUndefined(data.dbpErrCode) || !kony.sdk.isNullOrUndefined(data.MFAAttributes)) {
                    presentataionFailure(data);
                } else if (Array.isArray(data)) {
                    presentationSuccess(data);

                }
            } else {
                presentataionFailure(error);
            }
        }
    };

    /* Security Question Service call */

    MultiFactorAuthentication_BusinessController.prototype.submitAnswers = function(params, presentationSuccess, presentataionFailure) {
        var otpResendObj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        otpResendObj.save(params, saveCompletionCallback, "online");

        function saveCompletionCallback(status, data, error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status, data, error);
            if (obj.status === true) {
                presentationSuccess(obj.data);
            } else {
                presentataionFailure(obj.errmsg);
            }
        }
    };

    /* Bulk Bill Pay Security Question Service call */

    MultiFactorAuthentication_BusinessController.prototype.billPaySubmitAnswers = function(params, presentationSuccess, presentataionFailure) {
        var transactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Transactions');
        transactionModel.customVerb('createBulkBillPay', params, saveCompletionCallback);

        function saveCompletionCallback(status, data, error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status, data, error);
            if (obj.status === true) {
                presentationSuccess(obj.data);
            } else {
                presentataionFailure(obj.errmsg);
            }
        }
    };
    return MultiFactorAuthentication_BusinessController;
});