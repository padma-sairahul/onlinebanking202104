define([], function() {
    function BusinessBanking_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(BusinessBanking_BusinessController, kony.mvc.Business.Controller);
    BusinessBanking_BusinessController.prototype.initializeBusinessController = function() {};
    BusinessBanking_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return BusinessBanking_BusinessController;
});