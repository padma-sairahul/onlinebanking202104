define([], function() {
    function PersonalFinanceManagement_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(PersonalFinanceManagement_BusinessController, kony.mvc.Business.Controller);
    PersonalFinanceManagement_BusinessController.prototype.initializeBusinessController = function() {};
    PersonalFinanceManagement_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return PersonalFinanceManagement_BusinessController;
});