var arridglobal = {};
var botherror = {};
var billcount = 1;
define(['CommonUtilities'], function(CommonUtilities) {
    function LoanPay_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        var viewModel = [];
    }
    inheritsFrom(LoanPay_PresentationController, kony.mvc.Presentation.BasePresenter);
    LoanPay_PresentationController.prototype.initializePresentationController = function() {};
    LoanPay_PresentationController.prototype.presentLoanPay = function(data) {
        var navManager = applicationManager.getNavigationManager();
        if (kony.application.getCurrentForm().id !== "frmPayDueAmount") {
            navManager.navigateTo("frmPayDueAmount");
        }
        navManager.updateForm(data);
    };

    LoanPay_PresentationController.prototype.payDueDetails = function(data, action) {
        var self = this;
        self.showProgressBar();
        var loanManager = applicationManager.getAccountManager();
        loanManager.fetchAccountDetails(data, this.paySuccessDetails.bind(this, data, action), this.payErrorDetails.bind(this, data));
    };
    LoanPay_PresentationController.prototype.paySuccessDetails = function(data, action, response) {
        var self = this;
        self.showProgressBar();
        if (action === "PayDue") {
            this.presentLoanPay({
                "payDueDetails": response
            });
        } else if (action === "toAccountDetails") {
            this.presentLoanPay({
                "accountDetails": response
            });
        } else {
            this.presentLoanPay({
                "accountBalance": response
            });
        }
    };
    LoanPay_PresentationController.prototype.payErrorDetails = function(data, response) {
        this.presentLoanPay({
            "serverError": response.errorMessage
        });
        this.hideProgressBar();
    };

    //SimulationCallBack
    LoanPay_PresentationController.prototype.simulation = function(data) {
        billcount = 1;
        var self = this;
        self.showProgressBar();
        var loanManager = applicationManager.getLoansManager();
        loanManager.simulationCall(data, this.simulationSuccessCallback.bind(this, data), this.simulationErrorCallback.bind(this, data));
    };
    //SimulationSuccessCallBack
    LoanPay_PresentationController.prototype.simulationSuccessCallback = function(data, response) {
        var self = this;
        self.showProgressBar();
        arridglobal = {
            "arrangementId": data.arrangementId,
            "billType": "PAYOFF",
            "paymentDate": data.effectiveDate
        };
        kony.timer.schedule("fetchamounttimer", this.getBillAmount, 15, false);
    };
    //SimulationErrorCallBack
    LoanPay_PresentationController.prototype.getBillAmount = function() {
        var self = this;
        self.showProgressBar();
        var loanManager = applicationManager.getLoansManager();
        loanManager.fetchLoanPayoffAmount(arridglobal, this.BillSuccessCallback, this.BillErrorCallback);
    };
    LoanPay_PresentationController.prototype.simulationErrorCallback = function(data, params) {
        var self = this;
        self.showProgressBar();
        botherror = {};
        arridglobal = {
            "arrangementId": data.arrangementId,
            "billType": "PAYOFF",
            "paymentDate": data.effectiveDate
        };
        this.getBillAmount();
        botherror.errorMessage = params.errorMessage;
    };
    //BillAmountSuccessCallBack
    LoanPay_PresentationController.prototype.BillSuccessCallback = function(response) {
        var self = this;
        self.showProgressBar();
        this.presentLoanPay({
            "properties": response
        });
    };
    //BillAmountErrorCallBack
    LoanPay_PresentationController.prototype.BillErrorCallback = function(response) {
        var self = this;
        self.showProgressBar();
        if (billcount <= 2) {
            billcount++;
            this.getBillAmount();
        } else {
            var obj;
            if (response.serverErrorRes.backendError != null || response.serverErrorRes.backendError != undefined)
                obj = JSON.parse(response.serverErrorRes.backendError);
            else
                obj = response.serverErrorRes.dbpErrMsg;
            if (botherror.errorMessage) {
                this.presentLoanPay({
                    "fetchAmountError": botherror.errorMessage + " & " + obj.message
                });
            } else {
                this.presentLoanPay({
                    "fetchAmountError": obj.message
                });
            }
        }
    };
    //Transacte Date
    LoanPay_PresentationController.prototype.getTBankDate = function(callback) {
        applicationManager.getRecipientsManager().fetchBankDate({}, this.getBankDateSuccess.bind(this, callback), this.getBankDateFailure.bind(this, callback));
    };
    LoanPay_PresentationController.prototype.getBankDateSuccess = function(callback, response) {
        this.bankDate = response.date[0];
        this.presentLoanPay({
            'bankDate': response.date[0]
        });
    };
    LoanPay_PresentationController.prototype.getBankDateFailure = function(callback, response) {
        this.presentLoanPay({
            'bankDate': true
        });
    };
    //TermsandConditionsCall
    LoanPay_PresentationController.prototype.termsandconditions = function() {
        var self = this;
        self.showProgressBar();
        var termsAndConditionModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TermsAndConditionsModule");
        termsAndConditionModule.presentationController.showTermsAndConditions("BillPay_TnC", this.getTnCSuccess, this.getTnCOnFailure);
    };
    LoanPay_PresentationController.prototype.getTnCSuccess = function(termsAndConditionContent) {
        if (termsAndConditionContent) {
            viewModel = termsAndConditionContent;
        }
        this.presentLoanPay({
            transferConfirm: viewModel
        });
    };
    LoanPay_PresentationController.prototype.getTnCOnFailure = function(response) {

    };
    /**
     * To get frontend date string
     * @member LoanPay_PresentationController
     * @param {Date} dateString
     * @returns {Date} frontend date
     * @throws {void} - None
     */
    LoanPay_PresentationController.prototype.getFormattedDateString = function(dateString) {
        return CommonUtilities.getFrontendDateString(dateString);
    };
    /**
     * To get backend date string
     * @member LoanPay_PresentationController
     * @param {Date} dateString
     * @param {String} dateFormat
     * @returns {Date} backend date (yyyy-mm-dd) format
     * @throws {void} - None
     */
    LoanPay_PresentationController.prototype.getBackendDate = function(dateString) {
        return CommonUtilities.getBackendDateFormat(dateString);
    };
    /**
     * Entry Function for Loan Due
     * @member LoanPay_PresentationController
     * @param {Object} response contains account details
     * @returns {void} - None
     * @throws {void} - None
     */
    LoanPay_PresentationController.prototype.navigateToLoanDue = function(response) {
        var self = this;
        self.showProgressBar();
        if (response)
            viewModel = response;
        this.presentLoanPay({
            loanDue: viewModel
        });
    };
    /**
     * Entry Function for Loan Pay Off
     * @member LoanPay_PresentationController
     * @param {Object} response contains account details
     * @returns {void} - None
     * @throws {void} - None
     */
    LoanPay_PresentationController.prototype.navigateToLoanPay = function(response) {
        var self = this;
        self.showProgressBar();
        if (response)
            viewModel = response;
        this.presentLoanPay({
            loanPayoff: viewModel
        });
    };
    LoanPay_PresentationController.prototype.showView = function(frm, data) {
        this.presentUserInterface(frm, data);
    };
    /**
     * Function to Show Progress Bar
     * @member LoanPay_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    LoanPay_PresentationController.prototype.showProgressBar = function() {
        var self = this;
        self.presentLoanPay({
            "ProgressBar": {
                show: true
            }
        });
    };
    /**
     * Function to Hide Progress Bar
     * @member LoanPay_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    LoanPay_PresentationController.prototype.hideProgressBar = function() {
        var self = this;
        self.presentLoanPay({
            "ProgressBar": {
                show: false
            }
        });
    };
    /**
     * Function to create transfer for Loan Pay Off and Loan Pay Due Amount
     * @member LoanPay_PresentationController
     * @param {Object} data
     * @param {String} context stores context
     * @returns {void} - None
     * @throws {void} - None
     */
    LoanPay_PresentationController.prototype.payLoanOff = function(data, context) {
        var self = this;
        self.showProgressBar();
        applicationManager.getTransactionManager().createTransferToOwnAccounts(data, this.onSuccessPayLoanOff.bind(this, data, context), this.onFailurePayLoanOff.bind(this));
    };
    /**
     * Method called as success callback for PayLoanOff
     * @param {Object} response - response from service for PayLoanOff
     */
    LoanPay_PresentationController.prototype.onSuccessPayLoanOff = function(data, context, response) {
        this.hideProgressBar();
        var responseData = {
            "data": data,
            "referenceId": response.referenceId
        };
        if (context === "payOtherAmount") {
            this.presentLoanPay({
                payOtherAmount: responseData
            });
        } else if (context === "payCompleteDue") {
            this.presentLoanPay({
                payCompleteDue: responseData
            });
        } else if (context === "payCompleteMonthlyDue") {
            this.presentLoanPay({
                payCompleteMonthlyDue: responseData
            });
        }
    };
    /**
     * Method called as failure calback for PayLoanOff
     * @param {Object} response - response from service for PayLoanOff
     */
    LoanPay_PresentationController.prototype.onFailurePayLoanOff = function(response) {
        this.presentLoanPay({
            "serverError": response.errorMessage
        });
        this.hideProgressBar();
    };
    /**
     * Function to Navigate to Accounts module
     * @member LoanPay_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    LoanPay_PresentationController.prototype.backToAccount = function() {
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountsModule.presentationController.showAccountsDashboard();
    };
    /**
     * Function to fetch updated account object
     * @member LoanPay_PresentationController
     * @param {Object} accountID
     * @param {String} context stores context
     * @returns {void} - None
     * @throws {void} - None
     */
    LoanPay_PresentationController.prototype.fetchUpdatedAccountDetails = function(accountID, context) {
        this.loanaccount = accountID;
        var self = this;
        var param = {
            "accountID": accountID
        };
        applicationManager.getAccountManager().fetchInternalAccountByID(param, this.onSuccessFetchUpdatedAccountDetails.bind(this, context), this.onFailureFetchUpdatedAccountDetails.bind(this));
    };
    /**
     * Method called as success callback for FetchUpdatedAccountDetails
     * @param {Object} response - response from service for FetchUpdatedAccountDetails
     */
    LoanPay_PresentationController.prototype.onSuccessFetchUpdatedAccountDetails = function(context, response) {
        if (context === "newAccountSelection") {
            this.presentLoanPay({
                newAccountSelection: response
            });
        } else if (context === "navigationToAccountDetails") {
            var accountDetails = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
            accountDetails.presentationController.showAccountDetails(response[0]);
        } else if (context === "validateData") {
            this.presentLoanPay({
                validateData: response
            });
        } else if (context === "updateFromAccount") {
            this.presentLoanPay({
                updateFromAccount: response
            });
        } else if (context === "updateToAccount") {
            this.presentLoanPay({
                updateToAccount: response
            });
        } else if (context === "populateAccountData") {
            this.presentLoanPay({
                populateAccountData: response
            });
        } else if (context === "navigationToAccountDetailsfromLoan") {
            for (var i = 0; i < response.length; i++) {
                if (response[i].accountID == this.loanaccount) {
                    var accountDetails = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountDetails.presentationController.showAccountDetails(response[i]);
                }
            }
        }
    };
    /**
     * Method called as failure calback for FetchUpdatedAccountDetails
     * @param {Object} response - response from service for FetchUpdatedAccountDetails
     */
    LoanPay_PresentationController.prototype.onFailureFetchUpdatedAccountDetails = function(response) {
        this.hideProgressBar();
        CommonUtilities.showServerDownScreen();
    };
    LoanPay_PresentationController.prototype.fetchCheckingAccounts = function() {
        applicationManager.getAccountManager().fetchInternalAccounts(this.onSuccessFetchCheckingAccounts.bind(this), this.onFailureFetchCheckingAccounts.bind(this));
    };
    /**
     * Method called as success callback for FetchCheckingAccounts
     * @param {Object} response - response from service for FetchCheckingAccounts
     */
    LoanPay_PresentationController.prototype.onSuccessFetchCheckingAccounts = function(response) {
        this.presentLoanPay({
            "loadAccounts": response
        });
    };
    /**
     * Method called as failure calback for FetchCheckingAccounts
     * @param {Object} response - response from service for FetchCheckingAccounts
     */
    LoanPay_PresentationController.prototype.onFailureFetchCheckingAccounts = function(response) {
        this.hideProgressBar();
        CommonUtilities.showServerDownScreen();
    };
    return LoanPay_PresentationController;
});