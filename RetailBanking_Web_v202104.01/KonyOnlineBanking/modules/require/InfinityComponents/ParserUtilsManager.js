define(function () {

  /**
   * Parser Utility Manager Constructor
   */
  function ParserUtilsManager() {
    this.context = {};
    this.breakPointConfig = {};
  };
  /**
   * Set context object to ParserUtilityManager
   * @param {Object} context
   */
  ParserUtilsManager.prototype.setContext = function (context) {
    this.context = context;
  };
  /**
   * Set the breakpoint config to ParserUtilityManager
   * @param {Object} breakPointConfig 
   */
  ParserUtilsManager.prototype.setBreakPointConfig = function (breakPointConfig) {
    this.breakPointConfig = breakPointConfig;
  };
  ParserUtilsManager.prototype.getcomponentConfigValue = function(value, selectedtype) {
    var count = 0;
    for (var key in value) {
      count++;
      if (key.includes("$.BREAKPTS.")) {
        var key2 = this.getBreakPointValue(key.split("$.BREAKPTS.")[1])
        if (key2 == selectedtype) {
          return value[key];
        }
      }
      if (count == Object.keys(value).length) {
        return value["default"];
      }
    }
    return value;
  };
  ParserUtilsManager.prototype.getBreakPointValue = function (text) {
    var breakPointConfigValue = "";
    try {
      breakPointConfigValue = this.breakPointConfig[text];
    } catch (e) {
      kony.print(e);
    }
    if (breakPointConfigValue != "" && breakPointConfigValue != undefined && breakPointConfigValue != null) return breakPointConfigValue;
    return text;
  };
  /**
   * Identify the value is a JSON string or plain string.
   * @param {String} value
   * @return {String} - value for string
   */
  ParserUtilsManager.prototype.getParsedValue = function (value) {
    var JSONValue = {}
    try {
      JSONValue = JSON.parse(value);
      if (JSONValue && Object.keys(JSONValue).length > 0)
        return JSON.stringify(this.processJSON(JSONValue));
    }
    catch (e) {}
    return this.getParsedText(value);
  };
  /**
   * Parse JSON and replace the values in decorator pattern with actual values.
   * @param {JSON} JSONValue
   * @return {JSON} - JSON with actual values
   */
  ParserUtilsManager.prototype.processJSON = function (JSONValue) {
    for (var key in JSONValue) {
      JSONValue[key] = this.getParsedValue(JSONValue[key]);
    }
    return JSONValue;
  };
  /**
   * Parse text and return appropriate string
   * @param {JSON} text 
   * @return {String} - parsed String
   */
  ParserUtilsManager.prototype.getParsedText = function (text) {
    var parseStr = text;
    var delimiters = "i$"
    var regex = new RegExp("{[[" + delimiters + "\].+?(}{1,})", "g")
    var decoratorArray = parseStr.match(regex);
    if (decoratorArray) {
      for (var i = 0; i < decoratorArray.length; i++) {
        parseStr = parseStr.replace(decoratorArray[i], this.getDecoratorText(decoratorArray[i]));
      }
    }
    return parseStr;
  };
  /**
   * Parse decorator text and return appropriate string
   * @param {JSON} JSONValue
   * @return {String} - value either from contet or from service response
   */
  ParserUtilsManager.prototype.getDecoratorText = function (decoratorText) {
    if (decoratorText.indexOf("{i.") != -1)
      return this.geti18nText(decoratorText.substring(decoratorText.indexOf("{i.") + 3, decoratorText.length - 1));
    else if (decoratorText.indexOf("{$.") != -1) {
      var unicode = decoratorText.substring(3, decoratorText.indexOf(".", 3))
      var valueStartIndex = decoratorText.indexOf(".", 3) + 1;
      if (unicode == "c") {
        return this.getContextValue(decoratorText.substring(valueStartIndex, decoratorText.length - 1));
      } else {
        return this.getResponseValue(decoratorText.substring(valueStartIndex, decoratorText.length - 1), unicode);
      }
    }
  };
  /**
   * get i18n value for the String.
   * @param {String} text
   * @return {String} - i18n value for string
   */
  ParserUtilsManager.prototype.geti18nText = function (text) {
    var i18ntext = "";
    try {
      i18ntext = kony.i18n.getLocalizedString(text);
    } catch (e) {}
    if (i18ntext != "" && i18ntext != undefined && i18ntext != null)
      return i18ntext;
    return text;
  };
  /**
   * get context value for the String.
   * @param {String} text
   * @return {String} - context value for string
   */
  ParserUtilsManager.prototype.getContextValue = function (text) {
    var context = "";
    try {
      let parsedText = this.getParsedValue(text);
      context = this.context[parsedText];
      if (!context) {
        context = eval("this.context." + parsedText);
      }
    } catch (e) {}
    if (context != "" && context != undefined && context != null)
      return context;
    return "";
  };
  /**
   * get value from response data for the String.
   * @param {String} text
   * @return {String} - value for string
   */
  ParserUtilsManager.prototype.getResponseValue = function (text, unicode) {
    var responseValue = ""
    try {
      responseValue = this.unicode[unicode][this.getParsedValue(text)];
    } catch (e) {}
    if (responseValue != "" && responseValue != undefined && responseValue != null)
      return responseValue;
    return "";
  };
  return ParserUtilsManager;
});