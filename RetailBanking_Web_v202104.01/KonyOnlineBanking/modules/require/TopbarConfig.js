/*eslint require-jsdoc:0
          complexity:0
*/
define([], function () {

	var orientationHandler = new OrientationHandler();
    var checkUserFeature = function (feature) {
        return applicationManager.getConfigurationManager().checkUserFeature(feature);
    }
    var checkAtLeastOneFeaturePresent = function (features) {
        return features.some(checkUserFeature);
    }

    var checkUserPermission = function (permission) {
        return applicationManager.getConfigurationManager().checkUserPermission(permission);
    }

    var checkAtLeastOnePermission = function (permissions) {
        return permissions.some(checkUserPermission);
    }
    var TRANSFERS_CONTEXTUAL_MENU = [
        {
            isVisible: function () {
                return  checkAtLeastOneFeaturePresent([
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER",
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER",
                    "INTRA_BANK_FUND_TRANSFER",
                    "TRANSFER_BETWEEN_OWN_ACCOUNT"
                ]) 
            },
            id: "Transfers",
            widget: "flxTransferMoney",
            onClick: function () {               
                applicationManager.getModulesPresentationController("TransferModule").showTransferGatewayScreen({ gateway: "true" });
            }
        },
        {
            isVisible: function () {
                return checkUserFeature("BILL_PAY");
            },
            id: "Bill Pay",
            widget: "flxPayBills",
            onClick: function () {               
                   var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule");
                   billPayModule.presentationController.showBillPaymentScreen({ context: "BulkPayees", loadBills: true });
            }
        },
        {
            isVisible: function () {
               return checkUserFeature("P2P"); 
            },
            id: "Pay A Person",
            widget: "flxSendMoney",
            onClick: function () {              
                    var PayAPersonModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    PayAPersonModule.presentationController.showPayAPerson();
            }
        },
        {
            isVisible: function () {
                  var configurationManager = applicationManager.getConfigurationManager();
                var wireTransferEligible = applicationManager.getUserPreferencesManager().getWireTransferEligibleForUser();
                return (wireTransferEligible === "true" || wireTransferEligible === true) 
                &&  checkAtLeastOneFeaturePresent([
                    "DOMESTIC_WIRE_TRANSFER",
                    "INTERNATIONAL_WIRE_TRANSFER"
                ]);
            },
            id: "Wire Transfers",
            widget: "flxWireMoney",
            onClick: function () {               
                    var WireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                    WireTransferModule.presentationController.showWireTransfer();
            }
        },
    ];
    
    var FAST_TRANFERS_CONTEXTUAL_MENU = [
        {
            isVisible: function () {
                return checkAtLeastOnePermission([
                    "TRANSFER_BETWEEN_OWN_ACCOUNT_CREATE",
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE",
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE",
                    "INTRA_BANK_FUND_TRANSFER_CREATE",
                    "P2P_CREATE"
                ])		
            },
            id: "Transfers",
            widget: "flxTransferMoney",
            onClick: function () {
                  applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen();
            }
        },
        {
            isVisible: function () {
                return checkAtLeastOnePermission([
                    "TRANSFER_BETWEEN_OWN_ACCOUNT_VIEW",
                    "INTRA_BANK_FUND_TRANSFER_VIEW",
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER_VIEW",
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_VIEW",
                    "P2P_VIEW"
                ])	
            },
            id: "Bill Pay",
            widget: "flxPayBills",
            onClick: function () {
               // applicationManager.getModulesPresentationController("TransferFastModule").showScheduledTransactions();
			   applicationManager.getNavigationManager().navigateTo("frmPastPaymentsNew");
            }
        },
        {
            isVisible: function () {
                return checkAtLeastOnePermission([
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER_VIEW_RECEPIENT",
                    "INTRA_BANK_FUND_TRANSFER_VIEW_RECEPIENT",
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_VIEW_RECEPIENT",
                    "P2P_VIEW_RECEPIENT"
                ])
            },
            id: "Pay A Person",
            widget: "flxSendMoney",
            onClick: function () {
              kony.application.showLoadingScreen();
               kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule").presentationController.showTransferScreen({showManageRecipients:true}); 
            }
        },
        {
            isVisible: function () {
               return checkAtLeastOnePermission([
                "TRANSFER_BETWEEN_OWN_ACCOUNT_CREATE_RECEPIENT",
                "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE_RECEPIENT",
                "INTRA_BANK_FUND_TRANSFER_CREATE_RECEPIENT",
                "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE_RECEPIENT",
                "P2P_CREATE_RECEPIENT"
            ]);
            },
            id: "Wire Transfers",
            widget: "flxWireMoney",
            onClick: function () {
                var addRecipientTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule");
                addRecipientTransferModule.presentationController.showTransferScreen({showRecipientGateway:true});
            }
        }
    ];

    var EUROPE_CONTEXTUAL_MENU =[
        {
            isVisible: function () {
                return checkAtLeastOnePermission([
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE",
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE",
                    "INTRA_BANK_FUND_TRANSFER_CREATE",
                ])		
            },
            id: "Transfers",
            widget: "flxTransferMoney",
            onClick: function () {
                    applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "MakePayment" });
            }
        },
        {
            isVisible: function () {
                return checkUserPermission("TRANSFER_BETWEEN_OWN_ACCOUNT_CREATE");
            },
            id: "Bill Pay",
            widget: "flxPayBills",
            onClick: function () {
                    applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "MakePaymentOwnAccounts" });
            }
        },
        {
            isVisible: function () {
               return true;
            },
            id: "Pay A Person",
            widget: "flxSendMoney",
            onClick: function () {
                    applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "ManageBeneficiaries" });
            }
        },
        {
            isVisible: function () {
                return checkAtLeastOnePermission([
                    "TRANSFER_BETWEEN_OWN_ACCOUNT_VIEW",
                    "INTRA_BANK_FUND_TRANSFER_VIEW",
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER_VIEW",
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_VIEW",
                    "DIRECT_DEBIT_VIEW"
                ])	
            },
            id: "Wire Transfers",
            widget: "flxWireMoney",
            onClick: function () {
                    applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "PastPayments" });
                }
        },
        {
            isVisible: function () {
                // var configurationManager = applicationManager.getConfigurationManager();
                // if (configurationManager.getDeploymentGeography() === "EUROPE") {
                //     return checkUserFeature("PAY_MULTIPLE_BENEFICIARIES");
                // }
                return true;
            },
            id: "Pay Multiple Beneficiaries",
            widget: "flxPayMultipleBeneficiaries",
            onClick: function () {
                applicationManager.getModulesPresentationController("PayMultipleBeneficiariesModule").showPayMultipleBeneficiaries({"showManageBeneficiaries": true});
            }
        }
    ];

    var config = {
        getContextualMenu: function () {
            if (applicationManager.getConfigurationManager().getDeploymentGeography() === "EUROPE") {
                return EUROPE_CONTEXTUAL_MENU;
            }
          if (applicationManager.getConfigurationManager().isFastTransferEnabled == "false") {
            return TRANSFERS_CONTEXTUAL_MENU;
          }
            else {
             return FAST_TRANFERS_CONTEXTUAL_MENU;
            }
        },
        USER_ACTIONS: 
        [
            {   
                id: "Profile Settings",
                text: "i18n.ProfileManagement.profilesettings",
                isVisible: function () {
                    return checkUserPermission("PROFILE_SETTINGS_VIEW");
                },
                onClick: function () {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.enterProfileSettings("profileSettings");
                }
            },
            {
                id: "Security Settings",
                text: "i18n.ProfileManagement.SecuritySettings",
                onClick: function() {
                    var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    alertsModule.presentationController.enterProfileSettings("securityQuestions");
                }
            },
            {   
                id: "Account Settings",
                text: "i18n.Accounts.ContextualActions.updateSettingAndPreferences",
                isVisible: function () {
                    return checkUserPermission("ACCOUNT_SETTINGS_VIEW");
                },
                onClick: function () {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    //profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.enterProfileSettings("accountSettings");
                }
            },
            {
              id: "Approval Matrix",
              text: "i18n.Settings.ApprovalMatrix.approvalMatrix",
              onClick: function() {
                var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                profileModule.presentationController.enterProfileSettings("approvalMatrix");
              },
              isVisible : function() {
                return kony.application.getCurrentBreakpoint() !== 1024 && !orientationHandler.isTablet && checkUserPermission('APPROVAL_MATRIX_VIEW');
              }
            },          
            {   
                id: "Alert Settings",
                text: "i18n.ProfileManagement.Alerts",
                isVisible: function () {
                    return checkUserPermission("ALERT_MANAGEMENT");
                },
                onClick: function () {
                    var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    //alertsModule.presentationController.initializeUserProfileClass();
                    alertsModule.presentationController.enterProfileSettings("alertSettings");
                }
            },
            {   
                id: "Consent Management",
                text: "i18n.ProfileManagement.Consent",
                isVisible: function (){
                  return applicationManager.getConfigurationManager().checkUserPermission("CDP_CONSENT_VIEW");
                },
                onClick: function () {
                    var consentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    consentModule.presentationController.enterProfileSettings("consentManagement");
                }
            },
           {   
                id: "Manage Account Access",
                text: "i18n.ProfileManagement.ManageAccountAccess",
				isVisible: function (){
                  return applicationManager.getConfigurationManager().checkUserPermission("PSD2_TPP_CONSENT_VIEW");
                },
                onClick: function () {
                    var consentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    consentModule.presentationController.enterProfileSettings("manageAccountAccess");
                }
            }
        ],
        ACCOUNTS: {
            onClick: function () {
                var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                accountsModule.presentationController.showAccountsDashboard();
            }
        },
        BILLS: {
            onClick: function() {
                var payBillsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule");
                payBillsModule.presentationController.showBillPaymentScreen({ context: "BulkPayees", loadBills: true });
            }
        },
        NOTIFICATIONS: {
            onClick: function () {
                var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                 alertsModule.presentationController.showAlertsPage();
            }
        },
        MESSAGES: {
            onClick: function () {
                var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                alertsModule.presentationController.showAlertsPage('',{show:"Messages"});
            }
        },
        HELP: {
            onClick: function () {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                var formId = kony.application.getCurrentForm().id;
                informationContentModule.presentationController.showOnlineHelp(formId);
            }
        },
        LOGOUT: {
            excludedForms: ['frmContactUsPrivacyTandC', 
                            'frmOnlineHelp', 
                            'frmLocateUs', 
                            'frmNewUserOnboarding'
                        ],
            onClick: function () {
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                var context = {
                    "action": "Logout"
                };
                authModule.presentationController.doLogout(context);
            }
        },
        FEEDBACK: {
            onClick: function () {
                if (kony.application.getCurrentForm() !== "frmCustomerFeedback") {
                    var feedbackModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeedbackModule");
                    feedbackModule.presentationController.showFeedback();
                }
                
            }
        }
    }
    return {
        config: config
    }
});