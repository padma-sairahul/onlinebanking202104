/* eslint require-jsdoc: 0,
          olblint/enforce-i18n: 0,
          olblint/image-names: 0
*/
define(['FormControllerUtility','OLBConstants'], function(FormControllerUtility, OLBConstants) {
    var orientationHandler = new OrientationHandler();
    var checkUserFeature = function (feature) {
        return applicationManager.getConfigurationManager().checkUserFeature(feature);
    }
    var checkAtLeastOneFeaturePresent = function (features) {
        return features.some(checkUserFeature);
    }

    var checkUserPermission = function (permission) {
        return applicationManager.getConfigurationManager().checkUserPermission(permission);
    }

    var checkAtLeastOnePermission = function (permissions) {
        return permissions.some(checkUserPermission);
    }
	
	var checkAllPermissions = function (permissions) {
        return permissions.every(checkUserPermission);
    }

    var widgetsMap = [{
            id: "ACCOUNTS",
            menu: "flxAccountsMenu",
            text: "i18n.topmenu.accounts",
            toolTip: "i18n.topmenu.accounts",
            icon: "a",
            subMenu: {
                children: [{
                        id: "MY ACCOUNTS",
                        text: "i18n.hamburger.myaccounts",
                        toolTip: "i18n.hamburger.myaccounts",
                        onClick: function() {
                            var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                            accountsModule.presentationController.showAccountsDashboard();
                        }
                    },
                    {
                        id: "Stop Payment Requests",
                        text:  "i18n.olb.chequeManagement.chequeManagement",
                        toolTip:"i18n.olb.chequeManagement.chequeManagement",
                        onClick: function() {
                            var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
                            accountsModule.presentationController.showStopPayments();
                        },
                        isVisible: function() {
                          return checkAtLeastOneFeaturePresent([
							"CHEQUE_BOOK_REQUEST",
							"STOP_PAYMENT_REQUEST",
							"VIEW_CHEQUES"
						  ]);
                        }
                    },
                    {
                        id: "Open New Account",
                        text: "i18n.WireTransfer.CreateNewAccount",
                        toolTip: "i18n.WireTransfer.CreateNewAccount",
                        onClick: function() {
                            var nuoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
                            nuoModule.presentationController.showNewAccountOpening();
                        },
						isVisible: function() {
                          var configurationManager = applicationManager.getConfigurationManager();
                          if(configurationManager.isSMEUser === "true" || configurationManager.isMBUser === "true" )
                              return false;  
                          else
                              return checkUserFeature("NAO");
                        }
                    },
                    {
                        id: "Add External Bank Accounts",
                        text: "i18n.Hamburger.AddExternalBankAccounts",
                        toolTip: "i18n.Hamburger.AddExternalBankAccounts",
                        onClick: function() {
                            var externalAccountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ExternalAccountsModule");
                            externalAccountsModule.presentationController.getBankListForCountry();
                        },
                        isVisible: function() {                                              
                          	var configurationManager = applicationManager.getConfigurationManager();
                          	if(configurationManager.isRBUser === "true" || configurationManager.isCombinedUser === "true")
                            {
                                if(checkUserPermission("MANAGE_EXTERNAL_ACCOUNT") && applicationManager.getConfigurationManager().getConfigurationValue('AggregatedExternalAccountEnabled')){
                                    return true;  
                                }
                            }  
                          	else
                              	return false;
                        }
                    },
                    {
                        id: "Card Management",
                        text: "i18n.hamburger.cardmanagement",
                        toolTip: "i18n.hamburger.cardmanagement",
                        onClick: function() {
                            var cardsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
                            cardsModule.presentationController.navigateToManageCards();
                        },
                        isVisible: function () {
                            return checkUserFeature("CARD_MANAGEMENT");
                        }
                    },
                    {
                        id: "Disputed Transaction",
                        text: "i18n.StopCheckPayments.DisputedTransactions",
                        toolTip: "i18n.StopCheckPayments.DisputedTransactions",
                        onClick: function() {
                            var disputeModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("DisputeTransactionModule");
                            disputeModule.presentationController.showDisputeTransactionModule({
                                show: OLBConstants.ACTION.SHOW_DISPUTE_LIST
                            });
                        },
                        isVisible: function () {
                            return checkUserFeature("DISPUTE_TRANSACTIONS");
                        }
                    },
                  
                    {
                        id: "PFM",
                        text: "i18n.accounts.PersonalFinanceManagement",
                        toolTip: "i18n.accounts.PersonalFinanceManagement",
                        onClick: function() {
                            var pfmModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PersonalFinanceManagementModule");
                            pfmModule.presentationController.initPFMForm();
                        },
                        isVisible: function() {
                          var configurationManager = applicationManager.getConfigurationManager();
                          return ( (configurationManager.isRBUser === "true" || configurationManager.isCombinedUser === "true")
                                   && checkUserPermission("PERSONAL_FINANCE_MANAGEMENT"));
                        }
                    },
                    {
                        id: "Account Statements",
                        text: "i18n.hamburger.accountstatements",
                        toolTip: "i18n.hamburger.accountstatements",
                        onClick: function() {
                          var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule');
                          var accounts = presenter.presentationController.accounts; 
                          var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                          if(accounts!==null && accounts!==undefined &&  accounts.length>0){
                            accountsModule.presentationController.showFormatEstatements(accounts[0]);
	  						}
                        },
                       isVisible: function() {
                        return checkAtLeastOnePermission(["VIEW_COMBINED_STATEMENTS", "VIEW_ESTATEMENTS"])
                    }
                    },
                ]

            },
        },
        {
            isVisible: function() {
                var configurationManager = applicationManager.getConfigurationManager();
                return configurationManager.isFastTransferEnabled === "false" && configurationManager.getDeploymentGeography() !== "EUROPE" && checkAtLeastOneFeaturePresent([
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER",
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER",
                    "INTRA_BANK_FUND_TRANSFER",
                    "TRANSFER_BETWEEN_OWN_ACCOUNT"
                ])
            },
            id: "TRANSFERS",
            menu: "flxTransfers",
            text: "i18n.hamburger.transfers",
            toolTip: "i18n.hamburger.transfers",
            icon: "t",
            subMenu: {
                parent: "flxTransfersSubMenu",
                children: [{
                    id: "Transfer Money",
                    text: "i18n.billPay.BillPayMakeTransfer",
                    toolTip: "i18n.billPay.BillPayMakeTransfer",
                    isVisible: function ()  {
                        return checkAtLeastOnePermission([
                            "TRANSFER_BETWEEN_OWN_ACCOUNT_CREATE",
                            "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE",
                            "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE",
                            "INTRA_BANK_FUND_TRANSFER_CREATE",
                        ])
                    },
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen();
                    },
                }, {
                    id: "Transfer history",
                    text: "i18n.hamburger.transferHistory",
                    toolTip: "i18n.hamburger.transferHistory",
                    isVisible: function () {
                        return checkAtLeastOnePermission([
                            "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_VIEW",
                            "INTER_BANK_ACCOUNT_FUND_TRANSFER_VIEW",
                            "INTRA_BANK_FUND_TRANSFER_VIEW",
                            "TRANSFER_BETWEEN_OWN_ACCOUNT_VIEW",
                        ])
                    },
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            initialView: 'recent'
                        })
                    }
                }, {
                    id: "External Accounts",
                    text: "i18n.hamburger.externalAccounts",
                    toolTip: "i18n.hamburger.externalAccounts",
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            initialView: 'externalAccounts'
                        });
                    },
                    isVisible: function() {
                        return checkAtLeastOnePermission([
                            "TRANSFER_BETWEEN_OWN_ACCOUNT_VIEW_RECEPIENT",
                            "INTRA_BANK_FUND_TRANSFER_VIEW_RECEPIENT",
                            "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_VIEW_RECEPIENT"
                        ])
                    }
                }, {
                    id: "Add Infinity Accounts",
                    text: "i18n.hamburger.addKonyAccount",
                    toolTip: "i18n.hamburger.addKonyAccount",
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            initialView: "addInternalAccounts"
                        });
                    },
                    isVisible: function() {
                        return checkUserPermission("INTRA_BANK_FUND_TRANSFER_CREATE_RECEPIENT");
                    }
                }, {
                    id: "Add Non Kony Accounts",
                    text: "i18n.hamburger.addNonKonyAccount",
                    toolTip: "i18n.hamburger.addNonKonyAccount",
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            initialView: 'addExternalAccounts'
                        });
                    },
                    isVisible: function() {
                        return checkAtLeastOnePermission([
                            "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE_RECEPIENT",
                            "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE_RECEPIENT",
                        ])
                    }
                }]
            },
        },
        {		
            isVisible: function() {		
                var configurationManager = applicationManager.getConfigurationManager();		
                return  configurationManager.isFastTransferEnabled === "true" && 
                checkAtLeastOneFeaturePresent([
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER",
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER",
                    "INTRA_BANK_FUND_TRANSFER",
                    "P2P",
                    "TRANSFER_BETWEEN_OWN_ACCOUNT"
                ])
        },		
            id: "FASTTRANSFERS",		
        menu: "flxTransfers",		
        text: "i18n.hamburger.transfers",		
        toolTip: "i18n.hamburger.transfers",		
        icon: "t",		
        subMenu: {		
            parent: "flxTransfersSubMenu",		
            children: [{		
                id: "Transfer Money",		
                text: "i18n.hamburger.transfer",		
                toolTip: "i18n.hamburger.transfer",		
                onClick: function() {		
                    applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen();		
                },		
                isVisible: function() {		
                    var configurationManager = applicationManager.getConfigurationManager();		
                    return checkAtLeastOnePermission([
                        "TRANSFER_BETWEEN_OWN_ACCOUNT_CREATE",
                        "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE",
                        "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE",
                        "INTRA_BANK_FUND_TRANSFER_CREATE",
                        "P2P_CREATE"
                    ])		
                }		
            }, 		
            {		
                id: "Transfer history",		
                text: "i18n.Transfers.TRANSFERACTIVITIES",		
                toolTip: "i18n.Transfers.TRANSFERACTIVITIES",		
                onClick: function() {		
                  //  applicationManager.getModulesPresentationController("TransferFastModule").showScheduledTransactions();
				   applicationManager.getNavigationManager().navigateTo("frmPastPaymentsNew");
                },		
                 isVisible: function() {		
                   return checkAtLeastOnePermission([
                       "TRANSFER_BETWEEN_OWN_ACCOUNT_VIEW",
                       "INTRA_BANK_FUND_TRANSFER_VIEW",
                       "INTER_BANK_ACCOUNT_FUND_TRANSFER_VIEW",
                       "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_VIEW",
                       "P2P_VIEW"
                   ])	
                }		
            }		
            , {		
                id: "External Accounts",		
                text: "i18n.PayAPerson.ManageRecipient",		
                toolTip: "i18n.PayAPerson.ManageRecipient",		
                onClick: function() {
                  	kony.application.showLoadingScreen();
                     kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule").presentationController.showTransferScreen({
                       showManageRecipients:true
                     });		
                },		
               isVisible: function() {		
                return checkAtLeastOnePermission([
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER_VIEW_RECEPIENT",
                    "INTRA_BANK_ACCOUNT_FUND_TRANSFER_VIEW_RECEPIENT",
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_VIEW_RECEPIENT",
                    "P2P_VIEW_RECEPIENT"
                ])
                }
            }, {		
                id: "Add Infinity Accounts",		
                text: "i18n.PayAPerson.AddRecipient",		
                toolTip: "i18n.PayAPerson.AddRecipient",		
                onClick: function() {		
                     var addRecipientTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule");
                addRecipientTransferModule.presentationController.showTransferScreen({
                  showRecipientGateway:true
                });			
                },		
                isVisible: function() {		
                    return checkAtLeastOnePermission([
                        "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE_RECEPIENT",
                    	"INTRA_BANK_FUND_TRANSFER_CREATE_RECEPIENT",
                    	"INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE_RECEPIENT",
                    	"P2P_CREATE_RECEPIENT"
                    ]);
                }
            }]		
            },		
        },		
                              {		
            isVisible: function() {		
                var configurationManager = applicationManager.getConfigurationManager();		
                return configurationManager.getDeploymentGeography() === "EUROPE" && checkAtLeastOneFeaturePresent([
                    "INTERNATIONAL_ACCOUNT_FUND_TRANSFER",
                    "INTER_BANK_ACCOUNT_FUND_TRANSFER",
                    "INTRA_BANK_FUND_TRANSFER",
                    "TRANSFER_BETWEEN_OWN_ACCOUNT"
                ])
        },		
            id: "EUROTRANSFERS",		
        menu: "flxTransfers",		
        text: "i18n.TransfersEur.PaymentsAndTransfers",		
        toolTip: "i18n.TransfersEur.PaymentsAndTransfers",		
        icon: "t",		
        subMenu: {		
            parent: "flxTransfersSubMenu",		
            children: [{		
                id: "Make a Payment",		
                text: "i18n.TransfersEur.MakePayment",		
                toolTip: "i18n.TransfersEur.MakePayment",		
                onClick: function() {		
                    applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "MakePayment" });	
                },		
                isVisible: function() {		
                    return checkAtLeastOnePermission([
                        "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE",
                        "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE",
                        "INTRA_BANK_FUND_TRANSFER_CREATE",
                    ])
                }		
            }, 		
            {		
                id: "Transfer Between Accounts",		
                text: "i18n.TransfersEur.TransferBetweenAccounts",		
                toolTip: "i18n.TransfersEur.TransferBetweenAccounts",		
                onClick: function() {		
                    applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "MakePaymentOwnAccounts" });	
                },		
                 isVisible: function() {		
                  return checkUserPermission('TRANSFER_BETWEEN_OWN_ACCOUNT_CREATE');
                }		
            },		
            {		
                id: "Manage Beneficiaries",		
                text: "i18n.TransfersEur.ManageBeneficiaries",		
                toolTip: "i18n.TransfersEur.ManageBeneficiaries",		
                onClick: function() {		
                     applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "ManageBeneficiaries" });		
                },		
               isVisible: function() {		
                return true;
                }
            }, {		
                id: "Manage Payments",		
                text: "i18n.TransfersEur.ManageTransactions",		
                toolTip: "i18n.TransfersEur.ManageTransactions",		
                onClick: function() {
                  applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "PastPayments" });
                },		
                isVisible: function() {		
                    return checkAtLeastOnePermission([
                        "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_VIEW",
                        "INTER_BANK_ACCOUNT_FUND_TRANSFER_VIEW",
                        "INTRA_BANK_FUND_TRANSFER_VIEW",
                        "TRANSFER_BETWEEN_OWN_ACCOUNT_VIEW",
                        "DIRECT_DEBIT_VIEW"
                    ])
                }
            }]		
            },		
        },
         {
           id: "UNIFIEDTRANSFER",
           text: "i18n.UnifiedTransfer.HamburgerMenuTitle",
           toolTip: "i18n.UnifiedTransfer.HamburgerMenuTitle",
           skin: "sknLblFontType0a57b91c10db243",
           icon: "&",
           onClick: function() {
                        var navMan = applicationManager.getNavigationManager();
                        var configManager = applicationManager.getConfigurationManager();
                        navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
                    },
           isVisible: function() {
                        return checkAtLeastOnePermission(["INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE", "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE", "INTRA_BANK_FUND_TRANSFER_CREATE", ])
                    },
           subMenu: {
             children: []
           }
        },              
        {
            isVisible: function() {
                return checkUserFeature("BILL_PAY");
            },
            id: "Bill Pay",
            text : function(){
                 var configurationManager = applicationManager.getConfigurationManager();
                 return (configurationManager.isFastTransferEnabled === "true" ? "i18n.Pay.MyBills":"i18n.billPay.BillPay");
            },
            toolTip:  function(){
                 var configurationManager = applicationManager.getConfigurationManager();
                 return (configurationManager.isFastTransferEnabled === "true" ? "i18n.Pay.MyBills":"i18n.billPay.BillPay");
            },
            icon: function(){
                 var configurationManager = applicationManager.getConfigurationManager();
              return "B";
            },
            subMenu: {
                children: [{
                    id: "Pay a Bill",
                    text: "i18n.hamburger.payABill",
                    toolTip: "i18n.hamburger.payABill",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule");
                        if(checkUserPermission("BILL_PAY_BULK"))
                          billPayModule.presentationController.showBillPaymentScreen({ context: "BulkPayees", loadBills: true });
                        else
                          billPayModule.presentationController.showBillPaymentScreen({ context: "MakeOneTimePayment" }) ;
                    },
                    isVisible: function () {
                        return checkUserPermission("BILL_PAY_CREATE");
                    }
                }, {
                    id: "Bill Pay History",
                    text: "i18n.hamburger.billPayHistory",
                    toolTip: "i18n.hamburger.billPayHistory",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule");
                        billPayModule.presentationController.showBillPaymentScreen({ context: "History", loadBills: true });
                    },
                    isVisible: function () {
                        return checkAtLeastOnePermission(["BILL_PAY_CREATE", "BILL_PAY_VIEW_PAYEES"]);
                    }
                }, {
                    id: "My Payee List",
                    text: "i18n.hamburger.myPayeeList",
                    toolTip: "i18n.hamburger.myPayeeList",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule");
                        billPayModule.presentationController.showBillPaymentScreen({ context: "ManagePayees", loadBills: true });
                    },
                    isVisible: function () {
                        return checkUserPermission("BILL_PAY_VIEW_PAYEES");
                    }
                }, {
                    id: "Add Payee",
                    text: "i18n.billPay.addPayee",
                    toolTip: "i18n.billPay.addPayee",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule");
                        billPayModule.presentationController.showBillPaymentScreen({ context: "AddPayee" });
                    },
                    isVisible: function () {
                        return checkUserPermission("BILL_PAY_CREATE_PAYEES");
                    }
                }, {
                    id: "Make One Time Payment",
                    text: "i18n.BillPay.MAKEONETIMEPAYMENT",
                    toolTip: "i18n.BillPay.MAKEONETIMEPAYMENT",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule");
                        billPayModule.presentationController.showBillPaymentScreen({ context: "MakeOneTimePayment" }) ;
                    },
                    isVisible: function () {
                        return checkUserPermission("BILL_PAY_CREATE");
                    }
                }]
            }
        },
        {
          	isVisible: function() {
              return checkAtLeastOneFeaturePresent([
                'ACH_COLLECTION', 'ACH_PAYMENT', 'ACH_FILES'
              ]);
            },
            id: "ACH",
            text: "i18n.konybb.ACH.ACH", 
          	skin: "sknLblFontType0a57b91c10db243",
            icon: "$",
            subMenu: {
                children: [
                       {
                          isVisible: function() {
                              return ((orientationHandler.isTablet) || kony.application.getCurrentBreakpoint() === 1024 ) && 
                                (
                                (checkUserFeature("ACH_COLLECTION") && checkUserPermission("ACH_COLLECTION_CREATE_TEMPLATE")) || 
                                (checkUserFeature("ACH_PAYMENT") && checkUserPermission("ACH_PAYMENT_CREATE_TEMPLATE"))
                                );
                          },
                          id: "Create a Template",
                          text: "i18n.konybb.common.createATemplate",
                          onClick: function() {
                              var navigationObject = {
                                                      "requestData" : null,
                                                      "onSuccess"   : {
                                                                          "form"    : "frmACHDashboard",
                                                                          "module"  : "ACHModule",
                                                                          "context" : {
                                                                                      "key"          : BBConstants.CREATE_ACH_TEMPLATES,
                                                                                      "responseData" : null
                                                                                      }
                                                                      },
                                                          "onFailure" : {
                                                                          "form"    : "AuthModule/frmLogin",
                                                                          "module"  : "AuthModule",
                                                                          "context" : {
                                                                                      "key" 		   : BBConstants.LOG_OUT,
                                                                                      "responseData" : null
                                                                                      }          
                                                                      }
                                                      };
                              var ACHModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ACHModule");
                              ACHModule.presentationController.noServiceNavigate(navigationObject); 
                          }
                    },                  
                     {
                       	isVisible: function() {
                         	return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && (
                              checkAtLeastOnePermission(applicationManager.getConfigurationManager().getViewACHTemplatePermissionsList()) &&
                              checkAtLeastOnePermission(applicationManager.getConfigurationManager().getCreateACHTransactionPermissionsList())
                            );
                       	},
                        id: "Make Payment With Template",
                        text: "i18n.konybb.ACH.MakePaymentWithTemplate",
                        onClick: function() {
                            var navigationObject = {
                                                    "requestData" : null,
                                                    "onSuccess"   : {
                                                                        "form"    : "frmACHDashboard",
                                                                        "module"  : "ACHModule",
                                                                        "context" : {
                                                                                    "key"          : BBConstants.SHOW_ACH_TEMPLATES_TAB,
                                                                                    "responseData" : null
                                                                                    }
                                                                    },
                                                        "onFailure" : {
                                                                        "form"    : "AuthModule/frmLogin",
                                                                        "module"  : "AuthModule",
                                                                        "context" : {
                                                                                    "key" 		   : BBConstants.LOG_OUT,
                                                                                    "responseData" : null
                                                                                    }          
                                                                    }
                                                    };
                            var ACHModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ACHModule");
                            ACHModule.presentationController.noServiceNavigate(navigationObject); 
                        }
                  },
                  {
                    isVisible: function() {
                      return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && checkAtLeastOnePermission(
                        	applicationManager.getConfigurationManager().getCreateACHTransactionPermissionsList()
                        );
                    },
                    id: "Make One Time Payment",
                    text: "i18n.konybb.ACH.MakeOneTimePayment",
                    onClick: function() {
                            var navigationObject = {
                                                    "requestData" : null,
                                                    "onSuccess"   : {
                                                                        "form"    : "frmACHDashboard",
                                                                        "module"  : "ACHModule",
                                                                        "context" : {
                                                                                    "key"          : BBConstants.TRANSACTION_WITHOUT_TEMPLATE,
                                                                                    "responseData" : null
                                                                                    }
                                                                    },
                                                        "onFailure" : {
                                                                        "form"    : "AuthModule/frmLogin",
                                                                        "module"  : "AuthModule",
                                                                        "context" : {
                                                                                    "key" 		   : BBConstants.LOG_OUT,
                                                                                    "responseData" : null
                                                                                    }          
                                                                    }
                                                    };
                            var ACHModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ACHModule");
                            ACHModule.presentationController.noServiceNavigate(navigationObject); 
                        }
                  }, 
                  {
                    isVisible: function() {
                      return checkAtLeastOnePermission(
                        	applicationManager.getConfigurationManager().getViewACHTransactionPermissionsList()
                        );
                    },
                    id: "ACH History", 
                    text: "i18n.konybb.ACH.ACHHistory",
                    onClick: function() {
                            var navigationObject = {
                                                    "requestData" : null,
                                                    "onSuccess"   : {
                                                                        "form"    : "frmACHDashboard",
                                                                        "module"  : "ACHModule",
                                                                        "context" : {
                                                                                    "key"          : BBConstants.SHOW_ACH_TRANSACTIONS_TAB,
                                                                                    "responseData" : null
                                                                                    }
                                                                    },
                                                        "onFailure" : {
                                                                        "form"    : "AuthModule/frmLogin",
                                                                        "module"  : "AuthModule",
                                                                        "context" : {
                                                                                    "key" 		   : BBConstants.LOG_OUT,
                                                                                    "responseData" : null
                                                                                    }          
                                                                    }
                                                    };
                            var ACHModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ACHModule");
                            ACHModule.presentationController.noServiceNavigate(navigationObject); 
                        }
                  }, 
                  {
                    isVisible: function() {
                      return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && checkAtLeastOnePermission(
						applicationManager.getConfigurationManager().getViewACHFilePermissionsList()
                      );
                    },
                    id: "Files",
                    text: "i18n.konybb.hamburger.ACHFiles",
                    onClick: function() {
                            var navigationObject = {
                                                    "requestData" : null,
                                                    "onSuccess"   : {
                                                                        "form"    : "frmACHDashboard",
                                                                        "module"  : "ACHModule",
                                                                        "context" : {
                                                                                    "key"          : BBConstants.SHOW_ACH_FILES_TAB,
                                                                                    "responseData" : null
                                                                                    }
                                                                    },
                                                        "onFailure" : {
                                                                        "form"    : "AuthModule/frmLogin",
                                                                        "module"  : "AuthModule",
                                                                        "context" : {
                                                                                    "key" 		   : BBConstants.LOG_OUT,
                                                                                    "responseData" : null
                                                                                    }          
                                                                    }
                                                    };
                            var ACHModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ACHModule");
                            ACHModule.presentationController.noServiceNavigate(navigationObject); 
                        }
                  }]
            }
        },
        {
          isVisible: function() {		
            var configurationManager = applicationManager.getConfigurationManager();		
         	var features = configurationManager.getBulkPaymentFeaturePermissionsList();
            return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && checkAtLeastOnePermission(features);	
          },
          
            id: "Bulk Payments",
            text: "i18n.kony.BulkPayments.bulkPaymentHeader",
            toolTip: "i18n.kony.BulkPayments.bulkPaymentHeader",
            skin: "sknLblFontType0a57b91c10db243",
            icon: "#",
            subMenu: {
              children: [
               {
                  id: "Upload Status",
                  text: "i18n.kony.BulkPayments.viewUploadedfiles",
                  toolTip:"i18n.kony.BulkPayments.viewUploadedfiles",
                 isVisible: function() {
                   var features = applicationManager.getConfigurationManager().getBulkPaymentFilesViewPermissionsList();
                   return checkAtLeastOnePermission(features);
                  },
                  onClick: function() {
                    
                    var BulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
                    BulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsDashboard","Upload Status");
                  }
                },
                {
                  id: "Review & Submit",
                  text: "i18n.kony.BulkPayments.PaymentsStatus",
                  toolTip:"i18n.kony.BulkPayments.PaymentsStatus",
                   isVisible: function() {
                     var features = applicationManager.getConfigurationManager().getBulkPaymentRequestViewPermissionsList();
                     return checkAtLeastOnePermission(features);
                  },
                  onClick: function() {
                    var BulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
                    BulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsDashboard","Review & Submit"); 
                  }
                },
                
                {
                  id: "Processed Requests",
                  text: "i18n.kony.BulkPayments.paymentHistory",
                  toolTip:"i18n.kony.BulkPayments.paymentHistory",
                 isVisible: function() {
                   var features = applicationManager.getConfigurationManager().getBulkPaymentRequestViewPermissionsList();
                   return checkAtLeastOnePermission(features);
                  },
                  onClick: function() {
                    
                    var BulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
                    BulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsDashboard","Processed Requests");
                  }
                },
                {
                  id: "Templates",
                  text: "i18n.kony.BulkPayments.viewTemplates",
                  toolTip:"i18n.kony.BulkPayments.viewTemplates",
                 isVisible: function() {
                   var features = applicationManager.getConfigurationManager().getBulkPaymentTemplateViewPermissionsList();
                   return checkAtLeastOnePermission(features);
                  },
                  onClick: function() {
                    
                    var BulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
                    BulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsDashboard",BBConstants.BULKPAYMENT_VIEW_TEMPLATES);
                  }
                },
                 {
                  id: "Upload File",
                  text: "i18n.kony.BulkPayments.uploadFileAndMakeBulkPayments",
     				toolTip: "i18n.kony.BulkPayments.uploadFileAndMakeBulkPayments",
                   isVisible: function() {
                     var features = applicationManager.getConfigurationManager().getBulkPaymentFileUploadPermissionsList();
                     return checkAtLeastOnePermission(features);
                  },
                  onClick: function() {
                    var BulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
                    BulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsUploadFile",BBConstants.BULKPAYMENTS_UPLOAD_FILE);
                  }
                },
                {
                  id: "Create New Template",
                  text: "i18n.kony.BulkPayments.createTemplates",
                  toolTip: "i18n.kony.BulkPayments.createTemplates",
                   isVisible: function() {
                     var features = applicationManager.getConfigurationManager().getBulkPaymentTemplateCreatePermissionsList();
                     return checkAtLeastOnePermission(features);
                  },
                  onClick: function() {
                    var BulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
                    BulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsTemplate",BBConstants.BULKPAYMENTS_CREATE_TEMPLATE);
                  }
                }
              ]
            }
          },
         {
           id: "Exchange Rates",
           text: "i18n.kony.exchangeRates.ExchangeRatesHeader",
           toolTip: "i18n.kony.exchangeRates.ExchangeRatesHeader",
           skin: "sknLblFontType0a57b91c10db243",
           icon: "&",
           onClick: function() {
             var ForeignExchangeModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ForeignExchange");
             ForeignExchangeModule.presentationController.noServiceNavigate();
           },
           isVisible: function() {
             return checkUserFeature("FX_RATES");
           },
           subMenu: {
             children: []
           }
        }, 
        {
            isVisible: function() {
              var configurationManager = applicationManager.getConfigurationManager();
        	  //if(configurationManager.isSMEUser === "true" || configurationManager.isMBBUser === "true" || configurationManager.isCombinedUser === "true"){
                var features = configurationManager.getApprovalReqModulePermissionsList();
              	return checkAtLeastOnePermission(features);
              //}
//               else{
//                 return false;
//               }
            },
            id: "Approvals Requests",
            text: "i18n.konybb.Common.ApprovalsRequests",
          	skin: "sknLblFontType0a57b91c10db243",
            icon: "%",
            subMenu: {
                children: [
                  {
                        id: "My Approvals",
                        text: "i18n.konybb.Common.MyApprovals",
                    	isVisible: function() {
                           var features = applicationManager.getConfigurationManager().getApprovalsFeaturePermissionsList();
	                       return checkAtLeastOnePermission(features);
	                    },
                        onClick: function() {
                            var navigationObject = {
                                                    "requestData" : null,
                                                    "onSuccess"   : {
                                                                        "form"    : "frmBBApprovalsDashboard",
                                                                        "module"  : "ApprovalsReqModule",
                                                                        "context" : {
                                                                                    "key"          : BBConstants.DASHBOARD_DEFAULT_TAB,
                                                                                    "responseData" : null
                                                                                    }
                                                                    },
                                                        "onFailure" : {
                                                                        "form"    : "AuthModule/frmLogin",
                                                                        "module"  : "AuthModule",
                                                                        "context" : {
                                                                                    "key" 		   : BBConstants.LOG_OUT,
                                                                                    "responseData" : null
                                                                                    }          
                                                                    }
                                                    };
                            var ApprovalsReqModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
                            ApprovalsReqModule.presentationController.noServiceNavigate(navigationObject); 
                        }
                  },
                  {
                        id: "My Requests",
                        text: "i18n.konybb.Common.MyRequests",
                    	isVisible: function() {
                          var features = applicationManager.getConfigurationManager().getRequestsFeaturePermissionsList();
	                      return checkAtLeastOnePermission(features);
	                    },
                        onClick: function() {
                            var navigationObject = {
                                                    "requestData" : null,
                                                    "onSuccess"   : {
                                                                        "form"    : "frmBBRequestsDashboard",
                                                                        "module"  : "ApprovalsReqModule",
                                                                        "context" : {
                                                                                    "key"          : BBConstants.DASHBOARD_DEFAULT_TAB,
                                                                                    "responseData" : null
                                                                                    }
                                                                    },
                                                        "onFailure" : {
                                                                        "form"    : "AuthModule/frmLogin",
                                                                        "module"  : "AuthModule",
                                                                        "context" : {
                                                                                    "key" 		   : BBConstants.LOG_OUT,
                                                                                    "responseData" : null
                                                                                    }          
                                                                    }
                                                    };
                            var ApprovalsReqModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
                            ApprovalsReqModule.presentationController.noServiceNavigate(navigationObject); 
                        }
					},
                  {
                    id: "Approval History",
                    text: "i18n.konybb.Common.approvalHistory",
                    isVisible: function() {
                      var features = applicationManager.getConfigurationManager().getBulkPaymentsApprovalsFeaturePermissionsList();
//                       return !(kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) && checkAtLeastOnePermission(features);
                      return false;
                    },
                    onClick: function() {
                        var navigationObject = {
                                                "requestData" : null,
                                                "onSuccess"   : {
                                                                    "form"    : "frmBBApprovalHistory",
                                                                    "module"  : "ApprovalsReqModule",
                                                                    "context" : {
                                                                                "key"          : BBConstants.DASHBOARD_DEFAULT_TAB,
                                                                                "responseData" : null
                                                                                }
                                                                },
                                                    "onFailure" : {
                                                                    "form"    : "AuthModule/frmLogin",
                                                                    "module"  : "AuthModule",
                                                                    "context" : {
                                                                                "key"          : BBConstants.LOG_OUT,
                                                                                "responseData" : null
                                                                                }          
                                                                }
                                                };
                        var ApprovalsReqModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
                        ApprovalsReqModule.presentationController.noServiceNavigate(navigationObject); 
                    }
                  },
                  {
                    id: "Request History",
                    text: "i18n.konybb.Common.requestHistory",
                    isVisible: function() {
                      var features = applicationManager.getConfigurationManager().getBulkPaymentRequestPermissionsList();
//                       return !(kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) && checkAtLeastOnePermission(features);
                      return false;
                    },
                    onClick: function() {
                            var navigationObject = {
                                                    "requestData" : null,
                                                    "onSuccess"   : {
                                                                        "form"    : "frmBBRequestHistory",
                                                                        "module"  : "ApprovalsReqModule",
                                                                        "context" : {
                                                                                    "key"          : BBConstants.DASHBOARD_DEFAULT_TAB,
                                                                                    "responseData" : null
                                                                                    }
                                                                    },
                                                        "onFailure" : {
                                                                        "form"    : "AuthModule/frmLogin",
                                                                        "module"  : "AuthModule",
                                                                        "context" : {
                                                                                    "key"          : BBConstants.LOG_OUT,
                                                                                    "responseData" : null
                                                                                    }          
                                                                    }
                                                    };
                            var ApprovalsReqModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
                            ApprovalsReqModule.presentationController.noServiceNavigate(navigationObject); 
                        }
                  }
                ]
            }
        },              
		{
            isVisible: function() {
              return checkAtLeastOneFeaturePresent([
                OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER,
                OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER
              ])
            },
            id: "Wire Transfer",
            text: "i18n.transfers.wireTransfer",
            toolTip: "i18n.transfers.wireTransfer",
           icon: function() {
            return "6";
        },
            subMenu: {
                children: [{
                    id: "Make Transfer",
                    text: "i18n.AccountDetails.MAKETRANSFER",
                    toolTip: "i18n.AccountDetails.MAKETRANSFER",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "makeTransfer"
                        })
                    },
                    isVisible: function() {
                        return checkAtLeastOnePermission([
                            OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_CREATE,
                            OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_CREATE
                        ])
                      },
                }, {
                    id: "History",
                    text: "i18n.billPay.History",
                    toolTip: "i18n.billPay.History",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "wireTransferHistory"
                        })
                    },
                    isVisible: function () {
                        return checkAtLeastOnePermission([
                            OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_CREATE,
                            OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_CREATE,
                            OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_VIEW,
                            OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_VIEW
                        ])
                    }
                }, {
                    id: "My Recipients",
                    text: "i18n.WireTransfer.MyRecepient",
                    toolTip: "i18n.WireTransfer.MyRecepient",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "myRecipients"
                        })
                    },
                    isVisible: function () {
                        return checkAtLeastOnePermission([
                            "DOMESTIC_WIRE_TRANSFER_VIEW_RECEPIENT",
                          	"DOMESTIC_WIRE_TRANSFER_DELETE_RECEPIENT",
                          	"DOMESTIC_WIRE_TRANSFER_CREATE_RECEPIENT",
                          	"INTERNATIONAL_WIRE_TRANSFER_CREATE_RECEPIENT",
                          	"INTERNATIONAL_WIRE_TRANSFER_DELETE_RECEPIENT",
                          	"INTERNATIONAL_WIRE_TRANSFER_VIEW_RECEPIENT"
                        ])
                    }
                }, {
                    id: "Add Recipient",
                    text: "i18n.PayAPerson.AddRecipient",
                    toolTip: "i18n.PayAPerson.AddRecipient",
                   onClick: function() {
                     if((checkUserPermission("DOMESTIC_WIRE_TRANSFER_CREATE_RECEPIENT") === true) || ((checkUserPermission("DOMESTIC_WIRE_TRANSFER_CREATE_RECEPIENT") === true) && (checkUserPermission("INTERNATIONAL_WIRE_TRANSFER_CREATE_RECEPIENT") === true)))
                       {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
                        wireTransferModule.presentationController.showWireTransferAddRecipientStep1({
                            landingPageView: "addRecipient"
                        })
                       }
                     else if (checkUserPermission("INTERNATIONAL_WIRE_TRANSFER_CREATE_RECEPIENT") === true)
                       {
                          var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
                        wireTransferModule.presentationController.showWireTransferInternationalStep1({
                            landingPageView: "addRecipientInternational"
                        }) 
                       }
                    },
                   isVisible: function () {
                       return checkAtLeastOnePermission([
                        OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_CREATE_RECEPIENT,
                        OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_CREATE_RECEPIENT
                    ])
                   }
                }, {
                    id: "Make One Time Payment",
                    text: "i18n.BillPay.MAKEONETIMEPAYMENT",
                    toolTip: "i18n.BillPay.MAKEONETIMEPAYMENT",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
                        wireTransferModule.presentationController.showOneTimeWireTransfer({
                            landingPageView: "oneTimeTransfer"
                        })
                    },
                    isVisible: function() {
                        return checkAtLeastOnePermission([
                            OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_CREATE,
                            OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_CREATE
                         ])
                      },
                },{
                    id: "Create New Template",
                    text: "i18n.wireTransfers.CreateNewTemplate",
                    toolTip: "i18n.wireTransfers.CreateNewTemplate",
                    onClick: function() {
                       var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
					    wireTransferModule.presentationController.resetPrimaryDetails();
					    wireTransferModule.presentationController.resetRecipientData();
                        wireTransferModule.presentationController.navigateToCreateTemplateForm();

                    },
                    isVisible: function() {
                        return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && checkAtLeastOnePermission([
                            OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_UPDATE_BULK_TEMPLATES,
                            OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_UPDATE_BULK_TEMPLATES
                        ])
                      },
                },{
                    id: "Files & Templates",
                    text: "i18n.wireTransfers.Files&Templates",
                    toolTip: "i18n.wireTransfers.Files&Templates",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
                        var params = {
                          "formName" : "frmBulkTransferFiles",
                          "bulkWireCategoryFilter":"All"
                        };
                        wireTransferModule.presentationController.showBulkwirefiles(params);
                    },
                    isVisible: function() {
                        return checkAtLeastOnePermission([
                            OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_VIEW_BULK_FILES,
                            OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_VIEW_BULK_FILES, 
                            OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_VIEW_BULK_TEMPLATES,
                            OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_VIEW_BULK_TEMPLATES
                        ])
                      },
                },{
                    id: "Make Bulk Transfer",
                    text: "i18n.bulkWire.makeBulkTransfer",
                    toolTip: "i18n.bulkWire.makeBulkTransfer",
                    onClick: function() {
                      var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
                      var params = {
                          "formName" : "frmMakeBulkTransferTemplate",
                          "bulkWireCategoryFilter": OLBConstants.BULKWIRE_CATEGORY_FILTER.TEMPLATES
                        };
                        wireTransferModule.presentationController.showBulkwirefiles(params);
                    },
                    isVisible: function() {
                        return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && 
                        ((checkAtLeastOnePermission([ OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_VIEW_BULK_FILES,OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_VIEW_BULK_TEMPLATE]) 
                         && checkAllPermissions([OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_CREATE])) ||
                        (checkAtLeastOnePermission([OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_VIEW_BULK_FILES, OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_VIEW_BULK_TEMPLATES]) 
                         && checkAllPermissions([OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_CREATE])))
                      },
                }, {
                    id: "Add Bulk Transfer File",
                    text: "i18n.BulkWire.AddBulkWireFile",
                    toolTip: "i18n.BulkWire.AddBulkWireFile",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferNew");
                        applicationManager.getNavigationManager().navigateTo("frmAddBulkTransferFile");
                    },
                    isVisible: function() {
                         return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && checkAtLeastOnePermission([
                           OLBConstants.BULK_WIRE_PERMISSIONS.DOMESTIC_WIRE_TRANSFER_UPLOAD_BULK_FILES,
                           OLBConstants.BULK_WIRE_PERMISSIONS.INTERNATIONAL_WIRE_TRANSFER_UPLOAD_BULK_FILES
                        ])
                      },
                }]
            }
        },
        {
            id: "ALERTS AND MESSAGES",
            text : function(){
             var configurationManager = applicationManager.getConfigurationManager();
             return (configurationManager.isFastTransferEnabled === "true" ? "i18n.AlertsAndMessages.Message":"i18n.AlertsAndMessages.AlertsAndMessages");
        },
        toolTip:  function(){
             var configurationManager = applicationManager.getConfigurationManager();
             return (configurationManager.isFastTransferEnabled === "true" ? "i18n.AlertsAndMessages.Message":"i18n.AlertsAndMessages.AlertsAndMessages");
        },
        isVisible: function () {
            return checkUserFeature("MESSAGES");
        },
       icon: "m",
            subMenu: {
                parent: "flxTransfersSubMenu",
                children: [{
                    id: "Alerts",
                    text: "i18n.AlertsAndMessages.Alerts",
                    toolTip: "i18n.AlertsAndMessages.Alerts",
                    onClick: function() {
                        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                        alertsMsgsModule.presentationController.showAlertsPage();
                    },
                    isVisible: function () {
                        return checkUserPermission("NOTIFICATION_VIEW");
                    },
                }, {
                    id: "My Messages",
                    text: "i18n.AlertsAndMessages.Messages",
                    toolTip: "i18n.AlertsAndMessages.Messages",
                    onClick: function() {
                        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                        alertsMsgsModule.presentationController.showAlertsPage("hamburgerMenu", {
                            show: "Messages"
                        });
                    },
                    isVisible: function () {
                        return checkUserPermission("MESSAGES_VIEW");
                    },
                }, {
                    id: "New Message",
                    text: "i18n.AlertsAndMessages.NewMessagesMod",
                    toolTip: "i18n.AlertsAndMessages.NewMessagesMod",
                    onClick: function() {
                        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                        alertsMsgsModule.presentationController.showAlertsPage("hamburgerMenu", {
                            show: "CreateNewMessage"
                        });
                    },
                    isVisible: function () {
                        return checkUserPermission("MESSAGES_CREATE_OR_REPLY");
                    }
                }]
            }
        },
        {
            isVisible: function() {
               return checkUserFeature("USER_MANAGEMENT")
            },
            id: "User Management",
            text: "i18n.common.UserManagement",
            toolTip: "i18n.common.UserManagement",
            icon: "u",
            subMenu: {
                children: [{
                    id: "All Users",
                    text: "i18n.userManagement.allUsers",
                    toolTip: "i18n.userManagement.allUsers",
                    onClick: function() {
                        var userModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
                        userModule.presentationController.showUserManagent({
                            show: 'showAllUsers'
                        });
                    },
                    isVisible: function () {
                        return checkAtLeastOnePermission([
                            "USER_MANAGEMENT_ACTIVATE",
                            "USER_MANAGEMENT_DELETE",
                            "USER_MANAGEMENT_SUSPEND",
                            "USER_MANAGEMENT_VIEW"
                        ]);
                    }
                },{
                    id: "User Roles",
                    text: "i18n.customRoles.userRoles",
                    toolTip: "i18n.customRoles.userRoles",
                    onClick: function() {
                      var userModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
                      userModule.presentationController.showUserManagent({
                         show: 'showUserRoles'
                      });
                    },
                    isVisible: function () {
                      return checkAtLeastOnePermission([ 
                        "CUSTOM_ROLES_VIEW"
                      ]) && !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile);
                    }
                }, /*{
                    id: "Create A User",
                    text: "i18n.userManagement.createAuser",
                    toolTip: "i18n.userManagement.createAuser",
                    onClick: function() {
                        var userModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
                        userModule.presentationController.showUserManagent({
                            show: 'createNewUser'
                        });
                    },
                    isVisible: function() {
                        return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && checkUserPermission("USER_MANAGEMENT_CREATE");
                    }
                },*/
				{
                    id: "Create UM User",
                    text: "i18n.userManagement.createAuser",
                    toolTip: "i18n.userManagement.createAuser",
                    onClick: function() {
                        var userModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
                        userModule.presentationController.showUserManagent({
                            show: 'creatUMUser'
                        });
                    },
                    isVisible: function() {
                        return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && checkUserPermission("USER_MANAGEMENT_CREATE");
                    }
                }, 
				{
                    id: "Create Custom Role",
                    text: "i18n.customRole.createCustomRole",
                    toolTip: "i18n.customRole.createCustomRole",
                    onClick: function() {
                        var userModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
                        userModule.presentationController.showUserManagent({
                            show: 'createNewCustomRole'
                        });
                    },
                    isVisible: function() {
                        return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) && checkUserPermission("CUSTOM_ROLES_CREATE");
                    }
                }]
            }
        },
        {
            isVisible: function() {
                var configurationManager = applicationManager.getConfigurationManager();
                return configurationManager.isFastTransferEnabled === "false" && checkUserFeature("P2P") && 
                applicationManager.getConfigurationManager().getDeploymentGeography() !== "EUROPE"; 
            },
            id: "Pay a Person",
            text: "i18n.p2p.PayAPerson",
            toolTip: "i18n.p2p.PayAPerson",
            icon: "P",
            subMenu: {
                children: [{
                    id: "Send Money",
                    text: "i18n.Pay.SendMoney",
                    toolTip: "i18n.Pay.SendMoney",
                    onClick: function() {
                        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("sendMoneyTab");
                    },
                    isVisible: function () {
                        return checkUserPermission("P2P_CREATE");
                    }
                }, {
                    id: "History",
                    text: "i18n.billPay.History",
                    toolTip: "i18n.billPay.History",
                    onClick: function() {
                        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("SentTransactionsTab");
                    },
                    isVisible: function () {
                        return checkUserPermission("P2P_VIEW");
                    }
                }, {
                    id: "My Recipients",
                    text: "i18n.WireTransfer.MyRecepient",
                    toolTip: "i18n.WireTransfer.MyRecepient",
                    onClick: function() {
                        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("ManageRecipients");

                    },
                    isVisible: function () {
                        return checkAtLeastOnePermission([
                            "P2P_MANAGE_PAYEES",
                            "P2P_CREATE"
                        ])
                    }
                }, {
                    id: "Add Recipient",
                    text: "i18n.PayAPerson.AddRecipient",
                    toolTip: "i18n.PayAPerson.AddRecipient",
                    onClick: function() {
                        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("AddRecipient");

                    },
                    isVisible: function () {
                        return checkUserPermission("P2P_MANAGE_PAYEES");
                    }
                }, {
                    id: "Send Money to New Recipient",
                    text: "i18n.PayAPerson.SendMoneyToNewRecipient",
                    toolTip: "i18n.PayAPerson.SendMoneyToNewRecipient",
                    onClick: function() {
                    	var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("sendMoneyToNewRecipient");
                    },
                    isVisible: function () {
                        return checkUserPermission("P2P_CREATE");
                    }
                }]
            }
        },
        {
            isVisible: function() {
              return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) &&  checkUserFeature("PROFILE_SETTINGS");
            },
            id: "Settings",
            text: "i18n.ProfileManagement.Settingscapson",
            toolTip: "i18n.ProfileManagement.Settingscapson",
            icon: "s",
            subMenu: {
                children: [{
                    id: "Profile Settings",
                  	text: "i18n.ProfileManagement.profilesettings",
                    toolTip: "i18n.ProfileManagement.profilesettings",
                    onClick: function() {
                        var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        profileModule.presentationController.enterProfileSettings("profileSettings");
                    },
                  	isVisible : function() {
                      return checkUserPermission('PROFILE_SETTINGS_VIEW');
                    }
                }, 
                {
                    id: "Security Settings",
                    text: "i18n.ProfileManagement.SecuritySettings",
                    toolTip: "i18n.ProfileManagement.SecuritySettings",
                    onClick: function() {
                        var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        alertsModule.presentationController.enterProfileSettings("securityQuestions");
                    }
                },
                {
                    id: "Account Settings",
                    text: "i18n.Accounts.ContextualActions.updateSettingAndPreferences",
                    toolTip: "i18n.Accounts.ContextualActions.updateSettingAndPreferences",
                    onClick: function() {
                        var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        //profileModule.presentationController.initializeUserProfileClass();
                        profileModule.presentationController.enterProfileSettings("accountSettings");
                    },
                    isVisible: function () {
                        return checkUserPermission("ACCOUNT_SETTINGS_VIEW");
                    }
                },
                {
                    id: "Approval Matrix",
                    text: "i18n.Settings.ApprovalMatrix.approvalMatrix",
                    toolTip: "i18n.Settings.ApprovalMatrix.approvalMatrix",
                    onClick: function() {
                        var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        profileModule.presentationController.enterProfileSettings("approvalMatrix");
                    },
                  	isVisible : function() {
                      return kony.application.getCurrentBreakpoint() !== 1024 && !orientationHandler.isTablet && checkUserPermission('APPROVAL_MATRIX_VIEW');
                    }
                },                           
                {
                    id: "Alert Settings",
                    text: "i18n.ProfileManagement.Alerts",
                    toolTip: "i18n.ProfileManagement.Alerts",
                    onClick: function() {
                        var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        //alertsModule.presentationController.initializeUserProfileClass();
                        alertsModule.presentationController.enterProfileSettings("alertSettings");
                    },
                    isVisible: function () {
                        return checkUserPermission("ALERT_MANAGEMENT");
                    }
                },
                {
                    id: "Consent Management",
                    text: "i18n.ProfileManagement.Consent",
                    toolTip: "i18n.ProfileManagement.Consent",
                    onClick: function() {
                        var consentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        consentModule.presentationController.enterProfileSettings("consentManagement");
                    },
                    isVisible: function (){
                        return applicationManager.getConfigurationManager().checkUserPermission("CDP_CONSENT_VIEW");
                    }
                },
                            {
                    id: "Manage Account Access",
                    text: "i18n.ProfileManagement.ManageAccountAccess",
                    toolTip: "i18n.ProfileManagement.ManageAccountAccess",
                    onClick: function() {
                        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        accountModule.presentationController.enterProfileSettings("manageAccountAccess");
                    },
                    isVisible: function (){
                        return applicationManager.getConfigurationManager().checkUserPermission("PSD2_TPP_CONSENT_VIEW");
                    }
                }
                          
                          ]
            }
        },
	/*{
        id: "Investment Banking",
        text: "i18n.wealth.investmentBanking",
        toolTip: "i18n.wealth.investmentBanking",
        icon: "&",
        subMenu: {
            children: [{
                id: "Wealth Dashboard",
                text: "i18n.wealth.wealthDashboard",
                toolTip: "i18n.wealth.wealthDashboard",
                onClick: function() {
                     var navMan = applicationManager.getNavigationManager();
					       navMan.navigateTo("CopyfrmAccountsLanding");
                }
            }]
        }
    },*/
        {
            id: "About Us",
            text: "i18n.hamburger.aboutus",
            toolTip: "i18n.hamburger.aboutus",
            icon: "A",
            subMenu: {
                children: [{
                        id: "Terms & Conditions",
                        text: "i18n.common.TnC",
                        toolTip: "i18n.common.TnC",
                        onClick: function() {
                            var termsAndConditionModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TermsAndConditionsModule");
                            termsAndConditionModule.presentationController.showTermsAndConditions(OLBConstants.TNC_FLOW_TYPES.Hamburger_TnC);
                            var formName = kony.application.getCurrentForm();
                            if(formName.id!=="frmContactUsPrivacyTandC"){
                            if (_kony.mvc.GetController(formName.id, true).view.customheader != undefined) {
                                _kony.mvc.GetController(formName.id, true).view.customheader.customhamburger.collapseAll();
                                _kony.mvc.GetController(formName.id, true).view.customheader.forceCloseHamburger();
                            }
                            else {
                                _kony.mvc.GetController(formName.id, true).view.customheadernew.collapseAll();
                                _kony.mvc.GetController(formName.id, true).view.customheadernew.forceCloseHamburger();

                            }
                          }
                        }
                    }, {
                        id: "Privacy Policy",
                        text: "i18n.footer.privacy",
                        toolTip: "i18n.footer.privacy",
                        onClick: function() {
                            var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                            informationContentModule.presentationController.showPrivacyPolicyPage();
                        }
                    }, {
                        id: "Contact Us",
                        text: "i18n.footer.contactUs",
                        toolTip: "i18n.footer.contactUs",
                        onClick: function() {
                            var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                            informationContentModule.presentationController.showContactUsPage();
                        }
                    },
                    {
                        id: "Locate Us",
                        text: "i18n.footer.locateUs",
                        toolTip: "i18n.footer.locateUs",
                        onClick: function() {
                            var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
                            locateUsModule.presentationController.showLocateUsPage();

                        }
                    },
                    {
                        id: "FAQs",
                        text: "i18n.topmenu.help",
                        toolTip: "i18n.topmenu.help",
                        onClick: function() {
                            var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                            informationContentModule.presentationController.showFAQs();
                        }
                    },
                    {
                        id: "Feedback",
                        text: "i18n.CustomerFeedback.Feedback",
                        toolTip: "i18n.CustomerFeedback.Feedback",
                        onClick: function() {
                            if (kony.application.getCurrentForm() !== "frmCustomerFeedback") {
                                var feedbackModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeedbackModule");
                                feedbackModule.presentationController.showFeedback();
                            }            
                        },
                        isVisible: function () {
                            return checkUserFeature("FEEDBACK")
                        }
                    }
                ]
            }
        },
    ];
  
    return {
        config: widgetsMap
    };
});
