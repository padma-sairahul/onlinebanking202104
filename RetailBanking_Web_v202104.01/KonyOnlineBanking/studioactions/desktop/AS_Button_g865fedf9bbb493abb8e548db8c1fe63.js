function AS_Button_g865fedf9bbb493abb8e548db8c1fe63(eventobject) {
    var self = this;
    this.login();
    this.presenter.onLogin(this, {
        "username": this.view.main.tbxUserName.text,
        "password": this.view.main.tbxPassword.text
    });
}