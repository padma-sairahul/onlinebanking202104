function AS_Calendar_f947e7c3e6154676bbbcda9a36a69b34(eventobject, isValidDateSelected) {
    var dateVal = this.view.frmPayDueAmount.flxContainer.flxMainContainer.flxBillPay.PayDueAmount.flxCalender.CalendarSendDate.data;
    if (dateVal != 2014 - 07 - 04) {
        this.view.frmPayDueAmount.flxContainer.flxMainContainer.flxBillPay.PayDueAmount.lblDueDate.isVisible = "false";
        this.view.frmPayDueAmount.flxContainer.flxMainContainer.flxBillPay.PayDueAmount.flxInfoDueDate.isVisible = "true";
    }
}