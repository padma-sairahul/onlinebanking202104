function AS_Button_b69c13c643644c4d9072f33a34f8baaf(eventobject) {
    //this.login();
    var status = true;
    if (this.view.main.imgRememberMe.src == 'unchecked_box.png') {
        status = false;
    }
    this.view.main.lblWelcome.isVisible = true;
    this.view.main.rtxErrorMsg.setVisibility(false);
    this.view.flxMain.flxLogoutMsg.setVisibility(false);
    this.view.flxMain.skin = "sknFlexLoginMainBackgroundKA";
    var enteredUserName = this.view.main.tbxUserName.text;
    if (this.checkMasked(enteredUserName) === true) {
        var unMaskedUserName = this.getUnMaskedUserName(enteredUserName);
        if (unMaskedUserName != null) this.presenter.onLogin(this, {
            "username": unMaskedUserName,
            "password": this.view.main.tbxPassword.text,
            "rememberme": status
        });
    } else this.presenter.onLogin(this, {
        "username": enteredUserName,
        "password": this.view.main.tbxPassword.text,
        "rememberme": status
    });
}