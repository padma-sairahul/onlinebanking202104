define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_AppEvents_i81ca284166540a1bac00706c79bffbc: function AS_AppEvents_i81ca284166540a1bac00706c79bffbc(eventobject) {
        var self = this;
        kony.mvc.MDAApplication.getSharedInstance().appContext.deeplinkUrl = {
            deeplinkpath: eventobject.deeplinkpath,
            formID: eventobject.formID,
        }
    },
    AS_AppEvents_c46982b2ddb840c8968e3d128fb49d07: function AS_AppEvents_c46982b2ddb840c8968e3d128fb49d07(eventobject) {
        var self = this;
    },
    AS_AppEvents_f5dafde5e96a4991ae16f91a08b8a8bc: function AS_AppEvents_f5dafde5e96a4991ae16f91a08b8a8bc(eventobject) {
        var self = this;
        kony.print("Testing JS Load");
        var isIOS13 = (/(iPad|iPhone);.*CPU.*OS 13_\d/i).test(navigator.userAgent);
        if (isIOS13) {
            kony.application.setApplicationBehaviors({
                disableForceRepaint: true
            });
        }
        require(["AccountsModule/frmAccountsLanding", "AccountsModule/frmAccountsLandingController", "AccountsModule/frmDashboard", "AccountsModule/frmDashboardController"], function() {});
        require(["BBAccountsModule/frmBBAccountsLanding", "BBAccountsModule/frmBBAccountsLandingController"], function() {});
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        var ApplicationManager = require('ApplicationManager');
        applicationManager = ApplicationManager.getApplicationManager();
        var config = applicationManager.getConfigurationManager();
        if (performance.navigation.type === 1) {
            config.setBrowserRefreshProperty("true");
        }
        var sm = applicationManager.getStorageManager();
        var langObjFromStorage = sm.getStoredItem("langObj");
        if (!kony.sdk.isNullOrUndefined(langObjFromStorage)) {
            config.configurations.setItem("LOCALE", config.locale[langObjFromStorage.language]);
            config.configurations.setItem('DATEFORMAT', config.frontendDateFormat[config.getLocale()]);
        } else {
            config.configurations.setItem("LOCALE", "en_US");
            config.configurations.setItem('DATEFORMAT', config.frontendDateFormat["en_US"]);
        }
        kony.i18n.setCurrentLocaleAsync(config.configurations.getItem("LOCALE"), function() {}, function() {});
        applicationManager.getConfigurationManager().fetchApplicationProperties(function(res) {
            applicationManager.getNavigationManager().updateForm({
                isLanguageSelectionEnabled: res.isLanguageSelectionEnabled
            }, "frmLogin");
            if (config.isAppPropertiesLoaded === "false") {
                config.setAppProperties("true");
                kony.application.dismissLoadingScreen();
            }
            //config.fetchClientSideConfigurations();
        }, function() {
            kony.application.dismissLoadingScreen();
        })
        document.body.addEventListener('contextmenu', function(e) {
            e.preventDefault();
            alert(kony.i18n.getLocalizedString("i18n.general.rightclickdisabled"));
        });
    }
});