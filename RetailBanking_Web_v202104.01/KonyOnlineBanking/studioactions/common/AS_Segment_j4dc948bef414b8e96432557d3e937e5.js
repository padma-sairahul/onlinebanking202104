function AS_Segment_j4dc948bef414b8e96432557d3e937e5(eventobject, sectionNumber, rowNumber) {
    var self = this;
    return self.onFrequencySelection.call(this, this.view.segFrequencyList, this.view.lblSelectedFrequency, this.view.imgFrequencyDropdownIcon, this.view.flxFrequencyList);
}