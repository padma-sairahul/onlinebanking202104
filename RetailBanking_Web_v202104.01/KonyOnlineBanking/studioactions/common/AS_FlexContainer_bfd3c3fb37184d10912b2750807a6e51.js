function AS_FlexContainer_bfd3c3fb37184d10912b2750807a6e51(eventobject) {
    var self = this;
    return self.toggleDropdownVisibility.call(this, this.view.flxFrequencyDropdown, this.view.flxFrequencyList, this.view.imgFrequencyDropdownIcon);
}