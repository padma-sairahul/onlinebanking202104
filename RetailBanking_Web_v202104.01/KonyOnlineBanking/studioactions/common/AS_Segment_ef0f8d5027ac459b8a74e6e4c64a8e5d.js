function AS_Segment_ef0f8d5027ac459b8a74e6e4c64a8e5d(eventobject, sectionNumber, rowNumber) {
    var self = this;
    var data = this.view.segTransactions.data;
    var index = this.view.segTransactions.selectedIndex[1];
    if (data[index].template == "segTransactionsRowSavings") {
        data[index].template = "segTransactionsRowSelectedSavings";
        this.view.segTransactions.setDataAt(data[index], index);
    } else {
        data[index].template = "segTransactionsRowSavings";
        this.view.segTransactions.setDataAt(data[index], index);
    }
}