function AS_Segment_ef85c48f225e408d8783fd51f77e63e0(eventobject, sectionNumber, rowNumber) {
    var self = this;
    return self.setDropdownSelectedValue.call(this, this.view.segTransferCCYList, this.view.lblSelectedTransferCCY, this.view.imgTransferCCYDropdownIcon, this.view.flxTransferCCYList);
}