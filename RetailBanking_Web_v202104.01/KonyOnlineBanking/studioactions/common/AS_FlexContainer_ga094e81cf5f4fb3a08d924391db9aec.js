function AS_FlexContainer_ga094e81cf5f4fb3a08d924391db9aec(eventobject) {
    var self = this;
    return self.toggleDropdownVisibility.call(this, this.view.flxTransferCCYDropdown, this.view.flxTransferCCYList, this.view.imgTransferCCYDropdownIcon);
}