function AS_FlexContainer_e360b14a31ae40089984863824e6d586(eventobject) {
    var self = this;
    return self.toggleDropdownVisibility.call(this, this.view.flxTransferDurationTypeDropdown, this.view.flxTransferDurationTypeList, this.view.imgTransferDurationTypeDropdownIcon);
}